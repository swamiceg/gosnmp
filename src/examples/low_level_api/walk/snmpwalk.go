/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to
 * perform WALK operation using webnms/snmp package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmpwalk.go [options] hostname OID
 *
 * v1 request:
 * go run snmpwalk.go [-d] [-c community] [-p port] [-t timeout] [-r retries] host OID
 * e.g. For v1 request give -v v1 or drop the option -v.
 * go run snmpwalk.go -p 161 -c public webnms .1.3.6.1
 *
 * v2c request:
 * go run snmpwalk.go [-d] [-v version(v1,v2)] [-c community] [-p port] [-t timeout] [-r retries] host OID
 * e.g.
 * go run snmpwalk.go -p 161 -v v2 -c public webnms .1.3.6.1
 *
 * v3 request:
 * go run snmpwalk.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-r retries] [-t timeout] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)] host OID
 * e.g.
 * go run snmpwalk.go -v v3 -u initial2 -a MD5 -w initial2Pass webnms .1.3.6.1
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-t] <timeout>      				 - Timeout. By default 5000ms.
 * [-r] <retries>      				 - Retries. By default 0.
 * [-v] <version>      				 - version(v1 / v2 / v3). By default v1.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-n] <contextName>  				 - The contextName to be used for the v3 pdu.
 * [-e] <engineID>					 - Remote Engine's EngineID.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host Mandatory      				 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * OID  Mandatory      				 - Give multiple no. of Object Identifiers.
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
	"webnms/snmp/util"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "walk [-d] [-v version(v1,v2, v3)] [-c community] \n" +
		"[-p port] [-r retries] [-t timeout]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] [-e engineID] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host OID\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if the user didn't supply the mandatory params
	if len(fp.RemArgs) < 2 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Create new SnmpAPI and SnmpSession instance
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	api.SetDebug(fp.Debug)

	//Create UDP options and set it on the SnmpSession
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(fp.RemArgs[0])
	udp.SetRemotePort(fp.Port)
	ses.SetProtocolOptions(udp)

	//Open a new SnmpSession
	if err = ses.Open(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer ses.Close() //Close the SnmpSession in any case
	defer api.Close() //Close the SnmpAPI in any case

	//Perform Database Initialization for SNMPv3
	if fp.Version == consts.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = api.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	mesg := msg.NewSnmpMessage()
	if fp.Version == consts.Version3 {
		err = util.Init_V3_LCD(ses,
			udp,
			fp.UserName,
			fp.EngineID, //validation should be done
			fp.AuthProtocol,
			fp.AuthPassword,
			fp.PrivProtocol,
			fp.PrivPassword,
			true, //Validate User
		)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		mesg.SetUserName(fp.UserName)
		mesg.SetContextName(fp.ContextName)
		//Set the security level for the msg.
		mesg.SetSecurityLevel(fp.GetSecurityLevel())
	}

	//Construct SnmpMessage
	mesg.SetVersion(fp.Version)
	mesg.SetCommunity(fp.Community)
	mesg.SetCommand(consts.GetNextRequest)
	mesg.SetRetries(fp.Retries)
	mesg.SetTimeout(fp.Timeout)

	oid := snmpvar.NewSnmpOID(fp.RemArgs[1])
	if oid == nil {
		fmt.Fprintln(os.Stderr, "Error: Invalid OID", "'"+fp.RemArgs[1]+"'.")
		os.Exit(1)
	}
	mesg.AddNull(*oid)
	rootOID := oid

	//Make continous getnext request till the OID is not in the SubTree
	for {
		//Make a Synchronous request using the Session opened
		if resp, err := ses.SyncSend(mesg); err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		} else {
			if resp.ErrorStatus() != 0 {
				fmt.Println(resp.ErrorString())
				break
			}
			if !(inSubTree(rootOID.Value(), *resp)) {
				fmt.Println("Not in Subtree.")
				break
			} else {
				fmt.Println(resp.PrintVarBinds() + "\n")

				oid := resp.ObjectIDAt(0)
				newMsg := mesg.CopyWithoutVarBinds()
				if oid != nil {
					newMsg.AddNull(*oid)
				}
				mesg = *newMsg
			}
		}
	}
}

//Check if first varbind oid has rootoid as an ancestor in MIB tree
func inSubTree(root []uint32, pdu msg.SnmpMessage) bool {

	oid := pdu.ObjectIDAt(0)
	if oid == nil {
		return false
	}
	oidArray := oid.Value()
	if len(oidArray) < len(root) {
		return false
	}

	for i, v := range root {
		if oidArray[i] != v {
			return false
		}
	}
	return true
}
