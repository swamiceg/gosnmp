/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to do
 * an asynchronous SNMP operation GET using webnms/snmp package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmpasynget.go [options] hostname OID [OID] ...
 *
 * v1 request:
 * go run snmpasynget.go [-d] [-c community] [-p port] [-t timeout] [-r retries] host OID [OID] ...
 * e.g. For v1 request give -v v1 or drop the option -v.
 * go run snmpasynget.go -p 161 -c public webnms 1.1.0 1.2.0
 *
 * v2c request:
 * go run snmpasynget.go [-d] [-v version(v1,v2)] [-c community] [-p port] [-t timeout] [-r retries] host OID [OID] ...
 * e.g.
 * go run snmpasynget.go -p 161 -v v2 -c public webnms 1.1.0 1.2.0
 *
 * v3 request:
 * go run snmpasynget.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-r retries] [-t timeout] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)] host OID [OID] ...
 * e.g.
 * go run snmpasynget.go -v v3 -u initial2 -a MD5 -w initial2Pass webnms 1.2.0
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-t] <timeout>      				 - Timeout. By default 5000ms.
 * [-r] <retries>      				 - Retries. By default 0.
 * [-v] <version>      				 - version(v1 / v2 / v3). By default v1.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-n] <contextName>  				 - The contextName to be used for the v3 pdu.
 * [-e] <engineID>					 - Remote Engine's EngineID.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host Mandatory      				 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * OID  Mandatory      				 - Give multiple no. of Object Identifiers.
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
	"webnms/snmp/util"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "asyncget [-d] [-v version(v1,v2,v3)] [-c community] \n" +
		"[-p port] [-r retries] [-t timeout]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] [-e engineID] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host OID [OID] ...\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if the user didn't supply the mandatory params
	if len(fp.RemArgs) < 2 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Create new SnmpAPI and SnmpSession instance
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	api.SetDebug(fp.Debug)

	//Create UDP options and set it on the SnmpSession
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(fp.RemArgs[0])
	udp.SetRemotePort(fp.Port)
	ses.SetProtocolOptions(udp)

	//Add the SnmpClient to SnmpSession for async callback
	ses.AddSnmpClient(new(client))

	//Open a new SnmpSession
	if err = ses.Open(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer ses.Close() //Close the SnmpSession in any case
	defer api.Close() //Close the SnmpAPI in any case

	//Perform Database Initialization for SNMPv3
	if fp.Version == consts.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = api.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	mesg := msg.NewSnmpMessage()
	if fp.Version == consts.Version3 {
		err = util.Init_V3_LCD(ses,
			udp,
			fp.UserName,
			fp.EngineID, //validation should be done
			fp.AuthProtocol,
			fp.AuthPassword,
			fp.PrivProtocol,
			fp.PrivPassword,
			true, //Validate User
		)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		mesg.SetUserName(fp.UserName)
		mesg.SetContextName(fp.ContextName)
		//Set the security level for the msg. If not, default security level NoAuthNoPriv will be used
		mesg.SetSecurityLevel(fp.GetSecurityLevel())
	}

	//Construct SnmpMessage
	mesg.SetVersion(fp.Version)
	mesg.SetCommunity(fp.Community)
	mesg.SetCommand(consts.GetRequest)
	mesg.SetRetries(fp.Retries)
	mesg.SetTimeout(fp.Timeout)
	//Add the list of Varbinds
	for i := 1; i < len(fp.RemArgs); i++ {
		oid := snmpvar.NewSnmpOID(fp.RemArgs[i])
		if oid != nil {
			mesg.AddNull(*oid)
		}
	}

	//Make a Asynchronous request using the Session opened
	if _, err = ses.Send(mesg); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	ses.Wait() //Wait for all the Asynchronous requests to complete
}

//Implement SnmpClient interface for async callback
type client int

func (c client) Callback(resp *msg.SnmpMessage, reqID int32) {
	if resp != nil {
		po := resp.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage
		fmt.Println("\nReceived response packet from", po.SessionID())
		fmt.Println(resp) //This will print the complete packet.
	} else {
		fmt.Println("Request Timed Out: Nil PDU received!")
	}
}

func (c client) Authenticate(res *msg.SnmpMessage, community string) bool {
	if res.Community() == community {
		return true
	}
	return true
}

func (c client) DebugStr(dbgStr string) {
	fmt.Println(dbgStr)
}
