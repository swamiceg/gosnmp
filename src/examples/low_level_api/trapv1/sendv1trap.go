/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to send a
 * v1 TRAP message using webnms/snmp package of Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run sendv1trap.go hostname enterprise-oid agent-addr generic-trap specific-trap
 *      timeticks [OID Type Value] ...
 *
 * go run sendv1trap.go [-d] [-c community] [-p port] host enterprise agent-addr generic-trap specific-trap timeticks [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) value] ...
 * e.g.
 * go run sendv1trap.go -p 162 -c public webnms 1.2.0 webnms 0 6 1000 1.5.0 OctetString WebNMSTrap
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 * * Options:
 * [-d]      		- Debug output. By default off.
 * [-c] <community> - community String. By default "public".
 * [-p] <port>      - remote port no. By default 162.
 * enterprise       - Object Identifier (sysObjectID for generic traps)
 * agent-addr       - the IP address of the agent sending the trap
 * generic-trap     - generic trap type INTEGER (0..6)
 * specific-trap    - specific code INTEGER(0..2147483647)
 * timeticks        - the value of object sysUpTime when the event occurred
 * host mandatory   - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * OID  option      - Give multiple no. of Object Identifiers with type and value.
 * Type             - Type of the object (OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString)
 * Value            - object-instance value
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"

	"fmt"
	"os"
	"strconv"
	//import database driver if needed
)

func main() {

	var usage = "trapv1 [-d] [-c community] \n" +
		"[-p port]" + "\n" +
		"host enterpriseOID agent-addr generic-type specific-type timeticks [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...\n"
	var err error

	//Print usage error if the user didn't supply the mandatory params
	if len(fp.RemArgs) < 6 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Create new SnmpAPI and SnmpSession instance
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	api.SetDebug(fp.Debug)

	//Create UDP options and set it on the SnmpSession
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(fp.RemArgs[0])
	if fp.Port > 0 {
		udp.SetRemotePort(fp.Port)
	} else {
		udp.SetRemotePort(162) //Default Trap Port
	}
	ses.SetProtocolOptions(udp)

	//Open a new SnmpSession
	if err = ses.Open(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer ses.Close() //Close the SnmpSession in any case

	//Construct SNMPV1 Trap SnmpMessage
	trap := msg.NewSnmpMessage()
	trap.SetVersion(consts.Version1)
	trap.SetCommunity(fp.Community)
	trap.SetCommand(consts.TrapRequest)
	oid := snmpvar.NewSnmpOID(fp.RemArgs[1])
	if oid != nil {
		trap.SetEnterprise(*oid) //Set the Enterprise OID
	}
	trap.SetAgentAddress(fp.RemArgs[2]) //Set the Agent address

	if gt, err := strconv.Atoi(fp.RemArgs[3]); err != nil { //Set the Trap generic trap
		fmt.Fprintln(os.Stderr, "Invalid Generic Type:", err)
		os.Exit(1)
	} else {
		trap.SetGenericType(gt)
	}
	if st, err := strconv.Atoi(fp.RemArgs[4]); err != nil { //Set the Trap specific type
		fmt.Fprintln(os.Stderr, "Invalid Specific Type:", err)
		os.Exit(1)
	} else {
		trap.SetSpecificType(st)
	}
	if uptime, err := strconv.Atoi(fp.RemArgs[5]); err != nil { //Set the sysUpTime
		fmt.Fprintln(os.Stderr, "Invalid SysUpTime:", err)
		os.Exit(1)
	} else {
		trap.SetUpTime(uint32(uptime))
	}
	//Add the list of Varbinds
	for i := 6; i < len(fp.RemArgs); i++ {
		if (len(fp.RemArgs) - i) < 3 {
			fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
			os.Exit(1)
		}
		oid := snmpvar.NewSnmpOID(fp.RemArgs[i])
		i++
		varType := GetType(fp.RemArgs[i])
		i++
		if va, err := snmpvar.CreateSnmpVar(varType, fp.RemArgs[i]); err != nil { //Create new Snmp Variable
			fmt.Fprintln(os.Stderr, "Error in creating SnmpVar:", err)
			os.Exit(1)
		} else {
			if oid != nil {
				trap.AddVarBind(msg.NewSnmpVarBind(*oid, va))
			}
		}
	}

	//Send the trap using Async request - No response needed
	if _, err = ses.Send(trap); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		host := fp.RemArgs[0] + ":" + strconv.Itoa(udp.RemotePort())
		fmt.Println("SNMPv1 Trap sent to", host)
	}
}

func GetType(t string) byte {
	switch t {
	case "OctetString":
		return consts.OctetString
	case "BitString":
		return consts.BitString
	case "SnmpNullVar":
		return consts.SnmpNullVar
	case "Counter":
		return consts.Counter
	case "Counter64":
		return consts.Counter64
	case "Gauge":
		return consts.Gauge
	case "Opaque":
		return consts.Opaque
	case "Integer":
		return consts.Integer
	case "ObjectIdentifier":
		return consts.ObjectIdentifier
	case "IpAddress":
		return consts.IpAddress
	case "TimeTicks":
		return consts.TimeTicks
	}

	return byte(0)
}
