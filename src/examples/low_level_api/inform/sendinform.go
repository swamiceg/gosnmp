/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to send a
 * v2c/v3 INFORM message using webnms/snmp package of Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run sendinform.go [OPTIONS] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 *
 * v2c Inform:
 * go run sendinform.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 * e.g.
 * go run sendinform.go -p 162 -c public webnms 16535 1.2.0 1.5.0 OctetString WebNMSInform
 *
 *
 * v3 Inform:
 * go run sendinform.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 * e.g.
 * go run sendinform.go -p 162 -c public -v v3 -u initial2 -a MD5 -w initial2Pass webnms 16535 1.2.0 1.5.0 OctetString WebNMSInform
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]      					     - Debug output. By default off.
 * [-v] <version>      				 - version(v2 / v3). By default v2c.
 * [-c] <community>     			 - community String. By default "public".
 * [-p] <port>     					 - remote port no. By default 162.
 * [-e] <engineID>					 - Remote Engine's EngineID.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host      Mandatory 			 	 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * timeticks Mandatory 			 	 - the value of object sysUpTime when the event occurred
 * OID-value Mandatory 			 	 - Object Identifier
 * OID                			 	 - Give multiple no. of Object Identifiers with value.
 * Type               			 	 - Type of the object (OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString).
 * Value               			 	 - object-instance value
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
	"webnms/snmp/util"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "inform [-d] [-v version(v2,v3)] [-c community] \n" +
		"[-p port]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] [-e engineID] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...\n"
	var err error

	//Validate the flags supplied
	if err := fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if the user didn't supply the mandatory params
	if len(fp.RemArgs) < 3 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	if fp.Version != consts.Version2C && fp.Version != consts.Version3 {
		fmt.Fprintln(os.Stderr, fmt.Sprintf("Unsupported version: %s to send inform message. Defaulting to SNMPv2c.", fp.Version))
		fp.Version = consts.Version2C
	}

	//Create new SnmpAPI and SnmpSession instance
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	api.SetDebug(fp.Debug)

	//Create UDP options and set on the SnmpSession
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(fp.RemArgs[0])
	if fp.Port > 0 {
		udp.SetRemotePort(fp.Port)
	} else {
		udp.SetRemotePort(162) //Default Trap Port
	}
	ses.SetProtocolOptions(udp)

	//Add the SnmpClient to SnmpSession for async callback- to receive response for Inform request
	ses.AddSnmpClient(new(informClient))

	//Open a new SnmpSession
	if err = ses.Open(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer ses.Close() //Close the SnmpSession in any case
	defer api.Close()

	//Perform Database Initialization for SNMPv3
	if fp.Version == consts.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = api.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}
	fmt.Println(fp.PrivProtocol)
	inform := msg.NewSnmpMessage()
	if fp.Version == consts.Version3 {
		//We are not authoritative in case of sending Inform message,
		//Perform LCD initialization
		err = util.Init_V3_LCD(ses,
			udp,
			fp.UserName,
			fp.EngineID, //validation should be done
			fp.AuthProtocol,
			fp.AuthPassword,
			fp.PrivProtocol,
			fp.PrivPassword,
			false, //Dont Validate User
		)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		inform.SetUserName(fp.UserName)
		inform.SetContextName(fp.ContextName)
		//Set the security level for the msg.
		inform.SetSecurityLevel(fp.GetSecurityLevel())
	}

	//Construct Inform SnmpMessage
	inform.SetVersion(fp.Version)
	inform.SetCommunity(fp.Community)
	inform.SetCommand(consts.InformRequest)

	//Add TimeTicks VarBind
	oid := snmpvar.NewSnmpOID(".1.3.6.1.2.1.1.3.0")
	if va, err := snmpvar.CreateSnmpVar(consts.TimeTicks, fp.RemArgs[1]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		if oid != nil {
			inform.AddVarBind(msg.NewSnmpVarBind(*oid, va))
		}
	}

	//Add TrapOID VarBind
	oid = snmpvar.NewSnmpOID(".1.3.6.1.6.3.1.1.4.1.0")
	if va, err := snmpvar.CreateSnmpVar(consts.ObjectIdentifier, fp.RemArgs[2]); err != nil {
		fmt.Fprintln(os.Stderr, "Invalid TrapOID:", err)
		os.Exit(1)
	} else {
		if oid != nil {
			inform.AddVarBind(msg.NewSnmpVarBind(*oid, va))
		}
	}

	//Add the list of Varbinds
	for i := 3; i < len(fp.RemArgs); i++ {
		if (len(fp.RemArgs) - i) < 3 {
			fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
			os.Exit(1)
		}
		oid := snmpvar.NewSnmpOID(fp.RemArgs[i])
		i++
		varType := GetType(fp.RemArgs[i])
		i++
		if va, err := snmpvar.CreateSnmpVar(varType, fp.RemArgs[i]); err != nil { //Create new Snmp Variable
			fmt.Fprintln(os.Stderr, "Error in creating SnmpVar.", err)
			os.Exit(1)
		} else {
			if oid != nil {
				inform.AddVarBind(msg.NewSnmpVarBind(*oid, va))
			}
		}
	}

	//Make a Asynchronous request using the Session opened
	if _, err = ses.Send(inform); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		fmt.Println("Inform request sent to", fp.RemArgs[0], "!")
	}
	ses.Wait()
}

//Implement SnmpClient interface for async callback
type informClient int

func (c informClient) Callback(resp *msg.SnmpMessage, reqID int32) {

	if resp != nil {
		po := resp.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage
		fmt.Println("\nReceived inform response from", po.SessionID(), ":")
		fmt.Println(resp)
	} else {
		fmt.Println("Nil PDU received!")
	}
}

func (c informClient) Authenticate(res *msg.SnmpMessage, community string) bool {
	if res.Community() == community {
		return true
	}
	return true
}

func (c informClient) DebugStr(dbgStr string) {
	fmt.Println(dbgStr)
}

func GetType(t string) byte {
	switch t {
	case "OctetString":
		return consts.OctetString
	case "BitString":
		return consts.BitString
	case "SnmpNullVar":
		return consts.SnmpNullVar
	case "Counter":
		return consts.Counter
	case "Counter64":
		return consts.Counter64
	case "Gauge":
		return consts.Gauge
	case "Opaque":
		return consts.Opaque
	case "Integer":
		return consts.Integer
	case "ObjectIdentifier":
		return consts.ObjectIdentifier
	case "IpAddress":
		return consts.IpAddress
	case "TimeTicks":
		return consts.TimeTicks
	}

	return byte(0)
}
