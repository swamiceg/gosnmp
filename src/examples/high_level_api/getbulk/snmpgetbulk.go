/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to do
 * the basic SNMP operation GET-BULK using webnms/snmp/hl package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmpgetbulk.go [options] hostname non-repeaters max-repetitions OID [OID] ...
 *
 * Note: Non-repeaters and Max-Repetitions are mandatory arguments. Version should be at least v2c.
 *
 * v2c request:
 * go run snmpgetbulk.go [-d] [-v version(v1,v2)] [-c community] [-p port] [-t timeout] [-r retries] host Non-Repeaters Max-Repetitions OID [OID] ...
 * e.g.
 * go run snmpgetbulk.go -p 161 -v v2 -c public webnms 1 2 1.1.0 1.2.0
 *
 * v3 request:
 * go run snmpgetbulk.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-r retries] [-t timeout] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)] host Non-Repeaters Max-Repetitions OID [OID] ...
 * e.g.
 * go run snmpgetbulk.go -v v3 -u initial2 -a MD5 -w initial2Pass webnms 1 2 1.2.0
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-t] <timeout>      				 - Timeout. By default 5000ms.
 * [-r] <retries>      				 - Retries. By default 0.
 * [-v] <version>      				 - version(v2 / v3). By default v2c.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  		     - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-n] <contextName>  				 - The contextName to be used for the v3 pdu.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host Mandatory      				 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * nonRepeaters Mandatory      		 - non-repeaters. Mandatory
 * maxRepetions Mandatory			 - max-repetitions. Mandatory
 * OID  Mandatory      				 - Give multiple no. of Object Identifiers.
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"

	"fmt"
	"os"
	"strconv"
	//import database driver if needed
)

func main() {

	var usage = "getbulk [-d] [-v version(v2,v3)] [-c community] \n" +
		"[-p port] [-r retries] [-t timeout]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host Non-Repeaters Max-Repetitions OID [OID] ...\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if user didn't supply the mandatory params
	if len(fp.RemArgs) < 4 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	if fp.Version != hl.Version2C && fp.Version != hl.Version3 {
		fmt.Fprintln(os.Stderr, fmt.Sprintf("Unsupported version: %s to perform Get-Bulk operation. Defaulting to SNMPv2c.", fp.Version))
		fp.Version = hl.Version2C
	}

	//Create a new SnmpTarget instance
	target := hl.NewSnmpTarget()

	target.SetDebug(fp.Debug)
	target.SetVersion(fp.Version)
	target.SetCommunity(fp.Community)

	target.SetTargetHost(fp.RemArgs[0])
	target.SetTargetPort(fp.Port)
	target.SetRetries(fp.Retries)
	target.SetTimeout(fp.Timeout)

	if nr, err := strconv.Atoi(fp.RemArgs[1]); err != nil {
		fmt.Fprintln(os.Stderr, "Invalid Non-Repeaters:", err)
		os.Exit(1)
	} else {
		target.SetNonRepeaters(int32(nr))
	}

	if mr, err := strconv.Atoi(fp.RemArgs[2]); err != nil {
		fmt.Fprintln(os.Stderr, "Invalid Max-Repetitions:", err)
		os.Exit(1)
	} else {
		target.SetMaxRepetitions(int32(mr))
	}

	target.SetOIDList(fp.RemArgs[3:])

	defer target.ReleaseResources() //Release the underlying low-level resources in any case.

	//Perform Database Initialization for SNMPv3
	if fp.Version == hl.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = target.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	//Perform LCD initialization for SNMPv3
	if fp.Version == hl.Version3 {
		target.SetPrincipal(fp.UserName)
		target.SetContextName(fp.ContextName)
		target.SetAuthProtocol(fp.AuthProtocol)
		target.SetAuthPassword(fp.AuthPassword)
		target.SetPrivProtocol(fp.PrivProtocol)
		target.SetPrivPassword(fp.PrivPassword)

		_, err = target.CreateV3Tables()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}

	if result, err := target.GetBulkVariableBindings(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		fmt.Println("Response received. Values:")
		for _, val := range result {
			fmt.Println(val)
		}
	}
}
