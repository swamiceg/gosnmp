/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to send a
 * v1 TRAP message using webnms/snmp/hl package of Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run sendv1trap.go hostname enterprise-oid agent-addr generic-trap specific-trap
 *      timeticks [OID Type Value] ...
 *
 * go run sendv1trap.go [-d] [-c community] [-p port] host enterprise agent-addr generic-trap specific-trap timeticks [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) value] ...
 * e.g.
 * go run sendv1trap.go -p 162 -c public webnms 1.2.0 webnms 0 6 1000 1.5.0 OctetString WebNMSTrap
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 * * Options:
 * [-d]      		- Debug output. By default off.
 * [-c] <community> - community String. By default "public".
 * [-p] <port>      - remote port no. By default 162.
 * host mandatory   - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * enterprise       - Object Identifier (sysObjectID for generic traps)
 * agent-addr       - the IP address of the agent sending the trap
 * generic-trap     - generic trap type INTEGER (0..6)
 * specific-trap    - specific code INTEGER(0..2147483647)
 * timeticks        - the value of object sysUpTime when the event occurred
 * OID  option      - Give multiple no. of Object Identifiers with type and value.
 * Type             - Type of the object (OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString)
 * Value            - object-instance value
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"
	"webnms/snmp/snmpvar"

	"fmt"
	"os"
	"strconv"
	//import database driver if needed
)

func main() {

	var usage = "trapv1 [-d] [-c community] \n" +
		"[-p port]" + "\n" +
		"host enterpriseOID agent-addr generic-type specific-type timeticks [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if user didn't supply the mandatory params
	if len(fp.RemArgs) < 6 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Create a new SnmpTarget instance
	target := hl.NewSnmpTarget()

	target.SetDebug(fp.Debug)
	target.SetCommunity(fp.Community)

	target.SetTargetHost(fp.RemArgs[0])
	if fp.Port > 0 {
		target.SetTargetPort(fp.Port)
	} else {
		target.SetTargetPort(162) //Default Trap Port
	}

	defer target.ReleaseResources() //Release the underlying low-level resources in any case.

	enterprise := fp.RemArgs[1]
	agentAddr := fp.RemArgs[2]

	var genericType int
	var specType int
	var upTime int

	if gt, err := strconv.Atoi(fp.RemArgs[3]); err != nil { //Set the Trap generic trap
		fmt.Fprintln(os.Stderr, "Invalid Generic Type:", err)
		os.Exit(1)
	} else {
		genericType = gt
	}

	if st, err := strconv.Atoi(fp.RemArgs[4]); err != nil { //Set the Trap generic trap
		fmt.Fprintln(os.Stderr, "Invalid Specific Type:", err)
		os.Exit(1)
	} else {
		specType = st
	}

	if ut, err := strconv.Atoi(fp.RemArgs[5]); err != nil { //Set the sysUpTime
		fmt.Fprintln(os.Stderr, "Invalid SysUpTime:", err)
		os.Exit(1)
	} else {
		upTime = ut
	}

	count := 0
	oidlist := make([]string, len(fp.RemArgs[1:])/3)
	varlist := make([]snmpvar.SnmpVar, len(fp.RemArgs[1:])/3)

	//Add list of OIDs
	for i := 6; i < len(fp.RemArgs); i++ {
		if (len(fp.RemArgs) - i) < 3 {
			fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
			os.Exit(1)
		}

		oidlist[count] = fp.RemArgs[i]
		i++
		varType := GetType(fp.RemArgs[i])
		i++
		if va, err := snmpvar.CreateSnmpVar(varType, fp.RemArgs[i]); err != nil { //Create new Snmp Variable
			fmt.Fprintln(os.Stderr, "Error in creating SnmpVar:", err)
			os.Exit(1)
		} else {
			varlist[count] = va
		}
		count++
	}
	target.SetOIDList(oidlist)

	if err = target.SendTrap(enterprise, agentAddr, genericType, specType, uint32(upTime), varlist); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		host := fp.RemArgs[0] + ":" + strconv.Itoa(target.TargetPort())
		fmt.Println("SNMPv1 Trap sent to", host)
	}
}

func GetType(t string) byte {
	switch t {
	case "OctetString":
		return hl.OctetString
	case "BitString":
		return hl.BitString
	case "SnmpNullVar":
		return hl.SnmpNullVar
	case "Counter":
		return hl.Counter
	case "Counter64":
		return hl.Counter64
	case "Gauge":
		return hl.Gauge
	case "Opaque":
		return hl.Opaque
	case "Integer":
		return hl.Integer
	case "ObjectIdentifier":
		return hl.ObjectIdentifier
	case "IpAddress":
		return hl.IpAddress
	case "TimeTicks":
		return hl.TimeTicks
	}

	return byte(0)
}
