/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to
 * receive v2c/v3 INFORMS using webnms/snmp/hl package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmpinformd.go [options]
 *
 * Receive v2c inform :
 * go run snmpinformd.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port]
 * e.g.
 * go run snmpinformd.go -p 161 -v v2 -c public
 *
 * Receive v3 inform:
 * go run snmpinformd.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-e engineID] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)]
 * e.g.
 * go run snmpinformd.go -v v3 -u initial2 -a MD5 -w initial2Pass -e 127.0.0.1#161
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-v] <version>      				 - version(v2 / v3). By default v2c.
 * [-e] <engineID>					 - Remote Engine's EngineID to receive v3 Informs.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"
	"webnms/snmp/msg"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "informd [-d] [-v version(v2,v3)] [-p port] [-c community] \n" +
		"[-u username] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] [-e engineID] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error
	if len(fp.RemArgs) > 0 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	if fp.Version != hl.Version2C && fp.Version != hl.Version3 {
		fmt.Fprintln(os.Stderr, fmt.Sprintf("Unsupported version: %s to receive inform messages. Defaulting to SNMPv2c.", fp.Version))
		fp.Version = hl.Version2C
	}

	var trapPort int
	if fp.Port > 0 {
		trapPort = fp.Port
	} else {
		trapPort = 162 //Default Trap Port
	}

	receiver, err := hl.NewTrapReceiverByPort(trapPort)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error in starting InformReceiver:", err)
		os.Exit(1)
	}
	receiver.SetDebug(fp.Debug)
	receiver.SetCommunity(fp.Community)
	receiver.SetAutoInformResponse(true)
	//Authenticate all the incoming traps
	receiver.AuthenticateTraps(true)

	receiver.AddTrapListener(new(trapReceiver))

	//Perform Database Initialization for SNMPv3
	if fp.Version == hl.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = receiver.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	//For SNMPv3
	//We are authorititative when receiving informs. Hence initialize the user and engine
	//entry in LCD.
	if fp.Version == hl.Version3 {

		var engID []byte = fp.EngineID
		if engID == nil {
			engID = receiver.SnmpEngine().SnmpEngineID()
		} else {
			//Update the local entity's EngineID
			receiver.SnmpEngine().SetSnmpEngineID(engID)
		}

		receiver.SetPrincipal(fp.UserName)
		receiver.SetContextName(fp.ContextName)
		receiver.SetAuthProtocol(fp.AuthProtocol)
		receiver.SetAuthPassword(fp.AuthPassword)
		receiver.SetPrivProtocol(fp.PrivProtocol)
		receiver.SetPrivPassword(fp.PrivPassword)

		receiver.AddInformUser() //User added to the local Entity's Engine
	}

	fmt.Println("Inform receiver started at port", receiver.Port())

	select {} //Wait for inform messages indefinitely
}

type trapReceiver int

func (t trapReceiver) ReceiveTrap(trap msg.SnmpMessage) {
	po := trap.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage

	if trap.Command() == hl.InformRequest {
		fmt.Println("\nReceived Inform from", po.SessionID(), ":")
		fmt.Println(trap)
	} else {
		fmt.Fprint(os.Stderr, "\nReceived non-inform request!")
	}
}
