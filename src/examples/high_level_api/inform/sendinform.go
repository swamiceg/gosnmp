/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to send a
 * v2c/v3 INFORM message using webnms/snmp/hl package of Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run sendinform.go [OPTIONS] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 *
 * v2c Inform:
 * go run sendinform.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 * e.g.
 * go run sendinform.go -p 162 -c public webnms 16535 1.2.0 1.5.0 OctetString WebNMSInform
 *
 *
 * v3 Inform:
 * go run sendinform.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] host timeticks TrapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...
 * e.g.
 * go run sendinform.go -p 162 -c public -v v3 -u initial2 -a MD5 -w initial2Pass webnms 16535 1.2.0 1.5.0 OctetString WebNMSInform
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]      					     - Debug output. By default off.
 * [-v] <version>      				 - version(v2 / v3). By default v2c.
 * [-c] <community>     			 - community String. By default "public".
 * [-p] <port>     					 - remote port no. By default 162.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-n] <contextName>  				 - The contextName to be used for the v3 pdu.
 * [-e] <engineID>					 - Remote Engine's EngineID.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host      Mandatory 			 	 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * timeticks Mandatory 			 	 - the value of object sysUpTime when the event occurred
 * OID-value Mandatory 			 	 - Object Identifier
 * OID                			 	 - Give multiple no. of Object Identifiers with value.
 * Type               			 	 - Type of the object (OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString).
 * Value               			 	 - object-instance value
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"
	"webnms/snmp/snmpvar"

	"fmt"
	"os"
	"strconv"
	//import database driver if needed
)

func main() {

	var usage = "inform [-d] [-v version(v2,v3)] [-c community] \n" +
		"[-p port]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host timeticks trapOID [OID Type(OctetString|Integer|ObjectIdentifier|IpAddress|TimeTicks|Gauge|Counter|Counter64|Opaque|BitString) Value] ...\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if user didn't supply the mandatory params
	if len(fp.RemArgs) < 3 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	if fp.Version != hl.Version2C && fp.Version != hl.Version3 {
		fmt.Fprintln(os.Stderr, fmt.Sprintf("Unsupported version: %s to send inform message. Defaulting to SNMPv2c.", fp.Version))
		fp.Version = hl.Version2C
	}

	//Create a new SnmpTarget instance
	target := hl.NewSnmpTarget()

	target.SetDebug(fp.Debug)
	target.SetCommunity(fp.Community)

	target.SetTargetHost(fp.RemArgs[0])
	if fp.Port > 0 {
		target.SetTargetPort(fp.Port)
	} else {
		target.SetTargetPort(162) //Default Trap Port
	}
	target.SetVersion(fp.Version)

	defer target.ReleaseResources() //Release the underlying low-level resources in any case.

	var upTime int
	if ut, err := strconv.Atoi(fp.RemArgs[1]); err != nil { //Set the sysUpTime
		fmt.Fprintln(os.Stderr, "Invalid SysUpTime:", err)
		os.Exit(1)
	} else {
		upTime = ut
	}

	trapOid := fp.RemArgs[2]

	count := 0
	oidlist := make([]string, len(fp.RemArgs[1:])/3)
	varlist := make([]snmpvar.SnmpVar, len(fp.RemArgs[1:])/3)

	//Add list of OIDs
	for i := 3; i < len(fp.RemArgs); i++ {
		if (len(fp.RemArgs) - i) < 3 {
			fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
			os.Exit(1)
		}

		oidlist[count] = fp.RemArgs[i]
		i++
		varType := GetType(fp.RemArgs[i])
		i++
		if va, err := snmpvar.CreateSnmpVar(varType, fp.RemArgs[i]); err != nil { //Create new Snmp Variable
			fmt.Fprintln(os.Stderr, "Error in creating SnmpVar:", err)
			os.Exit(1)
		} else {
			varlist[count] = va
		}
		count++
	}
	target.SetOIDList(oidlist)

	//Perform Database Initialization for SNMPv3
	if fp.Version == hl.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = target.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	//For SNMPv3
	//We are not authoritative in case of sending Inform message,
	//Perform LCD initialization
	if fp.Version == hl.Version3 {
		target.SetPrincipal(fp.UserName)
		target.SetContextName(fp.ContextName)
		target.SetAuthProtocol(fp.AuthProtocol)
		target.SetAuthPassword(fp.AuthPassword)
		target.SetPrivProtocol(fp.PrivProtocol)
		target.SetPrivPassword(fp.PrivPassword)

		_, err = target.CreateV3Tables()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}

	//Check whether valid trap oid is provided.
	if snmpvar.NewSnmpOID(trapOid) == nil {
		fmt.Fprintln(os.Stderr, "Invalid TrapOID specified:", trapOid)
		os.Exit(1)
	}

	if result, err := target.SendInform(uint32(upTime), trapOid, varlist); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		fmt.Println("Response received. Values:")
		for _, val := range result {
			fmt.Println(val)
		}
	}
}

func GetType(t string) byte {
	switch t {
	case "OctetString":
		return hl.OctetString
	case "BitString":
		return hl.BitString
	case "SnmpNullVar":
		return hl.SnmpNullVar
	case "Counter":
		return hl.Counter
	case "Counter64":
		return hl.Counter64
	case "Gauge":
		return hl.Gauge
	case "Opaque":
		return hl.Opaque
	case "Integer":
		return hl.Integer
	case "ObjectIdentifier":
		return hl.ObjectIdentifier
	case "IpAddress":
		return hl.IpAddress
	case "TimeTicks":
		return hl.TimeTicks
	}

	return byte(0)
}
