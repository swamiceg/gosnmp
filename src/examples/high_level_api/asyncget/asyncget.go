/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with this product
 * for terms and conditions associated with the usage of this source file.
 */

/**
 * This is an example program to explain how to write an application to do
 * an asynchronous SNMP operation GET using webnms/snmp/hl package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmpasynget.go [options] hostname OID [OID] ...
 *
 * v1 request:
 * go run snmpasynget.go [-d] [-c community] [-p port] [-t timeout] [-r retries] host OID [OID] ...
 * e.g. For v1 request give -v v1 or drop the option -v.
 * go run snmpasynget.go -p 161 -c public webnms 1.1.0 1.2.0
 *
 * v2c request:
 * go run snmpasynget.go [-d] [-v version(v1,v2)] [-c community] [-p port] [-t timeout] [-r retries] host OID [OID] ...
 * e.g.
 * go run snmpasynget.go -p 161 -v v2 -c public webnms 1.1.0 1.2.0
 *
 * v3 request:
 * go run snmpasynget.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-r retries] [-t timeout] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)] host OID [OID] ...
 * e.g.
 * go run snmpasynget.go -v v3 -u initial2 -a MD5 -w initial2Pass webnms 1.2.0
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-t] <timeout>      				 - Timeout. By default 5000ms.
 * [-r] <retries>      				 - Retries. By default 0.
 * [-v] <version>      				 - version(v1 / v2 / v3). By default v1.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-n] <contextName>  				 - The contextName to be used for the v3 pdu.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 * host Mandatory      				 - The RemoteHost (agent).Format (string without double qoutes/IpAddress).
 * OID  Mandatory      				 - Give multiple no. of Object Identifiers.
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"
	"webnms/snmp/msg"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "asyncget [-d] [-v version(v1,v2,v3)] [-c community] \n" +
		"[-p port] [-r retries] [-t timeout]" + "\n" +
		"[-u username] [-n contextname] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]" + "\n" +
		"host OID [OID] ...\n"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error if user didn't supply the mandatory params
	if len(fp.RemArgs) < 2 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Create a new SnmpTarget instance
	server := hl.NewSnmpRequestServer()
	server.SetDebug(fp.Debug)

	//Set Message parameters
	server.SetVersion(fp.Version)
	server.SetCommunity(fp.Community)
	server.SetTargetHost(fp.RemArgs[0])
	server.SetTargetPort(fp.Port)
	server.SetRetries(fp.Retries)
	server.SetTimeout(fp.Timeout)
	server.SetOIDList(fp.RemArgs[1:])

	//Add the listener for which the responses should be received.
	server.AddListener(new(listener))
	server.SendTimeoutMsg(true)

	defer server.ReleaseResources() //Release the underlying low-level resources in any case.

	//Perform Database Initialization for SNMPv3
	if fp.Version == hl.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = server.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	//Perform LCD initialization for SNMPv3
	if fp.Version == hl.Version3 {
		server.SetPrincipal(fp.UserName)
		server.SetContextName(fp.ContextName)
		server.SetAuthProtocol(fp.AuthProtocol)
		server.SetAuthPassword(fp.AuthPassword)
		server.SetPrivProtocol(fp.PrivProtocol)
		server.SetPrivPassword(fp.PrivPassword)

		_, err = server.CreateV3Tables()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}

	//Send Async Get request with the RequestServer
	if _, err = server.GetList(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	select {}
}

type listener int

func (l listener) ReceiveMessage(msg *msg.SnmpMessage) {
	if msg != nil {
		po := msg.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage
		fmt.Println("\nReceived response packet from", po.SessionID())
		fmt.Println(msg)
	} else {
		fmt.Println("Request Timed Out.")
	}
}
