/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the associated COPYRIGHTS file for more details.
 */

/**
 * This is an example program to explain how to write an application to
 * receive v1/v2c/v3 TRAPS using webnms/snmp/hl package of
 * Go SNMP API.
 * The user could run this application by giving any one of the following usage.
 *
 * go run snmptrapd.go [options]
 *
 * Receive v1 trap:
 * go run snmptrapd.go [-d] [-c community] [-p port]
 * e.g. For v1 request give -v v1 or drop the option -v.
 * go run snmptrapd.go -p 161 -c public
 *
 * Receive v2c trap :
 * go run snmptrapd.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port]
 * e.g.
 * go run snmptrapd.go -p 161 -v v2 -c public
 *
 * Receive v3 trap:
 * go run snmptrapd.go [-d] [-v version(v1,v2,v3)] [-c community] [-p port] [-e engineID] [-u user] [-a auth_protocol] [-w auth_password] [-s priv_password] [-pp privProtocol(DES/AES-128/AES-192/AES-256/3DES)]
 * e.g.
 * go run snmptrapd.go -v v3 -u initial2 -a MD5 -w initial2Pass -e 127.0.0.1#161
 *
 * If the oid is not starting with a dot (.) it will be prefixed by .1.3.6.1.2.1 .
 * So the entire OID of 1.1.0 will become .1.3.6.1.2.1.1.1.0 . You can also
 * give the entire OID .
 *
 * Options:
 * [-d]                				 - Debug output. By default off.
 * [-c] <community>    				 - community String. By default "public".
 * [-p] <port>         				 - remote port no. By default 161.
 * [-v] <version>      				 - version(v1 / v2 / v3). By default v1.
 * [-e] <engineID>					 - Remote Engine's EngineID to receive v3 traps.
 * [-u] <username>     				 - The v3 principal/userName
 * [-a] <authProtocol>  			 - The authProtocol(MD5/SHA). Mandatory if authPassword is specified
 * [-pp]<privProtocol> 				 - The privProtocol(DES/3DES/AES-128/AES-192/AES-256).
 * [-w] <authPassword> 				 - The authentication password.
 * [-s] <privPassword> 				 - The privacy protocol password. Must be accompanied with auth password and authProtocol fields.
 * [-DB_Driver] <driverName> 		 - SQL Database drivername.
 * [-DB_DataSource] <dataSourceName> - Valid data source name string within double quotes (Ex: "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable").
 * [-DB_Name] <databaseName> 		 - Name of the database (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase).
 */

package main

import (
	fp "examples/parser"
	"webnms/snmp/hl"
	"webnms/snmp/msg"

	"fmt"
	"os"
	//import database driver if needed
)

func main() {

	var usage = "trapd [-d] [-v version(v1,v2,v3)] [-p port] [-c community] \n" +
		"[-u username] [-a authprotocol (MD5/SHA)] [-w authpassword]" + "\n" +
		"[-pp privprotocol (DES/3DES/AES-128/AES-192/AES-256)] [-s privpassword] [-e engineID] " + "\n" +
		"[-DB_Driver DatabaseDriverName] [-DB_DataSource DataSourceName] " +
		"[-DB_Name Database Name (Postgres/MySQL/Sqlite/SqlServer/Oracle/DB2/Sybase)]"
	var err error

	//Validate the flags supplied
	if err = fp.ValidateFlags(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	//Print usage error
	if len(fp.RemArgs) > 0 {
		fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
		os.Exit(1)
	}

	var trapPort int
	if fp.Port > 0 {
		trapPort = fp.Port
	} else {
		trapPort = 162 //Default Trap Port
	}

	receiver, err := hl.NewTrapReceiverByPort(trapPort)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error in starting TrapReceiver:", err)
		os.Exit(1)
	}
	receiver.SetDebug(fp.Debug)
	receiver.SetCommunity(fp.Community)
	receiver.AddTrapListener(new(trapReceiver))
	//Authenticate all the incoming traps
	receiver.AuthenticateTraps(true)

	//Perform Database Initialization for SNMPv3
	if fp.Version == hl.Version3 {
		if fp.DriverName != "" || fp.DataSrcName != "" {
			if fp.DriverName == "" {
				fmt.Fprintln(os.Stderr, "Driver Name is empty. Cannot initialize Database.")
				os.Exit(1)
			}
			if fp.DataSrcName == "" {
				fmt.Fprintln(os.Stderr, "Data source name is empty. Cannot initialize Database.")
				os.Exit(1)
			}

			err = receiver.InitDB(fp.DriverName, fp.DataSrcName, fp.DialectID)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	//For SNMPv3
	//We are not authorititative when receiving traps. Hence initialize the user and engine
	//entry in LCD
	if fp.Version == hl.Version3 {
		//We should perform discovery and time sync before receiving traps.
		//But just for the case of example, we are creating the entries manually.

		var engID []byte = fp.EngineID
		if engID == nil {
			fmt.Fprintln(os.Stderr, "Remote entity's SnmpEngineID is required for receiving v3 traps.")
			fmt.Fprintln(os.Stderr, "Usage:", "\n"+usage)
			os.Exit(1)
		}

		receiver.SetPrincipal(fp.UserName)
		receiver.SetContextName(fp.ContextName)
		receiver.SetAuthProtocol(fp.AuthProtocol)
		receiver.SetAuthPassword(fp.AuthPassword)
		receiver.SetPrivProtocol(fp.PrivProtocol)
		receiver.SetPrivPassword(fp.PrivPassword)

		receiver.AddTrapUser(engID) //User added for the Remote Entity's Engine
	}

	fmt.Println("Trap receiver started at port", receiver.Port())

	select {}
}

type trapReceiver int

func (t trapReceiver) ReceiveTrap(trap msg.SnmpMessage) {
	po := trap.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage

	if trap.Command() == hl.TrapRequest {
		fmt.Println("\nReceived V1 Trap from", po.SessionID(), ":")
		fmt.Println(trap)
	} else if trap.Command() == hl.Trap2Request {
		fmt.Println("\nReceived V2 Trap from", po.SessionID(), ":")
		fmt.Println(trap)
	} else {
		fmt.Fprintln(os.Stderr, "\nReceived non-trap request!")
	}
}
