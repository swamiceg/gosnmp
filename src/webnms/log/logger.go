/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package log

import (
	"errors"
	"fmt"
	"runtime"
	"strings"
	"time"
)

//Logging levels
type LogLevel int

const (
	ALL LogLevel = iota
	FINEST
	TRACE
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
	PERF
)

var levelStrings = [...]string{"ALL", "FINEST", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "PERF"}

func (l LogLevel) String() string {
	if l < ALL || l > PERF {
		return "<nil>"
	}
	return levelStrings[int(l)]
}

//LogRecord struct to store the complete Log message with details
type LogRecord struct {
	Level   LogLevel  //Level of the Log message
	Created time.Time //Date and Time of the Log creation
	Source  string    //Source string representing the caller of this Log
	Message string    //Log message string
}

//A Filter represents the log level below which no log records
//are written to the associated LogWriter.
type Filter struct {
	Level LogLevel
	LogWriter
}

//Logger is a collection of log writers along with the log level (Filters), through which logs are forwarded to different destination.
//This is the basic type which should be used for logging.
type Logger map[string]*Filter

//Creates a new Logger
func NewLogger() Logger {
	return make(Logger)
}

//NewDefaultLogger returns the default Logger with the console 'stdout' as the destination.
func NewDefaultLogger(lvl LogLevel) Logger {
	return Logger{"stdout": &Filter{lvl, NewConsoleLogWriter()}}
}

//AddFilter adds a new LogWriter to the Logger, which only logs messages at the level lvl or higher.
//
//'filterName' is used to uniquely identify the different LogWriters/Level combination under this Logger.
func (log Logger) AddFilter(filterName string, lvl LogLevel, writer LogWriter) Logger {
	log[filterName] = &Filter{lvl, writer}
	return log
}

//Close closes all the LogWriters under this Logger.
func (log Logger) Close() {
	for key, filter := range log {
		filter.Close()
		delete(log, key)
	}
}

//Logf logs the message at the given log level to all the LogWriters configured under this Logger.
//It formats the log message using format string provided and uses the caller as its Source.
func (log Logger) Logf(lvl LogLevel, format string, args ...interface{}) {
	log.intLogf(lvl, format, args...)
}

//Used for internal call
func (log Logger) intLogf(lvl LogLevel, format string, args ...interface{}) {
	skip := true
	for _, filter := range log {
		if lvl >= filter.Level {
			skip = false
		}
	}
	if skip {
		return
	}
	// Determine caller function
	pc, _, lineno, ok := runtime.Caller(2)
	src := ""
	if ok {
		src = fmt.Sprintf("%s:%d", runtime.FuncForPC(pc).Name(), lineno)
	}

	msg := format
	if len(args) > 0 {
		msg = fmt.Sprintf(format, args...) //Format the args.. based on the 'format' string
	}

	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  src,
		Message: msg,
	}

	for _, filter := range log {
		if lvl >= filter.Level { //Write only when the lvl is equal (or) greater than the Filter log level
			filter.Write(rec)
		}
	}
}

//Logc logs ths string returned by the closure at the given log level lvl.
//Caller is used as a source of logging.
func (log Logger) Logc(lvl LogLevel, closure func() string) {
	log.intLogc(lvl, closure)
}

//Used for internal call
func (log Logger) intLogc(lvl LogLevel, closure func() string) {
	skip := true
	for _, filter := range log {
		if lvl >= filter.Level {
			skip = false
		}
	}
	if skip {
		return
	}
	// Determine caller function
	pc, _, lineno, ok := runtime.Caller(2)
	src := ""
	if ok {
		src = fmt.Sprintf("%s:%d", runtime.FuncForPC(pc).Name(), lineno)
	}

	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  src,
		Message: closure(),
	}

	for _, filter := range log {
		if lvl >= filter.Level { //Write only when the lvl is equal (or) greater than the Filter log level
			filter.Write(rec)
		}
	}
}

//Log logs the message at the given log level lvl by
//taking the user provided src as the source of the log message.
func (log Logger) Log(lvl LogLevel, src, message string) {

	rec := &LogRecord{
		Level:   lvl,
		Created: time.Now(),
		Source:  src,
		Message: message,
	}

	for _, filter := range log {
		if lvl >= filter.Level { //Write only when the lvl is equal (or) greater than the Filter log level
			filter.Write(rec)
		}
	}
}

//Finest logs a message at the finest log level. This is the basic logging level.
//See Debug for explanation of arguments.
func (log Logger) Finest(arg0 interface{}, args ...interface{}) {

	switch firstArg := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(FINEST, firstArg, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(FINEST, firstArg)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(FINEST, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Trace logs a message at TRACE log level.
//See Debug for explanation of arguments.
func (log Logger) Trace(arg0 interface{}, args ...interface{}) {

	switch firstArg := arg0.(type) {
	case string: //With format string
		log.intLogf(TRACE, firstArg, args...)
	case func() string: //For function closure
		log.intLogc(TRACE, firstArg)
	default:
		log.intLogf(TRACE, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Debug logs a message at DEBUG log level.
// The behavior of Debug depends on the first argument:
// - arg0 is a string
//   When given a string as the first argument, this behaves like Logf but with
//   the DEBUG log level: the first argument is interpreted as a format for the
//   latter arguments.
// - arg0 is a func()string
//   When given a closure of type func()string, this logs the string returned by
//   the closure iff it will be logged.  The closure runs at most one time.
// - arg0 is interface{}
//   When given anything else, the log message will be each of the arguments
//   formatted with %v and separated by spaces.
func (log Logger) Debug(arg0 interface{}, args ...interface{}) {

	switch firstArg := arg0.(type) {
	case string:
		// Use the string as a format string
		log.intLogf(DEBUG, firstArg, args...)
	case func() string:
		// Log the closure (no other arguments used)
		log.intLogc(DEBUG, firstArg)
	default:
		// Build a format string so that it will be similar to Sprint
		log.intLogf(DEBUG, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Info logs message at the INFO log level.
//See Debug for explanation of arguments.
func (log Logger) Info(arg0 interface{}, args ...interface{}) {

	switch firstArg := arg0.(type) {
	case string:
		log.intLogf(INFO, firstArg, args...)
	case func() string:
		log.intLogc(INFO, firstArg)
	default:
		log.intLogf(INFO, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Warn logs message at the WARN log level and returns an error message after formatting.
//See Debug for explanation of arguments.
func (log Logger) Warn(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(WARN, msg)

	return errors.New(msg)
}

//Error logs message at the ERROR log level and returns an error message after formatting.
func (log Logger) Error(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(ERROR, msg)

	return errors.New(msg)
}

//Fatal logs message at the FATAL log level and returns an error message after formatting.
func (log Logger) Fatal(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	log.intLogf(FATAL, msg)

	return errors.New(msg)
}
