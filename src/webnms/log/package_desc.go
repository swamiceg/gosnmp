/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package log provides level-based and highly configurable logging.
//Utility functions are provided to perform logging in the API, with options for forwarding to different destinations such as File, Console, Socket.
//
//Example code snippets:
//
//  lg := log.NewLogger()
//  lg.AddFilter("stdout", log.DEBUG, log.NewConsoleLogWriter())
//  lg.AddFilter("log",    log.FINE,  log.NewFileLogWriter("example.log", true))
//  lg.Info("The time is now: %s", time.LocalTime().Format("15:04:05 MST 2006/01/02"))
//
//  The first two lines can be combined with the utility NewDefaultLogger:
//
//  lg := log.NewDefaultLogger(log.DEBUG)
//  lg.AddFilter("log",    log.FINE,  log.NewFileLogWriter("example.log", true))
//  lg.Info("The time is now: %s", time.LocalTime().Format("15:04:05 MST 2006/01/02"))
//
//To enable logging in the API:
//
//  Following example code snippet explains how to forward the log messages to a file at 'Debug' level:
//
//  //Create a new file log writer to log messages to example.log file with rotation enabled.
//  fileLogWriter := log.NewFileLogWriter("example.log", true)
//
//  //Format the log messages based on the format code string provided. Refer FileLogWriter for more details.
//  fileLogWriter.SetFormat(“[%D %T] [%L] (%S) %M”)
//
//  //Add the file log writer with debug level filter
//  log.AddFilter(“FileLog”, log.DEBUG, fileLogWriter)
//
//All the packages use this log package for performing logging at various levels.
package log
