/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package log

import (
	"fmt"
	"io"
	"os"
)

//Implementation of LogWriter interface to forward log messages to Console
type ConsoleLogWriter struct {
	msgChannel chan *LogRecord
	out        io.Writer
	format     string
	writeFlag  bool
}

//NewConsoleLogWriter returns pointer to ConsoleLogWriter which can be used for forwarding log messages to console ('stdout').
//
//Refer 'AddFilter()' method on Logger to add this LogWriter to the Logger.
func NewConsoleLogWriter() *ConsoleLogWriter {
	writer := &ConsoleLogWriter{
		msgChannel: make(chan *LogRecord),
		out:        os.Stdout,
		format:     "[%D %T] [%L] (%S) %M", //Default format string
		writeFlag:  true,
	}
	go writer.run()
	return writer
}

//SetFormat sets the Formatting string on the ConsoleLogWriter, which is used for formatting the log messages.
//
//Refer 'FormatLogMessage()' function for further details on the format codes.
func (c *ConsoleLogWriter) SetFormat(format string) {
	if format != "" {
		c.format = format
	}
}

//Write writes the log message to the console.
func (c *ConsoleLogWriter) Write(rec *LogRecord) {
	if c.writeFlag { //Do not write on the closed channels
		c.msgChannel <- rec
	}
}

//Close stops the Logger from sending log messages to the console on this ConsoleLogWriter.
//Writing log messages after closing can rise unexpected behaviour.
func (c *ConsoleLogWriter) Close() {
	c.writeFlag = false //To make sure that no write happens on closed channel
	close(c.msgChannel)
}

func (c *ConsoleLogWriter) run() {
	for v := range c.msgChannel {
		fmt.Fprint(c.out, FormatLogMessage(c.format, *v))
	}
}
