/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package log

import (
	"errors"
	"fmt"
	"strings"
)

//Wrapper variable for accessing log package. It contains instance of Logger type.
//User need not instantiate the 'Logger'. Refer Logger type.
//
//They should add the Filters such as File/Console at different log levels/name combination using the utility functions provided.
//Use AddFilter() method to add new Filters to the Logger.
//Default implementation is provided for Console/File/Socket LogWriters.
//
//User can also provide their own implementation of LogWriter interface.
//
//To enable logging in SNMP API:
//
//  log.AddFilter("fileLog", log.DEBUG, log.NewFileLogWriter("snmp_api.log", true))
//  log.AddFilter("consoleLog", log.FINE, log.NewConsoleLogWriter())
var LogWrapper *LogManager

//To Hold logger and other variables.
//For internal use only.
type LogManager struct {
	Logger
	perfFlag bool
	//DebugClients can be stored here
}

//EnablePerformanceLogging enables/disables the performance logging in SNMP API.
//If enabled, performance will be logged in different modules for performing SNMP operations.
func EnablePerformanceLogging(perf bool) {
	LogWrapper.perfFlag = perf
}

//IsPerf returns bools indicating whether performance logging is enabled in SNMP API.
func IsPerf() bool {
	return LogWrapper.perfFlag
}

//Let's perform static initialization for this package.
func init() {
	mgr := new(LogManager)
	mgr.Logger = NewLogger()

	LogWrapper = mgr
}

//AddFilter adds a new LogWriter to the Logger, which only logs messages at the level lvl or higher.
//
//'filterName' is used to uniquely identify the different LogWriters/Level combination under this Logger.
func AddFilter(filterName string, lvl LogLevel, writer LogWriter) Logger {
	LogWrapper.Logger[filterName] = &Filter{lvl, writer}
	return LogWrapper.Logger
}

//Return a slice of Log Filters available.
func LogFilters() []Filter {
	var filterSlice []Filter
	if len(LogWrapper.Logger) > 0 {
		for _, filter := range LogWrapper.Logger {
			filterSlice = append(filterSlice, *filter)
		}
	}
	return filterSlice
}

//Close closes all the LogWriters under this Logger.
func Close() {
	LogWrapper.Logger.Close()
}

//Logf logs the message at the given log level to all the LogWriters configured under this Logger.
//It formats the log message using format string provided and uses the caller as its Source.
func Logf(lvl LogLevel, format string, args ...interface{}) {
	LogWrapper.Logger.intLogf(lvl, format, args...)
}

//Logc logs ths string returned by the closure at the given log level lvl.
//Caller is used as a source of logging.
func Logc(lvl LogLevel, closure func() string) {
	LogWrapper.Logger.intLogc(lvl, closure)
}

//Log logs the message at the given log level lvl by
//taking the user provided src as the source of the log message.
func Log(lvl LogLevel, src, message string) {
	LogWrapper.Logger.Log(lvl, src, message)
}

//Finest logs a message at the finest log level. This is the basic logging level.
//See Debug for explanation of arguments.
func Finest(arg0 interface{}, args ...interface{}) {
	switch firstArg := arg0.(type) {
	case string:
		// Use the string as a format string
		LogWrapper.Logger.intLogf(FINEST, firstArg, args...)
	case func() string:
		// Log the closure (no other arguments used)
		LogWrapper.Logger.intLogc(FINEST, firstArg)
	default:
		// Build a format string so that it will be similar to Sprint
		LogWrapper.Logger.intLogf(FINEST, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Trace logs a message at TRACE log level.
//See Debug for explanation of arguments.
func Trace(arg0 interface{}, args ...interface{}) {
	switch firstArg := arg0.(type) {
	case string: //With format string
		LogWrapper.Logger.intLogf(TRACE, firstArg, args...)
	case func() string: //For function closure
		LogWrapper.Logger.intLogc(TRACE, firstArg)
	default:
		LogWrapper.Logger.intLogf(TRACE, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

// Debug logs a message at DEBUG log level.
// The behavior of Debug depends on the first argument:
// - arg0 is a string
//   When given a string as the first argument, this behaves like Logf but with
//   the DEBUG log level: the first argument is interpreted as a format for the
//   latter arguments.
// - arg0 is a func()string
//   When given a closure of type func()string, this logs the string returned by
//   the closure iff it will be logged.  The closure runs at most one time.
// - arg0 is interface{}
//   When given anything else, the log message will be each of the arguments
//   formatted with %v and separated by spaces.
func Debug(arg0 interface{}, args ...interface{}) {
	switch firstArg := arg0.(type) {
	case string:
		// Use the string as a format string
		LogWrapper.Logger.intLogf(DEBUG, firstArg, args...)
	case func() string:
		// Log the closure (no other arguments used)
		LogWrapper.Logger.intLogc(DEBUG, firstArg)
	default:
		// Build a format string so that it will be similar to Sprint
		LogWrapper.Logger.intLogf(DEBUG, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Info logs message at the INFO log level.
//See Debug for explanation of arguments.
func Info(arg0 interface{}, args ...interface{}) {
	switch firstArg := arg0.(type) {
	case string:
		LogWrapper.Logger.intLogf(INFO, firstArg, args...)
	case func() string:
		LogWrapper.Logger.intLogc(INFO, firstArg)
	default:
		LogWrapper.Logger.intLogf(INFO, fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
}

//Warn logs message at the WARN log level and returns an error message after formatting.
//See Debug for explanation of arguments.
func Warn(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	LogWrapper.Logger.intLogf(WARN, msg)

	return errors.New(msg)
}

//Error logs message at the ERROR log level and returns an error message after formatting.
func Error(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	LogWrapper.Logger.intLogf(ERROR, msg)

	return errors.New(msg)
}

//Fatal logs message at the FATAL log level and returns an error message after formatting.
func Fatal(arg0 interface{}, args ...interface{}) error {
	msg := ""
	switch firstArg := arg0.(type) {
	case string:
		msg = fmt.Sprintf(firstArg, args...)
	case func() string:
		msg = firstArg()
	default:
		msg = fmt.Sprintf(fmt.Sprintf("%v", arg0)+strings.Repeat(" %v", len(args)), args...)
	}
	LogWrapper.Logger.intLogf(FATAL, msg)

	return errors.New(msg)
}
