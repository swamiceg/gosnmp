/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package log

import (
	"bytes"
	"fmt"
	"os"
	"time"
)

//Implementation of LogWriter interface to forward log messages to File
type FileLogWriter struct {
	msgChannel     chan *LogRecord
	fileName       string
	file           *os.File
	header         string
	footer         string
	format         string
	writeFlag      bool
	rotate         bool
	curr_lines     uint64
	curr_size      uint64
	max_curr_lines uint64
	max_curr_size  uint64
}

//NewFileLogWriter returns pointer to FileLogWriter, which can be used for forwarding log messages to the 'filename' provided.
//'rot' indicates whether file rotation should be enabled or not.
//
//Refer 'AddFilter()' method on Logger to add this LogWriter to the Logger.
func NewFileLogWriter(filename string, rot bool) *FileLogWriter {
	fileWriter := &FileLogWriter{
		fileName:   filename,
		msgChannel: make(chan *LogRecord),
		rotate:     rot,
		header:     "########## Started Logging - " + time.Now().String() + "#############",
		footer:     "########## End of Logging - " + time.Now().String() + "############",
		format:     "[%D %T] [%L] (%S) %M",
		writeFlag:  true,
	}

	if err := fileWriter.rotateFile(); err != nil {
		fmt.Fprint(os.Stderr, fmt.Sprintf("Error in opening file (%s): %s\n", fileWriter.fileName, err))
		return nil
	}

	go func() {

		defer func() {
			if fileWriter.file != nil {
				//Print the end of logging
				fmt.Fprint(fileWriter.file, FormatLogMessage(fileWriter.footer, LogRecord{Created: time.Now()}))
				fileWriter.file.Close()
			}
		}()

		for v := range fileWriter.msgChannel {
			if (fileWriter.max_curr_lines != 0 && fileWriter.curr_lines >= fileWriter.max_curr_lines) || (fileWriter.max_curr_size != 0 && fileWriter.curr_size >= fileWriter.max_curr_size) {
				if err := fileWriter.rotateFile(); err != nil {
					fmt.Fprint(os.Stderr, fmt.Sprintf("Error in opening file (%s): %s\n", fileWriter.fileName, err))
					return
				}
			}
			if n, err := fmt.Fprint(fileWriter.file, FormatLogMessage(fileWriter.format, *v)); err != nil {
				fmt.Fprint(os.Stderr, fmt.Sprintf("Error in writing to file (%s): %s\n", fileWriter.fileName, err))
			} else {
				fileWriter.curr_lines++
				fileWriter.curr_size += uint64(n)
			}
		}
	}()

	return fileWriter
}

//SetFormat sets the Formatting string on the FileLogWriter, which is used for formatting the log messages.
//
//Refer 'FormatLogMessage()' function for further details on the format codes.
func (f *FileLogWriter) SetFormat(format string) {
	if format != "" {
		f.format = format
	}
}

//SetMaxLines sets the maximum number of lines on this FileLogWriter above which new file will be created/rotated.
func (f *FileLogWriter) SetMaxLines(maxLines uint64) {
	f.max_curr_lines = maxLines
}

//SetMaxSize sets the maximum size on the FileLogWriter above which new fill will be created/rotated.
func (f *FileLogWriter) SetMaxSize(maxSize uint64) {
	f.max_curr_size = maxSize
}

//Write writes the log message to a file.
func (f *FileLogWriter) Write(rec *LogRecord) {
	if f.writeFlag {
		f.msgChannel <- rec
	}
}

//Close stops the Logger from forwrding the log messages to the file configured on this FileLogWriter.
func (f *FileLogWriter) Close() {
	f.writeFlag = false //To make sure that no write happens on closed channel
	close(f.msgChannel)
}

func (f *FileLogWriter) rotateFile() error {
	if f.file != nil {
		fmt.Fprint(f.file, FormatLogMessage(f.footer, LogRecord{Created: time.Now()}))
		f.file.Close() // Close any previously opened file
	}

	// If we are keeping log files, move it to the next available number
	if f.rotate {
		_, err := os.Lstat(f.fileName)
		if err == nil { // file exists
			// Find the next available number
			num := 1
			fname := ""
			for ; err == nil && num <= 999; num++ {
				fname = f.fileName + fmt.Sprintf(".%03d", num)
				_, err = os.Lstat(fname)
			}
			// return error if the last file checked still existed
			if err == nil {
				return fmt.Errorf("Rotate: Cannot find free log number to rename %s\n", f.fileName)
			}

			// Rename the file to its newfound home
			err = os.Rename(f.fileName, fname)
			if err != nil {
				return fmt.Errorf("Rotate: %s\n", err)
			}
		}
	}

	// Open the log file
	fd, err := os.OpenFile(f.fileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		return err
	}
	f.file = fd

	now := time.Now()
	fmt.Fprint(f.file, FormatLogMessage(f.header, LogRecord{Created: now}))

	// initialize rotation values
	f.curr_lines = 0
	f.curr_size = 0

	return nil
}

//FormatLogMessage formats the LogRecord rec based on the format string provided and returns a string.
//
//  Known format codes:
//  %T - Time (15:04:05 IST)
//  %t - Time (15:04)
//  %D - Date (2006/01/02)
//  %d - Date (01/02/06)
//  %L - Level (ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL)
//  %S - Source
//  %M - Message
//
//Ignores unknown format.
//
//Recommended Format: "[%D %T] [%L] (%S) %M"
func FormatLogMessage(format string, rec LogRecord) string {

	pieces := bytes.Split([]byte(format), []byte{'%'})

	zone, _ := rec.Created.Zone()
	shortTime := fmt.Sprintf("%02d:%02d", rec.Created.Hour(), rec.Created.Minute())
	longTime := fmt.Sprintf("%02d:%02d:%02d %s", rec.Created.Hour(), rec.Created.Minute(), rec.Created.Second(), zone)
	shortDate := fmt.Sprintf("%02d/%02d/%02d", rec.Created.Day(), rec.Created.Month(), rec.Created.Year()%100)
	longDate := fmt.Sprintf("%02d/%02d/%02d", rec.Created.Day(), rec.Created.Month(), rec.Created.Year())

	var out bytes.Buffer
	// Iterate over the pieces, replacing known formats
	for i, piece := range pieces {
		if i > 0 && len(piece) > 0 {
			switch piece[0] {
			case 'T':
				out.WriteString(longTime)
			case 't':
				out.WriteString(shortTime)
			case 'D':
				out.WriteString(longDate)
			case 'd':
				out.WriteString(shortDate)
			case 'L':
				out.WriteString(levelStrings[int(rec.Level)])
			case 'S':
				out.WriteString(rec.Source)
			case 'M':
				out.WriteString(rec.Message)
			}
			if len(piece) > 1 {
				out.Write(piece[1:])
			}
		} else if len(piece) > 0 {
			out.Write(piece)
		}
	}
	out.WriteByte('\n')

	return out.String()
}
