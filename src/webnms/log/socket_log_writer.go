/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package log

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
)

//Implementation of LogWriter interface to forward log messages to UDP/TCP socket.
type SocketLogWriter chan *LogRecord

//NewSocketLogWriter returns pointer to SocketLogWriter which can be used for forwarding log messages to UDP/TCP socket.
//It tries to connect to the address using the network provided and returns nil if there is any error.
//
//Refer 'AddFilter()' method on Logger to add this LogWriter to the Logger.
func NewSocketLogWriter(network, address string) *SocketLogWriter {

	socketWriter := make(SocketLogWriter)
	conn, err := net.Dial(network, address)
	if err != nil {
		fmt.Fprint(os.Stderr, err.Error()+"\n")
		return nil
	}

	go func() {
		defer func() {
			if conn != nil {
				conn.Close()
			}
		}()
		for v := range socketWriter {
			js, err := json.Marshal(v)
			if err != nil {
				fmt.Fprint(os.Stderr, "Error in marshalling the Log message: ", err.Error(), "\n")
				return
			}
			if _, err = conn.Write(js); err != nil {
				fmt.Fprint(os.Stderr, "Error in writing to the socket: ", err.Error(), "\n")
			}
		}
	}()

	return &socketWriter
}

//Write writes the LogRecord to the UDP/TCP socket opened.
func (s *SocketLogWriter) Write(rec *LogRecord) {
	*s <- rec
}

//Close stops the Logger to forward the log message to the socket.
func (s *SocketLogWriter) Close() {
	close(*s)
}
