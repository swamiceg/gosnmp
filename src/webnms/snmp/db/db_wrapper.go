/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package db

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
)

//Database represents a single DB entity, which holds the DB connection instance
//and list of tables under the database.
//
//SQLDialect instance should be passed to perform DB specific SQL operations.
//
//For internal use only.
type Database struct {
	dbConn  *sql.DB //DB Connection
	dialect SQLDialect

	tables []*Table
}

//Creates new database instance based on db and dialect passed.
func NewDatabase(db *sql.DB, dialect SQLDialect) *Database {
	return &Database{
		dbConn:  db,
		dialect: dialect,
	}
}

func (db *Database) AddTable(table interface{}) *Table {
	//Extract the Type of the table
	typ := reflect.TypeOf(table)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	//Table should be of kind Struct
	if typ.Kind() != reflect.Struct {
		return nil //Cannot create table
	}

	for _, table := range db.tables {
		//If table already exist in db, just update the name and return it
		if table.TableType == typ {
			table.TableName = typ.Name()
			return table
		}
	}

	//Extract the columns (struct fields) from the table interface
	cols := constructColumns(typ)
	if cols == nil { //Can be an empty Struct
		return nil
	}

	//Add the new table to db
	newTable := &Table{
		TableName: typ.Name(),
		TableType: typ,
		Columns:   cols,
		DB:        db,
	}
	db.tables = append(db.tables, newTable)
	//Set the table instance on each column
	for _, col := range newTable.Columns {
		col.Table = newTable
	}

	return newTable
}

//Construct column slice from the struct go type
func constructColumns(typ reflect.Type) (cols []*Column) {

	//Additional Type checks
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	if typ.Kind() != reflect.Struct {
		return
	}

	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)

		//If it is anonymous struct, then construct columns recursively
		if field.Anonymous && field.Type.Kind() == reflect.Struct {
			subCols := constructColumns(field.Type)
			for _, subCol := range subCols {
				appendFlag := true
				for _, col := range cols {
					if col.ColumnName == subCol.ColumnName {
						appendFlag = false
					}
				}
				//Append only the distinctive columns
				if appendFlag {
					cols = append(cols, subCol)
				}
			}
		} else {
			appendFlag := true
			for _, col := range cols {
				if col.ColumnName == field.Name {
					appendFlag = false //There is already a column
				}
			}
			//Append only the distinctive column
			if appendFlag {
				col := &Column{
					ColumnName: field.Name,
					ColumnType: field.Type,
				}
				cols = append(cols, col)
			}
		}
	}

	return
}

//CreateTables create the tables in database by creating the query for all the
//tables added in db.
//Return error if any.
func (db *Database) CreateTables() error {
	if len(db.tables) == 0 {
		return errors.New("No tables exist in db. Call AddTable() on db to add one.")
	}

	var buf bytes.Buffer
	//Loop over all the tables available in db
	for _, table := range db.tables {

		//Create schema for the table
		if strings.TrimSpace(table.SchemaName) != "" {
			buf.WriteString(db.dialect.IfSchemaNotExist("CREATE SCHEMA", table.SchemaName))
			buf.WriteString(" " + table.SchemaName + ";")
		}

		tabCreate := db.dialect.IfTableNotExist("CREATE TABLE", table.SchemaName, table.TableName)
		buf.WriteString(tabCreate)
		buf.WriteString(" ")
		buf.WriteString(db.dialect.TableName(table.SchemaName, table.TableName))
		buf.WriteString(" ( ")

		for i, col := range table.Columns {
			if i > 0 {
				buf.WriteString(", ")
			}
			buf.WriteString(db.dialect.QuoteIdentifier(col.ColumnName)) //Column is quoted for safety, when using Keywords
			buf.WriteString(" ")
			buf.WriteString(db.dialect.ToSQLType(col.ColumnType, col.MaxSize))
			if col.IsPK && len(table.Keys) == 1 { //For single PrimaryKey
				buf.WriteString(" PRIMARY KEY")
			}
		}

		//For multiple Primary Keys
		if len(table.Keys) > 1 {
			buf.WriteString(", PRIMARY KEY ( ")
			for i, key := range table.Keys {
				if i > 0 {
					buf.WriteString(", ")
				}
				buf.WriteString(key.ColumnName)
			}
		}

		buf.WriteString(" )")
		buf.WriteString(db.dialect.QuerySuffix())

		_, err := db.dbConn.Exec(buf.String())
		if err != nil {
			return fmt.Errorf("Error in creating table '%s': %s", table.TableName, err.Error())
		}

		buf.Reset()
	}

	return nil
}

//SelectOne performs select query on the table using the keys provided.
//At least one primary key is needed to perform this operation.
//
//keys should be in same order as primary keys in table.
func (db *Database) SelectOne(table interface{}, keys ...interface{}) (interface{}, error) {

	//We need at least one key to perform this operation
	if len(keys) == 0 {
		return nil, errors.New("Empty primary key passed. Cannot perform select operation.")
	}

	//Extract the Table type
	typ := reflect.TypeOf(table)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return nil, errors.New("Table should be of type struct.")
	}

	//Check whether the table exist in db
	var tableToQuery *Table
	for _, tab := range db.tables {
		if tab.TableType == typ {
			tableToQuery = tab
			break
		}
	}
	if tableToQuery == nil {
		return nil, fmt.Errorf("No table exist in db of type: %s. Call AddTable(table) on db to add this table.", typ.String())
	}

	//Construct the Value to bind the results
	res := make([]interface{}, len(tableToQuery.Columns))
	val := reflect.New(typ)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	colValueMap := valToColumns(val)

	//Construct the query
	var buf bytes.Buffer
	buf.WriteString("SELECT ")
	for i, col := range tableToQuery.Columns { //We are selecting only the column available in table to avoid conflicts
		if i > 0 {
			buf.WriteString(", ")
		}

		buf.WriteString(db.dialect.QuoteIdentifier(col.ColumnName))

		f := colValueMap[col.ColumnName]
		res[i] = f.Addr().Interface()

	}
	buf.WriteString(" FROM ")
	buf.WriteString(db.dialect.TableName(tableToQuery.SchemaName, tableToQuery.TableName))

	for i, key := range tableToQuery.Keys {
		if i == 0 {
			buf.WriteString(" WHERE ")
		} else {
			buf.WriteString(" AND ")
		}
		buf.WriteString(db.dialect.QuoteIdentifier(key.ColumnName))
		buf.WriteString("=")
		buf.WriteString(db.dialect.BindVar(i)) //Can be ?, $1 etc.,
	}

	row := db.dbConn.QueryRow(buf.String(), keys...)

	err := row.Scan(res...)
	if err != nil {
		return nil, err //Maybe no row exist in the table
	}

	//Construct the table struct from the scanned res.
	//	tableInterface := tableToQuery.bindSliceToStruct(typ, res ...)
	//	if tableInterface == nil {
	//		return nil, fmt.Errorf("Failure in constructing the table (%s) struct from the values scanned from DB.", typ.String())
	//	}

	return val.Interface(), nil
}

//Bind the values scanned to the Type and return the interface value
func (t *Table) bindSliceToStruct(typ reflect.Type, vals ...interface{}) interface{} {
	if len(t.Columns) != len(vals) {
		return nil
	}
	if typ.Kind() != reflect.Struct { //Cannot bind to non-struct type
		return nil
	}

	//Create new Value of Type typ.
	structVal := reflect.New(typ)
	colValueMap := valToColumns(structVal) //Get the Columns-Value Map for easy binding of values to struct

	var col *Column
	for i, val := range vals { //vals should be equal to t.Columns in length, type etc.,
		col = t.Columns[i]
		if v, ok := colValueMap[col.ColumnName]; ok {
			if v.Kind() == reflect.TypeOf(val).Kind() { //Set the value on struc only their Kinds are similar.
				v.Set(reflect.ValueOf(val))
			}
		}

		/*f := structVal.Elem().FieldByName(col.ColumnName)
		if f.Kind() == reflect.TypeOf(val).Kind() {
			f.Set(reflect.ValueOf(val))
		}*/
		//fmt.Println("Addr: val:", f.Interface(), addr, val)
	}

	return structVal.Interface()
}

//SelectAll fetches all the rows from the db based on the table Type.
//Returns an array of entries.
func (db *Database) SelectAll(table interface{}) ([]interface{}, error) {

	//Extract the Table type
	typ := reflect.TypeOf(table)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return nil, errors.New("Table should be of type struct.")
	}

	//Check whether the table exist in db
	var tableToQuery *Table
	for _, tab := range db.tables {
		if tab.TableType == typ {
			tableToQuery = tab
			break
		}
	}
	if tableToQuery == nil {
		return nil, fmt.Errorf("No table exist in db of type: %s. Call AddTable(table) on db to add this table.", typ.String())
	}

	//Slice to bind all the rows fetched
	var valSlice []interface{}

	//Construct the query
	var buf bytes.Buffer
	buf.WriteString("SELECT ")
	for i, col := range tableToQuery.Columns { //We are selecting only the column available in table to avoid conflicts
		if i > 0 {
			buf.WriteString(", ")
		}

		buf.WriteString(db.dialect.QuoteIdentifier(col.ColumnName))
	}
	buf.WriteString(" FROM ")
	buf.WriteString(db.dialect.TableName(tableToQuery.SchemaName, tableToQuery.TableName))

	rows, err := db.dbConn.Query(buf.String())
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		val         reflect.Value
		res         []interface{}
		colValueMap map[string]reflect.Value
	)
	for rows.Next() {
		res = make([]interface{}, len(tableToQuery.Columns))
		val = reflect.New(typ)
		if val.Kind() == reflect.Ptr {
			val = val.Elem()
		}
		colValueMap = valToColumns(val)
		for i, col := range tableToQuery.Columns {
			f := colValueMap[col.ColumnName]
			res[i] = f.Addr().Interface()
		}

		err = rows.Scan(res...)
		if err != nil {
			return valSlice, err
		}

		valSlice = append(valSlice, val.Interface())
	}

	return valSlice, nil
}

//SelectQuery performs select query on the table using the column names and arguments provided.
//Return a list of rows from DB based on the args provided.
func (db *Database) SelectQuery(table interface{}, cols []string, args ...interface{}) ([]interface{}, error) {

	if len(cols) != len(args) {
		return nil, errors.New("Columns and arguments count mismatch. Cannot perform select operation.")
	}

	//Extract the Table type
	typ := reflect.TypeOf(table)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return nil, errors.New("Table should be of type struct.")
	}

	//Check whether the table exist in db
	var tableToQuery *Table
	for _, tab := range db.tables {
		if tab.TableType == typ {
			tableToQuery = tab
			break
		}
	}
	if tableToQuery == nil {
		return nil, fmt.Errorf("No table exist in db of type: %s. Call AddTable(table) on db to add this table.", typ.String())
	}

	//Slice to bind all the rows fetched
	var valSlice []interface{}

	//Construct the query
	var buf bytes.Buffer
	buf.WriteString("SELECT ")
	for i, col := range tableToQuery.Columns { //We are selecting only the column available in table to avoid conflicts
		if i > 0 {
			buf.WriteString(", ")
		}

		buf.WriteString(db.dialect.QuoteIdentifier(col.ColumnName))
	}
	buf.WriteString(" FROM ")
	buf.WriteString(db.dialect.TableName(tableToQuery.SchemaName, tableToQuery.TableName))

	var argsToDB []interface{}
	for i, colName := range cols {
		for _, col := range tableToQuery.Columns {
			if colName == col.ColumnName {
				argsToDB = append(argsToDB, args[i])
				if i == 0 {
					buf.WriteString(" WHERE ")
				} else {
					buf.WriteString(" AND ")
				}
				buf.WriteString(db.dialect.QuoteIdentifier(col.ColumnName))
				buf.WriteString("=")
				buf.WriteString(db.dialect.BindVar(i)) //Can be ?, $1 etc.,
				break
			}
		}
	}

	rows, err := db.dbConn.Query(buf.String(), argsToDB...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		val         reflect.Value
		res         []interface{}
		colValueMap map[string]reflect.Value
	)
	for rows.Next() {
		res = make([]interface{}, len(tableToQuery.Columns))
		val = reflect.New(typ)
		if val.Kind() == reflect.Ptr {
			val = val.Elem()
		}
		colValueMap = valToColumns(val)
		for i, col := range tableToQuery.Columns {
			f := colValueMap[col.ColumnName]
			res[i] = f.Addr().Interface()
		}

		err = rows.Scan(res...)
		if err != nil {
			return valSlice, err
		}

		valSlice = append(valSlice, val.Interface())
	}

	if len(valSlice) == 0 {
		//Empty row returned from DB
		return nil, fmt.Errorf("No rows returned from DB matching the select query for table: %s.", tableToQuery.TableName)
	}

	return valSlice, nil
}

//Insert inserts the row in to the table based on the row Type.
//Used for inserting a single row in table.
func (db *Database) Insert(row interface{}) error {
	typ := reflect.TypeOf(row)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	//Row should be of type Struct
	if typ.Kind() != reflect.Struct {
		return fmt.Errorf("Unsupported row Type: %s. Should be of type Struct.", typ.Kind().String())
	}

	//Get the Value of row
	val := reflect.ValueOf(row)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	colValueMap := valToColumns(val) //Costruct Column-Value map for easy access
	if colValueMap == nil {
		return errors.New("Failure in creating the values from the row passed.")
	}

	var tableToInsert *Table
	for _, table := range db.tables {
		if table.TableType == typ {
			tableToInsert = table
			break
		}
	}
	if tableToInsert == nil {
		return fmt.Errorf("No table exist in db of type: %s.", typ.String())
	}

	//Check for empty string for Primary Keys
	for _, key := range tableToInsert.Keys {
		if val, ok := colValueMap[key.ColumnName]; ok {
			if val.Type().Kind() == reflect.String && val.String() == "" {
				return fmt.Errorf("Empty string passed for Primary Key '%s'.", key.ColumnName)
			}
		}
	}

	//Construct the 'Insert' Query
	var buf bytes.Buffer
	buf.WriteString("INSERT INTO ")
	buf.WriteString(db.dialect.TableName(tableToInsert.SchemaName, tableToInsert.TableName))

	var columns bytes.Buffer
	var bindvars bytes.Buffer
	var args []interface{}
	for i, col := range tableToInsert.Columns {
		if i > 0 {
			columns.WriteString(", ")
			bindvars.WriteString(", ")
		}
		columns.WriteString(db.dialect.QuoteIdentifier(col.ColumnName))
		bindvars.WriteString(db.dialect.BindVar(i))

		if value, ok := colValueMap[col.ColumnName]; !ok {
			//return column missing to perform insert - This should not happen
		} else {
			//fmt.Println("Arg: ", col.ColumnName, value.Interface())
			args = append(args, value.Interface())
		}
	}

	buf.WriteString(" (")
	buf.WriteString(columns.String())
	buf.WriteString(") ")

	buf.WriteString("VALUES")
	buf.WriteString(" (")
	buf.WriteString(bindvars.String())
	buf.WriteString(") ")
	buf.WriteString(db.dialect.QuerySuffix())

	_, err := db.dbConn.Exec(buf.String(), args...)

	return err
}

//Delete deletes the row in the table based on the row Type.
//row should contain the value(s) of primary key(s) for the table.
func (db *Database) Delete(row interface{}) (int64, error) {
	//Get the row Type
	typ := reflect.TypeOf(row)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return -1, fmt.Errorf("Unsupported row Type: %s. Should be of type Struct.", typ.Kind().String())
	}

	//Get the Value of row
	val := reflect.ValueOf(row)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	colValueMap := valToColumns(val) //Costruct Column-Value map for easy access
	if colValueMap == nil {
		return -1, errors.New("Failure in creating the values from the row passed.")
	}

	var tableToDelete *Table
	for _, table := range db.tables {
		if table.TableType == typ {
			tableToDelete = table
			break
		}
	}
	if tableToDelete == nil {
		return -1, fmt.Errorf("No table exist in db of type: %s.", typ.String())
	}

	var buf bytes.Buffer
	buf.WriteString("DELETE FROM ")
	buf.WriteString(db.dialect.TableName(tableToDelete.SchemaName, tableToDelete.TableName))
	buf.WriteString(" WHERE ")

	var args []interface{}
	for i, key := range tableToDelete.Keys {
		if i > 0 {
			buf.WriteString(" AND ")
		}
		buf.WriteString(db.dialect.QuoteIdentifier(key.ColumnName))
		buf.WriteString("=")
		buf.WriteString(db.dialect.BindVar(i))

		if value, ok := colValueMap[key.ColumnName]; !ok {
			//return column missing to perform insert - This should not happen
		} else {
			//fmt.Println("Arg: ", col.ColumnName, value.Interface())
			args = append(args, value.Interface())
		}
	}

	result, err := db.dbConn.Exec(buf.String(), args...)
	if err != nil {
		return -1, err //No rows deleted
	}
	//Get the number of rows affected
	n, _ := result.RowsAffected()

	return n, nil
}

//DeleteAll deletes all the rows in the table based on the row Type.
//Equivalent to truncate.
func (db *Database) DeleteAll(row interface{}) (int64, error) {
	//Get the row Type
	typ := reflect.TypeOf(row)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return -1, fmt.Errorf("Unsupported row Type: %s. Should be of type Struct.", typ.Kind().String())
	}

	var tableToDelete *Table
	for _, table := range db.tables {
		if table.TableType == typ {
			tableToDelete = table
			break
		}
	}
	if tableToDelete == nil {
		return -1, fmt.Errorf("No table exist in db of type: %s.", typ.String())
	}

	var buf bytes.Buffer
	buf.WriteString("DELETE FROM ")
	buf.WriteString(db.dialect.TableName(tableToDelete.SchemaName, tableToDelete.TableName))

	result, err := db.dbConn.Exec(buf.String())
	if err != nil {
		return -1, err //No rows deleted
	}
	//Get the number of rows affected
	n, _ := result.RowsAffected()

	return n, nil
}

//Update updates the table based on the row Type passed.
//Row will be updated based on the values of primary key(s) of the row passed.
func (db *Database) Update(row interface{}) (int64, error) {
	//Get the row Type
	typ := reflect.TypeOf(row)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	if typ.Kind() != reflect.Struct {
		return -1, fmt.Errorf("Unsupported type row: %s. Should be of type struct.", typ.Kind().String())
	}

	//Get the Value of row
	val := reflect.ValueOf(row)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	colValueMap := valToColumns(val) //Costruct Column-Value map for easy access
	if colValueMap == nil {
		return -1, errors.New("Failure in creating the values from the row passed.")
	}

	var tableToUpdate *Table
	for _, table := range db.tables {
		if table.TableType == typ {
			tableToUpdate = table
			break
		}
	}
	if tableToUpdate == nil {
		return -1, fmt.Errorf("No table exist in db of type: %s.", typ.String())
	}

	//Construct the 'Update' query
	var buf bytes.Buffer
	buf.WriteString("UPDATE ")
	buf.WriteString(db.dialect.TableName(tableToUpdate.SchemaName, tableToUpdate.TableName))
	buf.WriteString(" SET ")

	var args []interface{}
	colLen := len(tableToUpdate.Columns)
	argCount := 0
	for i, col := range tableToUpdate.Columns {
		if !col.IsPK { //Update only non-primary key columns
			buf.WriteString(col.ColumnName)
			buf.WriteString("=")
			buf.WriteString(db.dialect.BindVar(argCount))
			argCount++

			if i <= colLen-2 {
				buf.WriteString(", ")
			}

			//Append the update arguments value
			if value, ok := colValueMap[col.ColumnName]; !ok {
				//return column missing to perform insert - This should not happen
			} else {
				//fmt.Println("Arg: ", col.ColumnName, value.Interface())
				args = append(args, value.Interface())
			}
		}
	}

	buf.WriteString(" WHERE ")
	for i, key := range tableToUpdate.Keys {
		if i > 0 {
			buf.WriteString(" AND ")
		}
		buf.WriteString(db.dialect.QuoteIdentifier(key.ColumnName))
		buf.WriteString("=")
		buf.WriteString(db.dialect.BindVar(argCount))
		argCount++

		//Append the Keys value
		if value, ok := colValueMap[key.ColumnName]; !ok {
			//return column missing to perform insert - This should not happen
		} else {
			//fmt.Println("Arg: ", col.ColumnName, value.Interface())
			args = append(args, value.Interface())
		}
	}

	result, err := db.dbConn.Exec(buf.String(), args...)
	if err != nil {
		return -1, err
	}
	//Get the number of rows affected.
	n, _ := result.RowsAffected()

	return n, nil
}

//Construct Column-Value map based on the val passed.
//For internal use.
func valToColumns(val reflect.Value) (colValueMap map[string]reflect.Value) {

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if colValueMap == nil {
		colValueMap = make(map[string]reflect.Value)
	}
	var fieldType reflect.Kind
	for i := 0; i < val.NumField(); i++ {
		typField := val.Type().Field(i)
		valField := val.Field(i)

		if typField.Type.Kind() == reflect.Ptr {
			fieldType = typField.Type.Elem().Kind()
		} else {
			fieldType = typField.Type.Kind()
		}
		if fieldType == reflect.Struct {
			colMap := valToColumns(valField)
			//Do not override. Add the value in Map.
			for key, value := range colMap {
				if _, ok := colValueMap[key]; !ok {
					colValueMap[key] = value
				}
			}
		} else {
			//Do not override. Add the value in Map.
			if _, ok := colValueMap[typField.Name]; !ok {
				colValueMap[typField.Name] = valField
			}
		}
	}

	return
}

func (db *Database) Close() error {
	return db.dbConn.Close()
}

//Table represents the database table, which holds the slice of Columns.
//Primary Keys should be set on this table for all database operations.
type Table struct {
	TableName  string
	SchemaName string
	TableType  reflect.Type
	DB         *Database

	Columns []*Column
	Keys    []*Column
}

//SetKeys set the Primary Keys for the table t based on the keys passed.
//string is case-sensitive
func (t *Table) SetKeys(keys ...string) {
	if len(t.Columns) == 0 || len(keys) == 0 {
		return
	}

	for _, key := range keys {
		for _, col := range t.Columns {
			if col.ColumnName == key { //Case-sensitive
				t.Keys = append(t.Keys, col)
				col.IsPK = true // Column is Primary Key
				break
			}
		}
	}
}

//Set the SchemaName for the table t.
func (t *Table) SetSchemaName(schemaName string) {
	t.SchemaName = schemaName
}

//Column represents the  column fields in the Table.
//Table holds the slice of Column.
//
//For internal use.
type Column struct {
	ColumnName string //fieldname
	ColumnType reflect.Type
	MaxSize    int
	*Table
	IsPK bool
}
