/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package db

import (
	"fmt"
	"reflect"
	"strings"
)

//SQLDialect interface provides method specific to each Structured Query Languages,
//which helps in the construction of Queries at ease which differs across different database.
//
//Default implementation is provided for Databases such as MySQL, Postgres, Sqlite3
//
//User using database other than the one mentioned above should implement this interface and register
//the same with SnmpAPI's RegisterSQLDialect() method.
type SQLDialect interface {
	//Returns the quoted tablename with non-empty schema name which can be used for query construction.
	//For example: schemaName.tableName
	TableName(schemaName string, tableName string) string

	//ToSQLType returns the SQL specific datatype string based on the colType provided.
	//MaxSize can be used for constructing the datatypes with max value.
	//
	//For example: Go's 'int' type is equivalent to 'integer' in Postgres.
	ToSQLType(colType reflect.Type, maxSize int) string

	//QuerySuffix returns the string that should be appended to SQL queries.
	//For example: ";" for MySQL/Postgres
	QuerySuffix() string

	//bind variable string to use when forming SQL statements
	// in many Databases it is "?", but Postgres appears to use $1
	//
	//i is a zero based index of the bind variable in this statement
	BindVar(i int) string

	//IfTableNotExist returns the string specific to db, which helps in creating the table only if it doesn't exist in Database.
	//For example: "IF NOT EXISTS"  for MySQL database.
	IfTableNotExist(command, schema, table string) string

	//IfSchemaNotExist returns the string specific to db, which helps in creating the schema only if it doesn't exist in Database.
	//For example: "IF NOT EXISTS"  for MySQL database.
	IfSchemaNotExist(command, schema string) string

	//QuoteIdentifier encloses the identifier with special characters, which helps in using
	//reserved keywords as identifiers in SQL queries.
	//
	//For example: MySQL: Uses backtick for enclosing the identifier. table -> `table`
	QuoteIdentifier(identifier string) string
}

/*
	MySQL Dialect
*/

//Dialect implementation of MySQL Database.
type MySQLDialect struct{}

func (dia MySQLDialect) TableName(schemaName string, tableName string) string {
	if strings.EqualFold(schemaName, "") {
		return dia.QuoteIdentifier(tableName)
	}

	return dia.QuoteIdentifier(schemaName) + "." + dia.QuoteIdentifier(tableName)
}

func (dia MySQLDialect) ToSQLType(colType reflect.Type, maxSize int) string {
	switch colType.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(colType.Elem(), maxSize)
	case reflect.Bool:
		return "boolean"
	case reflect.Int8:
		return "tinyint"
	case reflect.Uint8:
		return "tinyint unsigned"
	case reflect.Int16:
		return "smallint"
	case reflect.Uint16:
		return "smallint unsigned"
	case reflect.Int, reflect.Int32:
		return "int"
	case reflect.Uint, reflect.Uint32:
		return "int unsigned"
	case reflect.Int64:
		return "bigint"
	case reflect.Uint64:
		return "bigint unsigned"
	case reflect.Float64, reflect.Float32:
		return "double"
	case reflect.Slice:
		if colType.Elem().Kind() == reflect.Uint8 {
			return "mediumblob"
		}
	}

	switch colType.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "double"
	case "NullBool":
		return "tinyint"
	case "Time":
		return "datetime"
	}

	if maxSize < 1 {
		maxSize = 255
	}
	return fmt.Sprintf("varchar(%d)", maxSize)

}

func (dia MySQLDialect) QuerySuffix() string {
	return ";"
}

func (dia MySQLDialect) BindVar(i int) string {
	return "?"
}

func (dia MySQLDialect) IfSchemaNotExist(command, schema string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia MySQLDialect) IfTableNotExist(command, schema, table string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia MySQLDialect) QuoteIdentifier(field string) string {
	return "`" + field + "`"
}

/*
	Postgres Dialect
*/

//Dialect implementation of Postgres Database.
type PostgresDialect struct{}

func (dia PostgresDialect) TableName(schemaName string, tableName string) string {
	if strings.EqualFold(schemaName, "") {
		return dia.QuoteIdentifier(tableName)
	}

	return dia.QuoteIdentifier(schemaName) + "." + dia.QuoteIdentifier(tableName)
}

func (dia PostgresDialect) ToSQLType(colType reflect.Type, maxSize int) string {
	switch colType.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(colType.Elem(), maxSize)
	case reflect.Bool:
		return "boolean"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "integer"
	case reflect.Int64, reflect.Uint64:
		return "bigint"
	case reflect.Float64:
		return "double precision"
	case reflect.Float32:
		return "real"
	case reflect.Slice:
		if colType.Elem().Kind() == reflect.Uint8 {
			return "bytea"
		}
	}

	switch colType.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "double precision"
	case "NullBool":
		return "boolean"
	case "Time":
		return "timestamp with time zone"
	}

	if maxSize > 0 {
		return fmt.Sprintf("varchar(%d)", maxSize)
	} else {
		return "text"
	}
}

func (dia PostgresDialect) QuerySuffix() string {
	return ";"
}

func (dia PostgresDialect) BindVar(i int) string {
	return fmt.Sprintf("$%d", i+1)
}

func (dia PostgresDialect) IfSchemaNotExist(command, schema string) string {
	return fmt.Sprintf("%s", command) //if not exists command throwing error in Postgres, Hence removed it.
}

func (dia PostgresDialect) IfTableNotExist(command, schema, table string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia PostgresDialect) QuoteIdentifier(field string) string {
	return `"` + strings.ToLower(field) + `"`
}

/*
	SQLite Dialect
*/

//Dialect implementation of SQLite Database.
type SqliteDialect struct{}

// sqlite does not have schemas like PostgreSQL does, so just escape it like normal
func (dia SqliteDialect) TableName(schema string, table string) string {
	return dia.QuoteIdentifier(table)
}

func (dia SqliteDialect) ToSQLType(val reflect.Type, maxSize int) string {
	switch val.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(val.Elem(), maxSize)
	case reflect.Bool:
		return "integer"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return "integer"
	case reflect.Float64, reflect.Float32:
		return "real"
	case reflect.Slice:
		if val.Elem().Kind() == reflect.Uint8 {
			return "blob"
		}
	}

	switch val.Name() {
	case "NullInt64":
		return "integer"
	case "NullFloat64":
		return "real"
	case "NullBool":
		return "integer"
	case "Time":
		return "datetime"
	}

	if maxSize < 1 {
		maxSize = 255
	}
	return fmt.Sprintf("varchar(%d)", maxSize)
}

func (dia SqliteDialect) QuerySuffix() string { return ";" }

// Returns "?"
func (dia SqliteDialect) BindVar(i int) string {
	return "?"
}

func (dia SqliteDialect) IfSchemaNotExist(command, schema string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia SqliteDialect) IfTableNotExist(command, schema, table string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia SqliteDialect) QuoteIdentifier(identifier string) string {
	return `"` + identifier + `"`
}

/*
	SQLServer Dialect
*/

//Dialect implementation of SQL Server Database.
type SqlServerDialect struct{}

func (dia SqlServerDialect) TableName(schema string, table string) string {
	if strings.TrimSpace(schema) == "" {
		return table
	}
	return schema + "." + table
}

func (dia SqlServerDialect) ToSQLType(val reflect.Type, maxSize int) string {
	switch val.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(val.Elem(), maxSize)
	case reflect.Bool:
		return "bit"
	case reflect.Int8:
		return "tinyint"
	case reflect.Uint8:
		return "smallint"
	case reflect.Int16:
		return "smallint"
	case reflect.Uint16:
		return "int"
	case reflect.Int, reflect.Int32:
		return "int"
	case reflect.Uint, reflect.Uint32:
		return "bigint"
	case reflect.Int64:
		return "bigint"
	case reflect.Uint64:
		return "bigint"
	case reflect.Float32:
		return "real"
	case reflect.Float64:
		return "float(53)"
	case reflect.Slice:
		if val.Elem().Kind() == reflect.Uint8 {
			return "varbinary"
		}
	}

	switch val.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "float(53)"
	case "NullBool":
		return "tinyint"
	case "Time":
		return "datetime"
	}

	if maxSize < 1 {
		maxSize = 255
	}
	return fmt.Sprintf("varchar(%d)", maxSize)
}

func (dia SqlServerDialect) QuerySuffix() string { return ";" }

// Returns "?"
func (dia SqlServerDialect) BindVar(i int) string {
	return "?"
}

func (dia SqlServerDialect) IfSchemaNotExist(command, schema string) string {
	s := fmt.Sprintf("if not exists (select name from sys.schemas where name = '%s') %s", schema, command)
	return s
}

func (dia SqlServerDialect) IfTableNotExist(command, schema, table string) string {
	var schema_clause string
	if strings.TrimSpace(schema) != "" {
		schema_clause = fmt.Sprintf("table_schema = '%s' and ", schema)
	}
	s := fmt.Sprintf("if not exists (select * from information_schema.tables where %stable_name = '%s') %s", schema_clause, table, command)
	return s
}

func (dia SqlServerDialect) QuoteIdentifier(identifier string) string {
	return `"` + identifier + `"`
}

/*
	Oracle Dialect
*/

//Dialect implementation of Oracle Database.
type OracleDialect struct{}

func (dia OracleDialect) TableName(schema string, table string) string {
	if strings.TrimSpace(schema) == "" {
		return dia.QuoteIdentifier(table)
	}

	return schema + "." + dia.QuoteIdentifier(table)
}

func (dia OracleDialect) ToSQLType(val reflect.Type, maxSize int) string {
	switch val.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(val.Elem(), maxSize)
	case reflect.Bool:
		return "boolean"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "integer"
	case reflect.Int64, reflect.Uint64:
		return "bigint"
	case reflect.Float64:
		return "double precision"
	case reflect.Float32:
		return "real"
	case reflect.Slice:
		if val.Elem().Kind() == reflect.Uint8 {
			return "bytea"
		}
	}

	switch val.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "double precision"
	case "NullBool":
		return "boolean"
	case "NullTime", "Time":
		return "timestamp with time zone"
	}

	if maxSize > 0 {
		return fmt.Sprintf("varchar(%d)", maxSize)
	} else {
		return "text"
	}

}

func (dia OracleDialect) QuerySuffix() string { return "" }

// Returns "$(i+1)"
func (dia OracleDialect) BindVar(i int) string {
	return fmt.Sprintf(":%d", i+1)
}

func (dia OracleDialect) IfSchemaNotExist(command, schema string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia OracleDialect) IfTableNotExist(command, schema, table string) string {
	return fmt.Sprintf("%s IF NOT EXISTS", command)
}

func (dia OracleDialect) QuoteIdentifier(identifier string) string {
	return `"` + strings.ToUpper(identifier) + `"`
}

/*
	DB2 Dialect
*/

//Dialect implementation of DB2 Database.
//
//BUG: DB2 and Sybase requires 'IF NOT EXISTS' clause for table creation/schema creation to be executed using 'exec' immediate command.
//Differs from other databases. So, IF NOT EXISTS case excluded for both these Dialects.
//Hence, user should take care of cleaning the database prior to the usage.
type DB2Dialect struct{}

func (dia DB2Dialect) TableName(schemaName string, tableName string) string {
	if strings.EqualFold(schemaName, "") {
		return dia.QuoteIdentifier(tableName)
	}

	return dia.QuoteIdentifier(schemaName) + "." + dia.QuoteIdentifier(tableName)
}

func (dia DB2Dialect) ToSQLType(colType reflect.Type, maxSize int) string {
	switch colType.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(colType.Elem(), maxSize)
	case reflect.Bool:
		return "boolean"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "integer"
	case reflect.Int64, reflect.Uint64:
		return "bigint"
	case reflect.Float64:
		return "double"
	case reflect.Float32:
		return "real"
	case reflect.Slice:
		if colType.Elem().Kind() == reflect.Uint8 {
			return "blob"
		}
	}

	switch colType.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "double"
	case "NullBool":
		return "boolean"
	case "Time":
		return "timestamp"
	}

	if maxSize > 0 {
		return fmt.Sprintf("varchar(%d)", maxSize)
	} else {
		return "text"
	}
}

func (dia DB2Dialect) QuerySuffix() string {
	return ""
}

func (dia DB2Dialect) BindVar(i int) string {
	return "?"
}

func (dia DB2Dialect) IfSchemaNotExist(command, schema string) string {
	return command
}

func (dia DB2Dialect) IfTableNotExist(command, schema, table string) string {
	return command
}

func (dia DB2Dialect) QuoteIdentifier(identifier string) string {
	return `"` + identifier + `"`
}

/*
	Sybase Dialect
*/

//Dialect implementation of Sybase Database.
//
//BUG: DB2 and Sybase requires 'IF NOT EXISTS' clause for table creation/schema creation to be executed using 'exec' immediate command.
//Differs from other databases. So, IF NOT EXISTS case is excluded for both these Dialects.
//Hence, user should take care of cleaning the database prior to the usage.
type SybaseDialect struct{}

func (dia SybaseDialect) TableName(schemaName string, tableName string) string {
	if strings.EqualFold(schemaName, "") {
		return dia.QuoteIdentifier(tableName)
	}

	return dia.QuoteIdentifier(schemaName) + "." + dia.QuoteIdentifier(tableName)
}

func (dia SybaseDialect) ToSQLType(colType reflect.Type, maxSize int) string {
	switch colType.Kind() {
	case reflect.Ptr:
		return dia.ToSQLType(colType.Elem(), maxSize)
	case reflect.Bool:
		return "bit"
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "integer"
	case reflect.Int64, reflect.Uint64:
		return "bigint"
	case reflect.Float64:
		return "double"
	case reflect.Float32:
		return "real"
	case reflect.Slice:
		if colType.Elem().Kind() == reflect.Uint8 {
			return "varbinary"
		}
	}

	switch colType.Name() {
	case "NullInt64":
		return "bigint"
	case "NullFloat64":
		return "double"
	case "NullBool":
		return "bit"
	case "Time":
		return "timestamp"
	}

	if maxSize > 0 {
		return fmt.Sprintf("varchar(%d)", maxSize)
	} else {
		return "text"
	}
}

func (dia SybaseDialect) QuerySuffix() string {
	return ";"
}

func (dia SybaseDialect) BindVar(i int) string {
	return "?"
}

func (dia SybaseDialect) IfSchemaNotExist(command, schema string) string {
	return command
}

func (dia SybaseDialect) IfTableNotExist(command, schema, table string) string {
	return command
}

func (dia SybaseDialect) QuoteIdentifier(identifier string) string {
	return `"` + identifier + `"`
}
