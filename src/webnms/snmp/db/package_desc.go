/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package db acts as an interface between the database and all the SNMP operations in the API.
It provides various wrapper types to interact with the database for fetching, inserting, removing data and other
database operations.

It defines various dialects like MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, Sybase dialects
for inter-operability among different database queries.

  Note:
  * This package is for internal use only. User need not use this package directly.
  * Refer SnmpAPI and High-Level APIs 'InitDB()' method to open a database connection and use DB as a persistent storage.
*/
package db
