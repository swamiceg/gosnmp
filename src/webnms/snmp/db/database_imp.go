/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package db

import (
	"database/sql"
	"encoding/hex"
	"errors"
	"strconv"
)

//SQLDialect Constants. These constants should be passed while Initializing the database.
//It is required for all the database operations.
//These dialect ids are required for acheiving inter-operability between different databases.
const (
	MySQL     int = 1
	Postgres  int = 2
	SQLite    int = 3
	Oracle    int = 4
	SQLServer int = 5
	DB2       int = 6
	Sybase    int = 7
)

//DBOperationsImpl is snmp specific DB implementation to perform database operations.
//It helps in opening DB connection and performing basic database operations for tables such as
//USMEntry and EngineEntry.
//
//For internal use only. Refer SnmpAPI and High-Level APIs InitDB() method.
type DBOperationsImpl struct {
	dbConn   *sql.DB
	database *Database
	dialect  SQLDialect
}

//NewDB opens a new database connection based on the driverName and dataSourceName.
//It creates tables in the database required for SNMP.
//
//Driver should be loaded in prior.
//For internal use only.
func NewDB(driverName string, dataSourceName string, dialectId int) (*DBOperationsImpl, error) {

	dbImpl := new(DBOperationsImpl)
	var err error

	//Open DB connection
	if dbImpl.dbConn, err = sql.Open(driverName, dataSourceName); err != nil {
		return nil, errors.New("Error while opening database :: " + err.Error())
	}

	dialect := sqlDialects[dialectId]
	if dialect == nil {
		return nil, errors.New("Dialect not registered for dialectId :: " + strconv.Itoa(dialectId))
	}

	//Store the dialect instane dbImpl.
	dbImpl.dialect = dialect

	//Initialize Database instance for performing DB operations
	dbImpl.database = NewDatabase(dbImpl.dbConn, dialect)
	//Create Engine and USM table required
	err = createTables(dbImpl.database)
	if err != nil {
		dbImpl.database.Close()
		return nil, err
	}

	return dbImpl, nil
}

//Holds the SQLDialects
var sqlDialects = map[int]SQLDialect{
	MySQL:     MySQLDialect{},
	Postgres:  PostgresDialect{},
	SQLite:    SqliteDialect{},
	Oracle:    OracleDialect{},
	SQLServer: SqlServerDialect{},
	DB2:       DB2Dialect{},
	Sybase:    SybaseDialect{},
}

var schemaName string = "snmpdb"

func createTables(db *Database) error {
	tab := db.AddTable(EngineEntry{})
	tab.SetKeys("DBKey")
	//tab.SetSchemaName(schemaName)

	tab = db.AddTable(UsmEntry{})
	tab.SetKeys("DBKey")
	//tab.SetSchemaName(schemaName)

	return db.CreateTables()
}

//RegisterSQLDialect adds/registers the new SQLDialect implementation on the database.
//dialect should be an implementation of SQLDialect interface.
//
//User should use this method when they want to provide the new implementation for the database
//other than provided by the API.
//
//Default implementation exist for: MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, and Sybase.
//
//Returns error in case of failure in registering the Dialect.
func RegisterNewDialect(dialectId int, dialect SQLDialect) error {

	_, ok := sqlDialects[dialectId]
	if ok {
		return errors.New("Key :: " + strconv.Itoa(dialectId) + " already exists. Choose different dialectId to register.")
	}

	sqlDialects[dialectId] = dialect

	return nil
}

//Returns a slice of SQLDialects registered
func SQLDialects() []SQLDialect {
	var dialects []SQLDialect
	for _, d := range sqlDialects {
		dialects = append(dialects, d)
	}

	return dialects
}

/*
	Engine Entry
*/

//EngineEntry is the wrapper type to store and retrieve data required for SNMP Peer Engines.
//It holds data such as EngineID, EngineBoots etc.,
//
//'Host##Port' is the key for the engine entries.
type EngineEntry struct {
	DBKey       string
	Host        string
	Port        int
	EngineName  string
	EngineId    []byte
	EngineTime  int32
	EngineBoots int32
	StartTime   int64 //To record the start time of the engine
	//This value records the
	//highest value of SnmpEngineTime that was received by the
	//non-authoritative SNMP engine from the authoritative SNMP engine
	LatestReceivedEngineTime int32
}

//Returns new instance of EngineEntry.
func NewEngineEntry(host string,
	port int,
	eName string,
	eId []byte,
	eTime int32,
	eBoots int32,
	startTime int64,
	latestRcvdEngTime int32,
) EngineEntry {

	dbKey := engineEntryKey(host, port)

	return EngineEntry{
		DBKey:                    dbKey,
		Host:                     host,
		Port:                     port,
		EngineName:               eName,
		EngineId:                 eId,
		EngineTime:               eTime,
		EngineBoots:              eBoots,
		StartTime:                startTime,
		LatestReceivedEngineTime: latestRcvdEngTime,
	}
}

func engineEntryKey(host string, port int) string {
	return host + "##" + strconv.Itoa(port) //Host name is dotted decimal IP address
}

//Insert engine entry into DB.
func (dbImpl *DBOperationsImpl) InsertEngineEntry(host string,
	port int,
	eName string,
	eId []byte,
	eTime int32,
	eBoots int32,
	startTime int64,
	latestRcvdEngTime int32,
) error {

	entry := NewEngineEntry(host, port, eName, eId, eTime, eBoots, startTime, latestRcvdEngTime)

	return dbImpl.database.Insert(entry)
}

//Update engine entry in DB.
func (dbImpl *DBOperationsImpl) UpdateEngineEntry(host string,
	port int,
	eName string,
	eId []byte,
	eTime int32,
	eBoots int32,
	startTime int64,
	latestRcvdEngTime int32,
) error {

	entry := NewEngineEntry(host, port, eName, eId, eTime, eBoots, startTime, latestRcvdEngTime)

	count, err := dbImpl.database.Update(&entry)
	if count != 1 {
		return errors.New("Invalid no of rows updated")
	}
	return err
}

//Returns engine entry based on host, port from DB.
func (dbImpl *DBOperationsImpl) EngineEntry(host string, port int) (EngineEntry, error) {

	dbKey := engineEntryKey(host, port)
	entry, err := dbImpl.database.SelectOne(EngineEntry{}, dbKey)
	if err != nil {
		return EngineEntry{}, err
	}

	return entry.(EngineEntry), err
}

//Returns a slice of engine entries from DB.
func (dbImpl *DBOperationsImpl) EngineEntries() ([]EngineEntry, error) {
	entries, err := dbImpl.database.SelectAll(EngineEntry{})
	if entries != nil {
		return toEngineEntries(entries), err
	}

	return nil, err
}

//Returns a slice of engine entries based on EngineID.
func (dbImpl *DBOperationsImpl) EngineEntriesByID(engineID []byte) ([]EngineEntry, error) {

	entries, err := dbImpl.database.SelectQuery(EngineEntry{}, []string{"EngineId"}, engineID)
	if err != nil {
		return nil, err
	}

	return toEngineEntries(entries), err
}

//Deletes engine entry based on host, port from DB.
func (dbImpl *DBOperationsImpl) DeleteEngineEntry(host string, port int) error {

	dbKey := engineEntryKey(host, port)
	entry := EngineEntry{DBKey: dbKey}
	count, err := dbImpl.database.Delete(entry)

	if count != 1 {
		return errors.New("Invalid number of rows deleted :: " + strconv.FormatInt(count, 10))
	}

	return err
}

//Deletes all the engine entries from DB. Similar to truncate table.
func (dbImpl *DBOperationsImpl) DeleteEngineEntries() error {
	entry := EngineEntry{}
	count, err := dbImpl.database.DeleteAll(entry)

	if count < 1 {
		return errors.New("Invalid number of rows deleted :: " + strconv.FormatInt(count, 10))
	}

	return err
}

func toEngineEntries(ifSlice []interface{}) []EngineEntry {
	var engSlice []EngineEntry
	for _, val := range ifSlice {
		engSlice = append(engSlice, val.(EngineEntry))
	}

	return engSlice
}

/*
	USM Entry
*/

//USMEntry is the wrapper type to store and retrieve data required for USM UserTable.
//It holds data such as EngineID, Username, Authentication protocol etc.,
//
//'Username##EngineID' is the key for the USM user entries.
type UsmEntry struct {
	DBKey         string
	EngineId      []byte
	UserName      string
	SecurityLevel string
	SecurityName  string
	AuthProtocol  string
	AuthPassword  string
	AuthKey       []byte
	PrivProtocol  string
	PrivPassword  string
	PrivKey       []byte
}

//Returns new isntance of UsmEntry.
func NewUsmEntry(
	eId []byte,
	userName string,
	securityLevel string,
	securityName string,
	authProtocol string,
	authPassword string,
	authKey []byte,
	privProtocol string,
	privPassword string,
	privKey []byte,
) UsmEntry {

	dbKey := usmEntryKey(userName, eId)

	return UsmEntry{
		DBKey:         dbKey,
		EngineId:      eId,
		UserName:      userName,
		SecurityLevel: securityLevel,
		SecurityName:  securityName,
		AuthProtocol:  authProtocol,
		AuthPassword:  authPassword,
		AuthKey:       authKey,
		PrivProtocol:  privProtocol,
		PrivPassword:  privPassword,
		PrivKey:       privKey,
	}
}

func usmEntryKey(userName string, eId []byte) string {
	return userName + "##" + hex.EncodeToString(eId)
}

//Returns USM user entry based username and engineID from DB.
func (dbImpl *DBOperationsImpl) UsmEntry(userName string, eId []byte) (UsmEntry, error) {

	dbKey := usmEntryKey(userName, eId)
	entry, err := dbImpl.database.SelectOne(UsmEntry{}, dbKey)
	if err != nil {
		return UsmEntry{}, err
	}

	return entry.(UsmEntry), err
}

//Returns all the USM user entries from DB.
func (dbImpl *DBOperationsImpl) UsmEntries() ([]UsmEntry, error) {
	entries, err := dbImpl.database.SelectAll(UsmEntry{})
	if entries != nil {
		return toUsmEntries(entries), err
	}

	return nil, err
}

//Returns a slice of USM user entriers based on the EngineID.
func (dbImpl *DBOperationsImpl) UsmEntriesByID(engineID []byte) ([]UsmEntry, error) {

	entries, err := dbImpl.database.SelectQuery(UsmEntry{}, []string{"EngineId"}, engineID)
	if err != nil {
		return nil, err
	}

	return toUsmEntries(entries), err
}

//Inserts the USM user entry into DB.
func (dbImpl *DBOperationsImpl) InsertUsmEntry(
	eId []byte,
	userName string,
	secLevel string,
	secName string,
	authProtocol string,
	authPassword string,
	authKey []byte,
	privProtocol string,
	privPassword string,
	privKey []byte,
) error {

	entry := NewUsmEntry(eId, userName, secLevel, secName, authProtocol, authPassword, authKey, privProtocol, privPassword, privKey)

	return dbImpl.database.Insert(entry)
}

//Updates USM user entry in DB.
func (dbImpl *DBOperationsImpl) UpdateUsmEntry(
	eId []byte,
	userName string,
	secLevel string,
	secName string,
	authProtocol string,
	authPassword string,
	authKey []byte,
	privProtocol string,
	privPassword string,
	privKey []byte,
) error {

	entry := NewUsmEntry(eId, userName, secLevel, secName, authProtocol, authPassword, authKey, privProtocol, privPassword, privKey)

	count, err := dbImpl.database.Update(&entry)
	if count != 1 {
		return errors.New("Invalid no of rows updated")
	}
	return err
}

//Deletes USM user entry based on username, engineID from DB.
func (dbImpl *DBOperationsImpl) DeleteUsmEntry(userName string, eId []byte) error {

	dbKey := usmEntryKey(userName, eId)
	entry := UsmEntry{DBKey: dbKey}
	count, err := dbImpl.database.Delete(entry)

	if count != 1 {
		return errors.New("Invalid number of rows deleted :: " + strconv.FormatInt(count, 10))
	}

	return err
}

//Deletes all the USM user entries from DB. Similar to truncate table.
func (dbImpl *DBOperationsImpl) DeleteUsmEntries() error {
	entry := UsmEntry{}
	count, err := dbImpl.database.DeleteAll(entry)

	if count < 1 {
		return errors.New("Invalid number of rows deleted :: " + strconv.FormatInt(count, 10))
	}

	return err
}

func toUsmEntries(ifSlice []interface{}) []UsmEntry {
	var usmSlice []UsmEntry
	for _, val := range ifSlice {
		usmSlice = append(usmSlice, val.(UsmEntry))
	}

	return usmSlice
}

//Closes the database connection if open any.
func (dbImpl *DBOperationsImpl) CloseDB() error {
	return dbImpl.database.Close()
}
