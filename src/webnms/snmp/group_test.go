/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"webnms/snmp/consts"
	"webnms/snmp/msg"

	//"fmt"
	"testing"
)

//Test SnmpGroup counters
var cmdCountersTest = []consts.Command{
	consts.GetRequest,
	consts.GetNextRequest,
	consts.SetRequest,
	//consts.GetBulkRequest,
}

func TestCounters(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	var err error

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
	}
	mesg := msg.NewSnmpMessage()

	//Send a valid Confirmed Requests and test the counters
	var count uint32 = 5
	for _, cmd := range cmdCountersTest {
		for i := 0; i < int(count); i++ {
			mesg.SetCommand(cmd)
			_, err = ses.SyncSend(mesg)
			if err != nil {
				t.Fatalf("SnmpGroup counters test. Err: %s.", err.Error())
			}
		}
	}
	sesPo := ses.ProtocolOptions()
	grp := api.SnmpGroup(sesPo.LocalHost(), sesPo.LocalPort())
	if grp == nil {
		t.Errorf("SnmpGroup: SnmpSession has failed to create SnmpGroup for localhost: %s; localport: %d;", sesPo.LocalHost(), sesPo.LocalPort())
	} else {
		//Check the counters values in SnmpGroup
		if grp.SnmpOutPktsCounter() != (uint32(len(cmdCountersTest)) * count) {
			t.Errorf("SnmpGroup: Invalid out pkts counter. Received: %d. Expected count: %d.", grp.SnmpOutPktsCounter(), (uint32(len(cmdCountersTest)) * count))
		}
		if grp.SnmpInPktsCounter() != (uint32(len(cmdCountersTest)) * count) {
			t.Errorf("SnmpGroup: Invalid in pkts counter. Received: %d. Expected count: %d.", grp.SnmpInPktsCounter(), (uint32(len(cmdCountersTest)) * count))
		}
		if grp.SnmpInGetResponsesCounter() != (uint32(len(cmdCountersTest)) * count) {
			t.Errorf("SnmpGroup: Invalid in GetResponses counter. Received: %d. Expected count: %d.", grp.SnmpInGetResponsesCounter(), (uint32(len(cmdCountersTest)) * count))
		}
		if grp.SnmpOutGetRequestsCounter() != count {
			t.Errorf("SnmpGroup: Invalid out GetRequests counter. Received: %d. Expected count: %d.", grp.SnmpOutGetRequestsCounter(), count)
		}
		if grp.SnmpOutGetNextsCounter() != count {
			t.Errorf("SnmpGroup: Invalid out GetNext Requests counter. Received: %d. Expected count: %d.", grp.SnmpOutGetNextsCounter(), count)
		}
		if grp.SnmpOutSetRequestsCounter() != count {
			t.Errorf("SnmpGroup: Invalid out Set Requests counter. Received: %d. Expected count: %d.", grp.SnmpOutSetRequestsCounter(), count)
		}
	}

}
