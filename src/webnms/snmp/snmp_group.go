/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"webnms/snmp/consts"
	"webnms/snmp/msg"
)

//SnmpGroup maintains the counter values. The SnmpGroup counters
//are maintained for each SNMP entity. i.e for each local_addrs, local_port pair in the SnmpSession.
//Except for the counter variables snmpInBadCommunityNames, snmpInBadCommunityUses, snmpInTotalReqVars and
//snmpInTotalSetVars, all other variables are updated by the api itself.
//The api user will have to explicitly call the increment methods to update
//the above mentioned four variables.
//
//The api user need not create instance of SnmpGroup. It is instantiated by the SnmpSession.
//Since the SnmpGroup is maintained for each local_addrs, local_port of the SnmpSession,
//the api user will have to get the corresponding SnmpGroup instance. Each SnmpGroup object is maintained in a table, in the SnmpAPI.
//The SnmpAPI provides methods to get the particular SnmpGroup entry for local_addrs, local_port pair.
type SnmpGroup struct { //From SNMPv2-MIB
	//In Counters::
	snmpInPkts         uint32
	snmpInTotalReqVars uint32
	snmpInTotalSetVars uint32
	//Command
	snmpInGetRequests  uint32
	snmpInGetNexts     uint32
	snmpInSetRequests  uint32
	snmpInGetResponses uint32
	snmpInTraps        uint32
	//Error
	snmpInTooBigs           uint32
	snmpInNoSuchNames       uint32
	snmpInBadValues         uint32
	snmpInReadOnlys         uint32
	snmpInGenErrs           uint32
	snmpInBadVersions       uint32
	snmpInBadCommunityNames uint32
	snmpInBadCommunityUses  uint32
	snmpInASNParseErrs      uint32

	//Out Counters::
	snmpOutPkts uint32
	//Command
	snmpOutGetRequests  uint32
	snmpOutGetNexts     uint32
	snmpOutSetRequests  uint32
	snmpOutGetResponses uint32
	snmpOutTraps        uint32
	//Error
	snmpOutTooBigs     uint32
	snmpOutNoSuchNames uint32
	snmpOutBadValues   uint32
	snmpOutGenErrs     uint32

	snmpSilentDrops uint32
	snmpProxyDrops  uint32
}

//Increment the counters after sending SNMP packets
//Command and error counters incremented
func (grp *SnmpGroup) ProcessInPkts(mesg msg.SnmpMessage) {
	//Process command
	switch mesg.Command() {
	case consts.GetRequest:
		grp.snmpInGetRequests++
	case consts.GetNextRequest:
		grp.snmpInGetNexts++
	case consts.SetRequest:
		grp.snmpInSetRequests++
	case consts.GetResponse:
		grp.snmpInGetResponses++
	case consts.TrapRequest, consts.Trap2Request:
		grp.snmpInTraps++
	}

	//Process error
	switch mesg.ErrorStatus() {
	case consts.TooBig:
		grp.snmpInTooBigs++
	case consts.NoSuchName:
		grp.snmpInNoSuchNames++
	case consts.BadValue:
		grp.snmpInBadValues++
	case consts.ReadOnly:
		grp.snmpInReadOnlys++
	case consts.GeneralError:
		grp.snmpInGenErrs++
	}
}

//Increment the counters after receiving SNMP packets
//Command and error counters incremented
func (grp *SnmpGroup) ProcessOutPkts(mesg msg.SnmpMessage) {
	grp.snmpOutPkts++

	//Process command
	switch mesg.Command() {
	case consts.GetRequest:
		grp.snmpOutGetRequests++
	case consts.GetNextRequest:
		grp.snmpOutGetNexts++
	case consts.SetRequest:
		grp.snmpOutSetRequests++
	case consts.GetResponse: //Outgoing response may contain errors, increment counters
		{
			grp.snmpOutGetResponses++
			//Process error
			switch mesg.ErrorStatus() {
			case consts.TooBig:
				grp.snmpOutTooBigs++
			case consts.NoSuchName:
				grp.snmpOutNoSuchNames++
			case consts.BadValue:
				grp.snmpOutBadValues++
			case consts.GeneralError:
				grp.snmpOutGenErrs++
			}
		}
	case consts.TrapRequest, consts.Trap2Request:
		grp.snmpOutTraps++
	}
}

//Incrementors and Getter methods:

/*
	For In Packets
*/

//Increments SnmpInPkts counter by 1. SnmpInPkts is the total number of messages delivered to the SNMP
//entity from the transport service.
func (grp *SnmpGroup) IncrSnmpInPkts() { grp.snmpInPkts++ }

//Returns SnmpInPkts counter. SnmpInPkts is the total number of messages delivered to the SNMP
//entity from the transport service.
func (grp *SnmpGroup) SnmpInPktsCounter() uint32 { return grp.snmpInPkts }

//Increments SnmpInTotalReqVars counter by 1. SnmpInTotalReqVars is the total number of MIB objects which have been
//retrieved successfully by the SNMP protocol entity
//as the result of receiving valid SNMP Get-Request and Get-Next PDUs.
func (grp *SnmpGroup) IncrSnmpInTotalReqVars() { grp.snmpInTotalReqVars++ }

//Returns SnmpInTotalReqVars counter. SnmpInTotalReqVars is the total number of MIB objects which have been
//retrieved successfully by the SNMP protocol entity
//as the result of receiving valid SNMP Get-Request and Get-Next PDUs.
func (grp *SnmpGroup) SnmpInTotalReqVarsCounter() uint32 { return grp.snmpInTotalReqVars }

//Increments SnmpInTotalSetVars counter by 1. SnmpInTotalSetVars is the total number of MIB objects which have been
//altered successfully by the SNMP protocol entity as the result of receiving valid SNMP Set-Request PDUs.
func (grp *SnmpGroup) IncrSnmpInTotalSetVars() { grp.snmpInTotalSetVars++ }

//Returns SnmpInTotalSetVars counter. SnmpInTotalSetVars is the total number of MIB objects which have been
//altered successfully by the SNMP protocol entity as the result of receiving valid SNMP Set-Request PDUs.
func (grp *SnmpGroup) SnmpInTotalSetVarsCounter() uint32 { return grp.snmpInTotalSetVars }

//Increments SnmpInGetRequests counter by 1. SnmpInGetRequests is the total number of SNMP Get-Request PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpInGetRequests() { grp.snmpInGetRequests++ }

//Returns SnmpInGetRequests counter. SnmpInGetRequests is the total number of SNMP Get-Request PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpInGetRequestsCounter() uint32 { return grp.snmpInGetRequests }

//Increments SnmpInGetNexts counter by 1. SnmpInGetNexts is the total number of SNMP Get-Next PDUs which have been
//accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpInGetNexts() { grp.snmpInGetNexts++ }

//Returns SnmpInGetNexts counter. SnmpInGetNexts is the total number of SNMP Get-Next PDUs which have been
//accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpInGetNextsCounter() uint32 { return grp.snmpInGetNexts }

//Increments SnmpInSetRequests counter by 1. SnmpInSetRequests is the total number of SNMP Set-Request PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpInSetRequests() { grp.snmpInSetRequests++ }

//Returns SnmpInSetRequests counter. SnmpInSetRequests is the total number of SNMP Set-Request PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpInSetRequestsCounter() uint32 { return grp.snmpInSetRequests }

//Increments SnmpInGetResponses counter by 1. SnmpInGetResponses is the total number of SNMP Get-Response PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpInGetResponses() { grp.snmpInGetResponses++ }

//Returns SnmpInGetResponses counter. SnmpInGetResponses is the total number of SNMP Get-Response PDUs which
//have been accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpInGetResponsesCounter() uint32 { return grp.snmpInGetResponses }

//Increments SnmpInTraps counter by 1. SnmpInTraps is the total number of SNMP Trap PDUs which have been
//accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpInTraps() { grp.snmpInTraps++ }

//Returns SnmpInTraps counter. SnmpInTraps is the total number of SNMP Trap PDUs which have been
//accepted and processed by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpInTrapsCounter() uint32 { return grp.snmpInTraps }

//Increments SnmpInTooBigs counter by 1. SnmpInTooBigs is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `tooBig'.
func (grp *SnmpGroup) IncrSnmpInTooBigs() { grp.snmpInTooBigs++ }

//Returns SnmpInTooBigs counter. SnmpInTooBigs is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `tooBig'.
func (grp *SnmpGroup) SnmpInTooBigsCounter() uint32 { return grp.snmpInTooBigs }

//Increments SnmpInNoSuchNames counter by 1. SnmpInNoSuchNames is the total number of messages delivered to the SNMP
//entity from the transport service.total number of SNMP PDUs which were delivered to the SNMP protocol entity and for
//which the value of the error-status field was `noSuchName'.
func (grp *SnmpGroup) IncrSnmpInNoSuchNames() { grp.snmpInNoSuchNames++ }

//Returns SnmpInNoSuchNames counter. SnmpInNoSuchNames is the total number of messages delivered to the SNMP
//entity from the transport service.total number of SNMP PDUs which were delivered to the SNMP protocol entity and for
//which the value of the error-status field was `noSuchName'.
func (grp *SnmpGroup) SnmpInNoSuchNamesCounter() uint32 { return grp.snmpInNoSuchNames }

//Increments SnmpInBadValues counter by 1. SnmpInBadValues is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `badValue'.
func (grp *SnmpGroup) IncrSnmpInBadValues() { grp.snmpInBadValues++ }

//Returns SnmpInBadValues counter. SnmpInBadValues is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `badValue'.
func (grp *SnmpGroup) SnmpInBadValuesCounter() uint32 { return grp.snmpInBadValues }

//Increments SnmpInReadOnlys counter by 1. SnmpInReadOnlys is the total number valid SNMP PDUs which were delivered
//to the SNMP protocol entity and for which the value of the error-status field was `readOnly'.
//It should be noted that it is a protocol error to generate an SNMP PDU which contains the value `readOnly' in the
//error-status field, as such this object is provides as a means of detecting incorrect implementations of the SNMP.
func (grp *SnmpGroup) IncrSnmpInReadOnlys() { grp.snmpInReadOnlys++ }

//Returns SnmpInReadOnlys counter. SnmpInReadOnlys is the total number valid SNMP PDUs which were delivered
//to the SNMP protocol entity and for which the value of the error-status field was `readOnly'.
//It should be noted that it is a protocol error to generate an SNMP PDU which contains the value `readOnly' in the
//error-status field, as such this object is provides as a means of detecting incorrect implementations of the SNMP.
func (grp *SnmpGroup) SnmpInReadOnlysCounter() uint32 { return grp.snmpInReadOnlys }

//Increments SnmpInGenErrs counter by 1. SnmpInGenErrs is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `genErr'.
func (grp *SnmpGroup) IncrSnmpInGenErrs() { grp.snmpInGenErrs++ }

//Returns SnmpInGenErrs counter. SnmpInGenErrs is the total number of SNMP PDUs which were
//delivered to the SNMP protocol entity and for which the value of the error-status field was `genErr'.
func (grp *SnmpGroup) SnmpInGenErrsCounter() uint32 { return grp.snmpInGenErrs }

//Increments SnmpInBadVersions counter by 1. SnmpInBadVersions is the total number of SNMP messages
//which were delivered to the SNMP entity and were for an unsupported SNMP version.
func (grp *SnmpGroup) IncrSnmpInBadVersions() { grp.snmpInBadVersions++ }

//Returns SnmpInBadVersions counter. SnmpInBadVersions is the total number of SNMP messages
//which were delivered to the SNMP entity and were for an unsupported SNMP version.
func (grp *SnmpGroup) SnmpInBadVersionsCounter() uint32 { return grp.snmpInBadVersions }

//Increments SnmpInBadCommunityNames counter by 1. SnmpInBadCommunityNames is the total number of community-based SNMP messages (for
//example,  SNMPv1) delivered to the SNMP entity which used an SNMP community name not known to said entity.
func (grp *SnmpGroup) IncrSnmpInBadCommunityNames() { grp.snmpInBadCommunityNames++ }

//Returns SnmpInBadCommunityNames counter. SnmpInBadCommunityNames is the total number of community-based SNMP messages (for
//example,  SNMPv1) delivered to the SNMP entity which used an SNMP community name not known to said entity.
func (grp *SnmpGroup) SnmpInBadCommunityNamesCounter() uint32 { return grp.snmpInBadCommunityNames }

//Increments SnmpInBadCommunityUses counter by 1. SnmpInBadCommunityUses is the total number of community-based SNMP messages
//(for example, SNMPv1) delivered to the SNMP entity which represented an SNMP operation that was not allowed for
//the SNMP community named in the message.
func (grp *SnmpGroup) IncrSnmpInBadCommunityUses() { grp.snmpInBadCommunityUses++ }

//Returns SnmpInBadCommunityUses counter. SnmpInBadCommunityUses is the total number of community-based SNMP messages
//(for example, SNMPv1) delivered to the SNMP entity which represented an SNMP operation that was not allowed for
//the SNMP community named in the message.
func (grp *SnmpGroup) SnmpInBadCommunityUsesCounter() uint32 { return grp.snmpInBadCommunityUses }

//Increments SnmpInASNParseErrs counter by 1. SnmpInASNParseErrs is the total number of ASN.1 or BER errors encountered by
//the SNMP entity when decoding received SNMP messages.
func (grp *SnmpGroup) IncrSnmpInASNParseErrs() { grp.snmpInASNParseErrs++ }

//Returns SnmpInASNParseErrs counter. SnmpInASNParseErrs is the total number of ASN.1 or BER errors encountered by
//the SNMP entity when decoding received SNMP messages.
func (grp *SnmpGroup) SnmpInASNParseErrsCounter() uint32 { return grp.snmpInASNParseErrs }

/*
	For Out Packets
*/

//Increments SnmpOutPkts counter by 1. SnmpOutPkts is the total number of SNMP Messages which were
//passed from the SNMP protocol entity to the transport service.
func (grp *SnmpGroup) IncrSnmpOutPkts() { grp.snmpOutPkts++ }

//Returns SnmpOutPkts counter. SnmpOutPkts is the total number of SNMP Messages which were
//passed from the SNMP protocol entity to the transport service.
func (grp *SnmpGroup) SnmpOutPktsCounter() uint32 { return grp.snmpOutPkts }

//Increments SnmpOutGetRequests counter by 1. SnmpOutGetRequests is the total number of SNMP Get-Request PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpOutGetRequests() { grp.snmpOutGetRequests++ }

//Returns SnmpOutGetRequests counter. SnmpOutGetRequests is the total number of SNMP Get-Request PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpOutGetRequestsCounter() uint32 { return grp.snmpOutGetRequests }

//Increments SnmpOutGetNexts counter by 1. SnmpOutGetNexts is the total number of SNMP Get-Next PDUs which have
//been generated by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpOutGetNexts() { grp.snmpOutGetNexts++ }

//Returns SnmpOutGetNexts counter. SnmpOutGetNexts is the total number of SNMP Get-Next PDUs which have
//been generated by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpOutGetNextsCounter() uint32 { return grp.snmpOutGetNexts }

//Increments SnmpOutSetRequests counter by 1. SnmpOutSetRequests is the total number of SNMP Set-Request PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpOutSetRequests() { grp.snmpOutSetRequests++ }

//Returns SnmpOutSetRequests counter. SnmpOutSetRequests is the total number of SNMP Set-Request PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpOutSetRequestsCounter() uint32 { return grp.snmpOutSetRequests }

//Increments SnmpOutGetResponses counter by 1. SnmpOutGetResponses is the total number of SNMP Get-Response PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpOutGetResponses() { grp.snmpOutGetResponses++ }

//Returns SnmpOutGetResponses counter. SnmpOutGetResponses is the total number of SNMP Get-Response PDUs which
//have been generated by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpOutGetResponsesCounter() uint32 { return grp.snmpOutGetResponses }

//Increments SnmpOutTraps counter by 1. SnmpOutTraps is the total number of SNMP Trap PDUs which have
//been generated by the SNMP protocol entity.
func (grp *SnmpGroup) IncrSnmpOutTraps() { grp.snmpOutTraps++ }

//Returns SnmpOutTraps counter. SnmpOutTraps is the total number of SNMP Trap PDUs which have
//been generated by the SNMP protocol entity.
func (grp *SnmpGroup) SnmpOutTrapsCounter() uint32 { return grp.snmpOutTraps }

//Increments SnmpOutTooBigs counter by 1. SnmpOutTooBigs is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `tooBig.'
func (grp *SnmpGroup) IncrSnmpOutTooBigs() { grp.snmpOutTooBigs++ }

//Returns SnmpOutTooBigs counter. SnmpOutTooBigs is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `tooBig.'
func (grp *SnmpGroup) SnmpOutTooBigsCounter() uint32 { return grp.snmpOutTooBigs }

//Increments SnmpOutNoSuchNames counter by 1. SnmpOutNoSuchNames is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status was `noSuchName'.
func (grp *SnmpGroup) IncrSnmpOutNoSuchNames() { grp.snmpOutNoSuchNames++ }

//Returns SnmpOutNoSuchNames counter. SnmpOutNoSuchNames is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status was `noSuchName'.
func (grp *SnmpGroup) SnmpOutNoSuchNamesCounter() uint32 { return grp.snmpOutNoSuchNames }

//Increments SnmpOutBadValues counter by 1. SnmpOutBadValues is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `badValue'.
func (grp *SnmpGroup) IncrSnmpOutBadValues() { grp.snmpOutBadValues++ }

//Returns SnmpOutBadValues counter. SnmpOutBadValues is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `badValue'.
func (grp *SnmpGroup) SnmpOutBadValuesCounter() uint32 { return grp.snmpOutBadValues }

//Increments SnmpOutGenErrs counter by 1. SnmpOutGenErrs is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `genErr'.
func (grp *SnmpGroup) IncrSnmpOutGenErrs() { grp.snmpOutGenErrs++ }

//Returns SnmpOutGenErrs counter. SnmpOutGenErrs is the total number of SNMP PDUs which were generated
//by the SNMP protocol entity and for which the value of the error-status field was `genErr'.
func (grp *SnmpGroup) SnmpOutGenErrsCounter() uint32 { return grp.snmpOutGenErrs }

//Increments SnmpProxyDrops counter by 1. SnmpProxyDrops is the total number of Confirmed Class PDUs
//(such as GetRequest-PDUs, GetNextRequest-PDUs, GetBulkRequest-PDUs, SetRequest-PDUs, and
//InformRequest-PDUs) delivered to the SNMP entity which were silently dropped because the transmission of
//the (possibly translated) message to a proxy target failed in a manner (other than a time-out) such that
//no Response Class PDU (such as a Response-PDU) could be returned.
func (grp *SnmpGroup) IncrSnmpProxyDrops() { grp.snmpProxyDrops++ }

//Returns SnmpProxyDrops counter. SnmpProxyDrops is the total number of Confirmed Class PDUs
//(such as GetRequest-PDUs, GetNextRequest-PDUs, GetBulkRequest-PDUs, SetRequest-PDUs, and
//InformRequest-PDUs) delivered to the SNMP entity which were silently dropped because the transmission of
//the (possibly translated) message to a proxy target failed in a manner (other than a time-out) such that
//no Response Class PDU (such as a Response-PDU) could be returned.
func (grp *SnmpGroup) SnmpProxyDropsCounter() uint32 { return grp.snmpProxyDrops }

//Increments SnmpSilentDrops counter by 1. SnmpSilentDrops is the total number of Confirmed Class PDUs (such as
//GetRequest-PDUs, GetNextRequest-PDUs, GetBulkRequest-PDUs, SetRequest-PDUs, and
//InformRequest-PDUs) delivered to the SNMP entity which were silently dropped because the size of a reply
//containing an alternate Response Class PDU (such as a Response-PDU) with an empty variable-bindings field
//was greater than either a local constraint or the maximum message size associated with the originator of the request.
func (grp *SnmpGroup) IncrSnmpSilentDrops() { grp.snmpSilentDrops++ }

//Returns SnmpSilentDrops counter. SnmpSilentDrops is the total number of Confirmed Class PDUs (such as
//GetRequest-PDUs, GetNextRequest-PDUs, GetBulkRequest-PDUs, SetRequest-PDUs, and
//InformRequest-PDUs) delivered to the SNMP entity which were silently dropped because the size of a reply
//containing an alternate Response Class PDU (such as a Response-PDU) with an empty variable-bindings field
//was greater than either a local constraint or the maximum message size associated with the originator of the request.
func (grp *SnmpGroup) SnmpSilentDropsCounter() uint32 { return grp.snmpSilentDrops }
