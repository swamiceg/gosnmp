/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"container/list"
	"errors"
	"net"
	"strconv"
	"strings"
	"time"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/mpm"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	"webnms/snmp/engine/security/v1v2"
	"webnms/snmp/engine/transport/tcp"
	"webnms/snmp/engine/transport/udp"
)

//SnmpAPI is the root type defined in Go SNMP API. It contains list of SnmpSessions, underwhich it monitors the SNMP requests for Timeouts and retry.
//
//User must add the SnmpAPI instance to their SnmpSession using NewSnmpSession(SnmpAPI) function, which only then will monitor the entire communication that happens under the SnmpSession.
//
//SnmpAPI instantiates the SnmpEngine and it's respective SubSystems such as MsgProcessingSubSystem, SecuritySubSystem, and Transport SubSystem.
//It registers all the SnmpModels under these SubSystems by default. User need not instantiate any security models/msg processing models unless
//they are implementing their custom models.
//
//Hence, creating an instance of SnmpAPI indicates a complete SnmpEngine as per RFC3411 architecture, and thereby indicates an SNMP Entity.
//
//Note: Create instance of it using NewSnmpAPI() function.
type SnmpAPI struct {
	engine  engine.SnmpEngine //This will hold the instance of an SNMPv3 Engine.
	snmpLCD *engine.SnmpLCD

	snmpGrpMap  map[string]*SnmpGroup //To store the SnmpGroup for each Session
	dbImpl      *db.DBOperationsImpl
	dbFlag      bool
	sessionList list.List
	apiClosed   bool
	debug       bool
}

//NewSnmpAPI creates and returns a new SnmpAPI instance. It will start a new goroutine to monitor the SnmpSessions registered with it.
//
//SnmpAPI is the root type defined in SNMP API. It contains list of SnmpSessions, underwhich it monitors the SNMP requests for Timeouts and retry.
//It also performs all the initialization required for the Engine framework.
//
//Note: Call Close method on the SnmpAPI instance in order to stop the GoRoutine which monitors the SnmpSessions.
func NewSnmpAPI() *SnmpAPI {
	api := new(SnmpAPI)
	api.init()

	go api.startMonitoring() //Start monitoring routine to monitor the SnmpSessions under this SnmpAPI

	return api
}

//Initialize the SnmpEngine framework.
func (api *SnmpAPI) init() {
	api.snmpGrpMap = make(map[string]*SnmpGroup)
	//Create new SnmpLCD
	api.snmpLCD = engine.NewSnmpLCD()
	//Create new SnmpEngine
	api.engine = engine.NewSnmpEngine(api.snmpLCD, "localhost", 0, 0)

	//Create and plug instance of SubSystems into Engine
	mps := engine.NewMsgProcessingSubSystem(api.engine)
	secSS := engine.NewSecuritySubSystem(api.engine)
	transportSS := engine.NewTransportSubSystem(api.engine)
	api.engine.SetMsgProcessingSubSystem(mps)
	api.engine.SetSecuritySubSystem(secSS)
	api.engine.SetTransportSubSystem(transportSS)
	//Dispatcher registration can be here

	//Create and plug SNMP models into the SubSystems.

	//Message Processing Models
	v1mp := mpm.NewMsgProcessingModelV1(mps)
	mps.AddModel(v1mp) //Register this MPM in Msg Processing SubSystem

	v2cmp := mpm.NewMsgProcessingModelV2C(mps)
	mps.AddModel(v2cmp)

	v3mp := mpm.NewMsgProcessingModelV3(mps)
	mps.AddModel(v3mp)

	//Security Models
	v1v2sec := v1v2.NewSnmpSecurityModelV1V2(secSS)
	secSS.AddModel(v1v2sec) //Register V1 SecurityModel in Security SubSystem
	secSS.AddModel(v1v2sec) //Register V2C SecurityModel in Security SubSystem

	usmModel := usm.NewUSMSecurityModel(secSS)
	secSS.AddModel(usmModel) //Register USM SecurityModel in Security SubSystem

	//Transport Models (TM)
	udp := new(udp.UDPTransportModel)
	tcp := new(tcp.TCPTransportModel)
	udp.SetTransportSubSystem(transportSS)
	tcp.SetTransportSubSystem(transportSS)
	transportSS.AddModel(udp) //Register UDP transport model in Transport SubSystem
	transportSS.AddModel(tcp) //Register TCP transport model in Transport SubSystem
}

//Returns an instance of SnmpEngine associated with this api, which holds all the SubSystems and their models.
//Represents a SNMP entity.
func (api *SnmpAPI) SnmpEngine() engine.SnmpEngine {
	return api.engine
}

//InitDB initializes the database connection using the driverName and dataSourceName provided.
//User should have loaded the specific DB driver to open Database connection.
//
//This opens the database connection and creates the necessary tables in the database required for the SNMP API.
//
//dialectID is an integer constant indicating the SQLDialect which the database will be using
//for performing database operations.
//Dialect is required for inter-operability between different database queries.
//
//Refer db package for the list of SQLDialect implementation and their associated constants. If specific dialect
//implementation is not available, user should implement their own and register the same using RegisterSQLDialect() method.
//Refer db.SQLDialect interface for more details.
//
//Returns error in case of any db related error occurs (or) Unsupported DialectID passed.
func (api *SnmpAPI) InitDB(driverName, dataSourceName string, dialectID int) error {
	if api.dbImpl == nil {
		var err error
		api.dbImpl, err = db.NewDB(driverName, dataSourceName, dialectID)
		if err != nil {
			return err
		}

		//Set the DBImpl on SnmpEngine
		api.engine.SetDBOperations(api.dbImpl)
		api.SetDatabaseFlag(true)
	}

	return nil
}

//Closes the DB connection if open any.
func (api *SnmpAPI) CloseDB() error {
	if api.dbImpl != nil {
		err := api.dbImpl.CloseDB()
		api.dbImpl = nil
		api.SetDatabaseFlag(false)
		return err
	}

	return errors.New("Database is not initialized. Cannot close the DB.")
}

//RegisterSQLDialect adds/registers the new SQLDialect implementation on the database.
//dialect should be an implementation of db.SQLDialect interface.
//
//User should use this method when they want to provide the new implementation for the database
//other than provided by the API.
//
//Default implementation exist for: MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, Sybase
//
//Returns error in case of failure in registering the Dialect
func (api *SnmpAPI) RegisterSQLDialect(dialectID int, dialect db.SQLDialect) error {
	return db.RegisterNewDialect(dialectID, dialect)
}

//Returns a slice of SQLDialects registered in db.
func (api *SnmpAPI) SQLDialects() []db.SQLDialect {
	return db.SQLDialects()
}

//SetDatabase enables/diables the usage of database for all the SNMP operations internally.
//If enabled DB will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
//
//Note: When Database usage is disabled in the middle, it may cause some adverse effect of re-synchronization to get the new values
//with all the SNMP entities and fill the local LCD.
func (api *SnmpAPI) SetDatabaseFlag(flag bool) {
	api.dbFlag = flag
	api.engine.SetDatabaseFlag(flag)
}

//Returns bool indicating whether database is enabled for SNMP operations.
func (api *SnmpAPI) IsDatabaseFlag() bool {
	return api.dbFlag
}

//PeerEngineLCD returns the LCD of engine entries specific to the security model.
//EngineLCD holds the entries of PeerEngine instance, which has details about the peer SNMP engines.
//
//Each security model will have its own lcd for storing engine entries.
func (api *SnmpAPI) PeerEngineLCD(securityModel int32) engine.SnmpPeerEngineLCD {
	secSS := api.engine.SecuritySubSystem() //Each security subsystem will have its own PeerEngineLCD
	return secSS.PeerEngineLCD(securityModel)
}

//USMPeerEngineLCD returns the LCD of engine entries specific to USM.
//PeerEngineLCD contains details of the peer SNMP entities such as EngineID, EngineBoots etc.,
func (api *SnmpAPI) USMPeerEngineLCD() *usm.USMPeerEngineLCD {
	usmEngineLCD := api.PeerEngineLCD(security.USMID)
	return usmEngineLCD.(*usm.USMPeerEngineLCD)
}

//USMUserLCD returns the User based security model's UserLCD instance, which contains details of
//Secure Users created under it.
//
//  Refer usm.USMUserLCD type to operate on this user lcd.
func (api *SnmpAPI) USMUserLCD() usm.USMUserLCD {
	secSS := api.engine.SecuritySubSystem()
	userLCD := api.snmpLCD.ModelLCD(secSS, security.USMID) //This will return USM's user LCD

	return userLCD.(usm.USMUserLCD) //Using this user can add/remove users to the LCD.
}

//SetDebug defines whether debugging output should be generated. In the debug mode, the PDU data is printed in hex format in the console.
func (api *SnmpAPI) SetDebug(d bool) {
	api.debug = d
}

//Debug returns a bool indicating whether the debugging is enabled on this SnmpAPI or not.
func (api *SnmpAPI) Debug() bool {
	return api.debug
}

//SnmpGroup returns the instance of SnmpGroup associated with the localhost/port combination.
//Each SnmpSession (dispatcher) creates SnmpGroup, which maintains list of SNMP Counters specific to it.
func (api *SnmpAPI) SnmpGroup(localhost string, localport int) *SnmpGroup {
	grpKey := getGrpKey(localhost, localport)
	if strings.TrimSpace(grpKey) == "" {
		return nil
	} else {
		if grp, ok := api.snmpGrpMap[grpKey]; ok {
			return grp
		}
	}
	return nil
}

func (api *SnmpAPI) addSnmpGroup(localhost string, localport int, grp *SnmpGroup) {
	if grp != nil {
		grpKey := getGrpKey(localhost, localport)
		if strings.TrimSpace(grpKey) != "" {
			api.snmpGrpMap[grpKey] = grp
		}
	}
}

func (api *SnmpAPI) removeSnmpGroup(localhost string, localport int) {
	grpKey := getGrpKey(localhost, localport)
	if _, ok := api.snmpGrpMap[grpKey]; ok {
		delete(api.snmpGrpMap, grpKey)
	}
}

func getGrpKey(addr string, port int) (key string) {
	addr = getIPFromHost(addr)
	if addr == "" { //Failure in resolving IP
		return
	}
	key = addr + "##" + strconv.Itoa(port)
	return
}

func getIPFromHost(host string) (addr string) {
	if host != "" {
		ipAddr, err := net.ResolveIPAddr("ip", host)
		if err != nil {
			return
		}
		addr = ipAddr.String()
	}
	return
}

//SnmpSessions returns a slice of SnmpSession monitored by this api.
func (api *SnmpAPI) SnmpSessions() []*SnmpSession {
	var sesList []*SnmpSession
	for ses := api.sessionList.Front(); ses != nil; ses = ses.Next() {
		s := ses.Value.(*SnmpSession)
		sesList = append(sesList, s)
	}

	return sesList
}

//RemoveSnmpSession removes this session (ses) from SnmpAPI's SnmpSessionList.
//SnmpAPI will stop monitoring this SnmpSession.
func (api *SnmpAPI) RemoveSnmpSession(ses *SnmpSession) {
	for session := api.sessionList.Front(); session != nil; session = session.Next() {
		val := (session).Value.(*SnmpSession)
		if val == ses {
			api.sessionList.Remove(session)
			break
		}
	}
}

//CheckTimeouts returns a slice of SnmpSession on this SnmpAPI which have pending/timed-out requests.
func (api *SnmpAPI) CheckTimeouts() []*SnmpSession {
	var sesList []*SnmpSession
	for ses := api.sessionList.Front(); ses != nil; ses = ses.Next() {
		s := ses.Value.(*SnmpSession)
		if l := s.TimeoutList(); l.Len() > 0 {
			sesList = append(sesList, s)
		}
	}

	return sesList
}

//Close closes this SnmpAPI and stop monitoring the SnmpSessions under it.
//It closes all the SnmpSessions monitored by it.
func (api *SnmpAPI) Close() {
	api.apiClosed = true
	//Close all the SnmpSessions
	for session := api.sessionList.Front(); session != nil; session = session.Next() {
		val := (session).Value.(*SnmpSession)
		val.Close() //Call the SnmpSession's close method
	}
	api.CloseDB() //Close the DB connection if any

	log.Debug("Succesfully closed the SnmpAPI and all the SnmpSessions monitored by it.")
}

//Monitor method adds this SnmpSession into its session list, which will monitor
//all the requests under this session for Timeouts and Retries.
func (api *SnmpAPI) monitor(ses *SnmpSession) {
	ses.api = api
	api.sessionList.PushBack(ses)
}

//Start monitoring the sessions
func (api *SnmpAPI) startMonitoring() {
	log.Info("Started monitoring the SnmpSessions.")
	//Loop over the SnmpSessions and Monitor the requests under each session for Timeout and Retries
	for !api.apiClosed {

		for session := api.sessionList.Front(); session != nil; session = session.Next() {
			val := (session).Value.(*SnmpSession)
			val.monitorRequests()
		}
		//Interval time before start monitoring sessionList
		time.Sleep(time.Millisecond * monitorDelay)
	}
	log.Debug("Out of monitoring SnmpSessions routine. SnmpAPI has been closed.")
}

const (
	monitorDelay                 = 100             //Delay before start monitoring list of SnmpSessions (in millisecond)
	snmpPort      int            = 161             //Default port
	snmpTrapPort  int            = 162             //Default trap port
	snmpVersion   consts.Version = consts.Version1 //Default SnmpVersion
	snmpCommunity string         = "public"        //Default Community string
)
