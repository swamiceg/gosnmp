/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package util

import (
	"fmt"
	"strings"

	"webnms/log"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

//USM Report OIDs
const (
	usmStatsUnsupportedSecLevels string = ".1.3.6.1.6.3.15.1.1.1.0"
	usmStatsNotInTimeWindows     string = ".1.3.6.1.6.3.15.1.1.2.0"
	usmStatsUnknownUserNames     string = ".1.3.6.1.6.3.15.1.1.3.0"
	usmStatsUnknownEngineIDs     string = ".1.3.6.1.6.3.15.1.1.4.0"
	usmStatsWrongDigests         string = ".1.3.6.1.6.3.15.1.1.5.0"
	usmStatsDecryptionErrors     string = ".1.3.6.1.6.3.15.1.1.6.0"
)

//Init_V3_LCD is a comprehensive routine that aids in an initialization of
//USM specific SNMPv3 LCD prior to further communication with the remote entity.
//
//It performs discovery with the remote entity using ProtocolOptions provided and fills the
//USMPeerEngineLCD with the desired parameters. If non-nil engineID value is passed, discovery will not be performed.
//
//It also creates and adds the USMSecureUser entry in the USMUserLCD after performing necessary password to key translation.
//If authentication is required for the user, it performs time-synchronization with the remote entity and updates the EB/ET values for the PeerEngine.
//In case of Time-Synchronization failure, user entry will not be created in USMUserLCD.
//
//It uses the session instance to perform all the operations. session should be in open state.
//
//This method also performs validation on the user based on the value of validateUser.
//		* For NoAuthNoPriv - Simple get-request will be sent to see if the user exist.
//		* For AuthPriv - Get-Request at AuthPriv security level will be sent to validate the PrivProtocol and PrivPassword.
//
//Returns error (of type 'InitV3Error') in the following possible cases:
//		* If ProtocolOptions is nil, username is empty, session is not open
//		* If failure in Discovery process
//		* If failure in Time-Synchronization due to Invalid UserName, Invalid AuthProtocol/AuthPassword, (or) if authentication is not needed for the user
//		* If failure in validating the user - User not exist in the remote entity, PrivProtocol/PrivPassword is wrong
func Init_V3_LCD(session *snmp.SnmpSession,
	protocolOptions transport.ProtocolOptions, //Required for obtaining RemoteHost/Port
	userName string, //Required for time-sync
	engineID []byte, //If nil, Discovery will performed
	authProtocol consts.AuthProtocol,
	authPassword string,
	privProtocol consts.PrivProtocol,
	privPassword string,
	validateUser bool,
) error {

	if session == nil {
		return newInitV3Error(consts.DiscoveryFailure, "SnmpSession instance is nil")
	}
	if session.API() == nil {
		return newInitV3Error(consts.DiscoveryFailure, "SnmpAPI instance is not set on SnmpSession")
	}

	var transport transport.ProtocolOptions
	if protocolOptions != nil {
		transport = protocolOptions
	} else {
		transport = session.ProtocolOptions()
	}

	if transport == nil {
		return newInitV3Error(consts.DiscoveryFailure, "ProtocolOptions instance is nil")
	}

	if strings.EqualFold(userName, "") {
		return newInitV3Error(consts.DiscoveryFailure, "UserName is empty")
	}

	var remoteEngine *usm.USMPeerEngine
	var err error

	//Discovery Process:
	if engineID == nil {
		remoteEngine, err = DiscoverHost(session, transport)
		if err != nil {
			//log.Debug("Error in EngineID discovery for the Remote Entity: %s. Error: %s. ", transport.SessionID(), err.Error() + "Engine entry not created.")
			return err
		}
		engineID = remoteEngine.AuthoritativeEngineID()
	} else {
		usmEngineLCD := session.API().USMPeerEngineLCD()

		var peer engine.SnmpPeerEngine
		if peer = usmEngineLCD.Engine(transport.RemoteHost(),
			transport.RemotePort(),
		); peer == nil {
			//Create new peer engine
			remoteEngine = usm.NewUSMPeerEngine(transport.RemoteHost(),
				transport.RemotePort(),
			)
			log.Info("Creating new engine entry for the entity: %s", transport.SessionID())
			remoteEngine.SetAuthoritativeEngineID(engineID)
			usmEngineLCD.AddEngine(remoteEngine)
		} else {
			remoteEngine = peer.(*usm.USMPeerEngine)
			remoteEngine.SetAuthoritativeEngineID(engineID)
			usmEngineLCD.AddEngine(remoteEngine) //Just update the entry
		}
	}

	//Time-Synchronization:
	if !strings.EqualFold(strings.TrimSpace(userName), "") { //Even if authProtocol is empty, we can perform validation for the user
		err = TimeSynchronize(session,
			transport,
			remoteEngine,
			userName,
			authProtocol,
			authPassword,
			privProtocol,
			privPassword,
			validateUser,
		)
		if err != nil {
			//log.Debug("Error in Time-Synchronization for the Remote Entity: %s. Error: %s. ", transport.SessionID(), err.Error() + "User entry not created.")
			return err
		}
	} else {
		//We are not performing time-sync, as no username and authentication protocol is supplied.
		return newInitV3Error(consts.TimeSyncFailure, "UserName is empty")
	}

	return nil
}

//DiscoverHost performs discovery with the remote entity using the address available in the ProtocolOptions.
//As per RFC 3414 - Section 4.
//On successful discovery, new PeerEngine entry will be created and added in the USMPeerEngineLCD.
//
//Even if the entry already exist for the remote entity with a valid EngineID, re-discovery will be performed.
//Returns error (of type 'InitV3Error') in case of failure in discovery process.
//
//Note: USMPeerEngine.DiscoverEngineID method can also be used directly to perform discovery.
func DiscoverHost(session *snmp.SnmpSession, protocolOptions transport.ProtocolOptions) (*usm.USMPeerEngine, error) {

	if session == nil {
		return nil, newInitV3Error(consts.DiscoveryFailure, "SnmpSession instance is nil")
	}

	var snmpSession *snmp.SnmpSession = new(snmp.SnmpSession)
	*snmpSession = *session
	if snmpSession.API() == nil {
		return nil, newInitV3Error(consts.DiscoveryFailure, "SnmpAPI instance is not set on SnmpSession")
	}

	var transport transport.ProtocolOptions
	if protocolOptions != nil {
		transport = protocolOptions
		snmpSession.SetProtocolOptions(protocolOptions)
	} else {
		transport = snmpSession.ProtocolOptions()
	}

	if transport == nil {
		return nil, newInitV3Error(consts.DiscoveryFailure, "ProtocolOptions instance is nil")
	}
	var remotePeer *usm.USMPeerEngine

	//Discovery Process
	usmEngineLCD := session.API().USMPeerEngineLCD()
	peer := usmEngineLCD.Engine(transport.RemoteHost(),
		transport.RemotePort(),
	)

	//var engineID []byte = nil
	if peer == nil {
		//First time we are performing discovery.
		remotePeer = usm.NewUSMPeerEngine(transport.RemoteHost(), //Creating New Peer
			transport.RemotePort(),
		)
		//Failure in creating remotePeer due to invalid address
		if remotePeer == nil {
			errStr := fmt.Sprintf("Invalid RemoteHost/RemotePort:'%s:%d'", transport.RemoteHost(), transport.RemotePort())
			log.Error("Discovery Failure: " + errStr)
			return nil, newInitV3Error(consts.DiscoveryFailure, errStr)
		}
		log.Info("Creating new engine entry for the entity: %s", transport.SessionID())
		usmEngineLCD.AddEngine(remotePeer)
	} else if (peer.AuthoritativeEngineID() != nil) &&
		(len(peer.AuthoritativeEngineID()) > 0) {
		//We already have Engine Entry for this remote entity. No need to perform discovery.
		//log.Println("We are performing re-discovery for this Remote Entity.")
		remotePeer = peer.(*usm.USMPeerEngine)
	}

	//Perform discovery on PeerEngine
	engineID := remotePeer.DiscoverEngineID(snmpSession) //This would definitely return us a new EngineID.
	if engineID == nil {
		usmEngineLCD.RemoveEngineByEntry(remotePeer)
		errStr := "Engine Entry not created for Entity: " + transport.SessionID()
		log.Error("Discovery Failure: " + errStr)
		return nil, newInitV3Error(consts.DiscoveryFailure, errStr)
	}

	//If EngineID is not created, it indicates the discovery failure. - Additional Check.
	/*if remotePeer.AuthoritativeEngineID() == nil || len(remotePeer.AuthoritativeEngineID()) == 0 {
		usmEngineLCD.RemoveEngineByEntry(remotePeer)
		errStr := "Discovery Failure: Engine Entry not created for Entity: " + transport.SessionID()
		log.Error(errStr)
		return nil, errors.New(errStr)
	}*/

	return remotePeer, nil
}

//TimeSynchorize achieves Time-Synchronization with the remote entity using the peerEngine instance provided.
//It creates and adds the USMSecureUser entry in the USMUserLCD after performing necessary password to key translation.
//
//If authentication is required for the user, it performs time-synchronization with the remote entity and updates the EB/ET values for the PeerEngine.
//In case of Time-Synchronization failure, user entry will not be created in USMUserLCD.
//
//This method also performs validation on the user based on the value of validateUser.
//		* For NoAuthNoPriv - Simple get-request will be sent to see if the user exist.
//		* For AuthPriv - Get-Request at AuthPriv security level will be sent to validate the PrivProtocol and PrivPassword.
//
//Note: USMSecureUser.TimeSynchronize method can also be used to perform time-synchronization with the remote entity.
func TimeSynchronize(session *snmp.SnmpSession,
	protocolOptions transport.ProtocolOptions, //Required for obtaining RemoteHost/Port
	peerEngine *usm.USMPeerEngine,
	userName string, //Required for time-sync
	authProtocol consts.AuthProtocol,
	authPassword string,
	privProtocol consts.PrivProtocol,
	privPassword string,
	validateUser bool,
) error {

	if session == nil {
		return newInitV3Error(consts.TimeSyncFailure, "SnmpSession instance is nil")
	}
	if session.API() == nil {
		return newInitV3Error(consts.TimeSyncFailure, "SnmpAPI instance is not set on SnmpSession")
	}

	var transport transport.ProtocolOptions
	if protocolOptions != nil {
		transport = protocolOptions
	} else {
		transport = session.ProtocolOptions()
	}

	if transport == nil {
		return newInitV3Error(consts.TimeSyncFailure, "ProtocolOptions instance is nil")
	}

	//EngineID check
	if peerEngine == nil ||
		peerEngine.AuthoritativeEngineID() == nil ||
		len(peerEngine.AuthoritativeEngineID()) == 0 { //EngineID is nil,

		return newInitV3Error(consts.TimeSyncFailure, "PeerEngine instance is nil/EngineID is empty")
	}

	if strings.EqualFold(userName, "") {
		return newInitV3Error(consts.TimeSyncFailure, "UserName is empty")
	}
	/*if authProtocol == "" || authProtocol == usm.NO_AUTH {
		return errors.New("Cannot Perform Time-Synchronization: AuthProtocol cannot be empty (or) usm.NO_AUTH.")
	}*/
	usmEngineLCD := session.API().USMPeerEngineLCD()
	if peer := usmEngineLCD.Engine(peerEngine.RemoteHost(),
		peerEngine.RemotePort(),
	); peer == nil {
		//Engine entry is not created in EngineLCD. Let's add one.
		usmEngineLCD.AddEngine(peerEngine)
	}

	engineID := peerEngine.AuthoritativeEngineID()

	var err error
	var update bool = false
	userLCD := session.API().USMUserLCD()

	oldAuthAlgo := consts.NO_AUTH
	oldPrivAlgo := consts.NO_PRIV
	var oldAuthKey []byte
	var oldPrivKey []byte

	//Check for the user in LCD.
	usmSecureUser, _ := userLCD.SecureUser(engineID, userName)
	if usmSecureUser != nil { //We have the user in our LCD.
		if usmSecureUser.AuthProtocol() != nil {
			oldAuthAlgo = consts.AuthProtocol(usmSecureUser.AuthProtocol().OID())
		}
		if usmSecureUser.PrivProtocol() != nil {
			oldPrivAlgo = consts.PrivProtocol(usmSecureUser.PrivProtocol().OID())
		}
		oldAuthKey = usmSecureUser.AuthKey()
		oldPrivKey = usmSecureUser.PrivKey()

		//There is already an user exist in USM LCD. Let's update the entry.
		err = userLCD.UpdateUser(engineID,
			usmSecureUser.UserName(),
			authProtocol,
			authPassword,
			privProtocol,
			privPassword,
		)
		if err != nil { //Error in updating the user.
			return newInitV3Error(consts.TimeSyncFailure, fmt.Sprintf("Failure in updating USM User (%s): %s", userName, err.Error()))
		}
		update = true
		log.Debug("Updated the USM user entry in LCD. EngineID: %s. User: %s.", string(engineID), userName)
	} else {
		//We should add the user to USM LCD.
		usmSecureUser, err = userLCD.AddUser(engineID, //AddUser method will check for the valid auth protocol/privprotocol
			userName,
			authProtocol,
			authPassword,
			privProtocol,
			privPassword,
		)
		if err != nil { //Failure in creating the user entry
			return newInitV3Error(consts.TimeSyncFailure, fmt.Sprintf("Failure in creating USM User (%s): %s", userName, err.Error()))
		}
		log.Debug("Created new USM user entry in LCD. EngineID: %s. User: %s.", string(engineID), userName)
	}

	//We have a valid USM Secure User now.
	if usmSecureUser.SecurityLevel() > security.NoAuthNoPriv {
		//Perform time-sync only if at least authentication is needed for this user.

		//Perform Time-Synchronization on USM Secure User
		err := usmSecureUser.TimeSynchronize(session, transport)
		if err != nil {
			//Time-Synchronization Error. Let's perform the User Related Handling.
			if !update { // We have created a new user, hence remove it.
				userLCD.RemoveUser(usmSecureUser.EngineID(),
					usmSecureUser.UserName(),
				)
			} else {
				//Updation should be reverted to old entries.
				//AuthProtocol and AuthPassword may be Wrong.
				usmSecureUser.SetAuthAlgorithm(oldAuthAlgo)
				usmSecureUser.SetPrivAlgorithm(oldPrivAlgo)
				usmSecureUser.SetAuthKey(oldAuthKey)
				usmSecureUser.SetPrivKey(oldPrivKey)

				userLCD.AddSecureUser(usmSecureUser.EngineID(), usmSecureUser)
			}
			log.Error("Time-Synchronization failure for the user '%s' with EngineID: %s. Remote Entity:: %s. %s", usmSecureUser.UserName(), usmSecureUser.EngineID(), transport.SessionID(), "User entry not created.")
			return newInitV3Error(consts.TimeSyncFailure, err.Error())
		}
	} else {
		//return fmt.Errorf("Cannot Perform Time-Synchronization: Time-Sync is not required for the User: %s. SecurityLevel: %d.", usmSecureUser.UserName(), usmSecureUser.SecurityLevel())
	}

	//We are performing this validate for PrivProtocol/PrivPassword - AuthPriv
	//UserValidation for NoAuthNoPriv
	if validateUser &&
		(usmSecureUser.SecurityLevel() == security.AuthPriv || usmSecureUser.SecurityLevel() == security.NoAuthNoPriv) {

		//Let's create simple GetRequest message with Auth-Priv Security Level
		mesg := msg.NewSnmpMessage()
		mesg.SetVersion(consts.Version3)
		mesg.SetCommand(consts.GetNextRequest)
		mesg.SetSecurityLevel(usmSecureUser.SecurityLevel())
		mesg.SetUserName(usmSecureUser.UserName()) //Should be a valid UserName
		mesg.SetProtocolOptions(transport)
		//If no varbinds attached, response is coming with AuthNoPriv Security Level
		mesg.AddNull(*snmpvar.NewSnmpOID(".1.3.6"))

		resp, err := session.SyncSend(mesg)
		if err != nil {
			//CleanUp - Validation failure occurred. Let's perform the User Related Handling.
			if !update { // We have created a new user, hence remove it.
				userLCD.RemoveUser(usmSecureUser.EngineID(),
					usmSecureUser.UserName(),
				)
			} else {
				//Updation should be reverted to old entries.
				//AuthProtocol and AuthPassword may be Wrong.
				usmSecureUser.SetAuthAlgorithm(oldAuthAlgo)
				usmSecureUser.SetPrivAlgorithm(oldPrivAlgo)
				usmSecureUser.SetAuthKey(oldAuthKey)
				usmSecureUser.SetPrivKey(oldPrivKey)

				userLCD.AddSecureUser(usmSecureUser.EngineID(), usmSecureUser)
			}

			//Validation Failure
			log.Error("Validation failure for the user '%s' with EngineID: %s. Remote Entity:: %s.", usmSecureUser.UserName(), usmSecureUser.EngineID(), transport.SessionID())
			return newInitV3Error(consts.ValidationFailure, err.Error())
		}

		if resp.Command() == consts.ReportMessage {
			if reportVarb := resp.VarBinds(); reportVarb != nil {
				//CleanUp - Validation failure occurred. Let's perform the User Related Handling.
				if !update { // We have created a new user, hence remove it.
					userLCD.RemoveUser(usmSecureUser.EngineID(),
						usmSecureUser.UserName(),
					)
				} else {
					//Updation should be reverted to old entries.
					//AuthProtocol and AuthPassword may be Wrong.
					usmSecureUser.SetAuthAlgorithm(oldAuthAlgo)
					usmSecureUser.SetPrivAlgorithm(oldPrivAlgo)
					usmSecureUser.SetAuthKey(oldAuthKey)
					usmSecureUser.SetPrivKey(oldPrivKey)

					userLCD.AddSecureUser(usmSecureUser.EngineID(), usmSecureUser)
				}

				reportOID := reportVarb[0].ObjectID().String()
				switch reportOID {
				case usmStatsDecryptionErrors: //This is the most possible case for Auth-Priv
					return newInitV3Error(consts.DecryptionError, fmt.Sprintf("Decryption Error: PrivProtocol(%s)/PrivPassword is wrong.\nUsername: %s.", usmSecureUser.PrivAlgorithm(), usmSecureUser.UserName()))
				case usmStatsUnknownUserNames: //This is the most possible case for NoAuth-NoPriv
					return newInitV3Error(consts.UnknownUserNameError, fmt.Sprintf("UnknownUserName Error: %s.", usmSecureUser.UserName()))
				case usmStatsUnsupportedSecLevels:
					return newInitV3Error(consts.UnSupportedSecurityLevelError, fmt.Sprintf("UnSupportedSecurityLevel Error: %s.\nUsername: %s.", usmSecureUser.SecurityLevel(), usmSecureUser.UserName()))
				case usmStatsNotInTimeWindows:
					return newInitV3Error(consts.NotInTimeWindowsError, fmt.Sprintf("NotInTimeWindows Error. Username: %s.", usmSecureUser.UserName()))
				case usmStatsUnknownEngineIDs:
					return newInitV3Error(consts.UnknownEngineIDError, fmt.Sprintf("UnknownEngineID Error: %v.\nUsername: %s.", usmSecureUser.EngineID(), usmSecureUser.UserName()))
				case usmStatsWrongDigests:
					return newInitV3Error(consts.WrongDigestsError, fmt.Sprintf("WrongDigests Error: AuthProtocol(%s)/AuthPassword is wrong.\nUsername: %s.", usmSecureUser.AuthAlgorithm(), usmSecureUser.UserName()))
				default:
					return newInitV3Error(consts.ValidationFailure, fmt.Sprintf("Unknown error. Username: %s.", usmSecureUser.UserName()))
				}
			}
		} else if resp.Command() == consts.GetResponse {
			log.Info("USM user entry successfully created in LCD. EngineID: %s. User: %s.", string(engineID), userName)
			return nil //We dont care about any error messages
		}

	}
	log.Info("USM user entry successfully created in LCD. EngineID: %s. User: %s.", string(engineID), userName)

	return nil
}

//PasswordToAuthKey translates the authentication password to localized authentication key based on the authentication protocol
//and EngineID provided.
//This utility function can be used when user wants to create the USM Secure user and set the authentication key manually.
//
//  Note: authProtocol should be MD5 or SHA1. Returns nil in case of any failure in conversion.
func PasswordToAuthKey(authProtocol consts.AuthProtocol, authPassword string, engineID []byte) []byte {

	var authAlgo usm.USMAuthAlgorithm
	switch authProtocol {
	case consts.MD5_AUTH:
		authAlgo = usm.NewMD5()
	case consts.SHA_AUTH:
		authAlgo = usm.NewSHA1()
	default:
		return nil
	}

	secretKey := authAlgo.PasswordToKey(authPassword)
	if secretKey != nil {
		return authAlgo.LocalizeAuthKey(secretKey, engineID)
	}

	return nil
}

//PasswordToPrivKey translates the privacy password to localized privacy key based on the authentication protocol,
//privacy protocol and EngineID provided.
//This utility function can be used when user wants to create the USM Secure user and set the privacy key manually.
//
//  Note: authProtocol should be MD5 or SHA1 and privProtocol should be DES, 3DES or AES-128/192/256.
//  Returns nil in case of any failure in conversion.
func PasswordToPrivKey(authProtocol consts.AuthProtocol, privPassword string, engineID []byte, privProtocol consts.PrivProtocol) []byte {
	var authAlgo usm.USMAuthAlgorithm
	var privAlgo usm.USMPrivAlgorithm

	switch authProtocol {
	case consts.MD5_AUTH:
		authAlgo = usm.NewMD5()
	case consts.SHA_AUTH:
		authAlgo = usm.NewSHA1()
	default:
		return nil
	}

	switch privProtocol {
	case consts.DES_PRIV:
		privAlgo = usm.NewDES(nil)
	case consts.TRIPLE_DES_PRIV:
		privAlgo = usm.NewTripleDES(nil)
	case consts.AES_128_PRIV:
		privAlgo = usm.NewAES128(nil)
	case consts.AES_192_PRIV:
		privAlgo = usm.NewAES192(nil)
	case consts.AES_256_PRIV:
		privAlgo = usm.NewAES256(nil)
	default:
		return nil
	}

	secretKey := authAlgo.PasswordToKey(privPassword)
	if secretKey != nil {
		privKey := authAlgo.LocalizePrivKey(secretKey, engineID, privAlgo.KeySize())
		if privKey == nil { //PrivAlgorithm's keysize might be greater than authalgorithm keysize.
			privKey = authAlgo.ExtendPrivKey(privPassword, engineID, privAlgo.KeySize())
		}

		return privKey
	}

	return nil
}

//SNMPv3 LCD initialization error struct.
//Implements error interface.
//
//All the utility functions in util package returns error of this type.
type InitV3Error struct {
	errorCode   int
	errorString string
}

func newInitV3Error(errorCode int, errorString string) *InitV3Error {
	return &InitV3Error{
		errorCode:   errorCode,
		errorString: errorString,
	}
}

//Returns error string.
func (i *InitV3Error) Error() string {
	errStr := ""
	switch i.errorCode {
	case consts.DiscoveryFailure:
		errStr += "Discovery Failure: "
	case consts.TimeSyncFailure:
		errStr += "Time-Sync Failure: "
	case consts.ValidationFailure:
		errStr += "Validation Failure: "
	}
	errStr += i.errorString
	if !strings.HasSuffix(errStr, ".") { //Add dot
		errStr += "."
	}

	return errStr
}

//Returns error code indicating the type of error.
//
//  -1 indicates Discovery failure
//  -2 indicates Time-sync failure
//  -3 indicates User validation failure
func (i *InitV3Error) ErrorCode() int {
	return i.errorCode
}
