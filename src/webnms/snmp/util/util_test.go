/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package util

import (
	"webnms/snmp"
	"webnms/snmp/consts"

	"strings"
	"testing"
)

var (
	remoteHost = "localhost"
	remotePort = 8001
)

func TestDiscoverHost(t *testing.T) {

	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)

	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	ses.Open()

	_, err := DiscoverHost(nil, udp)
	if err == nil {
		t.Error("DiscoverHost: Not throwing error when session instance is nil")
	}

	//First time discovery of remoteHost
	_, err = DiscoverHost(ses, udp)
	if err != nil {
		t.Error(err)
	}

	//Re-Discovery of remoteHost
	_, err = DiscoverHost(ses, udp)
	if err != nil {
		t.Error(err)
	} else {
		//There should be only one created in LCD.
		if len(api.USMPeerEngineLCD().Engines()) > 1 {
			t.Errorf("DiscoverHost: Invalid number of peers created in EngLCD. Expected: 1. Got: %d.", len(api.USMPeerEngineLCD().Engines()))
		}
	}

	//Discovery with Invalid host/port
	udp.SetRemoteHost("dsfdsf")
	udp.SetRemotePort(6566)
	ses.SetTimeout(500)
	_, err = DiscoverHost(ses, udp)
	if err == nil {
		t.Error("DiscoverHost: Not throwing error when remotehost/port is invalid.")
	} else {
		//There should be only one created in LCD.
		if len(api.USMPeerEngineLCD().Engines()) > 1 {
			t.Errorf("DiscoverHost: Invalid number of peers created in EngLCD. Expected: 1. Got: %d.", len(api.USMPeerEngineLCD().Engines()))
		}
	}

	//Discovery with Unknown host/port
	udp.SetRemoteHost("1.1.1.1")
	udp.SetRemotePort(6566)
	_, err = DiscoverHost(ses, udp)
	if err == nil {
		t.Error("DiscoverHost: Not throwing error when remotehost/port is unknown.")
	} else {
		//There should be only one created in LCD.
		if len(api.USMPeerEngineLCD().Engines()) > 1 {
			t.Errorf("DiscoverHost: Invalid number of peers created in EngLCD. Expected: 1. Got: %d.", len(api.USMPeerEngineLCD().Engines()))
		}
	}
	api.Close()
}

func TestTimeSynchronize(t *testing.T) {
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)

	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	ses.Open()

	remotePeer, err := DiscoverHost(ses, udp)
	if err != nil {
		t.Fatal(err)
	}

	//Valid case:
	err = TimeSynchronize(ses, udp, remotePeer, "noAuthUser", consts.NO_AUTH, "", consts.NO_PRIV, "", true)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 1 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 1. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Valid case: - Resynchronization
	err = TimeSynchronize(ses, udp, remotePeer, "noAuthUser", consts.NO_AUTH, "", consts.NO_PRIV, "", true)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 1 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 1. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Valid case - AuthNoPriv
	err = TimeSynchronize(ses, udp, remotePeer, "authUser", consts.MD5_AUTH, "authUser", consts.NO_PRIV, "", true)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 2 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 2. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Invalid case
	err = TimeSynchronize(ses, udp, remotePeer, "xyz", consts.NO_AUTH, "", consts.NO_PRIV, "", true)
	if err == nil {
		t.Error("TimeSynchronize:  Not throwing error when username is invalid.")
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 2 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 2. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Invalid case: At NoAuthNoPriv security level, if validation is disabled, it should create entry in LCD.
	err = TimeSynchronize(ses, udp, remotePeer, "xyz", consts.NO_AUTH, "", consts.NO_PRIV, "", false)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 3 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 3. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}
}

func TestInitV3LCD(t *testing.T) {
	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	ses.Open()

	//Valid case - Priv user entry should be present in the remote peer
	err := Init_V3_LCD(ses, udp, "privUser", nil, consts.MD5_AUTH, "privUser", consts.DES_PRIV, "privUser", true)
	if err != nil {
		t.Error(err)
	} else {
		//There should be only one created in LCD.
		if len(api.USMPeerEngineLCD().Engines()) != 1 {
			t.Errorf("DiscoverHost: Invalid number of peers created in EngLCD. Expected: 1. Got: %d.", len(api.USMPeerEngineLCD().Engines()))
		}
		if len(api.USMUserLCD().SecureUsers()) != 1 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 1. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	ses.SetProtocolOptions(udp)
	//Valid case: - Resynchronization
	err = Init_V3_LCD(ses, nil, "privUser", nil, consts.MD5_AUTH, "privUser", consts.DES_PRIV, "privUser", true)
	if err != nil {
		t.Error(err)
	} else {
		//There should be only one created in LCD.
		if len(api.USMPeerEngineLCD().Engines()) != 1 {
			t.Errorf("DiscoverHost: Invalid number of peers created in EngLCD. Expected: 1. Got: %d.", len(api.USMPeerEngineLCD().Engines()))
		}
		if len(api.USMUserLCD().SecureUsers()) != 1 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 1. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Valid case
	err = Init_V3_LCD(ses, udp, "authUser", nil, consts.MD5_AUTH, "authUser", consts.NO_PRIV, "", true)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 2 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 2. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Invalid case
	err = Init_V3_LCD(ses, udp, "xyz", nil, consts.NO_AUTH, "", consts.NO_PRIV, "", true)
	if err == nil {
		t.Error("TimeSynchronize:  Not throwing error when username is invalid.")
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 2 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 2. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Invalid case: At NoAuthNoPriv security level, if validation is disabled, it should create entry in LCD.
	err = Init_V3_LCD(ses, udp, "xyz", nil, consts.NO_AUTH, "", consts.NO_PRIV, "", false)
	if err != nil {
		t.Error(err)
	} else {
		if len(api.USMUserLCD().SecureUsers()) != 3 {
			t.Errorf("Time-Synchronize: Invalid no of user entries in user lcd. Expected: 3. Got: %d.", len(api.USMUserLCD().SecureUsers()))
		}
	}

	//Invalid case
	err = Init_V3_LCD(nil, udp, "xyz", nil, consts.NO_AUTH, "", consts.NO_PRIV, "", true)
	if err == nil {
		t.Error("TimeSynchronize:  Not throwing error when session is nil.")
	}

	//Invalid case: Wrong username
	err = Init_V3_LCD(ses, udp, "xyz", nil, consts.MD5_AUTH, "authUser", consts.NO_PRIV, "", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("UnknownUserName")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when username is wrong. Expected: UnknownUserName. Got: %s.", err.Error())
	}

	//Invalid case: Wrong authprotocol
	err = Init_V3_LCD(ses, udp, "authUser", nil, consts.SHA_AUTH, "authUser", consts.NO_PRIV, "", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("WrongDigests")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when authprotocol is wrong. Expected: WrongDigests. Got: %s.", err.Error())
	}
	//Invalid case: Wrong authpassword
	err = Init_V3_LCD(ses, udp, "authUser", nil, consts.MD5_AUTH, "authUser11", consts.NO_PRIV, "", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("WrongDigests")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when authpassword is wrong. Expected: WrongDigests. Got: %s.", err.Error())
	}
	//Invalid case: Wrong privprotocol
	err = Init_V3_LCD(ses, udp, "privUser", nil, consts.MD5_AUTH, "privUser", consts.TRIPLE_DES_PRIV, "privUser", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("Decryption")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when privprotocol is wrong. Expected: Decryption. Got: %s.", err.Error())
	}
	//Invalid case: Wrong privpassword
	err = Init_V3_LCD(ses, udp, "privUser", nil, consts.MD5_AUTH, "privUser", consts.DES_PRIV, "privUser11", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("Decryption")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when privpassword is wrong. Expected: Decryption. Got: %s.", err.Error())
	}

	//Invalid case: invalid security level
	err = Init_V3_LCD(ses, udp, "privUser", nil, consts.MD5_AUTH, "privUser", consts.NO_PRIV, "", true)
	if err != nil && !strings.Contains(strings.ToLower(err.Error()), strings.ToLower("UnSupportedSecurityLevel")) {
		t.Errorf("TimeSynchronize:  Not throwing expected error when security level is invalid. Expected: UnSupportedSecurityLevel. Got: %s.", err.Error())
	}

}
