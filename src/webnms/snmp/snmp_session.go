/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"bytes"
	"container/list"
	"encoding/asn1"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	//User defined libs
	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport/tcp"
	"webnms/snmp/engine/transport/udp"
	trans "webnms/snmp/engine/transport"
)

//SnmpSession is the basic communication type for performing any SNMP operations.
//Instantiate this type using NewSnmpSession(SnmpAPI) function and perform various communication operations.
//
//It helps to send Sychronous and Asynchronous request to the remote entity.
//
//  - Method 'SyncSend(SnmpMessage)' can be used to send Synchronous request to the server, which waits for the response
//    from the server till timeout.
//  - Method 'Send(SnmpMessage)' can be used to send aynchronous request to the desired host/port.
//    To receive the response, the SnmpClient interface should be implemented and registered using "AddSnmpClient()" method.
//It also provides various methods for performing effective SNMP Communication.
//
//SnmpSession is an implementation of engine.Dispatcher interface (part of SnmpEngine architecture).
//
//  Note:
//  * Each SnmpSession is associated with a SnmpAPI, thereby directly associated with single SnmpEngine instance.
//  * All the SnmpSessions under the SnmpAPI share the instance of the SnmpModels such as Message Processing models, Security models.
//  * Multiple SnmpSessions under a SnmpAPI is associated with a SnmpEngine, so it shares the same EngineID.
type SnmpSession struct { //Dispatcher Implementation
	protocolOptions trans.ProtocolOptions
	timeout         int
	retries         int
	api             *SnmpAPI
	transportModel  engine.SnmpTransportModel
	sessionOpen     bool
	//Queues
	snmpclient       //Struct for storing SnmpClients
	outgoingMsgCache *msgCacheStore
	incomingMsgCache *msgCacheStore
	*syncContainer             //Maps for storing incoming messages for Sync
	timeoutList      list.List //List for storing timed-out PDUs

	timeoutPolicy    TimeoutPolicy  //For calculating timeouts
	packetBufferSize int            //Buffer used for receiving packet from server
	asyncGroup       sync.WaitGroup //WaitGroup counter - to wait for async responses
	autoInform       bool           //To perform auto response for Inform request
	snmpCallBack     *SnmpCallBack
	callbackFlag     bool //Callback to be called from a separate thread
	//To set the default values in Session level
	version    consts.Version
	community  string
	remotePort int
	remoteHost string
	localPort  int
	localHost  string
	readBuffer int

	*sessionCounters
	snmpGrp      *SnmpGroup
	msgMaxSize   int32
	userName     string
	v3AuthEnable bool

	*snmpSubSystemImpl
	//ReqID Generator
	reqId    int32
	incReqID func() int32
}

//Struct for storing the SnmpClients for Asynchronous requests
type snmpclient struct {
	clients    *set               //Set of SnmpClients. Since containers/list is not efficient to push and retrieve data, we are using 'set' DS.
	clientMaps map[int]SnmpClient //Map to store SnmpClient and the corresponding ClientId
	clientSync sync.RWMutex
	clientId   int //UniqueId for each SnmpClient to perform callback
}

/*Sync Container*/
//This Queue is used only for synchronous communication
type syncContainer struct {
	/* We are maintaining a separate Queue for V1/V2C and V3. Since V3 msgs can be uniquely identified only by MsgID,
	   Since in case of Decryption Errors, incoming V3 Msgs RequestID doesnot match with outstanding request.
	*/
	v1syncQueue map[int32]*msg.SnmpMessage //RequestID is the key for this Queue (V1/V2C)
	v3syncQueue map[int32]*msg.SnmpMessage //MsgID is the key for this Queue (V3)
	v1syncQLock sync.RWMutex
	v3syncQLock sync.RWMutex
}

func initSyncContainer() *syncContainer {
	return &syncContainer{
		v1syncQueue: make(map[int32]*msg.SnmpMessage),
		v3syncQueue: make(map[int32]*msg.SnmpMessage),
	}
}

func (sc *syncContainer) addToSync(msg *msg.SnmpMessage) {
	if msg != nil {
		if msg.Version() == consts.Version3 {
			sc.v3syncQLock.Lock() //Write lock
			defer sc.v3syncQLock.Unlock()
			sc.v3syncQueue[msg.MsgID()] = msg
		} else {
			sc.v1syncQLock.Lock() //Write lock
			defer sc.v1syncQLock.Unlock()
			sc.v1syncQueue[msg.RequestID()] = msg
		}
	}
}

func (sc *syncContainer) getFromV1Sync(reqID int32) *msg.SnmpMessage {
	sc.v1syncQLock.RLock() //Read lock
	defer sc.v1syncQLock.RUnlock()
	return sc.v1syncQueue[reqID]
}

func (sc *syncContainer) getFromV3Sync(msgID int32) *msg.SnmpMessage {
	sc.v3syncQLock.RLock() //Read lock
	defer sc.v3syncQLock.RUnlock()
	return sc.v3syncQueue[msgID]
}

func (sc *syncContainer) removeFromV1Sync(reqID int32) *msg.SnmpMessage {
	sc.v1syncQLock.Lock() //Write lock
	defer sc.v1syncQLock.Unlock()
	if msg, ok := sc.v1syncQueue[reqID]; ok {
		delete(sc.v1syncQueue, reqID)
		return msg
	}
	return nil
}

func (sc *syncContainer) removeFromV3Sync(msgID int32) *msg.SnmpMessage {
	sc.v3syncQLock.Lock() //Write lock
	defer sc.v3syncQLock.Unlock()
	if msg, ok := sc.v3syncQueue[msgID]; ok {
		delete(sc.v3syncQueue, msgID)
		return msg
	}
	return nil
}

/*End of Sync container*/

var (
	incoming_cache_size     = 100 //Maximum number of responses in the queue
	timeout_queue_size  int = 100 //Maximum number of timeouts in the queue
)

//NewSnmpSession creates and returns new SnmpSession instance after setting the default values.
//SnmpAPI instance is passed in order to monitor the requests that happens throught this SnmpSession.
//
//User should create the SnmpSession instance using this function.
func NewSnmpSession(snmpAPI *SnmpAPI) *SnmpSession {
	session := new(SnmpSession)
	session.api = snmpAPI //Set the SnmpAPI on Session
	session.initSession() //Initialize default values for this Session

	return session
}

//To initialize default values for Session
func (s *SnmpSession) initSession() {
	//Initialize ReqID generator
	s.incReqID = s.incrementReqId()
	//This buffer will be used for receiving packet from server (Session.Open())
	if s.packetBufferSize == 0 {
		s.packetBufferSize = 65535
	}
	//Initialize timeoutPolicy to default
	if s.timeoutPolicy == nil {
		s.timeoutPolicy = new(ExponentialTimeOutPolicy)
	}
	s.autoInform = true //Sets Auto response for Inform request by default

	//Set default constants on Session
	s.remotePort = snmpPort
	s.version = snmpVersion
	s.community = snmpCommunity

	//Initialize SnmpClient's store
	s.clients = newSet() //Must initialize this SnmpClient Set
	s.clientMaps = make(map[int]SnmpClient)
	s.clientId = 1000

	//Initialise the request and response maps
	s.incomingMsgCache = initMsgCacheStore()
	s.outgoingMsgCache = initMsgCacheStore()
	s.syncContainer = initSyncContainer()

	s.sessionCounters = new(sessionCounters)
	s.snmpSubSystemImpl = newSubSystem(s.api.SnmpEngine())
	s.v3AuthEnable = true //Authentication enabled by default
}

//This is a dispatcher counters
//Defined as per RFC 3418 - snmp group
type sessionCounters struct {
	snmpInPktsCounter uint32 //Increment counter
	//snmpInASNParseErrsCounter     uint32 //Increment counter and discard incoming message
	snmpInBadVersionsCounter      uint32 //Increment counter and discard incoming message
	snmpUnknownPDUHandlersCounter uint32 //(This may not be required for us) //Increment counter and send report
}

//Returns the number of IN packets counter. SnmpInPkts is the total number of messages delivered to the SNMP
//entity from the transport service.
func (sc *sessionCounters) SnmpInPktsCounter() uint32 {
	return sc.snmpInPktsCounter
}

//Returns the SnmpInBadVersions counter. SnmpInBadVersions is the total number of SNMP messages
//which were delivered to the SNMP entity and were for an unsupported SNMP version.
func (sc *sessionCounters) SnmpInBadVersionsCounter() uint32 {
	return sc.snmpInBadVersionsCounter
}

//Returns the UnknownPDUHandlers counter. SnmpUnknownPDUHandlers is the total number of messages received which doesnot
//have PDUHandlers to process it.
func (sc *sessionCounters) SnmpUnknownPDUHandlersCounter() uint32 {
	return sc.snmpUnknownPDUHandlersCounter
}

func (sc *sessionCounters) incSnmpInPktsCounter() uint32 {
	sc.snmpInPktsCounter++
	return sc.snmpInPktsCounter
}

func (sc *sessionCounters) incSnmpInBadVersionsCounter() uint32 {
	sc.snmpInBadVersionsCounter++
	return sc.snmpInBadVersionsCounter
}

func (sc *sessionCounters) incSnmpUnknownPDUHandlersCounter() uint32 {
	sc.snmpUnknownPDUHandlersCounter++
	return sc.snmpUnknownPDUHandlersCounter
}

//Returns a pointer to SnmpAPI which is used for monitoring this session (or)
//returns nil if it is not set on this session.
func (s *SnmpSession) API() *SnmpAPI {
	return s.api
}

//NewUDPProtocolOptions returns new instance of udp.UDPProtocolOptions with default values.
func NewUDPProtocolOptions() *udp.UDPProtocolOptions {
	return new(udp.UDPProtocolOptions)
}

//NewTCPProtocolOptions returns new instance of tcp.TCPProtocolOptions with default values.
func NewTCPProtocolOptions() *tcp.TCPProtocolOptions {
	return new(tcp.TCPProtocolOptions)
}

//SetProtocolOptions sets this ProtocolOptions on Session Level, which will be used for all the communcation that happens with this Session.
//ProtocolOptions set on the SnmpMessage will override the ProtocolOptions set on this Session.
//
//Note: If protocoloptions is nil, it will not be set in the session.
func (s *SnmpSession) SetProtocolOptions(protocolOptions trans.ProtocolOptions) {
	if protocolOptions != nil {
		s.protocolOptions = protocolOptions
	}
}

//ProtocolOptions returns the ProtocolOptions (UDP/TCP) set on this Session.
//Returns nil when no protocoloption is set.
func (s *SnmpSession) ProtocolOptions() trans.ProtocolOptions {
	return s.protocolOptions
}

//SetTimeout sets the timeout value in 'milliseconds'. The timeout is the time to wait for the first response in milli-seconds, before attempting a retransmission.
//
//The timeout in the SnmpMessage overrides the timeout value in session.
func (s *SnmpSession) SetTimeout(timeout int) *SnmpSession {
	if timeout > 0 {
		s.timeout = timeout
	}
	return s
}

//Timeout returns the Timeout value set on this Session in 'milliseconds'.
//The timeout is the time to wait for the first response in milli-seconds, before attempting a retransmission.
func (s *SnmpSession) Timeout() int {
	return s.timeout
}

//SetRetries sets the number of retries before timeout.
//The timeout value grows exponentially for each retries. This exponential way of timeout policy can be changed by implementing TimeoutPolicy interface and defining your own 'calculateTimeout()' method.
//
//The retries in the SnmpMessage overrides the retries value in session.
func (s *SnmpSession) SetRetries(retries int) *SnmpSession {
	if retries >= 0 {
		s.retries = retries
	}
	return s
}

//Retries returns the number of retries for each request before Timeout, that is set on this Session.
func (s *SnmpSession) Retries() int {
	return s.retries
}

//AddSnmpClient subscribe for callbacks. This adds the SnmpClient interface implementation to this session, which will invoke the callback method on responses.
//
//Type defined by you should implement SnmpClient interface by implementing Callback()/Authenticate() methods, to send and receive messages asynchronously.
//The callback() method is automatically called in when a response arrives.
//
//All the clients added through this method will be invoked on the arrival of SnmpMessage from remote entity. Refer AddSnmpClientWithID method.
func (s *SnmpSession) AddSnmpClient(client SnmpClient) *SnmpSession {
	if client != nil {
		s.clients.Add(client)
	}
	return s
}

//AddSnmpClientWithID subscribe for callbacks. This adds the SnmpClient interface implementation to this session, which will invoke the callback method.
//
//Type defined by you should implement SnmpClient interface by implementing Callback()/Authenticate() methods to send and receive messages asynchronously.
//Callback() method is automatically called in when a response arrives.
//
//This method will return an ID which should be set on the SnmpMessage before making any request, so that only the particular SnmpClient's callback method will get invoked on the arrival of SnmpMessage.
func (s *SnmpSession) AddSnmpClientWithID(client SnmpClient) int {
	var clientId int
	if client != nil {
		s.clientId++
		clientId = s.clientId
		//Add to client maps - to invoke the specific SnmpClient based on clientId
		s.clientSync.Lock()
		defer s.clientSync.Unlock()
		s.clientMaps[clientId] = client
		s.clients.Add(client)
	}

	return clientId
}

//SnmpClients returns all the SnmpClients registered on this Session for callback as a slice.
func (s *SnmpSession) SnmpClients() []SnmpClient {
	var cliSlice []SnmpClient
	//Add clients from clientslist
	for _, i := range s.clients.List() { //Typecast and append
		cliSlice = append(cliSlice, i.(SnmpClient))
	}

	return cliSlice
}

//IsSnmpClientExist returns boolean value indicating whether the client exist (registered) in the SnmpSession s.
func (s *SnmpSession) IsSnmpClientExist(client SnmpClient) bool {
	return s.clients.Contains(client)
}

//SnmpClientsWithID returns the Map in which the clientID (int) and the SnmpClient are associated.
func (s *SnmpSession) SnmpClientsWithID() map[int]SnmpClient {
	return s.clientMaps
}

//RemoveAllSnmpClients removes all the SnmpClients that are registered for Callback on this Session.
//It also removes the SnmpClients registered with ID.
func (s *SnmpSession) RemoveAllSnmpClients() {
	s.clients.Clear()
	s.clientSync.Lock()
	defer s.clientSync.Unlock()
	//clear the client map
	for id := range s.clientMaps {
		delete(s.clientMaps, id)
	}
}

//RemoveSnmpClient removes the SnmpClient from this Session. Unsubscribe for callbacks.
func (s *SnmpSession) RemoveSnmpClient(client SnmpClient) {
	if client != nil {
		//Remove the SnmpClient from both client list and client map
		s.clients.Remove(client)
		s.clientSync.Lock()
		defer s.clientSync.Unlock()
		for id, cl := range s.clientMaps {
			if cl == client {
				//There maybe more than one SnmpClient added of same instance, hence remove all
				delete(s.clientMaps, id)
			}
		}
	}
}

//RemoveSnmpClientWithID removes the SnmpClient from this Session by passing clientID. Unsubscribe this client for callbacks.
func (s *SnmpSession) RemoveSnmpClientWithID(clientID int) {
	s.clientSync.Lock()
	defer s.clientSync.Unlock()
	delete(s.clientMaps, clientID)
}

//SetTimeoutPolicy sets the TimeoutPolicy on this Session. Sets the user's own implementation of TimeoutPolicy.
//
//By default ExponentialTimeOutPolicy will be used.
func (s *SnmpSession) SetTimeoutPolicy(timeoutPolicy TimeoutPolicy) {
	if timeoutPolicy != nil {
		s.timeoutPolicy = timeoutPolicy
	}
}

//RestoreToDefaultTimeoutPolicy restores the TimeoutPolicy to default 'ExponentialTimeoutPolicy' on this Session.
func (s *SnmpSession) RestoreToDefaultTimeoutPolicy() {
	s.timeoutPolicy = new(ExponentialTimeOutPolicy)
}

//SetPacketBufferSize sets the Packet Buffer Size which will be used for receiving the snmp packets on this Session.
//By default buffer size used is 65535.
func (s *SnmpSession) SetPacketBufferSize(size int) {
	if size > 0 {
		s.packetBufferSize = size
	}
}

//PacketBufferSize returns the Packet Buffer Size set on this Session. Default value is 65535.
func (s *SnmpSession) PacketBufferSize() int {
	return s.packetBufferSize
}

//Requests returns a slice of SnmpMessage that are in the request queue.
//Only the outgoing requests that didn't receive any response will be returned.
//
//Note: Timed out requests are available in timeout queue.
//Refer TimedOutRequests method.
func (s *SnmpSession) Requests() []msg.SnmpMessage {
	var ls []msg.SnmpMessage
	var mesg *msg.SnmpMessage
	for _, cache := range s.outgoingMsgCache.entries() { //Loop over all the outgoing messages
		mesg = cache.msg
		if mesg.IsConfirmed() {
			ls = append(ls, *mesg)
		}
	}

	return ls
}

//Responses returns a slice of SnmpMessage that are in the response queue.
//
//Only the incoming responses that are not yet processed (not delivered to Client) will be returned.
func (s *SnmpSession) Responses() []msg.SnmpMessage {
	var ls []msg.SnmpMessage
	var mesg *msg.SnmpMessage
	for _, cache := range s.incomingMsgCache.entries() {
		mesg = cache.msg

		if mesg.Command() == consts.GetResponse ||
			mesg.Command() == consts.ReportMessage {
			ls = append(ls, *mesg)
		}
	}

	return ls
}

//TimedOutRequests returns all the timed out requests on this Session.
//
//Returns a slice of timed out SnmpMessages if any or an empty slice if there are no timed out requests.
func (s *SnmpSession) TimedOutRequests() []msg.SnmpMessage {
	var ls []msg.SnmpMessage
	for timeout := s.timeoutList.Front(); timeout != nil; timeout = timeout.Next() {
		msg := timeout.Value.(*msg.SnmpMessage)
		ls = append(ls, *msg)
	}

	return ls
}

//TimeoutList returns the list of requests that are in timed out queue
func (s *SnmpSession) TimeoutList() (list list.List) {
	return s.timeoutList
}

//SetAutoInformResponse sets the automatic response flag for the Inform Request.
//If this flag is set to true, SNMP stack automatically sends a Get-Reponse message back to the sender.
//The default value is true.
func (s *SnmpSession) SetAutoInformResponse(set bool) {
	s.autoInform = set
}

//AutoInformResponse returns bool indicating whether automatic response for Inform request is enabled or not.
func (s *SnmpSession) AutoInformResponse() bool {
	return s.autoInform
}

//IsSessionOpen returns true if the session is open.
func (s *SnmpSession) IsSessionOpen() bool {
	return s.sessionOpen
}

//SetVersion sets the SNMP Version to be used for all the outgoing messages through this Session.
//Version in the SnmpMessage will override the version set in the Session.
//
//Default value is SNMPv1.
func (s *SnmpSession) SetVersion(ver consts.Version) *SnmpSession {
	s.version = ver
	return s
}

//Version returns the SNMP Version set on this SnmpSession.
func (s *SnmpSession) Version() consts.Version {
	return s.version
}

//SetCommunity sets the SNMP Community string to be used for all the outgoing messages through this Session.
//Community in the SnmpMessage will override the community set in the Session.
func (s *SnmpSession) SetCommunity(community string) *SnmpSession {
	s.community = community //Can set even empty string
	return s
}

//Community returns the community string set on this SnmpSession.
func (s *SnmpSession) Community() string {
	return s.community
}

//Sets this to true if the user wants the callback to be called from a seperate goroutine.
//Recommended if you're doing any serious work in the callback, especially sending new requests.
//
//The SnmpAPI provides two ways of processing received messages, using the callback method. When the CallbackRoutine is set true, the callback method is invoked from a separate goroutine as the response arrives.
//If it is set false, then callback method is invoked from the same goroutine as the receiver routine and subsequent responses can be received only, if the user returns from the callback method.
//You can set the value of CallbackRoutine based on, whether you want callback's to be invoked from a separate routine or from the SnmpSession receiver routine itself.
//
//	 The performance of the receiver goroutine in receiving responses or
//	 traps is little bit poorer,If we invoke the callback from a separate goroutine.
//	 In order to be called back when a response is received, applications should implement the SnmpClient interface and
//	 register with the SnmpSession, using the AddSnmpClient() method.
func (s *SnmpSession) SetCallbackRoutine(cbFlag bool) {
	s.callbackFlag = cbFlag
}

//RegisterTransportModel registers a new Transport Model (TM) with the Transport subsystem.
//This transport model can be used as a underlying transport protocol for any communication with the server.
//
//Refer engine.SnmpTransportModel interface in transport package for more information.
//
//  Note:
//  1. This method should be used only if you are using transport protocols (or) transport models other than UDP/TCP.
//  2. Equivalent ProtocolOptions should be used with the session in order to use this transport model.
func (s *SnmpSession) RegisterTransportModel(transportModel engine.SnmpTransportModel) {
	if transportModel != nil {
		eng := s.SnmpEngine()
		tpSS := eng.TransportSubSystem()
		tpSS.AddModel(transportModel)
	}
}

//SnmpGroup returns the snmp group instance which holds all the counters
//specific to this SnmpSession.
//For each LocalHost/LocalPort combination SnmpGroup instance is associated.
//
//Use SnmpAPI's SnmpGroup(localhost, localport) method to get the group instance associated with each host/port combination.
func (s *SnmpSession) SnmpGroup() *SnmpGroup {
	return s.snmpGrp
}

//Open opens a new SNMP Session which can be used for communicating with SNMP peer.
//This method also spawn the receiver goroutine which waits for the incoming packets.
//
//Opens a connection using the ProtocolOptions set on s, else it will use UDP ProtocolOptions by default.
//Returns error in case of failure in opening the session.
//
//  Note:
//  * This method will instantiate a new transport model through which packets are sent and received.
//  * In case custom transport protocol is used (other than UDP/TCP), it should be registered with session using RegisterTransportModel method.
func (s *SnmpSession) Open() error {

	if s.sessionOpen {
		return errors.New("Session is already open. Cannot reopen the Session.")
	}

	//Set UDP by default
	if s.protocolOptions == nil {
		//Setting default values - using udp by default
		//udpOpts.LocalHost = s.LocalHost
		udpOpts := new(udp.UDPProtocolOptions)
		udpOpts.SetLocalPort(s.localPort)
		udpOpts.SetRemoteHost(s.remoteHost)
		udpOpts.SetRemotePort(s.remotePort)
		s.protocolOptions = udpOpts
	}

	//Initialize SnmpTransportModel
	if s.transportModel = s.getTransportModel(); s.transportModel == nil {
		log.Error("Failure in Initializing the Transport Provider.")
		return errors.New("Failure in Initializing the Transport Provider. Refer RegisterTransportModel method in case you are using custom transport protocol.")
	}

	//Open the transport provider - It establishes the connection
	if errOpen := s.transportModel.Open(s.protocolOptions); errOpen != nil { //Set the params on ProtocolOptions
		logStr := "Error in opening Session: " + errOpen.Error()
		log.Error(logStr)
		return errors.New(logStr)
	}

	s.sessionOpen = true //Session is open now
	log.Debug("Session connection is open.")

	//Initialize the SnmpGroup to manage the counters specific to this Dispatcher (SnmpSession)
	s.snmpGrp = new(SnmpGroup)
	s.api.addSnmpGroup(s.protocolOptions.LocalHost(), s.protocolOptions.LocalPort(), s.snmpGrp)
	if s.api != nil {
		s.api.monitor(s) //Command SnmpAPI to start monitoring this SnmpSession
	}
	//Set the Connection params in Session
	switch s.protocolOptions.(type) {
	case *udp.UDPProtocolOptions:
		udpOpts := s.protocolOptions.(*udp.UDPProtocolOptions)
		s.readBuffer = udpOpts.ReadBufferSize()
		s.localHost = udpOpts.LocalHost()
		s.localPort = udpOpts.LocalPort()
	case *tcp.TCPProtocolOptions:
		tcpOpts := s.protocolOptions.(*tcp.TCPProtocolOptions)
		s.readBuffer = tcpOpts.ReadBufferSize()
		s.localHost = tcpOpts.LocalHost()
		s.localPort = tcpOpts.LocalPort()
	default:
		//Can set only general params.
		s.localHost = s.protocolOptions.LocalHost()
		s.localPort = s.protocolOptions.LocalPort()
	}

	go s.receiverRoutine() //Start the receiver go routine - Wait for packets from Agent

	return nil
}

//Return the SnmpTransportModel based on ProtocolOptions
func (s *SnmpSession) getTransportModel() engine.SnmpTransportModel {
	//Transport Protocols provided by default - UDP/TCP

	//Get the transport subsystem instance
	modelID := s.protocolOptions.ID()
	tpSS := s.SnmpEngine().TransportSubSystem()

	transportModel := tpSS.Model(modelID)
	if transportModel != nil {
		//Transport model is available for this ModelID.
		if tm, ok := transportModel.(engine.SnmpTransportModel); ok { //Check if this model is of type SnmpTransportModel
			//let's create a new instance of Transport Model
			tmType := reflect.TypeOf(tm)
			if tmType.Kind() == reflect.Ptr {
				tmType = tmType.Elem()
			}
			//Create new instance of TM.
			//New instance is needed, because we need a dedicated Transport Model (UDP/TCP) for each session and cannot reuse the same across different session.
			tmVal := reflect.New(tmType)
			tmInterface := tmVal.Interface()
			if transModel, ok := tmInterface.(engine.SnmpTransportModel); ok {
				transModel.SetTransportSubSystem(tpSS)
				return transModel //Successful case, return the instance of TM.
			}
		}
	}

	return nil
}

//Receiver GoRoutine which waits for incoming packets from the server
func (s *SnmpSession) receiverRoutine() {

	var logStr string
	var receiveBytes []byte
	log.Debug("started receiver routine to received snmp msgs.")
	//Loop until the Session gets closed
	for s.sessionOpen {
		//Create SnmpTransportPacket to read the incoming packet
		receiveBytes = make([]byte, s.packetBufferSize) //By default packetBufferSize is set to 65535
		incomingPacket := new(trans.SnmpTransportPacket)
		incomingPacket.ProtocolOptions = s.protocolOptions.Copy()
		incomingPacket.ProtocolData = receiveBytes

		length, err := s.transportModel.Read(incomingPacket)
		if err != nil {
			//log.Println(errR.Error())
			log.Error(err.Error() + ". Stopping receiver routine.")
			break
		}

		if !s.sessionOpen {
			log.Debug("End of receiver routine. Session has been closed.") //Log the end of receiver routine
			break                                                          //No more processing on received packet
		}
		//fmt.Println("IC Data::\n", printOctets(incomingPacket.ProtocolData[:length], length))
		//Process the received data
		recProtocolOptions := incomingPacket.ProtocolOptions
		var recEncodedData []byte = make([]byte, length)
		copy(recEncodedData, incomingPacket.ProtocolData[:length])

		//Increment In Packets counter
		s.incSnmpInPktsCounter()
		s.snmpGrp.IncrSnmpInPkts()

		//Let's check for the version
		version := decodeVersion(recEncodedData)

		//Message Processing SubSystem instance
		mps := s.api.SnmpEngine().MsgProcessingSubSystem()
		if version == -1 { //Error in determing the version
			mps.IncSnmpInASNParseErrsCounter(consts.Version1) //Let's use verison1
			s.snmpGrp.IncrSnmpInASNParseErrs()
			logStr = "Unable to determine SNMP version from PDU received. Dropping the msg. RemoteAddress:: " + recProtocolOptions.SessionID()
			log.Info(logStr)
			continue //No further processing of received message
		} else if (version != consts.Version1) &&
			(version != consts.Version2C) &&
			(version != consts.Version3) {
			//Unsupported SNMP Version - We support only V1/V2C/V3 as of now
			logStr = "Msg with Invalid SNMP version received. Dropping the msg. RemoteAddress:: " + recProtocolOptions.SessionID()
			log.Info(logStr)
			s.incSnmpInBadVersionsCounter()
			s.snmpGrp.IncrSnmpInBadVersions()
			continue //No further processing of received message
		}

		if s.api != nil && s.api.debug {
			s.printPacketBytes(recProtocolOptions, recEncodedData)
		}

		//Transport cache could have been created by transport layer.
		//It should be passed onto MsgProcessing SubSystem for processing.
		tmStateReference := incomingPacket.TmStateReference

		var receivedMsg *msg.SnmpMessage
		//Pass the message to Message Processing SubSystem for processing
		_, receivedMsg, stateReference, statusInformation := mps.PrepareDataElements(version,
			s, //This is a dispatcher
			recProtocolOptions,
			recEncodedData,
			length,
			tmStateReference, //Transport cache
		)
		//fmt.Println(receivedMsg, stateReference, statusInformation)
		//Error returned by MPM
		if statusInformation != nil { //There maybe any authentication/decryption failure
			//Perform error check
			err := strings.ToLower(statusInformation.Error())
			if strings.Contains(err, "asn1") { //Decode error
				s.snmpGrp.IncrSnmpInASNParseErrs()
			} else if strings.Contains(err, "unsupported security model") { //Unsupported sec model
				//s.snmpGrp.IncrSnmp
			} else if strings.Contains(err, "invalid") {
				//s.snmpGrp.IncrSnmp
			}
			if receivedMsg == nil {
				//Log the error
				logStr = statusInformation.Error()
				log.Info(logStr)
				if s.clients.Len() > 0 { //Inform the SnmpClients
					for _, client := range s.clients.List() {
						client.(SnmpClient).DebugStr(logStr)
					}
				} else {
					fmt.Fprintln(os.Stderr, logStr)
				}
				continue //Discard the message
			} else {
				logStr = "Authentication Failure on received msg: " + getMsgDebugStr(*receivedMsg) + "\n" + statusInformation.Error()
				log.Info(logStr)
				if s.clients.Len() > 0 { //Inform the SnmpClients
					for _, client := range s.clients.List() {
						client.(SnmpClient).DebugStr(logStr)
					}
				} else {
					fmt.Fprintln(os.Stderr, logStr)
				}
				receivedMsg.SetAuthenticationFailure(true) //UnAuthenticated Message
			}
		}
		//Process the non-nil SnmpMessage, this can be UnAuthenticated Message - V2Trap.

		//Set the ProtocolOptions and Byte array in the responseMsg
		receivedMsg.SetProtocolOptions(recProtocolOptions)
		receivedMsg.SetProtocolData(incomingPacket.ProtocolData[:length])

		//Set the params for incoming response/report
		if receivedMsg.Command() == consts.GetResponse || receivedMsg.Command() == consts.ReportMessage {
			dispStateReference := s.outgoingMsgCache.getEntry(receivedMsg.RequestID())
			if dispStateReference != nil {
				setParamsOnResponsePDU(dispStateReference.msg, receivedMsg)
			}
		}
		/*if s.api != nil && s.api.debug {
			s.printReceivedPacket(*receivedMsg) //Print the received packet
		}*/

		//Handle StateReference
		cache := newDispatcherStateReference(receivedMsg)
		if stateReference != nil { //This could be an incoming request message. Add this reference to incomingMsg cache
			cache.setStateReference(stateReference) //Let's store the stateReference, using which we can send the response
		}

		//Add the received pdu to cache
		s.incomingMsgCache.addEntry(cache) //Let's add all the incoming messages

		//Check for SnmpCallback
		if s.callbackFlag {
			if s.snmpCallBack == nil {
				s.snmpCallBack = newSnmpCallBack(s) //Start a new Callback routine once
			}
			s.snmpCallBack.addToResponseMaps(receivedMsg) //Add to the response queue of SnmpCallBack
			continue                                      //No further processing in the current goroutine
		}

		s.processReceivedPacket(receivedMsg) //Process the receivedPacket
	}

	log.Debug("End of receiver routine. SnmpSession will no longer receive packets.")
}

func decodeVersion(enc1Bytes []byte) consts.Version {
	type rawVersion struct {
		Version   int
		OtherData asn1.RawValue
	}

	versionStruct := new(rawVersion)

	//Get the version by decoding the whole message once
	if _, wholeErr := asn1.Unmarshal(enc1Bytes, versionStruct); wholeErr != nil {
		return -1
	}

	return consts.Version(versionStruct.Version)
}

//####Received Packet Processing####//

//Process the received packet. Enqueue the response for Sync requests (or) Notify the SnmpClients for Async requests.
func (s *SnmpSession) processReceivedPacket(receivedMsg *msg.SnmpMessage) {
	//Increment the counters for incoming message
	if s.snmpGrp != nil {
		s.snmpGrp.ProcessInPkts(*receivedMsg)
	}
	//	if s.api != nil && s.api.debug {
	//			printReceivedPacket(*receivedMsg) //Print the received packet
	//	}
	//If it is Response/Internal class, We should check the corresponding outstanding request in Queue

	//If it is Incoming Request (Confirmed Class) - Get/Get-Next/Set/Get-Bulk/Set/Inform,
	//We should Enqueue the message and pass it to the app/clients

	//If auto-inform set to true, send response and pass the inform to client

	//If Notification TrapV1/TrapV2c, then pass it to app/client
	if (receivedMsg.Command() == consts.GetResponse) ||
		(receivedMsg.Command() == consts.ReportMessage) {
		//RequestID should match for even SNMPV3. So we are checking with the RequestID.
		dispStateReference := s.outgoingMsgCache.removeEntry(receivedMsg.RequestID())
		//fmt.Println("dispStateRef: ", dispStateReference)
		if dispStateReference == nil {
			//log.Println("Corresponding request is not found for the response msg with RequestID:", responseMsg.requestID)
			s.incomingMsgCache.removeEntry(receivedMsg.RequestID())
			logStr := "Received Response/Report has no relavant outstanding requests. Dropping the msg: " + getMsgDebugStr(*receivedMsg)
			log.Info(logStr)
			return //Discard the packet. Can be spoofing.
		} else {
			setParamsOnResponsePDU(dispStateReference.msg, receivedMsg) //Additional
			if receivedMsg.Sync() {
				//If someone expecting the response, pass it to the sync queue
				s.addToSync(receivedMsg)
			}
		}
	}
	s.sendInformResponse(*receivedMsg) //Send the response for Inform request

	//Call the desired callback method of the client for aynchronous request
	//Incoming Requests/Notification should be pushed to the SnmpClients, So that they can send response back.
	if !receivedMsg.Sync() ||
		receivedMsg.IsConfirmed() ||
		isTrapRequest(*receivedMsg) {

		s.notifySnmpClient(receivedMsg)
	}

	return
}

//Notify the SnmpClient on receiving packet (or) on time out
func (s *SnmpSession) notifySnmpClient(msg *msg.SnmpMessage) {
	msgCopy := msg.Copy()
	//If ClientId is set on Outgoing SnmpMessage, call the desired SnmpClient
	if msg.ClientID() != 0 {
		s.clientSync.RLock()
		//Call the desired SnmpClient with the clientId found in clientMaps
		if client := s.clientMaps[msg.ClientID()]; client != nil {
			if client.Authenticate(msgCopy, s.community) { //Authenticate received msg
				client.Callback(msgCopy, msg.RequestID())
			} else {
				//log
				logStr := "Received msg is not authenticated by SnmpClient. Hence dropping it. Msg:: " + getMsgDebugStr(*msg)
				log.Info(logStr)
			}
			s.incomingMsgCache.removeEntry(msgCopy.RequestID()) //Informed all the clients, so dequeue it from incoming msg cache
		}
		s.clientSync.RUnlock()
	} else if s.clients.Len() > 0 {
		//Call all the SnmpClients found in the clients list
		for _, client := range s.clients.List() {
			cb := client.(SnmpClient)
			if cb.Authenticate(msgCopy, s.community) { //Authenticate received msg
				cb.Callback(msgCopy, msgCopy.RequestID())
			} else {
				logStr := "Received msg is not authenticated by SnmpClient. Hence dropping it. Msg:: " + getMsgDebugStr(*msg)
				log.Info(logStr)
			}
		}
		s.incomingMsgCache.removeEntry(msgCopy.RequestID()) //Informed all the clients, so dequeue it from incoming msg cache
	} else {
		//Check the incomingMsgCache for overflow
		if len(s.incomingMsgCache.entries()) >= incoming_cache_size {
			for id, _ := range s.incomingMsgCache.entries() {
				if msgCopy.RequestID() != id {
					s.incomingMsgCache.removeEntry(id)
					break
				}
			}
		}
		logStr := "No clients exist to pickup the incoming msg. SnmpMessage is put into the IncomingMsg Queue. Msg:: " + getMsgDebugStr(*msg)
		log.Info(logStr)
	}

	//If it is an Asynchronous request and unconfirmed message we should signal
	if (msgCopy.Command() == consts.GetResponse || msgCopy.Command() == consts.ReportMessage) && !msgCopy.Sync() {
		s.signalDone() //Signal the arrival of packet/request timeout for Async requests
	}
}

//To send timeout messages to SnmpClients
func (s *SnmpSession) notifyTimeOut(timedOutReq *msg.SnmpMessage) {
	//If ClientId is set on Outgoing SnmpMessage, then call the desired SnmpClient for Timeout
	if timedOutReq.ClientID() != 0 {
		s.clientSync.RLock()
		//Call the desired SnmpClient with the clientId found in clientMaps
		if client := s.clientMaps[timedOutReq.ClientID()]; client != nil {
			client.Callback(nil, timedOutReq.RequestID()) //Inform client with null PDU
		}
		s.clientSync.RUnlock()
	} else if s.clients.Len() > 0 {
		//Call all the SnmpClients found in the clients list
		for _, client := range s.clients.List() {
			client.(SnmpClient).Callback(nil, timedOutReq.RequestID()) //Inform clients with null PDU
		}
	}

	//We're done waiting for this Async Request.
	if !timedOutReq.Sync() {
		s.signalDone() //Signal the timeout for Async requests
	}
}

//Signal the arrival of async packet - decrement WaitGroup counter
func (s *SnmpSession) signalDone() {
	//Recovery is included as safety measure
	defer func() {
		if r := recover(); r != nil {
			//s.asyncGroup.Add(1)
			log.Fatal("Recovered from waitgroup counter panic. Something went wrong with Async Wait Group signal.")
		}
	}()

	s.asyncGroup.Done()
}

//Copy the desired params from RequestPDU to ResponsePDU
func setParamsOnResponsePDU(reqMsg *msg.SnmpMessage, respMsg *msg.SnmpMessage) {
	//Todo: Need to set various params on responsePDU
	respMsg.SetClientID(reqMsg.ClientID())
	respMsg.SetSync(reqMsg.Sync())
}

//Checks whether the incoming request is Inform or not
func isInformRequest(msg msg.SnmpMessage) (inform bool) {
	if msg.Command() == consts.InformRequest {
		inform = true
	}
	return
}

//Checks whether the incoming request is Trap or not
func isTrapRequest(msg msg.SnmpMessage) (trap bool) {
	if msg.Command() == consts.TrapRequest || msg.Command() == consts.Trap2Request {
		trap = true
	}
	return
}

//Send back the response for Inform Request
func (s *SnmpSession) sendInformResponse(msg msg.SnmpMessage) {

	//Send the automatic response for Inform request
	if isInformRequest(msg) && s.autoInform {
		//log.Println("Auto response for Inform request: ", s.autoInform) //log the auto inform response
		msg.SetCommand(consts.GetResponse)
		msg.SetErrorStatus(0)
		msg.SetErrorIndex(0)

		secModel := s.getSecurityModel(msg)
		msg.SetMsgSecurityModel(secModel) //USM by default for SNMPv3
		cache := s.incomingMsgCache.getEntry(msg.RequestID())

		if s.api != nil && s.api.Debug() {
			s.debugPrints("\nAuto response for Inform request: ", log.ALL, msg.Sync(), msg.ClientID())
		}
		if err := s.ReturnResponseMsg(msg, cache.stateReference); err != nil {
			//log failure inform response
			//fmt.Println("Error in sending the inform response. Inform Request:: %s. Error: %s.", getMsgDebugStr(*msgCopy), err.Error())
			log.Fatal("Error in sending the inform response. Inform Request:: %s. Error: %s.", getMsgDebugStr(msg), err.Error())
		}
	}
}

//####End - Received Packet Processing####//

//SyncSend sends SnmpMessage synchronously and returns pointer to response SnmpMessage and error in case of any failure.
//This is a synchronous request, returns after receiving SNMP response, or time out.
//
//  Returns error in the following conditions:
//  * Socket send error.
//  * If the command is not set on the reqMsg.
//  * If this method is called even before the open method is called.
//  * If some encode error occurs.
func (s *SnmpSession) SyncSend(reqMsg msg.SnmpMessage) (*msg.SnmpMessage, error) {

	//Check for sesionFlag
	if !s.sessionOpen {
		return nil, errors.New("Session is not open. Call Open() method on Session.")
	}

	reqMsg.SetSync(true)
	s.setTimeoutRetries(&reqMsg) //Set Timeouts and Retries

	//Calculate the time expiry using timeoutpolicy (in MilliSeconds)
	var timeExpire int
	timeExpire = currentTimeInMillis() + (s.getTimeExpiresForSyncSend(reqMsg.Retries(), reqMsg.Timeout()))

	//Send the PDU
	uniqueID, err := s.Send(reqMsg)
	if err != nil {
		return nil, err
	}

	//Wait for response - Check the ResponseList till timeExpire
	responseMsg, err := s.checkResponseList(reqMsg, uniqueID, timeExpire)
	if err != nil {
		logStr := "SyncSend failure for the msg:: " + getMsgDebugStr(reqMsg)
		log.Debug(logStr)
		return nil, err
	}

	return responseMsg, nil
}

//Send sends SnmpMessage on this session after encoding the reqMsg.
//This is an asynchronous request, returns the RequestID/MsgID of the SnmpMessage (based on version) and an error in case of any failure.
//
//SnmpClient should be implemented and registered with SnmpSession in order to receive the response messages.
//
//  Returns error in the following conditions:
//  * Socket send error.
//  * If the command is not set on the reqMsg.
//  * If this method is called even before the open method is called.
//  * If some encode error occurs.
func (s *SnmpSession) Send(reqMsg msg.SnmpMessage) (int32, error) {

	//Check for sesionFlag
	if !s.sessionOpen {
		return -1, errors.New("Session is not open. Call Open() method on SnmpSession.")
	}
	if s.transportModel == nil {
		return -1, errors.New("Cannot send the PDU. SnmpTransportModel is not set on the Session.")
	}
	if s.api == nil { //To check whether the api.Monitor() method is called
		return -1, errors.New("SnmpAPI is not set on the Session. SnmpSession instance may not have been created using NewSnmpSession(SnmpAPI) method.")
	}
	s.setUniqueRequestId(&reqMsg) //Set a Unique RequestId on the PDU	starting from 1
	setEnqueueOnPDU(&reqMsg)      //To avoid enqueing the resending PDUs

	if !reqMsg.Sync() { //This check is done to avoid repetition
		s.setTimeoutRetries(&reqMsg) //Set Timeout and Retries if it is not set already (in SyncSend)
		s.asyncGroup.Add(1)          //Add Async request to the asyncGroup (WaitGroup) - For Session.Wait()
	}
	s.setParamsOnMsg(&reqMsg)
	uniqueID, errSend := s.sendData(&reqMsg)

	if errSend != nil { //Perform cleanup on failure
		if reqMsg.Enqueue() {
			s.outgoingMsgCache.removeEntry(reqMsg.RequestID()) //Message is not sent, hence remove it from requestMaps.
		}
		if !reqMsg.Sync() {
			s.signalDone()
		}
		return -1, errSend
	}
	//fmt.Println("Send::", reqMsg.RequestID(), reqMsg.MsgID(), reqMsg.GetID())
	//uniqueID is RequestID in case of SNMPV1/V2C and MsgID in case of SNMPV3
	return uniqueID, nil
}

//Send the packet and enqueue it (only for original send)
//reqMsg should have all the required params filled by now, this method will not perform any check.
func (s *SnmpSession) sendData(reqMsg *msg.SnmpMessage) (int32, error) {

	var err error
	var encodedData []byte
	var msgID int32 = -1
	//Set the ProtocolOptions on the outgoing packet
	outgoingPacket := new(trans.SnmpTransportPacket)
	if reqMsg.ProtocolOptions() != nil {
		//Set the PDU level ProtocolOptions
		outgoingPacket.ProtocolOptions = reqMsg.ProtocolOptions()
	} else {
		//Set the Session level ProtocolOptions
		outgoingPacket.ProtocolOptions = s.protocolOptions
		reqMsg.SetProtocolOptions(s.protocolOptions)
	}
	//If remote port is not set on ProtocolOptions, use 161 by default
	if outgoingPacket.ProtocolOptions.RemotePort() <= 0 {
		outgoingPacket.ProtocolOptions.SetRemotePort(s.remotePort)
	}

	msgID, encodedData, tmStateReference, err := s.processOutgoingMessage(reqMsg)
	if err != nil {
		log.Debug("Error in processing the outgoing msg. " + err.Error())
		return msgID, err
	}

	outgoingPacket.ProtocolData = encodedData
	//Transport cache could have been created by any transport-aware security model.
	//It should be passed onto transport layer for security processing.
	outgoingPacket.TmStateReference = tmStateReference

	//Set the TimeExpiry on the PDU to perform retry and timeout
	s.setTimeExpiry(reqMsg)

	//Push to maps only if it is a original send
	if reqMsg.Enqueue() &&
		reqMsg.IsConfirmed() { //Add only the confirmed class PDUs to Cache
		s.outgoingMsgCache.addEntry(newDispatcherStateReference(reqMsg))
	}
	if s.api != nil && s.api.debug {
		s.printBeforeSending(*outgoingPacket, *reqMsg)
	}

	if err = s.transportModel.Write(outgoingPacket); err != nil {
		errStr := "Error in Sending the packet: " + err.Error()
		log.Fatal(errStr)
		return -1, errors.New(errStr)
	}
	if s.snmpGrp != nil { //Process the outgoing message for counters
		s.snmpGrp.ProcessOutPkts(*reqMsg)
	}

	//fmt.Println("sendData::", reqMsg.RequestID(), reqMsg.MsgID(), reqMsg.GetID())
	return msgID, nil
}

//ReturnResponseMsg sends the response msg for the incoming request to the remote entity.
//
//stateReference represents the cache prepared from incoming message. If it is nil, stateReference will be obtained from internal store.
//If no cache reference found, it returns error.
func (s *SnmpSession) ReturnResponseMsg(respMsg msg.SnmpMessage, stateReference engine.StateReference) error {

	//We should have StateReference in place to proceed with the Response Message
	if stateReference == nil {
		//Let's look the incomingMsg cache for the stateReference
		incCache := s.incomingMsgCache.getEntry(respMsg.RequestID())
		if incCache != nil && incCache.stateReference != nil {
			stateReference = incCache.stateReference
			s.incomingMsgCache.removeEntry(respMsg.RequestID())
		} else {
			return errors.New("No StateReference exist to process this outgoing response.")
		}
	}
	s.setParamsOnMsg(&respMsg)

	mps := s.api.SnmpEngine().MsgProcessingSubSystem()
	//Will be forwarded to respective Message Processing Model based on the Version.
	encodedData, tmStateReference, err := mps.PrepareResponseMessage(respMsg.ProtocolOptions(), //ProtocolOptions
		respMsg.Version(),
		respMsg.MaxSizeResponseScopedPDU(),
		respMsg.MsgSecurityModel(),
		respMsg.SecurityName(),  //It will be community string for v1/v2c and username for Version3
		respMsg.SecurityLevel(), //Default - NoAuthNoPriv
		respMsg.ContextEngineID(),
		respMsg.ContextName(), //can be null
		respMsg.SnmpPDU,
		stateReference,
		nil, //We are not passing StatusInformation, Bcos PrepareResponseMsg is not generating report, it is handled by PrepareDataElements()
	)

	if err != nil {
		log.Debug("Error in preparing the response/report message: " + err.Error())
		return err
	}

	outgoingPacket := new(trans.SnmpTransportPacket)
	outgoingPacket.ProtocolOptions = respMsg.ProtocolOptions()
	outgoingPacket.ProtocolData = encodedData
	//Transport cache could have been created by any transport-aware security model.
	//It should be passed onto transport layer for security processing.
	outgoingPacket.TmStateReference = tmStateReference

	if s.api != nil && s.api.debug {
		s.printBeforeSending(*outgoingPacket, respMsg)
	}
	if err = s.transportModel.Write(outgoingPacket); err != nil { //sendMessage TM ASI
		log.Fatal("Error in Sending the response/report message: " + err.Error())
		return err //return -1, errors.New("Error in Sending the packet: " + err.Error())
	}
	if s.snmpGrp != nil { //Process the outgoing response message for counters
		s.snmpGrp.ProcessOutPkts(respMsg)
	}

	return nil
}

//This method processes the outgoing message and return the final encoded byte array to send (or) returns error in case of failure.
func (s *SnmpSession) processOutgoingMessage(reqMsg *msg.SnmpMessage) (int32, []byte, trans.TransportStateReference, error) {

	mps := s.api.SnmpEngine().MsgProcessingSubSystem()

	//Will be forwarded to respective Message Processing Model based on the Version.
	id, pduByteArray, tmStateReference, err := mps.PrepareOutgoingMessage(reqMsg.ProtocolOptions(), //ProtocolOptions
		reqMsg.Version(),
		reqMsg.MsgSecurityModel(),
		reqMsg.SecurityName(),  //It will be community string for v1/v2c and username for Version3
		reqMsg.SecurityLevel(), //Default - NoAuthNoPriv
		reqMsg.ContextEngineID(),
		reqMsg.ContextName(), //can be null
		reqMsg.SnmpPDU,
		reqMsg.IsConfirmed(), //If true, respective MPM will cache the outgoing request
	)

	return id, pduByteArray, tmStateReference, err
}

func (s *SnmpSession) setParamsOnMsg(msg *msg.SnmpMessage) {

	//if version is not set on SnmpMessage, then use the version set on SnmpSession
	if msg.Version() < 0 {
		msg.SetVersion(s.version) //Default Version1
	}

	//if community string is not set on SnmpMessage, then use the community string set on SnmpSession
	if msg.Community() == "<nil>" {
		msg.SetCommunity(s.community)
	}

	//Set the security model for outgoing message
	if msg.Version() == consts.Version3 && msg.MsgSecurityModel() > 2 {
		//If some security model is explicitly set on SnmpMessage, donot override it.
		//We can check if the model is registered with the subsystem.
	} else { //Else use our default security models.
		secModel := s.getSecurityModel(*msg)
		if secModel == -1 {
			log.Warn("There is no Security Model exist to process the message: " + getMsgDebugStr(*msg))
		}
		msg.SetMsgSecurityModel(secModel) //USM by default for SNMPv3
	}

	//Let's perform v3 related check
	if msg.Version() == consts.Version3 {
		if msg.UserName() == "" || msg.UserName() == "<nil>" {
			msg.SetUserName(s.userName) //default - nil
		}
		if msg.MsgMaxSize() < 484 {
			msg.SetMsgMaxSize(s.msgMaxSize) //Default msg max size
		}

		//Check the security level
		secLevel := msg.SecurityLevel()
		if secLevel != consts.NoAuthNoPriv &&
			secLevel != consts.AuthNoPriv &&
			secLevel != consts.AuthPriv {
			msg.SetSecurityLevel(consts.NoAuthNoPriv)
		}
	} else {
		msg.SetSecurityLevel(consts.NoAuthNoPriv)
	}

}

//Sets the enqueue flag on SnmpMessage
func setEnqueueOnPDU(reqMsg *msg.SnmpMessage) {
	//Enqueue only the requests which expects response
	if (reqMsg.Command() == consts.GetRequest) ||
		(reqMsg.Command() == consts.GetNextRequest) ||
		(reqMsg.Command() == consts.GetBulkRequest) ||
		(reqMsg.Command() == consts.SetRequest) ||
		(reqMsg.Command() == consts.InformRequest) {
		reqMsg.SetEnqueue(true)
	} else {
		reqMsg.SetEnqueue(false)
	}
}

func (s *SnmpSession) getSecurityModel(mesg msg.SnmpMessage) int32 {
	switch mesg.Version() {
	case consts.Version1:
		return security.V1sec
	case consts.Version2C:
		return security.V2Csec
	case consts.Version3:
		if mesg.MsgSecurityModel() > 2 {
			//If security model is set explicitly on the msg, donot override it.
			return mesg.MsgSecurityModel()
		} else {
			//USM security model is used by default for SNMPv3
			return consts.USM //return s.DefaultV3SecurityModel()
		}
	}

	return -1
}

//####Timeout and Retries#####//

//Set Timeout and Retries on the PDU
func (s *SnmpSession) setTimeoutRetries(reqMsg *msg.SnmpMessage) {
	//In MilliSeconds

	//If SnmpMessage's timeout value is greater than zero, it will be used.
	//Else session's timeout value will be used in case it is greater than zero
	//otherwise default value of 5 secs will be used.
	if reqMsg.Timeout() <= 0 {
		if s.timeout > 0 {
			//Set the Session Timeout value on PDU
			reqMsg.SetTimeout(s.timeout)
		} else {
			//Set the default value
			reqMsg.SetTimeout(5000)
		}
	}

	//If SnmpMessage's retries value is greater than or equal to zero, it will be used.
	//Else session's timeout value will be used if it is a non-negative value.
	if reqMsg.Retries() < 0 {
		if s.retries >= 0 {
			//Set the Session Retries value on PDU
			reqMsg.SetRetries(s.retries)
		}
	}
	reqMsg.SetOriginalRetries(reqMsg.Retries())
}

//Set the TimeExpiry value on the PDU
func (s *SnmpSession) setTimeExpiry(pdu *msg.SnmpMessage) {
	pdu.SetTimeExpire(currentTimeInMillis() + s.timeoutPolicy.CalculateTimeout((pdu.OriginalRetries()-pdu.Retries()), pdu.Timeout()))
}

//Calculate TimeExpire value for synchronous request
func (s *SnmpSession) getTimeExpiresForSyncSend(retries, timeout int) int {
	//Default timeout calculation will be exponential
	var timeExpires int
	for i := 0; i <= retries; i++ {
		timeExpires += s.timeoutPolicy.CalculateTimeout(i, timeout)
	}
	return timeExpires
}

//####Timeout and Retries#####//

//Check for the arrival of response message till time expire
func (s *SnmpSession) checkResponseList(reqMsg msg.SnmpMessage, uniqueID int32, timeExpire int) (*msg.SnmpMessage, error) {
	resp := new(msg.SnmpMessage)
	//Loop until synchrounous timeout
	if reqMsg.Version() == consts.Version3 {
		for {
			if resp = s.getFromV3Sync(uniqueID); resp != nil {
				s.removeFromV3Sync(uniqueID) //Remove is done only if the required msg is available, as it would perform write lock on the sync queue.
				return resp, nil
			}
			time.Sleep(time.Nanosecond * 1) //Delay - impt
			if currentTimeInMillis() > (timeExpire + 50) {
				//if DEBUG - Print and log
				//			for _, resp :=  range s.Responses() { //OutStanding Responses
				//				if resp.MsgID() == 5 {
				//					return &resp, nil
				//				}
				//			}
				return nil, errors.New("Time out occurred. No response from the server.")
			}
		}
	} else {
		for {
			if resp = s.getFromV1Sync(uniqueID); resp != nil {
				s.removeFromV1Sync(uniqueID) //Remove is done only if the required msg is available, as it would perform write lock on the sync queue.
				return resp, nil
			}
			time.Sleep(time.Nanosecond * 1) //Delay - impt
			if currentTimeInMillis() > (timeExpire + 50) {
				//if DEBUG - Print and log
				//			for _, resp :=  range s.Responses() { //OutStanding Responses
				//				if resp.MsgID() == 5 {
				//					return &resp, nil
				//				}
				//			}
				return nil, errors.New("Time out occurred. No response from the server.")
			}
		}
	}

	return nil, errors.New("Time out occurred. No response from the server.")
}

//Add timed out PDU address to the timeout list
func (s *SnmpSession) enqueueToTimeoutList(timeoutMsg *msg.SnmpMessage) {
	if timeoutMsg != nil {
		s.timeoutList.PushBack(timeoutMsg)
	}
}

//Remove the PDU from the timeout list by Request Id
func (s *SnmpSession) dequeueFromTimeoutList(requestId int32) (timeoutMsg msg.SnmpMessage, success bool) {

	for mesg := s.timeoutList.Front(); mesg != nil; mesg = mesg.Next() {
		p := mesg.Value.(*msg.SnmpMessage)
		if p.RequestID() == requestId {
			success = true
			timeoutMsg = *p
			s.timeoutList.Remove(mesg)
			break
		}
	}

	return
}

//Wait blocks the current routine till the arrival of responses for all asynchronous requests made through this session.
//
//It implements wait group counter to wait for the response messages.
//To perform async communication, user of the API can use channels (or) other ways to wait for response messages.
//This is just an additional utility method for acheiving the same.
func (s *SnmpSession) Wait() {
	//Wait for the completion of all async requests
	s.asyncGroup.Wait()
}

//Close closes the Snmp Session and stops the receiver routine.
//This method will simply return if it is called even before calling the open method or if it is called for the second time.
func (s *SnmpSession) Close() {

	if !s.sessionOpen {
		log.Trace("Failure in closing the session. Session is not open.")
		return
	}

	//Close the SnmpSession. This will stop the receiver goroutine.
	s.sessionOpen = false //Quick signal for receiver routine

	//Stop the CallBackRoutine if one is running
	if s.snmpCallBack != nil {
		s.snmpCallBack.stopCallBackRoutine()
	}
	//Remove the session from api list
	if api := s.api; api != nil {
		api.removeSnmpGroup(s.protocolOptions.LocalHost(), s.protocolOptions.LocalPort())
		api.RemoveSnmpSession(s)
	}
	//Close the connection (UDP/TCP) opened through this Session
	if err := s.transportModel.Close(); err != nil {
		log.Fatal("Error in closing the SnmpSession: " + err.Error())
	}
}

//###RequestId Geneneration###//
/*type requestId struct {
	id int
}

func (req *requestId) increment() int {
	req.id++
	return req.id
}

//Start incrementing the RequestId from 0
var requestIdGenerator interface {
	increment() int
} = &requestId{0}

func getNewRequestId() int {
	//Add Sync here
	reqId := requestIdGenerator.increment()
	return reqId
}*/

//Set Unique RequestId on the PDU
func (s *SnmpSession) setUniqueRequestId(snmpMsg *msg.SnmpMessage) {
	if snmpMsg.RequestID() <= 0 { //Set unique RequestID on the Msg only if it is not set
		snmpMsg.SetRequestID(s.incReqID())
	}
}

func (s *SnmpSession) incrementReqId() func() int32 {
	return func() (id int32) {
		s.reqId++
		atomic.AddInt32(&id, s.reqId)
		return
	}
}

//###End of RequestId Geneneration###//

//###Monitoring Requests###//

//Monitor all the SNMP Requests made through this Session for Timeout and Retries.
func (s *SnmpSession) monitorRequests() {
	var msg *msg.SnmpMessage

	//Loop over the request maps for Timeout and Retries
	for _, cache := range s.outgoingMsgCache.entries() {
		msg = cache.msg
		if currentTimeInMillis() > (msg.TimeExpire()) {
			if msg.Retries() <= 0 { //Print Timeout and remove the request PDU from requestList
				s.outgoingMsgCache.removeEntry(msg.RequestID()) //Remove the SnmpMessage from request maps
				if s.timeoutList.Len() >= timeout_queue_size {  //If timeout queue overflows, remove one message.
					if mesg := s.timeoutList.Front(); mesg != nil {
						s.timeoutList.Remove(mesg)
					}
				}
				s.enqueueToTimeoutList(msg) //Add to timeout list
				if s.callbackFlag {         //Handover the timeout PDU to the CallbackRoutine if one is running
					if s.snmpCallBack == nil {
						s.snmpCallBack = newSnmpCallBack(s)
					}
					s.snmpCallBack.addToTimeoutMaps(msg) //Add the timeout msg to the SnmpCallBack's Timeout queue
				} else {
					s.notifyTimeOut(msg) //Notify SnmpClients through Callback with Nil PDU
				}
				if s.api.debug {
					s.printTimeout(*msg)
				}
			} else { //Resend the PDU if there are retries left
				msg.SetRetries(msg.Retries() - 1)
				msg.SetEnqueue(false) //Don't enqueue this to requestList
				s.sendData(msg)
				break
			}
		}
	}

}

//###End of Monitoring Requests###//

//##### Utility Functions######//

//Print the hex octets after sending the data
func (s *SnmpSession) printBeforeSending(transPacket trans.SnmpTransportPacket, pdu msg.SnmpMessage) {
	var printBuf bytes.Buffer
	var target string

	protocolOptions := transPacket.ProtocolOptions
	pduByte := transPacket.ProtocolData
	target = protocolOptions.SessionID() //This would give us the remote address ("hostname:port")

	//Original Send
	if pdu.OriginalRetries() == pdu.Retries() {
		printBuf.WriteString("Sent to: " + target + "\n")
		printBuf.WriteString("Sent time: " + time.Now().String() + "\n")
		printBuf.WriteString("Length of Snmp Data: ")
		printBuf.WriteString(strconv.Itoa(len(pduByte)) + "\n" + "Data: " + "\n")
		printBuf.WriteString(printOctets(pduByte, len(pduByte)) + "\n")
	} else {
		//Resending the packet
		printBuf.WriteString("Timed out. Resent the packet with Request Id " + "\"" + strconv.Itoa(int(pdu.RequestID())) + "\"" + " to: ")
		printBuf.WriteString(target + "\n")
		printBuf.WriteString("Resent Time: " + time.Now().String())
	}
	s.debugPrints(printBuf.String(), log.DEBUG, pdu.Sync(), pdu.ClientID())
}

//Print the hex octets of received packet
func (s *SnmpSession) printReceivedPacket(msg msg.SnmpMessage) {

	var printBuf bytes.Buffer
	pduBytes := msg.ProtocolData()

	printBuf.WriteString("Packet From: ")
	printBuf.WriteString(msg.ProtocolOptions().SessionID())
	printBuf.WriteString("\n" + "Received Time: " + time.Now().String() + "\n")
	printBuf.WriteString("Length of SNMP Data: " + strconv.Itoa(len(pduBytes)) + "\n" + "Data: " + "\n")
	printBuf.WriteString(printOctets(pduBytes, len(pduBytes)))

	s.debugPrints(printBuf.String(), log.DEBUG, msg.Sync(), msg.ClientID())
}

func (s *SnmpSession) printPacketBytes(protocolOptions trans.ProtocolOptions, pduBytes []byte) {
	var printBuf bytes.Buffer

	printBuf.WriteString("Packet From: ")
	printBuf.WriteString(protocolOptions.SessionID())
	printBuf.WriteString("\n" + "Received Time: " + time.Now().String() + "\n")
	printBuf.WriteString("Length of SNMP Data: " + strconv.Itoa(len(pduBytes)) + "\n" + "Data: " + "\n")
	printBuf.WriteString(printOctets(pduBytes, len(pduBytes)))

	s.debugPrints(printBuf.String(), log.DEBUG, false, -1)
}

//Print the Timeout message
func (s *SnmpSession) printTimeout(msg msg.SnmpMessage) {
	var printBuf bytes.Buffer

	printBuf.WriteString("Timed out. No more retries to ")
	printBuf.WriteString(msg.ProtocolOptions().SessionID())
	printBuf.WriteString("\n" + "Request ID: " + strconv.FormatInt(int64(msg.RequestID()), 10))
	printBuf.WriteString("\n" + "Time: " + time.Now().String() + "\n")

	s.debugPrints(printBuf.String(), log.DEBUG, msg.Sync(), msg.ClientID())
}

//To get the Hex octets from the byte array
func printOctets(data []byte, length int) string {
	hexUnits := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"}
	ch := make([]string, length*3-1)
	length--
	j, i := 0, 0
	for i = 0; i < length; i++ {

		ch[j] = hexUnits[int(((data[i] & 0xf0) >> 4))]
		j++
		ch[j] = hexUnits[int((data[i] & 0x0f))]
		j++

		if (i+1)%20 == 0 {
			ch[j] = "\n"
			j++
		} else {
			ch[j] = " "
			j++
		}

	}
	ch[j] = hexUnits[int(((data[i] & 0xf0) >> 4))]
	j++
	ch[j] = hexUnits[int((data[i] & 0x0f))]
	j++

	var octetStr string
	for _, str := range ch {
		octetStr += str
	}

	return octetStr
}

//Get the Current System Time (in MilliSeconds) as an int
func currentTimeInMillis() int {
	return int(time.Now().UnixNano() / 1000000)
}

//Debug Prints will be forwarded to loggers (and) SnmpClient's DebugPrints if async send
//Else printed in console
func (s *SnmpSession) debugPrints(dbgStr string, lvl log.LogLevel, syncFlag bool, clientID int) {

	//For Synchronous
	if syncFlag {
		if log.LogFilters() != nil {
			log.Logf(lvl, dbgStr)
		} else { //Just write to console
			if s.api.Debug() {
				fmt.Println(dbgStr)
			}
		}
	} else { //For Asynchornous
		//Fwd the debugPrints to SnmpClients
		if clientID > 0 {
			s.clientSync.RLock()
			defer s.clientSync.RUnlock()
			//Call the desired SnmpClient with the clientId found in clientMaps
			if client := s.clientMaps[clientID]; client != nil {
				client.DebugStr(dbgStr)
			} else {
				log.Logf(lvl, dbgStr) //Send it to logger
			}

		} else if s.clients.Len() > 0 {
			//Call all the SnmpClients found in the clients list
			for _, client := range s.clients.List() {
				client.(SnmpClient).DebugStr(dbgStr)
			}
		} else {
			if s.api.Debug() {
				fmt.Println(dbgStr)
			}
		}
	}
}

func getMsgDebugStr(mesg msg.SnmpMessage) string {
	msgStr := ""
	msgStr += "ReqID: " + strconv.FormatInt(int64(mesg.RequestID()), 10) + "; "
	msgStr += "Command: " + getMSGCommand(mesg.Command()) + "; "
	if mesg.ProtocolOptions() != nil {
		msgStr += "RemoteAddress: " + mesg.ProtocolOptions().SessionID() + ";"
	}

	return msgStr
}

//getMSGCommand returns the string form of the SnmpMessage's command.
func getMSGCommand(cmd consts.Command) string {

	var cmdStr string = ""

	switch cmd {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	case consts.ReportMessage:
		cmdStr = "Report"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}

//###### End of Utility Functions######//

//Implement SnmpSubSystem interface
//For dispatcher (SnmpSession), we are providing a dummy implementation.
type snmpSubSystemImpl struct {
	subSystemModels map[int32]engine.SnmpModel
	snmpEngine      engine.SnmpEngine
}

//Returns instance of SubSystem. This holds all the SnmpModels.
func newSubSystem(snmpEngine engine.SnmpEngine) *snmpSubSystemImpl {
	return &snmpSubSystemImpl{
		subSystemModels: make(map[int32]engine.SnmpModel),
		snmpEngine:      snmpEngine, //Need to override SnmpEngine?
	}
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) AddModel(model engine.SnmpModel) {
	if model != nil {
		if _, ok := ss.subSystemModels[model.ID()]; ok {
			return //Model already exist
		}

		ss.subSystemModels[model.ID()] = model
	}
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) RemoveModel(modelID int32) engine.SnmpModel {
	if model, ok := ss.subSystemModels[modelID]; ok {
		delete(ss.subSystemModels, modelID)
		return model
	}
	return nil
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) Model(modelID int32) engine.SnmpModel { //Check for nil value
	return ss.subSystemModels[modelID]
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) Models() []engine.SnmpModel {
	var models []engine.SnmpModel
	for _, model := range ss.subSystemModels {
		models = append(models, model)
	}
	return models
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) ModelIDs() []int32 {
	var ids []int32
	for id, _ := range ss.subSystemModels {
		ids = append(ids, id)
	}
	return ids
}

//Implementation of SnmpSubSystem interface. For internal use only.
func (ss *snmpSubSystemImpl) ModelNames() []string {
	var modelNames []string
	for id, model := range ss.subSystemModels {
		modelNames = append(modelNames, model.Name()+" ["+strconv.Itoa(int(id))+"]")
	}
	return modelNames
}

//Returns the SnmpEngine associated with this SnmpSession.
//
//Note: Multiple SnmpSessions under a SnmpAPI share the same SnmpEngine.
func (ss *snmpSubSystemImpl) SnmpEngine() engine.SnmpEngine {
	return ss.snmpEngine
}
