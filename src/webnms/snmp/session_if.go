/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"webnms/snmp/msg"
)

//This is the interface implemented by client programs that wish to use callback, authentication functions of the API.
//Without implementing this interface, synchronous requests can be used.
//
//To use asynchronous communication, this interface must be implemented to receive the responses.
//
//Register this SnmpClient using SnmpSession.AddSnmpClient(SnmpClient) method.
type SnmpClient interface {
	//This function needs to be supplied by the user of the API to use callbacks.
	//Callback on a client gets invoked only if the client authenticate returns true. On the arrival of response for asynchronous request, callback method will get called with the SnmpMessage and RequestID.
	Callback(*msg.SnmpMessage, int32)

	//This function needs to be supplied by the user of the API to add authentication.
	//If not, no authentication will be done. If this method returns false, a message will be printed to Standard Error Output and the PDU will be dropped at the receiver end.
	Authenticate(*msg.SnmpMessage, string) bool

	//This function needs to be supplied by the user of the API to receive debug prints.
	DebugStr(dbgStr string)
}

//Interface for calculating Timeouts
type TimeoutPolicy interface {
	//'CalculateTimeout' method should calculate the timeout value and return it.
	//The first argument is the timeout value that is set on the pdu before sending a SNMP request and the retries is the iteration.
	/*
	 Here is a better explanation:
	 For example, if the timeout value is 5000 MilliSeconds and the retries
	 value is 3, then this method will be called with the following values.
	 * CalculateTimeout(5000, 0) -- this should return the timeout for the actual request that is sent
	 * CalculateTimeout(5000, 1) -- this should return the timeout for the first re-try
	 * CalculateTimeout(5000, 2) -- this should return the timeout for the second re-try
	 * CalculateTimeout(5000, 3) -- this should return the timeout for the third re-try
	*/
	CalculateTimeout(int, int) int
}

//ExponentialTimeoutPolicy is the default implementation of TimeoutPolicy interface for calculating timeouts.
type ExponentialTimeOutPolicy struct{}

//Calculate the timeout based on retries and timeout values exponentially.
func (exp ExponentialTimeOutPolicy) CalculateTimeout(retries, timeout int) int {
	return (1 << uint(retries)) * timeout
}
