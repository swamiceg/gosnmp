/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"webnms/snmp/snmpvar"
)

//SnmpVarBind is the type of SNMP Variable Binding. It contains an object identifier and an SnmpVar.
type SnmpVarBind struct {
	objectID       snmpvar.SnmpOID
	variable       snmpvar.SnmpVar
	exceptionIndex int
}

//NewSnmpVarBind creates variable binding with given Object ID and Variable. If the specified snmpVar is nil, SnmpNull variable-binding will be created with the specified oid.
func NewSnmpVarBind(oid snmpvar.SnmpOID, snmpVar snmpvar.SnmpVar) SnmpVarBind {

	varBind := SnmpVarBind{}
	varBind.objectID = oid

	if snmpVar == nil {
		varBind.variable = snmpvar.NewSnmpNull()
	} else {
		varBind.variable = snmpVar
	}
	return varBind
}

//ObjectID returns the object identifier in the SnmpVarBind.
func (varBind *SnmpVarBind) ObjectID() snmpvar.SnmpOID {
	return varBind.objectID
}

//SetObjectID sets the object identifier in the SnmpVarBind.
func (varBind *SnmpVarBind) SetObjectID(oid snmpvar.SnmpOID) *SnmpVarBind {
	varBind.objectID = oid
	return varBind
}

//Variable returns the variable in the SnmpVarBind.
func (varBind *SnmpVarBind) Variable() snmpvar.SnmpVar {
	return varBind.variable
}

//SetVariable sets the variable in the SnmpVarBind. If the specified snmpVar is nil, SnmpNull variable-binding will be set in the SnmpVarBind.
func (varBind *SnmpVarBind) SetVariable(snmpVar snmpvar.SnmpVar) *SnmpVarBind {
	if snmpVar == nil {
		varBind.variable = snmpvar.NewSnmpNull()
	} else {
		varBind.variable = snmpVar
	}

	return varBind
}

//ExceptionIndex returns the exception if any in the SnmpVarBind. This is used as a storage for the exception value in the v2c agent's response pdu. Unlike in SnmpV1, there are some exception responses in SnmpV2 where the errstat is set to zero and the tag value of the variable field in the varbind is set to an exception value.
func (varBind *SnmpVarBind) ExceptionIndex() int {
	return varBind.exceptionIndex
}

//SetExceptionIndex sets the exception if any in the SnmpVarBind. This is used as a storage for the exception value in the v2c agent's response pdu. Unlike in SnmpV1, there are some exception responses in SnmpV2 where the errstat is set to zero and the tag value of the variable field in the varbind is set to an exception value.
//
//This function is used internally while decoding. API users are advised not to use it.
func (varBind *SnmpVarBind) SetExceptionIndex(expIndex int) *SnmpVarBind {
	varBind.exceptionIndex = expIndex
	return varBind
}

//String returns the string form of this variable-binding to a printable string in the form, OID : value
func (varBind *SnmpVarBind) String() string {
	return varBind.objectID.String() + " : " + varBind.variable.String()
}

//TagString converts the varBind to a printable string on two lines, the first with Object ID : <oid-value> and the second with <variable-type> : <variable-value>
func (varBind *SnmpVarBind) TagString() string {
	return varBind.objectID.TagString() + "\n" + varBind.variable.TagString()
}

//Equals returns true if this SnmpVarBind object and the incoming SnmpVarBind object are same in their contents and false otherwise.
func (varBind *SnmpVarBind) Equals(varb SnmpVarBind) bool {

	oid := varBind.objectID
	tempOID := varb.objectID

	if !oid.Equals(tempOID) {
		return false
	}

	return snmpvar.CompareVar(varBind.variable, varb.variable)
}
