/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package msg defines the PDU structures for all three SNMP versions SNMPv1, SNMPv2c and SNMPv3
//as per RFC1157. Supports different types of messages such as Get, Set, Trap, Inform etc.,
//
//SnmpMessage and SnmpVarBind types should be used by the API users for creating / manipulating the SNMP messages. Please take a look at the documentation of SnmpMessage and SnmpVarBind for more details.
//
//SnmpPDU and ScopedPDU are used for internal purposes only. API users are not recommended to use them directly in their applications.
package msg
