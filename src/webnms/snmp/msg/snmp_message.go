/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"bytes"
	"encoding/asn1"
	"strconv"
	"strings"

	"webnms/snmp/consts"
	"webnms/snmp/engine/transport"
	"webnms/snmp/snmpvar"
)

//SnmpMessage encapsulates a v1, v2c or v3 Snmp Message.
//SnmpMessage should be obtained using NewSnmpMessage() method only.
//
//Any SNMP operation can be performed using this type.
//
//The following should be set on it before performing any SNMP operation:
//
//	1. The SNMP command. It may be consts.GetRequest, consts.SetRequest, consts.GetBlkRequest etc. Refer consts.Command for possible commands that can be set.
//	2. The SnmpVarBinds that should be present in this message.
//	3. The network communication variables such as remoteHost and remotePort ( in case of UDP ). This is not needed if it is already present in SnmpSession. 
//	   But it is always good to set it on message rather than on SnmpSession.
//
//The following code snippet will give a clear idea on how to do a SNMP operation using SnmpMessage
//
//	msg := msg.NewSnmpMessage()
//	msg.SetCommand(consts.GetRequest)
//	msg.AddNull(snmpvar.NewSnmpOID("1.5.0"))
//
//If the underlying protocol used is UDP, then an instance of UDPProtocolOptions should be instantiated to set the remotePort and remoteHost variables.
//
//Note: By default WebNMS SNMP Stack uses UDP as the underlying protocol. 
//
//	udp := snmp.NewUDPProtocolOptions()
//	udp.SetRemoteHost("localhost")
//	udp.SetRemotePort(8001)
//	msg.SetProtocolOptions(udp)
//	response := session.SyncSend(msg)
//
//When a message is re-used, ensure the request id is set properly. If request id is set non-zero, the API leaves it as it is. If it is 0, then the API assigns a unique request id, to ensure no conflict with other requests. So, if you do not want to manage the request ids yourself, set it to 0 before each request. With SnmpSession.SyncSend() you don't need to store the value for later reference. With SnmpSession.Send(), the reqid assigned is returned, and you will need it later to check up on the response.
//
//The SnmpMessage provides most of the communication parameters related methods that are available with the SnmpSession. Wherever the value of the parameter is set in the message, it overrides the value in the session.
type SnmpMessage struct {
	version   consts.Version
	community string

	HeaderData
	msgSecurityParameters []byte
	userName              string

	ScopedPDU
}

//HeaderData represents the header data as in RFC3412.
type HeaderData struct {
	msgID            int32
	msgMaxSize       int32
	msgFlags         byte
	msgSecurityModel int32
}

//NewSnmpMessage creates a new SnmpMessage instance.
func NewSnmpMessage() SnmpMessage {
	msg := SnmpMessage{}
	msg.timeout = -1
	msg.retries = -1
	msg.community = "<nil>"
	msg.version = consts.Version(-1)
	msg.securityLevel = 0

	return msg
}

//decodeVersion decodes the version of the snmp message.
func decodeVersion(enc1Bytes []byte) int {
	type rawVersion struct {
		Version   int
		OtherData asn1.RawValue
	}

	versionStruct := new(rawVersion)

	//Get the version by decoding the whole message once
	if _, wholeErr := asn1.Unmarshal(enc1Bytes, versionStruct); wholeErr != nil {
		return -1
	}

	return versionStruct.Version
}

//String is the string form of SnmpMessage.
func (msg *SnmpMessage) String() string {

	msgBuffer := bytes.NewBufferString("Message ::\n")
	msgBuffer.WriteString("Version : " + msg.version.String() + "\n")
	if msg.version == consts.Version3 {
		msgBuffer.WriteString("MessageID : " + strconv.FormatInt(int64(msg.msgID), 10) + "\n")
		msgBuffer.WriteString("Maximum Size : " + strconv.FormatInt(int64(msg.msgMaxSize), 10) + "\n")
		msgBuffer.WriteString("Flags : " + strconv.Itoa(int(msg.msgFlags)) + "\n")
		msgBuffer.WriteString("Security Model : " + strconv.FormatInt(int64(msg.msgSecurityModel), 10) + "\n")

		switch msg.msgFlags {
		case 0, 4:
			msgBuffer.WriteString("Security Level : NO_AUTH_NO_PRIV\n")
		case 1, 5:
			msgBuffer.WriteString("Security Level : AUTH_NO_PRIV\n")
		case 3, 7:
			msgBuffer.WriteString("Security Level : AUTH_PRIV\n")
		}
		msgBuffer.WriteString("ContextEngineID : " + printOctets(msg.contextEngineID, len(msg.contextEngineID)) + "\n")
		msgBuffer.WriteString("ContextName : " + msg.contextName + "\n")
	} else {
		msgBuffer.WriteString("Community : " + msg.community + "\n")
	}
	msgBuffer.WriteString("Command : " + msg.getMSGCommand() + "\n")

	if msg.command != consts.TrapRequest {
		msgBuffer.WriteString("Request-ID : " + strconv.FormatInt(int64(msg.requestID), 10) + "\n")
	}

	if msg.command == consts.GetBulkRequest {
		msgBuffer.WriteString("Non-Repeaters : " + strconv.FormatInt(int64(msg.nonRepeaters), 10) + "\n")
		msgBuffer.WriteString("Max-Repetitions : " + strconv.FormatInt(int64(msg.maxRepetitions), 10) + "\n")
	} else if msg.command == consts.TrapRequest {
		msgBuffer.WriteString("Generic Type : " + strconv.Itoa(msg.genericType) + "\n")
		msgBuffer.WriteString("Specific Type : " + strconv.Itoa(msg.specificType) + "\n")
		msgBuffer.WriteString("Enterprise OID : " + msg.enterprise.String() + "\n")
		msgBuffer.WriteString("Agent Address : " + msg.agentAddress + "\n")
		msgBuffer.WriteString("Agent Uptime : " + snmpvar.NewSnmpTimeTicks(uint32(msg.upTime)).String() + "\n")
	} else {
		msgBuffer.WriteString("Error Status : " + strconv.FormatInt(int64(msg.errorStatus), 10) + "(" + errorString(msg.errorStatus) + ")" + "\n")
		msgBuffer.WriteString("Error Index : " + strconv.FormatInt(int64(msg.errorIndex), 10) + "\n")
	}
	msgBuffer.WriteString("Varbinds : \n")
	msgBuffer.WriteString(msg.PrintVarBinds())

	return msgBuffer.String()
}

//versionString returns 0(v1)/1(v2c)/3(v3) incase if version is set to Version1/Version2C/Version3. "Invalid version" returned otherwise.
func (msg *SnmpMessage) versionString() string {

	var versionStr string

	switch msg.version {
	case consts.Version1:
		versionStr = "0(v1)"
	case consts.Version2C:
		versionStr = "1(v2c)"
	case consts.Version3:
		versionStr = "3(v3)"
	default:
		versionStr = "Invalid version"
	}

	return versionStr
}

//getMSGCommand returns the string form of the SnmpMessage's command.
func (msg *SnmpMessage) getMSGCommand() string {

	var cmdStr string = ""

	switch msg.command {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	case consts.ReportMessage:
		cmdStr = "Report"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}

//Version returns snmp Version number in message.
//
//The version set in message overrides the version in the session. For example, if a session version is set to snmp.Version2C, and a message is sent without setting its version explicitly (the message will have the default version of snmp.Version1), then an SNMPv2C message is sent to the peer SNMP entity. On the other hand, if the message version is set explicitly to snmp.Version2C, an SNMPv2c message will be sent to the peer entity.
func (msg *SnmpMessage) Version() consts.Version {
	return msg.version
}

//SetVersion sets snmp Version number in message. The valid version constants to be set are Version1 and Version2C.
//
//The version set in message overrides the version in the session. For example, if a session version is set to snmp.Version2C, and a message is sent without setting its version explicitly (the message will have the default version of snmp.Version1), then an SNMPv2C message is sent to the peer SNMP entity. On the other hand, if the message version is set explicitly to snmp.Version2C, an SNMPv2c message will be sent to the peer entity.
func (msg *SnmpMessage) SetVersion(ver consts.Version) *SnmpMessage {
	if ver == consts.Version1 || ver == consts.Version2C || ver == consts.Version3 {
		msg.version = ver
	}
	return msg
}

//Community returns community string received/sent.
//
//The community string in the message overrides the community set in session. This means, only when the community string in the message is empty, the one in session is used.
func (msg *SnmpMessage) Community() string {
	return msg.community
}

//SetCommunity sets community string received/sent.
//
//The community string in the message overrides the community set in session. This means, only when the community string in the message is empty, the one in session is used.
func (msg *SnmpMessage) SetCommunity(comString string) *SnmpMessage {
	msg.community = comString
	return msg
}

//Command returns command type of this SnmpMessage. The command type is one of the constants defined in the consts package.
func (msg *SnmpMessage) Command() consts.Command {
	return msg.command
}

//SetCommand sets command type of this SnmpMessage. The command type is one of the below constants defined in the consts package.
//
//snmp.GetRequest, snmp.GetNextRequest, snmp.GetResponse, snmp.SetRequest, snmp.TrapRequest, snmp.GetBulkRequest, snmp.InformRequest, snmp.Trap2Request & snmp.ReportMes//sage
func (msg *SnmpMessage) SetCommand(cmd consts.Command) *SnmpMessage {
	if cmd == consts.GetRequest || cmd == consts.GetNextRequest || cmd == consts.GetResponse || cmd == consts.SetRequest || cmd == consts.TrapRequest || cmd == consts.GetBulkRequest || cmd == consts.InformRequest || cmd == consts.Trap2Request || cmd == consts.ReportMessage {
		msg.command = cmd
	}
	return msg
}

//RequestID returns the request id of this message.
func (msg *SnmpMessage) RequestID() int32 {
	return msg.requestID
}

//SetRequestID sets the requestID for this message. If requestID is set to non-zero, the API leaves it alone. If it is 0, the API assigns a unique requestID to ensure no conflict with other requests. So, if you do not want to manage the request ids yourself, it should be 0. The default value is 0. If you are re-using the message and do not want to manage the requestID yourself, set it to 0 each time.
func (msg *SnmpMessage) SetRequestID(reqID int32) *SnmpMessage {
	msg.requestID = reqID
	return msg
}

//ErrorStatus returns the error status of this message.
func (msg *SnmpMessage) ErrorStatus() consts.ErrorStatus {
	return msg.errorStatus
}

//SetErrorStatus sets the Error status for this message.
//
//The error status values that can be set using this method are,
//
//snmp.NoError, snmp.TooBig, snmp.NoSuchName, snmp.BadValue, snmp.ReadOnly, snmp.GeneralError, snmp.NoAccess, snmp.WrongType, snmp.WrongLength, snmp.WrongEncoding,
//snmp.WrongValue, snmp.NoCreation, snmp.InconsistentValue, snmp.ResourceUnavailable, snmp.CommitFailed, snmp.UndoFailed, snmp.AuthorizationError,
//snmp.NotWritable & snmp.InconsistentName.
//
//It will ignore values other than the one mentioned above.
func (msg *SnmpMessage) SetErrorStatus(errStat consts.ErrorStatus) *SnmpMessage {
	if errStat >= 0 && errStat <= 18 {
		msg.errorStatus = errStat
	}
	return msg
}

//ErrorIndex returns the error index of this message.
func (msg *SnmpMessage) ErrorIndex() int32 {
	return msg.errorIndex
}

//SetErrorIndex sets the error index of this Message. Only values in range 0 to 2147483647 will be accepted by this method.
func (msg *SnmpMessage) SetErrorIndex(errIndex int32) *SnmpMessage {
	if errIndex >= 0 {
		msg.errorIndex = errIndex
	}
	return msg
}

//ErrorString returns error information as a String, with offending varbind if any.
func (msg *SnmpMessage) ErrorString() string {
	errBuffer := bytes.NewBufferString("")
	if msg.errorStatus != 0 {
		errBuffer.WriteString("Error in response. ")
		errBuffer.WriteString(errorString(msg.errorStatus))
		errBuffer.WriteString("\nIndex: ")
		errBuffer.WriteString(strconv.FormatInt(int64(msg.errorIndex), 10))
		errBuffer.WriteString("\n")
		if msg.errorIndex > 0 && len(msg.snmpVarbs) > int(msg.errorIndex) {
			errBuffer.WriteString("Errored Object ID: ")
			errBuffer.WriteString(msg.ObjectIDAt(int(msg.errorIndex)).String())
			errBuffer.WriteString("\n")
		}
	}
	return errBuffer.String()
}

//ErrorMessage returns the corresponding error message for the error status specified.
func ErrorMessage(errorStatus consts.ErrorStatus) string {
	return errorString(errorStatus)
}

//errorString returns the corresponding error message for the error status specified. Utility method used internally.
func errorString(errStat consts.ErrorStatus) string {
	var returnString string

	switch errStat {
	case consts.NoError:
		returnString = "No Error"
	case consts.TooBig:
		returnString = "Response message would have been too large."
	case consts.NoSuchName:
		returnString = "There is no such variable name in this MIB."
	case consts.BadValue:
		returnString = "The value given has the wrong type or length."
	case consts.ReadOnly:
		returnString = "This variable is read only."
	case consts.GeneralError:
		returnString = "A general failure occured."
	case consts.NoAccess:
		returnString = "A no access error occured"
	case consts.WrongType:
		returnString = "A wrong type error occured."
	case consts.WrongLength:
		returnString = "A wrong length error occured."
	case consts.WrongEncoding:
		returnString = "A wrong encoding error occured."
	case consts.WrongValue:
		returnString = "A wrong value error occured."
	case consts.NoCreation:
		returnString = "A no creation error occured."
	case consts.InconsistentValue:
		returnString = "An inconsistent value error occured."
	case consts.ResourceUnavailable:
		returnString = "A resource unavailable error occured."
	case consts.CommitFailed:
		returnString = "A commit failed error occured."
	case consts.UndoFailed:
		returnString = "A undo failed error occured."
	case consts.AuthorizationError:
		returnString = "A authorization failed  error occured."
	case consts.NotWritable:
		returnString = "A not writable error occured."
	case consts.InconsistentName:
		returnString = "An inconsistent name error occured."
	default:
		returnString = "Unknown error code: " + strconv.FormatInt(int64(errStat), 10)
	}

	return returnString
}

//NonRepeaters returns non-repeaters value of this message.
func (msg *SnmpMessage) NonRepeaters() int32 {
	return msg.nonRepeaters
}

//SetNonRepeaters sets non-repeaters value for this message. The valid values ranges from 0 to 2147483647(both inclusive) as per RFC1905.
func (msg *SnmpMessage) SetNonRepeaters(nonRep int32) *SnmpMessage {
	if nonRep >= 0 && nonRep <= 2147483647 {
		msg.nonRepeaters = nonRep
	}
	return msg
}

//MaxRepetitions returns max-repetitions value of this message.
func (msg *SnmpMessage) MaxRepetitions() int32 {
	return msg.maxRepetitions
}

//SetMaxRepetitions sets max-repetitions value for this message. The valid values ranges from 0 to 2147483647(both inclusive) as per RFC1905.
func (msg *SnmpMessage) SetMaxRepetitions(maxRep int32) *SnmpMessage {
	if maxRep >= 0 && maxRep <= 2147483647 {
		msg.maxRepetitions = maxRep
	}
	return msg
}

//Enterprise returns enterprise OID, i.e., returns the enterprise field, of the Trap-PDU.
func (msg *SnmpMessage) Enterprise() snmpvar.SnmpOID {
	return msg.enterprise
}

//SetEnterprise sets enterprise OID, i.e., the enterprise field, of the Trap-PDU.
func (msg *SnmpMessage) SetEnterprise(oid snmpvar.SnmpOID) *SnmpMessage {
	msg.enterprise = oid
	return msg
}

//AgentAddress returns address of object generating trap. Incase of SNMPv2c TRAP PDU if one of the varbind contains the snmpTrapAddress.0 oid the corresponding value is returned as the agent address.
func (msg *SnmpMessage) AgentAddress() string {
	return msg.agentAddress
}

//SetAgentAddress sets address of object generating trap. Incase of SNMPv2c TRAP PDU, an additional varbind is added, if its not already present in the varbind list. The varbind contains the snmpTrapAddress.0 (defined in RFC2576) as the oid and the value (SnmpVar) will be the agent address. There is no need to add this varbind explicitly.
func (msg *SnmpMessage) SetAgentAddress(address string) *SnmpMessage {
	msg.agentAddress = address
	return msg
}

//GenericType returns generic trap type of this message.
func (msg *SnmpMessage) GenericType() int {
	return msg.genericType
}

//SetGenericType sets generic trap type of this message. This method will accept values only from 0 to 6(both inclusive).
func (msg *SnmpMessage) SetGenericType(genType int) *SnmpMessage {
	if genType >= 0 && genType < 7 {
		msg.genericType = genType
	}
	return msg
}

//SpecificType returns specific trap type of this message.
func (msg *SnmpMessage) SpecificType() int {
	return msg.specificType
}

//SetSpecificType sets specific trap type of this message.
func (msg *SnmpMessage) SetSpecificType(specType int) *SnmpMessage {
	msg.specificType = specType
	return msg
}

//UpTime returns the timeStamp of the trap or inform. The timeStamp value is in milliseconds.
//
//This will return the value that is present in the time-stamp field present in a SNMPv1 trap-pdu. Incase of SNMPv2 trap-pdu or Inform-pdu, this method will return the sysUpTime value that is present in the first varbind. If this PDU is not a trap PDU, then this method will return '0'.
func (msg *SnmpMessage) UpTime() uint32 {
	uptime := msg.upTime

	if msg.command == consts.Trap2Request || msg.command == consts.InformRequest {
		uptime = 0
		if len(msg.snmpVarbs) > 0 {
			varBind := msg.snmpVarbs[0]
			if strings.EqualFold(varBind.ObjectID().String(), ".1.3.6.1.2.1.1.3.0") {
				switch varBind.Variable().(type) {
				case snmpvar.SnmpTimeTicks:
					tickvar := varBind.Variable().(snmpvar.SnmpTimeTicks)
					uptime = tickvar.Value()
				default:
					uptime = 0
				}
			}
		}
	}

	return uptime
}

//SetUpTime sets the timeStamp of the trap or inform. The timeStamp value is in milliseconds.
func (msg *SnmpMessage) SetUpTime(uptime uint32) *SnmpMessage {
	msg.upTime = uptime
	return msg
}

//TrapOID returns the trap-oid that will be present in the second variable binding of this SNMPv2 trap-pdu. If this is not a SNMPv2 trap-pdu, then the method will return nil.
func (msg *SnmpMessage) TrapOID() *snmpvar.SnmpOID {
	if msg.command == consts.Trap2Request || msg.command == consts.InformRequest {
		if len(msg.snmpVarbs) > 1 {
			varBind := msg.snmpVarbs[1]
			if strings.EqualFold(varBind.ObjectID().String(), ".1.3.6.1.6.3.1.1.4.1.0") {
				switch varBind.Variable().(type) {
				case snmpvar.SnmpOID:
					oidvar := varBind.Variable().(snmpvar.SnmpOID)
					return &oidvar
				case *snmpvar.SnmpOID:
					oidvar := varBind.Variable().(*snmpvar.SnmpOID)
					return oidvar
				default:
					return nil
				}
			}
		}
	}
	return nil
}

//SetProtocolOptions sets this ProtocolOptions on SnmpMessage.
//Also, ProtocolOptions set on the SnmpMessage will override the ProtocolOptions set on the SnmpSession.
//
//Parameter should be of type transport.ProtocolOptions. Will not be set if nil is passed.
//
//Currently UDP/TCP are supported.
func (msg *SnmpMessage) SetProtocolOptions(protocolOptions transport.ProtocolOptions) *SnmpMessage {
	if protocolOptions != nil {
		msg.protocolOptions = protocolOptions
	}
	return msg
}

//ProtocolOptions returns the UDP/TCP ProtocolOptions set on this SnmpMessage.
//Returns nil when no protocoloption is set.
func (msg *SnmpMessage) ProtocolOptions() transport.ProtocolOptions {
	return msg.protocolOptions
}

//VarBinds returns list of SnmpVarBind in this message as a slice.
func (msg *SnmpMessage) VarBinds() []SnmpVarBind {
	return msg.snmpVarbs
}

//SetVarBinds sets the SnmpVarBind slice to this message
func (msg *SnmpMessage) SetVarBinds(varbs []SnmpVarBind) *SnmpMessage {
	msg.snmpVarbs = varbs
	return msg
}

//Retries returns number of retries for this message before timeout.
//
//The retries in the message overrides the retries value in session. This means, only when the retries is not set in the message explicitly, the session value is used. The default value of retries is 0.
func (msg *SnmpMessage) Retries() int {
	return msg.retries
}

//SetRetries sets number of retries for this message before timeout.
//
//The retries in the message overrides the retries value in session. This means, only when the retries is not set in the message explicitly, the session value is used. The default value of retries is 0. The retries value is set only if the retryVal is non-negative.
func (msg *SnmpMessage) SetRetries(retryVal int) *SnmpMessage {
	if retryVal >= 0 {
		msg.retries = retryVal
	}
	return msg
}

//Timeout returns the timeout value of this message. The timeout is the time to wait for the first response in milli-seconds, before attempting a retransmission.
//
//The timeout value grows exponentially, after the first retransmission. For example, if the timeout is set to 5000 (meaning 5 seconds) and retries is set to 3, the first retransmission will happen after 5 seconds, the second after 15 seconds etc.
//
//The timeout in the message overrides the timeout value in session. This means, only when the timeout is not set in the message explicitly, the session timeout value is used. The default value of timeout is 5000 milliseconds.
func (msg *SnmpMessage) Timeout() int {
	return msg.timeout
}

//SetTimeout sets the timeout value of this message. The timeout is the time to wait for the first response in milli-seconds, before attempting a retransmission.
//
//The timeout value grows exponentially, after the first retransmission. For example, if the timeout is set to 5000 (meaning 5 seconds) and retries is set to 3, the first retransmission will happen after 5 seconds, the second after 15 seconds etc.
//
//The timeout in the message overrides the timeout value in session. This means, only when the timeout is not set in the message explicitly, the session timeout value is used. The default value of timeout is 5000 milliseconds. The timeout value to be set should be in milliseconds. The timeout value is set only if the timeoutVal is >= 0.
func (msg *SnmpMessage) SetTimeout(timeoutVal int) *SnmpMessage {
	if timeoutVal >= 0 {
		msg.timeout = timeoutVal
	}
	return msg
}

//SecurityName returns the message's community string for v1/v2c and UserName in case of v3.
func (msg *SnmpMessage) SecurityName() string {
	if msg.Version() == consts.Version1 || msg.Version() == consts.Version2C {
		return msg.community
	} else if msg.Version() == consts.Version3 {
		return msg.userName
	}

	return ""
}

//UserName returns the principal on whose behalf SNMPv3 requests are to be made.
//
//While sending an SNMPv3 request this userName has to be set.
func (msg *SnmpMessage) UserName() string {
	return msg.userName
}

//SetUserName sets the principal on whose behalf SNMPv3 requests are to be made.
//
//While sending an SNMPv3 request this userName has to be set.
func (msg *SnmpMessage) SetUserName(userName string) *SnmpMessage {
	msg.userName = userName
	return msg
}

//For internal use only.
func (msg *SnmpMessage) SecurityLevel() consts.SecurityLevel {
	return msg.securityLevel
}

//For internal use only.
func (msg *SnmpMessage) SetSecurityLevel(securityLevel consts.SecurityLevel) *SnmpMessage {
	if securityLevel == consts.NoAuthNoPriv ||
		securityLevel == consts.AuthNoPriv ||
		securityLevel == consts.AuthPriv {
		msg.securityLevel = securityLevel
	}
	return msg
}

//ClientID returns the id for the source which sent this message
func (msg *SnmpMessage) ClientID() int {
	return msg.clientId
}

//SetClientID sets the id for source which sent this message
func (msg *SnmpMessage) SetClientID(id int) *SnmpMessage {
	msg.clientId = id
	return msg
}

//ContextEngineID returns the contextID sent/recieved with this message
func (msg *SnmpMessage) ContextEngineID() []byte {
	return msg.contextEngineID
}

//SetContextEngineID sets the contextID sent/recieved with this message
func (msg *SnmpMessage) SetContextEngineID(contextID []byte) *SnmpMessage {
	msg.contextEngineID = contextID
	return msg
}

//ContextName returns the contextname sent/recieved with this message
func (msg *SnmpMessage) ContextName() string {
	return msg.contextName
}

//SetContextName sets the contextname sent/recieved with this message
func (msg *SnmpMessage) SetContextName(contextName string) *SnmpMessage {
	msg.contextName = contextName
	return msg
}

//AddNull adds a variable binding which contains this SnmpOID and a placeholder ( NULL ) as the value.
//Only non-null, valid SnmpOID will be added to the list of variable bindings. This method is used while doing a Get, GetNext or GetBulk request.
func (msg *SnmpMessage) AddNull(oid snmpvar.SnmpOID) {
	nullVarb := NewSnmpVarBind(oid, snmpvar.NewSnmpNull())
	nullVarbSlice := []SnmpVarBind{nullVarb}
	msg.snmpVarbs = append(msg.snmpVarbs, nullVarbSlice...)
}

//AddVarBind adds SNMP variable at the end of PDU's list of variable bindings.
func (msg *SnmpMessage) AddVarBind(varb SnmpVarBind) *SnmpMessage {
	msg.snmpVarbs = append(msg.snmpVarbs, []SnmpVarBind{varb}...)
	return msg
}

//AddVarBindAt adds SNMP variable at specified index in PDUs list of variable bindings. Index starts from 0. Will not be added if index is less than 0 or index is greater than varBind slice length or an invalid OID is present in the varbind.
func (msg *SnmpMessage) AddVarBindAt(index int, varb SnmpVarBind) *SnmpMessage {
	if varb.ObjectID().Value() == nil || index < 0 || index > len(msg.snmpVarbs) {
		return msg
	}
	msg.snmpVarbs = append(msg.snmpVarbs[:index], append([]SnmpVarBind{varb}, msg.snmpVarbs[index:]...)...)
	return msg
}

//RemoveVarBindAt removes SNMP variable binding at specified index in PDUs list of variable bindings. Indexes start at 0.
func (msg *SnmpMessage) RemoveVarBindAt(index int) *SnmpMessage {
	if index < 0 || index >= len(msg.snmpVarbs) {
		return msg
	}
	msg.snmpVarbs = append(msg.snmpVarbs[:index], msg.snmpVarbs[index+1:]...)
	return msg
}

//RemoveVarBind removes specified SNMP variable binding from PDUs list of variable bindings.
func (msg *SnmpMessage) RemoveVarBind(varBind SnmpVarBind) *SnmpMessage {
	if varBind.ObjectID().Value != nil {
		for index, varb := range msg.snmpVarbs {
			if varb.Equals(varBind) {
				msg.snmpVarbs = append(msg.snmpVarbs[:index], msg.snmpVarbs[index+1:]...)
			}
		}
	}
	return msg
}

//VarBindAt returns specified variable binding from PDUs list of variable bindings. Indexes start at 0. Returns nil if index is less than 0 or index is greater than varBind array length.
func (msg *SnmpMessage) VarBindAt(index int) *SnmpVarBind {
	if index < 0 || index >= len(msg.snmpVarbs) {
		return nil
	}
	return &msg.snmpVarbs[index]
}

//ObjectIDAt returns the specified SNMP ObjectID from PDUs list of variable bindings. Indexes start at 0. Returns nil if index is less than 0 or index is greater than varBind array length.
func (msg *SnmpMessage) ObjectIDAt(index int) *snmpvar.SnmpOID {
	if index < 0 || index >= len(msg.snmpVarbs) {
		return nil
	}
	oid := msg.snmpVarbs[index].ObjectID()
	return &oid
}

//VariableAt returns specified SNMP variable from PDUs list of variables. Indexes start at 0. Returns nil if index is less than 0 or index is greater than varBind array length.
func (msg *SnmpMessage) VariableAt(index int) snmpvar.SnmpVar {
	if index < 0 || index >= len(msg.snmpVarbs) {
		return nil
	}
	return msg.snmpVarbs[index].Variable()
}

//SetVariableAt sets SNMP variable at specified index in PDUs list of variables, to value var. Indexes start at 0. Will not be set if the index is less than 0 or index is greater than varBind array length.
func (msg *SnmpMessage) SetVariableAt(index int, snmpVar snmpvar.SnmpVar) *SnmpMessage {
	if index < 0 || index >= len(msg.snmpVarbs) {
		return msg
	}
	if snmpVar == nil {
		snmpVar = snmpvar.NewSnmpNull()
	}
	msg.snmpVarbs[index].SetVariable(snmpVar)
	return msg
}

//VariableForOID returns specified SNMP variable from PDUs list of variables for the given OID. Returns nil if the OID is not present in the PDUs list of variable bindings.
func (msg *SnmpMessage) VariableForOID(oid snmpvar.SnmpOID) snmpvar.SnmpVar {
	for _, varbind := range msg.snmpVarbs {
		if varbind.ObjectID().Equals(oid) {
			return varbind.Variable()
		}
	}

	return nil
}

//Fix will remove the variable binding at the index specified by SnmpMessage's errorIndex.
func (msg *SnmpMessage) Fix() *SnmpMessage {
	if msg.errorIndex > 0 && int(msg.errorIndex) <= len(msg.VarBinds()) {
		msg.RemoveVarBindAt(int(msg.errorIndex) - 1)
	}
	return msg
}

//PrintVarBinds returns String form of all variable bindings with Tags, e.g. STRING:...
func (msg *SnmpMessage) PrintVarBinds() string {
	tagStringSlice := make([]string, len(msg.snmpVarbs))
	for key, varbs := range msg.snmpVarbs {
		tagStringSlice[key] = varbs.TagString()
	}
	return strings.Join(tagStringSlice, "\n")
}

//Copy makes a copy of entire SnmpMessage.
func (msg *SnmpMessage) Copy() *SnmpMessage {
	newMsg := new(SnmpMessage)
	*newMsg = *msg
	return newMsg
}

//CopyWithoutVarBinds makes a copy of entire SnmpMessage except the variable bindings.
func (msg *SnmpMessage) CopyWithoutVarBinds() *SnmpMessage {
	newMsg := new(SnmpMessage)
	*newMsg = *msg
	newMsg.SetVarBinds([]SnmpVarBind{})
	return newMsg
}

//Snmp3Message related setter getter methods. MsgID returns the message id for the SNMPV3 packet.
func (msg *SnmpMessage) MsgID() int32 {
	return msg.msgID
}

//SetMsgID sets the message id for the SNMPV3 packet.
func (msg *SnmpMessage) SetMsgID(id int32) *SnmpMessage {
	if id >= 0 {
		msg.msgID = id
	}
	return msg
}

//MsgMaxSize returns the message max size field for the SNMPV3 packet.
func (msg *SnmpMessage) MsgMaxSize() int32 {
	return msg.msgMaxSize
}

//SetMsgMaxSize sets the message max size field for the SNMPV3 packet.
func (msg *SnmpMessage) SetMsgMaxSize(maxSize int32) *SnmpMessage {
	msg.msgMaxSize = maxSize
	if maxSize < 484 {
		msg.msgMaxSize = 484
	}
	return msg
}

//MsgFlags returns the flags field in the SNMPV3 packet.
func (msg *SnmpMessage) MsgFlags() byte {
	return msg.msgFlags
}

//SetMsgFlags sets the falgs field in the SNMPV3 packet.
func (msg *SnmpMessage) SetMsgFlags(msgFlag byte) *SnmpMessage {
	if msgFlag == 0 || msgFlag == 1 || msgFlag == 3 || msgFlag == 4 || msgFlag == 5 || msgFlag == 7 {
		msg.msgFlags = msgFlag
	}
	return msg
}

//MsgSecurityModel returns the security model associated with this SNMPV3 message.
func (msg *SnmpMessage) MsgSecurityModel() int32 {
	return msg.msgSecurityModel
}

//SetMsgSecurityModel sets the security model associated with this SNMPV3 message.
func (msg *SnmpMessage) SetMsgSecurityModel(secModel int32) *SnmpMessage {
	if secModel >= 1 {
		msg.msgSecurityModel = secModel
	}
	return msg
}

//Sets the security parameters byte array on the msg. For internal use only.
func (msg *SnmpMessage) SetMsgSecurityParameters(secParams []byte) *SnmpMessage {
	msg.msgSecurityParameters = secParams
	return msg
}

//Returns the SecurityParameters on the msg as bytes. For internal use only.
func (msg *SnmpMessage) MsgSecurityParameters() []byte {
	return msg.msgSecurityParameters
}

//Sets the ScopedPDU portion of the msg. For SNMPv3 messages. For internal use only.
func (msg *SnmpMessage) SetScopedPDUData(scopedPDUData []byte) *SnmpMessage {
	msg.scopedPDUData = scopedPDUData
	return msg
}

//ScopedPDU portion of the msg is returned as bytes. For internal use only.
func (msg *SnmpMessage) ScopedPDUData() []byte {
	return msg.scopedPDUData
}

//Sets the ScopedPDU portion of the msg. For SNMPv3 messages. For internal use only.
func (msg *SnmpMessage) SetScopedPDU(spdu ScopedPDU) *SnmpMessage {
	msg.ScopedPDU = spdu
	return msg
}

//Indicates whether this message is sent synchronously or not. For internal use only.
func (msg *SnmpMessage) Sync() bool {
	return msg.syncFlag
}

//Sets the sync flag to indicate whether the msg is send synchronously or not. For internal use only.
func (msg *SnmpMessage) SetSync(sync bool) *SnmpMessage {
	msg.syncFlag = sync
	return msg
}

//For internal use only.
func (msg *SnmpMessage) OriginalRetries() int {
	return msg.originalRetries
}

//For internal use only.
func (msg *SnmpMessage) SetOriginalRetries(ret int) *SnmpMessage {
	msg.originalRetries = ret
	return msg
}

//For internal use only.
func (msg *SnmpMessage) TimeExpire() int {
	return msg.timeExpire
}

//For internal use only.
func (msg *SnmpMessage) SetTimeExpire(timeExpire int) *SnmpMessage {
	msg.timeExpire = timeExpire
	return msg
}

//For internal use only.
func (msg *SnmpMessage) Enqueue() bool {
	return msg.enqueue
}

//For internal use only.
func (msg *SnmpMessage) SetEnqueue(enq bool) *SnmpMessage {
	msg.enqueue = enq
	return msg
}

//Returns the encoded byte array of the msg if one exist. For internal use only.
func (msg *SnmpMessage) ProtocolData() []byte {
	return msg.protocolData
}

//Sets the raw bytes protocol data on the msg. For internal use only.
func (msg *SnmpMessage) SetProtocolData(data []byte) *SnmpMessage {
	if data != nil {
		msg.protocolData = data
	}
	return msg
}

//IsConfirmed returns bool indicating whether this msg belongs to Confirmed Class.
//
//Confirmed Class: Get/Get-Next/Set/Get-Bulk/Inform
func (msg *SnmpMessage) IsConfirmed() bool {
	return msg.SnmpPDU.IsConfirmed()
}

//For internal use only.
func (msg *SnmpMessage) SetAuthenticationFailure(authFailure bool) *SnmpMessage {
	msg.authFailure = authFailure
	return msg
}

//For internal use only.
func (msg *SnmpMessage) IsAuthenticationFailure() bool {
	return msg.authFailure
}

//GetID returns RequestID of the msg for v1/v2c and MsgID for v3. For internal use only.
func (msg *SnmpMessage) GetID() int32 {
	if msg.Version() == consts.Version3 {
		return msg.msgID
	} else {
		return msg.requestID
	}
}

//For internal use only.
func (msg *SnmpMessage) MaxSizeResponseScopedPDU() int32 {
	return msg.maxSizeResponseScopedPDU
}

//For internal use only.
func (msg *SnmpMessage) SetMaxSizeResponseScopedPDU(maxSize int32) *SnmpMessage {
	msg.maxSizeResponseScopedPDU = maxSize
	return msg
}

//To get the Hex octets from the byte array.
func printOctets(data []byte, length int) string {
	hexUnits := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"}
	ch := make([]string, length*3-1)
	length--
	j, i := 0, 0
	for i = 0; i < length; i++ {

		ch[j] = hexUnits[int(((data[i] & 0xf0) >> 4))]
		j++
		ch[j] = hexUnits[int((data[i] & 0x0f))]
		j++

		if (i+1)%20 == 0 {
			ch[j] = "\n"
			j++
		} else {
			ch[j] = " "
			j++
		}

	}
	ch[j] = hexUnits[int(((data[i] & 0xf0) >> 4))]
	j++
	ch[j] = hexUnits[int((data[i] & 0x0f))]
	j++

	var octetStr string = "0x "
	for _, str := range ch {
		octetStr += str
	}

	return octetStr
}
