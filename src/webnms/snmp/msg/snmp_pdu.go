/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"bytes"
	"strconv"
	"strings"

	"webnms/snmp/consts"
	"webnms/snmp/engine/transport"
	"webnms/snmp/snmpvar"
)

type Command int

//SnmpPDU represents the SNMP PDU used in protocol operations.
type SnmpPDU struct {
	command        consts.Command
	requestID      int32
	errorStatus    consts.ErrorStatus
	errorIndex     int32
	nonRepeaters   int32
	maxRepetitions int32
	enterprise     snmpvar.SnmpOID
	agentAddress   string
	genericType    int
	specificType   int
	upTime         uint32
	snmpVarbs      []SnmpVarBind

	securityLevel            consts.SecurityLevel
	maxSizeResponseScopedPDU int32
	protocolOptions          transport.ProtocolOptions
	protocolData             []byte //To store the byte array - internal use
	retries                  int
	timeout                  int
	originalRetries          int
	clientId                 int

	timeExpire  int
	syncFlag    bool
	enqueue     bool
	authFailure bool
}

//Command returns command type of this PDU. The command type is one of the constants defined in the snmp package.
func (pdu *SnmpPDU) Command() consts.Command {
	return pdu.command
}

//SetCommand sets command type of this PDU. The command type is one of the below constants defined in the snmp package.
//
//snmp.GetRequest, snmp.GetNextRequest, snmp.GetResponse, snmp.SetRequest, snmp.TrapRequest, snmp.GetBulkRequest, snmp.InformRequest, snmp.Trap2Request & snmp.ReportMes//sage
func (pdu *SnmpPDU) SetCommand(cmd consts.Command) *SnmpPDU {
	if cmd == consts.GetRequest || cmd == consts.GetNextRequest || cmd == consts.GetResponse || cmd == consts.SetRequest || cmd == consts.TrapRequest || cmd == consts.GetBulkRequest || cmd == consts.InformRequest || cmd == consts.Trap2Request || cmd == consts.ReportMessage {
		pdu.command = cmd
	}
	return pdu
}

//RequestID returns the request id of this PDU.
func (pdu *SnmpPDU) RequestID() int32 {
	return pdu.requestID
}

//SetRequestID sets the Request id for this PDU. If requestID is set to non-zero, the API leaves it alone. If it is 0, the API assigns a unique requestID to ensure no conflict with other requests. So, if you do not want to manage the request ids yourself, it should be 0. The default value is 0. If you are re-using the PDU and do not want to manage the requestID yourself, set it to 0 each time.
func (pdu *SnmpPDU) SetRequestID(reqID int32) *SnmpPDU {
	pdu.requestID = reqID
	return pdu
}

//ErrorStatus returns the error status of this PDU.
func (pdu *SnmpPDU) ErrorStatus() consts.ErrorStatus {
	return pdu.errorStatus
}

//SetErrorStatus sets the Error status for this PDU.
//
//The error status values that can be set using this method are,
//
//consts.NoError, consts.TooBig, consts.NoSuchName, consts.BadValue, consts.ReadOnly, consts.GeneralError, consts.NoAccess, consts.WrongType, consts.WrongLength, consts.WrongEncoding,
//consts.WrongValue, consts.NoCreation, consts.InconsistentValue, consts.ResourceUnavailable, consts.CommitFailed, consts.UndoFailed, consts.AuthorizationError,
//consts.NotWritable & consts.InconsistentName.
//
//It will ignore values other than the one mentioned above.
func (pdu *SnmpPDU) SetErrorStatus(errStat consts.ErrorStatus) *SnmpPDU {
	if errStat >= 0 && errStat <= 18 {
		pdu.errorStatus = errStat
	}
	return pdu
}

//ErrorIndex returns the error index of this PDU.
func (pdu *SnmpPDU) ErrorIndex() int32 {
	return pdu.errorIndex
}

//SetErrorIndex sets the error index of this PDU. Only values >= 0 && <=2147483647 will be accepted by this method.
func (pdu *SnmpPDU) SetErrorIndex(errIndex int32) *SnmpPDU {
	if errIndex >= 0 {
		pdu.errorIndex = errIndex
	}
	return pdu
}

//ErrorString returns error information as a String, with offending varbind if any.
func (pdu *SnmpPDU) ErrorString() string {
	errBuffer := bytes.NewBufferString("")
	if pdu.errorStatus != 0 {
		errBuffer.WriteString("Error in response. ")
		errBuffer.WriteString(errorString(pdu.errorStatus))
		errBuffer.WriteString("\nIndex: ")
		errBuffer.WriteString(strconv.FormatInt(int64(pdu.errorIndex), 10))
		errBuffer.WriteString("\n")
		if pdu.errorIndex > 0 && len(pdu.snmpVarbs) > int(pdu.errorIndex) {
			errBuffer.WriteString("Errored Object ID: ")
			errBuffer.WriteString(pdu.ObjectIDAt(int(pdu.errorIndex)).String())
			errBuffer.WriteString("\n")
		}
	}
	return errBuffer.String()
}

//NonRepeaters returns Non-Repeaters value of this PDU.
func (pdu *SnmpPDU) NonRepeaters() int32 {
	return pdu.nonRepeaters
}

//SetNonRepeaters sets Non-Repeaters value for this PDU.
func (pdu *SnmpPDU) SetNonRepeaters(nonRep int32) *SnmpPDU {
	if nonRep >= 0 {
		pdu.nonRepeaters = nonRep
	}
	return pdu
}

//MaxRepetitions returns Max-Repetitions value of this PDU.
func (pdu *SnmpPDU) MaxRepetitions() int32 {
	return pdu.maxRepetitions
}

//SetMaxRepetitions sets Max-Repetitions value for this PDU. The valid values ranges from 0 to 65535(both inclusive).
func (pdu *SnmpPDU) SetMaxRepetitions(maxRep int32) *SnmpPDU {
	if maxRep >= 0 {
		pdu.maxRepetitions = maxRep
	}
	return pdu
}

//Enterprise returns enterprise OID, i.e., returns the enterprise field, of the Trap-PDU.
func (pdu *SnmpPDU) Enterprise() snmpvar.SnmpOID {
	return pdu.enterprise
}

//SetEnterprise sets enterprise OID, i.e., the enterprise field, of the Trap-PDU.
func (pdu *SnmpPDU) SetEnterprise(oid snmpvar.SnmpOID) *SnmpPDU {
	pdu.enterprise = oid
	return pdu
}

//AgentAddress returns address of object generating trap. In the case of SNMPv2 TRAP PDU if one of the varbind contains the snmpTrapAddress.0 oid the corresponding value is returned as the agent address.
func (pdu *SnmpPDU) AgentAddress() string {
	return pdu.agentAddress
}

//SetAgentAddress sets address of object generating trap. In the case of SNMPv2c TRAP PDU an additional varbind is added, if its not already present in the varbind list. The varbind contains the snmpTrapAddress.0 (defined in RFC2576) as the oid and the value (SnmpVar) will be the agent address. There is no need to add this varbind explicitly.
func (pdu *SnmpPDU) SetAgentAddress(address string) *SnmpPDU {
	pdu.agentAddress = address
	return pdu
}

//GenericType returns generic trap type.
func (pdu *SnmpPDU) GenericType() int {
	return pdu.genericType
}

//SetGenericType sets generic trap type of this PDU. This method will accept values only from 0 to 6(both inclusive).
func (pdu *SnmpPDU) SetGenericType(genType int) *SnmpPDU {
	if genType >= 0 && genType < 7 {
		pdu.genericType = genType
	}
	return pdu
}

//SpecificType returns specific trap type.
func (pdu *SnmpPDU) SpecificType() int {
	return pdu.specificType
}

//SetSpecificType sets specific trap type.
func (pdu *SnmpPDU) SetSpecificType(specType int) *SnmpPDU {
	pdu.specificType = specType
	return pdu
}

//UpTime returns the timeStamp of the object which has generated the trap. The timeStamp value is in milliseconds.
//
//This will return the value that is present in the time-stamp field present in a SNMPv1 trap-pdu. In case of a SNMPv2 trap-pdu or Inform-pdu, this method will return the sysUpTime value that is present in the first varbind. If this PDU is not a trap PDU, then this method will return '0'.
func (pdu *SnmpPDU) UpTime() uint32 {
	uptime := pdu.upTime

	if pdu.command == consts.Trap2Request || pdu.command == consts.InformRequest {
		uptime = 0
		if len(pdu.snmpVarbs) > 0 {
			varBind := pdu.snmpVarbs[0]
			if strings.EqualFold(varBind.ObjectID().String(), ".1.3.6.1.2.1.1.3.0") {
				switch varBind.Variable().(type) {
				case snmpvar.SnmpTimeTicks:
					tickvar := varBind.Variable().(snmpvar.SnmpTimeTicks)
					uptime = tickvar.Value()
				default:
					uptime = 0
				}
			}
		}
	}

	return uptime
}

//SetUpTime sets the timeStamp of the object which has to generate the trap. The timeStamp value is in milliseconds.
func (pdu *SnmpPDU) SetUpTime(uptime uint32) *SnmpPDU {
	pdu.upTime = uptime
	return pdu
}

//TrapOID returns the trap-oid that will be present in the second variable binding of this SNMPv2 trap-pdu. If this is not a SNMPv2 trap-pdu, then the method will return nil.
func (pdu *SnmpPDU) TrapOID() *snmpvar.SnmpOID {
	if pdu.command == consts.Trap2Request || pdu.command == consts.InformRequest {
		if len(pdu.snmpVarbs) > 1 {
			varBind := pdu.snmpVarbs[1]
			if strings.EqualFold(varBind.ObjectID().String(), ".1.3.6.1.6.3.1.1.4.1.0") {
				switch varBind.Variable().(type) {
				case snmpvar.SnmpOID:
					oidvar := varBind.Variable().(snmpvar.SnmpOID)
					return &oidvar
				case *snmpvar.SnmpOID:
					oidvar := varBind.Variable().(*snmpvar.SnmpOID)
					return oidvar
				default:
					return nil
				}
			}
		}
	}
	return nil
}

//VarBinds returns list of SnmpVarBind in this PDU as a slice.
func (pdu *SnmpPDU) VarBinds() []SnmpVarBind {
	return pdu.snmpVarbs
}

//SetVarBinds sets the list of SnmpVarBind to this PDU
func (pdu *SnmpPDU) SetVarBinds(varbs []SnmpVarBind) *SnmpPDU {
	pdu.snmpVarbs = varbs
	return pdu
}

//AddNull adds a vairable binding which contains this SnmpOID and a placeholder ( NULL ) as the value.
//Only non-null, valid SnmpOID will be added to the list of variable bindings. This method is used while doing a GET or GETNEXT request.
func (pdu *SnmpPDU) AddNull(oid snmpvar.SnmpOID) *SnmpPDU {
	nullVarb := NewSnmpVarBind(oid, snmpvar.NewSnmpNull())
	nullVarbSlice := []SnmpVarBind{nullVarb}
	pdu.snmpVarbs = append(pdu.snmpVarbs, nullVarbSlice...)
	return pdu
}

//AddVarBind adds SNMP variable at the end of PDU's list of variable bindings.
func (pdu *SnmpPDU) AddVarBind(varb SnmpVarBind) *SnmpPDU {
	pdu.snmpVarbs = append(pdu.snmpVarbs, []SnmpVarBind{varb}...)
	return pdu
}

//AddVarBindAt adds SNMP variable at specified index in PDUs list of variable bindings. Index starts from 0.
//Will not be added if index is less than 0 or index is greater than varBind slice length or an invlaid OID is present in the varbind.
func (pdu *SnmpPDU) AddVarBindAt(index int, varb SnmpVarBind) *SnmpPDU {
	if varb.ObjectID().Value() == nil || index < 0 || index > len(pdu.snmpVarbs) {
		return pdu
	}
	pdu.snmpVarbs = append(pdu.snmpVarbs[:index], append([]SnmpVarBind{varb}, pdu.snmpVarbs[index:]...)...)
	return pdu
}

//RemoveVarBindAt removes SNMP variable binding at specified index in PDUs list of variable bindings. Indexes start at 0.
func (pdu *SnmpPDU) RemoveVarBindAt(index int) *SnmpPDU {
	if index < 0 || index >= len(pdu.snmpVarbs) {
		return pdu
	}
	pdu.snmpVarbs = append(pdu.snmpVarbs[:index], pdu.snmpVarbs[index+1:]...)
	return pdu
}

//RemoveVarBind removes specified SNMP variable binding from PDUs list of variable bindings.
func (pdu *SnmpPDU) RemoveVarBind(varBind SnmpVarBind) *SnmpPDU {
	if varBind.ObjectID().Value() != nil {
		for index, varb := range pdu.snmpVarbs {
			if varb.Equals(varBind) {
				pdu.snmpVarbs = append(pdu.snmpVarbs[:index], pdu.snmpVarbs[index+1:]...)
			}
		}
	}
	return pdu
}

//VarBindAt returns specified variable binding from PDUs list of variable bindings. Indexes start at 0.
//Returns nil if index is less than 0 or index is greater than varBind array length.
func (pdu *SnmpPDU) VarBindAt(index int) *SnmpVarBind {
	if index < 0 || index >= len(pdu.snmpVarbs) {
		return nil
	}
	return &pdu.snmpVarbs[index]
}

//ObjectIDAt returns the specified SNMP ObjectID from PDUs list of variable bindings. Indexes start at 0.
//Returns nil if index is less than 0 or index is greater than varBind array length.
func (pdu *SnmpPDU) ObjectIDAt(index int) *snmpvar.SnmpOID {
	if index < 0 || index >= len(pdu.snmpVarbs) {
		return nil
	}
	oid := pdu.snmpVarbs[index].ObjectID()
	return &oid
}

//VariableAt returns specified SNMP variable from PDUs list of variables. Indexes start at 0.
//Returns nil if index is less than 0 or index is greater than varBind array length.
func (pdu *SnmpPDU) VariableAt(index int) snmpvar.SnmpVar {
	if index < 0 || index >= len(pdu.snmpVarbs) {
		return nil
	}
	return pdu.snmpVarbs[index].Variable()
}

//SetVariableAt sets SNMP variable at specified index in PDUs list of variables, to value var. Indexes start at 0.
//Will not be set if the index is less than 0 or index is greater than varBind array length.
func (pdu *SnmpPDU) SetVariableAt(index int, snmpVar snmpvar.SnmpVar) *SnmpPDU {
	if index < 0 || index >= len(pdu.snmpVarbs) {
		return pdu
	}
	if snmpVar == nil {
		snmpVar = snmpvar.NewSnmpNull()
	}
	pdu.snmpVarbs[index].SetVariable(snmpVar)
	return pdu
}

//VariableForOID returns specified SNMP variable from PDUs list of variables for the given OID.
//Returns nil if the OID is not present in the PDUs list of variable bindings.
func (pdu *SnmpPDU) VariableForOID(oid snmpvar.SnmpOID) snmpvar.SnmpVar {
	for _, varbind := range pdu.snmpVarbs {
		if varbind.ObjectID().Equals(oid) {
			return varbind.Variable()
		}
	}

	return nil
}

//IsConfirmed returns bool indicating whether this msg belongs to Confirmed Class.
//
//Confirmed Class: Get/Get-Next/Set/Get-Bulk/Inform
func (pdu *SnmpPDU) IsConfirmed() bool {
	cmd := pdu.command
	if cmd == consts.GetRequest ||
		cmd == consts.GetNextRequest ||
		cmd == consts.GetBulkRequest ||
		cmd == consts.SetRequest ||
		cmd == consts.InformRequest {

		return true
	}

	return false
}
