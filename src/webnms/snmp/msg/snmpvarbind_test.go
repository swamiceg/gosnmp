/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"strings"
	"testing"

	"webnms/snmp/consts"
	"webnms/snmp/snmpvar"
)

func TestNewVarBind(t *testing.T) {

	//test NewSnmpVarBind method
	oid := snmpvar.NewSnmpOID("1.5.0")
	variable, err := snmpvar.CreateSnmpVar(consts.OctetString, "something")
	if err != nil {
		t.Error("testing failed. error while creating variable")
	}
	varb := NewSnmpVarBind(*oid, variable)
	if varb.Variable() == nil {
		t.Error("testing failed. Varbind not created,", varb)
	}

	//test by passing nil as variable to NewSnmpVarBind
	varb = NewSnmpVarBind(*oid, nil)
	if varb.Variable() == nil {
		t.Error("testing failed. Varbind not created,", varb)
	}

	//test for ObjectID method
	if !strings.EqualFold(varb.ObjectID().String(), ".1.3.6.1.2.1.1.5.0") {
		t.Error("Expected .1.3.6.1.2.1.1.5.0 got", varb.ObjectID())
	}

	//test for Variable method
	if varb.Variable() == nil || !strings.EqualFold(varb.Variable().String(), "NULL") {
		t.Error("Expected NULL got", varb.Variable())
	}

	//test for SetObjectID method
	varb.SetObjectID(*snmpvar.NewSnmpOID("1.2.0"))
	varb.SetVariable(snmpvar.NewSnmpString("something"))
	if !strings.EqualFold(varb.ObjectID().String(), ".1.3.6.1.2.1.1.2.0") {
		t.Error("Expected .1.3.6.1.2.1.1.2.0 got", varb.ObjectID())
	}
	//test for SetVariable method
	if !strings.EqualFold(varb.Variable().String(), "something") {
		t.Error("Expected something got", varb.Variable())
	}
	//test for SetVariable method by passing nil argument
	varb.SetVariable(nil)
	if varb.Variable() == nil || !strings.EqualFold(varb.Variable().String(), "NULL") {
		t.Error("Expected NULL got", varb.Variable())
	}

	//test for ExcecptionIndex method
	if varb.ExceptionIndex() != 0 {
		t.Error("ExceptionIndex should be 0")
	}

	//test for SetExceptionIndex method
	varb.SetExceptionIndex(2)
	if varb.ExceptionIndex() != 2 {
		t.Error("Set ExceptionIndex value to 2 failed, got", varb.ExceptionIndex())
	}

	//test for String method
	varb.SetVariable(snmpvar.NewSnmpString("something"))
	if !strings.EqualFold(varb.String(), ".1.3.6.1.2.1.1.2.0 : something") {
		t.Error("Expected string representation of SnmpVarBind to be .1.3.6.1.2.1.1.2.0 : something, instead got,", varb.String())
	}

	//test for TagString method
	if !strings.EqualFold(varb.TagString(), "Object ID : .1.3.6.1.2.1.1.2.0\nSTRING : something") {
		t.Error("Expected string representation of SnmpVarBind to be\nObject ID: .1.3.6.1.2.1.1.2.0\nSTRING: something\ninstead got", varb.TagString())
	}

	//test for Equals method by passing two same varbinds.
	varb1 := NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something"))
	if !varb.Equals(varb1) {
		t.Error(varb, ":", varb1)
	}

	//test for Equals method by passing two different varbinds.
	varb1 = NewSnmpVarBind(*snmpvar.NewSnmpOID("1.5.0"), snmpvar.NewSnmpInt(100))
	if varb.Equals(varb1) {
		t.Error(varb, ":", varb1)
	}
}
