/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
	"webnms/snmp/snmpvar"
)

func newScopedPDU() *ScopedPDU {

	spdu := new(ScopedPDU)
	return spdu
}

func TestSPDUPacketFields(t *testing.T) {

	spdu := newScopedPDU()

	//tests for command field
	if spdu.Command() != 0 {
		t.Error("scoped-pdu's default command should be 0 instead it is", spdu.Command())
	}

	spdu.SetCommand(consts.GetRequest)
	if spdu.Command() != consts.GetRequest {
		t.Error("scoped-pdu's command should be", consts.GetRequest, "instead it is", spdu.Command())
	}

	spdu.SetCommand(250)
	if spdu.Command() == 250 {
		t.Error("scoped-pdu's command cannot be 250")
	}

	//tests for requestID field
	if spdu.RequestID() != 0 {
		t.Error("scoped-pdu's default requestID value should be 0 instead it is", spdu.RequestID())
	}

	spdu.SetRequestID(10)
	if spdu.RequestID() != 10 {
		t.Error("scoped-pdu's requestID should be 10 instead it is", spdu.RequestID())
	}

	//tests for ErrorStatus field
	if spdu.ErrorStatus() != 0 {
		t.Error("scoped-pdu's default error-status value should be 0 instead it is", spdu.ErrorStatus())
	}

	spdu.SetErrorStatus(consts.WrongLength)
	if spdu.ErrorStatus() != consts.WrongLength {
		t.Error("scoped-pdu's error-status should be", consts.WrongLength, "instead it is", spdu.ErrorStatus())
	}

	spdu.SetErrorStatus(25)
	if spdu.ErrorStatus() == 25 {
		t.Error("scoped-pdu's error-status value should be in range 0-18, instead it is 25")
	}

	values := []int32{-1}

	//tests for ErrorIndex field
	if spdu.ErrorIndex() != 0 {
		t.Error("scoped-pdu's default error-index value should be 0 instead it is", spdu.ErrorIndex())
	}

	spdu.SetErrorIndex(2)
	if spdu.ErrorIndex() != 2 {
		t.Error("scoped-pdu's error-index should be 2 instead it is", spdu.ErrorIndex())
	}

	for _, value := range values {
		spdu.SetErrorIndex(value)
		if spdu.ErrorIndex() == value {
			t.Error("scoped-pdu's error-index should not be set to an invalid value,", value)
		}
	}

	//test for ErrorString method
	if !strings.HasPrefix(spdu.ErrorString(), "Error in response. ") {
		t.Error("scoped-pdu's ErrorString should start with the message 'Error in response.'")
	}

	//tests for Non-Repeaters field
	if spdu.NonRepeaters() != 0 {
		t.Error("scoped-pdu's default NonRepeaters value should be 0 instead it is", spdu.NonRepeaters())
	}

	spdu.SetNonRepeaters(5)
	if spdu.NonRepeaters() != 5 {
		t.Error("scoped-pdu's NonRepeaters value should be 5 instead it is", spdu.NonRepeaters())
	}

	for _, value := range values {
		spdu.SetNonRepeaters(value)
		if spdu.NonRepeaters() == value {
			t.Error("scoped-pdu's NonRepeaters should not be set to an invalid value,", value)
		}
	}

	//tests for MaxRep field
	if spdu.MaxRepetitions() != 0 {
		t.Error("scoped-pdu's default MaxRepetitions value should be 0 instead it is", spdu.MaxRepetitions())
	}

	spdu.SetMaxRepetitions(5)
	if spdu.MaxRepetitions() != 5 {
		t.Error("scoped-pdu's MaxRepetitions value should be 5 instead it is", spdu.MaxRepetitions())
	}

	for _, value := range values {
		spdu.SetMaxRepetitions(value)
		if spdu.MaxRepetitions() == value {
			t.Error("scoped-pdu's MaxRepetitions should not be set to an invalid value,", value)
		}
	}
}

func TestScopedPDUTrapFields(t *testing.T) {

	spdu := newScopedPDU()

	//tests for enterprise field
	if !strings.EqualFold(spdu.Enterprise().String(), "") {
		t.Error("scoped-pdu's default Enterprise value should be empty instead it is", spdu.Enterprise())
	}

	spdu.SetEnterprise(*snmpvar.NewSnmpOID("1.5.0"))
	if !strings.EqualFold(spdu.Enterprise().String(), ".1.3.6.1.2.1.1.5.0") {
		t.Error("scoped-pdu's Enterprise value should be .1.3.6.1.2.1.1.5.0 instead it is", spdu.Enterprise())
	}

	//tests for agent-address field
	if !strings.EqualFold(spdu.AgentAddress(), "") {
		t.Error("scoped-pdu's default agent-address value should be empty instead it is", spdu.AgentAddress())
	}

	spdu.SetAgentAddress("1.1.1.1")
	if !strings.EqualFold(spdu.AgentAddress(), "1.1.1.1") {
		t.Error("scoped-pdu's agent-address value should be 1.1.1.1 instead it is", spdu.Enterprise())
	}

	spdu.SetAgentAddress("1.1.1.1.1")
	if !strings.EqualFold(spdu.AgentAddress(), "1.1.1.1.1") {
		t.Error("scoped-pdu's agent-address value should be 1.1.1.1.1 instead it is", spdu.Enterprise())
	}

	//tests for generic-type field
	if spdu.GenericType() != 0 {
		t.Error("scoped-pdu's default generic-type value should be 0 instead it is", spdu.GenericType())
	}

	spdu.SetGenericType(2)
	if spdu.GenericType() != 2 {
		t.Error("scoped-pdu's generic-type value should be 2 instead it is", spdu.GenericType())
	}

	spdu.SetGenericType(10)
	if spdu.GenericType() == 10 {
		t.Error("scoped-pdu's generic-type should be in range 0-7, instead it is tried to set as 10")
	}

	//tests for specific-type field
	if spdu.SpecificType() != 0 {
		t.Error("scoped-pdu's default specific-type value should be 0 instead it is", spdu.SpecificType())
	}

	spdu.SetSpecificType(2)
	if spdu.SpecificType() != 2 {
		t.Error("scoped-pdu's specific-type value should be 2 instead it is", spdu.SpecificType())
	}

	//tests for upTime field
	if spdu.UpTime() != 0 {
		t.Error("scoped-pdu's default uptime value should be 0 instead it is", spdu.UpTime())
	}

	spdu.SetUpTime(1000)
	if spdu.UpTime() != 1000 {
		t.Error("scoped-pdu's uptime value should be 1000 instead it is", spdu.UpTime())
	}

	//tests for TrapOID && UpTime method by adding varbinds
	spdu.SetCommand(consts.Trap2Request)
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.2.1.1.3.0"), snmpvar.NewSnmpTimeTicks(10000)))
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.6.3.1.1.4.1.0"), snmpvar.NewSnmpOID("1.4.0")))

	if spdu.UpTime() != 10000 {
		t.Error("scoped-pdu's uptime value should be 10000 instead it is", spdu.UpTime())
	}

	if spdu.TrapOID() != nil && !strings.EqualFold(spdu.TrapOID().String(), ".1.3.6.1.2.1.1.4.0") {
		t.Error("scoped-pdu's trapoid value should be .1.3.6.1.2.1.1.4.0 instead it is,", spdu.TrapOID())
	}
}

func TestScopedPDUVarBinds(t *testing.T) {

	spdu := newScopedPDU()

	//test for AddNull method
	spdu.AddNull(*snmpvar.NewSnmpOID("1.5.0"))
	if spdu.VarBinds() != nil {
		if !strings.EqualFold(spdu.ObjectIDAt(0).String(), ".1.3.6.1.2.1.1.5.0") {
			t.Error("OID 1.5.0 not added using AddNull method")
		}
	} else {
		t.Error("OID 1.5.0 not added using AddNull method")
	}

	//test for AddVarBind method
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	if spdu.VarBinds() != nil {
		if !strings.EqualFold(spdu.VarBindAt(1).String(), ".1.3.6.1.2.1.1.1.0 : something") {
			t.Error("Varbind not added using AddVarBind method")
		}
	} else {
		t.Error("Varbind not added using AddVarBind method")
	}

	indices := []int{-1, 10}
	var beforeLen, afterLen int

	//test for AddVarBindAt method
	spdu.AddVarBindAt(1, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	if spdu.VarBinds() != nil {
		if !strings.EqualFold(spdu.VarBindAt(1).String(), ".1.3.6.1.2.1.1.2.0 : something") {
			t.Error("Varbind not added using AddVarBindAt method at position 1")
		}
	} else {
		t.Error("Varbind not added using AddVarBindAt method at position 1")
	}

	for _, index := range indices {
		beforeLen = len(spdu.VarBinds())
		spdu.AddVarBindAt(index, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.3.0"), snmpvar.NewSnmpString("something")))
		afterLen = len(spdu.VarBinds())
		if afterLen-beforeLen == 1 {
			t.Error("Varbind should not be added for invalid index,", index, "is given for AddVarBindAt")
		}
	}

	//tests for RemoveVarBindAt method
	beforeLen = len(spdu.VarBinds())
	spdu.RemoveVarBindAt(1)
	afterLen = len(spdu.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind not removed for valid index 1")
	}

	for _, index := range indices {
		beforeLen = len(spdu.VarBinds())
		spdu.RemoveVarBindAt(index)
		afterLen = len(spdu.VarBinds())
		if beforeLen-afterLen == 1 {
			t.Error("Varbind should not be removed if invalid index,", index, "is given for RemoveVarBindAt")
		}
	}

	//tests for RemoveVarBind method
	beforeLen = len(spdu.VarBinds())
	spdu.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(spdu.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.1.0 : something not removed")
	}

	beforeLen = len(spdu.VarBinds())
	spdu.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(spdu.VarBinds())
	if beforeLen-afterLen == 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.4.0 : something should not be removed")
	}

	//tests for VarBindAt, ObjectAt & VariableAt methods
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpInt(10)))
	spdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.7.0"), snmpvar.NewSnmpOID("1.1.0")))

	if spdu.VarBindAt(1) == nil {
		t.Error("VarBind not returned for valid index : 1")
	}
	if spdu.ObjectIDAt(1) == nil {
		t.Error("ObjectID not returned for valid index : 1")
	}
	if spdu.VariableAt(1) == nil {
		t.Error("Variable not returned for valid index : 1")
	}

	for _, index := range indices {
		if spdu.VarBindAt(index) != nil {
			t.Error("VarBind should not be returned for invalid index :", index)
		}
		if spdu.ObjectIDAt(index) != nil {
			t.Error("ObjectID should not be returned for invalid index :", index)
		}
		if spdu.VariableAt(index) != nil {
			t.Error("Variable should not be returned for invalid index :", index)
		}
	}

	//tests for VariableForOID method
	if spdu.VariableForOID(*snmpvar.NewSnmpOID("1.4.0")) == nil {
		t.Error("Expected VariableFor OID 1.4.0 not to be nil")
	}
	if spdu.VariableForOID(*snmpvar.NewSnmpOID("1.3.0")) != nil {
		t.Error("Expected VariableFor OID 1.3.0 to be nil")
	}

	//tests for SetVaraibleAt method
	spdu.SetVariableAt(0, snmpvar.NewSnmpIp("1.1.1.1"))
	if !strings.EqualFold(spdu.VariableAt(0).String(), "1.1.1.1") {
		t.Error("VariableAt 0 should be 1.1.1.1 instead it is,", spdu.VariableAt(0))
	}

	for _, index := range indices {
		spdu.SetVariableAt(index, snmpvar.NewSnmpInt(1000))
		if spdu.VariableAt(index) != nil {
			t.Error("Variable should not be set for invalid index", index)
		}
	}

	//tests for IsConfirmed method
	if spdu.IsConfirmed() {
		t.Error("scoped-pdu's default is-confirmed flag should be false")
	}

	commands := []consts.Command{consts.GetRequest, consts.GetNextRequest, consts.GetResponse, consts.SetRequest, consts.TrapRequest,
		consts.GetBulkRequest, consts.InformRequest, consts.Trap2Request, consts.ReportMessage}
	for _, command := range commands {
		spdu.SetCommand(command)
		switch command {
		case consts.GetRequest, consts.GetNextRequest, consts.SetRequest, consts.GetBulkRequest, consts.InformRequest:
			if !spdu.IsConfirmed() {
				t.Error("message's is-confirmed flag should be true for command :", command)
			}
		case consts.GetResponse, consts.TrapRequest, consts.Trap2Request, consts.ReportMessage:
			if spdu.IsConfirmed() {
				t.Error("message's is-confirmed flag should be false for command :", command)
			}
		default:
			t.Error("Invalid command set for SetCommand method")
		}
	}
}

func TestScopedPDUV3Fields(t *testing.T) {

	spdu := newScopedPDU()

	//tests for security level field
	if spdu.SnmpPDU.securityLevel != 0 {
		t.Error("scoped-pdu's default security level should be 0, instead it is,", spdu.SnmpPDU.securityLevel)
	}
	spdu.SetSecurityLevel(1)
	if spdu.SnmpPDU.securityLevel != 1 {
		t.Error("scoped-pdu's security level should be 1, instead it is,", spdu.SnmpPDU.securityLevel)
	}
	spdu.SetSecurityLevel(5)
	if spdu.SnmpPDU.securityLevel == 5 {
		t.Error("scoped-pdu's security level should be 0, 1 or 3")
	}

	//tests for ContextEngineID field
	if !bytes.EqualFold(spdu.ContextEngineID(), []byte{}) {
		t.Error("scoped-pdu's default context engine-id should be []", spdu.ContextEngineID())
	}
	spdu.SetContextEngineID([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9})
	if !bytes.EqualFold(spdu.ContextEngineID(), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}) {
		t.Error("scoped-pdu's context engine-id should be [1 2 3 4 5 6 7 8 9]", spdu.ContextEngineID())
	}

	//tests for ContextName field
	if !strings.EqualFold(spdu.ContextName(), "") {
		t.Error("scoped-pdu's default contextname should be empty-string, instead it is,", spdu.ContextName())
	}
	spdu.SetContextName("priv")
	if !strings.EqualFold(spdu.ContextName(), "priv") {
		t.Error("scoped-pdu's contextname should be priv, instead it is,", spdu.ContextName())
	}
}
