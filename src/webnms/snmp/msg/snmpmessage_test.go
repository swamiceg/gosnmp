/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
	"webnms/snmp/snmpvar"
)

func TestMessagePacketFields(t *testing.T) {

	msg := NewSnmpMessage()

	//tests for both valid and invalid values for SetVersion. test for Version method included too.
	if msg.Version() != consts.Version(-1) {
		t.Error("Default value for message version should be", consts.Version1, "instead it is", msg.Version())
	}

	msg.SetVersion(consts.Version2C)
	if msg.Version() != consts.Version2C {
		t.Error("Message version should be", consts.Version2C, "instead it is", msg.Version())
	}

	msg.SetVersion(consts.Version(5))
	if msg.Version() == consts.Version(5) {
		t.Error("Message version should not be", consts.Version(5))
	}

	//tests for community field of SnmpMessage
	if !strings.EqualFold(msg.Community(), "<nil>") {
		t.Error("Default value for should be <nil> instead it is", msg.Community())
	}

	msg.SetCommunity("something")
	if !strings.EqualFold(msg.Community(), "something") {
		t.Error("Message community should be something instead it is", msg.Community())
	}

	//tests for command field
	if msg.Command() != 0 {
		t.Error("Default message command value should be 0 instead it is", msg.Command())
	}

	msg.SetCommand(consts.GetRequest)
	if msg.Command() != consts.GetRequest {
		t.Error("Message command should be", consts.GetRequest, "instead it is", msg.Command())
	}

	msg.SetCommand(250)
	if msg.Command() == 250 {
		t.Error("Message's command should not be 250")
	}

	//tests for requestID field
	if msg.RequestID() != 0 {
		t.Error("Message's default requestID value should be 0 instead it is", msg.RequestID())
	}

	msg.SetRequestID(10)
	if msg.RequestID() != 10 {
		t.Error("Message's requestID should be 10 instead it is", msg.RequestID())
	}

	//tests for ErrorStatus field
	if msg.ErrorStatus() != 0 {
		t.Error("Message's default error-status value should be 0 instead it is", msg.ErrorStatus())
	}

	msg.SetErrorStatus(consts.WrongLength)
	if msg.ErrorStatus() != consts.WrongLength {
		t.Error("Message's error-status should be", consts.WrongLength, "instead it is", msg.ErrorStatus())
	}

	msg.SetErrorStatus(25)
	if msg.ErrorStatus() == 25 {
		t.Error("Message's error-status value should be in range 0-18, instead it is 25")
	}

	values := []int32{-1}

	//tests for ErrorIndex field
	if msg.ErrorIndex() != 0 {
		t.Error("Message's default error-index value should be 0 instead it is", msg.ErrorIndex())
	}

	msg.SetErrorIndex(2)
	if msg.ErrorIndex() != 2 {
		t.Error("Message's error-index should be 2 instead it is", msg.ErrorIndex())
	}

	for _, value := range values {
		msg.SetErrorIndex(value)
		if msg.ErrorIndex() == value {
			t.Error("Message's error-index should not be set to an invalid value,", value)
		}
	}

	if !strings.HasPrefix(msg.ErrorString(), "Error in response. ") {
		t.Error("Message's ErrorString should start with the message 'Error in response.'")
	}

	//tests for ErrorMessage method
	if !strings.EqualFold(ErrorMessage(2), "There is no such variable name in this MIB.") {
		t.Error("Expected 'There is no such variable name in this MIB.' but got", ErrorMessage(2))
	}

	if !strings.EqualFold(ErrorMessage(25), "Unknown error code: 25") {
		t.Error("Expected 'Unknown error code: 25' but got", ErrorMessage(25))
	}

	//tests for Non-Repeaters field
	if msg.NonRepeaters() != 0 {
		t.Error("Message's default NonRepeaters value should be 0 instead it is", msg.NonRepeaters())
	}

	msg.SetNonRepeaters(5)
	if msg.NonRepeaters() != 5 {
		t.Error("Message's NonRepeaters value should be 5 instead it is", msg.NonRepeaters())
	}

	for _, value := range values {
		msg.SetNonRepeaters(value)
		if msg.NonRepeaters() == value {
			t.Error("Message's NonRepeaters should not be set to an invalid value,", value)
		}
	}

	//tests for MaxRep field
	if msg.MaxRepetitions() != 0 {
		t.Error("Message's default MaxRepetitions value should be 0 instead it is", msg.MaxRepetitions())
	}

	msg.SetMaxRepetitions(5)
	if msg.MaxRepetitions() != 5 {
		t.Error("Message's MaxRepetitions value should be 5 instead it is", msg.MaxRepetitions())
	}

	for _, value := range values {
		msg.SetMaxRepetitions(value)
		if msg.MaxRepetitions() == value {
			t.Error("Message's MaxRepetitions should not be set to an invalid value,", value)
		}
	}
}

func TestMessageTrapFields(t *testing.T) {

	msg := NewSnmpMessage()

	//tests for enterprise field
	if !strings.EqualFold(msg.Enterprise().String(), "") {
		t.Error("Message's default Enterprise value should be empty instead it is", msg.Enterprise())
	}

	msg.SetEnterprise(*snmpvar.NewSnmpOID("1.5.0"))
	if !strings.EqualFold(msg.Enterprise().String(), ".1.3.6.1.2.1.1.5.0") {
		t.Error("Message's Enterprise value should be .1.3.6.1.2.1.1.5.0 instead it is", msg.Enterprise())
	}

	//tests for agent-address field
	if !strings.EqualFold(msg.AgentAddress(), "") {
		t.Error("Message's default agent-address value should be empty instead it is", msg.AgentAddress())
	}

	msg.SetAgentAddress("1.1.1.1")
	if !strings.EqualFold(msg.AgentAddress(), "1.1.1.1") {
		t.Error("Message's agent-address value should be 1.1.1.1 instead it is", msg.Enterprise())
	}

	msg.SetAgentAddress("1.1.1.1.1")
	if !strings.EqualFold(msg.AgentAddress(), "1.1.1.1.1") {
		t.Error("Message's agent-address value should be 1.1.1.1.1 instead it is", msg.Enterprise())
	}

	//tests for generic-type field
	if msg.GenericType() != 0 {
		t.Error("Message's default generic-type value should be 0 instead it is", msg.GenericType())
	}

	msg.SetGenericType(2)
	if msg.GenericType() != 2 {
		t.Error("Message's generic-type value should be 2 instead it is", msg.GenericType())
	}

	msg.SetGenericType(10)
	if msg.GenericType() == 10 {
		t.Error("Message's generic-type should be in range 0-7, instead it is tried to set as 10")
	}

	//tests for specific-type field
	if msg.SpecificType() != 0 {
		t.Error("Message's default specific-type value should be 0 instead it is", msg.SpecificType())
	}

	msg.SetSpecificType(2)
	if msg.SpecificType() != 2 {
		t.Error("Message's specific-type value should be 2 instead it is", msg.SpecificType())
	}

	//tests for upTime field
	if msg.UpTime() != 0 {
		t.Error("Message's default uptime value should be 0 instead it is", msg.UpTime())
	}

	msg.SetUpTime(1000)
	if msg.UpTime() != 1000 {
		t.Error("Message's uptime value should be 1000 instead it is", msg.UpTime())
	}

	//tests for TrapOID && UpTime method by adding varbinds
	msg.SetVersion(consts.Version2C)
	msg.SetCommand(consts.Trap2Request)
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.2.1.1.3.0"), snmpvar.NewSnmpTimeTicks(10000)))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.6.3.1.1.4.1.0"), snmpvar.NewSnmpOID("1.4.0")))

	if msg.UpTime() != 10000 {
		t.Error("Message's uptime value should be 10000 instead it is", msg.UpTime())
	}

	if msg.TrapOID() != nil && !strings.EqualFold(msg.TrapOID().String(), ".1.3.6.1.2.1.1.4.0") {
		t.Error("Message's trapoid value should be .1.3.6.1.2.1.1.4.0 instead it is,", msg.TrapOID())
	}
}

func TestMessageV3PacketFields(t *testing.T) {

	msg := NewSnmpMessage()

	//tests for msgid field
	if msg.MsgID() != 0 {
		t.Error("Message's default msgid value should be 0, instead it is,", msg.MsgID())
	}
	msg.SetMsgID(5)
	if msg.MsgID() != 5 {
		t.Error("Message's msgid value should be 5, instead it is,", msg.MsgID())
	}
	msg.SetMsgID(-5)
	if msg.MsgID() == -5 {
		t.Error("Message's msgid value should not be negative, instead it is,", msg.MsgID())
	}

	//tests for msgmaxsize
	if msg.MsgMaxSize() != 0 {
		t.Error("Message's default msg max-size value should be 0, instead it is,", msg.MsgMaxSize())
	}
	msg.SetMsgMaxSize(500)
	if msg.MsgMaxSize() != 500 {
		t.Error("Message's msg max-size value should be 500, instead it is,", msg.MsgMaxSize())
	}
	msg.SetMsgMaxSize(50)
	if msg.MsgMaxSize() == 50 {
		t.Error("Message's msg max-size value should not be less than 484")
	}

	//tests for msgFlags
	if msg.MsgFlags() != 0 {
		t.Error("Message's default msg flags value should be 0, instead it is,", msg.MsgFlags())
	}
	msg.SetMsgFlags(1)
	if msg.MsgFlags() != 1 {
		t.Error("Message's msg flags value should be 1, instead it is,", msg.MsgFlags())
	}
	msg.SetMsgFlags(2)
	if msg.MsgFlags() == 2 {
		t.Error("Message's msg flags value should not be 2 or 6")
	}

	//tests for msg security-model
	if msg.MsgSecurityModel() != 0 {
		t.Error("Message's default msg sec-model value should be 0, instead it is,", msg.MsgSecurityModel())
	}
	msg.SetMsgSecurityModel(1)
	if msg.MsgSecurityModel() != 1 {
		t.Error("Message's msg sec-model value should be 1, instead it is,", msg.MsgSecurityModel())
	}
	msg.SetMsgSecurityModel(-1)
	if msg.MsgSecurityModel() == -1 {
		t.Error("Message's msg sec-model value should not be 0 or negative")
	}

	//tests for msg security-parameters
	if !bytes.EqualFold(msg.MsgSecurityParameters(), []byte{}) {
		t.Error("Message's default msg sec-params value should be [], instead it is,", msg.MsgSecurityParameters())
	}
	msg.SetMsgSecurityParameters([]byte{1, 2, 3, 4})
	if !bytes.EqualFold(msg.MsgSecurityParameters(), []byte{1, 2, 3, 4}) {
		t.Error("Message's msg sec-params value should be [1 2 3 4], instead it is,", msg.MsgSecurityParameters())
	}

	//tests for scoped pdu-data
	if !bytes.EqualFold(msg.ScopedPDUData(), []byte{}) {
		t.Error("Message's default scoped pdu-data value should be [], instead it is,", msg.ScopedPDUData())
	}
	msg.SetScopedPDUData([]byte{1, 2, 3, 4})
	if !bytes.EqualFold(msg.ScopedPDUData(), []byte{1, 2, 3, 4}) {
		t.Error("Message's msg scoped pdu-data value should be [1 2 3 4], instead it is,", msg.ScopedPDUData())
	}

	//tests for maxSizeResponse scoped-pdu
	if msg.MaxSizeResponseScopedPDU() != 0 {
		t.Error("Message's default maxSizeResponse scoped-pdu value should be 0, instead it is,", msg.MaxSizeResponseScopedPDU())
	}
	msg.SetMaxSizeResponseScopedPDU(65000)
	if msg.MaxSizeResponseScopedPDU() != 65000 {
		t.Error("Message's maxSizeResponse scoped-pdu value should be 65000, instead it is,", msg.MaxSizeResponseScopedPDU())
	}
}

func TestMessageVarBinds(t *testing.T) {

	msg := NewSnmpMessage()

	//test for AddNull method
	msg.AddNull(*snmpvar.NewSnmpOID("1.5.0"))
	if msg.VarBinds() != nil {
		if !strings.EqualFold(msg.ObjectIDAt(0).String(), ".1.3.6.1.2.1.1.5.0") {
			t.Error("OID 1.5.0 not added using AddNull method")
		}
	} else {
		t.Error("OID 1.5.0 not added using AddNull method")
	}

	//test for AddVarBind method
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	if msg.VarBinds() != nil {
		if !strings.EqualFold(msg.VarBindAt(1).String(), ".1.3.6.1.2.1.1.1.0 : something") {
			t.Error("Varbind not added using AddVarBind method")
		}
	} else {
		t.Error("Varbind not added using AddVarBind method")
	}

	//test for AddVarBindAt method
	msg.AddVarBindAt(1, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	if msg.VarBinds() != nil {
		if !strings.EqualFold(msg.VarBindAt(1).String(), ".1.3.6.1.2.1.1.2.0 : something") {
			t.Error("Varbind not added using AddVarBindAt method at position 1")
		}
	} else {
		t.Error("Varbind not added using AddVarBindAt method at position 1")
	}

	beforeLen := len(msg.VarBinds())
	msg.AddVarBindAt(-1, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.3.0"), snmpvar.NewSnmpString("something")))
	afterLen := len(msg.VarBinds())
	if afterLen-beforeLen == 1 {
		t.Error("Varbind should not be added if negative position is given for AddVarBindAt method")
	}

	beforeLen = len(msg.VarBinds())
	msg.AddVarBindAt(10, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.3.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(msg.VarBinds())
	if afterLen-beforeLen == 1 {
		t.Error("Varbind should not be added if index given for AddVarBindAt method is greater than length of varBinds")
	}

	//tests for RemoveVarBindAt method
	beforeLen = len(msg.VarBinds())
	msg.RemoveVarBindAt(1)
	afterLen = len(msg.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind not removed for valid index")
	}

	beforeLen = len(msg.VarBinds())
	msg.RemoveVarBindAt(-1)
	afterLen = len(msg.VarBinds())
	if beforeLen-afterLen == 1 {
		t.Error("Varbind removed for invalid negative index")
	}

	beforeLen = len(msg.VarBinds())
	msg.RemoveVarBindAt(10)
	afterLen = len(msg.VarBinds())
	if beforeLen-afterLen == 1 {
		t.Error("Varbind not removed for invalid index : index > length of varBinds")
	}

	//tests for RemoveVarBind method
	beforeLen = len(msg.VarBinds())
	msg.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(msg.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.1.0 : something not removed")
	}

	beforeLen = len(msg.VarBinds())
	msg.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(msg.VarBinds())
	if beforeLen-afterLen == 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.4.0 : something should not be removed")
	}

	//tests for VarBindAt, ObjectAt & VariableAt methods
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpInt(10)))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.7.0"), snmpvar.NewSnmpOID("1.1.0")))

	if msg.VarBindAt(1) == nil {
		t.Error("VarBind not returned for valid index : 1")
	}
	if msg.ObjectIDAt(1) == nil {
		t.Error("ObjectID not returned for valid index : 1")
	}
	if msg.VariableAt(1) == nil {
		t.Error("Variable not returned for valid index : 1")
	}

	indices := []int{-1, 10}
	for _, index := range indices {
		if msg.VarBindAt(index) != nil {
			t.Error("VarBind should not be returned for invalid index :", index)
		}
		if msg.ObjectIDAt(index) != nil {
			t.Error("ObjectID should not be returned for invalid index :", index)
		}
		if msg.VariableAt(index) != nil {
			t.Error("Variable should not be returned for invalid index :", index)
		}
	}

	//tests for VariableForOID method
	if msg.VariableForOID(*snmpvar.NewSnmpOID("1.4.0")) == nil {
		t.Error("Expected VariableFor OID 1.4.0 not to be nil")
	}
	if msg.VariableForOID(*snmpvar.NewSnmpOID("1.3.0")) != nil {
		t.Error("Expected VariableFor OID 1.3.0 to be nil")
	}

	//tests for SetVaraibleAt method
	msg.SetVariableAt(0, snmpvar.NewSnmpIp("1.1.1.1"))
	if !strings.EqualFold(msg.VariableAt(0).String(), "1.1.1.1") {
		t.Error("VariableAt 0 should be 1.1.1.1 instead it is,", msg.VariableAt(0))
	}

	for _, index := range indices {
		msg.SetVariableAt(index, snmpvar.NewSnmpInt(1000))
		if msg.VariableAt(index) != nil {
			t.Error("Variable should not be set for invalid index", index)
		}
	}
}

func TestMessageFields(t *testing.T) {

	msg := NewSnmpMessage()

	//tests for retries field
	if msg.Retries() != -1 {
		t.Error("Message's default retries value should be -1 instead it is", msg.Retries())
	}
	msg.SetRetries(2)
	if msg.Retries() != 2 {
		t.Error("Message's retries value should be 2 instead it is", msg.Retries())
	}
	msg.SetRetries(-2)
	if msg.Retries() == -2 {
		t.Error("Retries value cannot be negative")
	}

	//tests for timeout field
	if msg.Timeout() != -1 {
		t.Error("Message's default timeout value should be -1 instead it is", msg.Timeout())
	}
	msg.SetTimeout(2000)
	if msg.Timeout() != 2000 {
		t.Error("Message's timeout value should be 2000 instead it is", msg.Timeout())
	}
	msg.SetTimeout(-2000)
	if msg.Timeout() == -2000 {
		t.Error("Message's timeout value should be -2000 instead it is", msg.Timeout())
	}

	//tests for varbinds field
	if len(msg.VarBinds()) != 0 {
		t.Error("Varbinds length should be 0 by default")
	}

	varbs := []SnmpVarBind{NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.2.1.1.3.0"), snmpvar.NewSnmpTimeTicks(10000)),
		NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.6.3.1.1.4.1.0"), snmpvar.NewSnmpOID("1.4.0"))}
	msg.SetVarBinds(varbs)
	if len(msg.VarBinds()) != 2 {
		t.Error("Varbinds length should be 2 instead it is,", len(msg.VarBinds()))
	}

	//test for protocol options field
	if msg.ProtocolOptions() != nil {
		t.Error("protocol-options should be nil by default, instead it is", msg.ProtocolOptions())
	}

	//test for client-id field
	if msg.ClientID() != 0 {
		t.Error("clientid should be 0 by default, instead it is", msg.ClientID())
	}
	msg.SetClientID(5)
	if msg.ClientID() != 5 {
		t.Error("clientid should be 5, instead it is", msg.ClientID())
	}

	//tests for sync field
	if msg.Sync() {
		t.Error("message's default sync value should be false")
	}
	msg.SetSync(true)
	if !msg.Sync() {
		t.Error("message's sync value should be true")
	}

	//tests for original retries
	if msg.OriginalRetries() != 0 {
		t.Error("message's default original-retries value should be 0, instead it is", msg.OriginalRetries())
	}
	msg.SetOriginalRetries(2)
	if msg.OriginalRetries() != 2 {
		t.Error("message's original-retries value should be 2, instead it is", msg.OriginalRetries())
	}

	//tests for time-expire
	if msg.TimeExpire() != 0 {
		t.Error("message's default time-expire value should be 0, instead it is", msg.TimeExpire())
	}
	msg.SetTimeExpire(2000)
	if msg.TimeExpire() != 2000 {
		t.Error("message's time-expire value should be 2000, instead it is", msg.TimeExpire())
	}

	//tests for enqueue field
	if msg.Enqueue() {
		t.Error("message's default enqueue value should be false")
	}
	msg.SetEnqueue(true)
	if !msg.Enqueue() {
		t.Error("message's enqueue value should be true")
	}

	//tests for protocol-data
	if !bytes.EqualFold(msg.ProtocolData(), []byte{}) {
		t.Error("Message's default protocol-data value should be [], instead it is,", msg.ProtocolData())
	}
	msg.SetProtocolData([]byte{1, 2, 3, 4})
	if !bytes.EqualFold(msg.ProtocolData(), []byte{1, 2, 3, 4}) {
		t.Error("Message's protocol-data value should be [1 2 3 4], instead it is,", msg.ProtocolData())
	}

	//tests for auth-failure field
	if msg.IsAuthenticationFailure() {
		t.Error("message's default auth-failure flag should be false")
	}
	msg.SetAuthenticationFailure(true)
	if !msg.IsAuthenticationFailure() {
		t.Error("message's auth-failure value should be true")
	}

	//tests for IsConfirmed method
	if msg.IsConfirmed() {
		t.Error("message's default is-confirmed flag should be false")
	}

	commands := []consts.Command{consts.GetRequest, consts.GetNextRequest, consts.GetResponse, consts.SetRequest, consts.TrapRequest,
		consts.GetBulkRequest, consts.InformRequest, consts.Trap2Request, consts.ReportMessage}
	for _, command := range commands {
		msg.SetCommand(command)
		switch command {
		case consts.GetRequest, consts.GetNextRequest, consts.SetRequest, consts.GetBulkRequest, consts.InformRequest:
			if !msg.IsConfirmed() {
				t.Error("message's is-confirmed flag should be true for command :", command)
			}
		case consts.GetResponse, consts.TrapRequest, consts.Trap2Request, consts.ReportMessage:
			if msg.IsConfirmed() {
				t.Error("message's is-confirmed flag should be false for command :", command)
			}
		default:
			t.Error("Invalid command set for SetCommand method")
		}
	}

	//tests for GetID method
	if msg.GetID() != 0 {
		t.Error("Expected 0 as default value from GetID but got,", msg.GetID(), "for Version1/Version2C SnmpMessage")
	}
	msg.SetRequestID(5)
	if msg.GetID() != 5 {
		t.Error("Expected 5 but got,", msg.GetID(), "for Version1/Version2C SnmpMessage")
	}
	msg.SetMsgID(7)
	if msg.GetID() == 7 {
		t.Error("Expected GetID not to return 7 as it should return only the reqID for Version1/2C message")
	}
	msg.SetVersion(consts.Version3)
	if msg.GetID() != 7 {
		t.Error("Expected 7 as but got,", msg.GetID(), "for Version3 SnmpMessage")
	}
	msg.SetRequestID(3)
	if msg.GetID() == 3 {
		t.Error("Expected GetID not to return 3 as it should return only the msgID for Version3 message")
	}

}

func TestMessageV3Fields(t *testing.T) {

	msg := NewSnmpMessage()

	//test for security-name && username field
	if !strings.EqualFold(msg.SecurityName(), "") {
		t.Error("Message's default security-name should be emptystring if version not set, instead it is,", msg.SecurityName())
	}
	msg.SetVersion(consts.Version1)
	if !strings.EqualFold(msg.SecurityName(), "<nil>") {
		t.Error("Message's default security-name should be <nil> for Version1/2C, instead it is,", msg.SecurityName())
	}
	msg.SetCommunity("something")
	if !strings.EqualFold(msg.SecurityName(), "something") {
		t.Error("Message's security-name should be something, instead it is,", msg.SecurityName())
	}
	msg.SetVersion(consts.Version3)
	if !strings.EqualFold(msg.SecurityName(), "") {
		t.Error("Message's default security-name should be emptystring for Version3, instead it is,", msg.SecurityName())
	}
	if !strings.EqualFold(msg.UserName(), "") {
		t.Error("Message's username should be empty-string, instead it is,", msg.UserName())
	}
	msg.SetUserName("privUser")
	if !strings.EqualFold(msg.SecurityName(), "privUser") {
		t.Error("Message's security-name should be privUser, instead it is,", msg.SecurityName())
	}
	if !strings.EqualFold(msg.UserName(), "privUser") {
		t.Error("Message's username should be privUser, instead it is,", msg.UserName())
	}

	//tests for security level field
	if msg.SecurityLevel() != consts.NoAuthNoPriv {
		t.Error("Message's default security level should be NOAUTH_NOPRIV, instead it is,", msg.SecurityLevel())
	}
	msg.SetSecurityLevel(1)
	if msg.SecurityLevel() != 1 {
		t.Error("Message's security level should be 1, instead it is,", msg.SecurityLevel())
	}
	msg.SetSecurityLevel(5)
	if msg.SecurityLevel() == 5 {
		t.Error("Message's security level should be 0, 1 or 3")
	}

	//tests for ContextEngineID field
	if !bytes.EqualFold(msg.ContextEngineID(), []byte{}) {
		t.Error("Message's default context engine-id should be []", msg.ContextEngineID())
	}
	msg.SetContextEngineID([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9})
	if !bytes.EqualFold(msg.ContextEngineID(), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9}) {
		t.Error("Message's context engine-id should be [1 2 3 4 5 6 7 8 9]", msg.ContextEngineID())
	}

	//tests for ContextName field
	if !strings.EqualFold(msg.ContextName(), "") {
		t.Error("Message's default contextname should be empty-string, instead it is,", msg.ContextName())
	}
	msg.SetContextName("priv")
	if !strings.EqualFold(msg.ContextName(), "priv") {
		t.Error("Message's contextname should be priv, instead it is,", msg.ContextName())
	}
}

func TestMessageUtilities(t *testing.T) {

	msg := NewSnmpMessage()

	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpInt(10)))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpInt(20)))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.3.0"), snmpvar.NewSnmpInt(30)))
	msg.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpInt(40)))

	//tests for Fix method
	beforeLen := len(msg.VarBinds())
	msg.SetErrorIndex(1)
	msg.Fix()
	afterLen := len(msg.VarBinds())

	if beforeLen-afterLen != 1 {
		t.Error("VarBindAt index 1 should be removed after Fix method")
	}

	indices := []int32{10}

	for _, index := range indices {
		beforeLen = len(msg.VarBinds())
		msg.SetErrorIndex(index)
		msg.Fix()
		afterLen := len(msg.VarBinds())

		if beforeLen-afterLen == 1 {
			t.Error("VarBindAt invalid index", index, "should not be removed after Fix method")
		}
	}

	//tests for Copy method
	dupMsg := msg.Copy()
	if dupMsg != nil {
		if len(msg.VarBinds()) != len(dupMsg.VarBinds()) {
			t.Error("Expected Copy to copy the whole message including the varbinds")
		}
	}

	//tests for CopyWithoutVarBinds method
	dupMsg = msg.CopyWithoutVarBinds()
	if dupMsg != nil && len(dupMsg.VarBinds()) != 0 {
		t.Error("Expected 0 for len(dupMsg.VarBinds) method as CopyWithoutVarBinds should copy whole message without varbinds")
	}
}
