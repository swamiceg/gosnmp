/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"strings"
	"testing"

	"webnms/snmp/consts"
	"webnms/snmp/snmpvar"
)

func newPDU() *SnmpPDU {

	pdu := new(SnmpPDU)
	return pdu
}

func TestPDUPacketFields(t *testing.T) {

	pdu := newPDU()

	//tests for command field
	if pdu.Command() != 0 {
		t.Error("pdu's default command should be 0 instead it is", pdu.Command())
	}

	pdu.SetCommand(consts.GetRequest)
	if pdu.Command() != consts.GetRequest {
		t.Error("pdu's command should be", consts.GetRequest, "instead it is", pdu.Command())
	}

	pdu.SetCommand(250)
	if pdu.Command() == 250 {
		t.Error("pdu's command cannot be 250")
	}

	//tests for requestID field
	if pdu.RequestID() != 0 {
		t.Error("pdu's default requestID value should be 0 instead it is", pdu.RequestID())
	}

	pdu.SetRequestID(10)
	if pdu.RequestID() != 10 {
		t.Error("pdu's requestID should be 10 instead it is", pdu.RequestID())
	}

	//tests for ErrorStatus field
	if pdu.ErrorStatus() != 0 {
		t.Error("pdu's default error-status value should be 0 instead it is", pdu.ErrorStatus())
	}

	pdu.SetErrorStatus(consts.WrongLength)
	if pdu.ErrorStatus() != consts.WrongLength {
		t.Error("pdu's error-status should be", consts.WrongLength, "instead it is", pdu.ErrorStatus())
	}

	pdu.SetErrorStatus(25)
	if pdu.ErrorStatus() == 25 {
		t.Error("pdu's error-status value should be in range 0-18, instead it is 25")
	}

	values := []int32{-1}

	//tests for ErrorIndex field
	if pdu.ErrorIndex() != 0 {
		t.Error("pdu's default error-index value should be 0 instead it is", pdu.ErrorIndex())
	}

	pdu.SetErrorIndex(2)
	if pdu.ErrorIndex() != 2 {
		t.Error("pdu's error-index should be 2 instead it is", pdu.ErrorIndex())
	}

	for _, value := range values {
		pdu.SetErrorIndex(value)
		if pdu.ErrorIndex() == value {
			t.Error("pdu's error-index should not be set to an invalid value,", value)
		}
	}

	//test for ErrorString method
	if !strings.HasPrefix(pdu.ErrorString(), "Error in response. ") {
		t.Error("pdu's ErrorString should start with the message 'Error in response.'")
	}

	//tests for Non-Repeaters field
	if pdu.NonRepeaters() != 0 {
		t.Error("pdu's default NonRepeaters value should be 0 instead it is", pdu.NonRepeaters())
	}

	pdu.SetNonRepeaters(5)
	if pdu.NonRepeaters() != 5 {
		t.Error("pdu's NonRepeaters value should be 5 instead it is", pdu.NonRepeaters())
	}

	for _, value := range values {
		pdu.SetNonRepeaters(value)
		if pdu.NonRepeaters() == value {
			t.Error("pdu's NonRepeaters should not be set to an invalid value,", value)
		}
	}

	//tests for MaxRep field
	if pdu.MaxRepetitions() != 0 {
		t.Error("pdu's default MaxRepetitions value should be 0 instead it is", pdu.MaxRepetitions())
	}

	pdu.SetMaxRepetitions(5)
	if pdu.MaxRepetitions() != 5 {
		t.Error("pdu's MaxRepetitions value should be 5 instead it is", pdu.MaxRepetitions())
	}

	for _, value := range values {
		pdu.SetMaxRepetitions(value)
		if pdu.MaxRepetitions() == value {
			t.Error("pdu's MaxRepetitions should not be set to an invalid value,", value)
		}
	}
}

func TestPDUTrapFields(t *testing.T) {

	pdu := newPDU()

	//tests for enterprise field
	if !strings.EqualFold(pdu.Enterprise().String(), "") {
		t.Error("pdu's default Enterprise value should be empty instead it is", pdu.Enterprise())
	}

	pdu.SetEnterprise(*snmpvar.NewSnmpOID("1.5.0"))
	if !strings.EqualFold(pdu.Enterprise().String(), ".1.3.6.1.2.1.1.5.0") {
		t.Error("pdu's Enterprise value should be .1.3.6.1.2.1.1.5.0 instead it is", pdu.Enterprise())
	}

	//tests for agent-address field
	if !strings.EqualFold(pdu.AgentAddress(), "") {
		t.Error("pdu's default agent-address value should be empty instead it is", pdu.AgentAddress())
	}

	pdu.SetAgentAddress("1.1.1.1")
	if !strings.EqualFold(pdu.AgentAddress(), "1.1.1.1") {
		t.Error("pdu's agent-address value should be 1.1.1.1 instead it is", pdu.Enterprise())
	}

	pdu.SetAgentAddress("1.1.1.1.1")
	if !strings.EqualFold(pdu.AgentAddress(), "1.1.1.1.1") {
		t.Error("pdu's agent-address value should be 1.1.1.1.1 instead it is", pdu.Enterprise())
	}

	//tests for generic-type field
	if pdu.GenericType() != 0 {
		t.Error("pdu's default generic-type value should be 0 instead it is", pdu.GenericType())
	}

	pdu.SetGenericType(2)
	if pdu.GenericType() != 2 {
		t.Error("pdu's generic-type value should be 2 instead it is", pdu.GenericType())
	}

	pdu.SetGenericType(10)
	if pdu.GenericType() == 10 {
		t.Error("pdu's generic-type should be in range 0-7, instead it is tried to set as 10")
	}

	//tests for specific-type field
	if pdu.SpecificType() != 0 {
		t.Error("pdu's default specific-type value should be 0 instead it is", pdu.SpecificType())
	}

	pdu.SetSpecificType(2)
	if pdu.SpecificType() != 2 {
		t.Error("pdu's specific-type value should be 2 instead it is", pdu.SpecificType())
	}

	//tests for upTime field
	if pdu.UpTime() != 0 {
		t.Error("pdu's default uptime value should be 0 instead it is", pdu.UpTime())
	}

	pdu.SetUpTime(1000)
	if pdu.UpTime() != 1000 {
		t.Error("pdu's uptime value should be 1000 instead it is", pdu.UpTime())
	}

	//tests for TrapOID && UpTime method by adding varbinds
	pdu.SetCommand(consts.Trap2Request)
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.2.1.1.3.0"), snmpvar.NewSnmpTimeTicks(10000)))
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID(".1.3.6.1.6.3.1.1.4.1.0"), snmpvar.NewSnmpOID("1.4.0")))

	if pdu.UpTime() != 10000 {
		t.Error("pdu's uptime value should be 10000 instead it is", pdu.UpTime())
	}

	if pdu.TrapOID() != nil && !strings.EqualFold(pdu.TrapOID().String(), ".1.3.6.1.2.1.1.4.0") {
		t.Error("pdu's trapoid value should be .1.3.6.1.2.1.1.4.0 instead it is,", pdu.TrapOID())
	}
}

func TestPDUVarBinds(t *testing.T) {

	pdu := newPDU()

	//test for AddNull method
	pdu.AddNull(*snmpvar.NewSnmpOID("1.5.0"))
	if pdu.VarBinds() != nil {
		if !strings.EqualFold(pdu.ObjectIDAt(0).String(), ".1.3.6.1.2.1.1.5.0") {
			t.Error("OID 1.5.0 not added using AddNull method")
		}
	} else {
		t.Error("OID 1.5.0 not added using AddNull method")
	}

	//test for AddVarBind method
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	if pdu.VarBinds() != nil {
		if !strings.EqualFold(pdu.VarBindAt(1).String(), ".1.3.6.1.2.1.1.1.0 : something") {
			t.Error("Varbind not added using AddVarBind method")
		}
	} else {
		t.Error("Varbind not added using AddVarBind method")
	}

	indices := []int{-1, 10}
	var beforeLen, afterLen int

	//test for AddVarBindAt method
	pdu.AddVarBindAt(1, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	if pdu.VarBinds() != nil {
		if !strings.EqualFold(pdu.VarBindAt(1).String(), ".1.3.6.1.2.1.1.2.0 : something") {
			t.Error("Varbind not added using AddVarBindAt method at position 1")
		}
	} else {
		t.Error("Varbind not added using AddVarBindAt method at position 1")
	}

	for _, index := range indices {
		beforeLen = len(pdu.VarBinds())
		pdu.AddVarBindAt(index, NewSnmpVarBind(*snmpvar.NewSnmpOID("1.3.0"), snmpvar.NewSnmpString("something")))
		afterLen = len(pdu.VarBinds())
		if afterLen-beforeLen == 1 {
			t.Error("Varbind should not be added for invalid index,", index, "is given for AddVarBindAt")
		}
	}

	//tests for RemoveVarBindAt method
	beforeLen = len(pdu.VarBinds())
	pdu.RemoveVarBindAt(1)
	afterLen = len(pdu.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind not removed for valid index 1")
	}

	for _, index := range indices {
		beforeLen = len(pdu.VarBinds())
		pdu.RemoveVarBindAt(index)
		afterLen = len(pdu.VarBinds())
		if beforeLen-afterLen == 1 {
			t.Error("Varbind should not be removed if invalid index,", index, "is given for RemoveVarBindAt")
		}
	}

	//tests for RemoveVarBind method
	beforeLen = len(pdu.VarBinds())
	pdu.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(pdu.VarBinds())
	if beforeLen-afterLen != 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.1.0 : something not removed")
	}

	beforeLen = len(pdu.VarBinds())
	pdu.RemoveVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpString("something")))
	afterLen = len(pdu.VarBinds())
	if beforeLen-afterLen == 1 {
		t.Error("Varbind .1.3.6.1.2.1.1.4.0 : something should not be removed")
	}

	//tests for VarBindAt, ObjectAt & VariableAt methods
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpString("something")))
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.4.0"), snmpvar.NewSnmpInt(10)))
	pdu.AddVarBind(NewSnmpVarBind(*snmpvar.NewSnmpOID("1.7.0"), snmpvar.NewSnmpOID("1.1.0")))

	if pdu.VarBindAt(1) == nil {
		t.Error("VarBind not returned for valid index : 1")
	}
	if pdu.ObjectIDAt(1) == nil {
		t.Error("ObjectID not returned for valid index : 1")
	}
	if pdu.VariableAt(1) == nil {
		t.Error("Variable not returned for valid index : 1")
	}

	for _, index := range indices {
		if pdu.VarBindAt(index) != nil {
			t.Error("VarBind should not be returned for invalid index :", index)
		}
		if pdu.ObjectIDAt(index) != nil {
			t.Error("ObjectID should not be returned for invalid index :", index)
		}
		if pdu.VariableAt(index) != nil {
			t.Error("Variable should not be returned for invalid index :", index)
		}
	}

	//tests for VariableForOID method
	if pdu.VariableForOID(*snmpvar.NewSnmpOID("1.4.0")) == nil {
		t.Error("Expected VariableFor OID 1.4.0 not to be nil")
	}
	if pdu.VariableForOID(*snmpvar.NewSnmpOID("1.3.0")) != nil {
		t.Error("Expected VariableFor OID 1.3.0 to be nil")
	}

	//tests for SetVaraibleAt method
	pdu.SetVariableAt(0, snmpvar.NewSnmpIp("1.1.1.1"))
	if !strings.EqualFold(pdu.VariableAt(0).String(), "1.1.1.1") {
		t.Error("VariableAt 0 should be 1.1.1.1 instead it is,", pdu.VariableAt(0))
	}

	for _, index := range indices {
		pdu.SetVariableAt(index, snmpvar.NewSnmpInt(1000))
		if pdu.VariableAt(index) != nil {
			t.Error("Variable should not be set for invalid index", index)
		}
	}

	//tests for IsConfirmed method
	if pdu.IsConfirmed() {
		t.Error("pdu's default is-confirmed flag should be false")
	}

	commands := []consts.Command{consts.GetRequest, consts.GetNextRequest, consts.GetResponse, consts.SetRequest, consts.TrapRequest,
		consts.GetBulkRequest, consts.InformRequest, consts.Trap2Request, consts.ReportMessage}
	for _, command := range commands {
		pdu.SetCommand(command)
		switch command {
		case consts.GetRequest, consts.GetNextRequest, consts.SetRequest, consts.GetBulkRequest, consts.InformRequest:
			if !pdu.IsConfirmed() {
				t.Error("message's is-confirmed flag should be true for command :", command)
			}
		case consts.GetResponse, consts.TrapRequest, consts.Trap2Request, consts.ReportMessage:
			if pdu.IsConfirmed() {
				t.Error("message's is-confirmed flag should be false for command :", command)
			}
		default:
			t.Error("Invalid command set for SetCommand method")
		}
	}
}
