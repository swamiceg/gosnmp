/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package msg

import (
	"bytes"
	"strconv"

	"webnms/snmp/consts"
)

//ScopedPDU is a block of data containing a contextEngineID, a contextName, and a PDU
//It is a part of SnmpMessage struct.
//User need not instantiate this type directly.
type ScopedPDU struct {
	contextEngineID []byte
	contextName     string
	SnmpPDU

	scopedPDUData []byte
}

//ContextEngineID returns the contextID sent/recieved with this scoped pdu.
func (spdu *ScopedPDU) ContextEngineID() []byte {
	return spdu.contextEngineID
}

//SetContextEngineID sets the contextID sent/recieved with this scoped pdu
func (spdu *ScopedPDU) SetContextEngineID(contextID []byte) *ScopedPDU {
	spdu.contextEngineID = contextID
	return spdu
}

//ContextName returns the contextname sent/recieved with this scoped pdu
func (spdu *ScopedPDU) ContextName() string {
	return spdu.contextName
}

//SetContextName sets the contextname sent/recieved with this scoped pdu
func (spdu *ScopedPDU) SetContextName(contextName string) *ScopedPDU {
	spdu.contextName = contextName
	return spdu
}

//SetScopedPDUData sets the scoped pdu data in bytes
func (spdu *ScopedPDU) SetScopedPDUData(scopedPDUData []byte) *ScopedPDU {
	spdu.scopedPDUData = scopedPDUData
	return spdu
}

//ScopedPDUData returns the scoped pdu data in bytes
func (spdu *ScopedPDU) ScopedPDUData() []byte {
	return spdu.scopedPDUData
}

//Command returns command type of this scoped PDU. The command type is one of the constants defined in the snmp package.
func (spdu *ScopedPDU) Command() consts.Command {
	return spdu.command
}

//SetCommand sets command type of this scoped PDU. The command type is one of the below constants defined in the snmp package.
//
//snmp.GetRequest, snmp.GetNextRequest, snmp.GetResponse, snmp.SetRequest, snmp.TrapRequest, snmp.GetBulkRequest, snmp.InformRequest, snmp.Trap2Request & snmp.ReportMes//sage
func (spdu *ScopedPDU) SetCommand(cmd consts.Command) *ScopedPDU {
	if cmd == consts.GetRequest || cmd == consts.GetNextRequest || cmd == consts.GetResponse || cmd == consts.SetRequest || cmd == consts.TrapRequest || cmd == consts.GetBulkRequest || cmd == consts.InformRequest || cmd == consts.Trap2Request || cmd == consts.ReportMessage {
		spdu.command = cmd
	}
	return spdu
}

//RequestID returns the request id of this scoped PDU.
func (spdu *ScopedPDU) RequestID() int32 {
	return spdu.requestID
}

//SetRequestID sets the Request id for this scoped PDU. If requestID is set to non-zero, the API leaves it alone. If it is 0, the API assigns a unique requestID to ensure no conflict with other requests. So, if you do not want to manage the request ids yourself, it should be 0. The default value is 0. If you are re-using the PDU and do not want to manage the requestID yourself, set it to 0 each time.
func (spdu *ScopedPDU) SetRequestID(reqID int32) *ScopedPDU {
	spdu.requestID = reqID
	return spdu
}

//ErrorStatus returns the error status of this scoped PDU.
func (spdu *ScopedPDU) ErrorStatus() consts.ErrorStatus {
	return spdu.errorStatus
}

//SetErrorStatus sets the Error status for this scoped PDU.
//
//The error status values that can be set using this method are,
//
//snmp.NoError, snmp.TooBig, snmp.NoSuchName, snmp.BadValue, snmp.ReadOnly, snmp.GeneralError, snmp.NoAccess, snmp.WrongType, snmp.WrongLength, snmp.WrongEncoding,
//snmp.WrongValue, snmp.NoCreation, snmp.InconsistentValue, snmp.ResourceUnavailable, snmp.CommitFailed, snmp.UndoFailed, snmp.AuthorizationError,
//snmp.NotWritable & snmp.InconsistentName.
//
//It will ignore values other than the one mentioned above.
func (spdu *ScopedPDU) SetErrorStatus(errStat consts.ErrorStatus) *ScopedPDU {
	if errStat >= 0 && errStat <= 18 {
		spdu.errorStatus = errStat
	}
	return spdu
}

//ErrorIndex returns the error index of this scoped PDU.
func (spdu *ScopedPDU) ErrorIndex() int32 {
	return spdu.errorIndex
}

//SetErrorIndex sets the error index of this scoped PDU. Only values >= 0 && <=2147483647 will be accepted by this method.
func (spdu *ScopedPDU) SetErrorIndex(errIndex int32) *ScopedPDU {
	if errIndex >= 0 {
		spdu.errorIndex = errIndex
	}
	return spdu
}

//ErrorString returns error information as a String, with offending varbind if any.
func (spdu *ScopedPDU) ErrorString() string {
	errBuffer := bytes.NewBufferString("")
	if spdu.errorStatus != 0 {
		errBuffer.WriteString("Error in response. ")
		errBuffer.WriteString(errorString(spdu.errorStatus))
		errBuffer.WriteString("\nIndex: ")
		errBuffer.WriteString(strconv.FormatInt(int64(spdu.errorIndex), 10))
		errBuffer.WriteString("\n")
		if spdu.errorIndex > 0 && len(spdu.snmpVarbs) > int(spdu.errorIndex) {
			errBuffer.WriteString("Errored Object ID: ")
			errBuffer.WriteString(spdu.ObjectIDAt(int(spdu.errorIndex)).String())
			errBuffer.WriteString("\n")
		}
	}
	return errBuffer.String()
}

//NonRepeaters returns Non-Repeaters value of this scoped PDU.
func (spdu *ScopedPDU) NonRepeaters() int32 {
	return spdu.nonRepeaters
}

//SetNonRepeaters sets Non-Repeaters value for this scoped PDU.
func (spdu *ScopedPDU) SetNonRepeaters(nonRep int32) *ScopedPDU {
	if nonRep >= 0 {
		spdu.nonRepeaters = nonRep
	}
	return spdu
}

//MaxRepetitions returns Max-Repetitions value of this scoped PDU.
func (spdu *ScopedPDU) MaxRepetitions() int32 {
	return spdu.maxRepetitions
}

//SetMaxRepetitions sets Max-Repetitions value for this scoped PDU. The valid values ranges from 0 to 65535(both inclusive).
func (spdu *ScopedPDU) SetMaxRepetitions(maxRep int32) *ScopedPDU {
	if maxRep >= 0 {
		spdu.maxRepetitions = maxRep
	}
	return spdu
}

//AgentAddress returns address of object generating trap. In the case of SNMPv2 TRAP PDU if one of the varbind contains the snmpTrapAddress.0 oid the corresponding value is returned as the agent address.
func (spdu *ScopedPDU) AgentAddress() string {
	return spdu.agentAddress
}

//SetAgentAddress sets address of object generating trap. In the case of SNMPv2c TRAP PDU an additional varbind is added, if its not already present in the varbind list. The varbind contains the snmpTrapAddress.0 (defined in RFC2576) as the oid and the value (SnmpVar) will be the agent address. There is no need to add this varbind explicitly.
func (spdu *ScopedPDU) SetAgentAddress(address string) *ScopedPDU {
	spdu.agentAddress = address
	return spdu
}

//VarBinds returns list of SnmpVarBind in this scoped PDU as a slice.
func (spdu *ScopedPDU) VarBinds() []SnmpVarBind {
	return spdu.snmpVarbs
}

//SetVarBinds sets the list of SnmpVarBind to this scoped PDU
func (spdu *ScopedPDU) SetVarBinds(varbs []SnmpVarBind) *ScopedPDU {
	spdu.snmpVarbs = varbs
	return spdu
}

//For internal use only.
func (spdu *ScopedPDU) SetSecurityLevel(securityLevel consts.SecurityLevel) *ScopedPDU {
	if securityLevel == consts.NoAuthNoPriv ||
		securityLevel == consts.AuthNoPriv ||
		securityLevel == consts.AuthPriv {
		spdu.securityLevel = securityLevel
	}
	return spdu
}

//IsConfirmed returns bool indicating whether this msg belongs to Confirmed Class.
//
//Confirmed Class: Get/Get-Next/Set/Get-Bulk/Inform
func (spdu *ScopedPDU) IsConfirmed() bool {
	return spdu.SnmpPDU.IsConfirmed()
}
