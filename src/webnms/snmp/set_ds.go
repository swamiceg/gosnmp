/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"sync"
)

//Set type is similar to Java's Set data structure in collections framework.
//Allows us to store unique values, no duplicates are allowed.
//
//Provides utility methods to store and retrieve data efficiently.
type set struct {
	Value map[interface{}]bool
	sync.RWMutex
}

//NewSet returns new instance of Set.
func newSet() *set {
	return &set{
		Value: make(map[interface{}]bool),
	}
}

//Add adds new val to the Set s.
func (s *set) Add(val interface{}) {
	s.Lock()
	defer s.Unlock()
	s.Value[val] = true
}

//Remove removes the val from Set s.
func (s *set) Remove(val interface{}) {
	s.Lock()
	defer s.Unlock()
	delete(s.Value, val)
}

//Contains returns bool indicating whether Set s contains the val or not.
func (s *set) Contains(val interface{}) bool {
	s.RLock()
	defer s.RUnlock()
	_, ok := s.Value[val]

	return ok
}

//List returns a slice of values stored in Set s.
func (s *set) List() []interface{} {
	s.RLock()
	defer s.RUnlock()
	var ifSlice []interface{}
	for v := range s.Value {
		ifSlice = append(ifSlice, v)
	}
	return ifSlice
}

//Len returns the length of Set s.
func (s *set) Len() int {
	return len(s.List())
}

//IsEmpty returns bool indicating whether Set s is empty or not.
func (s *set) IsEmpty() bool {
	return s.Len() == 0
}

//Clear clears all the values stored in Set s.
func (s *set) Clear() {
	s.Lock()
	defer s.Unlock()
	s.Value = make(map[interface{}]bool)
}
