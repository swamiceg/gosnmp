/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package snmp is the core implementations of the SNMP framework. 
//It defines SNMP Variables like SnmpOID, SnmpInteger etc., as per ASN.1 standard, and SnmpMessages required for SNMP communication.
//It facilitates communication with remote SNMP enabled devices and helps in performing all the SNMP operations like Get, Get-Next, Set etc.,
//
//This package is a direct implementation of SNMP as per RFC standards supporting all three versions SNMPv1, SNMPv2c, and SNMPv3.
//
//It provides various types and methods to perform all the SNMP operations at ease. These low-level APIs provides
//users with full control of managing all the resources SnmpAPI, SnmpSession and underlying transport.
//It also has APIs to perform asynchronous communication through SnmpClient interface (by implementing methods like Callback, Authenticate etc).
//
//package snmp collectively comprises the Low-Level APIs of the product.
//The usage of this low-level API is recommended in embedded applications where code size limitations apply.
//
//Refer 'webnms/snmp/hl' package for the High-Level APIs.
package snmp
