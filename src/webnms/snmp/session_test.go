/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"webnms/snmp/consts"
	"webnms/snmp/msg"

	//"fmt"
	"testing"
	"time"
)

//Testing for all methods in SnmpSession type

func TestSessionOpen(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)

	//Send msg without opening session
	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)
	_, err := ses.SyncSend(mesg)
	if err == nil {
		t.Error("Session SyncSend(): Not throwing error while sending msg on Unopened SnmpSession")
	}

	//Test whether the Session is open
	if ses.IsSessionOpen() {
		t.Error("SnmpSession IsSessionOpen(): SessionOpen flag is invalid")
	}

	//Open Session without ProtocolOptions
	err = ses.Open()
	if err != nil {
		t.Error(err)
	}
	ses.Close()

	//Open Session with UDP protocoloptions
	udp := NewUDPProtocolOptions()
	udp.SetLocalHost(localHost)
	udp.SetLocalPort(localPort)
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
	} else {
		//Test whether the Session is open
		if !ses.IsSessionOpen() {
			t.Error("SnmpSession IsSessionOpen(): SessionOpen flag is invalid")
		}

		//Is localhost/localport values updated in Session??
		if localPort != ses.localPort {
			t.Error("Binded local port different from provided port number")
		}

		//Check whether the Session is added in SnmpAPI
		exist := false
		for _, session := range api.SnmpSessions() {
			if session == ses {
				exist = true
				break
			}
		}
		if !exist {
			t.Error("SnmpSession is not monitored by SnmpAPI.")
		}

		//Check whether the SnmpGroup is added in SnmpAPI
		grp := api.SnmpGroup(localHost, localPort)
		if grp == nil {
			t.Error("SnmpGroup is not added in SnmpAPI.")
		}
	}
	api.Close()
}

func TestSessionClose(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	ses.Close()

	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err := ses.Open()
	if err != nil {
		t.Error(err)
	}

	ses.Close()
	//Check whether Session is closed successfully
	if ses.IsSessionOpen() {
		t.Error("Failure in closing SnmpSession. Flag is not set to false.")
	}

	//Send message on closed session
	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)
	_, err = ses.SyncSend(mesg)
	if err == nil {
		t.Error("Session Close(): Not throwing error while sending msg on closed SnmpSession")
	}

	//Check whether SnmpSession is removed from SnmpAPI
	exist := false
	for _, session := range api.SnmpSessions() {
		if session == ses {
			exist = true
			break
		}
	}
	if exist {
		t.Error("Session Close(): SnmpSession is not removed from SnmpAPI.")
	}

	//Check whether the SnmpGroup is added in SnmpAPI
	grp := api.SnmpGroup(ses.localHost, ses.localPort)
	if grp != nil {
		t.Error("Session Close(): SnmpGroup is not removed from SnmpAPI.")
	}

	/*Should test Callback*/

	api.Close()
}

func TestSyncSend(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)

	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)
	//Send message without opening SnmpSession
	_, err := ses.SyncSend(mesg)
	if err == nil {
		t.Error("Session SyncSend(): Not throwing error while sending msg on unopened SnmpSession")
	}

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
	}

	//Send a valid SnmpMessage - Valid Case
	resp, err := ses.SyncSend(mesg)
	if err != nil {
		t.Error(err)
	} else {
		//Check whether ProtocolOptions are filled in the response
		if resp.ProtocolOptions() == nil {
			t.Error("Session SyncSend(): ProtocolOptions is not filled in the response message.")
		} else if resp.ProtocolOptions().RemotePort() != remotePort {
			t.Errorf("Received response from different port than expected. Port No.: %d", resp.ProtocolOptions().RemotePort())
		}
	}

	//Send empty message - Invalid Case
	_, err = ses.SyncSend(msg.NewSnmpMessage())
	if err == nil {
		t.Error("Session SyncSend(): Not throwing error while sending empty message")
	}

	//Msg ProtocolOptions Test
	udp.SetRemotePort(100) //Change the Session's RemotePort
	ses.SetProtocolOptions(udp)
	msgUDP := NewUDPProtocolOptions()
	msgUDP.SetRemoteHost(remoteHost)
	msgUDP.SetRemotePort(remotePort)
	mesg.SetProtocolOptions(msgUDP)
	_, err = ses.SyncSend(mesg) //Should send message to 'remotePort' specified in msg's ProtocolOptions
	if err != nil {
		t.Error(err)
	}
}

func TestSend(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	var err error

	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)
	//Send message without opening SnmpSession
	_, err = ses.Send(mesg)
	if err == nil {
		t.Error("Session Send(): Not throwing error while sending msg on unopened SnmpSession")
	}

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
	}

	//Send a valid SnmpMessage without adding SnmpClient
	var reqID int32
	var woClientQ, wClientQ bool
	id1, err := ses.Send(mesg)
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second * 2) //Give some time for populating incoming queue
	for _, mesg := range ses.Responses() {
		reqID = mesg.RequestID()
		if reqID == id1 { //Response is in the queue
			woClientQ = true
		}
	}
	if !woClientQ {
		t.Error("Session Send(): Missed response is not put in the Queue.")
	}

	//Send a valid SnmpMessage after adding SnmpClient
	ses.AddSnmpClient(new(client))
	id2, err := ses.Send(mesg)
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second * 1) //Give some time for populating incoming queue
	for _, mesg := range ses.Responses() {
		reqID = mesg.RequestID()
		if reqID == id2 {
			wClientQ = true
		}
	}

	if wClientQ {
		t.Error("Session Send(): Received response is still in the Queue. Not removed.")
	}

	mesg.SetCommand(consts.GetResponse)
	_, err = ses.Send(mesg) //Should send message to 'remotePort' specified in msg's ProtocolOptions
	if err != nil {
		t.Error("SnmpSession: Send(): Error while sending GetResponse message: %s.", err)
	}

}

var TimeoutRetriesTest = []struct {
	timeout int
	retries int
	expiry  int //In Milliseconds
}{
	{1000, 1, 3000},
	{500, 3, 7500},
	{100, 5, 6300},
	{100, -4, 6300}, //Should not set '-4' timeout value. Previous value will be used
}

func TestTimeoutRetries(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	var err error

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(100) //Invalid port for timeout test
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
	}
	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)

	var start, end int
	//Session Timeout/Retries Test
	for _, tr := range TimeoutRetriesTest {
		ses.SetTimeout(tr.timeout)
		ses.SetRetries(tr.retries)

		start = currentTimeInMillis()
		ses.SyncSend(mesg) //Should timeout
		end = currentTimeInMillis() - start

		if !((end > (tr.expiry - 100)) && (end < (tr.expiry + 100))) { //100ms buffer can be given
			t.Error("Session Timeout: Failure in SnmpSession timeout interval.")
		}
	}

	//Msg Session Timeout/Retries Test
	ses.SetTimeout(1000)
	ses.SetRetries(0)
	for _, tr := range TimeoutRetriesTest {
		ses.SetTimeout(100) //Msg values should override session's values
		mesg.SetTimeout(tr.timeout)
		mesg.SetRetries(tr.retries)

		start = currentTimeInMillis()
		ses.SyncSend(mesg) //Should timeout
		end = currentTimeInMillis() - start

		if !((end > (tr.expiry - 100)) && (end < (tr.expiry + 100))) { //100ms buffer can be given
			t.Error("Session Timeout: Failure in message timeout interval.")
		}
	}
	api.Close()
}

func TestSnmpClient(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)

	cl1 := new(client)
	///Add SnmpClient and test the methods
	ses.AddSnmpClient(cl1)
	if !ses.IsSnmpClientExist(cl1) {
		t.Error("Session AddSnmpClient(): Failure in adding SnmpClient to SnmpSession.")
	}
	if len(ses.SnmpClients()) != 1 {
		t.Errorf("Session SnmpClients(): Client slice should be of size 1. But returned %d.", len(ses.SnmpClients()))
	}
	ses.AddSnmpClient(nil)
	if ses.IsSnmpClientExist(nil) {
		t.Error("Session IsSnmpClientExist(): Nil client exist in Session's client list.")
	}

	//Remove SnmpClient and test the methods
	ses.RemoveSnmpClient(cl1)
	if ses.IsSnmpClientExist(cl1) {
		t.Error("Session RemoveSnmpClient(): Failure in removing SnmpClient from SnmpSession.")
	}
	if len(ses.SnmpClients()) != 0 {
		t.Errorf("Session SnmpClients(): Client slice should be of size 0. But returned %d.", len(ses.SnmpClients()))
	}

	//RemoveAllSnmpClient Test
	ses.AddSnmpClient(new(client))
	ses.AddSnmpClient(new(client))
	ses.RemoveAllSnmpClients()
	if len(ses.SnmpClients()) != 0 {
		t.Error("Session RemoveAllSnmpClients(): Client list is not empty.")
	}

	//Add/Remove SnmpClientWithID
	cId := ses.AddSnmpClientWithID(cl1)
	var success bool
	for id, _ := range ses.SnmpClientsWithID() {
		if id == cId {
			success = true
			break
		}
	}
	if !success || !ses.IsSnmpClientExist(cl1) {
		t.Error("Session AddSnmpClientWithID(): Failure in adding SnmpClient with ID.")
	}
	ses.RemoveAllSnmpClients()
}

var cbCount int
var dbgPrintCount int

func TestClientCallback(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	var err error

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)

	cbCount = 0
	dbgPrintCount = 0
	cl := new(client)
	cl1 := new(client1)
	cl2 := new(client2)
	ses.AddSnmpClient(cl)
	ses.AddSnmpClientWithID(cl1) //Ignore the ID
	id := ses.AddSnmpClientWithID(cl2)

	mesg.SetClientID(id) //Callback should happen for only this client - cl2

	//Send a valid message and test callback with clientID
	_, err = ses.Send(mesg)
	if err != nil {
		t.Error(err)
	} else {
		//Wait for incoming message
		ses.Wait()
		if cbCount != 1 { //Only one SnmpClient should be called
			t.Error("SnmpSession Client Callback: Invalid number of snmp clients received callbacks.")
		}
		if dbgPrintCount > 0 {
			t.Error("SnmpSession Client DebugPrint: Received debugprints when api debug is disabled.")
		}
	}

	//Send a valid message and test callback without clientID
	cbCount = 0
	dbgPrintCount = 0
	api.SetDebug(true) //Enable debug in api
	mes := msg.NewSnmpMessage()
	mes.SetCommand(consts.GetRequest)
	_, err = ses.Send(mes)
	if err != nil {
		t.Error(err)
	} else {
		//Wait for incoming message
		ses.Wait()
		if cbCount != 3 { //All three SnmpClients should be called
			t.Errorf("SnmpSession Callback: Invalid number of snmp clients received callbacks. Expected callback for 3 clients. cbCount: %d", cbCount)
		}
		if dbgPrintCount != 6 { //Sent + Received prints = 3+3
			t.Errorf("SnmpSession Client DebugPrint: Invalid debugprints count when api debug is enabled: %d. Expected count: 6", dbgPrintCount)
		}
	}

}

//Test unique requestID generation
func TestReqIDGeneration(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)

	mesg := msg.NewSnmpMessage()
	for i := 1; i <= 1000; i++ {
		ses.setUniqueRequestId(&mesg)
		if int(mesg.RequestID()) != i {
			t.Errorf("Error in RequestID generation. Expected value: %d. Received value: %d.", i, mesg.RequestID())
		}
		mesg.SetRequestID(0)
	}
	mesg.SetRequestID(22)
	ses.setUniqueRequestId(&mesg)
	if mesg.RequestID() != 22 {
		t.Error("SnmpSession ReqID generation: RequestID set on SnmpMessage is overrided by SnmpSession.")
	}
}

//Test the incoming and outgoing message queue (cache) for consistency
func TestMsgQueuing(t *testing.T) {
	//Requests/Responses/TimedoutList methods test
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	var err error

	//Open SnmpSession
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(100) //Invalid port to test Timeout
	ses.SetProtocolOptions(udp)
	err = ses.Open()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)

	//Send message to invalid snmp port and test Requests()/TimedOutRequest() method
	count := 3
	for i := 0; i < count; i++ {
		ses.Send(mesg) //This message should not get any response
	}
	if len(ses.Requests()) != count {
		t.Errorf("SnmpSession Requests(): Outstanding request queue length is invalid. Len: %d.", len(ses.Requests()))
	}
	ses.Wait() //Wait for Timeout
	if len(ses.TimedOutRequests()) != (count) {
		t.Errorf("SnmpSession TimedOutRequests(): TimedOutRequests queue length is invalid. Len: %d.", len(ses.TimedOutRequests()))
	}

	for i := 0; i < count; i++ {
		ses.SyncSend(mesg) //This message should not get any response
	}
	time.Sleep(time.Second * 2)
	if len(ses.TimedOutRequests()) != (count + count) {
		t.Errorf("SnmpSession TimedOutRequests(): TimedOutRequests queue length is invalid. Len: %d.", len(ses.TimedOutRequests()))
	}

	//Send message to valid snmp port and test Responses() method
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)

	for i := 0; i < count; i++ {
		_, err = ses.Send(mesg)
		if err != nil {
			t.Error(err)
			t.FailNow()
		}
	}
	ses.Wait()
	if len(ses.Responses()) != count { //We haven't picked the response. Hence it should be in the queue
		t.Error("SnmpSession Responses(): Response queue length is invalid.")
	}

}

//Implement SnmpClient interface for Async send
type client int

func (c *client) Callback(resp *msg.SnmpMessage, reqID int32) {
	cbCount++
}
func (c *client) Authenticate(resp *msg.SnmpMessage, community string) bool {
	return true
}
func (c *client) DebugStr(dbgStr string) {
	dbgPrintCount++
}

//SnmpClient1
type client1 int

func (c *client1) Callback(resp *msg.SnmpMessage, reqID int32) {
	cbCount++
}
func (c *client1) Authenticate(resp *msg.SnmpMessage, community string) bool {
	return true
}
func (c *client1) DebugStr(dbgStr string) {
	dbgPrintCount++
}

//SnmpClient2
type client2 int

func (c *client2) Callback(resp *msg.SnmpMessage, reqID int32) {
	cbCount++
}
func (c *client2) Authenticate(resp *msg.SnmpMessage, community string) bool {
	return true
}
func (c *client2) DebugStr(dbgStr string) {
	dbgPrintCount++
}
