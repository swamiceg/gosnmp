/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package consts provides all the SNMP constants to aid in development.
//
//'webnms/snmp/hl' package derive all these constants for ease of use.
package consts

//SNMP Version constants.
type Version int

const (
	Version1  Version = 0
	Version2C Version = 1
	Version3  Version = 3
)

//PDU Type constants as per rfc3416.
type Command byte

const (
	GetRequest     Command = 0xa0
	GetNextRequest Command = 0xa1
	GetResponse    Command = 0xa2
	SetRequest     Command = 0xa3
	TrapRequest    Command = 0xa4
	GetBulkRequest Command = 0xa5
	InformRequest  Command = 0xa6
	Trap2Request   Command = 0xa7
	ReportMessage  Command = 0xa8
)

//ErrorStatus constants as per rfc3416.
type ErrorStatus int32

const (
	NoError             ErrorStatus = 0
	TooBig              ErrorStatus = 1
	NoSuchName          ErrorStatus = 2
	BadValue            ErrorStatus = 3
	ReadOnly            ErrorStatus = 4
	GeneralError        ErrorStatus = 5
	NoAccess            ErrorStatus = 6
	WrongType           ErrorStatus = 7
	WrongLength         ErrorStatus = 8
	WrongEncoding       ErrorStatus = 9
	WrongValue          ErrorStatus = 10
	NoCreation          ErrorStatus = 11
	InconsistentValue   ErrorStatus = 12
	ResourceUnavailable ErrorStatus = 13
	CommitFailed        ErrorStatus = 14
	UndoFailed          ErrorStatus = 15
	AuthorizationError  ErrorStatus = 16
	NotWritable         ErrorStatus = 17
	InconsistentName    ErrorStatus = 18
)

//SNMPv2 Exception codes as per rfc1905.
type ExceptionCode byte

const (
	NoSuchObjectExp   ExceptionCode = 128
	NoSuchInstanceExp ExceptionCode = 129
	EndOfMibViewExp   ExceptionCode = 130
)

//Snmp Variable constants. Used for creating SnmpVar.
const (
	Sequence         byte = 0x30
	Integer          byte = 0x02
	BitString        byte = 0x03
	OctetString      byte = 0x04
	Bits             byte = 0xdd
	SnmpNullVar      byte = 0x05
	ObjectIdentifier byte = 0x06
	IpAddress        byte = 0x40
	Counter          byte = 0x41
	Gauge            byte = 0x42
	TimeTicks        byte = 0x43
	Opaque           byte = 0x44
	Counter64        byte = 0x46
)

//User Security Model (USM) Unique ID.
const (
	USM int32 = 3
)

//SNMPv3 Security Level constants.
type SecurityLevel int

const (
	NoAuthNoPriv SecurityLevel = 0
	AuthNoPriv   SecurityLevel = 1
	AuthPriv     SecurityLevel = 3
)

//Constants required for USM based Authentication Protocols.
type AuthProtocol string

const (
	NO_AUTH  AuthProtocol = "1.3.6.1.6.3.10.1.1.1"
	MD5_AUTH AuthProtocol = "1.3.6.1.6.3.10.1.1.2"
	SHA_AUTH AuthProtocol = "1.3.6.1.6.3.10.1.1.3"
)

//Constants required for USM based Privacy Protocols.
type PrivProtocol string

const (
	NO_PRIV         PrivProtocol = "1.3.6.1.6.3.10.1.2.1"
	DES_PRIV        PrivProtocol = "1.3.6.1.6.3.10.1.2.2"
	TRIPLE_DES_PRIV PrivProtocol = "1.3.6.1.6.3.10.1.2.3"
	AES_128_PRIV    PrivProtocol = "1.3.6.1.6.3.10.1.2.4"
	AES_192_PRIV    PrivProtocol = "1.3.6.1.6.3.10.1.2.20"
	AES_256_PRIV    PrivProtocol = "1.3.6.1.6.3.10.1.2.21"
)

//SNMPv3 LCD Initialization constants. Used for identifying the status/error while performing discovery/time-sync operations.
const (
	InitV3Success                 int = 1
	DiscoveryFailure              int = -1
	TimeSyncFailure               int = -2
	ValidationFailure             int = -3
	UnSupportedSecurityLevelError int = -4
	NotInTimeWindowsError         int = -5
	UnknownUserNameError          int = -6
	UnknownEngineIDError          int = -7
	WrongDigestsError             int = -8
	DecryptionError               int = -9
)

//SQLDialect Constants. Passed while Initializing the database.
//Required for all the database operations.
//Also, required for acheiving inter-operability among different databases.
const (
	MySQL     int = 1
	Postgres  int = 2
	SQLite    int = 3
	Oracle    int = 4
	SQLServer int = 5
	DB2       int = 6
	Sybase    int = 7
)

//Returns SNMP version string.
func (ver Version) String() string {
	switch ver {
	case Version1:
		return "SNMPv1"
	case Version2C:
		return "SNMPv2c"
	case Version3:
		return "SNMPv3"
	default:
		return "Invalid_Version"
	}
}

//Returns Security Level string.
func (sec SecurityLevel) String() string {
	switch sec {
	case NoAuthNoPriv:
		return "NOAUTH_NOPRIV"
	case AuthNoPriv:
		return "AUTH_NOPRIV"
	case AuthPriv:
		return "AUTH_PRIV"
	default:
		return "Invalid_Security_Level"
	}
}
