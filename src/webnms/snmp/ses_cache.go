/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"sync"

	"webnms/snmp/engine"
	"webnms/snmp/msg"
)

//This is the cache data specific to Dispatcher (SnmpSession).
//Used to coordinate between incoming and outgoing messages.
type dispatcherStateReference struct {
	msg            *msg.SnmpMessage
	stateReference engine.StateReference //StateReference can be used for sending response
}

func newDispatcherStateReference(msg *msg.SnmpMessage) *dispatcherStateReference {
	return &dispatcherStateReference{
		msg: msg,
	}
}

func (sr *dispatcherStateReference) setStateReference(stateReference engine.StateReference) {
	sr.stateReference = stateReference
}

//Dispatcher (SnmpSession) message cache store
type msgCacheStore struct {
	store map[int32]*dispatcherStateReference
	mutex sync.RWMutex
}

func initMsgCacheStore() *msgCacheStore {
	return &msgCacheStore{
		store: make(map[int32]*dispatcherStateReference),
	}
}

func (m *msgCacheStore) addEntry(cache *dispatcherStateReference) {
	if cache != nil {
		m.mutex.Lock()
		defer m.mutex.Unlock()
		m.store[cache.msg.RequestID()] = cache
	}
}

func (m *msgCacheStore) removeEntry(reqID int32) *dispatcherStateReference {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	if cache, ok := m.store[reqID]; ok {
		delete(m.store, reqID)
		return cache
	}
	return nil
}

func (m *msgCacheStore) getEntry(reqID int32) *dispatcherStateReference {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	return m.store[reqID]
}

func (m *msgCacheStore) entries() map[int32]*dispatcherStateReference {
	return m.store
}
