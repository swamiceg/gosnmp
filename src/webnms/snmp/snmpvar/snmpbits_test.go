/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

func TestNewSnmpBits(t *testing.T) {

	binaryValues := []string{"11111111", "111111"}

	for _, value := range binaryValues {
		if _, e := NewSnmpBits(value, 2); e != nil {
			t.Error("Error should not be thrown for valid value,", value)
		}
	}

	hexValues := []string{"ff", "a"}

	for _, value := range hexValues {
		if _, e := NewSnmpBits(value, 16); e != nil {
			t.Error("Error should not be thrown for valid value,", value)
		}
	}
}

func TestNewSnmpBitsFromBytes(t *testing.T) {
	var v SnmpBits = NewSnmpBitsFromBytes([]byte{0xff})
	if v.Value() == nil {
		t.Error("Value should not be nil for valid SnmpBits byte array")
	}
}

func TestBitsEqual(t *testing.T) {
	v1, _ := NewSnmpBits("11111111", 2)
	v2, _ := NewSnmpBits("ff", 16)

	if !v1.Equals(*v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	v1, _ = NewSnmpBits("11110000", 2)
	v2, _ = NewSnmpBits("ff", 16)

	if v1.Equals(*v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

func TestBitsType(t *testing.T) {
	v, _ := NewSnmpBits("ff", 16)
	if v.Type() != consts.Bits {
		t.Error("Expected", consts.Bits, "got", v.Type())
	}
}

func TestBitsValue(t *testing.T) {
	v, _ := NewSnmpBits("ff", 16)
	if !bytes.EqualFold(v.Value(), []byte{0xff}) {
		t.Error("Expected [ff], got", v.Value())
	}
}

func TestBitsString(t *testing.T) {
	v, _ := NewSnmpBits("ff", 16)
	if !strings.EqualFold(v.String(), string([]byte{0xff})) {
		t.Error("Expected �, got", v.String())
	}
}

func TestBitsTagString(t *testing.T) {
	v, _ := NewSnmpBits("ff", 16)
	if !strings.EqualFold(v.TagString(), "BITS : "+string([]byte{0xff})) {
		t.Error("Expected BITS : �, got", v.TagString())
	}
}

func TestBitsEncodeVar(t *testing.T) {
	v, _ := NewSnmpBits("ff", 16)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{4, 1, 255}) {
		t.Error("Expected [4 1 255], got", byteVal)
	}
}
