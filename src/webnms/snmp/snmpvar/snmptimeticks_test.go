/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpTimeTicks by passing valid value 10000
func TestNewSnmpTimeTicks(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if v.Type() != consts.TimeTicks {
		t.Error("Expected", "67", "got", v.Type())
	}
}

//test for Type. Expected result is 67.
func TestTimeTicksType(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if v.Type() != consts.TimeTicks {
		t.Error("Expected", "67", "got", v.Type())
	}
}

//test for Value. Expected result is 10000.
func TestTimeTicksValue(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if v.Value() != 10000 {
		t.Error("Expected", "10000", "got", v.Value())
	}
}

//test for Equals by creating two timeticks types of same values.
func TestTimeTicksEquals(t *testing.T) {

	//Check Equal method with same timeticks values
	v1 := NewSnmpTimeTicks(10000)
	v2 := NewSnmpTimeTicks(10000)

	if !v1.Equals(v2) {
		t.Error("Expected", v1.Value(), "got", v2.Value())
	}

	//Check Equals method with two different timeticks values
	v1 = NewSnmpTimeTicks(20000)
	v2 = NewSnmpTimeTicks(30000)

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.Value(), "but got", v2.Value())
	}
}

//test for String. expected result is 0 hours, 1 minutes, 40 seconds.
func TestTimeTicksString(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if !strings.EqualFold(v.String(), "0 hours, 1 minutes, 40 seconds") {
		t.Error("Expected", "0 hours, 1 minutes, 40 seconds", "got", v.String())
	}
}

//test for String. expected result is 00:01:40.
func TestTimeTicksShortString(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if !strings.EqualFold(v.ShortString(), "00:01:40") {
		t.Error("Expected", "00:01:40", "got", v.ShortString())
	}
}

//test for TagString. expected result is TIMEICKS : 0 hours, 1 minutes, 40 seconds
func TestTimeTicksTagString(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	if !strings.EqualFold(v.TagString(), "TIMETICKS : 0 hours, 1 minutes, 40 seconds") {
		t.Error("Expected TIMETICKS: 0 hours, 1 minutes, 40 seconds got", v.TagString())
	}
}

//test for EncodeVar. Expected result is [65 1 100]
func TestTimeTicksEncodeVar(t *testing.T) {
	v := NewSnmpTimeTicks(10000)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding timeticks value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{67, 2, 39, 16}) {
		t.Error("Expected [67 2 39 16], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestTimeTicksDecodeVar(t *testing.T) {
	v1 := NewSnmpTimeTicks(10000)

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.Value())
	}

	v2, decErr := SnmpTimeTicks{}.DecodeVar(byteVal)
	if decErr != nil {
		t.Error("Test failed. Got error while decoding value", v1.Value())
	}

	if !v1.Equals(*v2) {
		t.Error("Expected 10000, got", v2.Value())
	}
}
