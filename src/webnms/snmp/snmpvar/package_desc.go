/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package snmpvar defines all the SNMP data types derived from ASN.1 such as Integer, Counter,
//Object Identifier etc.,
//
//It contains SnmpVar interface that provides methods to present a uniform interface for applications working with the SNMP variables. Any type that implements these methods will be treated as a Snmp Variable (eg. SnmpInt, SnmpString etc).
//
//Almost all the base datatype implementations are provided in this package that include implementations for 
//	* Integer32 and INTEGER
//	* OCTET STRING
//	* OBJECT IDENTIFIER
//	* The BITS construct
//	* IpAddress
//	* Counter32
//	* Gauge32
//	* TimeTicks
//	* Opaque
//	* Counter64
//
//Implementations include methods to create new varibles, retrieve the value of the variable both as an instance value and in printable string form.
package snmpvar
