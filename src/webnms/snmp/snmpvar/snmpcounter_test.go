/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpCounter by passing valid value 100
func TestNewSnmpCounter(t *testing.T) {
	v := NewSnmpCounter(100)
	if v.Type() != consts.Counter {
		t.Error("Expected", "65", "got", v.Type())
	}
}

//test for Type. Expected result is 65.
func TestCounterType(t *testing.T) {
	v := NewSnmpCounter(100)
	if v.Type() != consts.Counter {
		t.Error("Expected", "65", "got", v.Type())
	}
}

//test for Value. Expected result is 100.
func TestCounterValue(t *testing.T) {
	v := NewSnmpCounter(100)
	if v.Value() != 100 {
		t.Error("Expected", "100", "got", v.Value())
	}
}

//test for Equals by creating two counter types of same values.
func TestCounterEquals(t *testing.T) {
	v1 := NewSnmpCounter(100)
	v2 := NewSnmpCounter(100)

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	v1 = NewSnmpCounter(200)
	v2 = NewSnmpCounter(300)

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

//test for String. expected result is 100.
func TestCounterString(t *testing.T) {
	v := NewSnmpCounter(100)
	if !strings.EqualFold(v.String(), "100") {
		t.Error("Expected", "100", "got", v.String())
	}
}

//test for TagString. expected result is COUNTER : 100
func TestCounterTagString(t *testing.T) {
	v := NewSnmpCounter(100)
	if !strings.EqualFold(v.TagString(), "COUNTER : 100") {
		t.Error("Expected COUNTER: 100 got", v.TagString())
	}
}

//test for Diff. Give a lesser value than that of the counter value as the argument.
func TestCounterDiff(t *testing.T) {
	v := NewSnmpCounter(100)
	if v.Diff(50) != 50 {
		t.Error("Expected diff", "50", "got", v.Diff(100))
	}
}

//test for CounterDiff. Give a lesser counter value as argument.
func TestCounterCounterDiff(t *testing.T) {
	v1 := NewSnmpCounter(50)
	v2 := NewSnmpCounter(100)

	if v2.CounterDiff(v1) != 50 {
		t.Error("Expected diff", "50", "got", v2.CounterDiff(v1))
	}
}

//test for EncodeVar. Expected result is {65, 1, 100}
func TestCounterEncodeVar(t *testing.T) {
	v := NewSnmpCounter(100)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding counter value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{65, 1, 100}) {
		t.Error("Expected {65, 1, 100}, got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestCounterDecodeVar(t *testing.T) {
	v1 := NewSnmpCounter(100)

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpCounter{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 100, got", v2.String())
	}
}
