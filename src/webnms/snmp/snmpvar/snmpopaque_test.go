/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpOpaque
func TestNewSnmpOpaque(t *testing.T) {
	v := NewSnmpOpaque([]byte{2, 1, 100})
	if v.Type() != consts.Opaque {
		t.Error("Expected", "68", "got", v.Type())
	}
}

//test for NewSnmpOpaqueFromString
func TestNewSnmpOpaqueFromString(t *testing.T) {
	v := NewSnmpOpaqueFromString("something")
	if v.Type() != consts.Opaque {
		t.Error("Expected", "68", "got", v.Type())
	}
}

//test for NewSnmpOpaqueFromVar
func TestNewSnmpOpaqueFromVar(t *testing.T) {

	//test for valid variable argument
	v := NewSnmpOpaqueFromVar(NewSnmpString("something"))
	if v.Type() != consts.Opaque {
		t.Error("Expected", "68", "got", v.Type())
	}

	//test for nil argument
	v = NewSnmpOpaqueFromVar(nil)
	if v.Type() != consts.Opaque {
		t.Error("Expected", "68", "got", v.Type())
	}
}

//test for Type. Expected result is 68.
func TestOpaqueType(t *testing.T) {
	v := NewSnmpOpaqueFromString("something")
	if v.Type() != consts.Opaque {
		t.Error("Expected", "68", "got", v.Type())
	}
}

//test for Value.
func TestOpaqueValue(t *testing.T) {
	v := NewSnmpOpaque([]byte{2, 1, 0})
	if !bytes.EqualFold(v.Value(), []byte{2, 1, 0}) {
		t.Error("Expected [2 1 0] got", v.Value())
	}
}

//test for Equals by creating two opaque types.
func TestOpaqueEquals(t *testing.T) {

	//Check Equal method with same values
	v1 := NewSnmpOpaque([]byte{2, 1, 100})
	v2 := NewSnmpOpaque([]byte{2, 1, 100})

	if !v1.Equals(v2) {
		t.Error("Expected", v1.Value(), "got", v2.Value())
	}

	//Check Equals method with two different values
	v1 = NewSnmpOpaque([]byte{2, 1, 100})
	v2 = NewSnmpOpaque([]byte{2, 1, 210})

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.Value(), "but got", v2.Value())
	}
}

//test for String.
func TestOpaqueString(t *testing.T) {
	v := NewSnmpOpaque([]byte{115, 111, 109, 101, 116, 104, 105, 110, 103})
	if !strings.EqualFold(v.String(), "something") {
		t.Error("Expected", "something", "got", v.String())
	}
}

//test for TagString.
func TestOpaqueTagString(t *testing.T) {
	v := NewSnmpOpaque([]byte{115, 111, 109, 101, 116, 104, 105, 110, 103})
	if !strings.EqualFold(v.TagString(), "OPAQUE : something") {
		t.Error("Expected OPAQUE: something got", v.TagString())
	}
}

//test for EncodeVar.
func TestOpaqueEncodeVar(t *testing.T) {
	v := NewSnmpOpaque([]byte{2, 1, 100})
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding gauge value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{68, 3, 2, 1, 100}) {
		t.Error("Expected [68 3 2 1 100], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestOpaqueDecodeVar(t *testing.T) {
	v1 := NewSnmpOpaque([]byte{2, 1, 100})

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.Value())
	}

	v2, _ := SnmpOpaque{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 100, got", v2.String())
	}
}

//test for DecodeVariable.
func TestOpaqueDecodeVariable(t *testing.T) {
	v := NewSnmpOpaque([]byte{2, 1, 100})
	variable := v.DecodeVariable()
	if variable == nil {
		t.Error("Expected variable to be not nil")
	}
}
