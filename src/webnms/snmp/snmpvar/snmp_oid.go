/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"webnms/snmp/consts"
)

const (
	OidPrefix string = ".1.3.6.1.2.1."
)

//SnmpOID implements SnmpVar. This type can be used to create a SnmpVar of type OBJECT IDENTIFIER. It also has methods to retrieve the value in different forms(eg. String, int array).
//
//Some examples for valid OID are:
//  oid1 := *snmp.NewSnmpOID("1.1.0")
//  oid2 := *snmp.NewSnmpOID(".1.3.6.1.2.1.1.1.0")
//Some examples for invalid OID are:
//  oid3 := snmp.NewSnmpOID(".iso.org.dod.internet.mgmt.mib-2.system.sysDescr")
//  oid4 := snmp.NewSnmpOID("sysDescr")
//as the argument will not accept names in the OID.
//
//If the given OID does not start with dot, then the constant OidPrefix(.1.3.6.1.2.1.) in the snmp package will be prepended with the given OID.
type SnmpOID struct {
	varType byte
	value   []uint32
}

//NewSnmpOIDByInts returns SnmpOID by taking a uint32 slice as an argument. The given slice is the complete OID and method does prepend any standard prefixes. Returns nil if oidArray is nil or length of the oidArray passed is not in the range 1 to 127.
func NewSnmpOIDByInts(oidArray []uint32) *SnmpOID {
	if oidArray == nil || len(oidArray) <= 0 || len(oidArray) >= 128 {
		return nil
	}
	return &SnmpOID{varType: consts.ObjectIdentifier, value: oidArray}
}

//NewSnmpOID returns SnmpOID which requires the argument to be a String OID of the form .N.N.N, or N.N.N. If the given OID does not start with a dot, then the constant OidPrefix(.1.3.6.1.2.1.) of snmp package will be prepended it. Here N (sub-identifier) should be a number in range 1 to 4294967295.
//
//This method does not report about the invalidity of the arguments, a better way to identify whether an SnmpOID is created succesfully or not is:
//  oid := snmp.NewSnmpOID("1.1.0")
//  if oid != nil {
//	fmt.Println("SnmpOID successfully created")
//  } else {
//	fmt.Println("There is some problem in creating SnmpOID")
//  }
//Returns nil in case of errors.
func NewSnmpOID(oidString string) *SnmpOID {
	if !strings.HasPrefix(oidString, ".") {
		oidString = OidPrefix + oidString
	}
	intArr := oidToInts(oidString)
	if intArr == nil {
		return nil
	}
	return &SnmpOID{varType: consts.ObjectIdentifier, value: intArr}
}

//Equals compares this SnmpOID to the specified SnmpOID type. Returns true only if the argument is not nil or non-empty and is an SnmpOID type that represents the same value as this type.
func (oid SnmpOID) Equals(oid2 SnmpOID) bool {
	if len(oid.value) == 0 || len(oid2.value) == 0 {
		return false
	}

	if fmt.Sprintf("%v", oid.value) != fmt.Sprintf("%v", oid2.value) {
		return false
	}

	return true
}

//Type returns the variable type in byte
func (oid SnmpOID) Type() byte {
	return oid.varType
}

//Value returns the variable value in []uint32. The number of subOIDs in the OID is limited to a max of 128. The value of an OID component also ranges from 0 to 2**32 -1. That is, it ranges from 0 to 4294967295.
func (oid SnmpOID) Value() []uint32 {
	return oid.value
}

//String form of SnmpOID. Used in printing the variable. Eg. If OID is "1.1.0", then this method returns the ObjectID ".1.3.6.1.2.1.1.1.0" as a string type.
func (oid SnmpOID) String() string {
	return intToStrSlice(oid.value)
}

//TagString converts the value of this SnmpOID type to a printable string where the type is tagged before the value with a tag "Object ID : ". For e.g if the SnmpOID has the value "1.1.0", then this method will return - "Object ID : .1.3.6.1.2.1.1.1.0" .
func (oid SnmpOID) TagString() string {
	return "Object ID : " + oid.String()
}

//oidToInts is used internally to parse the string oid to int array.
func oidToInts(oidString string) []uint32 {
	oidstring := strings.TrimPrefix(oidString, ".")
	splitslice := strings.Split(oidstring, ".")
	if len(splitslice) == 0 || len(splitslice) > 128 {
		return nil
	}
	oidintslice := make([]uint32, len(splitslice))

	var subid uint32
	for i, j := range splitslice {
		suboid, err := strconv.Atoi(j)
		if err != nil {
			return nil
		} else {
			subid = uint32(suboid)
		}
		oidintslice[i] = subid
	}
	return oidintslice
}

func intToStrSlice(ints []uint32) string {
	var oidStr string = ""
	for _, val := range ints {
		oidStr = oidStr + "." + strconv.FormatInt(int64(val), 10)
	}
	return oidStr
}

func UInt32ToInts(uIntSlice []uint32) []int {

	returnArr := make([]int, len(uIntSlice))
	for i, value := range uIntSlice {
		returnArr[i] = int(value)
	}
	return returnArr
}

func IntToUInt32s(intSlice []int) []uint32 {

	returnArr := make([]uint32, len(intSlice))
	for i, value := range intSlice {
		returnArr[i] = uint32(value)
	}
	return returnArr
}

//EncodeVar encodes this oid as per ASN1 encoding standards.
func (oid SnmpOID) EncodeVar() ([]byte, error) {
	return asn1.Marshal(asn1.ObjectIdentifier(UInt32ToInts(oid.value)))
}

//decodeVar decodes the ASN1 encoded byte array into SnmpOID type
func (oid SnmpOID) decodeVar(oidBytes []byte) (*SnmpOID, error) {
	SnmpOidDec1 := new(asn1.ObjectIdentifier)
	_, err := asn1.Unmarshal(oidBytes, SnmpOidDec1)
	if err != nil {
		return nil, errors.New("Unable to decode OID : " + err.Error())
	}
	return NewSnmpOIDByInts(IntToUInt32s(*SnmpOidDec1)), err
}
