/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"strings"
	"testing"

	"webnms/snmp/consts"
)

type testValue struct {
	varType byte
	value   string
	result  string
}

var tests = []testValue{
	{consts.OctetString, "something", "STRING : something"},
	{consts.Integer, "100", "INTEGER : 100"},
	{consts.Integer, "'100'b", "INTEGER : 4"},
	{consts.Integer, "'ff'h", "INTEGER : 255"},
	{consts.IpAddress, "192.168.1.100", "IPADDRESS : 192.168.1.100"},
	{consts.Opaque, "something", "OPAQUE : something"},
	{consts.ObjectIdentifier, "1.5.0", "Object ID : .1.3.6.1.2.1.1.5.0"},
	{consts.TimeTicks, "10000", "TimeTicks : 0 hours, 1 minutes, 40 seconds"},
	{consts.Counter, "100", "Counter : 100"},
	{consts.Counter, "'100'b", "Counter : 4"},
	{consts.Counter, "'ff'h", "Counter : 255"},
	{consts.Counter64, "1000000000", "Counter64 : 1000000000"},
	{consts.Counter64, "'100'b", "Counter64 : 4"},
	{consts.Counter64, "'ff'H", "Counter64 : 255"},
	{consts.Gauge, "100", "Gauge : 100"},
	{consts.Gauge, "'100'b", "Gauge : 4"},
	{consts.Gauge, "'ff'h", "Gauge : 255"},
}

var invalidtests = []testValue{
	{consts.Integer, "something", "INTEGER"},
	{consts.IpAddress, "192.168.1.1.1", "IPADDRESS"},
	{consts.ObjectIdentifier, "something", "Object ID"},
	{consts.TimeTicks, "something", "TIMETICKS"},
	{consts.Counter, "something", "COUNTER"},
	{consts.Counter64, "something", "COUNTER64"},
	{consts.Gauge, "something", "GAUGE"},
}

func TestCreateSnmpVar(t *testing.T) {
	//tests for valid behaviour
	for _, testVal := range tests {
		v, e := CreateSnmpVar(testVal.varType, testVal.value)
		if e != nil {
			t.Error("Error creating variable of type", testVal.varType, e.Error())
		} else {
			if !strings.EqualFold(v.TagString(), testVal.result) {
				t.Error("Expected", testVal.result, "got", v.TagString())
			}
		}
	}

	//test for invalid value for a valid type
	for _, iTestVal := range invalidtests {
		v, e := CreateSnmpVar(iTestVal.varType, iTestVal.value)
		if v != nil && e == nil {
			t.Error("Expected error not thrown for invalid value:", iTestVal.value, "while creating", iTestVal.result, "variable", v.String())
		}
	}

	//test for invalid type
	v, e := CreateSnmpVar(consts.SnmpNullVar, "")
	if v != nil && e == nil {
		t.Error("Expected error not thrown for invalid type", consts.SnmpNullVar)
	}
}

func TestCompareVar(t *testing.T) {

	var v1, v2 SnmpVar

	//test for 2 equal variables
	v1 = NewSnmpString("something")
	v2 = NewSnmpString("something")
	if !CompareVar(v1, v2) {
		t.Error(v1.String(), "not equal as", v2.String())
	}

	//test for 2 different type variables
	v2 = NewSnmpInt(100)
	if CompareVar(v1, v2) {
		t.Error(v1.String(), "equal as", v2.String())
	}

	//test for 2 same type variables with different values
	v2 = NewSnmpString("someotherthing")
	if CompareVar(v1, v2) {
		t.Error(v1.String(), "equal as", v2.String())
	}
}
