/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpOID
func TestNewSnmpOID(t *testing.T) {

	//tests for valid oid
	values := []string{"1.1.0", ".1.3.6.1.2.1.1.1.0", "-1.1.0"}
	for _, oid := range values {
		v := NewSnmpOID(oid)
		if v == nil {
			t.Error("SnmpOID not created for", oid)
		}
	}

	//tests for invalid oid
	iValues := []string{"1.a.0", "sysDesc", "1..0"}
	for _, oid := range iValues {
		v := NewSnmpOID(oid)
		if v != nil {
			t.Error("SnmpOID should not be created for", oid)
		}
	}
}

//test for NewSnmpOIDByInts
func TestNewSnmpOIDByInts(t *testing.T) {

	//test for valid value
	val := []uint32{1, 3, 6, 1, 2, 1, 1, 5, 0}
	v := NewSnmpOIDByInts(val)
	if v == nil {
		t.Error("SnmpOID not created for", val)
	}
}

//test for Type. Expected result is 6.
func TestOIDType(t *testing.T) {
	v := NewSnmpOID("1.1.0")
	if v.Type() != consts.ObjectIdentifier {
		t.Error("Expected", "6", "got", v.Type())
	}
}

//test for Value.
func TestOIDValue(t *testing.T) {
	v := NewSnmpOID("1.5.0")
	strSlice1 := fmt.Sprintf("%v", v.Value())
	strSlice2 := fmt.Sprintf("%v", []uint32{1, 3, 6, 1, 2, 1, 1, 5, 0})
	if strSlice1 != strSlice2 {
		t.Error("Expected", strSlice1, "got", v.Value())
	}
}

//test for Equals
func TestOIDEquals(t *testing.T) {

	//Check Equal method with same oid values
	v1 := NewSnmpOID("1.5.0")
	v2 := NewSnmpOID(".1.3.6.1.2.1.1.5.0")

	if !v1.Equals(*v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	//Check Equals method with two different oid values
	v1 = NewSnmpOID("1.5.0")
	v2 = NewSnmpOID("1.1.0")

	if v1.Equals(*v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

//test for String. expected result is .1.3.6.1.2.1.1.5.0.
func TestOIDString(t *testing.T) {
	v := NewSnmpOID("1.5.0")
	if !strings.EqualFold(v.String(), ".1.3.6.1.2.1.1.5.0") {
		t.Error("Expected", ".1.3.6.1.2.1.1.5.0", "got", v.String())
	}
}

//test for TagString. expected result is Object ID : .1.3.6.1.2.1.1.5.0
func TestOIDTagString(t *testing.T) {
	v := NewSnmpOID(".1.3.6.1.2.1.1.5.0")
	if !strings.EqualFold(v.TagString(), "Object ID : .1.3.6.1.2.1.1.5.0") {
		t.Error("Expected Object ID: .1.3.6.1.2.1.1.5.0 got", v.TagString())
	}
}

//test for EncodeVar. Expected result is [6 8 43 6 1 2 1 1 5 0]
func TestOIDEncodeVar(t *testing.T) {
	v := NewSnmpOID("1.5.0")
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding gauge value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{6, 8, 43, 6, 1, 2, 1, 1, 5, 0}) {
		t.Error("Expected [6 8 43 6 1 2 1 1 5 0], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestOIDDecodeVar(t *testing.T) {
	v1 := NewSnmpOID("1.5.0")

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpOID{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected .1.3.6.1.2.1.1.5.0, got", v2.String())
	}
}
