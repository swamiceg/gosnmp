/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"encoding/asn1"
	"encoding/hex"
	"errors"
	"strings"

	"webnms/snmp/consts"
)

//SnmpString is the type of SNMP String Variable. Implements SnmpVar. SnmpString can be used to create an SnmpVar of type OCTET STRING. It also has methods to retrieve the value in different forms(eg. String, byte).
type SnmpString struct {
	varType byte
	value   []byte
}

//NewSnmpString creates SnmpString with string argument. Hex string can be given to this method. To do so just enclose the string within single quotes. For example for 0x09a2bec3 use NewSnmpString("'09a2bec3'").
func NewSnmpString(str string) SnmpString {

	if strings.HasPrefix(str, "'") && strings.HasSuffix(str, "'") {
		strs := strings.Trim(str, "'")
		i, e := hex.DecodeString(strs)

		if e != nil {
			return SnmpString{varType: consts.OctetString, value: nil}
		} else {
			return SnmpString{varType: consts.OctetString, value: []byte(i)}
		}
	}
	return SnmpString{varType: consts.OctetString, value: []byte(str)}
}

//NewSnmpStringFromBytes creates SnmpString by taking byte array of octet string.
func NewSnmpStringFromBytes(byteVal []byte) SnmpString {
	return SnmpString{varType: consts.OctetString, value: byteVal}
}

//Equals returns true if both the SnmpString value are equal.
func (octString SnmpString) Equals(snmpStr SnmpString) bool {
	str1 := octString.value
	str2 := snmpStr.value

	return bytes.Equal(str1, str2)
}

//Type returns the variable type in byte
func (octString SnmpString) Type() byte {
	return octString.varType
}

//Value returns the variable value in string
func (octString SnmpString) Value() string {
	return octString.String()
}

//String returns string form of the SnmpString type in printable string. Used in printing variables.
func (octString SnmpString) String() string {
	return string(octString.value)
}

//Bytes returns the value of this SnmpString type in raw bytes.
func (octString SnmpString) Bytes() []byte {
	return octString.value
}

//TagString converts the value of this SnmpString type to a printable string where the type is tagged before the value with a tag "STRING : ". For e.g if the SnmpString has the value "WebNMS", then this method will return - STRING : WebNMS.
func (octString SnmpString) TagString() string {
	return "STRING : " + octString.String()
}

//EncodeVar encodes this variable as per ASN1 encoding standards.
func (octString SnmpString) EncodeVar() ([]byte, error) {
	stringbytes, err := asn1.Marshal(octString.value)
	return stringbytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpString type
func (octString SnmpString) decodeVar(octetStrBytes []byte) (*SnmpString, error) {
	SnmpStrDec1 := new([]byte)
	_, err := asn1.Unmarshal(octetStrBytes, SnmpStrDec1)
	if err != nil {
		return nil, errors.New("Unable to decode as SnmpString : " + err.Error())
	}
	octString = NewSnmpStringFromBytes(*SnmpStrDec1)
	return &octString, err
}
