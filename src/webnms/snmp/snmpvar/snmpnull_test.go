/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpNull
func TestNewSnmpNull(t *testing.T) {
	v := NewSnmpNull()
	if v.Type() != consts.SnmpNullVar {
		t.Error("Expected", "5", "got", v.Type())
	}
}

//test for NewSnmpNullErr with various ExceptionCodes like NoSuchObjectExp, NoSuchInstanceExp, EndOfMibViewExp, & invalid code too.
func TestNewSnmpNullErr(t *testing.T) {

	exceptions := []consts.ExceptionCode{consts.NoSuchObjectExp, consts.NoSuchInstanceExp, consts.EndOfMibViewExp, consts.ExceptionCode(131)}

	for _, value := range exceptions {
		v := NewSnmpNullErr(value)
		if v.Type() == consts.SnmpNullVar {
			switch value {
			case consts.NoSuchObjectExp, consts.NoSuchInstanceExp, consts.EndOfMibViewExp:
				if v.ErrorValue() != value {
					t.Error("Expected", value, "got", v.ErrorValue())
				}
			default:
				if v.ErrorValue() != consts.ExceptionCode(0) {
					t.Error("Expected", "0", "got", v.ErrorValue())
				}
			}
		} else {
			t.Error("Case failed for ExceptionCode", value, ": Expected type", "5", "got", v.Type())
		}
	}
}

//test for Type. Expected result is 5.
func TestGaugeNull(t *testing.T) {
	v := NewSnmpNull()
	if v.Type() != consts.SnmpNullVar {
		t.Error("Expected", "5", "got", v.Type())
	}
}

//test for Equals by creating two null values
func TestNullEquals(t *testing.T) {

	//Check Equal method with same null values
	v1 := NewSnmpNull()
	v2 := NewSnmpNull()

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	//Check Equals method with same exceptionCodes
	v1 = NewSnmpNullErr(consts.NoSuchObjectExp)
	v2 = NewSnmpNullErr(consts.NoSuchObjectExp)

	if !v1.Equals(v2) {
		t.Error("Expected *", v1.String(), "* but got *", v2.String(), "*")
	}

	//Check Equals method with two different exceptionCodes
	v1 = NewSnmpNullErr(consts.NoSuchObjectExp)
	v2 = NewSnmpNullErr(consts.NoSuchInstanceExp)

	if v1.Equals(v2) {
		t.Error("Expected other than *", v1.String(), "* but got *", v2.String(), "*")
	}
}

//test for String. expected result is NULL.
func TestNullString(t *testing.T) {
	//test case for default null object
	v := NewSnmpNull()
	if !strings.EqualFold(v.String(), "NULL") {
		t.Error("Expected", "NULL", "got", v.String())
	}

	//test case for null object with exception code
	v = NewSnmpNullErr(consts.NoSuchObjectExp)
	if !strings.EqualFold(v.String(), "There is no such object in this MIB.") {
		t.Error("Expected *", "There is no such object in this MIB.", "* got", v.String())
	}
}

//test for TagString. expected result is NULLOBJ : 100
func TestNullTagString(t *testing.T) {
	//test case for default null object
	v := NewSnmpNull()
	if !strings.EqualFold(v.TagString(), "NULLOBJ : NULL") {
		t.Error("Expected NULLOBJ: NULL got", v.TagString())
	}

	//test case for null object with exception code
	v = NewSnmpNullErr(consts.NoSuchObjectExp)
	if !strings.EqualFold(v.TagString(), "NULLOBJ : There is no such object in this MIB.") {
		t.Error("Expected *", "NULLOBJ: There is no such object in this MIB.", "* got", v.TagString())
	}
}

//test for EncodeVar. Expected result is [5 0]
func TestNullEncodeVar(t *testing.T) {

	//test case for default null object
	v := NewSnmpNull()
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding null value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{5, 0}) {
		t.Error("Expected [5 0], got", byteVal)
	}

	//test case for null object with exception code
	v = NewSnmpNullErr(consts.NoSuchObjectExp)
	byteVal, err = v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding null value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{5, 0, 128, 0}) {
		t.Error("Expected [5 0 128 0], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestNullDecodeVar(t *testing.T) {

	//test case for default null object
	v1 := NewSnmpNull()
	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}
	v2, _ := SnmpNull{}.decodeVar(byteVal)
	if !v1.Equals(*v2) {
		t.Error("Expected NULL, got", v2.String())
	}

	//test case for null object with exception code
	v1 = NewSnmpNullErr(consts.NoSuchObjectExp)
	byteVal, err = v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}
	v2, _ = SnmpNull{}.decodeVar(byteVal)
	if !v1.Equals(*v2) {
		t.Error("Expected *", "There is no such object in this MIB.", "* got *", v2.String(), "*")
	}
}
