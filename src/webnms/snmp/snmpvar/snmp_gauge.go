/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"strconv"

	"webnms/snmp/consts"
)

//SnmpGauge implements SnmpVar. Type of SNMP Gauge variable. Data is stored in uint32.
type SnmpGauge struct {
	varType byte
	value   uint32
}

//NewSnmpGauge creates a new SnmpGauge by taking uint32 as argument. The valid values ranges from 0 to 4294967295(both inclusive).
func NewSnmpGauge(value uint32) SnmpGauge {
	return SnmpGauge{varType: consts.Gauge, value: value}
}

//Equals compares this SnmpGauge to the specified SnmpGauge.
//Returns true only if both the gauge values are same.
func (gauge SnmpGauge) Equals(gauge2 SnmpGauge) bool {
	return gauge.value == gauge2.value
}

//Type returns the variable type in byte
func (gauge SnmpGauge) Type() byte {
	return gauge.varType
}

//Value returns the variable value in uint32
func (gauge SnmpGauge) Value() uint32 {
	return gauge.value
}

//String returns the string form of the SnmpGauge type in printable string. Used in printing variables.
func (gauge SnmpGauge) String() string {
	return strconv.FormatInt(int64(gauge.value), 10)
}

//TagString converts the value of this SnmpGauge to a printable string where the type is tagged before the value with a tag "GAUGE : ".
func (gauge SnmpGauge) TagString() string {
	return "GAUGE : " + gauge.String()
}

//EncodeVar encodes this gauge variable as per ASN1 encoding standards.
func (gauge SnmpGauge) EncodeVar() ([]byte, error) {
	gaugeBytes, err := asn1.Marshal(int32(gauge.value))
	gaugeBytes[0] = consts.Gauge
	return gaugeBytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpGauge type
func (gauge SnmpGauge) decodeVar(gaugeBytes []byte) (*SnmpGauge, error) {
	gaugeBytes[0] = consts.Integer
	SnmpGauDec1 := new(int32)
	_, err := asn1.Unmarshal(gaugeBytes, SnmpGauDec1)
	if err != nil {
		return nil, errors.New("Unable to decode Gauge value : " + err.Error())
	}
	gauge = NewSnmpGauge(uint32(*SnmpGauDec1))
	return &gauge, err
}
