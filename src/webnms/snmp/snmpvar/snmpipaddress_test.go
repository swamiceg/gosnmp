/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"net"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpIp
func TestNewSnmpIp(t *testing.T) {

	//tests for valid ip address
	value := []string{"192.168.1.100", "localhost", ""}

	for _, ip := range value {
		v := NewSnmpIp(ip)
		if v == nil {
			t.Error("SnmpIpAddress not created for value", ip)
		}
	}

	//tests for invalid ip address
	iValue := []string{"192.168.1.100.1", "1.a.3.b.5", "xyz"}

	for _, ip := range iValue {
		v := NewSnmpIp(ip)
		if v != nil {
			t.Error("SnmpIpAddress should not be created for value", ip)
		}
	}
}

//test for NewSnmpIpFromBytes
func TestNewSnmpIpFromBytes(t *testing.T) {

	//tests for valid byte array
	value := [][]byte{[]byte{1, 2, 3, 4}}

	for _, ipArr := range value {
		v := NewSnmpIpFromBytes(ipArr)
		if v == nil {
			t.Error("SnmpIpAddress not created for value", ipArr)
		}
	}

	//tests for invalid byte array
	iValue := [][]byte{[]byte{1, 2, 3, 4, 5}, []byte{1, 2, 3}}

	for _, ipArr := range iValue {
		v := NewSnmpIpFromBytes(ipArr)
		if v != nil {
			t.Error("SnmpIpAddress should not be created for value", ipArr)
		}
	}
}

//test for NewSnmpIpFromIP
func TestNewSnmpIpFromIP(t *testing.T) {

	//test for valid IP
	ip := net.ParseIP("127.0.0.1").To4()
	v := NewSnmpIpFromIP(ip)
	if v == nil {
		t.Error("SnmpIpAddress should not be created for value", ip)
	}

	//test for invalid IP
	ip = net.ParseIP("127.0.0.1")
	v = NewSnmpIpFromIP(ip)
	if v != nil {
		t.Error("SnmpIpAddress should not be created for value", ip)
	}

}

//test for Type. Expected result is 64.
func TestIpAddressType(t *testing.T) {
	v := NewSnmpIp("localhost")
	if v.Type() != consts.IpAddress {
		t.Error("Expected", "64", "got", v.Type())
	}
}

//test for Value.
func TestIpAddressValue(t *testing.T) {
	v := NewSnmpIp("localhost")
	if bytes.EqualFold(v.Value(), net.ParseIP("localhost").To4()) {
		t.Error("Expected", "127.0.0.1", "got", v.Value())
	}
}

//test for Equals by creating two ipaddress types.
func TestIpAddressEquals(t *testing.T) {

	//Check Equals method with same values
	v1 := NewSnmpIp("localhost")
	v2 := NewSnmpIp("localhost")

	if !v1.Equals(*v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	//Check Equals method with two different values
	v1 = NewSnmpIp("localhost")
	v2 = NewSnmpIp("1.2.3.4")

	if v1.Equals(*v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

//test for String.
func TestIpAddressString(t *testing.T) {

	//test by giving ip address
	v := NewSnmpIp("localhost")
	if !strings.EqualFold(v.String(), "127.0.0.1") {
		t.Error("Expected", "127.0.0.1", "got", v.String())
	}

	//test by giving hostname
	v = NewSnmpIp("1.1.1.1")
	if !strings.EqualFold(v.String(), "1.1.1.1") {
		t.Error("Expected", "1.1.1.1", "got", v.String())
	}
}

//test for TagString.
func TestIpAddressTagString(t *testing.T) {
	v := NewSnmpIp("localhost")
	if !strings.EqualFold(v.TagString(), "IPADDRESS : 127.0.0.1") {
		t.Error("Expected IPADDRESS: 1.1.1.1 got", v.TagString())
	}
}

//test for EncodeVar.
func TestIpAddressEncodeVar(t *testing.T) {
	v := NewSnmpIp("1.1.1.1")
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding ip value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{64, 4, 1, 1, 1, 1}) {
		t.Error("Expected [64 1 1 1 1 1], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestIpAddressDecodeVar(t *testing.T) {
	v1 := NewSnmpIp("198.168.1.100")

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpIpAddress{}.DecodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 198.168.1.100, got", v2.String())
	}
}
