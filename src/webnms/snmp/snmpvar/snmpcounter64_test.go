/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpCounter64 by passing valid value 123456789
func TestNewSnmpCounter64(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	if v.Type() != consts.Counter64 {
		t.Error("Expected", "70", "got", v.Type())
	}
}

//test for Type. Expected result is 70.
func TestCounter64Type(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	if v.Type() != consts.Counter64 {
		t.Error("Expected", "70", "got", v.Type())
	}
}

//test for Value. Expected result is 123456789.
func TestCounter64Value(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	if v.Value() != 123456789 {
		t.Error("Expected", "123456789", "got", v.Value())
	}
}

//test for Equals by creating two counter64 types of same/different values.
func TestCounter64Equals(t *testing.T) {
	v1 := NewSnmpCounter64(123456789)
	v2 := NewSnmpCounter64(123456789)

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	v1 = NewSnmpCounter64(100000000)
	v2 = NewSnmpCounter64(200000000)

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

//test for String. expected result is 123456789.
func TestCounter64String(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	if !strings.EqualFold(v.String(), "123456789") {
		t.Error("Expected", "123456789", "got", v.String())
	}
}

//test for TagString. expected result is COUNTER64 : 123456789
func TestCounter64TagString(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	if !strings.EqualFold(v.TagString(), "COUNTER64 : 123456789") {
		t.Error("Expected COUNTER64: 123456789 got", v.TagString())
	}
}

//test for Diff. Give a lesser value than that of the counter64 value as the argument.
func TestCounter64Diff(t *testing.T) {
	v := NewSnmpCounter64(100000000)
	if v.Diff(50000000) != 50000000 {
		t.Error("Expected diff", "50000000", "got", v.Diff(50000000))
	}

	//difference overflows
	if v.Diff(150000000) != 18446744073659551616 {
		t.Error("Expected diff", "18446744073659551616", "got", v.Diff(150000000))
	}
}

//test for Counter64Diff. Give a lesser counter64 value as argument.
func TestCounter64Counter64Diff(t *testing.T) {
	v1 := NewSnmpCounter64(50000000)
	v2 := NewSnmpCounter64(100000000)

	if v2.Counter64Diff(v1) != 50000000 {
		t.Error("Expected diff", "50000000", "got", v2.Counter64Diff(v1))
	}
}

//test for EncodeVar. Expected result is [70 4 7 91 205 21]
func TestCounter64EncodeVar(t *testing.T) {
	v := NewSnmpCounter64(123456789)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding counter value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{70, 4, 7, 91, 205, 21}) {
		t.Error("Expected [70 4 7 91 205 21], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestCounter64DecodeVar(t *testing.T) {
	v1 := NewSnmpCounter64(123456789)

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpCounter64{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 123456789, got", v2.String())
	}
}
