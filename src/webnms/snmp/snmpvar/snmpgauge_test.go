/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpGauge by passing valid value 100
func TestNewSnmpGauge(t *testing.T) {
	v := NewSnmpGauge(100)
	if v.Type() != consts.Gauge {
		t.Error("Expected", "66", "got", v.Type())
	}
}

//test for Type. Expected result is 66.
func TestGaugeType(t *testing.T) {
	v := NewSnmpGauge(100)
	if v.Type() != consts.Gauge {
		t.Error("Expected", "66", "got", v.Type())
	}
}

//test for Value. Expected result is 100.
func TestGaugeValue(t *testing.T) {
	v := NewSnmpGauge(100)
	if v.Value() != 100 {
		t.Error("Expected", "100", "got", v.Value())
	}
}

//test for Equals by creating two gauge types of same values.
func TestGaugeEquals(t *testing.T) {

	//Check Equal method with same gaugevalues
	v1 := NewSnmpGauge(100)
	v2 := NewSnmpGauge(100)

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	//Check Equals method with two different gauge values
	v1 = NewSnmpGauge(200)
	v2 = NewSnmpGauge(300)

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

//test for String. expected result is 100.
func TestGaugeString(t *testing.T) {
	v := NewSnmpGauge(100)
	if !strings.EqualFold(v.String(), "100") {
		t.Error("Expected", "100", "got", v.String())
	}
}

//test for TagString. expected result is GAUGE : 100
func TestGaugeTagString(t *testing.T) {
	v := NewSnmpGauge(100)
	if !strings.EqualFold(v.TagString(), "GAUGE : 100") {
		t.Error("Expected GAUGE: 100 got", v.TagString())
	}
}

//test for EncodeVar. Expected result is [65 1 100]
func TestGaugeEncodeVar(t *testing.T) {
	v := NewSnmpGauge(100)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding gauge value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{66, 1, 100}) {
		t.Error("Expected [66 1 100], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestGaugeDecodeVar(t *testing.T) {
	v1 := NewSnmpGauge(100)

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpGauge{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 100, got", v2.String())
	}
}
