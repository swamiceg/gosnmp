/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

func TestNewSnmpString(t *testing.T) {

	values := []string{"something", "'736f6d657468696e67'"}

	for _, value := range values {
		v := NewSnmpString(value)
		if v.Bytes() == nil {
			t.Error("Expected", value, "got", v.String())
		}
	}
}

func TestNewSnmpStringFromBytes(t *testing.T) {
	var v SnmpString = NewSnmpStringFromBytes([]byte("something"))
	if v.Bytes() == nil {
		t.Error("Expected something, got", v.String())
	}
}

func TestStringEqual(t *testing.T) {
	var v1, v2 SnmpString
	v1 = NewSnmpString("something")
	v2 = NewSnmpString("something")

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	v1 = NewSnmpString("something")
	v2 = NewSnmpString("someotherthing")

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

func TestStringType(t *testing.T) {
	var v SnmpString = NewSnmpString("something")
	if v.Type() != consts.OctetString {
		t.Error("Expected", consts.OctetString, "got", v.Type())
	}
}

func TestStringValue(t *testing.T) {
	var v SnmpString = NewSnmpString("something")
	if !strings.EqualFold(v.Value(), "something") {
		t.Error("Expected something, got", v.Value())
	}
}

func TestStringString(t *testing.T) {
	var v SnmpString = NewSnmpString("something")
	if !strings.EqualFold(v.String(), "something") {
		t.Error("Expected something, got", v.String())
	}
}

func TestStringTagString(t *testing.T) {
	var v SnmpString = NewSnmpString("something")
	if !strings.EqualFold(v.TagString(), "STRING : something") {
		t.Error("Expected something, got", v.TagString())
	}
}

func TestStringEncodeVar(t *testing.T) {
	var v SnmpString = NewSnmpString("something")
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{4, 9, 115, 111, 109, 101, 116, 104, 105, 110, 103}) {
		t.Error("Expected something, got", byteVal)
	}
}

func TestStringDecodeVar(t *testing.T) {
	var v1 SnmpString = NewSnmpString("something")
	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpString{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected something, got", v2.String())
	}
}
