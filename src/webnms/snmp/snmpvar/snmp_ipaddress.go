/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"errors"
	"net"

	"webnms/snmp/consts"
)

//SnmpIpAddress is the type of SNMP IpAddress Variable. Implements SnmpVar. Can be used to create a SnmpVar of type SnmpIpAddress. It also has methods to retrieve the value in different forms(eg. String, byte). Value is stored as net.IP.
type SnmpIpAddress struct {
	varType byte
	value   net.IP
}

//NewSnmpIp creates a new SnmpIpAddress with the specified host name or IP address as a string. This does not show any errors. To ensure that a valid IpAddress is created, always try to use a valid dotted format ip address or a valid hostname. Supplying hostname to this constructor could result in delays caused by DNS lookups and in case a failure occurs then nil will be returned. If empty parameter is passed as an argument to this function, then the type will be initialized with the IP-address "0.0.0.0".
func NewSnmpIp(ipString string) *SnmpIpAddress {
	if len(ipString) == 0 {
		ipString = "0.0.0.0"
	}
	addr, err := net.LookupHost(ipString)
	if err != nil {
		return nil
	}
	ipAdd := net.ParseIP(addr[0]).To4()
	return &SnmpIpAddress{varType: consts.IpAddress, value: ipAdd}
}

//NewSnmpIpFromIP creates a new SnmpIpAddress by accepting the net.IP. Returns nil if net.IP is not a byte array of length 4. (i.e)IP.To4() can to used to create a 4 byte IP.
func NewSnmpIpFromIP(ip net.IP) *SnmpIpAddress {
	if len(ip) != 4 {
		return nil
	}
	return &SnmpIpAddress{varType: consts.IpAddress, value: ip.To4()}
}

//NewSnmpIpFromBytes creates a new SnmpIpAddress by accepting the raw IP address - 4 bytes. Returns nil if the length of the byte array is not equal to 4.
func NewSnmpIpFromBytes(byteArr []byte) *SnmpIpAddress {
	if len(byteArr) == 4 {
		ipv4 := net.IPv4(byteArr[0], byteArr[1], byteArr[2], byteArr[3])
		return &SnmpIpAddress{varType: consts.IpAddress, value: ipv4}
	}
	return nil
}

//Equals compares this SnmpIpAddress to the specified SnmpIpAddress type. Returns true only if both the ipaddress values are same.
func (snmpIP SnmpIpAddress) Equals(snmpAdd SnmpIpAddress) bool {
	return snmpIP.value.Equal(snmpAdd.value)
}

//Type returns the variable type in byte
func (snmpIP SnmpIpAddress) Type() byte {
	return snmpIP.varType
}

//Value returns the variable value in net.IP
func (snmpIP SnmpIpAddress) Value() net.IP {
	return snmpIP.value
}

//String returns the string form of the SnmpIpAddress in printable string. Used in printing variables.
func (snmpIP SnmpIpAddress) String() string {
	return snmpIP.value.String()
}

//TagString converts the value of this SnmpIpAddress type to a printable string where the type is tagged before the value with a tag "IPADDRESS : ".
func (ipAddress SnmpIpAddress) TagString() string {
	return "IPAddress : " + ipAddress.String()
}

//EncodeVar encodes this ipaddress variable as per ASN1 encoding standards.
func (snmpIP SnmpIpAddress) EncodeVar() ([]byte, error) {
	slice1 := []byte{consts.IpAddress, net.IPv4len}
	return append(slice1, snmpIP.value...), nil
}

//decodeVar decodes the ASN1 encoded byte array into SnmpIpAddress type
func (snmpIP SnmpIpAddress) DecodeVar(ipAddrBytes []byte) (*SnmpIpAddress, error) {
	if ipAddrBytes == nil || len(ipAddrBytes) != 6 {
		return nil, errors.New("Unable to decode IpAddress : Invalid byte array")
	}
	snmpIPs := NewSnmpIpFromIP(ipAddrBytes[2:])
	return snmpIPs, nil
}
