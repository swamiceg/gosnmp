/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"encoding/asn1"
	"errors"

	"webnms/snmp/consts"
)

//SnmpBitString implements SnmpVar. Can be used to create an SnmpVar of type BITSTRING.
//It also has methods to retrive the value in different forms(eg. String, byte).
type SnmpBitString struct {
	varType byte
	value   []byte
}

//NewSnmpBitStringFromString returns SnmpBitstring by taking a string as argument. The bytes of the String should be encoded such that the first byte should contain the count of bits in the last byte.
func NewSnmpBitStringFromString(str string) SnmpBitString {
	return SnmpBitString{varType: consts.BitString, value: []byte(str)}
}

//NewSnmpBitStringFromBytes returns SnmpBitstring by taking byte slice as argument. The first byte should contain the count of bits in the last byte, which are not relevant.
func NewSnmpBitStringFromBytes(byteVal []byte) SnmpBitString {
	return SnmpBitString{varType: consts.BitString, value: byteVal}
}

//NewSnmpBitString encodes and returns SnmpBitstring by taking a byte array. No need to specify the count of unused bits in the last byte.
//The length parameter should just specify the total length of the bit string(which is the number of bits).
//
//Retuns nil on invalid parameters such as size less than the total number of bits in the array.
func NewSnmpBitString(value []byte, length int) *SnmpBitString {

	if len(value)*8 < length {
		return nil
	}
	arrSize := length / 8
	if length%8 > 0 {
		arrSize = arrSize + 1
	}
	actualValue := value[:arrSize]
	bitstr := asn1.BitString{Bytes: actualValue, BitLength: length}

	byteval, err := asn1.Marshal(bitstr)

	if err != nil {
		return nil
	}

	return &SnmpBitString{varType: consts.BitString, value: byteval[2:]}
}

//Equals compares this SnmpBitString to the specified SnmpBitString type.
//Returns true only if both the encoded bytes are equal.
func (bitStr SnmpBitString) Equals(bitStr2 SnmpBitString) bool {

	bitstr1 := bitStr.value
	bitstr2 := bitStr2.value

	return bytes.Equal(bitstr1, bitstr2)
}

//Value returns the value of this SnmpBitString as raw bytes.
func (bitStr SnmpBitString) Value() []byte {
	return bitStr.value
}

//Type returns the variable type in byte
func (bitStr SnmpBitString) Type() byte {
	return bitStr.varType
}

//String returns the string form of the SnmpBitString in printable string used in printing variables.
func (bitStr SnmpBitString) String() string {
	return string(bitStr.value)
}

//TagString converts the value of this SnmpBitString to a printable string where the type is tagged before the value with a tag "BITSTRING: ".
func (bitStr SnmpBitString) TagString() string {
	return "BITSTRING: " + bitStr.String()
}

//EncodeVar encodes this bitstring variable as per ASN1 encoding standards.
func (bitStr SnmpBitString) EncodeVar() ([]byte, error) {
	bitStringBytes, err := asn1.Marshal(bitStr.value)
	bitStringBytes[0] = consts.BitString
	return bitStringBytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpBitString type
func (bitStr SnmpBitString) decodeVar(bitStringBytes []byte) (*SnmpBitString, error) {
	bitStringBytes[0] = consts.OctetString
	SnmpBitStrDec := new([]byte)
	_, err := asn1.Unmarshal(bitStringBytes, SnmpBitStrDec)

	if err != nil {
		return nil, errors.New("Unable to decode BitString : " + err.Error())
	}

	return &SnmpBitString{consts.BitString, *SnmpBitStrDec}, nil
}
