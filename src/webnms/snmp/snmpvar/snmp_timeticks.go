/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"strconv"
	"strings"

	"webnms/snmp/consts"
)

//SnmpTimeTicks is the type of SNMP Timeticks (as defined in RFC1155) variable. Implements SnmpVar.
//
//A timeticks value has the unit of hundredth of a second. This type can be used to create an SnmpVar object of type SnmpTimeticks. It also has methods to retrieve the value in different forms(eg. long string, short string, uint32).
type SnmpTimeTicks struct {
	varType byte
	value   uint32
}

//NewSnmpTimeTicks creates a new SnmpTimeticks by taking uint32 value as its argument
func NewSnmpTimeTicks(timeTicks uint32) SnmpTimeTicks {
	return SnmpTimeTicks{varType: consts.TimeTicks, value: timeTicks}
}

//Equals compares this SnmpTimeTicks to the specified SnmpTimeTicks type. Returns true only if both the timeticks values are same.
func (timeTicks SnmpTimeTicks) Equals(snmpTicks SnmpTimeTicks) bool {
	if timeTicks.value == snmpTicks.value {
		return true
	}
	return false
}

//Type returns the variable type in byte
func (timeTicks SnmpTimeTicks) Type() byte {
	return timeTicks.varType
}

//Value returns the variable value in uint32
func (timeTicks SnmpTimeTicks) Value() uint32 {
	return timeTicks.value
}

//String returns the string form of the SnmpTimeTicks type in printable string with # days, # hours, etc.
func (timeTicks SnmpTimeTicks) String() string {
	timeticks := timeTicks.value
	days, hours, minutes, seconds := convertUInt32ToTime(timeticks)

	var dayStr string
	if days == 1 {
		dayStr = "1 day"
	} else {
		dayStr = strconv.FormatInt(int64(days), 10) + " days"
	}

	hourStr := strconv.FormatInt(int64(hours), 10) + " hours"
	minStr := strconv.FormatInt(int64(minutes), 10) + " minutes"
	secStr := strconv.FormatInt(int64(seconds), 10) + " seconds"

	var strArr []string
	if days == 0 {
		strArr = []string{hourStr, minStr, secStr}
	} else {
		strArr = []string{dayStr, hourStr, minStr, secStr}
	}

	return strings.Join(strArr, ", ")
}

//TagString converts the value of this SnmpTimeTicks type to a printable string where the type is tagged before the value with a tag "TIMETICKS : ".
func (timeTicks SnmpTimeTicks) TagString() string {
	return "TIMETICKS : " + timeTicks.String()
}

//ToShortString returns the value of this SnmpTimeticks type to a printable string with Nd HH:MM:SS
func (timeTicks SnmpTimeTicks) ShortString() string {

	timeticks := timeTicks.value

	days, hours, minutes, seconds := convertUInt32ToTime(timeticks)

	var dayStr string
	if days == 1 {
		dayStr = "1d"
	} else {
		dayStr = strconv.FormatInt(int64(days), 10) + "d "
	}

	hourStr := format(hours)
	minStr := format(minutes)
	secStr := format(seconds)

	strArr := []string{hourStr, minStr, secStr}

	var returnStr string
	if days != 0 {
		returnStr = dayStr + strings.Join(strArr, ":")
	} else {
		returnStr = strings.Join(strArr, ":")
	}
	return returnStr

}

func convertUInt32ToTime(timeticks uint32) (days uint32, hours uint32, minutes uint32, seconds uint32) {
	timeticks /= 100

	days = (timeticks / (60 * 60 * 24))
	timeticks %= (60 * 60 * 24)

	hours = (timeticks / (60 * 60))
	timeticks %= (60 * 60)

	minutes = (timeticks / 60)
	seconds = (timeticks % 60)

	return
}

func format(i uint32) string {
	s := strconv.FormatInt(int64(i), 10)
	R := len(s)
	if R < 2 {
		s = "0" + s
	}
	return s
}

//EncodeVar encodes this timeticks variable as per ASN1 encoding standards.
func (timeTicks SnmpTimeTicks) EncodeVar() ([]byte, error) {
	tickBytes, err := asn1.Marshal(int64(timeTicks.value))
	tickBytes[0] = consts.TimeTicks
	return tickBytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpTimeTicks type
func (timeTicks SnmpTimeTicks) DecodeVar(tickBytes []byte) (*SnmpTimeTicks, error) {
	tickBytes[0] = consts.Integer
	SnmpTickDec1 := new(int64)
	_, err := asn1.Unmarshal(tickBytes, SnmpTickDec1)
	if err != nil {
		return nil, errors.New("Unable to decode Timeticks value : " + err.Error())
	}
	timeTicks = NewSnmpTimeTicks(uint32(*SnmpTickDec1))
	return &timeTicks, err
}
