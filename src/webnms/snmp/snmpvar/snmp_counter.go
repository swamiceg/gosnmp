/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"strconv"

	"webnms/snmp/consts"
)

//SnmpCounter implements SnmpVar. Type of SNMP Counter variable. Data is stored in uint32.
type SnmpCounter struct {
	varType byte
	value   uint32
}

//NewSnmpCounter creates a new SnmpCounter by taking uint32 as argument. The valid values ranges from 0 to 4294967295(both inclusive).
func NewSnmpCounter(value uint32) SnmpCounter {
	return SnmpCounter{varType: consts.Counter, value: value}
}

//Type returns the variable type in byte
func (counter SnmpCounter) Type() byte {
	return counter.varType
}

//Value returns the variable value in uint32
func (counter SnmpCounter) Value() uint32 {
	return counter.value
}

//Equals compares this SnmpCounter to the specified SnmpCounter.
//Returns true only if both the counter values are same.
func (counter SnmpCounter) Equals(counter2 SnmpCounter) bool {
	return counter.value == counter2.value
}

//String returns the string form of the SnmpCounter in printable string. Used in printing variables.
func (counter SnmpCounter) String() string {
	return strconv.FormatInt(int64(counter.value), 10)
}

//TagString converts the value of this SnmpCounter to a printable string where the type is tagged before the value with a tag "COUNTER : ".
func (counter SnmpCounter) TagString() string {
	return "COUNTER : " + counter.String()
}

//Diff gets the (positive) increment of this Counter variable over a uint32. Takes care of wraparound by adding the MAX counter value if negative.
func (counter SnmpCounter) Diff(lastVal uint32) uint32 {
	return counter.value - lastVal
}

//CounterDiff gets the (positive) increment of this Counter variable over another. Takes care of wraparound by adding the MAX counter value if negative.
func (counter SnmpCounter) CounterDiff(lastVal SnmpCounter) uint32 {
	return counter.value - lastVal.value
}

//EncodeVar encodes this counter variable as per ASN1 encoding standards.
func (counter SnmpCounter) EncodeVar() ([]byte, error) {
	counterBytes, err := asn1.Marshal(int32(counter.value))
	counterBytes[0] = consts.Counter
	return counterBytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpCounter type
func (counter SnmpCounter) decodeVar(counterBytes []byte) (*SnmpCounter, error) {
	counterBytes[0] = consts.Integer
	SnmpCountDec1 := new(int64)
	_, err := asn1.Unmarshal(counterBytes, SnmpCountDec1)
	if err != nil {
		return nil, errors.New("Unable to decode Counter value : " + err.Error())
	}
	counter = NewSnmpCounter(uint32(*SnmpCountDec1))
	return &counter, nil
}
