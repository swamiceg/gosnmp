/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"strconv"

	"webnms/snmp/consts"
)

//SnmpCounter64 implements SnmpVar. Type of 64 Bit SNMP Counter variable. Data is stored in uint64.
type SnmpCounter64 struct {
	varType byte
	value   uint64
}

//NewSnmpCounter64 creates a new SnmpCounter64 by taking uint64 as argument. The valid values ranges from 0 to 18446744073709551615(i.e., 2^64-1).
func NewSnmpCounter64(value uint64) SnmpCounter64 {
	return SnmpCounter64{varType: consts.Counter64, value: value}
}

//Equals compares this SnmpCounter64 to the specified SnmpCounter64 type.
//Returns true only if both the counter64 values are same.
func (counter64 SnmpCounter64) Equals(counter2 SnmpCounter64) bool {
	return counter64.value == counter2.value
}

//Type returns the variable type in byte
func (counter64 SnmpCounter64) Type() byte {
	return counter64.varType
}

//Value returns the variable value in uint64
func (counter64 SnmpCounter64) Value() uint64 {
	return counter64.value
}

//String returns string form of the SnmpCounter64 in printable string used in printing variables.
func (counter64 SnmpCounter64) String() string {
	return strconv.FormatInt(int64(counter64.value), 10)
}

//TagString converts the value of this SnmpCounter64 to a printable string where the type is tagged before the value with a tag "COUNTER64 : ".
func (counter64 SnmpCounter64) TagString() string {
	return "COUNTER64 : " + counter64.String()
}

//Diff returns the difference between two SnmpCounter64 numbers as uint64.
func (counter64 SnmpCounter64) Diff(lastVal uint64) uint64 {
	return counter64.value - lastVal
}

//Counter64Diff returns the difference between this SnmpCounter64 and SnmpCounter64 argument as a uint64.
func (counter64 SnmpCounter64) Counter64Diff(counter64Obj SnmpCounter64) uint64 {
	return counter64.value - counter64Obj.value
}

//EncodeVar encodes this counter64 variable as per ASN1 encoding standards.
func (counter64 SnmpCounter64) EncodeVar() ([]byte, error) {
	counter64Bytes, err := asn1.Marshal(int64(counter64.value))
	counter64Bytes[0] = consts.Counter64
	return counter64Bytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpCounter64 type
func (counter64 SnmpCounter64) decodeVar(counter64Bytes []byte) (*SnmpCounter64, error) {
	counter64Bytes[0] = consts.Integer
	SnmpCount64Dec1 := new(int64)
	_, err := asn1.Unmarshal(counter64Bytes, SnmpCount64Dec1)

	if err != nil {
		return nil, errors.New("Unable to decode Counter64 value : " + err.Error())
	}

	counter64 = NewSnmpCounter64(uint64(*SnmpCount64Dec1))
	return &counter64, err
}
