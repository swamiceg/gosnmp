/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"errors"
	"strconv"
	"strings"

	"webnms/snmp/consts"
)

//SnmpVar provides methods to present a uniform interface for applications working with the SNMP variables and any type that implements these methods will be treated as a Snmp Variable (eg. SnmpInt, SnmpString etc). All the type that implements these methods are concerned mainly with creating new SNMP variables, retrieve the value of the variable both as an object and in printable form.
type SnmpVar interface {
	Type() byte
	String() string
	TagString() string
	EncodeVar() ([]byte, error)
}

//CreateSnmpVar creates a new SnmpVar type with the specified variable type and value. The argument varType is a constant from the snmp package. Returns error when variable creation error occurs.
func CreateSnmpVar(varType byte, value string) (SnmpVar, error) {
	switch varType {
	case consts.OctetString:
		return NewSnmpString(value), nil
	case consts.BitString:
		return NewSnmpBitStringFromString(value), nil
	case consts.IpAddress:
		ipAdd := NewSnmpIp(value)
		if ipAdd == nil {
			return nil, errors.New("Invalid value to create SnmpIpaddress : " + value)
		}
		return ipAdd, nil
	case consts.Opaque:
		return NewSnmpOpaqueFromString(value), nil
	case consts.ObjectIdentifier:
		oid := NewSnmpOID(value)
		if oid == nil {
			return nil, errors.New("Invalid value to create SnmpOID : " + value)
		}
		return oid, nil
	case consts.Integer:
		if intValue, err := getDecimalValue(value); err != nil {
			return nil, errors.New("Invalid value to create SnmpInt : " + value)
		} else {
			return NewSnmpInt(int32(intValue)), err
		}
	case consts.TimeTicks:
		if intValue, err := getDecimalValue(value); err != nil {
			return nil, errors.New("Invalid value to create SnmpTimeTicks : " + value)
		} else {
			return NewSnmpTimeTicks(uint32(intValue)), err
		}
	case consts.Counter:
		if intValue, err := getDecimalValue(value); err != nil {
			return nil, errors.New("Invalid value to create SnmpCounter : " + value)
		} else {
			return NewSnmpCounter(uint32(intValue)), err
		}
	case consts.Counter64:
		if intValue, err := getDecimalValue(value); err != nil {
			return nil, errors.New("Invalid value to create SnmpCounter64 : " + value)
		} else {
			return NewSnmpCounter64(uint64(intValue)), err
		}
	case consts.Gauge:
		if intValue, err := getDecimalValue(value); err != nil {
			return nil, errors.New("Invalid value to create SnmpGauge : " + value)
		} else {
			return NewSnmpGauge(uint32(intValue)), err
		}
	}

	return nil, errors.New("Invalid varType to create a SnmpVar")
}

func getDecimalValue(strVal string) (int64, error) {

	//Trim the leading and trailling spaces
	trimVal := strings.TrimSpace(strVal)

	//Value is given in binary
	if strings.HasPrefix(trimVal, "'") && (strings.HasSuffix(trimVal, "'b") || strings.HasSuffix(trimVal, "'B")) {
		if strings.HasSuffix(trimVal, "'b") {
			trimVal = strings.Trim(trimVal, "'b")
		} else {
			trimVal = strings.Trim(trimVal, "'B")
		}
		return strconv.ParseInt(trimVal, 2, 32)
	}

	//Value is given in hexa-decimal
	if strings.HasPrefix(trimVal, "'") && (strings.HasSuffix(trimVal, "'h") || strings.HasSuffix(trimVal, "'H")) {
		if strings.HasSuffix(trimVal, "'h") {
			trimVal = strings.Trim(trimVal, "'h")
		} else {
			trimVal = strings.Trim(trimVal, "'H")
		}
		return strconv.ParseInt(trimVal, 16, 32)
	}

	//Not a binary or hexa-decimal
	return strconv.ParseInt(strVal, 10, 32)
}

//CompareVar compares two SnmpVar types. Returns true if both the SnmpVar are of same type and has same value. False otherwise.
func CompareVar(var1 SnmpVar, var2 SnmpVar) bool {
	var boolVal bool
	varType := var2.Type()

	if var1.Type() != var2.Type() {
		return false
	}

	switch varType {
	case consts.OctetString:
		variable1 := var1.(SnmpString)
		variable2 := var2.(SnmpString)
		boolVal = variable1.Equals(variable2)
	case consts.Bits:
		variable1 := var1.(SnmpBits)
		variable2 := var2.(SnmpBits)
		boolVal = variable1.Equals(variable2)
	case consts.BitString:
		variable1 := var1.(SnmpBitString)
		variable2 := var2.(SnmpBitString)
		boolVal = variable1.Equals(variable2)
	case consts.IpAddress:
		variable1 := var1.(SnmpIpAddress)
		variable2 := var2.(SnmpIpAddress)
		boolVal = variable1.Equals(variable2)
	case consts.Opaque:
		variable1 := var1.(SnmpOpaque)
		variable2 := var2.(SnmpOpaque)
		boolVal = variable1.Equals(variable2)
	case consts.ObjectIdentifier:
		variable1 := var1.(SnmpOID)
		variable2 := var2.(SnmpOID)
		boolVal = variable1.Equals(variable2)
	case consts.Integer:
		variable1 := var1.(SnmpInt)
		variable2 := var2.(SnmpInt)
		boolVal = variable1.Equals(variable2)
	case consts.TimeTicks:
		variable1 := var1.(SnmpTimeTicks)
		variable2 := var2.(SnmpTimeTicks)
		boolVal = variable1.Equals(variable2)
	case consts.Counter:
		variable1 := var1.(SnmpCounter)
		variable2 := var2.(SnmpCounter)
		boolVal = variable1.Equals(variable2)
	case consts.Counter64:
		variable1 := var1.(SnmpCounter64)
		variable2 := var2.(SnmpCounter64)
		boolVal = variable1.Equals(variable2)
	case consts.Gauge:
		variable1 := var1.(SnmpGauge)
		variable2 := var2.(SnmpGauge)
		boolVal = variable1.Equals(variable2)
	case consts.SnmpNullVar:
		variable1 := var1.(SnmpNull)
		variable2 := var2.(SnmpNull)
		boolVal = variable1.Equals(variable2)
	}

	return boolVal
}

//DecodeSnmpVar decodes the rawBytes into the specific SnmpVar. Returns nil incase of decode errors.
func DecodeSnmpVar(snmpVarBytes []byte) SnmpVar {

	var returnVar SnmpVar
	var err error

	if snmpVarBytes == nil || len(snmpVarBytes) == 0 {
		return nil
	}

	VarType := snmpVarBytes[0]
	switch VarType {
	case consts.Integer:
		returnVar, err = SnmpInt{}.decodeVar(snmpVarBytes)
	case consts.BitString:
		returnVar, err = SnmpBitString{}.decodeVar(snmpVarBytes)
	case consts.SnmpNullVar:
		returnVar, err = SnmpNull{}.decodeVar(snmpVarBytes)
	case consts.OctetString:
		returnVar, err = SnmpString{}.decodeVar(snmpVarBytes)
	case consts.ObjectIdentifier:
		returnVar, err = SnmpOID{}.decodeVar(snmpVarBytes)
	case consts.IpAddress:
		returnVar, err = SnmpIpAddress{}.DecodeVar(snmpVarBytes)
	case consts.Counter:
		returnVar, err = SnmpCounter{}.decodeVar(snmpVarBytes)
	case consts.Gauge:
		returnVar, err = SnmpGauge{}.decodeVar(snmpVarBytes)
	case consts.TimeTicks:
		returnVar, err = SnmpTimeTicks{}.DecodeVar(snmpVarBytes)
	case consts.Counter64:
		returnVar, err = SnmpCounter64{}.decodeVar(snmpVarBytes)
	case consts.Opaque:
		returnVar, err = SnmpOpaque{}.decodeVar(snmpVarBytes)
	case byte(consts.NoSuchObjectExp), byte(consts.NoSuchInstanceExp), byte(consts.EndOfMibViewExp):
		returnVar, err = SnmpNull{}.decodeVar(snmpVarBytes)
	default:
		return nil
	}

	if err != nil {
		return nil
	}
	return returnVar
}
