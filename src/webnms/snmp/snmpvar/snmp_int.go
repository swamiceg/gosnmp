/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"encoding/asn1"
	"errors"
	"strconv"

	"webnms/snmp/consts"
)

//SnmpInt is the type of SNMP Integer Variable. Implements SnmpVar. Can be used to create a SnmpVar of type SnmpInt. It also has methods to retrieve the value in different forms(eg. interger String, byte).
type SnmpInt struct {
	varType byte
	value   int32
}

//NewSnmpInt creates a new SnmpInt by taking int32 as argument.
func NewSnmpInt(value int32) SnmpInt {
	return SnmpInt{varType: consts.Integer, value: value}
}

//Type returns the variable type in byte
func (integer SnmpInt) Type() byte {
	return integer.varType
}

//Value returns the variable value in int32
func (integer SnmpInt) Value() int32 {
	return integer.value
}

//Equals compares this SnmpInt to the specified SnmpInt. Returns true only if both the integer values are same.
func (integer SnmpInt) Equals(snmpint SnmpInt) bool {
	return integer.value == snmpint.value
}

//String returns the string form of the SnmpInt in printable string. Used in printing variables.
func (integer SnmpInt) String() string {
	return strconv.FormatInt(int64(integer.value), 10)
}

//TagString converts the value of this SnmpInt type to a printable string where the type is tagged before the value with a tag "INTEGER : ".
func (integer SnmpInt) TagString() string {
	return "INTEGER : " + integer.String()
}

//EncodeVar encodes this integer variable as per ASN1 encoding standards.
func (integer SnmpInt) EncodeVar() ([]byte, error) {
	intBytes, err := asn1.Marshal(integer.value)
	return intBytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpInt type
func (integer SnmpInt) decodeVar(intBytes []byte) (*SnmpInt, error) {
	SnmpIntDec1 := new(int32)
	_, err := asn1.Unmarshal(intBytes, SnmpIntDec1)
	if err != nil {
		return nil, errors.New("Unable to decode Integer : " + err.Error())
	}
	integer = NewSnmpInt(*SnmpIntDec1)
	return &integer, err
}
