/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

func TestNewSnmpInt(t *testing.T) {

	values := []int32{1, -1}

	for _, value := range values {
		v := NewSnmpInt(value)
		if v.Type() != consts.Integer {
			t.Error("Expected", value, "got", v.String())
		}
	}
}

func TestIntString(t *testing.T) {

	v := NewSnmpInt(100)
	if !strings.EqualFold(v.String(), "100") {
		t.Error("Expected 100 got", v.String())
	}
}

func TestIntTagString(t *testing.T) {

	v := NewSnmpInt(100)
	if !strings.EqualFold(v.TagString(), "INTEGER : 100") {
		t.Error("Expected INTEGER: 100 got", v.TagString())
	}
}

func TestIntEquals(t *testing.T) {

	var v1, v2 SnmpInt

	v1 = NewSnmpInt(100)
	v2 = NewSnmpInt(100)

	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	v1 = NewSnmpInt(100)
	v2 = NewSnmpInt(-100)

	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}
}

func TestIntValue(t *testing.T) {
	v := NewSnmpInt(100)
	if v.Value() != 100 {
		t.Error("Expected 100 got", v.Value())
	}
}

func TestIntEncodeVar(t *testing.T) {
	v := NewSnmpInt(100)
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v.String())
	}

	if !bytes.EqualFold(byteVal, []byte{2, 1, 100}) {
		t.Error("Expected {2, 1, 100}, got", byteVal)
	}
}

func TestIntDecodeVar(t *testing.T) {
	v1 := NewSnmpInt(100)

	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}

	v2, _ := SnmpInt{}.decodeVar(byteVal)

	if !v1.Equals(*v2) {
		t.Error("Expected 100, got", v2.String())
	}
}
