/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"errors"

	"webnms/snmp/consts"
)

//SnmpNull is the type of SNMP Null variable. Implements SnmpVar. This type can be used to create a SnmpVar object of type SnmpNull. It also has methods to retrieve the value in different forms(eg. String, byte). The SnmpNull type is used as a placeholder while sending Get, GetNext and GetBulk requests. Used in GetResponse too if it contains any error.
type SnmpNull struct {
	varType  byte
	errorVal consts.ExceptionCode
}

//NewSnmpNull creates a new SnmpNull type.
func NewSnmpNull() SnmpNull {
	return SnmpNull{consts.SnmpNullVar, consts.ExceptionCode(0)}
}

//NewSnmpNullErr creates a new SnmpNull with Error indication. Null Objects in Snmpv2c can take up additionally the error indication for the particular oid. code can be any one of NoSuchObjectExp, NoSuchInstanceExp or EndOfMibViewExp.
func NewSnmpNullErr(code consts.ExceptionCode) SnmpNull {
	nullVar := new(SnmpNull)
	nullVar.varType = consts.SnmpNullVar

	if code == consts.NoSuchObjectExp || code == consts.NoSuchInstanceExp || code == consts.EndOfMibViewExp {
		nullVar.errorVal = code
	}
	return *nullVar
}

//Type returns the variable type in byte
func (nullVar SnmpNull) Type() byte {
	return nullVar.varType
}

//ErrorValue returns the exception indication contained in this variable.
func (nullVar SnmpNull) ErrorValue() consts.ExceptionCode {
	return nullVar.errorVal
}

//Equals compares this SnmpNull to the specified SnmpNull type. Returns true if both contains same exception indication. False otherwise.
func (nullVar SnmpNull) Equals(snmpNull SnmpNull) bool {
	if nullVar.errorVal == snmpNull.errorVal {
		return true
	}
	return false
}

//String returns string form of the SnmpNull type in printable string. Used in printing variables. Returns a string "NULL" if there is no exception indication in this variable. Else a string indicating the snmp v2c/v3 exception will be returned.
func (nullVar SnmpNull) String() string {

	var returnStr string = "NULL"
	if nullVar.errorVal == consts.NoSuchObjectExp || nullVar.errorVal == consts.NoSuchInstanceExp || nullVar.errorVal == consts.EndOfMibViewExp {
		returnStr = exceptionString(nullVar.errorVal)
	}
	return returnStr
}

//TagString converts the value of this SnmpNull type to a printable string where the type is tagged before the value with a tag "NULLOBJ : ". The printable string will be "NULL" if there is no exception indication in this variable. Else a string indicating the snmp v2/v3 exception will be appended after the tag.
func (nullVar SnmpNull) TagString() string {
	return "NULLOBJ : " + nullVar.String()
}

//EncodeVar encodes this null variable as per ASN1 encoding standards.
func (nullVar SnmpNull) EncodeVar() ([]byte, error) {

	if nullVar.errorVal == consts.NoSuchObjectExp || nullVar.errorVal == consts.NoSuchInstanceExp || nullVar.errorVal == consts.EndOfMibViewExp {
		return []byte{consts.SnmpNullVar, 0x00, byte(nullVar.errorVal), 0x00}, nil
	}
	return []byte{consts.SnmpNullVar, 0x00}, nil
}

//decodeVar decodes the ASN1 encoded byte array into SnmpNull type
func (nullVar SnmpNull) decodeVar(nullBytes []byte) (*SnmpNull, error) {

	var null SnmpNull

	if len(nullBytes) == 2 {
		if nullBytes[0] == consts.SnmpNullVar {
			null = NewSnmpNull()
		} else if nullBytes[0] == byte(consts.NoSuchObjectExp) || nullBytes[0] == byte(consts.NoSuchInstanceExp) || nullBytes[0] == byte(consts.EndOfMibViewExp) {
			null = NewSnmpNullErr(consts.ExceptionCode(nullBytes[0]))
		} else {
			return nil, errors.New("Unable to decode Null variable : Invalid byte array received")
		}
	} else if len(nullBytes) == 4 {
		if nullBytes[2] != byte(consts.NoSuchObjectExp) && nullBytes[2] != byte(consts.NoSuchInstanceExp) && nullBytes[2] != byte(consts.EndOfMibViewExp) {
			return nil, errors.New("Unable to decode Null variable : Invalid byte array received")
		}
		null = NewSnmpNullErr(consts.ExceptionCode(nullBytes[2]))
	} else {
		return nil, errors.New("Unable to decode Null variable : Invalid byte array received")
	}

	return &null, nil
}

//exceptionString returns the string representing the variable's v2c/v3 exception.
func exceptionString(code consts.ExceptionCode) string {

	var returnStr string

	switch code {

	case consts.NoSuchObjectExp:
		returnStr = "There is no such object in this MIB."
	case consts.NoSuchInstanceExp:
		returnStr = "There is no such instance in this MIB."
	case consts.EndOfMibViewExp:
		returnStr = "This is a end of Mib View."
	default:
		returnStr = "UnKnown exception code"
	}

	return returnStr
}
