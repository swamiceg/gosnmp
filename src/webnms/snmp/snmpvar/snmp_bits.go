/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"encoding/asn1"
	"errors"
	"strconv"
	"strings"

	"webnms/snmp/consts"
)

//SnmpBits is a type of SNMP Bits Variable. Implements SnmpVar. It also has methods to retrive the value in various forms(eg. String, byte).
type SnmpBits struct {
	varType byte
	value   []byte
}

//NewSnmpBits constructs a new SnmpBits by taking a string value and the radix. The radix can be either 2 or 16 which represents binary and hex respectively. Padding of zero's is done at the end of the string value, when the length of the given string is not a multiple of 8 for binary and 2 for hex. Returns error if the radix is other than 2 or 16 and incase of invalid values for the radix.
func NewSnmpBits(strVal string, radix int) (*SnmpBits, error) {
	arr := []byte{}

	switch radix {
	case 2:
		//Append 0's to complete the octet
		if remVal := len(strVal) % 8; remVal != 0 {
			for i := 0; i < 8-remVal; i++ {
				strVal = strVal + "0"
			}
		}

		if arr = stringToByteArray(strVal); arr == nil {
			return nil, errors.New("Invalid value to create SnmpBits")
		}
	case 16:
		//Append 0's to complete the octet
		if remVal := len(strVal) % 2; remVal != 0 {
			for i := 0; i < 2-remVal; i++ {
				strVal = strVal + "0"
			}
		}

		if arr = hexStringToByteArray(strVal); arr == nil {
			return nil, errors.New("Invalid value to create SnmpBits")
		}
	default:
		return nil, errors.New("Invalid radix value to create SnmpBits")
	}

	return &SnmpBits{varType: consts.Bits, value: arr}, nil
}

func stringToByteArray(strVal string) []byte {
	arr := []byte{}

	//Convert the binary string to byte array
	for i := 0; i < len(strVal)/8; i++ {
		byteStr := strVal[i*8 : (i*8)+8]
		if value, err := strconv.ParseInt(byteStr, 2, 64); err != nil {
			return nil
		} else {
			arr = append(arr, byte(value))
		}
	}

	return arr
}

func hexStringToByteArray(strVal string) []byte {
	arr := []byte{}

	//Convert the binary string to byte array
	for i := 0; i < len(strVal)/2; i++ {
		byteStr := strVal[i*2 : (i*2)+2]
		if value, err := strconv.ParseInt(byteStr, 16, 64); err != nil {
			return nil
		} else {
			arr = append(arr, byte(value))
		}
	}

	return arr
}

//NewSnmpBitsFromBytes creates SnmpBits by taking byte array as argument.
func NewSnmpBitsFromBytes(byteVal []byte) SnmpBits {
	return SnmpBits{varType: consts.Bits, value: byteVal}
}

//Equals returns true if both the SnmpBits types are equal.
func (bits SnmpBits) Equals(bits1 SnmpBits) bool {
	arr1 := bits.value
	arr2 := bits1.value

	return bytes.Equal(arr1, arr2)
}

//Type returns the variable type in byte
func (bits SnmpBits) Type() byte {
	return bits.varType
}

//Value returns the variable value in byte array.
func (bits SnmpBits) Value() []byte {
	return bits.value
}

//String returns string form of the SnmpBits type in printable string used in printing variables.
func (bits SnmpBits) String() string {
	return string(bits.value)
}

//TagString converts the value of this SnmpBits type to a printable string where the type is tagged before the value with a tag "BITS : ".
func (bits SnmpBits) TagString() string {
	return "BITS : " + bits.String()
}

//ByteString converts the value of this SnmpBits instance to a printable HEX string. For e.g if an array of byte of length 2 has the following values: b[0] = 0x81 and b[1] = 0xFF, and this is represented as an SnmpBits type, then this method prints the HEX value as "81 FF".
func (bits SnmpBits) ByteString() string {
	var byteString string
	for _, val := range bits.value {
		byteString = byteString + " " + strconv.FormatInt(int64(val), 16)
	}
	return strings.TrimSpace(byteString)
}

//EncodeVar encodes this variable as per ASN1 encoding standards.
func (bits SnmpBits) EncodeVar() ([]byte, error) {
	stringbytes, err := asn1.Marshal(bits.value)
	return stringbytes, err
}
