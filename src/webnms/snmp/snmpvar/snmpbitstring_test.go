/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"strings"
	"testing"

	"webnms/snmp/consts"
)

//test for NewSnmpBitStringFromString by passing valid value something
func TestNewSnmpBitStringFromString(t *testing.T) {
	v := NewSnmpBitStringFromString("something")
	if v.Type() != consts.BitString {
		t.Error("Expected", "3", "got", v.Type())
	}
}

//test for NewSnmpBitStringFromBytes by passing valid byte slice
func TestNewSnmpBitStringFromBytes(t *testing.T) {
	v := NewSnmpBitStringFromBytes([]byte("something"))
	if v.Type() != consts.BitString {
		t.Error("Expected", "3", "got", v.Type())
	}
}

func TestNewSnmpBitString(t *testing.T) {
	v := NewSnmpBitString([]byte("something"), 71)
	if v == nil {
		t.Error("Expected something got", v)
	}

	v = NewSnmpBitString([]byte("something"), 73)
	if v != nil {
		t.Error("Expected nil got", v)
	}
}

//test for Type. Expected result is 3.
func TestBitStringType(t *testing.T) {
	v := NewSnmpBitStringFromString("something")
	if v.Type() != consts.BitString {
		t.Error("Expected", "3", "got", v.Type())
	}
}

//test for Value. Expected result is [115 111 109 101 116 104 105 110 103].
func TestBitStringValue(t *testing.T) {

	//Expected result is [115 111 109 101 116 104 105 110 103]
	v := NewSnmpBitStringFromString("something")
	if !bytes.EqualFold(v.Value(), []byte{115, 111, 109, 101, 116, 104, 105, 110, 103}) {
		t.Error("Expected", "[115 111 109 101 116 104 105 110 103]", "got", v.Value())
	}

	//Expected result is [0 115 111 109 101 116 104 105 110]
	v1 := NewSnmpBitString([]byte("something"), 64)
	if !bytes.EqualFold(v1.Value(), []byte{0, 115, 111, 109, 101, 116, 104, 105, 110}) {
		t.Error("Expected", "[0 115 111 109 101 116 104 105 110]", "got", v1.Value())
	}
}

//test for Equals by creating two bitstring types of same values.
func TestBitStringEquals(t *testing.T) {

	//Below two tests are for bitstrings created using NewSnmpBitStringFromString method
	//Check Equal method with same bitstring values
	v1 := NewSnmpBitStringFromString("something")
	v2 := NewSnmpBitStringFromString("something")
	if !v1.Equals(v2) {
		t.Error("Expected", v1.String(), "got", v2.String())
	}

	//Check Equals method with two different bitstring values
	v2 = NewSnmpBitStringFromString("someotherthing")
	if v1.Equals(v2) {
		t.Error("Expected other than", v1.String(), "but got", v2.String())
	}

	//Below two tests are for bitstrings created using NewSnmpBitString method
	//test method with same two values
	v3 := NewSnmpBitString([]byte("something"), 64)
	v4 := NewSnmpBitString([]byte("something"), 64)
	if !v3.Equals(*v4) {
		t.Error("Expected", v3.String(), "got", v4.String())
	}

	//test method with different values
	v4 = NewSnmpBitString([]byte("something"), 52)
	if v3.Equals(*v4) {
		t.Error("Expected", v3.String(), "got", v4.String())
	}
}

//test for String.
func TestBitStringString(t *testing.T) {
	//test is for bitstrings created using NewSnmpBitStringFrom method
	v := NewSnmpBitStringFromString("something")
	if !strings.EqualFold(v.String(), "something") {
		t.Error("Expected", "something", "got", v.String())
	}

	//test is for bitstrings created using NewSnmpBitString method
	v = *NewSnmpBitString([]byte("something"), 63)
	if !strings.EqualFold(v.String(), string([]byte{1, 115, 111, 109, 101, 116, 104, 105, 110})) {
		t.Error("Expected", "somethin", "got", v.String())
	}
}

//test for TagString. expected result is BITSTRING: something
func TestBitStringTagString(t *testing.T) {
	v := NewSnmpBitStringFromString("something")
	if !strings.EqualFold(v.TagString(), "BITSTRING: something") {
		t.Error("Expected BITSTRING: something got", v.TagString())
	}
}

//test for EncodeVar. Expected result is [3 9 115 111 109 101 116 104 105 110 103]
func TestBitStringEncodeVar(t *testing.T) {

	//test is for bitstrings created using NewSnmpBitStringFromString method
	v := NewSnmpBitStringFromString("something")
	byteVal, err := v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding bitstring value", v.String())
	}
	if !bytes.EqualFold(byteVal, []byte{3, 9, 115, 111, 109, 101, 116, 104, 105, 110, 103}) {
		t.Error("Expected [3 9 115 111 109 101 116 104 105 110 103], got", byteVal)
	}

	//test is for bitstrings created using NewSnmpBitString method
	v = *NewSnmpBitString([]byte("something"), 64)
	byteVal, err = v.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding bitstring value", v.String())
	}
	if !bytes.EqualFold(byteVal, []byte{3, 9, 0, 115, 111, 109, 101, 116, 104, 105, 110}) {
		t.Error("Expected [3 9 0 115 111 109 101 116 104 105 110], got", byteVal)
	}
}

//test for decodevar. Encode the var, decode the byte array and check for Equality.
func TestBitStringDecodeVar(t *testing.T) {

	//test is for bitstrings created using NewSnmpBitStringFromString method
	v1 := NewSnmpBitStringFromString("something")
	byteVal, err := v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}
	v2, _ := SnmpBitString{}.decodeVar(byteVal)
	if !v1.Equals(*v2) {
		t.Error("Expected something, got", v2.String())
	}

	//test is for bitstrings created using NewSnmpBitString method
	v1 = *NewSnmpBitString([]byte("something"), 64)
	byteVal, err = v1.EncodeVar()
	if err != nil {
		t.Error("Test failed. Got error while encoding value", v1.String())
	}
	v2, _ = SnmpBitString{}.decodeVar(byteVal)
	if !v1.Equals(*v2) {
		t.Error("Expected somethin, got", v2.String())
	}
}
