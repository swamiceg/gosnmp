/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmpvar

import (
	"bytes"
	"encoding/asn1"
	"errors"

	"webnms/snmp/consts"
)

//SnmpOpaque implements SnmpVar. Can be used to create an SnmpVar of type OPAQUE.
//
//An SnmpOpaque is used sometimes to wrap variables of other types. NewSnmpOpaqueFromVar method of this package allows such a usage. Decoded value of SnmpVar on such a usage of SnmpOpaque can be obtained using the DecodeVariable method.
type SnmpOpaque struct {
	varType byte
	value   []byte
}

//NewSnmpOpaque returns SnmpOpaque by taking a byte array as argument.
func NewSnmpOpaque(byteVal []byte) SnmpOpaque {
	return SnmpOpaque{varType: consts.Opaque, value: byteVal}
}

//NewSnmpOpaqueFromString returns SnmpOpaque by taking a string as argument.
func NewSnmpOpaqueFromString(str string) SnmpOpaque {
	return SnmpOpaque{varType: consts.Opaque, value: []byte(str)}
}

//NewSnmpOpaqueFromVar returns SnmpOpaque by taking a SnmpVar as argument. Allows to specify var type to be wrapped in as an SnmpOpaque type. Wraps SnmpNull if nil is passed as argument.
func NewSnmpOpaqueFromVar(variable SnmpVar) *SnmpOpaque {

	if variable == nil {
		variable = NewSnmpNull()
	}

	byteVal, err := variable.EncodeVar()
	if err != nil {
		return nil
	}
	return &SnmpOpaque{varType: consts.Opaque, value: byteVal}
}

//Value returns the byte array wrapped with this opaque data.
func (opaque SnmpOpaque) Value() []byte {
	return opaque.value
}

//Equals compares this SnmpOpaque to the specified SnmpOpaque type. Returns true only if both the bytevalue are equal.
func (opaque SnmpOpaque) Equals(opaque2 SnmpOpaque) bool {
	return bytes.Equal(opaque.value, opaque2.value)
}

//Type returns the variable type in byte
func (opaque SnmpOpaque) Type() byte {
	return opaque.varType
}

//String returns the string form of the SnmpOpaque type in printable string. Used in printing variables.
func (opaque SnmpOpaque) String() string {
	return string(opaque.value)
}

//TagString converts the value of this SnmpBitString type to a printable string where the type is tagged before the value with a tag "OPAQUE : ".
func (opaque SnmpOpaque) TagString() string {
	return "OPAQUE : " + opaque.String()
}

//DecodeVariable returns the SnmpVar object if any variable is wrapped inside the SnmpOpaque object. This routine should be used only if it is certain that the contents represent a SnmpVar object. It does not guarantee the accurate results in case the wrapped object does not represent an SnmpVar.
func (opaque SnmpOpaque) DecodeVariable() SnmpVar {
	return DecodeSnmpVar(opaque.value)
}

//EncodeVar encodes this opaque variable as per ASN1 encoding standards.
func (opaque SnmpOpaque) EncodeVar() ([]byte, error) {
	opaquebytes, err := asn1.Marshal(opaque.value)
	opaquebytes[0] = consts.Opaque
	return opaquebytes, err
}

//decodeVar decodes the ASN1 encoded byte array into SnmpOpaque type
func (opaque SnmpOpaque) decodeVar(opaqueBytes []byte) (*SnmpOpaque, error) {
	opaqueBytes[0] = consts.OctetString
	SnmpOpaqueDec := new([]byte)
	_, err := asn1.Unmarshal(opaqueBytes, SnmpOpaqueDec)
	if err != nil {
		return nil, errors.New("Unable to decode the endoced opaque value + " + err.Error())
	}
	opaque = NewSnmpOpaque(*SnmpOpaqueDec)
	return &opaque, err
}
