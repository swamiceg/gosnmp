/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	//"fmt"
	"testing"
	"time"

	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
)

var localHost string = "localhost"
var localPort int = 7500

func TestLocalPortBinding(t *testing.T) {
	var err error

	udpPO := snmp.NewUDPProtocolOptions()
	udpPO.SetLocalHost(localHost)
	udpPO.SetLocalPort(localPort)

	tcpPO := snmp.NewTCPProtocolOptions()
	tcpPO.SetLocalHost(localHost)
	tcpPO.SetLocalPort(localPort)

	//Start with UDP ProtocolOptions
	tr, err := NewTrapReceiver(udpPO)
	if err != nil {
		t.Fatal("Failure in starting the TrapReceiver with UDP ProtocolOptions: " + err.Error())
	} else {
		if tr.Port() != localPort {
			t.Errorf("TrapReceiver started at unexpected port. Expected: %d. Got: %d.", localPort, tr.Port())
		}
		if err = tr.Stop(); err != nil {
			t.Error(err)
		}
	}

	//Start with TCP ProtocolOptions
	tr, err = NewTrapReceiver(tcpPO)
	if err != nil {
		t.Error("Failure in starting the TrapReceiver with TCP ProtocolOptions: " + err.Error())
	} else {
		if tr.Port() != localPort {
			t.Errorf("TrapReceiver started at unexpected port. Expected: %d. Got: %d.", localPort, tr.Port())
		}
		if err = tr.Stop(); err != nil {
			t.Error(err)
		}
	}

	//Start with port number
	tr, err = NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Errorf("Failure in starting the TrapReceiver with localPort: %d. Error: %s", localPort, err)
	} else {
		if tr.Port() != localPort {
			t.Errorf("TrapReceiver started at unexpected port. Expected: %d. Got: %d.", localPort, tr.Port())
		}
		if err = tr.Stop(); err != nil {
			t.Error(err)
		}
	}

	//Start with nil ProtocolOptions
	tr, err = NewTrapReceiver(nil)
	if err != nil {
		t.Error("Failure in starting TrapReceiver with nil ProtocolOptions. Error: " + err.Error())
	} else {
		if tr.Port() <= 0 {
			t.Error("TrapReceiver: Port number not updated in TrapReceiver instance.")
		}
		tr.Stop()
	}

	//Start with zero port number
	tr, err = NewTrapReceiverByPort(0)
	if err != nil {
		t.Error("Failure in starting TrapReceiver with port 0. Error: " + err.Error())
	} else {
		if !tr.IsAlive() {
			t.Errorf("TrapReceiver: IsAlive returns false when trap receiver is open.")
		}
		if tr.Port() <= 0 {
			t.Error("TrapReceiver: Port number not updated in TrapReceiver instance.")
		}
		tr.Stop()
		if tr.IsAlive() {
			t.Errorf("TrapReceiver: IsAlive returns true when trap receiver is closed.")
		}
	}
}

var portSlice = []int{7200, 7201, 7203}

func TestChangeListenerPort(t *testing.T) {
	var err error

	tr, err := NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Fatal(err)
	} else {
		//Change the listener port
		for _, port := range portSlice {
			if err = tr.ChangeListenerPort(port); err != nil {
				t.Error(err)
				continue
			}
			if tr.Port() != port {
				t.Errorf("TrapReceiver: Failure is changing listener port. Expected: %d. Got: %d.", port, tr.Port())
			}
		}
		tr.Stop()
	}
}

func TestTrapListener(t *testing.T) {
	var err error

	tr, err := NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Fatal(err)
	}

	//Add/Remove trap listeners
	listener := new(listenerImpl)
	tr.AddTrapListener(listener)
	if len(tr.listeners) != 1 {
		t.Errorf("TrapReceiver: Invalid listeners count. Expected: 1. Got: %d.", len(tr.listeners))
	}

	tr.RemoveListener(listener)
	if len(tr.listeners) != 0 {
		t.Errorf("TrapReceiver: Invalid listeners count. Expected: 0. Got: %d.", len(tr.listeners))
	}

	tr.AddTrapListener(listener)
	tr.AddTrapListener(listener)
	tr.RemoveAllListeners()
	if len(tr.listeners) != 0 {
		t.Errorf("TrapReceiver: Invalid listeners count. Expected: 0. Got: %d.", len(tr.listeners))
	}

	tr.Stop()
}

var trapCount int = 0

func TestListenerCallbackV1V2(t *testing.T) {
	//V1/V2C listener callback test - With authentication enabled
	tr, err := NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Fatal(err)
	}

	listener := new(listenerImpl)
	tr.AddTrapListener(listener)

	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(localHost)
	udp.SetRemotePort(localPort)
	ses.SetProtocolOptions(udp)

	if err = ses.Open(); err != nil {
		t.Fatal(err)
	}
	trapMsg := msg.NewSnmpMessage()
	trapMsg.SetVersion(consts.Version2C)
	trapMsg.SetCommand(consts.Trap2Request)
	trapMsg.SetCommunity("public")

	//Valid Trap message test
	count := 0
	trapCount = 0
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++

		}
	}
	time.Sleep(time.Millisecond * 500) //Wait for sometime
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", count, trapCount)
	}

	//Invalid Trap messages test
	trapMsg.SetCommunity("Wrong community")
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount >= count {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", trapCount, count)
	}

	trapCount = 0 //Reinitialize
	community := []string{"public", "public", "private", "public", "aa", "as"}
	validCommunity := 0
	for _, com := range community {
		trapMsg.SetCommunity(com)
		if _, err = ses.Send(trapMsg); err == nil {
			if com == "public" {
				validCommunity++
			}
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != validCommunity {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", validCommunity, trapCount)
	}

	//V1/V2C listener callback test - With authentication disabled
	//Disable all the authentication
	tr.AuthenticateTraps(false)
	trapCount = 0 //Reinitialize
	count = 0
	for _, com := range community { //We should receive all the traps
		trapMsg.SetCommunity(com)
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", count, trapCount)
	}

	//Enable only community authentication - We should receive only community authenticated traps.
	tr.AuthenticateTraps(false)
	tr.SetCommunityAuthEnable(true)
	trapCount = 0 //Reinitialize
	validCommunity = 0
	for _, com := range community {
		trapMsg.SetCommunity(com)
		if _, err = ses.Send(trapMsg); err == nil {
			if com == "public" {
				validCommunity++
			}
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != validCommunity {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", validCommunity, trapCount)
	}

	//Disable only community authentication - We should receive all the traps.
	tr.AuthenticateTraps(true)
	tr.SetCommunityAuthEnable(false)
	trapCount = 0 //Reinitialize
	count = 0
	for _, com := range community { //We should receive all the traps
		trapMsg.SetCommunity(com)
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", count, trapCount)
	}
	tr.Stop()
}

func TestListenerCallbackV3(t *testing.T) {
	//V3 listener callback test - With authentication enabled
	tr, err := NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Fatal(err)
	}

	//Configure v3 params
	engID := []byte{1, 2, 3, 4}
	userName := "authUser"
	ctxName := "auth"
	authProtocol := consts.MD5_AUTH
	authPwd := "MD5"
	privProtocol := consts.NO_PRIV
	privPwd := ""
	tr.SetPrincipal(userName)
	tr.SetContextName(ctxName)
	tr.SetAuthProtocol(authProtocol)
	tr.SetAuthPassword(authPwd)
	tr.SetPrivProtocol(privProtocol)
	tr.SetPrivPassword(privPwd)
	tr.AddTrapUser(engID)

	listener := new(listenerImpl)
	tr.AddTrapListener(listener)

	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(localHost)
	udp.SetRemotePort(localPort)
	ses.SetProtocolOptions(udp)
	api.SnmpEngine().SetSnmpEngineID(engID)

	userLCD := api.USMUserLCD()
	secUser, err := userLCD.AddUser(engID,
		userName,
		authProtocol,
		authPwd,
		privProtocol,
		privPwd,
	)
	if err != nil {
		t.Fatal(err)
	}

	if err = ses.Open(); err != nil {
		t.Fatal(err)
	}
	trapMsg := msg.NewSnmpMessage()
	trapMsg.SetVersion(consts.Version3)
	trapMsg.SetCommand(consts.Trap2Request)
	trapMsg.SetCommunity("public")
	trapMsg.SetUserName(userName)
	trapMsg.SetContextName(ctxName)
	trapMsg.SetSecurityLevel(secUser.SecurityLevel())

	//Valid Trap message test
	trapCount = 0
	count := 0
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++

		}
	}
	time.Sleep(time.Millisecond * 500) //Wait for sometime
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving v3 trap messages. Expected traps: %d. Received: %d.", count, trapCount)
	}

	//Invalid Trap messages test - Invalid Authentication Protocol/Password
	secUser, err = userLCD.AddUser(engID,
		userName,
		authProtocol,
		"invalidpwd", //Modified auth password
		privProtocol,
		privPwd,
	)
	if err != nil {
		t.Fatal(err)
	}
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount >= count {
		t.Errorf("TrapReceiver: Failure in receiving v3 trap messages. Expected traps: %d. Received: %d.", trapCount, count)
	}

	//V3 listener callback test - With authentication disabled
	//Disable all the authentication
	tr.AuthenticateTraps(false)
	trapCount = 0 //Reinitialize
	count = 0
	for i := 0; i < 10; i++ { //We should receive all the traps
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving v3 trap messages. Expected traps: %d. Received: %d.", count, trapCount)
	}

	//Enable only v3 authentication - We should receive only authenticated v3 traps.
	tr.AuthenticateTraps(false)
	tr.SetV3AuthEnable(true)
	trapCount = 0 //Reinitialize
	count = 0
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if count > 0 && trapCount != 0 {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: 0. Received: %d.", count, trapCount)
	}

	//Disable only v3 authentication - We should receive all the traps.
	tr.AuthenticateTraps(true)
	tr.SetV3AuthEnable(false)
	trapCount = 0 //Reinitialize
	count = 0
	secUser, err = userLCD.AddUser(engID, //Add an user which doesnot exist in TrapReceiver
		"newUser",
		authProtocol,
		"invalidpwd", //Modified auth password
		privProtocol,
		privPwd,
	)
	if err != nil {
		t.Fatal(err)
	}
	for i := 0; i < 10; i++ { //We should receive all the traps
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving trap message. Expected traps: %d. Received: %d.", count, trapCount)
	}

	tr.Stop()
}

func TestStop(t *testing.T) {
	tr, err := NewTrapReceiverByPort(localPort)
	if err != nil {
		t.Fatal(err)
	}

	if err = tr.Stop(); err != nil {
		t.Error(err)
	}
	if tr.IsAlive() {
		t.Error("TrapReceiver: Failure in stopping the receiver")
	}

	api := snmp.NewSnmpAPI()
	ses := snmp.NewSnmpSession(api)
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost(localHost)
	udp.SetRemotePort(localPort)
	ses.SetProtocolOptions(udp)

	if err = ses.Open(); err != nil {
		t.Fatal(err)
	}
	trapMsg := msg.NewSnmpMessage()
	trapMsg.SetVersion(consts.Version2C)
	trapMsg.SetCommand(consts.Trap2Request)
	trapMsg.SetCommunity("public")

	tr.AddTrapListener(new(listenerImpl))

	trapCount = 0
	count := 0
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if count > 0 && trapCount > 0 {
		t.Error("TrapReceiver: Receiving trap messages after calling Stop method.")
	}

	//Change the listener port and test
	err = tr.ChangeListenerPort(localPort)
	if err != nil {
		t.Error(err)
	}
	if !tr.IsAlive() {
		t.Error("TrapReceiver: Failure in restarting the receiver")
	}

	trapCount = 0
	count = 0
	for i := 0; i < 10; i++ {
		if _, err = ses.Send(trapMsg); err == nil {
			count++
		}
	}
	time.Sleep(time.Millisecond * 500)
	if count > 0 && trapCount != count {
		t.Errorf("TrapReceiver: Failure in receiving trap messages after changing listener port. Expected traps: %d. Received: %d.", count, trapCount)
	}

}

type listenerImpl int

func (l *listenerImpl) ReceiveTrap(trap msg.SnmpMessage) {
	trapCount++
}
