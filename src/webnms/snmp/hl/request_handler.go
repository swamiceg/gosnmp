/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"errors"
	"strings"

	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

type requestHandler struct{}

func (h requestHandler) GetSnmpResponse(session *snmp.SnmpSession, req *msg.SnmpMessage, attemptPartial bool, attemptComplete bool, varbsPerRequest int) (*msg.SnmpMessage, error) {
	var resp *msg.SnmpMessage
	var err error
	cmd := req.Command()
	//Command which expects response from the server
	if cmd == consts.GetRequest || cmd == consts.GetNextRequest ||
		cmd == consts.SetRequest || cmd == consts.GetBulkRequest ||
		cmd == consts.InformRequest {

		if attemptComplete && cmd != consts.SetRequest {
			if resp, err = h.getCompleteData(session, req, varbsPerRequest); err != nil {
				return nil, err
			}
		} else {
			if resp, err = session.SyncSend(*req); err != nil {
				return nil, err
			}
		}
	} else {
		session.Send(*req)
		return nil, nil
	}

	if resp == nil {
		return nil, err
	}

	if len(resp.VarBinds()) <= 0 && resp.ErrorStatus() == consts.TooBig && strings.Contains(resp.ErrorString(), "too large") {
		return resp, nil
	}

	if len(resp.VarBinds()) <= 0 {
		return nil, errors.New("Empty Varbinds Returned")
	}

	if resp.ErrorStatus() != 0 {
		if attemptPartial && resp.ErrorIndex() != int32(consts.NoError) && len(resp.VarBinds()) > 1 {
			return h.getPartialMsgData(req, resp, session)
		}
	}

	return resp, nil
}

//getCompleteData returns the complete results by splitting the PDU based on the varbind count set using setVarBindCount() method.
//If non zero value is set, the request is sent with this number of varbinds otherwise the varbinds are split and sent till it succeeds.
func (h requestHandler) getCompleteData(session *snmp.SnmpSession, req *msg.SnmpMessage, varbsPerRequest int) (*msg.SnmpMessage, error) {

	var varLength int = len(req.VarBinds())
	var increment int = 0
	var start int = 0
	var end int = 0
	var nonReps int = int(req.NonRepeaters())
	var VARB_COUNT int = 5

	var sMsg *msg.SnmpMessage
	var respMsg *msg.SnmpMessage
	var err error

	var isSilentDrops bool
	var silentDropsCount uint32
	var isTooBig bool = false

	if varbsPerRequest != 0 {
		increment = varbsPerRequest
	} else {
		increment = varLength

		if sMsg, err = h.sendSilentDrops(session, req); err != nil {
			//Return nil pdu with respective error incase of sending error
			return nil, err
		}

		if sMsg == nil {
			//Return if request timed out
			return nil, errors.New("Response timed-out for Silent drops request")
		} else {

			//Count the SilentDrops if it is implemented in the agent
			version := sMsg.Version()

			if version == consts.Version1 {
				if sMsg.ErrorStatus() == consts.NoError {
					isSilentDrops = true
					silentDropsVar := sMsg.VariableAt(0).(*snmpvar.SnmpCounter)
					silentDropsCount = silentDropsVar.Value()
				}
			} else if (version == consts.Version2C || version == consts.Version3) && sMsg.ErrorStatus() == consts.NoError {
				if sMsg.VarBindAt(0).ExceptionIndex() == 0 {
					if strings.HasPrefix(sMsg.ObjectIDAt(0).String(), ".1.3.6.1.6.3.15.1.1.") {
						return nil, errors.New(getV3ErrorString(sMsg.ObjectIDAt(0).String()))
					}

					isSilentDrops = true
					silentDropsVar := sMsg.VariableAt(0).(*snmpvar.SnmpCounter)
					silentDropsCount = silentDropsVar.Value()
				}
			}

			if sMsg.ErrorStatus() == consts.GeneralError {
				return sMsg, nil
			}
		}
	}

	var tVarbs []msg.SnmpVarBind
	var nonRepIncrement int = 0

	for {
		//Temperary SnmpMessage for sending splitted varbinds request
		tMsg := req.CopyWithoutVarBinds()

		//Split the varbinds into half if the response is tooBig.
		if isTooBig {
			isTooBig = false
			increment = increment / 2
		}

		end = start + increment
		if end > varLength {
			end = varLength
		}

		//Set the spilled varbinds in temperary SnmpMessage
		if req.VarBinds() != nil {
			tMsg.SetVarBinds(req.VarBinds()[start:end])
		}

		if nonReps > (end - start) {
			tMsg.SetNonRepeaters(int32(end - start))
		} else {
			tMsg.SetNonRepeaters(int32(nonReps))
		}

		//Send the splitted temperary SnmpMessage
		if respMsg, err = h.syncSend(tMsg, session); err != nil {
			return nil, err
		}

		if respMsg == nil { //This may be because of the silentDrops
			if isSilentDrops {
				if sMsg, err = h.sendSilentDrops(session, req); err != nil {
					return nil, err
				}

				if sMsg != nil {
					if varbsPerRequest == 0 && increment > VARB_COUNT {
						varbsPerRequest = VARB_COUNT
						increment = VARB_COUNT
					}

					version := sMsg.Version()

					//If silent drops count increases try again else return nil because the request may be timed-out
					if version == consts.Version1 {
						if sMsg.ErrorStatus() == consts.NoError && silentDropsCount < sMsg.VariableAt(0).(*snmpvar.SnmpCounter).Value() {
							continue
						} else {
							return nil, errors.New("Request dropped silently")
						}
					} else if (version == consts.Version2C || version == consts.Version3) && sMsg.ErrorStatus() == consts.NoError {
						if sMsg.VarBindAt(0).ExceptionIndex() == 0 && silentDropsCount < sMsg.VariableAt(0).(*snmpvar.SnmpCounter).Value() {
							continue
						} else {
							return nil, errors.New("Request dropped silently")
						}
					} else {
						return nil, errors.New("Error occured while getting silentDrops value")
					}
				} else {
					return nil, errors.New("Request timed-out for silentdrops request")
				}
			} else {
				return nil, errors.New("Request timed-out for spiltted request")
			}
		}

		if respMsg.ErrorStatus() == consts.TooBig || strings.Contains(respMsg.ErrorString(), "too large") {
			//It means that a single varBind request is sent. Hence complete data cannot be retrieved because the varBinds can't be split further.
			if len(tMsg.VarBinds()) == 1 {
				return respMsg.CopyWithoutVarBinds(), nil
			}

			//Complete data cannot be retrieved if it is a single varBind request. So returning the response message.
			if varLength == 1 {
				return respMsg, nil
			}

			isTooBig = true
			continue
		} else if respMsg.ErrorStatus() == consts.GeneralError {
			return respMsg, nil
		} else {
			//Store the temporary varBinds in the complete Slice
			if respMsg.ErrorStatus() == consts.NoError {
				isTooBig = false

				if len(tMsg.VarBinds()) == len(respMsg.VarBinds()) && len(respMsg.VarBinds()) == 1 {

					if !strings.EqualFold(tMsg.ObjectIDAt(0).String(), respMsg.ObjectIDAt(0).String()) &&
						(req.Command() != consts.GetBulkRequest && req.Command() != consts.GetNextRequest) {

						if strings.HasPrefix(respMsg.ObjectIDAt(0).String(), ".1.3.6.1.6.3.15.1.1.") {
							return nil, errors.New(getV3ErrorString(respMsg.ObjectIDAt(0).String()))
						}

					} else if req.Command() == consts.GetBulkRequest || req.Command() == consts.GetNextRequest {

						if strings.HasPrefix(respMsg.ObjectIDAt(0).String(), ".1.3.6.1.6.3.15.1.1.") {
							return nil, errors.New(getV3ErrorString(respMsg.ObjectIDAt(0).String()))
						} else {
							tVarbs = append(tVarbs, respMsg.VarBinds()...)
							start = end

							if req.Command() == consts.GetBulkRequest {
								nonRepIncrement = nonRepIncrement + len(tMsg.VarBinds())
							}

							if len(tMsg.VarBinds()) > nonReps {
								nonReps = 0
							} else {
								nonReps = nonReps - len(tMsg.VarBinds())
							}
						}

					} else {

						tVarbs = append(tVarbs, respMsg.VarBinds()...)
						start = end

						if req.Command() == consts.GetBulkRequest {
							nonRepIncrement = nonRepIncrement + len(tMsg.VarBinds())
						}

						if len(tMsg.VarBinds()) > nonReps {
							nonReps = 0
						} else {
							nonReps = nonReps - len(tMsg.VarBinds())
						}
					}
				} else {

					if len(respMsg.VarBinds()) == 1 {
						if strings.HasPrefix(respMsg.ObjectIDAt(0).String(), ".1.3.6.1.6.3.15.1.1.") {
							return nil, errors.New(getV3ErrorString(respMsg.ObjectIDAt(0).String()))
						}
					} else {
						tVarbs = append(tVarbs, respMsg.VarBinds()...)
						start = end

						if req.Command() == consts.GetBulkRequest {
							nonRepIncrement = nonRepIncrement + len(tMsg.VarBinds())
						}

						if len(tMsg.VarBinds()) > nonReps {
							nonReps = 0
						} else {
							nonReps = nonReps - len(tMsg.VarBinds())
						}
					}
				}

			} else {
				returnMsg := req.Copy()
				returnMsg.SetCommand(respMsg.Command())
				returnMsg.SetErrorStatus(respMsg.ErrorStatus())
				returnMsg.SetErrorIndex(int32(start) + respMsg.ErrorIndex())
				return returnMsg, nil
			}
		}

		if (req.Command() != consts.GetBulkRequest && len(tVarbs) == len(req.VarBinds())) ||
			(req.Command() == consts.GetBulkRequest && nonRepIncrement == len(tVarbs)) {

			//Constructing the complete PDU
			completeMsg := respMsg.CopyWithoutVarBinds()
			if end == varLength {
				completeMsg.SetErrorIndex(respMsg.ErrorIndex())
			} else {
				completeMsg.SetErrorIndex(int32(start) + respMsg.ErrorIndex())
			}

			completeMsg.SetVarBinds(tVarbs)
			return completeMsg, nil
		}
	}
}

//sendSilentDrops sends request for the silentDrops OID and returns the response.
func (h requestHandler) sendSilentDrops(session *snmp.SnmpSession, req *msg.SnmpMessage) (*msg.SnmpMessage, error) {
	sMsg := req.CopyWithoutVarBinds()
	sMsg.SetCommand(consts.GetRequest)
	sMsg.AddNull(*snmpvar.NewSnmpOID(".1.3.6.1.2.1.11.31.0"))

	var resp *msg.SnmpMessage
	var err error
	if resp, err = session.SyncSend(*sMsg); err != nil {
		return nil, err
	}

	return resp, nil
}

//getV3ErrorString returns the respective USM error strings based on the report OID.
func getV3ErrorString(reportOid string) string {
	switch reportOid {
	case unSupportedSecurityLevelOid:
		return "usmStatsUnsupportedSecurityLevel"
	case notInTimeWindowsOid:
		return "usmStatsNotInTimeWindows"
	case unknownUserNamesOid:
		return "usmStatsUnknownUserNames"
	case unknownEngineIdOid:
		return "usmStatsUnknownEngineIDs"
	case wrongDigestsOid:
		return "usmStatsWrongDigests"
	case decryptErrorOid:
		return "usmStatsDecryptionErrors"
	default:
		return "Unknown USM report oid received"
	}
	return "Unknown USM report oid received"
}

//getPartialMsgData returns partial SnmpMessage in case of no such element in returned response SnmpMessage
func (h requestHandler) getPartialMsgData(req *msg.SnmpMessage, resp *msg.SnmpMessage, session *snmp.SnmpSession) (*msg.SnmpMessage, error) {

	if (resp.ErrorStatus() != 0 || resp.ErrorIndex() != 0) && len(resp.VarBinds()) > 1 {

		var err error
		var varbind1 msg.SnmpVarBind

		index := resp.ErrorIndex()
		varbind := resp.VarBindAt(int(index) - 1)

		if resp.ErrorStatus() != 0 {
			varbind1 = msg.NewSnmpVarBind(varbind.ObjectID(), snmpvar.NewSnmpString(msg.ErrorMessage(resp.ErrorStatus())))
		}

		if resp, err = h.getPartialData(req, resp, session); err != nil {
			return nil, err
		}

		if resp == nil {
			return nil, err
		}

		//This recursion loop is inserted to handle the situation, when the order of error index coming from the failed request is not proper.
		if resp, err = h.getPartialMsgData(req, resp, session); err != nil {
			return nil, err
		}

		if resp == nil {
			return nil, err
		}

		resp.AddVarBindAt(int(index)-1, varbind1)
	}

	resp.SetErrorStatus(0)
	resp.SetErrorIndex(0)

	return resp, nil
}

//getPartialData removes the error varbind from the request and sends it. The response PDU will be returned.
func (h requestHandler) getPartialData(req *msg.SnmpMessage, resp *msg.SnmpMessage, session *snmp.SnmpSession) (*msg.SnmpMessage, error) {

	var err error

	if len(resp.VarBinds()) <= 1 || resp.ErrorIndex() == 0 {
		return resp, errors.New("Varbinds size less than one")
	}

	req.SetErrorIndex(resp.ErrorIndex())
	req.Fix()
	req.SetErrorIndex(0)

	if resp, err = h.syncSend(req, session); err != nil {
		return nil, err
	}

	return resp, nil
}

func (h requestHandler) syncSend(req *msg.SnmpMessage, session *snmp.SnmpSession) (*msg.SnmpMessage, error) {

	var resp *msg.SnmpMessage
	var err error

	if resp, err = session.SyncSend(*req); err != nil {
		return nil, err
	}

	if resp == nil {
		return nil, err
	} else {
		if resp.Command() == consts.ReportMessage {
			if strings.HasPrefix(resp.ObjectIDAt(0).String(), ".1.3.6.1.6.3.15.1.1.") {
				return nil, errors.New(getV3ErrorString(resp.ObjectIDAt(0).String()))
			} else {
				return nil, errors.New("Error occured. Varbind received in Report Message.\n" + resp.PrintVarBinds())
			}
		} else if !isValidateOIDList(req, resp) && resp.ErrorStatus() == consts.TooBig && strings.Contains(resp.ErrorString(), "too large") {
			return resp, nil
		} else if !isValidateOIDList(req, resp) && (req.Command() != consts.GetNextRequest && req.Command() != consts.GetBulkRequest) {
			return nil, errors.New("Error occured. Unexpected OID.\n" + resp.PrintVarBinds())
		} else if req.Command() == consts.GetNextRequest && len(req.VarBinds()) != len(resp.VarBinds()) {
			return nil, errors.New("Error occured. Unexpected OID.\n" + resp.PrintVarBinds())
		}

		return resp, nil
	}

	//return resp, nil
}

//isValidateOIDList returns true if all varbinds in the requests and responses properly match. Returns false otherwise
func isValidateOIDList(req *msg.SnmpMessage, resp *msg.SnmpMessage) bool {

	reqVarbs := req.VarBinds()
	respVarbs := resp.VarBinds()

	if len(reqVarbs) != len(respVarbs) {
		return false
	}

	for i, _ := range reqVarbs {
		if strings.EqualFold(req.ObjectIDAt(i).String(), resp.ObjectIDAt(i).String()) {
			continue
		} else {
			return false
		}
	}

	return true
}

func (h requestHandler) SendSNMPRequest(session *snmp.SnmpSession, pdu msg.SnmpMessage) (int32, error) {
	var reqId int32 = -1
	var err error

	if reqId, err = session.Send(pdu); err != nil {
		return reqId, err
	}

	return reqId, nil
}
