/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	"webnms/log"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	trans "webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
	"webnms/snmp/util"
)

//This interface defines the listener for SNMP responses.
//User should implement this interface in order to receive
//response messages asynchronously when using 'SnmpRequestServer'.
//
//Listener can be added to SnmpRequestServer by calling AddListener() method on it.
type Listener interface {
	//The method is called whenever an SNMP response is received
	//at the generator of the event, e.g. SnmpRequestServer, or when
	//a timeout occurs, if timeout event has been requested.
	ReceiveMessage(*msg.SnmpMessage)
}

//SnmpRequestServer is a High-Level API that aid in performing all kind of SNMP Operations (Get/Get-Next/Set etc.,) at ease.
//
//It provides various handler methods, which asynchronously sends the SNMP request to requestServer and returns immediately.
//It contains a list of OIDs and Variables list which will be used for any requests that happens through this SnmpRequestServer.
//It maintains the instances of Low-Level resources like SnmpSession, SnmpAPI.
//
//  requestServer := hl.NewSnmpRequestServer()
//  requestServer.SetTargetHostPort("localhost:8001")
//  requestServer.SetOIDList([]string{"1.4.0", "1.5.0"})
//  requestServer.AddListener(new(client))
//  reqId, _ := requestServer.Get()
//
//  //Implement Listener interface to receive response messages:
//  type client int
//  func (c client) ReceiveMessage(mesg *msg.SnmpMessage) {
//	 fmt.Println("Received Msg:", mesg.PrintVarBinds())
//  }
//For garbage collection of the resources used, ReleaseResources() method should be invoked.
//Different SnmpRequestServers will reuse the same SnmpAPI & SnmpSession. So, for releasing these resources, ReleaseResources() method should be called on every
//SnmpRequestServer created and when this method is called on the final SnmpRequestServer sharing the same low-level resources, these will be garbage collected.
type SnmpRequestServer struct {
	*snmpServer
	handler   requestHandler
	listeners map[int]Listener
	requests  map[int32]*msg.SnmpMessage //To store the outgoing request message instance.

	session         *snmp.SnmpSession
	api             *snmp.SnmpAPI
	protocolOptions trans.ProtocolOptions
	errorMsgs       ErrorMessages

	localHost   string
	localPort   int
	targetHost  string
	targetPort  int
	sessionName string

	oidList        []snmpvar.SnmpOID
	varList        []snmpvar.SnmpVar
	version        consts.Version
	community      string
	timeout        int
	retries        int
	nonRepeaters   int32
	maxRepetitions int32
	sendTimeout    bool

	msgMaxSize      int32
	principal       string
	authProtocol    consts.AuthProtocol
	authPassword    string
	privProtocol    consts.PrivProtocol
	privPassword    string
	contextName     string
	contextEngineID []byte
	validateUser    bool //To validate AuthPriv User
	secModelID      int32

	errorCode      int
	errorIndex     int
	errorString    string
	exceptionCode  int
	exceptionCodes map[int]int

	transModel engine.SnmpTransportModel //For custom transport model
}

//NewSnmpRequestServer creates and returns a new SnmpRequestServer instance.
//
//  Note:
//  * It will open a listener and bind to any available port to receive SNMP packets.
//  * It uses UDP by default for communication.
//  * It shares the low-level resources.
func NewSnmpRequestServer() *SnmpRequestServer {
	var requestServer *SnmpRequestServer
	var err error
	if requestServer, err = newRequestServer(nil, 0, "", nil); err != nil {
		//log.Fatal(err)
		return nil
	}
	return requestServer
}

//NewSnmpRequestServerBySession creates and returns a new SnmpRequestServer by custom SnmpSession name and localport.
//If the sessionName is same, the request servers will share the same session. If the port is 0 it will use the available free port.
//
//  Note: Use this function, if you want to use a dedicated SnmpSession (low-level resource) for the SnmpRequestServer.
func NewSnmpRequestServerBySession(sessionName string, localPort int) *SnmpRequestServer {
	var requestServer *SnmpRequestServer
	var err error
	if requestServer, err = newRequestServer(nil, localPort, sessionName, nil); err != nil {
		//log.Fatal(err)
		return nil
	}
	return requestServer
}

//NewSnmpRequestServerByProtocolOptions creates and returns new SnmpRequestServer using the ProtocolOptions and TransportModel.
//
//  Note:
//  * Use this function, if you want to use transport protocols other than UDP (or)
//    if you require the target to be opened in any specific address/port.
//  * This function can be used when using custom transport protocol. TransportModel instance should not be nil in this case.
//  * For UDP/TCP, transportModel instance can be nil.
func NewSnmpRequestServerByProtocolOptions(protocolOptions trans.ProtocolOptions, transportModel engine.SnmpTransportModel) *SnmpRequestServer {
	var requestServer *SnmpRequestServer
	var err error
	if requestServer, err = newRequestServer(protocolOptions, 0, "", transportModel); err != nil {
		//log.Fatal(err)
		return nil
	}
	return requestServer
}

//Internal function to create SnmpRequestServer
func newRequestServer(protoOptions trans.ProtocolOptions, port int, sesName string, transportModel engine.SnmpTransportModel) (*SnmpRequestServer, error) {
	//Set UDP by default
	if protoOptions == nil {
		udp := snmp.NewUDPProtocolOptions()
		udp.SetLocalPort(port)
		protoOptions = udp
	} else {
		//In case protocolIOptions is provided we might need a dedicated SnmpSession to be used
		if strings.TrimSpace(sesName) == "" {
			sesName = fmt.Sprintf("CUST_PROTO:%p", protoOptions) //Address of the ProtocolOptions is used for constructing unique name.
		}
	}
	requestServer := &SnmpRequestServer{
		//Perform initialization
		listeners:       make(map[int]Listener),
		requests:        make(map[int32]*msg.SnmpMessage),
		protocolOptions: protoOptions,
		localPort:       port,
		sessionName:     sesName,
		localHost:       "localhost",
		targetHost:      "localhost",
		targetPort:      161,
		version:         Version1,
		community:       "public",
		maxRepetitions:  50,
		timeout:         5000,
		msgMaxSize:      484,
		authProtocol:    NO_AUTH,
		privProtocol:    NO_PRIV,
		secModelID:      security.USMID, //using USM by default
		errorCode:       Err_No_Error,
		errorIndex:      0,
		errorString:     "No error",
		exceptionCode:   -1,
		transModel:      transportModel,
	}
	requestServer.snmpServer = newSnmpServer()
	if err := requestServer.initSnmpStore(); err != nil {
		return nil, err
	}

	return requestServer, nil
}

//Initializes the SnmpServer's SnmpStore, and create new (or) reuse the SnmpSession/SnmpAPI from SnmpStore.
func (rs *SnmpRequestServer) initSnmpStore() error {
	rs.sessionName = rs.getSessionName()

	var ok bool
	//Take the SnmpAPI instance
	if _, ok = rs.snmpStore["API"]; !ok { //Creating API entry for very first time - new entry
		rs.api = snmp.NewSnmpAPI()
		im := make(map[string]*value)
		im["API"] = &value{rs.api, 1}
		rs.snmpStore["API"] = im
	} else if _, ok = rs.snmpStore["API"]["API"]; !ok { //Creating new SnmpAPI instance for first time
		rs.api = snmp.NewSnmpAPI()
		im := rs.snmpStore["API"]
		im["API"] = &value{rs.api, 1}
	} else if val, ok := rs.snmpStore["API"]["API"]; ok { //Reusing the already created SnmpAPI instance
		rs.api = val.llInstance.(*snmp.SnmpAPI)
		val.concurrentUsers++
	}

	//Take the SnmpSession instance
	if _, ok = rs.snmpStore["SESSION"]; !ok { //Creating SESSION for very first time - new entry
		rs.session = snmp.NewSnmpSession(rs.api) //Creating new SnmpSession
		im := make(map[string]*value)
		im[rs.sessionName] = &value{rs.session, 1}
		rs.snmpStore["SESSION"] = im
	} else if _, ok = rs.snmpStore["SESSION"][rs.sessionName]; !ok { //Creating new SnmpSession instance for first time
		rs.session = snmp.NewSnmpSession(rs.api) //Creating new SnmpSession
		im := rs.snmpStore["SESSION"]
		im[rs.sessionName] = &value{rs.session, 1}
	} else if val, ok := rs.snmpStore["SESSION"][rs.sessionName]; ok { //Reusing the already created SnmpSession instance
		rs.session = val.llInstance.(*snmp.SnmpSession) //Reusing the exisiting SnmpSession
		val.concurrentUsers++
	}

	if rs.protocolOptions != nil {
		rs.session.SetProtocolOptions(rs.protocolOptions)
	} else {
		//Using UDP by default
		udp := snmp.NewUDPProtocolOptions()
		udp.SetLocalHost(rs.localHost)
		udp.SetLocalPort(rs.localPort) //Bind to port if the user has specified one, else bind to any available port (0)
	}

	//Register the transport model in case custom transport model is used
	if rs.transModel != nil {
		rs.session.RegisterTransportModel(rs.transModel)
	}

	//Try to open new SnmpSession only if you have newly created the SnmpSession (first time).
	if !rs.session.IsSessionOpen() && (rs.snmpStore["SESSION"][rs.sessionName].concurrentUsers == 1) {
		if err := rs.session.Open(); err != nil {
			delete(rs.snmpStore["SESSION"], rs.sessionName)
			return err
		}
	}

	rs.errorMsgs, _ = newErrMap()
	initV3Errors()

	return nil
}

//####### SnmpRequestServer - Getter and Setter methods #########//

//SetOIDList sets the list of ObjectIDs in SnmpRequestServer rs.
//
//oidList can be a slice of type string (or) SnmpOID.
//For other types, this method sets the rs's error-code to Illegal_Argument and returns.
func (rs *SnmpRequestServer) SetOIDList(oidList interface{}) *SnmpRequestServer {
	rs.oidList = []snmpvar.SnmpOID{}
	if oidList != nil {
		switch ls := oidList.(type) {
		case []string:
			dropCount := 0
			for _, oidStr := range ls {
				if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
					rs.oidList = append(rs.oidList, *oid)
				} else {
					dropCount++
					log.Info("Failure in adding the OID: '%s' to rs. Invalid OID.", oidStr) //Invalid OID String
				}
			}
			//log.Println("Dropped count:",dropCount)
		case []snmpvar.SnmpOID:
			rs.oidList = ls
		default:
			rs.errorCode = Illegal_Argument
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Info("Failure in adding the OID: '%v' to rs. Invalid OID Type.", ls)
			return rs
		}
	} else {
		rs.oidList = nil
	}

	return rs
}

//OIDList returns a slice of ObjectID strings set on this SnmpRequestServer rs.
func (rs *SnmpRequestServer) OIDList() []string {
	oidStrSlice := make([]string, len(rs.oidList))
	for i, oid := range rs.oidList {
		oidStrSlice[i] = oid.String()
	}
	return oidStrSlice
}

//AddOID adds the ObjectID into SnmpRequestServer rs' ObjectID List.
//
//oid can be of type string OID, snmpvar.SnmpOID or *snmpvar.SnmpOID.
//For other types, this method simply returns after setting the rs error code to Illegal_Argument.
func (rs *SnmpRequestServer) AddOID(oid interface{}) *SnmpRequestServer {
	if oid != nil {
		switch o := oid.(type) {
		case string:
			if oid := snmpvar.NewSnmpOID(o); oid != nil {
				rs.oidList = append(rs.oidList, *oid)
			} else {
				log.Info("Failure in adding the OID: '%s' to rs. Invalid OID.", o) //Dropped. Invalid OID.
			}
		case snmpvar.SnmpOID:
			rs.oidList = append(rs.oidList, o)
		case *snmpvar.SnmpOID:
			rs.oidList = append(rs.oidList, *o)
		default:
			rs.errorCode = Illegal_Argument
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Info("Failure in adding the OID: '%v' to rs. Invalid OID Type.", o)
			return rs
		}
	}

	return rs
}

//AddOIDs adds the ObjectID list to the end of SnmpRequestServer rs's ObjectID List.
//
//oids can be slice of type string, snmpvar.SnmpOID (or) *snmpvar.SnmpOID.
//For other types, this method simply returns after setting the rs's error code to Illegal_Argument.
func (rs *SnmpRequestServer) AddOIDs(oids interface{}) *SnmpRequestServer {
	if oids != nil {
		switch ls := oids.(type) {
		case []string:
			dropCount := 0
			for _, oidStr := range ls {
				if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
					rs.oidList = append(rs.oidList, *oid)
				} else {
					dropCount++
					log.Info("Failure in adding the OID: '%s' to the rs. Invalid OID.", oidStr) //Invalid OID String
				}
			}
			log.Info("OID dropped count using rs's AddOIDs:", dropCount)
		case []snmpvar.SnmpOID:
			rs.oidList = append(rs.oidList, ls...)
		case []*snmpvar.SnmpOID:
			for _, oid := range ls {
				rs.oidList = append(rs.oidList, *oid)
			}
		default:
			rs.errorCode = Illegal_Argument
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Info("Failure in adding the OIDs: '%v' to the rs. Invalid OID Type.", ls)
			return rs
		}
	}

	return rs
}

//AddOIDAt adds the ObjectID into the specified index of SnmpRequestServer rs' ObjectID List. Index starts from 0.
//oid should be of type string, snmpvar.SnmpOID (or) *snmpvar.SnmpOID.
//
//  Will not be added in following cases:
//  1. if index is less than 0
//  2. if index is greater than rs' ObjectID List length plus one
//  3. if oid type is other than string, snmpvar.SnmpOID (or) *snmpvar.SnmpOID
//
//For oid type other than the mentioned above, this method simply returns after setting the rs error code to Illegal_Argument.
func (rs *SnmpRequestServer) AddOIDAt(index int, oid interface{}) *SnmpRequestServer {
	if index < 0 || index > len(rs.oidList) || oid == nil {
		return rs
	}
	switch o := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(o); snmpOID != nil {
			rs.oidList = append(rs.oidList[:index], append([]snmpvar.SnmpOID{*snmpOID}, rs.oidList[index:]...)...)
		} else {
			log.Info("Failure in adding the OID: '%s' to rs. Invalid OID.", o) //Dropped. Invalid OID.
		}
	case snmpvar.SnmpOID:
		rs.oidList = append(rs.oidList[:index], append([]snmpvar.SnmpOID{o}, rs.oidList[index:]...)...)
	case *snmpvar.SnmpOID:
		rs.oidList = append(rs.oidList[:index], append([]snmpvar.SnmpOID{*o}, rs.oidList[index:]...)...)
	default:
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Info("Failure in adding the OID: '%v' to rs. Invalid OID Type.", o)
		return rs
	}

	return rs
}

//RemoveOIDAt removes the ObjectID from the SnmpRequestServer rs' ObjectID List at specified 'index'.
//Does nothing if index is negative or is greater than the request server's ObjectID List size plus one.
func (rs *SnmpRequestServer) RemoveOIDAt(index int) *SnmpRequestServer {
	if index < 0 || index > len(rs.oidList) {
		return rs
	}
	rs.oidList = append(rs.oidList[:index], rs.oidList[index+1:]...)

	return rs
}

//OIDAt returns the ObjectID at specified index from the SnmpRequestServer rs' ObjectID List as a string. Index starts at 0.
//Returns "<nil>" if index is less than 0 or index is greater than the ObjectID List.
func (rs *SnmpRequestServer) OIDAt(index int) string {
	if index < 0 || index > len(rs.oidList) {
		//log.Println("Invalid index specified.")
		return "<nil>"
	}
	return rs.oidList[index].String()
}

//SetVersion sets the SnmpVersion ver on this SnmpRequestServer rs.
//Version1 is used by default.
func (rs *SnmpRequestServer) SetVersion(ver consts.Version) *SnmpRequestServer {
	if ver == Version1 || ver == Version2C || ver == Version3 {
		rs.version = ver
	}
	return rs
}

//Version returns the SnmpVersion set on this SnmpRequestServer rs.
func (rs *SnmpRequestServer) Version() consts.Version { return rs.version }

//SetCommunity sets the community string on SnmpRequestServer rs, which will be used for all requests that happens through this requestServer.
//Default value is public.
func (rs *SnmpRequestServer) SetCommunity(comm string) *SnmpRequestServer {
	rs.community = comm
	return rs
}

//Community returns the community string set on this SnmpRequestServer rs.
func (rs *SnmpRequestServer) Community() string { return rs.community }

//SetDebug determines if debug output will be printed.
func (rs *SnmpRequestServer) SetDebug(debug bool) *SnmpRequestServer {
	rs.api.SetDebug(debug)
	return rs
}

//Debug returns bool value indicating whether debug in enabled on SnmpRequestServer rs.
func (rs *SnmpRequestServer) Debug() bool { return rs.api.Debug() }

//SetTargetHost sets the requestServer hostname on rs. 'host' added will be used as a requestServer for all communications happens through rs.
func (rs *SnmpRequestServer) SetTargetHost(host string) *SnmpRequestServer {
	rs.targetHost = host
	rs.protocolOptions.SetRemoteHost(host)

	return rs
}

//SetTargetHostPort sets the hostname and port on the SnmpRequestServer rs.
//
//hostPort string should be of the form "host:port", "[host]:port", etc. Ex: localhost:161
//Returns error if hostPort string is invalid.
func (rs *SnmpRequestServer) SetTargetHostPort(hostPort string) error {
	host, port, err := net.SplitHostPort(hostPort)
	if err != nil {
		return err
	}
	if host != "" {
		rs.targetHost = host
		rs.protocolOptions.SetRemoteHost(host)
	}
	if port != "" {
		if portNum, err := strconv.Atoi(port); err != nil {
			return err
		} else {
			rs.SetTargetPort(portNum)
			rs.protocolOptions.SetRemotePort(portNum)
		}
	}

	return nil
}

//TargetHost returns the hostname string set on rs.
func (rs *SnmpRequestServer) TargetHost() string { return rs.targetHost }

//SetTargetPort sets the requestServer port number on rs. Port number should range between 0 and 65535.
func (rs *SnmpRequestServer) SetTargetPort(port int) *SnmpRequestServer {
	if port >= 0 && port <= 65535 {
		rs.targetPort = port
		rs.protocolOptions.SetRemotePort(port)
	}
	return rs
}

//TargetPort returns the requestServer port number set on rs.
func (rs *SnmpRequestServer) TargetPort() int { return rs.targetPort }

//SetTimeout sets the timeout value on rs in seconds.
//The timeout is the time to wait for the first response in seconds, before attempting a retransmission
//
//Default value is 5 seconds.
func (rs *SnmpRequestServer) SetTimeout(timeout int) *SnmpRequestServer {
	if timeout >= 0 && timeout*1000 != rs.timeout {
		rs.timeout = timeout * 1000
	}
	return rs
}

//Timeout returns the timeout value set on rs in seconds.
//Default value is 5 seconds.
func (rs *SnmpRequestServer) Timeout() int { return (rs.timeout / 1000) }

//SetRetries sets the number of retries before timeout on rs. Default retries value is 0.
func (rs *SnmpRequestServer) SetRetries(retries int) *SnmpRequestServer {
	if retries >= 0 {
		rs.retries = retries
	}
	return rs
}

//Retries returns the retries value set on rs. Default value is 0.
func (rs *SnmpRequestServer) Retries() int { return rs.retries }

//SetNonRepeaters sets Non-Repeaters value on rs, which will be used for Get-Bulk requests. Default value is 0.
//
//Sets the request server's error-code to Invalid_Non_Rep and returns, if a negative nonReps is specified.
func (rs *SnmpRequestServer) SetNonRepeaters(nonReps int32) *SnmpRequestServer {
	if nonReps >= 0 {
		rs.nonRepeaters = nonReps
	} else {
		rs.errorCode = Invalid_Non_Rep
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
	}
	return rs
}

//NonRepeaters returns the Non-Repeaters value set on rs.
//Default value is 0.
func (rs *SnmpRequestServer) NonRepeaters() int32 { return rs.nonRepeaters }

//SetMaxRepetitions sets the Max-Repetitions value on rs, which will be used for Get-Bulk requests.
//Default value is 50.
func (rs *SnmpRequestServer) SetMaxRepetitions(maxReps int32) *SnmpRequestServer {
	if maxReps >= 0 {
		rs.maxRepetitions = maxReps
	}
	return rs
}

//MaxRepetitions returns the Max-Repetiotions value set on rs.
//Default value is 50.
func (rs *SnmpRequestServer) MaxRepetitions() int32 { return rs.maxRepetitions }

//MsgMaxSize returns the message max size field for the SNMPV3 packet. Default value is 484.
func (rs *SnmpRequestServer) MsgMaxSize() int32 {
	return rs.msgMaxSize
}

//SetMsgMaxSize sets the message max size field for the SNMPV3 packet. If maxSize is less than 484, it will be set to 484.
func (rs *SnmpRequestServer) SetMsgMaxSize(maxSize int32) *SnmpRequestServer {
	rs.msgMaxSize = maxSize
	if maxSize < 484 {
		rs.msgMaxSize = 484
	}
	return rs
}

//SetPrincipal sets the SNMPv3 principal/security-name.
func (rs *SnmpRequestServer) SetPrincipal(userName string) *SnmpRequestServer {
	rs.principal = userName
	return rs
}

//Principal returns the SNMPv3 principal/security-name.
func (rs *SnmpRequestServer) Principal() string { return rs.principal }

//SetAuthProtocol sets the SNMPv3 auth protocol. The possible options are hl.NO_AUTH, hl.MD5_AUTH & hl.SHA_AUTH.
func (rs *SnmpRequestServer) SetAuthProtocol(authProto consts.AuthProtocol) *SnmpRequestServer {
	if authProto == NO_AUTH || authProto == MD5_AUTH || authProto == SHA_AUTH {
		rs.authProtocol = authProto
	}
	return rs
}

//AuthProtocol returns the SNMPv3 auth protocol.
func (rs *SnmpRequestServer) AuthProtocol() consts.AuthProtocol { return rs.authProtocol }

//SetAuthPassword sets the SNMPv3 auth password.
func (rs *SnmpRequestServer) SetAuthPassword(authPass string) *SnmpRequestServer {
	rs.authPassword = authPass
	return rs
}

//AuthPassword returns the SNMPv3 auth password.
func (rs *SnmpRequestServer) AuthPassword() string { return rs.authPassword }

//SetPrivProtocol sets the SNMPv3 priv protocol. Valid priv protocols are hl.NO_PRIV, hl.DES_PRIV, hl.TRIPLE_DES_PRIV, hl.AES_128_PRIV, hl.AES_192_PRIV & hl.AES_256_PRIV.
func (rs *SnmpRequestServer) SetPrivProtocol(privProto consts.PrivProtocol) *SnmpRequestServer {
	if privProto == NO_PRIV || privProto == DES_PRIV || privProto == TRIPLE_DES_PRIV || privProto == AES_128_PRIV || privProto == AES_192_PRIV || privProto == AES_256_PRIV {
		rs.privProtocol = privProto
	}
	return rs
}

//PrivProtocol returns the SNMPv3 priv protocol.
func (rs *SnmpRequestServer) PrivProtocol() consts.PrivProtocol { return rs.privProtocol }

//SetPrivPassword sets the SNMPv3 priv password.
func (rs *SnmpRequestServer) SetPrivPassword(privPass string) *SnmpRequestServer {
	rs.privPassword = privPass
	return rs
}

//PrivPassword returns the SNMPv3 priv password.
func (rs *SnmpRequestServer) PrivPassword() string { return rs.privPassword }

//SetContextName sets the SNMPv3 context name.
func (rs *SnmpRequestServer) SetContextName(cName string) *SnmpRequestServer {
	rs.contextName = cName
	return rs
}

//ContextName returns the SNMPv3 context name.
func (rs *SnmpRequestServer) ContextName() string { return rs.contextName }

//SetContextEngineID sets the SNMPv3 context engine id.
func (rs *SnmpRequestServer) SetContextEngineID(cID []byte) *SnmpRequestServer {
	rs.contextEngineID = cID
	return rs
}

//ContextEngineID returns the SNMPv3 context engine id.
func (rs *SnmpRequestServer) ContextEngineID() []byte { return rs.contextEngineID }

//ValidateUser enables the validation of the users for SNMPv3 cases.
//If enabled, validation will be performed for the user,
//
//	* NoAuthNoPriv: Simple GetNext-Request is sent to see if the user exist in the remote entity.
//	* AuthPriv: GetNext-Request as AuthPriv security level will be sent to validate the PrivProtocol and PrivPassword.
func (rs *SnmpRequestServer) ValidateUser(validate bool) {
	rs.validateUser = validate
}

//IsValidateUser returns bool indicating whether user validation in enabled on rs for SNMPv3.
func (rs *SnmpRequestServer) IsValidateUser() bool {
	return rs.validateUser
}

//ErrorCode returns the error code for the last request made through rs.
//
//Returns 0 if there is no error.
func (rs *SnmpRequestServer) ErrorCode() int { return rs.errorCode }

//ErrorString returns the error string for the last request made through rs.
//
//Before sending any request the error string will be "No Error Code Registered".
//The Error index which is the index of the error in the PDU will be appended to this Error String.
//
//Returns an error string (or) "No Error" in case of no error.
func (rs *SnmpRequestServer) ErrorString() string { return rs.errorString }

//ErrorResultString returns error string associated with the response PDU. Returns "Error: Request Timed Out", if msg is either nil or other than response/report PDU.
func (rs *SnmpRequestServer) ErrorResultString(msg *msg.SnmpMessage) string {

	var errorStr string = "No error"

	if msg == nil {
		rs.errorCode = Request_Timed_Out
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)

		errorStr = rs.errorString + " to " + rs.targetHost
		return errorStr
	}

	if msg.Command() != consts.GetResponse && msg.Command() != consts.ReportMessage {

		rs.errorCode = Request_Timed_Out
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)

		errorStr = rs.errorString + " to " + msg.ProtocolOptions().RemoteHost()
		return errorStr
	}

	if msg.ErrorStatus() != 0 {
		errorStr = getMSGCommand(msg.Command()) + " PDU received from " + msg.ProtocolOptions().RemoteHost() + "\n"
	}

	if msg.Version() == Version1 {
		if msg.ErrorStatus() != 0 {
			rs.errorCode = int(msg.ErrorStatus())
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)

			errorStr += "Error Indication in response: " + rs.errorString + "\n" + "ErrIndex: " + strconv.FormatInt(int64(msg.ErrorIndex()), 10)
			return errorStr
		}
	} else if msg.Version() == Version2C || msg.Version() == Version3 {
		if msg.ErrorStatus() != 0 {
			rs.errorCode = int(msg.ErrorStatus())
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)

			errorStr = "Error Indication in response: " + rs.errorString + "\n" + "ErrIndex: " + strconv.FormatInt(int64(msg.ErrorIndex()), 10)
			return errorStr
		} else {
			for _, varb := range msg.VarBinds() {
				code := varb.ExceptionIndex()
				if code != 0 {
					rs.errorCode = code
					rs.errorString = rs.errorMsgs.ErrorString(code)

					errorStr = getMSGCommand(msg.Command()) + " PDU received from " + msg.ProtocolOptions().RemoteHost() + "\n" + "Error Indication in response: " + rs.errorString + "\n" + varb.TagString()
					return errorStr
				}
			}
		}
	} else {
		rs.errorCode = Invalid_Version
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)

		return rs.errorString
	}

	return errorStr
}

var id int

//AddListener adds new Listener to SnmpRequestServer rs.
//SnmpRequestServer will push the incoming response messages to listener's ReceiveMessage() method.
func (rs *SnmpRequestServer) AddListener(listener Listener) *SnmpRequestServer {
	if listener != nil {
		rs.listeners[id] = listener
		id++
	}
	if rs.session != nil {
		if !rs.session.IsSnmpClientExist(rs) {
			rs.session.AddSnmpClient(rs) //Add TrapReceiver as SnmpClient to receive Trap Messages
		}
	}
	return rs
}

//RemoveListener removes the listener from SnmpRequestServer. This will unsubscribe the listener from receiving incoming responses.
func (rs *SnmpRequestServer) RemoveListener(listener Listener) *SnmpRequestServer {
	for key, l := range rs.listeners {
		if l == listener {
			delete(rs.listeners, key)
			return rs
		}
	}
	return rs
}

//RemoveAllListeners removes all the Listeners added to this SnmpRequestServer rs.
//
//Completely unsubscribe all the listeners from receiving response Snmp Messages.
func (rs *SnmpRequestServer) RemoveAllListeners() {
	for key, _ := range rs.listeners {
		delete(rs.listeners, key)
	}
	//rs.listeners = make(map[int]Listener)
}

//SetPacketBufferSize sets the Packers Buffer Size which will be used for receiving the snmp packets on rs'  SnmpSession.
//By default buffer size used is 65535.
func (rs *SnmpRequestServer) SetPacketBufferSize(buffer int) *SnmpRequestServer {
	if rs.session != nil && buffer > 0 {
		rs.session.SetPacketBufferSize(buffer)
	}
	return rs
}

//PacketBufferSize returns the Packet Buffer Size set on rs' SnmpSession.
//Default value is 65535.
func (rs *SnmpRequestServer) PacketBufferSize() int { return rs.session.PacketBufferSize() }

//SetAutoInformResponse sets the automatic response flag for the Inform Request.
//If this flag is set to true, then the SNMP stack automatically sends a Get-Response message back to the sender. The default value is true.
func (rs *SnmpRequestServer) SetAutoInformResponse(autoInform bool) *SnmpRequestServer {
	if rs.session != nil {
		rs.session.SetAutoInformResponse(autoInform)
	}
	return rs
}

//AutoInformResponse returns bool indicating whether automatic response for Inform request is enabled or not.
func (rs *SnmpRequestServer) AutoInformResponse() bool { return rs.session.AutoInformResponse() }

//SetTimeoutPolicy sets TimeoutPolicy on this Session. Sets the user's own implementation of TimeoutPolicy.
//Default TimeoutPolicy is exponential.
func (rs *SnmpRequestServer) SetTimeoutPolicy(timeoutPolicy snmp.TimeoutPolicy) *SnmpRequestServer {
	if rs.session != nil {
		rs.session.SetTimeoutPolicy(timeoutPolicy)
	}
	return rs
}

//RestoreToDefaultTimeoutPolicy restores the TimeoutPolicy to default 'ExponentialTimeoutPolicy' on rs' SnmpSession (low-level resource).
func (rs *SnmpRequestServer) RestoreToDefaultTimeoutPolicy() {
	if rs.session != nil {
		rs.session.RestoreToDefaultTimeoutPolicy()
	}
}

//RegisterV3SecurityModel registers the msg security model to be used for SNMPv3.
//By default USM will be used.
//
//secModel should be an implementation of SnmpSecurityModel interface.
//This model will be registered with the security subsystem.
//
//This method should be used only when using custom security models.
//For more details, refer engine.SnmpSecurityModel doc.
func (rs *SnmpRequestServer) RegisterV3SecurityModel(secModel engine.SnmpSecurityModel) {
	if secModel != nil {
		//Let's do the registeration of this security model with the security subsystem
		secSS := rs.api.SnmpEngine().SecuritySubSystem()
		modelID := secModel.ID()
		if secSS.Model(modelID) == nil {
			//Model is not registered in SecSS
			secSS.AddModel(secModel)
		}
	}
}

//SetV3SecurityModel sets the default security model to be used for SNMPv3.
//By default USM will be used.
//
//  Note:
//  * ModelID should be already registered with the Security Subsystem.
//  * This method should be used only when using custom security models.
func (rs *SnmpRequestServer) SetV3SecurityModel(secModelID int32) *SnmpRequestServer {
	rs.secModelID = secModelID
	return rs
}

//SecurityModel returns the name of the Security Model set on this request server rs.
func (rs *SnmpRequestServer) SecurityModel() string {
	secSS := rs.api.SnmpEngine().SecuritySubSystem()
	secModel := secSS.Model(rs.secModelID)
	if secModel != nil {
		secModel.Name()
	}

	return "<nil>"
}

//Implements SnmpClient interface in order to receive the response message asynchoronously.
//Process incoming message and fwd to Listeners.
//
//Request_Timed_Out - error code is set, in case the request is timed-out.
//
//For internal use only.
func (rs *SnmpRequestServer) Callback(resp *msg.SnmpMessage, reqID int32) {
	if resp != nil {
		delete(rs.requests, reqID) //Response received, hence delete the request.
		if len(rs.listeners) > 0 {
			//Call the respective trap listeners
			for _, v := range rs.listeners {
				v.ReceiveMessage(resp)
			}
		} else {
			//log.Println("There is no listener to catchup this message.")
		}
	} else {
		//log.Println(Nil response received.)

		rs.errorCode = Request_Timed_Out
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode) + " to " + rs.targetHost

		if rs.sendTimeout {
			if len(rs.listeners) > 0 {
				//Call the respective listener
				for _, v := range rs.listeners {
					v.ReceiveMessage(resp)
				}
			} else {
				//log.Println("There is no listener to catchup this message.")
			}
		}
	}
	return
}

//Authenticates the incoming message. Always returns true.
//
//For internal use only.
func (rs *SnmpRequestServer) Authenticate(res *msg.SnmpMessage, community string) bool {
	return true //No matter what, always authenticate incoming SnmpMessage.
}

//This logs the debug prints to the logger or err console.
//
//For internal use only.
func (rs *SnmpRequestServer) DebugStr(dbgStr string) {
	if log.LogFilters() != nil {
		log.Logf(log.DEBUG, dbgStr)
	} else { //Just write to error console
		if rs.Debug() {
			fmt.Fprintln(os.Stderr, dbgStr)
		}
	}
}

//Wait blocks the current routine till the arrival of responses for all asynchronous requests made through this SnmpRequestServer rs.
//
//It implements wait group counter to wait for the response messages.
//To perform async communication, user of the API can use channels (or) other ways to wait for response messages.
//This is just an additional utility method for acheiving the same.
//
//  Note:
//  * In case, multiple request servers are used in an application, this method will block for the responses for all the requests made through all the servers.
func (rs *SnmpRequestServer) Wait() {
	if rs.session != nil {
		rs.session.Wait()
	}
}

//SendTimeoutMsg sets the boolean state of whether this rs
//should send timeout events to registered listeners or not.
//
//If set to to true, all the listeners will receive the time-out events.
func (rs *SnmpRequestServer) SendTimeoutMsg(val bool) {
	rs.sendTimeout = val
}

//CreateV3Tables creates new user entry and adds it to USMPeerEngineLCD. Discovery and Time-Synchronization will be performed, if applicable.
//Returns 1 if entry is successfully added, 0 or negative value otherwise.
//If an userEntry corresponding to the userName and the engineID(or host and port) is already present in the USMPeerEngineLCD, then no new user entry will be created.
//The error, if any, should be checked specifically with the method ErrorString/ErrorCode or the error returned.
//The following error codes can be set,
//
//	* Discovery_Failed
//	* Time_Sync_Failed
//	* UnSupported_SecurityLevel_Error
//	* Not_In_Time_Windows_Error
//	* Unknown_Usernames_Error
//	* Unknown_EngineID_Error
//	* Wrong_Digest_Error
//	* Decrypt_Error
//
//Returns,
//  0 if the version is other than v3.
//  1 if userTable is created Successfully.
//  -1 if discovery fails.
//  -2 if TimeSync fails.
//  -3 if User is already created for that host and port.
//
//Note: Only for USM Security Model
func (rs *SnmpRequestServer) CreateV3Tables() (int, error) {
	start := 0
	log.Trace("* CreateV3Tables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	rs.errorCode = Err_No_Error
	rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
	rs.exceptionCode = -1

	if rs.version != Version3 {
		errStr := "Invalid version to create v3 tables. Version should be V3."
		log.Trace("Returning.. Error: " + errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return 0, errors.New(errStr)
	}

	usmEngineLCD := rs.session.API().USMPeerEngineLCD()
	remoteEngine := usmEngineLCD.Engine(rs.protocolOptions.RemoteHost(), rs.protocolOptions.RemotePort())
	var engineID []byte = nil
	if remoteEngine != nil &&
		remoteEngine.AuthoritativeEngineID() != nil &&
		len(remoteEngine.AuthoritativeEngineID()) > 0 {
		//Engine Entry for this remote entity already present.
		engineID = remoteEngine.AuthoritativeEngineID()
	}

	if engineID != nil {
		userLCD := rs.session.API().USMUserLCD()
		usmSecureUser, _ := userLCD.SecureUser(engineID, rs.principal)
		if usmSecureUser != nil {
			errStr := fmt.Sprintf("User entry already present for the user '%s'.", rs.principal)
			log.Trace("Returning.. Error: " + errStr)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -3, errors.New(errStr)
		}
	}

	err := util.Init_V3_LCD(rs.session,
		rs.protocolOptions,
		rs.principal,
		engineID,
		rs.authProtocol,
		rs.authPassword,
		rs.privProtocol,
		rs.privPassword,
		rs.validateUser, //ValidateUser
	)

	//Check the error received.
	if err != nil {
		errorCode := err.(*util.InitV3Error).ErrorCode()

		switch errorCode {
		case consts.DiscoveryFailure:
			rs.errorCode = Discovery_Failed
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. Error: " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, err
		case consts.TimeSyncFailure, consts.ValidationFailure, consts.DecryptionError, consts.UnknownUserNameError, consts.UnSupportedSecurityLevelError, consts.NotInTimeWindowsError, consts.UnknownEngineIDError, consts.WrongDigestsError:
			rs.setCreateV3ErrorCode(errorCode)
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			rs.exceptionCode = 0
			log.Trace("Returning.. Error: " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -2, err
		default:
			rs.errorCode = Discovery_Failed
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode) + " : " + err.Error()
			log.Trace("Returning.. Error: " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, err
		}
	}

	log.Trace("Returning.. V3 tables creation successful.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return 1, nil
}

//ManageV3Tables modifies the auth and priv parameters of the existing user entry, if any, or else creates new user entry and adds it to USMUserLCD.
//Discovery and Time-Synchronization will be performed, if applicable.
//The error, if any, should be checked using the method ErrorString/ErrorCode or the error returned.
//The following error codes can be set,
//
//  * Discovery_Failed
//  * Time_Sync_Failed
//  * UnSupported_SecurityLevel_Error
//  * Not_In_Time_Windows_Error
//  * Unknown_Usernames_Error
//  * Unknown_EngineID_Error
//  * Wrong_Digest_Error
//  * Decrypt_Error
//
//Note:
//
//	* Username cannot be modified
//	* Only for USM Security Model
func (rs *SnmpRequestServer) ManageV3Tables() error {
	start := 0
	log.Trace("* ManageV3Tables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	rs.errorCode = Err_No_Error
	rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
	rs.exceptionCode = -1

	if rs.version != Version3 {
		errStr := "Invalid version to create v3 tables. Version should be V3."
		log.Trace("Returning.. Error: " + errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return errors.New(errStr)
	}

	var err error = util.Init_V3_LCD(rs.session,
		rs.protocolOptions,
		rs.principal,
		nil,
		rs.authProtocol,
		rs.authPassword,
		rs.privProtocol,
		rs.privPassword,
		rs.validateUser,
	)

	if err != nil {
		errString := err.Error()
		errorCode := err.(*util.InitV3Error).ErrorCode()

		switch errorCode {
		case consts.DiscoveryFailure:
			rs.errorCode = Discovery_Failed
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		case consts.TimeSyncFailure, consts.ValidationFailure, consts.DecryptionError, consts.UnknownUserNameError, consts.UnSupportedSecurityLevelError, consts.NotInTimeWindowsError, consts.UnknownEngineIDError, consts.WrongDigestsError:
			rs.setCreateV3ErrorCode(errorCode)
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			rs.exceptionCode = 0
		default:
			rs.errorCode = Discovery_Failed
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode) + " : " + errString
		}

		log.Trace("Returning.. Error: " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return err
	}

	log.Trace("Returning.. V3 tables creation/updation successful.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return nil
}

//PeerEngineLCD returns the LCD of engine entries specific to the security model set on rs.
//By default USM's PeerEngineLCD will be returned.
func (rs *SnmpRequestServer) PeerEngineLCD() engine.SnmpPeerEngineLCD {
	return rs.api.PeerEngineLCD(rs.secModelID)
}

//USMUserLCD returns the User based security model's UserLCD instance, which contains details of
//Secure Users created under this model.
func (rs *SnmpRequestServer) USMUserLCD() usm.USMUserLCD {
	return rs.api.USMUserLCD()
}

//USMPeerEngineLCD returns the LCD of engine entries specific to USM.
//PeerEngineLCD contains details of the peer SNMP entities such as EngineID, EngineBoots etc.,
func (rs *SnmpRequestServer) USMPeerEngineLCD() engine.SnmpPeerEngineLCD {
	return rs.api.USMPeerEngineLCD()
}

//InitDB initializes the database connection using the driverName and dataSourceName provided.
//User should have loaded the specific DB driver to open Database connection.
//
//This opens the database connection and creates the necessary tables in the database required for the SNMP API.
//
//dialectID is an integer constant indicating the SQLDialect which the database will be using
//for performing database operations.
//Dialect is required for inter-operability between different database queries.
//
//Refer db package for the list of SQLDialect implementation and their associated constants. If specific dialect
//implementation is not available, user should implement their own and register the same using RegisterSQLDialect() method.
//Refer db.SQLDialect interface for more details.
//
//Returns error in case of any db related error occurs or if unsupported dialectID is passed.
func (rs *SnmpRequestServer) InitDB(driverName, dataSourceName string, dialectID int) error {
	start := 0
	dbStr := "DriverName: " + driverName + " DataSourceName: " + dataSourceName
	log.Trace("* InitDB(): %s;*", dbStr)
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Initialize the DB connection
	err := rs.api.InitDB(driverName, dataSourceName, dialectID)
	if err != nil {
		errStr := "Error in establishing DB connection: " + err.Error()
		log.Trace(errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return errors.New(errStr)
	}
	log.Trace("Succesfully established the DB connection and created the required tables.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return nil
}

//RegisterSQLDialect adds/registers the new SQLDialect implementation on the database.
//dialect should be an implementation of db.SQLDialect interface.
//
//User should use this method when they want to provide the new implementation for the database
//other than provided by the API.
//
//Default implementation exist for: MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, Sybase.
//
//Returns error in case of failure in registering the Dialect.
func (rs *SnmpRequestServer) RegisterSQLDialect(dialectID int, dialect db.SQLDialect) error {
	return db.RegisterNewDialect(dialectID, dialect)
}

//SetDatabaseFlag enables/diables the usage of database for all the SNMP operations internally.
//If enabled DB will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
//
//Note: When Database usage is disabled in the middle, it may cause some adverse effect of re-synchronization to get the new values
//with all the SNMP entities and fill the local LCD.
func (rs *SnmpRequestServer) SetDatabaseFlag(flag bool) {
	rs.api.SetDatabaseFlag(flag)
}

//IsDatabaseFlag returns bool indicating whether database storage is enabled for SNMP operations.
//False indicates that the runtime memory (LCD) is used for storing/retriving V3 related data.
func (rs *SnmpRequestServer) IsDatabaseFlag() bool {
	return rs.api.IsDatabaseFlag()
}

//#################### SNMP GET ######################//

//Get sends SNMP Get request to the requestServer host and returns immediately after sending the request.
//This method will send Get request by setting the first OID from rs' ObjectID List.
//
//It returns RequestID/MsgID of the SnmpMessage sent (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) Get() (int32, error) {
	start := 0
	log.Trace("* Get() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		//log.Println("No OIDs set on SnmpRequestServer.")
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList[:1])
	reqID, err := rs.sendSNMPRequest(rs.oidList[:1], consts.GetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetOID sends SNMP Get request to the requestServer host by using oid as ObjectID and returns immediately after sending the request.
//oid should be a ObjectID of type snmpvar.SnmpOID/*snmpvar.SnmpOID/string/[]uint32.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if OID is of invalid type (or) any error in communication with the target host.
func (rs *SnmpRequestServer) GetOID(oid interface{}) (int32, error) {
	start := 0
	log.Trace("* GetOID() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	oidSlice := make([]snmpvar.SnmpOID, 2)
	switch snmpOid := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		}
	case snmpvar.SnmpOID:
		oidSlice[0] = snmpOid
	case *snmpvar.SnmpOID:
		oidSlice[0] = *snmpOid
	case []uint32:
		if snmpOID := snmpvar.NewSnmpOIDByInts(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		}
	default:
		//log.Println("Invalid OID Type")
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice[:1])
	reqID, err := rs.sendSNMPRequest(oidSlice[:1], consts.GetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetList sends SNMP Get request to the requestServer host and returns immediately after sending the request.
//This method will send Get request by setting all the OIDs from rs' ObjectID List.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if no OIDs set on rs (or) any error in communication with the requestServer host.
func (rs *SnmpRequestServer) GetList() (int32, error) {
	start := 0
	log.Trace("* GetOID() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList)

	reqID, err := rs.sendSNMPRequest(rs.oidList, consts.GetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetOIDList sends SNMP Get request to the requestServer host and returns immediately after sending the request.
//This method will send Get request by using 'oidList' as VarBinds.
//oidList should be a slice of OIDs of type string/SnmpOID.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error in the following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, Get operation will be perfomed on remaining Valid OIDs and error will be returned.
func (rs *SnmpRequestServer) GetOIDList(oidList interface{}) (int32, error) {
	start := 0
	log.Trace("* GetOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++
				//Invalid OID String
			}
		}
		//log.Println("Dropped count:",dropCount)
		if dropCount == len(argType) {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			rs.errorCode = Dropped_Invalid_Oid
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Too few OIDs dropped in Get operation as they are invalid: " + rs.errorString)
			retErr = errors.New(rs.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)

	reqID, err := rs.sendSNMPRequest(oidSlice, consts.GetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, retErr
}

//#################### SNMP GET-NEXT ######################//

//GetNext sends SNMP Get-Next request to the requestServer host and returns immediately after sending the request.
//This method will send Get-Next request by setting the first OID from rs' ObjectID List.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) GetNext() (int32, error) {
	start := 0
	log.Trace("* GetNext() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}
	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList[:1])

	reqID, err := rs.sendSNMPRequest(rs.oidList[:1], consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, err
}

//GetNextOID sends SNMP Get-Next request to the requestServer host by using oid as ObjectID and returns immediately after sending the request.
//oid should be an ObjectID of type SnmpOID/string/[]int.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error code and returns error if oid is of invalid type (or) any error in communication with the target host.
func (rs *SnmpRequestServer) GetNextOID(oid interface{}) (int32, error) {
	start := 0
	log.Trace("* GetNextOID() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	oidSlice := make([]snmpvar.SnmpOID, 2)
	switch snmpOid := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		}
	case snmpvar.SnmpOID:
		oidSlice[0] = snmpOid
	case *snmpvar.SnmpOID:
		oidSlice[0] = *snmpOid
	case []uint32:
		if snmpOID := snmpvar.NewSnmpOIDByInts(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		}
	default:
		//log.Println("Invalid OID Type")
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice[:1])

	reqID, err := rs.sendSNMPRequest(oidSlice[:1], consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetNextList sends SNMP Get-Next request to the requestServer host and returns immediately after sending the request.
//This method will send Get request by setting all the OIDs from rs' ObjectID List.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) GetNextList() (int32, error) {
	start := 0
	log.Trace("* GetNextList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList)

	reqID, err := rs.sendSNMPRequest(rs.oidList, consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetNextOIDList sends SNMP Get-Next request to the requestServer host and returns immediately after sending the request.
//This method will send Get-Next request by using 'oidList' as VarBinds.
//oidList should be a slice of OIDs of type string/SnmpOID.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error code and returns error in the following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, Get-Next operation will be perfomed on remaining Valid OIDs and error will be returned.
func (rs *SnmpRequestServer) GetNextOIDList(oidList interface{}) (int32, error) {
	start := 0
	log.Trace("* GetNextOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++
				//Invalid OID String
			}
		}
		//log.Println("Dropped OIDs count:",dropCount)
		if dropCount == len(argType) {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			rs.errorCode = Dropped_Invalid_Oid
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Too few OIDs dropped in Get operation as they are invalid: " + rs.errorString)
			retErr = errors.New(rs.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)

	reqID, err := rs.sendSNMPRequest(oidSlice, consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, retErr
}

//#################### SNMP SET ######################//

//Set sends SNMP Set request to the requestServer host and returns immediately after sending the request.
//Set is performed on first OID from rs' ObjectID List using varString and varType as SNMP Variable.
//varType should be of any valid datatype as per ASN.1 standard.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) Set(varString string, varType byte) (int32, error) {
	start := 0
	log.Trace("* Set() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	snmpVar, err := snmpvar.CreateSnmpVar(varType, varString)
	if err != nil {
		rs.errorCode = Error_Creating_Variable
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}

	reqID, err := rs.SetVariable(snmpVar)
	if err != nil {
		log.Trace("Returning.. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, err
}

//SetVariable sends SNMP Set request to the requestServer host and returns immediately after sending the request.
//Set is performed on the first OID from rs' ObjectID List using snmpVar.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) SetVariable(snmpVar snmpvar.SnmpVar) (int32, error) {
	start := 0
	log.Trace("* SetVariable() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	rs.varList = []snmpvar.SnmpVar{snmpVar} //Set this on VarList so that variable bindings will be constructed with value part

	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList[:1])

	reqID, err := rs.sendSNMPRequest(rs.oidList[:1], consts.SetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, err
}

//SetVariables sends SNMP Set request to the requestServer host and returns immediately after sending the request.
//Set is performed on all the OIDs set on rs' ObjectID list with 'varSlice' as SNMP Variables.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) SetVariables(varSlice []snmpvar.SnmpVar) (int32, error) {
	start := 0
	log.Trace("* SetVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	rs.varList = varSlice

	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList)

	reqID, err := rs.sendSNMPRequest(rs.oidList, consts.SetRequest)
	if err != nil {
		log.Trace("Returning.. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, err
}

//########## Trap Message ############//

//SendTrap sends v1 trap message to the requestServer host with the params specified and variable bindings using OIDs from rs' ObjectID List and the 'values' specified.
//Sets the relevant error-code and returns error if any.
//
//  Note:
//  * This method is used to send v1 trap message.
//  * In case Version2C/Version3 is set on this request server rs, v2 trap message will be constructed with appropriate changes and send to target host.
func (rs *SnmpRequestServer) SendTrap(enterprise, agentAddress string, genericType, specificType int, sysUpTime uint32, values []snmpvar.SnmpVar) error {
	start := 0
	log.Trace("* SendTrap() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Construct the trap message
	trapMsg := msg.NewSnmpMessage()
	rs.constructOutgoingMessage(&trapMsg) //Set the generic params on the SnmpMessage

	//Trap Message
	if trapMsg.Version() == Version1 {
		trapMsg.SetCommand(consts.TrapRequest)
		trapMsg.SetEnterprise(*snmpvar.NewSnmpOID(enterprise))
		trapMsg.SetAgentAddress(agentAddress)
		trapMsg.SetGenericType(genericType)
		trapMsg.SetSpecificType(specificType)
		trapMsg.SetUpTime(sysUpTime)
	} else if trapMsg.Version() == Version2C || trapMsg.Version() == Version3 {
		//Convert to V2Trap as version used in target is SNMPV2C/SNMPV3.
		//V2Trap is constructed as per RFC 3584 'Coexistence between Version 1, Version 2, and Version 3' - Section 3.
		trapMsg.SetCommand(consts.Trap2Request)
		//SysUpTime VarBind - RFC 3584 - Section 3.1, Step 1
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(sysUpTimeOID),
				snmpvar.NewSnmpTimeTicks(sysUpTime),
			),
		)
		//TrapOID VarBind - RFC 3584 - Section 3.1, Step 2 & 3
		if genericType >= 0 && genericType <= 5 { //Let's use the Generic Trap OID
			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					*snmpvar.NewSnmpOID(trapOID),
					snmpvar.NewSnmpOID(genericTraps[genericType]),
				),
			)
		} else if genericType == 6 {
			//Construct the TrapOID based on 'enterprise string' provided as per Coexistence standard.
			enterpriseIntArray := snmpvar.NewSnmpOID(enterprise).Value()
			trapOIDIntArray := make([]uint32, len(enterpriseIntArray)+2)
			copy(trapOIDIntArray, enterpriseIntArray)
			trapOIDIntArray[len(enterpriseIntArray)] = 0 //Two additional Sub-Identifiers
			trapOIDIntArray[len(enterpriseIntArray)+1] = uint32(specificType)

			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					*snmpvar.NewSnmpOID(trapOID),
					snmpvar.NewSnmpOIDByInts(trapOIDIntArray),
				),
			)
		} else {
			rs.errorCode = Invalid_Generic_Type
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return errors.New(rs.errorString)
		}
	}

	//Add the remaining varbinds - RFC 3584 - Section 3.1, Step 4
	if values != nil {
		for i, oid := range rs.oidList {
			if values[i] == nil {
				break
			}
			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					oid,
					values[i],
				),
			)
		}
	}

	//EnterpriseOID VarBind: For trap conversion from v1 to v2c/v3, enterprise OID is attached as last varbind.
	if trapMsg.Version() > Version1 {
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(enterpriseOID),
				snmpvar.NewSnmpOID(enterprise),
			),
		)
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", trapMsg.VarBinds())

	//We have constructed the trap message, so call the 'handler' method directly
	reqID, err := rs.handler.SendSNMPRequest(rs.session, trapMsg)
	if err != nil {
		log.Trace("Returning.. Error in sending Trap: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return err
	}

	log.Trace("Sent Trap request. RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return err
}

//SendV2Trap sends v2 trap message to the requestServer host with the params specified and variable bindings using OIDs from rs' ObjectID List and the 'values' specified.
//Sets the relevant error-code and returns error if any.
//
//  Note:
//  * This method is used to send v2 trap message.
//  * In case Version1 is set on this request server rs, v1 trap message will be constructed with appropriate changes and send to target host.
func (rs *SnmpRequestServer) SendV2Trap(sysUpTime uint32, trapOid snmpvar.SnmpOID, values []snmpvar.SnmpVar) error {
	start := 0
	log.Trace("* SendV2Trap() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}
	//Construct v2 trap message
	trapMsg := msg.NewSnmpMessage()
	rs.constructOutgoingMessage(&trapMsg) //Set the generic params on the SnmpMessage

	if rs.Version() < Version2C {
		//Convert V2Trap to V1Trap as per - RFC 3584 'Coexistence between Version 1, Version 2, and Version 3'
		trapMsg.SetCommand(consts.TrapRequest)

		//RFC 3584 - Section 3.2
		//Step 2
		trapMsg.SetAgentAddress(rs.localHost) //Entity sending this trap

		//Step 3
		genericType := 6
		for i, trap := range genericTraps {
			if strings.EqualFold(trapOid.String(), trap) {
				genericType = i
				break
			}
		}
		trapMsg.SetGenericType(genericType)

		//Step 4
		if genericType >= 0 && genericType <= 5 {
			trapMsg.SetSpecificType(0)
		} else {
			trapOIDIntArray := trapOid.Value()
			trapOIDLen := len(trapOIDIntArray)
			trapMsg.SetSpecificType(int(trapOIDIntArray[trapOIDLen-1]))

			//Set EnterpriseOID - Step 1
			if trapOIDIntArray[trapOIDLen-2] == 0 { //Netx-to-last Sub-Identifier
				trapMsg.SetEnterprise(*snmpvar.NewSnmpOIDByInts(trapOIDIntArray[:(trapOIDLen - 3)])) //Remove last two sub-identifiers
			} else {
				trapMsg.SetEnterprise(*snmpvar.NewSnmpOIDByInts(trapOIDIntArray[:(trapOIDLen - 2)])) //Remove last sub-identifier
			}
		}

		//Step 5
		trapMsg.SetUpTime(sysUpTime)

		//Step 6 - Add the remaining varbinds
		if values != nil {
			for i, oid := range rs.oidList {
				if values[i] == nil {
					break
				}
				if values[i].Type() == consts.Counter64 {
					errStr := "Failure in converting to V1 Trap. Invalid Variable Binding Type: Counter64"
					log.Trace("Returning.. " + errStr)
					if log.IsPerf() {
						end := currentTimeInMillis()
						log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
					}
					return errors.New(errStr)
				}
				trapMsg.AddVarBind(
					msg.NewSnmpVarBind(
						oid,
						values[i],
					),
				)
			}
		}
	} else {
		trapMsg.SetCommand(consts.Trap2Request)
		//Add the variable bindings for v2 traps
		//SysUptime VarBind
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(sysUpTimeOID),
				snmpvar.NewSnmpTimeTicks(sysUpTime),
			),
		)

		//TrapOID VarBind
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(trapOID),
				trapOid,
			),
		)

		//Add the remaining varbinds
		if values != nil {
			for i, oid := range rs.oidList {
				if values[i] == nil {
					break
				}
				trapMsg.AddVarBind(
					msg.NewSnmpVarBind(
						oid,
						values[i],
					),
				)
			}
		}
	}

	//For V1Trap, Set the EnterpriseOID - RFC 3584 - Section 3.2 Step 1
	if (trapMsg.Version() == Version1) &&
		(trapMsg.GenericType() >= 0 && trapMsg.GenericType() <= 5) {
		found := false
		for _, varb := range trapMsg.VarBinds() {
			if strings.EqualFold(varb.ObjectID().String(), enterpriseOID) {
				trapMsg.SetEnterprise(varb.Variable().(snmpvar.SnmpOID))
				found = true
				break
			}
		}
		if !found {
			trapMsg.SetEnterprise(*snmpvar.NewSnmpOID(snmpTraps))
		}
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", trapMsg.VarBinds())

	//We have constructed the trap message, so call the 'request_handler' method directly
	reqID, err := rs.handler.SendSNMPRequest(rs.session, trapMsg)
	if err != nil {
		log.Trace("Returning.. Error in sending Trap: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return err
	}

	log.Trace("Sent Trap request. RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return err
}

//############## Inform Request #############//

//SendInform sends inform request to requestServer host with specified params and variable bindings using OIDs from rs' ObjectID List and 'variables' specified.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error code and returns error in the following cases:
//
//	* Version is not Version2C/Version3
//	* No OIDs set on rs
//	* Error in communication with the target host
func (rs *SnmpRequestServer) SendInform(upTime uint32, trapOid string, variables []snmpvar.SnmpVar) (int32, error) {
	start := 0
	log.Trace("* SendInform() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}
	if rs.Version() < Version2C {
		rs.errorCode = Invalid_Version
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	informMsg := msg.NewSnmpMessage()
	informMsg.SetCommand(consts.InformRequest)
	rs.constructOutgoingMessage(&informMsg)

	//SysUptime OID
	informMsg.AddVarBind(
		msg.NewSnmpVarBind(
			*snmpvar.NewSnmpOID(sysUpTimeOID),
			snmpvar.NewSnmpTimeTicks(upTime),
		),
	)
	//Trap OID
	informMsg.AddVarBind(
		msg.NewSnmpVarBind(
			*snmpvar.NewSnmpOID(trapOID),
			snmpvar.NewSnmpOID(trapOid), //Check the OID using MibOps later
		),
	)
	//Add the remaining list of VarBinds
	if variables != nil {
		for i, oid := range rs.oidList {
			if variables[i] == nil {
				break
			}
			informMsg.AddVarBind(
				msg.NewSnmpVarBind(
					oid,
					variables[i],
				),
			)
		}
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", informMsg.VarBinds())

	reqID, err := rs.handler.SendSNMPRequest(rs.session, informMsg)
	if err != nil {
		log.Trace("Returning.. Error in sending Inform request: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}
	//Expects response. Hence store it.
	rs.requests[reqID] = &informMsg

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, err
}

//############# SNMP Get-Bulk ###############//

//GetBulk sends SNMP Get-Bulk request to the requestServer host and returns immediately after sending the request.
//Get-Bulk request is made on the OIDs from rs's ObjectID List with non-repeaters and max-repetitions set on rs.
//
//It returns RequestID/MsgID of the SnmpMessage (based on version) and error if any.
//
//Sets the relevant error-code and returns error, if no OIDs set on rs (or) any error in communication with the target host.
func (rs *SnmpRequestServer) GetBulk() (int32, error) {
	start := 0
	log.Trace("* GetBulk() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(rs.oidList) == 0 {
		rs.errorCode = Oid_Not_Specified
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	if rs.Version() < Version2C {
		rs.errorCode = Invalid_Version
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		log.Trace("Returning.. " + rs.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, errors.New(rs.errorString)
	}
	if rs.nonRepeaters > int32(len(rs.oidList)) {
		rs.nonRepeaters = int32(len(rs.oidList)) // To make sure that non-repeaters value is less than the number of OIDs
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", rs.oidList)

	reqID, err := rs.sendSNMPRequest(rs.oidList, consts.GetBulkRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, nil
}

//GetBulkOIDList sends SNMP Get-Bulk request to the requestServer host with specified nonReps, maxReps and Variable Bindings constructed using 'oidList'.
//
//oidList should be a slice of OIDs of type string/SnmpOID.
//
//Sets the relevant error-code and returns error in the following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, Get-Bulk operation will be performed on remaining Valid OIDs and error will be returned.
func (rs *SnmpRequestServer) GetBulkOIDList(oidList interface{}, nonReps, maxReps int32) (int32, error) {
	start := 0
	log.Trace("* GetBulkOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++
				//Invalid OID String
			}
		}
		//log.Println("Dropped OIDs count:",dropCount)
		if dropCount == len(argType) {
			rs.errorCode = Oid_Not_Specified
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Returning.. " + rs.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, errors.New(rs.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			rs.errorCode = Dropped_Invalid_Oid
			rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
			log.Trace("Too few OIDs dropped in Get operation as they are invalid: " + rs.errorString)
			retErr = errors.New(rs.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		rs.errorCode = Illegal_Argument
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		return -1, errors.New(rs.errorString)
	}
	if rs.Version() < Version2C {
		rs.errorCode = Invalid_Version
		rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
		return -1, errors.New(rs.errorString)
	}
	if nonReps > int32(len(oidSlice)) {
		nonReps = int32(len(oidSlice)) // To make sure that non-repeaters value is less than number of OIDs
	}
	rs.SetNonRepeaters(nonReps)
	rs.SetMaxRepetitions(maxReps)

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)

	reqID, err := rs.sendSNMPRequest(oidSlice, consts.GetBulkRequest)
	if err != nil {
		log.Trace("Returning.. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return -1, err
	}

	log.Trace("Returning RequestID: %d", reqID)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return reqID, retErr
}

//ReleaseResources helps in releasing the resources used by rs immediately.
//
//Closes the SnmpSession and SnmpAPI if number of requestservers sharing it becomes zero.
//Otherwise decrement the requestserver's count for that SnmpSession and SnmpAPI.
func (rs *SnmpRequestServer) ReleaseResources() {
	if rs.session == nil {
		log.Warn("Failure in releasing resources. No SnmpSession is created on this SnmpRequestServer rs.")
		return
	}
	if !rs.session.IsSessionOpen() {
		log.Info("Session is already closed. Returning..")
		return
	}
	rs.mut.Lock()
	defer rs.mut.Unlock()

	//Close SnmpAPI
	if val, ok := rs.snmpStore["API"]["API"]; ok {
		if val.concurrentUsers == 1 {
			delete(rs.snmpStore["API"], "API")
			if rs.api != nil {
				rs.api.Close()
			}
		} else {
			rs.snmpStore["API"]["API"].concurrentUsers--
		}
		rs.api = nil
	}
	//Close SnmpSession
	if val, ok := rs.snmpStore["SESSION"][rs.sessionName]; ok {
		if val.concurrentUsers == 1 {
			delete(rs.snmpStore["SESSION"], rs.sessionName)
			if rs.session != nil {
				rs.session.Close()
			}
		} else {
			rs.snmpStore["SESSION"][rs.sessionName].concurrentUsers--
			rs.session.RemoveSnmpClient(rs)
		}
		rs.session = nil
	}
	rs.RemoveAllListeners()
	log.Debug("Successfully released the underlying resources of SnmpRequestServer rs.")
}

//########### Unexported Methods - Utilities #########//

func (rs *SnmpRequestServer) sendSNMPRequest(oids []snmpvar.SnmpOID, command consts.Command) (int32, error) {

	/*rs.errorCode = No_Err_Registered
	rs.errorString = rs.errorMsgs.ErrorString(rs.errorCode)
	rs.errorIndex = 0
	rs.exceptionCode = -1
	*/

	//Call SendSNMPRequest
	reqMsg := msg.NewSnmpMessage()
	reqMsg.SetCommand(command)
	rs.constructOutgoingMessage(&reqMsg)
	for _, v := range oids {
		reqMsg.AddNull(v)
	}
	//Add the value portion for Variable Bindings for Set/Trap operations
	if command == consts.SetRequest ||
		command == consts.TrapRequest ||
		command == consts.Trap2Request {
		for i, _ := range reqMsg.VarBinds() { //Add the SnmpVar to the VariableBinding
			if rs.varList[i] != nil {
				reqMsg.VarBinds()[i].SetVariable(rs.varList[i])
			} else {
				break //Break if there is no more SnmpVar to add
			}
		}
	}

	reqId, err := rs.handler.SendSNMPRequest(rs.session, reqMsg)
	if err == nil { //Request is successful.
		rs.requests[reqId] = &reqMsg
	}

	return reqId, err
}

//Set the basic and generic params on the outgoing SnmpMessage
func (rs SnmpRequestServer) constructOutgoingMessage(msg *msg.SnmpMessage) {
	msg.SetVersion(rs.version)
	msg.SetCommunity(rs.community)

	if (rs.protocolOptions != nil) &&
		(rs.protocolOptions.RemotePort() >= 0) &&
		(rs.protocolOptions.RemoteHost() != "") {
		msg.SetProtocolOptions(rs.protocolOptions)
	} else {
		//Set the default UDP protocoloptions
		udp := snmp.NewUDPProtocolOptions()
		udp.SetRemoteHost(rs.targetHost)
		udp.SetRemotePort(rs.targetPort)
		msg.SetProtocolOptions(udp)
	}

	//Add Timeout, retries
	if rs.timeout > 0 {
		msg.SetTimeout(rs.timeout)
	}
	if rs.retries > 0 {
		msg.SetRetries(rs.retries)
	}

	//For SnmpV3
	if rs.version == Version3 {
		msg.SetUserName(rs.principal)
		msg.SetContextName(rs.contextName)
		msg.SetContextEngineID(rs.contextEngineID)
		msg.SetMsgMaxSize(rs.msgMaxSize)
		msg.SetSecurityLevel(rs.getSecurityLevel())
		msg.SetMsgSecurityModel(rs.secModelID)
	}

	//For Get-Bulk request
	if msg.Command() == consts.GetBulkRequest {
		msg.SetNonRepeaters(rs.nonRepeaters)
		msg.SetMaxRepetitions(rs.maxRepetitions)
	}
}

func (rs SnmpRequestServer) getSessionName() string {
	var sessionName string
	var port int
	//Construct session name with localport if user wants to bind to particular localport
	if rs.protocolOptions != nil {
		port = rs.protocolOptions.LocalPort()
	} else {
		port = rs.localPort
	}
	if rs.sessionName != "" && port != 0 {
		sessionName = "SESSION:" + rs.sessionName + ":" + strconv.Itoa(port)
	} else if rs.sessionName != "" {
		sessionName = "SESSION:" + rs.sessionName
	} else if port != 0 {
		sessionName = "SESSION:" + strconv.Itoa(port)
	} else {
		sessionName = "SESSION"
	}

	return sessionName
}

//setCreateV3ErrorCode sets the errorCode in request server for CreateV3Tables method
func (rs *SnmpRequestServer) setCreateV3ErrorCode(errVal int) {
	switch errVal {
	case consts.UnSupportedSecurityLevelError:
		rs.errorCode = UnSupported_SecurityLevel_Error
	case consts.NotInTimeWindowsError:
		rs.errorCode = Not_In_Time_Windows_Error
	case consts.UnknownUserNameError:
		rs.errorCode = Unknown_Usernames_Error
	case consts.UnknownEngineIDError:
		rs.errorCode = Unknown_EngineID_Error
	case consts.WrongDigestsError:
		rs.errorCode = Wrong_Digest_Error
	case consts.DecryptionError:
		rs.errorCode = Decrypt_Error
	default:
		rs.errorCode = Time_Sync_Failed
	}
}

func (rs *SnmpRequestServer) getSecurityLevel() consts.SecurityLevel {
	var securityLevel byte = 0

	if rs.privProtocol != NO_PRIV && !strings.EqualFold(rs.privPassword, "") {
		if rs.authProtocol != NO_AUTH && !strings.EqualFold(rs.authPassword, "") {
			securityLevel = 3
		} else {
			securityLevel = 0
		}
	} else {
		if rs.authProtocol != NO_AUTH && !strings.EqualFold(rs.authPassword, "") {
			securityLevel = 1
		} else {
			securityLevel = 0
		}
	}

	return consts.SecurityLevel(securityLevel)
}
