/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

// This example demonstrates multi varbind Get request sent using SnmpTarget.
package hl

import (
	"fmt"
)

var (
	target SnmpTarget
)

// This example sends get request with multi varbinds (1.4.0/1.5.0)
// to remote server 161 and prints the response varbinds.
func ExampleSnmpTarget_GetList() {
	//Create a new SnmpTarget instance
	target = NewSnmpTarget()

	target.SetVersion(Version1)
	target.SetCommunity("public")

	target.SetTargetHost("localhost")
	target.SetTargetPort(8001)
	target.SetRetries(1)
	target.SetTimeout(10) //10 Seconds Timeout
	target.SetOIDList([]string{"1.4.0", "1.5.0"})

	defer target.ReleaseResources() //Release the underlying low-level resources in any case.

	if result, err := target.GetList(); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Response received. Values:")
		for _, val := range result {
			fmt.Println(val)
		}
	}
}
