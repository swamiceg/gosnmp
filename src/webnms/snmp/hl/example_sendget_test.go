/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

// This example demonstrates multi varbind Get request sent using SnmpRequestServer.
package hl

import (
	"fmt"
	"os"
	"webnms/snmp/msg"
)

//Implement Listener interface to receive response message asynchronously
type receiver int

func (r receiver) ReceiveMessage(resp *msg.SnmpMessage) {
	fmt.Println("Response received. Values:")
	if resp != nil {
		resp.PrintVarBinds()
	}
}

// This example sends get request with multi varbinds (1.4.0/1.5.0)
// to remote server 161 and returns immediately.
// Response message received and varbinds are printed in 'ReceiveMessage()' method asynchronously.
func ExampleSnmpRequestServer_GetList() {
	//Create new SnmpRequestServer instance
	reqServer := NewSnmpRequestServer()

	reqServer.SetVersion(Version1)
	reqServer.SetCommunity("public")

	reqServer.SetTargetHost("localhost")
	reqServer.SetTargetPort(161)
	reqServer.SetRetries(1)
	reqServer.SetTimeout(10) //10 Seconds Timeout
	reqServer.SetOIDList([]string{"1.4.0", "1.5.0"})

	defer reqServer.ReleaseResources() //Release the underlying low-level resources in any case.

	reqServer.AddListener(new(receiver))

	_, err := reqServer.GetList()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
