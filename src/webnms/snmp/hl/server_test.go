/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
	"time"

	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

func TestRequestServer(t *testing.T) {

	conf := Configuration()

	//test for NewSnmpRequestServer
	server := NewSnmpRequestServer()
	server.SetDebug(false)
	if server != nil {
		setServerConf(server, conf)
		_, err := server.GetList()
		if err != nil {
			t.Error("Error occured for valid values, err :", err)
		}
	} else {
		t.Error("RequestServer should not be nil")
	}
	server.ReleaseResources()

	//test for NewSnmpRequestServerBySession
	sessionServer := NewSnmpRequestServerBySession("Session1", 7001)
	if sessionServer != nil {
		setServerConf(sessionServer, conf)
		_, err := sessionServer.GetList()
		if err != nil {
			t.Error("Error occured for valid values :", err)
		}
	} else {
		t.Error("Session RequestServer should not be nil")
	}
	sessionServer.ReleaseResources()

	//test for NewSnmpRequestServerByProtocolOptions
	proOptServer := NewSnmpRequestServerByProtocolOptions(snmp.NewUDPProtocolOptions(), nil)
	if proOptServer != nil {
		setServerConf(proOptServer, conf)
		_, err := proOptServer.GetList()
		if err != nil {
			t.Error("Error occured for valid values :", err)
		}
	} else {
		t.Error("protocol options server should not be nil")
	}
	proOptServer.ReleaseResources()
}

func TestServerObjectID(t *testing.T) {

	conf := Configuration()

	validHosts := []string{"localhost", "127.0.0.1"}

	//tests for SetTargetHost with valid values
	for _, validHost := range validHosts {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.SetTargetHost(validHost)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid host,", server.TargetHost())
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	invalidHosts := []string{"invalidHost", "1.1.1.1.1"}

	//tests for SetTargetHost with invalid values
	for _, invalidHost := range invalidHosts {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.SetTargetHost(invalidHost)
			_, err := server.GetList()
			if err == nil {
				t.Error("Error should be thrown for invalid host,", server.TargetHost())
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	validOidLists := []interface{}{[]string{"1.3.0", "1.4.0"}, []snmpvar.SnmpOID{*snmpvar.NewSnmpOID("1.5.0"), *snmpvar.NewSnmpOID("1.6.0")}}
	invalidOidLists := []interface{}{[]string{"invalidOid1", "invalidOid2"}, []int{5, 6}, []string{}}
	validOids := []interface{}{"1.1.0", *snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpOID("1.1.0")}
	invalidOids := []interface{}{5, "invalidOid"}
	validIndex := 1
	invalidIndices := []int{-3, 10}

	//tests for SetOIDList with valid values
	for _, validOidList := range validOidLists {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.SetOIDList(validOidList)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", server.TargetHost())
			} else {
				if server.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", server.OIDList(), server.ErrorCode())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for SetOIDList with invalid values
	for _, invalidOidList := range invalidOidLists {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.SetOIDList(invalidOidList)
			_, err := server.GetList()
			if err == nil {
				t.Error("Error should be thrown for invalid oidlist,", server.OIDList())
			} else {
				if server.ErrorCode() != Oid_Not_Specified {
					t.Error("ErrorCode", Oid_Not_Specified, "should be set for invalid oidList,", server.OIDList())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOID with valid values
	for _, validOid := range validOids {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOID(validOid)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", server.OIDList())
			} else {
				if server.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", server.OIDList(), server.ErrorCode())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOID with invalid values
	for _, invalidOid := range invalidOids {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOID(invalidOid)
			if len(server.OIDList()) > 2 {
				t.Error("Invalid OID should not be added,", invalidOid)
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOIDs with valid values
	for _, validOidList := range validOidLists {
		server := NewSnmpRequestServer()

		if server != nil {
			setServerConf(server, conf)
			server.AddOIDs(validOidList)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", server.TargetHost())
			} else {
				if server.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", server.OIDList())
				} else if len(server.OIDList()) != 4 {
					t.Error("New OIDs not added to already existing OIDList", server.OIDList())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}

		server.ReleaseResources()
	}

	//tests for AddOIDs with invalid values
	for _, invalidOidList := range invalidOidLists {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOIDs(invalidOidList)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should be not thrown for invalid oidlist appended (original OIDs will already be present),", server.OIDList())
			} else {
				if len(server.OIDList()) != 2 {
					t.Error("Invalid OIds", invalidOidList, "should not be added to the existing OIDList", server.OIDList())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOIDAt with valid values
	for _, validOid := range validOids {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOIDAt(validIndex, validOid)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", server.OIDList())
			} else {
				if server.ErrorCode() != 0 || !strings.EqualFold(server.OIDAt(validIndex), ".1.3.6.1.2.1.1.1.0") {
					t.Error("ErrorCode should not be set for valid oidList,", server.OIDList(), server.ErrorCode())
				}
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOIDAt with invalid values
	for _, invalidOid := range invalidOids {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOIDAt(1, invalidOid)
			if len(server.OIDList()) > 2 {
				t.Error("Invalid OID should not be added,", invalidOid)
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for AddOIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.AddOIDAt(invalidIndex, "1.7.0")
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for invalid index,", invalidIndex)
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for RemoveOIDAt with valid values
	server := NewSnmpRequestServer()
	if server != nil {
		setServerConf(server, conf)
		server.RemoveOIDAt(validIndex)
		_, err := server.GetList()
		if err != nil {
			t.Error("Error should not be thrown for valid index,", validIndex)
		} else {
			if server.ErrorCode() != 0 {
				t.Error("OID should be removed for valid index,", validIndex)
			}
		}
	} else {
		t.Error("RequestServer should not be nil")
	}
	server.ReleaseResources()

	//tests for RemoveOIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			server.RemoveOIDAt(invalidIndex)
			_, err := server.GetList()
			if err != nil {
				t.Error("Error should not be thrown for invalid index,", invalidIndex)
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}

	//tests for OIDAt with valid values
	server1 := NewSnmpRequestServer()
	if server1 != nil {
		setServerConf(server1, conf)
		if !strings.EqualFold(server1.OIDAt(validIndex), ".1.3.6.1.2.1.1.5.0") {
			t.Error("Expected .1.3.6.1.2.1.1.5.0, instead got,", server1.OIDAt(validIndex))
		}
	} else {
		t.Error("RequestServer should not be nil")
	}
	server1.ReleaseResources()

	//tests for OIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		server := NewSnmpRequestServer()
		if server != nil {
			setServerConf(server, conf)
			if !strings.EqualFold(server.OIDAt(invalidIndex), "<nil>") {
				t.Error("Expected <nil>, instead got,", server1.OIDAt(invalidIndex))
			}
		} else {
			t.Error("RequestServer should not be nil")
		}
		server.ReleaseResources()
	}
}

func TestServerFields(t *testing.T) {

	//tests for SetVersion
	versions := []consts.Version{consts.Version1, consts.Version2C, consts.Version3}
	invalidVersion := consts.Version(5)

	server := NewSnmpRequestServer()
	if server.Version() != Version1 {
		t.Error("Default server version should be", Version1)
	}
	for _, version := range versions {
		server.SetVersion(version)
		if server.Version() != version {
			t.Error("Expecting", version, "but got,", server.Version())
		}
	}
	server.SetVersion(invalidVersion)
	if server.Version() == invalidVersion {
		t.Error(invalidVersion, "cannot be set to server")
	}

	//tests for SetCommunity
	if !strings.EqualFold(server.Community(), "public") {
		t.Error("Default server community should be public")
	}
	server.SetCommunity("private")
	if !strings.EqualFold(server.Community(), "private") {
		t.Error("Expecting private but got,", server.Community())
	}

	//tests for Debug method
	if server.Debug() {
		t.Error("Debug should not be set by default")
	}
	server.SetDebug(true)
	if !server.Debug() {
		t.Error("Debug should be set if true is passed to SetDebug")
	}
	server.SetDebug(false)

	//tests for TargetPort method
	if server.TargetPort() != 161 {
		t.Error("Expected default value for server port is 161, but got,", server.TargetPort())
	}
	server.SetTargetPort(9001)
	if server.TargetPort() != 9001 {
		t.Error("Expected server port to be 9001, but got,", server.TargetPort())
	}

	invalidPorts := []int{-161, 70000}

	for _, invalidPort := range invalidPorts {
		server.SetTargetPort(invalidPort)
		if server.TargetPort() == invalidPort {
			t.Error("Invalid port", invalidPort, "should not be set as targetPort")
		}
	}

	//tests for Timeout field
	if server.Timeout() != 5 {
		t.Error("Expected default value for timeout is 5, but got,", server.Timeout())
	}
	server.SetTimeout(2)
	if server.Timeout() != 2 {
		t.Error("Expected server timeout to be 2, but got,", server.Timeout())
	}

	invalidTimeouts := []int{-4, 0}
	for _, invalidTimeout := range invalidTimeouts {
		if server.Timeout() == invalidTimeout {
			t.Error("Invalid timeout value", invalidTimeout, "should not be set as server timeout")
		}
	}

	//tests for retry field
	if server.Retries() != 0 {
		t.Error("Expected default value for retries is 0, but got,", server.Retries())
	}
	server.SetRetries(2)
	if server.Retries() != 2 {
		t.Error("Expected server retries to be 2, but got,", server.Retries())
	}

	invalidRetries := []int{-4}
	for _, invalidRetry := range invalidRetries {
		if server.Retries() == invalidRetry {
			t.Error("Invalid retries value", invalidRetry, "should not be set as server retries")
		}
	}

	//tests for Non-Repeaters field
	if server.NonRepeaters() != 0 {
		t.Error("Expected default value for non-repeaters is 0, but got,", server.NonRepeaters())
	}
	server.SetNonRepeaters(2)
	if server.NonRepeaters() != 2 {
		t.Error("Expected server non-repeaters to be 2, but got,", server.NonRepeaters())
	}

	invalidNonReps := []int32{-4}
	for _, invalidNonRep := range invalidNonReps {
		if server.NonRepeaters() == invalidNonRep {
			t.Error("Invalid non-repeaters value", invalidNonRep, "should not be set as server non-repeaters")
		}
	}

	//tests for Max-Repetitions field
	if server.MaxRepetitions() != 50 {
		t.Error("Expected default value for Max-Repetitions is 50, but got,", server.MaxRepetitions())
	}
	server.SetMaxRepetitions(2)
	if server.MaxRepetitions() != 2 {
		t.Error("Expected server Max-Repetitions to be 2, but got,", server.MaxRepetitions())
	}

	invalidMaxReps := []int32{-4}
	for _, invalidMaxRep := range invalidMaxReps {
		if server.MaxRepetitions() == invalidMaxRep {
			t.Error("Invalid Max-Repetitions value", invalidMaxRep, "should not be set as server max-repetitions")
		}
	}

	//tests for MsgMaxSize field
	if server.MsgMaxSize() != 484 {
		t.Error("Expected default value for MsgMaxSize is 484, but got,", server.MsgMaxSize())
	}
	server.SetMsgMaxSize(5000)
	if server.MsgMaxSize() != 5000 {
		t.Error("Expected server MsgMaxSize to be 5000, but got,", server.MsgMaxSize())
	}

	invalidMaxSizes := []int32{-4, 0, 100}
	for _, invalidMaxSize := range invalidMaxSizes {
		if server.MsgMaxSize() == invalidMaxSize {
			t.Error("Invalid MsgMaxSize value", invalidMaxSize, "should not be set as server MsgMaxSize")
		}
	}

	server.ReleaseResources()
}

func TestServerV3Fields(t *testing.T) {

	server := NewSnmpRequestServer()

	//tests for principal field
	if !strings.EqualFold(server.Principal(), "") {
		t.Error("Expected default principal is empty string, instead it is,", server.Principal())
	}
	server.SetPrincipal("noAuthUser")
	if !strings.EqualFold(server.Principal(), "noAuthUser") {
		t.Error("Expected server principal is noAuthUser, but got,", server.Principal())
	}

	//tests for AuthProtocol field
	if server.AuthProtocol() != NO_AUTH {
		t.Error("Expected default authProtocol is", NO_AUTH, "instead it is,", server.AuthProtocol())
	}
	server.SetAuthProtocol(MD5_AUTH)
	if server.AuthProtocol() != MD5_AUTH {
		t.Error("Expected server authProtocol is", MD5_AUTH, "but got,", server.AuthProtocol())
	}
	server.SetAuthProtocol(consts.AuthProtocol("invalid"))
	if server.AuthProtocol() == consts.AuthProtocol("invalid") {
		t.Error("AuthProtocol other than NO_AUTH, MD5_AUTH && SHA_AUTH should not be set")
	}

	//tests for authPassword field
	if !strings.EqualFold(server.AuthPassword(), "") {
		t.Error("Expected default authPassword is empty string, instead it is,", server.AuthPassword())
	}
	server.SetAuthPassword("authUser")
	if !strings.EqualFold(server.AuthPassword(), "authUser") {
		t.Error("Expected server authPassword is authUser, but got,", server.AuthPassword())
	}

	//tests for PrivProtocol field
	if server.PrivProtocol() != NO_PRIV {
		t.Error("Expected default privProtocol is", NO_PRIV, "instead it is,", server.PrivProtocol())
	}
	server.SetPrivProtocol(DES_PRIV)
	if server.PrivProtocol() != DES_PRIV {
		t.Error("Expected server privProtocol is", DES_PRIV, "but got,", server.PrivProtocol())
	}
	server.SetPrivProtocol(consts.PrivProtocol("invalid"))
	if server.PrivProtocol() == consts.PrivProtocol("invalid") {
		t.Error("PrivProtocol other than NO_PRIV, DES_PRIV, TRIPLE_DES_PRIV, AES_128_PRIV, AES_192_PRIV && AES_256_PRIV should not be set")
	}

	//tests for privPassword field
	if !strings.EqualFold(server.PrivPassword(), "") {
		t.Error("Expected default privPassword is empty string, instead it is,", server.PrivPassword())
	}
	server.SetPrivPassword("privUser")
	if !strings.EqualFold(server.PrivPassword(), "privUser") {
		t.Error("Expected server privPassword is privUser, but got,", server.PrivPassword())
	}

	//tests for ContextName field
	if !strings.EqualFold(server.ContextName(), "") {
		t.Error("Expected default contextName is empty string, instead it is,", server.ContextName())
	}
	server.SetContextName("priv")
	if !strings.EqualFold(server.ContextName(), "priv") {
		t.Error("Expected server contextName is priv, but got,", server.ContextName())
	}

	//tests for contextEngineID field
	if !bytes.EqualFold(server.ContextEngineID(), []byte{}) {
		t.Error("Expected default contextEngineID is [], instead it is,", server.ContextEngineID())
	}
	server.SetContextEngineID([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0})
	if !bytes.EqualFold(server.ContextEngineID(), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) {
		t.Error("Expected server contextEngineID is [1 2 3 4 5 6 7 8 9 0], but got,", server.ContextEngineID())
	}

	//tests for ValidateUser field
	if server.IsValidateUser() {
		t.Error("Expected ValidateUser not to be set by default")
	}
	server.ValidateUser(true)
	if !server.IsValidateUser() {
		t.Error("Expected ValidateUser to be set to true")
	}
}

func TestServerV3Utils(t *testing.T) {

	server := NewSnmpRequestServer()
	conf := Configuration()
	setServerConf(server, conf)
	server.SetDebug(false)
	server.USMUserLCD().RemoveAllUsers()

	//tests for CreateV3Tables method
	server.SetVersion(Version3)
	out, err := server.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, error :", err)
	}

	//test for CreateV3Tables if entry already present
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -3 {
			t.Error("CreateV3Tables should return -3 if UserEntry already present, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if UserEntry already present")
	}

	//test for createV3Tables for user of security level AUTH_NO_PRIV
	server.SetPrincipal("authUser")
	server.SetAuthProtocol(MD5_AUTH)
	server.SetAuthPassword("authUser")
	server.SetContextName("auth")
	out, err = server.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, err :", err)
	}

	//test for createV3Tables for user of security level AUTH_PRIV

	//Invalid auth protocol
	server.SetPrincipal("privUser")
	server.SetAuthProtocol(SHA_AUTH)
	server.SetAuthPassword("privUser")
	server.SetPrivProtocol(DES_PRIV)
	server.SetPrivPassword("privUser")
	server.SetContextName("priv")
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect auth protocol is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect auth protocol is set")
	}

	//invalid auth password case
	server.SetAuthProtocol(MD5_AUTH)
	server.SetAuthPassword("invalidPassword")
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect auth password is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect auth password is set")
	}

	//invalid priv protocol case
	server.SetAuthPassword("privUser")
	server.SetPrivProtocol(AES_128_PRIV)
	server.ValidateUser(true)
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect priv protocol is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect priv protocol is set")
	}

	//invalid priv password case
	server.SetPrivProtocol(DES_PRIV)
	server.SetPrivPassword("invalidPassword")
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect priv password is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect priv password is set")
	}

	//valid values case for priv user
	server.SetPrivPassword("privUser")
	out, err = server.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, err :", err)
	}

	//invalid version case
	server.SetVersion(Version1)
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != 0 {
			t.Error("CreateV3Tables should return 0 for invalid version (version other than Version3), instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid version (version other than Version3) is set")
	}

	//invalid username case
	server.SetVersion(Version3)
	server.SetPrincipal("invalidUser")
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if invalid username is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid username is set")
	}

	server.SetPrincipal("privUser")
	server.SetTargetPort(9001)
	fmt.Println("Testing CreateV3Tables for discovery failed case. Wait for 5 seconds until request timeouts")
	out, err = server.CreateV3Tables()
	if err != nil {
		if out != -1 {
			t.Error("CreateV3Tables should return -1 if invalid host/port is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid host/port is set")
	}

	server.ReleaseResources()

	//tests for ManageV3Tables method
	server1 := NewSnmpRequestServer()
	//conf := Configuration()
	setServerConf(server1, conf)

	//tests for ManageV3Tables method
	server1.SetVersion(Version3)
	if err = server1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should return not return error for valid values, instead it returned,", err)
	}

	//test for ManageV3Tables for user of security level AUTH_NO_PRIV
	server1.SetPrincipal("authUser")
	server1.SetAuthProtocol(MD5_AUTH)
	server1.SetAuthPassword("authUser")
	server1.SetContextName("auth")
	if err = server1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should return not return error for valid values")
	}

	//invalid auth protocol
	server1.SetPrincipal("privUser")
	server1.SetAuthProtocol(SHA_AUTH)
	server1.SetAuthPassword("privUser")
	server1.SetPrivProtocol(DES_PRIV)
	server1.SetPrivPassword("privUser")
	server1.SetContextName("priv")
	if err = server1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect auth protocol is set")
	}

	//invalid auth password case
	server1.SetAuthProtocol(MD5_AUTH)
	server1.SetAuthPassword("invalidPassword")
	if err = server.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect auth password is set")
	}

	//invalid priv protocol case
	server1.SetAuthPassword("privUser")
	server1.SetPrivProtocol(AES_128_PRIV)
	server1.ValidateUser(true)
	if err = server1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect priv protocol is set")
	}

	//invalid priv password case
	server1.SetPrivProtocol(DES_PRIV)
	server1.SetPrivPassword("invalidPassword")
	if err = server.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect priv password is set")
	}

	//valid values case for priv user
	server1.SetPrivPassword("privUser")
	if err = server1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should not return error for valid values, err :", err)
	}

	//invalid version case
	server1.SetVersion(Version1)
	if err = server1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error for invalid version (version other than Version3)")
	}

	//invalid username case
	server1.SetVersion(Version3)
	server1.SetPrincipal("invalidUser")
	if err = server1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if invalid username is set")
	}

	server1.SetPrincipal("privUser")
	server1.SetTargetPort(9001)
	fmt.Println("Testing ManageV3Tables for discovery failed case. Wait for 5 seconds until request timeouts")
	err = server1.ManageV3Tables()
	if err == nil {
		t.Error("ManageV3Tables should return error if invalid host/port is set")
	}

	server.ReleaseResources()
}

func TestServerRequests(t *testing.T) {

	server := NewSnmpRequestServer()
	conf := Configuration()
	setServerConf(server, conf)

	//#################### SNMP GET TESTS ######################//
	fmt.Println("Testing for various get requests started")
	if _, err := server.Get(); err != nil {
		t.Error("Get should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetOID("1.3.0"); err != nil {
		t.Error("GetOID should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetList(); err != nil {
		t.Error("GetList should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetOIDList([]string{"1.3.0", "1.4.0"}); err != nil {
		t.Error("GetOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP GET-NEXT TESTS ######################//
	fmt.Println("Testing for various get-next requests started")
	if _, err := server.GetNext(); err != nil {
		t.Error("GetNext should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetNextOID("1.3.0"); err != nil {
		t.Error("GetNextOID should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetNextList(); err != nil {
		t.Error("GetNextList should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetNextOIDList([]string{"1.3.0", "1.4.0"}); err != nil {
		t.Error("GetNextOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP SET TESTS ######################//
	fmt.Println("Testing for various set requests started")

	varSlice := []snmpvar.SnmpVar{snmpvar.NewSnmpString("setTest"), snmpvar.NewSnmpString("setTest1")}

	if _, err := server.Set("1.4.0", consts.OctetString); err != nil {
		t.Error("Set should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.SetVariable(varSlice[0]); err != nil {
		t.Error("SetVariable should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.SetVariables(varSlice); err != nil {
		t.Error("SnmpSetVariables should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP GET-BULK TESTS ######################//
	fmt.Println("Testing for various get-bulk requests started")
	server.SetVersion(Version2C)
	if _, err := server.GetBulk(); err != nil {
		t.Error("GetBulk should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := server.GetBulkOIDList([]string{"1.4.0", "1.5.0"}, 0, 50); err != nil {
		t.Error("GetBulkOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP INFORM TESTS ######################//
	if _, err := server.SendInform(10000, ".1.3.6.1.2.1.11", []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendInform should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP TRAP TESTS ######################//
	server.SetTargetPort(conf.trapPort)
	server.SetVersion(Version1)
	if err := server.SendTrap(".1.3.6.1.2.1.11", "localhost", 2, 0, 10000, []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendTrap should not throw any error for valid values, instead it throws :", err)
	}

	server.SetVersion(Version2C)
	if err := server.SendV2Trap(10000, *snmpvar.NewSnmpOID(".1.3.6.1.2.1.11"), []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendTrap should not throw any error for valid values, instead it throws :", err)
	}

	server.SetTargetPort(conf.port)

	server.ReleaseResources()
}

func TestServerListener(t *testing.T) {
	server := NewSnmpRequestServer()
	conf := Configuration()
	setServerConf(server, conf)
	server.AddListener(new(listener))

	respCount = 0
	count := 0
	for i := 0; i < 10; i++ {
		_, err := server.GetList()
		if err != nil {
			t.Error("Returned error for valid values: ", err)
		} else {
			count++
		}
	}
	time.Sleep(time.Second * 2) //Wait for all the messages
	if respCount < count {
		t.Errorf("Failure in receiving responses. Expected: %d. Got: %d.", count, respCount)
	}

	fmt.Println("Testing callback for timeouts.")
	respCount = 0
	timeoutCount = 0
	count = 0
	server.SetTargetPort(8888) //Invalid port
	server.SendTimeoutMsg(true)
	for i := 0; i < 10; i++ {
		_, err := server.GetList()
		if err != nil {
			t.Error("Returned error for valid values: ", err)
		} else {
			count++
		}
	}
	time.Sleep(time.Second * 6) //Wait for all the messages
	if timeoutCount != count {
		t.Errorf("Failure in receiving responses. Expected: %d. Got: %d.", count, timeoutCount)
	}

}

func setServerConf(serv *SnmpRequestServer, conf *targetConf) {

	serv.SetTargetHost(conf.host)
	serv.SetTargetPort(conf.port)
	serv.SetOIDList(conf.oidlist)

	serv.SetPrincipal("noAuthUser")
	serv.SetContextName("noAuth")
}

var respCount int = 0
var timeoutCount int = 0

type listener struct{}

func (l *listener) ReceiveMessage(msg *msg.SnmpMessage) {
	if msg != nil {
		respCount++
	} else {
		timeoutCount++
	}
}
