/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"container/list"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"webnms/log"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security/usm"
	trans "webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

//SnmpTrapReceiver can be used to receive traps from the SNMP agents.
//Create an instance of this type by calling 'NewTrapReceiver(ProtocolOptions)' function.
//
//User can start receiving traps by just creating instance of it (specifying ProtocolOptions (or) Port)
//and registering to it.
//
//  receiver := hl.NewTrapReceiverByPort(162)
//  if receiver == nil {
//  fmt.Println("Error starting trap receiver.")
//  os.Exit(1)
//  }
//  receiver.AddTrapListener(new(SnmpTrapReceiver))
//
//  //Implement TrapListener interface in order to receive traps:
//  type SnmpTrapReceiver int
//  func (t trapReceiver) ReceiveTrap(trap msg.SnmpMessage) {
//	 fmt.Println("Received Trap:", trap.PrintVarBinds())
//  }
type SnmpTrapReceiver struct {
	*snmpServer
	api             *snmp.SnmpAPI
	session         *snmp.SnmpSession
	host            string
	port            int //Store the binded port number
	protocolOptions trans.ProtocolOptions
	listeners       map[int]TrapListener //TrapListener maps
	missedTraps     list.List            //To store the trap messages that are not delivered to client

	sessionName    string
	apiName        string
	receiverOpen   bool
	community      string
	informResponse bool
	callBack       bool
	commAuthEnable bool //For v1/v2c communtiy based authentication
	v3AuthEnable   bool //For v3 authentication
	trapAuthEnable bool //For authentication all the trap messages

	//V3 Params
	principal       string
	authProtocol    consts.AuthProtocol
	authPassword    string
	privProtocol    consts.PrivProtocol
	privPassword    string
	contextName     string
	contextEngineID []byte
}

//NewTrapReceiver returns a new instance of SnmpTrapReceiver and error in case of any failure in starting the trap receiver.
//
//This function will open a new SnmpSession using the protocolOptions provided and waits for the Trap message on the port provided.
//If protocolOptions passed is nil, any available port will be used for receiving Traps (using UDP). Use Port() method to check the localport where TrapReceiver is listening for Traps.
//
//  Note:
//  * Refer ChangeListenerPort() method to start receiving trap on different port.
//  * Stop() method can be used to stop receiving traps.
func NewTrapReceiver(protocolOptions trans.ProtocolOptions) (*SnmpTrapReceiver, error) {
	receiver := &SnmpTrapReceiver{
		//Perform initialization
		listeners:       make(map[int]TrapListener),
		host:            "localhost",
		community:       "public",
		apiName:         "DEFAPI",
		protocolOptions: protocolOptions,
		informResponse:  true,
	}
	receiver.AuthenticateTraps(true) //Do authentication by default
	receiver.snmpServer = newSnmpServer()
	if err := receiver.initTrapReceiver(); err != nil {
		//log the error
		log.Fatal("Error in starting the Trap receiver: %s", err.Error())
		return nil, err
	}
	receiver.receiverOpen = true
	log.Debug("Started Trap receiver at Port: %d", receiver.port)

	return receiver, nil
}

//NewTrapReceiver returns a new instance of SnmpTrapReceiver and error in case of any failure in starting the trap receiver.
//
//This function will open a new SnmpSession using UDP as underlying transport protocol by default and bind to 'port' provided and
//waits for the Trap message.
//
//  Note: Refer ChangeListenerPort() method.
func NewTrapReceiverByPort(port int) (*SnmpTrapReceiver, error) {
	if port < 0 || port > 65535 {
		//log.Println("Invalid port number. Cannot create trap receiver.")
		return nil, fmt.Errorf("Invalid Port Number '%d' to start Trap receiver.", port)
	}

	receiver := &SnmpTrapReceiver{
		//Perform initialization
		listeners: make(map[int]TrapListener),
		host:      "localhost",
		community: "public",

		apiName:        "DEFAPI",
		port:           port,
		authProtocol:   NO_AUTH,
		privProtocol:   NO_PRIV,
		informResponse: true,
	}
	receiver.AuthenticateTraps(true) //Do authentication by default
	receiver.snmpServer = newSnmpServer()
	if err := receiver.initTrapReceiver(); err != nil {
		//log the error
		log.Fatal("Error in starting the Trap receiver: %s", err.Error())
		return nil, err
	}
	receiver.receiverOpen = true
	log.Debug("Started Trap receiver at Port: %d", receiver.port)

	return receiver, nil
}

var (
	snmp_trap_port int = 162 //Default port
	listenerId     int = 0
)

//IsAlive returns bool indicating whether the trap receiver is listening for trap messages.
func (t *SnmpTrapReceiver) IsAlive() bool {
	return t.receiverOpen
}

//AddTrapListener adds new TrapListener to the SnmpTrapReceiver.
//listener's ReceiveTrap() function will be called for all the traps received.
func (t *SnmpTrapReceiver) AddTrapListener(listener TrapListener) *SnmpTrapReceiver {
	if listener != nil {
		t.listeners[listenerId] = listener
		listenerId++
	}

	if t.session != nil {
		if !t.session.IsSnmpClientExist(t) {
			//Add SnmpTrapReceiver as SnmpClient to receive Trap Messages
			t.session.AddSnmpClient(t)
		}
	}

	return t
}

//RemoveListener removes the listener from SnmpTrapReceiver. This unsubscribe the listener from receiving traps.
func (t *SnmpTrapReceiver) RemoveListener(listener TrapListener) *SnmpTrapReceiver {
	for key, l := range t.listeners {
		if l == listener {
			delete(t.listeners, key)
			return t
		}
	}

	return t
}

//RemoveAllListeners removes all the TrapListeners added to this SnmpTrapReceiver t.
func (t *SnmpTrapReceiver) RemoveAllListeners() *SnmpTrapReceiver {
	for key, _ := range t.listeners {
		delete(t.listeners, key)
	}
	return t
}

//SetCommunity sets community string on the SnmpTrapReceiver. Default value is public.
//This community string will be used for authentication of incoming traps.
func (t *SnmpTrapReceiver) SetCommunity(comm string) *SnmpTrapReceiver {
	t.community = comm
	if t.session != nil {
		t.session.SetCommunity(comm) //Set the community string on SnmpSession
	}
	return t
}

//SetPrincipal sets the SNMPv3 principal/security-name.
func (t *SnmpTrapReceiver) SetPrincipal(userName string) *SnmpTrapReceiver {
	t.principal = userName
	return t
}

//Principal returns the SNMPv3 principal/security-name.
func (t *SnmpTrapReceiver) Principal() string { return t.principal }

//SetAuthProtocol sets the SNMPv3 auth protocol. The possible options are hl.NO_AUTH, hl.MD5_AUTH & hl.SHA_AUTH.
func (t *SnmpTrapReceiver) SetAuthProtocol(authProto consts.AuthProtocol) *SnmpTrapReceiver {
	if authProto == NO_AUTH || authProto == MD5_AUTH || authProto == SHA_AUTH {
		t.authProtocol = authProto
	} else {
		//log.Println("Invalid auth protocol. Setting default value NO_AUTH")
		t.authProtocol = NO_AUTH
	}
	return t
}

//AuthProtocol returns the SNMPv3 auth protocol.
func (t *SnmpTrapReceiver) AuthProtocol() consts.AuthProtocol { return t.authProtocol }

//SetAuthPassword sets the SNMPv3 auth password.
func (t *SnmpTrapReceiver) SetAuthPassword(authPass string) *SnmpTrapReceiver {
	t.authPassword = authPass
	return t
}

//AuthPassword returns the SNMPv3 auth password.
func (t *SnmpTrapReceiver) AuthPassword() string { return t.authPassword }

//SetPrivProtocol sets the SNMPv3 priv protocol. Valid priv protocols are hl.NO_PRIV, hl.DES_PRIV, hl.TRIPLE_DES_PRIV, hl.AES_128_PRIV, hl.AES_192_PRIV & hl.AES_256_PRIV.
func (t *SnmpTrapReceiver) SetPrivProtocol(privProto consts.PrivProtocol) *SnmpTrapReceiver {
	if privProto == NO_PRIV || privProto == DES_PRIV || privProto == TRIPLE_DES_PRIV || privProto == AES_128_PRIV || privProto == AES_192_PRIV || privProto == AES_256_PRIV {
		t.privProtocol = privProto
	} else {
		//log.Println("Invalid priv protocol. Setting default value NO_PRIV")
		t.privProtocol = NO_PRIV
	}

	return t
}

//PrivProtocol returns the SNMPv3 priv protocol.
func (t *SnmpTrapReceiver) PrivProtocol() consts.PrivProtocol { return t.privProtocol }

//SetPrivPassword sets the SNMPv3 priv password.
func (t *SnmpTrapReceiver) SetPrivPassword(privPass string) *SnmpTrapReceiver {
	t.privPassword = privPass
	return t
}

//PrivPassword returns the SNMPv3 priv password.
func (t *SnmpTrapReceiver) PrivPassword() string { return t.privPassword }

//SetContextName sets the SNMPv3 context name.
func (t *SnmpTrapReceiver) SetContextName(cName string) *SnmpTrapReceiver {
	t.contextName = cName
	return t
}

//ContextName returns the SNMPv3 context name.
func (t *SnmpTrapReceiver) ContextName() string { return t.contextName }

//SetContextEngineID sets the SNMPv3 context engine id.
func (t *SnmpTrapReceiver) SetContextEngineID(cID []byte) *SnmpTrapReceiver {
	t.contextEngineID = cID
	return t
}

//ContextEngineID returns the SNMPv3 context engine id.
func (t *SnmpTrapReceiver) ContextEngineID() []byte { return t.contextEngineID }

//AuthenticateTraps enable/disable the authentication of trap messages.
//
//Community based authentication will be enabled/disabled for v1/v2c trap messages.
//SNMPv3 authentication will be enabled/disabled. If disabled, v3 trap messages with security level upto AuthNoPriv will not be dropped even if authentication fails.
//v3 trap message will be dropped only if PrivProtocol/PrivPassword is unknown/wrong.
//
//By default the authentication is enabled.
func (t *SnmpTrapReceiver) AuthenticateTraps(auth bool) {
	t.v3AuthEnable = auth
	t.commAuthEnable = auth
}

//IsTrapAuthEnabled returns bool indicating whether the trap authentication is enabled.
//
//Returns true if either community or V3 authentication is enabled.
func (t *SnmpTrapReceiver) IsTrapAuthEnabled() bool {
	return t.commAuthEnable || t.v3AuthEnable
}

//SetCommunityAuthEnable sets the community based authentication for v1/v2c trap and v2c inform messages.
//Set this to true if v1/v2c trap and v2c inform messages needs to be authenticated based on community string, else set this to false.
func (t *SnmpTrapReceiver) SetCommunityAuthEnable(comm bool) {
	t.commAuthEnable = comm
}

//IsCommunityAuthEnable returns bool indicating whether v1/v2c community based authentication is enabled.
func (t *SnmpTrapReceiver) IsCommunityAuthEnable() bool {
	return t.commAuthEnable
}

//SetV3AuthEnable enables/disables the authentication of v3 trap/inform messages.
//
//Enable this if v3 trap/inform message needs to be authenticated and trap/inform messages needs to be received from valid SnmpEntity
//and from valid user.
//Otherwise it will not authenticate the v3 trap/inform messages, all the trap/inform messages upto AuthNoPriv security level will be received.
func (t *SnmpTrapReceiver) SetV3AuthEnable(v3Auth bool) {
	t.v3AuthEnable = v3Auth
}

//IsV3AuthEnable returns bool indicating whether v3 authentication in enabled.
func (t *SnmpTrapReceiver) IsV3AuthEnable() bool {
	return t.v3AuthEnable
}

//SetDebug defines whether debugging output should be generated. In the debug mode, the PDU data is printed in hex format.
func (t *SnmpTrapReceiver) SetDebug(debug bool) *SnmpTrapReceiver {
	if t.api != nil {
		t.api.SetDebug(debug)
	}
	return t
}

//Port returns the port number where SnmpTrapReceiver is listening for Traps.
func (t *SnmpTrapReceiver) Port() int {
	return t.port
}

//MissedTraps returns a list of traps that are not consumed by any of the listeners in SnmpTrapReceiver t.
func (t *SnmpTrapReceiver) MissedTraps() list.List {
	return t.missedTraps
}

//SetAutoInformResponse sets the automatic response flag for the Inform Request.
//If this flag is set to true, then the SNMP stack automatically sends a Get-Reponse message back to the sender.
//
//The default value is true.
func (t *SnmpTrapReceiver) SetAutoInformResponse(autoRespond bool) *SnmpTrapReceiver {
	t.informResponse = autoRespond
	if t.session != nil {
		t.session.SetAutoInformResponse(autoRespond)
	}
	return t
}

//AutoInformResponse returns bool indicating whether automatic response for Inform request is enabled or not.
func (t *SnmpTrapReceiver) AutoInformResponse() bool {
	if t.session != nil {
		return t.session.AutoInformResponse()
	}
	return false
}

//Set this to true if the user wants the callback to be called from separate goroutine.
//By default it is false.
func (t *SnmpTrapReceiver) SetCallbackRoutine(callBack bool) *SnmpTrapReceiver {
	t.callBack = callBack
	if t.session != nil {
		t.session.SetCallbackRoutine(callBack)
	}
	return t
}

//CallbackRoutine returns bool indicating whether the callback routine is enabled on t.
func (t *SnmpTrapReceiver) CallbackRoutine() bool {
	return t.callBack
}

//Create and add User entry in USM User LCD.
//EngineID is the SnmpEngineID of the authoritative entity which will send the traps.
//
//User details such as Principal, ContextName should be configured on 't' before calling this method.
func (t *SnmpTrapReceiver) AddTrapUser(engineID []byte) {
	if engineID == nil {
		return
	}

	if !strings.EqualFold(t.principal, "") {
		var err error
		userLCD := t.api.USMUserLCD()
		usmSecureUser, _ := userLCD.SecureUser(engineID, t.principal)
		if usmSecureUser != nil { //We have the user in our LCD.
			//There is already an user exist in USM LCD. Let's update the entry.
			err = userLCD.UpdateUser(engineID,
				usmSecureUser.UserName(),
				t.authProtocol,
				t.authPassword,
				t.privProtocol,
				t.privPassword,
			)
			if err != nil { //Error in updating the user.
				log.Error("Failure in updating the user entry in USMUserLCD for user '%s'. Error: %s", t.principal, err.Error())
				return
			}
		} else {
			//We should add the user to USM LCD.
			usmSecureUser, err = userLCD.AddUser(engineID, //AddUser method will check for the valid auth protocol/privprotocol
				t.principal,
				t.authProtocol,
				t.authPassword,
				t.privProtocol,
				t.privPassword,
			)
			if err != nil { //Failure in creating the user entry
				log.Error("Failure in creating the user entry in USMUserLCD for user '%s'. Error: %s", t.principal, err.Error())
				return
			}
		}
		log.Debug("Successfully added the user '%s' with EngineID: %s", t.principal, string(engineID))
	}

}

//SnmpEngine returns instance of SnmpEngine, which holds all the SubSystems and their models.
//Represents a SNMP entity.
func (t *SnmpTrapReceiver) SnmpEngine() engine.SnmpEngine {
	if t.api != nil {
		return t.api.SnmpEngine()
	}
	return nil
}

//Create and add User entry in USM User LCD.
//EngineID is the local engine's EngineID
func (t *SnmpTrapReceiver) AddInformUser() {
	engineID := t.api.SnmpEngine().SnmpEngineID()
	t.AddTrapUser(engineID)
}

//USMUserLCD returns the User based security model's UserLCD instance, which contains details of
//Secure Users created under this model.
func (t *SnmpTrapReceiver) USMUserLCD() usm.USMUserLCD {
	return t.api.USMUserLCD()
}

//USMPeerEngineLCD returns the LCD of engine entries specific to USM.
//PeerEngineLCD contains details of the peer SNMP entities such as EngineID, EngineBoots etc.,
func (t *SnmpTrapReceiver) USMPeerEngineLCD() engine.SnmpPeerEngineLCD {
	return t.api.USMPeerEngineLCD()
}

//InitDB initializes the database connection using the driverName and dataSourceName provided.
//User should have loaded the specific DB driver to open Database connection.
//
//This opens the database connection and creates the necessary tables in the database required for the SNMP API.
//
//dialectID is an integer constant indicating the SQLDialect which the database will be using
//for performing database operations.
//Dialect is required for inter-operability between different database queries.
//
//Refer db package for the list of SQLDialect implementation and their associated constants. If specific dialect
//implementation is not available, user should implement their own and register the same using RegisterSQLDialect() method.
//Refer db.SQLDialect interface for more details.
//
//Returns error in case of any db related error occurs (or) Unsupported DialectID passed.
func (t *SnmpTrapReceiver) InitDB(driverName, dataSourceName string, dialectID int) error {
	start := 0
	dbStr := "DriverName: " + driverName + " DataSourceName: " + dataSourceName
	log.Trace("* InitDB(): %s;*", dbStr)
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Initialize the DB connection
	err := t.api.InitDB(driverName, dataSourceName, dialectID)
	if err != nil {
		errStr := "Error in establishing DB connection: " + err.Error()
		log.Trace(errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return errors.New(errStr)
	}
	log.Trace("Succesfully established the DB connection and created the required tables.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return nil
}

//RegisterSQLDialect adds/registers the new SQLDialect implementation on the database.
//dialect should be an implementation of db.SQLDialect interface.
//
//User should use this method when they want to provide the new implementation for the database
//other than provided by the API.
//
//Default implementation exist for: MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, Sybase.
//
//Returns error in case of failure in registering the Dialect
func (t *SnmpTrapReceiver) RegisterSQLDialect(dialectID int, dialect db.SQLDialect) error {
	return db.RegisterNewDialect(dialectID, dialect)
}

//SetDatabaseFlag enables/diables the usage of database for all the SNMP operations internally.
//If enabled DB will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
//
//Note: When Database usage is disabled in the middle, it may cause some adverse effect of re-synchronization to get the new values
//with all the SNMP entities and fill the local LCD.
func (t *SnmpTrapReceiver) SetDatabaseFlag(flag bool) {
	t.api.SetDatabaseFlag(flag)
}

//IsDatabaseFlag returns bool indicating whether database storage is enabled for SNMP operations.
//False indicates that the runtime memory (LCD) is used for storing/retriving V3 related data.
func (t *SnmpTrapReceiver) IsDatabaseFlag() bool {
	return t.api.IsDatabaseFlag()
}

//ChangeListenerPort allows the user to rebind to different port to listen for Trap messages.
//This method will close the SnmpSession previously opened and reopen a new SnmpSession using port p and protocolOptions previously set on this SnmpTrapReceiver t.
//
//This is similar to 'restart' option.
//Returns error if any.
func (t *SnmpTrapReceiver) ChangeListenerPort(p int) error {

	if p == t.port {
		return fmt.Errorf("SnmpTrapReceiver is already listening for Traps at port: %d", p)
	}
	if p < 0 || p > 65535 {
		return errors.New("Invalid port number: Port number should be greater than -1 and less than 65535.")
	}
	t.Stop() //Stop the SnmpTrapReceiver

	if t.protocolOptions != nil {
		t.protocolOptions.SetLocalPort(p)
	}
	t.port = p //UDPProtocolOptions will be used by default

	if err := t.initTrapReceiver(); err != nil {
		log.Fatal("Error in changing the listener port to '%d' for the Trap receiver: %s", p, err.Error())
		return err
	}
	t.receiverOpen = true
	log.Debug("Changed the listener port of Trap receiver to: %d", p)

	return nil
}

//Stop will stop this SnmpTrapReceiver t from receiving traps.
//This will cleanup the SnmpSession set on t.
//
//Use ChangeListenerPort() method to re-listen for trap messages on t.
func (t *SnmpTrapReceiver) Stop() error {
	if !t.receiverOpen {
		return errors.New("SnmpTrapReceiver is not open. Create new SnmpTrapReceiver instance by using NewTrapReceiver(ProtocolOptions) method.")
	}
	t.mut.Lock()
	defer t.mut.Unlock()
	//Close SnmpSession
	if val, ok := t.snmpStore["SESSION"][t.sessionName]; ok {
		if val.concurrentUsers == 1 { //No HL API is sharing the SnmpSession. Hence remove it from snmpStore.
			delete(t.snmpStore["SESSION"], t.sessionName)
			if t.session != nil {
				t.session.Close() //Close the SnmpSession
			}
		} else {
			t.snmpStore["SESSION"][t.sessionName].concurrentUsers--
			t.session.RemoveSnmpClient(t)
		}
		t.session = nil
	}
	//Close SnmpAPI
	if val, ok := t.snmpStore["API"]["API"]; ok {
		if val.concurrentUsers == 1 {
			delete(t.snmpStore["API"], "API")
			if t.api != nil {
				t.api.Close()
			}
		} else {
			t.snmpStore["API"]["API"].concurrentUsers--
		}
		t.api = nil
	}

	t.receiverOpen = false
	log.Debug("Trap receiver is stopped successfully. Not listening for traps at port: %d", t.port)
	t.port = 0 //Not listening on any port

	return nil
}

//Process trap message and forward to TrapListeners.
//API user need not use this method. For internal use only.
func (t *SnmpTrapReceiver) Callback(resp *msg.SnmpMessage, reqID int32) {
	if resp != nil {
		remoteAddr := "<Unknown>"
		if resp.ProtocolOptions() != nil {
			remoteAddr = resp.ProtocolOptions().SessionID()
		}
		if resp.Command() == consts.TrapRequest ||
			resp.Command() == consts.Trap2Request ||
			resp.Command() == consts.InformRequest {
			if len(t.listeners) > 0 {
				//Call the respective trap listeners
				for _, v := range t.listeners {
					v.ReceiveTrap(*resp)
				}
			} else {
				//Store the Trap message that is not consumed by any listener.
				log.Info("No listeners exist to receive the trap msg. Stored in missed traps queue. Trap Msg:: ReqID: %d; Remote Address: %s;",
					resp.RequestID(), remoteAddr)
				t.missedTraps.PushBack(resp)
			}
		} else {
			log.Info("Received non-trap message. Dropping it. MSG:: ReqID: %d; Command: %s; Remote Address: %s;", resp.RequestID(), getMSGCommand(resp.Command()), remoteAddr)
		}
	} else {
		log.Debug("Empty PDU received. Maybe decryption error could have occurred.")
	}
	return
}

//Authenticate Trap message based on the community string for v1/v2c.
//API user need not use this method. For internal use only.
func (t *SnmpTrapReceiver) Authenticate(res *msg.SnmpMessage, community string) bool {
	if res == nil {
		return true
	}
	remoteAddr := "<Unknown>"
	if res.ProtocolOptions() != nil {
		remoteAddr = res.ProtocolOptions().SessionID()
	}
	if !t.IsTrapAuthEnabled() { //If no authentication needed, just return true
		return true
	} else {
		//Check for version specific auth enable
		if res.Version() < Version3 {
			//Let's perform community check
			if !t.IsCommunityAuthEnable() {
				return true
			} else {
				if strings.EqualFold(res.Community(), t.community) {
					return true
				} else {
					log.Fatal("Dropping the message: Failure in community authentication. MSG:: ReqID: %d; Remote Address: %s;", res.RequestID(), remoteAddr)
					return false
				}
			}
		} else { //V3 Authentication
			if !t.IsV3AuthEnable() {
				return true
			} else {
				if res.IsAuthenticationFailure() {
					log.Fatal("Dropping the message: Failure in V3 authentication. MSG:: ReqID: %d; Remote Address: %s;", res.RequestID(), remoteAddr)
					return false
				} else {
					return true
				}
			}
		}
	}
	//log.Println("Authentication failed. Dropping this Trap PDU.")
}

//This logs the debug prints to the logger or err console.
//For internal use only.
func (t *SnmpTrapReceiver) DebugStr(dbgStr string) {
	if log.LogFilters() != nil {
		log.Logf(log.DEBUG, dbgStr)
	} else { //Just write to error console
		if t.api.Debug() {
			fmt.Fprintln(os.Stderr, dbgStr)
		}
	}
}

//This will perform the basic initialization for SnmpTrapReceiver. Create new SnmpSession, SnmpAPI instance.
func (t *SnmpTrapReceiver) initTrapReceiver() error {

	var port int
	var ok bool
	if t.protocolOptions != nil {
		port = t.protocolOptions.LocalPort()
	} else {
		port = t.port //Port number can be zero.
	}

	t.sessionName = "SESSION:" + strconv.Itoa(port)

	t.mut.Lock() //Lock for synchronization purpose
	//Take the SnmpAPI instance
	if _, ok = t.snmpStore["API"]; !ok { //Creating API entry for very first time - new entry
		t.api = snmp.NewSnmpAPI()
		im := make(map[string]*value)
		im[t.apiName] = &value{t.api, 1}
		t.snmpStore["API"] = im
	} else if _, ok = t.snmpStore["API"][t.apiName]; !ok { //Creating new SnmpAPI instance for first time
		t.api = snmp.NewSnmpAPI()
		im := t.snmpStore["API"]
		im[t.apiName] = &value{t.api, 1}
	} else if val, ok := t.snmpStore["API"][t.apiName]; ok { //Reusing the already created SnmpAPI instance
		t.api = val.llInstance.(*snmp.SnmpAPI)
		val.concurrentUsers++
	}

	//Take the SnmpSession instance
	if _, ok = t.snmpStore["SESSION"]; !ok { //Creating SESSION for very first time - new entry
		t.session = snmp.NewSnmpSession(t.api) //Creating new SnmpSession
		im := make(map[string]*value)
		im[t.sessionName] = &value{t.session, 1}
		t.snmpStore["SESSION"] = im
	} else if _, ok = t.snmpStore["SESSION"][t.sessionName]; !ok { //Creating new SnmpSession instance for first time
		t.session = snmp.NewSnmpSession(t.api) //Creating new SnmpSession
		im := t.snmpStore["SESSION"]
		im[t.sessionName] = &value{t.session, 1}
	} else if val, ok := t.snmpStore["SESSION"][t.sessionName]; ok { //Reusing the already created SnmpSession instance
		t.session = val.llInstance.(*snmp.SnmpSession) //Reusing the exisiting SnmpSession
		val.concurrentUsers++
	}
	t.mut.Unlock()

	//Try to open new SnmpSession only if you have newly created the SnmpSession (first time).
	if !t.session.IsSessionOpen() && (t.snmpStore["SESSION"][t.sessionName].concurrentUsers == 1) {
		if err := t.openSession(); err != nil {
			delete(t.snmpStore["SESSION"], t.sessionName)
			return err
		}
	} /*else {
		//We can check whether the session is opened on same ProtocolOptions as expected.
		//If not then we can throw error to inform the user.

	}*/

	//Set the params on SnmpSession for every initialization using the values set on trap receiver 't'.
	if len(t.listeners) > 0 {
		if !t.session.IsSnmpClientExist(t) {
			//Add SnmpTrapReceiver as SnmpClient to receive Trap Messages
			t.session.AddSnmpClient(t)
		}
	}
	t.session.SetAutoInformResponse(t.informResponse)
	t.session.SetCallbackRoutine(t.callBack)
	t.session.SetCommunity(t.community)

	return nil
}

//This method will open a new SnmpSession using the session instance set on SnmpTrapReceiver.
func (t *SnmpTrapReceiver) openSession() error {
	var port int
	if t.session != nil {
		if t.community != "" {
			t.session.SetCommunity(t.community)
		}
		if t.protocolOptions != nil {
			//In case local port is not set on ProtocolOptions, then use 162 by default.
			//Only for implementations with pointer receivers, default port can be set.
			po := t.protocolOptions
			port = po.LocalPort()
			if po.LocalPort() <= 0 {
				po.SetLocalPort(snmp_trap_port)
			}
			t.session.SetProtocolOptions(po)
		} else {
			//Using UDP by default
			udp := snmp.NewUDPProtocolOptions()
			//udp.SetLocalHost(t.host)
			udp.SetLocalPort(t.port)
			port = t.port
			t.session.SetProtocolOptions(udp)
		}

		//Open a new SnmpSession
		if err := t.session.Open(); err != nil {
			//log.Println(err)
			return err
		}

		//Set the binded port number on SnmpTrapReceiver
		if po := t.session.ProtocolOptions(); po != nil {
			t.port = po.LocalPort()
		}

		//If we are binding to any available port, then we should update the SnmpStore with this new port number (session+port)
		if port == 0 {
			sessionName := "SESSION:" + strconv.Itoa(port)
			t.mut.Lock()
			if _, ok := t.snmpStore["SESSION"][sessionName]; ok { //Zero port SessionName
				delete(t.snmpStore["SESSION"], sessionName)
				t.sessionName = "SESSION:" + strconv.Itoa(t.port)
				im := t.snmpStore["SESSION"]
				im[t.sessionName] = &value{t.session, 1}
			}
			t.mut.Unlock()
		}
	} else {
		return errors.New("Error in opening SnmpSession. Session instance is nil.")
	}

	return nil
}

//Checks whether the SnmpTrapReceiver client is already added to SnmpSesion's snmpclient list.
//func isClientExist(clientList list.List, snmpClient snmp.SnmpClient) bool {
//	for cl := clientList.Front(); cl != nil; cl = cl.Next() {
//		client := cl.Value.(snmp.SnmpClient)
//		if snmpClient == client {
//			return true
//		}
//	}
//
//	return false
//}

//TrapListener interface defines the listener for incoming traps.
//This listener can be added to SnmpTrapReceiver using 'AddTrapListener()' method to receive Trap messages.
//
//SnmpTrapReceiver calls the ReceiveTrap method of all the Trap Listener instance registered to it.
type TrapListener interface {
	//Invoked on the listener when the trap is
	//received by the generator, i.e, SnmpTrapReceiver.
	ReceiveTrap(trap msg.SnmpMessage)
}

//Need to implement filters for SnmpTrapReceiver

//To filter the incoming trap messages.
//type TrapFilter struct {
//	Version      snmp.Version
//	Community    string
//	GenericType  int
//	SpecificType int
//	AgentAddress string
//	Command      snmp.Command
//}
//
//func (f TrapFilter) checkFilter(trap msg.SnmpMessage) bool {
//	a := TrapFilter{}
//	if a == f {
//		return false //No filter added: Donot filter the trap message
//	}
//	if trap.Version() == f.Version && strings.EqualFold(trap.Community(), f.Community) &&
//		trap.GenericType() == f.GenericType && trap.SpecificType() != f.SpecificType &&
//		strings.EqualFold(trap.AgentAddress(), f.AgentAddress) && trap.Command() != f.Command {
//		return false
//	}
//	return true
//}
