/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"webnms/snmp/consts"
)

//SNMP Version constants.
const (
	Version1  = consts.Version1
	Version2C = consts.Version2C
	Version3  = consts.Version3
)

//PDU Type constants as per rfc3416.
const (
	GetRequest     = consts.GetRequest
	GetNextRequest = consts.GetNextRequest
	GetResponse    = consts.GetResponse
	SetRequest     = consts.SetRequest
	TrapRequest    = consts.TrapRequest
	GetBulkRequest = consts.GetBulkRequest
	InformRequest  = consts.InformRequest
	Trap2Request   = consts.Trap2Request
	ReportMessage  = consts.ReportMessage
)

//Snmp Variable constants. Used for creating SnmpVar.
const (
	Sequence         byte = 0x30
	Integer          byte = 0x02
	BitString        byte = 0x03
	OctetString      byte = 0x04
	SnmpNullVar      byte = 0x05
	ObjectIdentifier byte = 0x06
	IpAddress        byte = 0x40
	Counter          byte = 0x41
	Gauge            byte = 0x42
	TimeTicks        byte = 0x43
	Opaque           byte = 0x44
	Counter64        byte = 0x46
)

//Error constants
const (
	//Standard SNMP errors
	Err_No_Error             int = 0
	Err_Too_Big              int = 1
	Err_No_Such_Name         int = 2
	Err_Bad_Value            int = 3
	Err_Read_Only            int = 4
	Err_Gen_Err              int = 5
	Err_No_Access            int = 6
	Err_Wrong_Type           int = 7
	Err_Wrong_Length         int = 8
	Err_Wrong_Encoding       int = 9
	Err_Wrong_Value          int = 10
	Err_No_Creation          int = 11
	Err_Inconsistent_Value   int = 12
	Err_Resourse_Unavailable int = 13
	Err_Commit_Failed        int = 14
	Err_Undo_Failed          int = 15
	Err_Authorization_Error  int = 16
	Err_Not_Writable         int = 17
	Err_Inconsistent_Name    int = 18

	//API specific errors
	Not_Init                int = 19
	Invalid_Version         int = 20
	Oid_Not_Specified       int = 21
	Request_Timed_Out       int = 22
	Invalid_V1_Request      int = 23
	Empty_Varbind           int = 24
	Invalid_Non_Rep         int = 25
	Empty_Data              int = 26
	Varbind_Out_Of_Range    int = 27
	Empty_Variable          int = 28
	Invalid_Numeric_Val     int = 29
	Error_Creating_Variable int = 30
	Illegal_Argument        int = 31
	Dropped_Invalid_Oid     int = 32
	Invalid_Generic_Type    int = 33

	//V3 Errors
	UnSupported_SecurityLevel_Error int = 44
	Not_In_Time_Windows_Error       int = 45
	Unknown_Usernames_Error         int = 46
	Unknown_EngineID_Error          int = 47
	Wrong_Digest_Error              int = 48
	Decrypt_Error                   int = 49
	Discovery_Failed                int = 51
	Time_Sync_Failed                int = 52
	User_Not_Created                int = 53

	//Exception codes
	Exp_No_Such_Object   int = 128
	Exp_No_Such_Instance int = 129
	Exp_End_Of_Mib_View  int = 130

	No_Err_Registered int = -1
)

//SNMPv3 specific constants
const (
	//SNMPv3 Security Level constants
	NoAuthNoPriv = consts.NoAuthNoPriv
	AuthNoPriv   = consts.AuthNoPriv
	AuthPriv     = consts.AuthPriv

	//USM Authentication Protocol constants
	NO_AUTH  = consts.NO_AUTH
	MD5_AUTH = consts.MD5_AUTH
	SHA_AUTH = consts.SHA_AUTH

	//USM Privacy Protocol constants
	NO_PRIV         = consts.NO_PRIV
	DES_PRIV        = consts.DES_PRIV
	TRIPLE_DES_PRIV = consts.TRIPLE_DES_PRIV
	AES_128_PRIV    = consts.AES_128_PRIV
	AES_192_PRIV    = consts.AES_192_PRIV
	AES_256_PRIV    = consts.AES_256_PRIV
)

//SQLDialect Constants. These constants should be passed while Initializing the database.
//It is required for all the database operations.
//These dialect ids are required for acheiving inter-operability among different databases.
//
//
//  Note: All the constants defined in hl package are derived from snmp.consts package.
const (
	MySQL     int = consts.MySQL
	Postgres  int = consts.Postgres
	SQLite    int = consts.SQLite
	Oracle    int = consts.Oracle
	SQLServer int = consts.SQLServer
	DB2       int = consts.DB2
	Sybase    int = consts.Sybase
)

//Internal use
const (
	unSupportedSecurityLevelOid string = ".1.3.6.1.6.3.15.1.1.1.0"
	notInTimeWindowsOid         string = ".1.3.6.1.6.3.15.1.1.2.0"
	unknownUserNamesOid         string = ".1.3.6.1.6.3.15.1.1.3.0"
	unknownEngineIdOid          string = ".1.3.6.1.6.3.15.1.1.4.0"
	wrongDigestsOid             string = ".1.3.6.1.6.3.15.1.1.5.0"
	decryptErrorOid             string = ".1.3.6.1.6.3.15.1.1.6.0"
)
