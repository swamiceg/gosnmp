/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
//_ "github.com/lib/pq" - Load the database driver
)

var target *SnmpTarget

// This example initializes the Postgres database to store the USM configuration in DB.
// Database driver should be loaded before calling this method.
// Returns error in case of any failure.
func ExampleSnmpTarget_InitDB() {
	var (
		driverName  = "postgres"
		dataSrcName = "dbname=TestDB user=postgres host=127.0.0.1 port=5432 sslmode=disable" //Should be a valid DSN
		dialect     = Postgres                                                               //Dialect should be Postgres since we are using Postgres DB
	)

	target = NewSnmpTarget()

	err := target.InitDB(driverName, dataSrcName, dialect)
	if err != nil {
		panic(err)
	}
}
