/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

//ErrorMessages struct holds all the error messages used in hl package.
type ErrorMessages struct {
	errMap map[int]string
}

func newErrMap() (ErrorMessages, error) {
	erMap := make(map[int]string)
	errMsg := ErrorMessages{erMap}
	errMsg.intializeErrMsgs()
	return errMsg, nil
}

//Returns error string based on the error code passed.
func (e *ErrorMessages) ErrorString(code int) string {
	if code == -1 {
		return "No Error Code Registered"
	}

	return e.errMap[code]
}

//Adds new error string to the error messages list.
func (e *ErrorMessages) AddNewErrorString(errStr string) int {
	e.errMap[len(e.errMap)] = errStr
	return len(e.errMap) - 1
}

func (e *ErrorMessages) intializeErrMsgs() {
	e.errMap[Err_No_Error] = "No error"
	e.errMap[Err_Too_Big] = "Response message too large"
	e.errMap[Err_No_Such_Name] = "There is no such variable name in this mib."
	e.errMap[Err_Bad_Value] = "The value given has wrong type or length."
	e.errMap[Err_Read_Only] = "The variable is read only"
	e.errMap[Err_Gen_Err] = "A general failure occurred"
	e.errMap[Err_No_Access] = "A no access error occurred"
	e.errMap[Err_Wrong_Type] = "A wrong type error occurred"
	e.errMap[Err_Wrong_Length] = "A wrong length error occurred."
	e.errMap[Err_Wrong_Encoding] = "A wrong encoding error occurred."
	e.errMap[Err_Wrong_Value] = "A wrong value error occurred."
	e.errMap[Err_No_Creation] = "A no creation error occurred."
	e.errMap[Err_Inconsistent_Value] = "An inconsistent value error occurred."
	e.errMap[Err_Resourse_Unavailable] = "A resource unavailable error occurred."
	e.errMap[Err_Commit_Failed] = "A commit failed error occurred."
	e.errMap[Err_Undo_Failed] = "A undo failed error occurred."
	e.errMap[Err_Authorization_Error] = "A authorization failed error occurred."
	e.errMap[Err_Not_Writable] = "A not writable error occurred."
	e.errMap[Err_Inconsistent_Name] = "An inconsistent name error occurred."
	e.errMap[Not_Init] = "Error: SNMP not initialized"
	e.errMap[Invalid_Version] = "Error: Invalid Version"
	e.errMap[Oid_Not_Specified] = "Error: OID Not Specified"
	e.errMap[Request_Timed_Out] = "Error: Request Timed Out"
	e.errMap[Invalid_V1_Request] = "Error: Invalid Request for V1"
	e.errMap[Empty_Varbind] = "Error: Agent Returned Empty Varbind(s)"
	e.errMap[Invalid_Non_Rep] = "Error: Invalid Non-repeaters Value"
	e.errMap[Empty_Data] = "Error: Agent Returned Empty Data"
	e.errMap[Varbind_Out_Of_Range] = "Error: VariableBindings Index out of Range"
	e.errMap[Empty_Variable] = "Error: Agent Returned Empty Variable"
	e.errMap[Invalid_Numeric_Val] = "Error: Agent Returned Non-Numeric Value"
	e.errMap[Error_Creating_Variable] = "Error: Error creating variable."
	e.errMap[Illegal_Argument] = "Error: Illegal Argument(s)."
	e.errMap[Dropped_Invalid_Oid] = "Error: Few Invalid OIDs got dropped."
	e.errMap[UnSupported_SecurityLevel_Error] = "usmStatsUnsupportedSecurityLevel"
	e.errMap[Not_In_Time_Windows_Error] = "usmStatsNotInTimeWindows"
	e.errMap[Unknown_Usernames_Error] = "usmStatsUnknownUserNames"
	e.errMap[Unknown_EngineID_Error] = "usmStatsUnknownEngineIDs"
	e.errMap[Wrong_Digest_Error] = "usmStatsWrongDigests"
	e.errMap[Decrypt_Error] = "usmStatsDecryptionErrors"
	e.errMap[Discovery_Failed] = "Error: Discovery Failed"
	e.errMap[Time_Sync_Failed] = "Error:Time Synchronization Failed"
	e.errMap[User_Not_Created] = "Error: User Entry not created"
	e.errMap[Exp_No_Such_Object] = "There is no such object in this MIB."
	e.errMap[Exp_No_Such_Instance] = "There is no such instance in this MIB."
	e.errMap[Exp_End_Of_Mib_View] = "This is a end of Mib View."
}
