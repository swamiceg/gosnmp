/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/snmpvar"
)

type targetConf struct {
	host     string
	port     int
	trapPort int
	oidlist  []string
}

func TestTarget(t *testing.T) {

	conf := Configuration()

	//test for NewSnmpTarget
	target := NewSnmpTarget()
	if target != nil {
		setTargetConf(target, conf)
		_, err := target.GetList()
		if err != nil {
			t.Error("Error occured for valid values, err :", err)
		}
	} else {
		t.Error("Target should not be nil")
	}
	target.ReleaseResources()

	//test for NewSnmpTargetBySession
	sessionTarget := NewSnmpTargetBySession("Session1", 7001)
	if sessionTarget != nil {
		setTargetConf(sessionTarget, conf)
		_, err := sessionTarget.GetList()
		if err != nil {
			t.Error("Error occured for valid values :", err)
		}
	} else {
		t.Error("Session Target should not be nil")
	}
	sessionTarget.ReleaseResources()

	//test for NewSnmpTargetByProtocolOptions
	proOptTarget := NewSnmpTargetByProtocolOptions(snmp.NewUDPProtocolOptions(), nil)
	if proOptTarget != nil {
		setTargetConf(proOptTarget, conf)
		_, err := proOptTarget.GetList()
		if err != nil {
			t.Error("Error occured for valid values :", err)
		}
	} else {
		t.Error("protocol options target should not be nil")
	}
	proOptTarget.ReleaseResources()
}

func TestTargetObjectID(t *testing.T) {

	conf := Configuration()

	validHosts := []string{"localhost", "127.0.0.1"}

	//tests for SetTargetHost with valid values
	for _, validHost := range validHosts {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.SetTargetHost(validHost)
			_, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid host,", target.TargetHost())
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	invalidHosts := []string{"invalidHost", "1.1.1.1.1"}

	//tests for SetTargetHost with invalid values
	for _, invalidHost := range invalidHosts {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.SetTargetHost(invalidHost)
			_, err := target.GetList()
			if err == nil {
				t.Error("Error should be thrown for invalid host,", target.TargetHost())
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	validOidLists := []interface{}{[]string{"1.3.0", "1.4.0"}, []snmpvar.SnmpOID{*snmpvar.NewSnmpOID("1.5.0"), *snmpvar.NewSnmpOID("1.6.0")}}
	invalidOidLists := []interface{}{[]string{"invalidOid1", "invalidOid2"}, []int{5, 6}, []string{}}
	validOids := []interface{}{"1.1.0", *snmpvar.NewSnmpOID("1.1.0"), snmpvar.NewSnmpOID("1.1.0")}
	invalidOids := []interface{}{5, "invalidOid"}
	validIndex := 1
	invalidIndices := []int{-3, 10}

	//tests for SetOIDList with valid values
	for _, validOidList := range validOidLists {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.SetOIDList(validOidList)
			_, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", target.TargetHost())
			} else {
				if target.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for SetOIDList with invalid values
	for _, invalidOidList := range invalidOidLists {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.SetOIDList(invalidOidList)
			_, err := target.GetList()
			if err == nil {
				t.Error("Error should be thrown for invalid oidlist,", target.OIDList())
			} else {
				if target.ErrorCode() != Oid_Not_Specified {
					t.Error("ErrorCode", Oid_Not_Specified, "should be set for invalid oidList,", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOID with valid values
	for _, validOid := range validOids {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOID(validOid)
			_, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", target.OIDList())
			} else {
				if target.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOID with invalid values
	for _, invalidOid := range invalidOids {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOID(invalidOid)
			if len(target.OIDList()) > 2 {
				t.Error("Invalid OID should not be added,", invalidOid)
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOIDs with valid values
	for _, validOidList := range validOidLists {
		target := NewSnmpTarget()

		if target != nil {
			setTargetConf(target, conf)
			target.AddOIDs(validOidList)
			result, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", target.TargetHost())
			} else {
				if target.ErrorCode() != 0 {
					t.Error("ErrorCode should not be set for valid oidList,", target.OIDList())
				} else if len(result) != 4 {
					t.Error("New OIDs not added to already existing OIDList", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}

		target.ReleaseResources()
	}

	//tests for AddOIDs with invalid values
	for _, invalidOidList := range invalidOidLists {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOIDs(invalidOidList)
			result, err := target.GetList()
			if err != nil {
				t.Error("Error should be not thrown for invalid oidlist appended (original OIDs will already be present),", target.OIDList())
			} else {
				if len(result) != 2 {
					t.Error("Invalid OIds", invalidOidList, "should not be added to the existing OIDList", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOIDAt with valid values
	for _, validOid := range validOids {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOIDAt(validIndex, validOid)
			output, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for valid oidlist,", target.OIDList())
			} else {
				if target.ErrorCode() != 0 || len(output) <= 2 || !strings.EqualFold(target.OIDAt(validIndex), ".1.3.6.1.2.1.1.1.0") {
					t.Error("ErrorCode should not be set for valid oidList,", target.OIDList())
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOIDAt with invalid values
	for _, invalidOid := range invalidOids {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOIDAt(1, invalidOid)
			if len(target.OIDList()) > 2 {
				t.Error("Invalid OID should not be added,", invalidOid)
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for AddOIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.AddOIDAt(invalidIndex, "1.7.0")
			output, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for invalid index,", invalidIndex)
			} else {
				if len(output) > 2 {
					t.Error("OIDs should not be added to the oidlist for invalid index,", invalidIndex)
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for RemoveOIDAt with valid values
	target := NewSnmpTarget()
	if target != nil {
		setTargetConf(target, conf)
		target.RemoveOIDAt(validIndex)
		output, err := target.GetList()
		if err != nil {
			t.Error("Error should not be thrown for valid index,", validIndex)
		} else {
			if target.ErrorCode() != 0 || len(output) >= 2 {
				t.Error("OID should be removed for valid index,", validIndex)
			}
		}
	} else {
		t.Error("Target should not be nil")
	}
	target.ReleaseResources()

	//tests for RemoveOIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			target.RemoveOIDAt(invalidIndex)
			output, err := target.GetList()
			if err != nil {
				t.Error("Error should not be thrown for invalid index,", invalidIndex)
			} else {
				if len(output) < 2 {
					t.Error("OIDs should not be removed to the oidlist for invalid index,", invalidIndex)
				}
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}

	//tests for OIDAt with valid values
	target1 := NewSnmpTarget()
	if target1 != nil {
		setTargetConf(target1, conf)
		if !strings.EqualFold(target1.OIDAt(validIndex), ".1.3.6.1.2.1.1.5.0") {
			t.Error("Expected .1.3.6.1.2.1.1.5.0, instead got,", target1.OIDAt(validIndex))
		}
	} else {
		t.Error("Target should not be nil")
	}
	target1.ReleaseResources()

	//tests for OIDAt with invalid indices
	for _, invalidIndex := range invalidIndices {
		target := NewSnmpTarget()
		if target != nil {
			setTargetConf(target, conf)
			if !strings.EqualFold(target.OIDAt(invalidIndex), "<nil>") {
				t.Error("Expected <nil>, instead got,", target1.OIDAt(invalidIndex))
			}
		} else {
			t.Error("Target should not be nil")
		}
		target.ReleaseResources()
	}
}

func TestTargetFields(t *testing.T) {

	//tests for SetVersion
	versions := []consts.Version{consts.Version1, consts.Version2C, consts.Version3}
	invalidVersion := consts.Version(5)

	target := NewSnmpTarget()
	if target.Version() != consts.Version1 {
		t.Error("Default target version should be", consts.Version1)
	}
	for _, version := range versions {
		target.SetVersion(version)
		if target.Version() != version {
			t.Error("Expecting", version, "but got,", target.Version())
		}
	}
	target.SetVersion(invalidVersion)
	if target.Version() == invalidVersion {
		t.Error(invalidVersion, "cannot be set to target")
	}

	//tests for SetCommunity
	if !strings.EqualFold(target.Community(), "public") {
		t.Error("Default target community should be public")
	}
	target.SetCommunity("private")
	if !strings.EqualFold(target.Community(), "private") {
		t.Error("Expecting private but got,", target.Community())
	}

	//tests for Debug method
	if target.Debug() {
		t.Error("Debug should not be set by default")
	}
	target.SetDebug(true)
	if !target.Debug() {
		t.Error("Debug should be set if true is passed to SetDebug")
	}
	target.SetDebug(false)

	//tests for TargetPort method
	if target.TargetPort() != 161 {
		t.Error("Expected default value for target port is 161, but got,", target.TargetPort())
	}
	target.SetTargetPort(9001)
	if target.TargetPort() != 9001 {
		t.Error("Expected target port to be 9001, but got,", target.TargetPort())
	}

	invalidPorts := []int{-161, 70000}

	for _, invalidPort := range invalidPorts {
		target.SetTargetPort(invalidPort)
		if target.TargetPort() == invalidPort {
			t.Error("Invalid port", invalidPort, "should not be set as targetPort")
		}
	}

	//tests for Timeout field
	if target.Timeout() != 5 {
		t.Error("Expected default value for timeout is 5, but got,", target.Timeout())
	}
	target.SetTimeout(2)
	if target.Timeout() != 2 {
		t.Error("Expected target timeout to be 2, but got,", target.Timeout())
	}

	invalidTimeouts := []int{-4, 0}
	for _, invalidTimeout := range invalidTimeouts {
		if target.Timeout() == invalidTimeout {
			t.Error("Invalid timeout value", invalidTimeout, "should not be set as target timeout")
		}
	}

	//tests for retry field
	if target.Retries() != 0 {
		t.Error("Expected default value for retries is 0, but got,", target.Retries())
	}
	target.SetRetries(2)
	if target.Retries() != 2 {
		t.Error("Expected target retries to be 2, but got,", target.Retries())
	}

	invalidRetries := []int{-4}
	for _, invalidRetry := range invalidRetries {
		if target.Retries() == invalidRetry {
			t.Error("Invalid retries value", invalidRetry, "should not be set as target retries")
		}
	}

	//tests for Non-Repeaters field
	if target.NonRepeaters() != 0 {
		t.Error("Expected default value for non-repeaters is 0, but got,", target.NonRepeaters())
	}
	target.SetNonRepeaters(2)
	if target.NonRepeaters() != 2 {
		t.Error("Expected target non-repeaters to be 2, but got,", target.NonRepeaters())
	}

	invalidNonReps := []int32{-4}
	for _, invalidNonRep := range invalidNonReps {
		if target.NonRepeaters() == invalidNonRep {
			t.Error("Invalid non-repeaters value", invalidNonRep, "should not be set as target non-repeaters")
		}
	}

	//tests for Max-Repetitions field
	if target.MaxRepetitions() != 50 {
		t.Error("Expected default value for Max-Repetitions is 50, but got,", target.MaxRepetitions())
	}
	target.SetMaxRepetitions(2)
	if target.MaxRepetitions() != 2 {
		t.Error("Expected target Max-Repetitions to be 2, but got,", target.MaxRepetitions())
	}

	invalidMaxReps := []int32{-4}
	for _, invalidMaxRep := range invalidMaxReps {
		if target.MaxRepetitions() == invalidMaxRep {
			t.Error("Invalid Max-Repetitions value", invalidMaxRep, "should not be set as target max-repetitions")
		}
	}

	//tests for MsgMaxSize field
	if target.MsgMaxSize() != 484 {
		t.Error("Expected default value for MsgMaxSize is 484, but got,", target.MsgMaxSize())
	}
	target.SetMsgMaxSize(5000)
	if target.MsgMaxSize() != 5000 {
		t.Error("Expected target MsgMaxSize to be 5000, but got,", target.MsgMaxSize())
	}

	invalidMaxSizes := []int32{-4, 0, 100}
	for _, invalidMaxSize := range invalidMaxSizes {
		if target.MsgMaxSize() == invalidMaxSize {
			t.Error("Invalid MsgMaxSize value", invalidMaxSize, "should not be set as target MsgMaxSize")
		}
	}

	target.ReleaseResources()
}

func TestTargetV3Fields(t *testing.T) {

	target := NewSnmpTarget()

	//tests for principal field
	if !strings.EqualFold(target.Principal(), "") {
		t.Error("Expected default principal is empty string, instead it is,", target.Principal())
	}
	target.SetPrincipal("noAuthUser")
	if !strings.EqualFold(target.Principal(), "noAuthUser") {
		t.Error("Expected target principal is noAuthUser, but got,", target.Principal())
	}

	//tests for AuthProtocol field
	if target.AuthProtocol() != NO_AUTH {
		t.Error("Expected default authProtocol is", NO_AUTH, "instead it is,", target.AuthProtocol())
	}
	target.SetAuthProtocol(MD5_AUTH)
	if target.AuthProtocol() != MD5_AUTH {
		t.Error("Expected target authProtocol is", MD5_AUTH, "but got,", target.AuthProtocol())
	}
	target.SetAuthProtocol(consts.AuthProtocol("invalid"))
	if target.AuthProtocol() == consts.AuthProtocol("invalid") {
		t.Error("AuthProtocol other than NO_AUTH, MD5_AUTH && SHA_AUTH should not be set")
	}

	//tests for authPassword field
	if !strings.EqualFold(target.AuthPassword(), "") {
		t.Error("Expected default authPassword is empty string, instead it is,", target.AuthPassword())
	}
	target.SetAuthPassword("authUser")
	if !strings.EqualFold(target.AuthPassword(), "authUser") {
		t.Error("Expected target authPassword is authUser, but got,", target.AuthPassword())
	}

	//tests for PrivProtocol field
	if target.PrivProtocol() != NO_PRIV {
		t.Error("Expected default privProtocol is", NO_PRIV, "instead it is,", target.PrivProtocol())
	}
	target.SetPrivProtocol(DES_PRIV)
	if target.PrivProtocol() != DES_PRIV {
		t.Error("Expected target privProtocol is", DES_PRIV, "but got,", target.PrivProtocol())
	}
	target.SetPrivProtocol(consts.PrivProtocol("invalid"))
	if target.PrivProtocol() == consts.PrivProtocol("invalid") {
		t.Error("PrivProtocol other than NO_PRIV, DES_PRIV, TRIPLE_DES_PRIV, AES_128_PRIV, AES_192_PRIV && AES_256_PRIV should not be set")
	}

	//tests for privPassword field
	if !strings.EqualFold(target.PrivPassword(), "") {
		t.Error("Expected default privPassword is empty string, instead it is,", target.PrivPassword())
	}
	target.SetPrivPassword("privUser")
	if !strings.EqualFold(target.PrivPassword(), "privUser") {
		t.Error("Expected target privPassword is privUser, but got,", target.PrivPassword())
	}

	//tests for ContextName field
	if !strings.EqualFold(target.ContextName(), "") {
		t.Error("Expected default contextName is empty string, instead it is,", target.ContextName())
	}
	target.SetContextName("priv")
	if !strings.EqualFold(target.ContextName(), "priv") {
		t.Error("Expected target contextName is priv, but got,", target.ContextName())
	}

	//tests for contextEngineID field
	if !bytes.EqualFold(target.ContextEngineID(), []byte{}) {
		t.Error("Expected default contextEngineID is [], instead it is,", target.ContextEngineID())
	}
	target.SetContextEngineID([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0})
	if !bytes.EqualFold(target.ContextEngineID(), []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) {
		t.Error("Expected target contextEngineID is [1 2 3 4 5 6 7 8 9 0], but got,", target.ContextEngineID())
	}

	//tests for ValidateUser field
	if target.IsValidateUser() {
		t.Error("Expected ValidateUser not to be set by default")
	}
	target.ValidateUser(true)
	if !target.IsValidateUser() {
		t.Error("Expected ValidateUser to be set to true")
	}
}

func TestTargetV3Utils(t *testing.T) {

	target := NewSnmpTarget()
	conf := Configuration()
	setTargetConf(target, conf)
	target.USMUserLCD().RemoveAllUsers()

	//tests for CreateV3Tables method
	target.SetVersion(Version3)
	out, err := target.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, error :", err)
	}

	if _, err := target.Get(); err != nil {
		t.Error("Get should not return any V3 errors if UsmEntry present in the table")
	}

	//test for CreateV3Tables if entry already present
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -3 {
			t.Error("CreateV3Tables should return -3 if UserEntry already present, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if UserEntry already present, instead it throws,", err)
	}

	//test for createV3Tables for user of security level AUTH_NO_PRIV
	target.SetPrincipal("authUser")
	target.SetAuthProtocol(MD5_AUTH)
	target.SetAuthPassword("authUser")
	target.SetContextName("auth")
	out, err = target.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, err :", err)
	}

	if _, err := target.Get(); err != nil {
		t.Error("Get should not return any V3 errors if UsmEntry present in the table, instead it throws,", err)
	}

	//test for createV3Tables for user of security level AUTH_PRIV

	//tests for security level - AUTH_PRIV
	//Invalid auth protocol
	target.SetPrincipal("privUser")
	target.SetAuthProtocol(SHA_AUTH)
	target.SetAuthPassword("privUser")
	target.SetPrivProtocol(DES_PRIV)
	target.SetPrivPassword("privUser")
	target.SetContextName("priv")
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect auth protocol is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect auth protocol is set")
	}

	//test Get if usm-entry not present in the usm table
	if _, err := target.Get(); err == nil {
		t.Error("Get should return a V3 error if UsmEntry is not present in the usm table")
	}

	//invalid auth password case
	target.SetAuthProtocol(MD5_AUTH)
	target.SetAuthPassword("invalidPassword")
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect auth password is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect auth password is set")
	}

	//invalid priv protocol case
	target.SetAuthPassword("privUser")
	target.SetPrivProtocol(AES_128_PRIV)
	target.ValidateUser(true)
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect priv protocol is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect priv protocol is set")
	}

	//invalid priv password case
	target.SetPrivProtocol(DES_PRIV)
	target.SetPrivPassword("invalidPassword")
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if incorrect priv password is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if incorrect priv password is set")
	}

	//valid values case for priv user
	target.SetPrivPassword("privUser")
	out, err = target.CreateV3Tables()
	if err == nil {
		if out != 1 {
			t.Error("CreateV3Tables should return 1 for valid values, instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should not return error for valid values, err :", err)
	}

	//test for AUTH_PRIV request
	if _, err := target.Get(); err != nil {
		t.Error("Get should not return any V3 errors if UsmEntry present in the table")
	}

	//invalid version case
	target.SetVersion(Version1)
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != 0 {
			t.Error("CreateV3Tables should return 0 for invalid version (version other than Version3), instead it returned,", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid version (version other than Version3) is set")
	}

	//invalid username case
	target.SetVersion(Version3)
	target.SetPrincipal("invalidUser")
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -2 {
			t.Error("CreateV3Tables should return -2 if invalid username is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid username is set")
	}

	target.SetPrincipal("privUser")
	target.SetTargetPort(9001)
	fmt.Println("Testing CreateV3Tables for discovery failed case. Wait for 5 seconds until request timeouts")
	out, err = target.CreateV3Tables()
	if err != nil {
		if out != -1 {
			t.Error("CreateV3Tables should return -1 if invalid host/port is set, instead it returned", out)
		}
	} else {
		t.Error("CreateV3Tables should return error if invalid host/port is set")
	}

	target.ReleaseResources()

	//tests for ManageV3Tables method
	target1 := NewSnmpTarget()
	//conf := Configuration()
	setTargetConf(target1, conf)

	//tests for ManageV3Tables method
	target1.SetVersion(Version3)
	if err = target1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should return not return error for valid values, instead it returned,", err)
	}

	//test for ManageV3Tables for user of security level AUTH_NO_PRIV
	target1.SetPrincipal("authUser")
	target1.SetAuthProtocol(MD5_AUTH)
	target1.SetAuthPassword("authUser")
	target1.SetContextName("auth")
	if err = target1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should return not return error for valid values")
	}

	//invalid auth protocol
	target1.SetPrincipal("privUser")
	target1.SetAuthProtocol(SHA_AUTH)
	target1.SetAuthPassword("privUser")
	target1.SetPrivProtocol(DES_PRIV)
	target1.SetPrivPassword("privUser")
	target1.SetContextName("priv")
	if err = target1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect auth protocol is set")
	}

	//invalid auth password case
	target1.SetAuthProtocol(MD5_AUTH)
	target1.SetAuthPassword("invalidPassword")
	if err = target.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect auth password is set")
	}

	//invalid priv protocol case
	target1.SetAuthPassword("privUser")
	target1.SetPrivProtocol(AES_128_PRIV)
	target1.ValidateUser(true)
	if err = target1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect priv protocol is set")
	}

	//invalid priv password case
	target1.SetPrivProtocol(DES_PRIV)
	target1.SetPrivPassword("invalidPassword")
	if err = target.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if incorrect priv password is set")
	}

	//valid values case for priv user
	target1.SetPrivPassword("privUser")
	if err = target1.ManageV3Tables(); err != nil {
		t.Error("ManageV3Tables should return not return error for valid values, err :", err)
	}

	//invalid version case
	target1.SetVersion(Version1)
	if err = target1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error for invalid version (version other than Version3)")
	}

	//invalid username case
	target1.SetVersion(Version3)
	target1.SetPrincipal("invalidUser")
	if err = target1.ManageV3Tables(); err == nil {
		t.Error("ManageV3Tables should return error if invalid username is set")
	}

	target1.SetPrincipal("privUser")
	target1.SetTargetPort(9001)
	fmt.Println("Testing ManageV3Tables for discovery failed case. Wait for 5 seconds until request timeouts")
	err = target1.ManageV3Tables()
	if err == nil {
		t.Error("ManageV3Tables should return error if invalid host/port is set")
	}

	target.ReleaseResources()
}

func TestTargetRequests(t *testing.T) {

	target := NewSnmpTarget()

	if _, err := target.Get(); err == nil {
		t.Error("Get should throw error if no OID is specified")
	}

	conf := Configuration()
	setTargetConf(target, conf)

	//#################### SNMP GET TESTS ######################//
	fmt.Println("Testing for various get requests started")
	if _, err := target.Get(); err != nil {
		t.Error("Get should not throw any error for valid values, instead it throws :", err)
	}

	validOids := []interface{}{"1.1.0", snmpvar.NewSnmpOID("1.2.0"), snmpvar.NewSnmpOID("1.3.0"), []uint32{1, 3, 6, 1, 2, 1, 1, 4, 0}}
	for _, validOid := range validOids {
		if _, err := target.GetOID(validOid); err != nil {
			t.Error("GetOID should not throw any error for valid values, instead it throws :", err)
		}
	}

	if _, err := target.GetVariable(); err != nil {
		t.Error("GetVariable should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetVariableBinding(); err != nil {
		t.Error("GetVariableBinding should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetList(); err != nil {
		t.Error("GetList should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetVariables(); err != nil {
		t.Error("GetVariables should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetVariableBindings(); err != nil {
		t.Error("GetVariableBindings should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetOIDList([]string{"1.3.0", "1.4.0"}); err != nil {
		t.Error("GetOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP GET-NEXT TESTS ######################//
	fmt.Println("Testing for various get-next requests started")
	if _, err := target.GetNext(); err != nil {
		t.Error("GetNext should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextOID("1.3.0"); err != nil {
		t.Error("GetNextOID should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextVariable(); err != nil {
		t.Error("GetNextVariable should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextVariableBinding(); err != nil {
		t.Error("GetNextVariableBinding should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextList(); err != nil {
		t.Error("GetNextList should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextVariables(); err != nil {
		t.Error("GetNextVariables should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextVariableBindings(); err != nil {
		t.Error("GetNextVariableBindings should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetNextOIDList([]string{"1.3.0", "1.4.0"}); err != nil {
		t.Error("GetNextOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP GET-ALL TESTS ######################//
	fmt.Println("Testing for various get-all requests started")

	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})

	if _, err := target.GetAllList(); err != nil {
		t.Error("GetAllList should not throw any error for valid values, instead it throws :", err)
	}

	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})
	if _, err := target.GetAllVariables(); err != nil {
		t.Error("GetAllVariables should not throw any error for valid values, instead it throws :", err)
	}

	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})
	if _, err := target.GetAllVariableBindings(); err != nil {
		t.Error("GetAllVariableBindings should not throw any error for valid values, instead it throws :", err)
	}

	target.SetOIDList([]string{"1.4.0", "1.5.0"})

	//#################### SNMP GET-ALL TESTS ######################//
	fmt.Println("Testing for various get-all requests started")
	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})
	if _, err := target.GetAllList(); err != nil {
		t.Error("GetAllList should not throw any error for valid values, instead it throws :", err)
	}
	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})
	if _, err := target.GetAllVariables(); err != nil {
		t.Error("GetAllVariables should not throw any error for valid values, instead it throws :", err)
	}
	target.SetOIDList([]string{".1.3.6.1.2.1.2.2.1.2", ".1.3.6.1.2.1.2.2.1.3"})
	if _, err := target.GetAllVariableBindings(); err != nil {
		t.Error("GetAllVariableBindings should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP SET TESTS ######################//
	fmt.Println("Testing for various set requests started")

	varSlice := []snmpvar.SnmpVar{snmpvar.NewSnmpString("setTest"), snmpvar.NewSnmpString("setTest1")}
	target.SetOIDList([]string{"1.4.0", "1.5.0"})
	if _, err := target.Set("TestSet", consts.OctetString); err != nil {
		t.Error("Set should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.SetVariable(varSlice[0]); err != nil {
		t.Error("SetVariable should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.SetVariables(varSlice); err != nil {
		t.Error("SetVariables should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.SetVariableList(varSlice); err != nil {
		t.Error("SetVariableList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP GET-BULK TESTS ######################//
	fmt.Println("Testing for various get-bulk requests started")
	target.SetVersion(Version2C)
	if _, err := target.GetBulkList(); err != nil {
		t.Error("GetBulkList should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetBulkVariables(); err != nil {
		t.Error("GetBulkVariables should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetBulkVariableBindings(); err != nil {
		t.Error("GetBulkVariableBindings should not throw any error for valid values, instead it throws :", err)
	}

	if _, err := target.GetBulkOIDList([]string{"1.4.0", "1.5.0"}, 0, 50); err != nil {
		t.Error("GetBulkOIDList should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP INFORM TESTS ######################//
	if _, err := target.SendInform(10000, ".1.3.6.1.2.1.11", []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendInform should not throw any error for valid values, instead it throws :", err)
	}

	//#################### SNMP TRAP TESTS ######################//
	target.SetTargetPort(conf.trapPort)
	target.SetVersion(Version1)
	if err := target.SendTrap(".1.3.6.1.2.1.11", "localhost", 2, 0, 10000, []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendTrap should not throw any error for valid values, instead it throws :", err)
	}

	target.SetVersion(Version2C)
	if err := target.SendV2Trap(10000, *snmpvar.NewSnmpOID(".1.3.6.1.2.1.11"), []snmpvar.SnmpVar{snmpvar.NewSnmpString("informTest"), snmpvar.NewSnmpString("infTest")}); err != nil {
		t.Error("SendTrap should not throw any error for valid values, instead it throws :", err)
	}

	target.SetTargetPort(conf.port)

	target.ReleaseResources()
}

func Configuration() *targetConf {

	conf := new(targetConf)
	conf.host = "localhost"
	conf.port = 8001
	conf.trapPort = 8003
	conf.oidlist = []string{"1.4.0", "1.5.0"}

	return conf
}

func setTargetConf(targ *SnmpTarget, conf *targetConf) {

	targ.SetTargetHost(conf.host)
	targ.SetTargetPort(conf.port)
	targ.SetOIDList(conf.oidlist)

	targ.SetPrincipal("noAuthUser")
	targ.SetContextName("noAuth")
}
