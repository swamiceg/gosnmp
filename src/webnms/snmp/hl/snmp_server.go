/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"sync"
)

var server *snmpServer //To hold the single instance of SnmpServer
type snmpServer struct {
	snmpStore map[string](map[string]*value) //Inner map store the multiple instances of LL API like SnmpAPI, SnmpSession etc.,
	mut       sync.Mutex                     //For synchronization
}

//To hold the LL API's intance along with the concurrent users sharing this resource
type value struct {
	llInstance      interface{} //Instance of the LL API
	concurrentUsers int         //To store the number of HL API's sharing the resource
}

//Return a pointer to SnmpServer.
//Creates new instance if one is not exist already, else returns the instance created already.
func newSnmpServer() *snmpServer {
	if server == nil {
		//fmt.Println("Creating new SnmpServer")
		server = &snmpServer{
			snmpStore: make(map[string](map[string]*value)),
		}
	}

	return server
}
