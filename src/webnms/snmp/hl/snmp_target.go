/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package hl

import (
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"webnms/log"
	"webnms/snmp"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	trans "webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
	"webnms/snmp/util"
)

//SnmpTarget is a High-Level API that aids in performing all kind of SNMP Operations (Get/Get-Next/Set etc.,) at ease.
//
//It provides various handler methods, which synchronously sends the SNMP request to target and returns the response to the user.
//It contains a list of OIDs and Variables list which will be used for any requests that happens through this SnmpTarget.
//It maintains the instances of Low-Level resources like SnmpSession, SnmpAPI.
//
//  target := hl.NewSnmpTarget()
//  target.SetTargetHost("localhost")
//  target.SetOIDList([]string{"1.4.0", "1.5.0"})
//  resStr, _ := target.Get()
//
//SnmpTarget support multi-varbind requests. Multiple OIDs can be set using SetOIDList(), AddOIDs() etc. SNMP operations with this SNMP Target will use this list.
//
//For garbage collection of the resources used, ReleaseResources() method can be invoked.
//Different SnmpTarget instance will reuse the SnmpAPI and SnmpSession instance. So, for releasing these resources, ReleaseResources() method should be called on every
//SnmpTarget instance created and when this method is called on the final instance of SnmpTarget, these will be garbage collected.
type SnmpTarget struct {
	*snmpServer
	handler requestHandler

	session         *snmp.SnmpSession
	api             *snmp.SnmpAPI
	protocolOptions trans.ProtocolOptions
	errorMsgs       ErrorMessages

	localHost   string
	localPort   int
	targetHost  string
	targetPort  int
	sessionName string

	oidList        []snmpvar.SnmpOID
	varList        []snmpvar.SnmpVar
	version        consts.Version
	community      string
	timeout        int
	retries        int
	nonRepeaters   int32
	maxRepetitions int32

	msgMaxSize      int32
	principal       string
	authProtocol    consts.AuthProtocol
	authPassword    string
	privProtocol    consts.PrivProtocol
	privPassword    string
	contextName     string
	contextEngineID []byte
	validateUser    bool //To validate AuthPriv User
	secModelID      int32

	errorCode      int
	errorIndex     int
	errorString    string
	exceptionCode  int
	exceptionCodes map[int]int

	attemptPartial  bool
	attemptComplete bool
	varBindsPerReq  int
	maxRows         int
	transModel      engine.SnmpTransportModel //For custom transport model
}

//NewSnmpTarget creates and returns a new SnmpTarget instance.
//
//  Note:
//  * It will open a listener and bind to any available port to receive SNMP packets.
//  * It uses UDP by default for communication.
//  * It shares the low-level resources.
func NewSnmpTarget() *SnmpTarget {
	var target *SnmpTarget
	var err error
	if target, err = newTarget(nil, 0, "", nil); err != nil {
		//log.Fatal(err)
		return nil
	}
	return target
}

//NewSnmpTargetBySession creates and returns new SnmpTarget by custom SnmpSession name and localport.
//If the sessionName is same for multiple targets, it will share the same session. If the port is 0 it will use any available port.
//
//  Note: Use this function, if you want to use a dedicated SnmpSession (low-level resource) for the SnmpTarget.
func NewSnmpTargetBySession(sessionName string, localPort int) *SnmpTarget {
	var (
		target *SnmpTarget
		err    error
	)
	if target, err = newTarget(nil, localPort, sessionName, nil); err != nil {
		//log.Fatal(err)
		return nil
	}
	return target
}

//NewSnmpTargetByProtocolOptions creates and returns new SnmpTarget using the ProtocolOptions and TransportModel.
//
//  Note:
//  * Use this function, if you want to use transport protocols other than UDP (or)
//    if you require the target to be opened in any specific address/port.
//  * This function can be used when using custom transport protocol. TransportModel instance should not be nil in this case.
//  * For UDP/TCP, transportModel instance can be nil.
func NewSnmpTargetByProtocolOptions(protocolOptions trans.ProtocolOptions, transportModel engine.SnmpTransportModel) *SnmpTarget {
	var (
		target *SnmpTarget
		err    error
	)
	if target, err = newTarget(protocolOptions, 0, "", transportModel); err != nil {
		//log.Fatal(err)
		return nil
	}
	return target
}

//Internal function to create SnmpTarget
func newTarget(protoOptions trans.ProtocolOptions, port int, sesName string, transportModel engine.SnmpTransportModel) (*SnmpTarget, error) {
	//Set UDP by default
	if protoOptions == nil {
		udp := snmp.NewUDPProtocolOptions()
		udp.SetLocalPort(port)
		protoOptions = udp
	} else {
		//In case protocolIOptions is provided we might need a dedicated SnmpSession to be used
		if strings.TrimSpace(sesName) == "" {
			sesName = fmt.Sprintf("CUST_PROTO:%p", protoOptions) //Address of the ProtocolOptions is used for constructing unique name.
		}
	}

	target := &SnmpTarget{
		//Perform initialization
		protocolOptions: protoOptions,
		localPort:       port,
		sessionName:     sesName,
		localHost:       "localhost",
		targetHost:      "localhost",
		targetPort:      161,
		version:         Version1,
		community:       "public",
		maxRepetitions:  50,
		timeout:         5000, //in milliseconds
		msgMaxSize:      484,
		authProtocol:    NO_AUTH,
		privProtocol:    NO_PRIV,
		secModelID:      security.USMID, //Be default let's use USM.
		errorCode:       Err_No_Error,
		errorIndex:      0,
		errorString:     "No error",
		exceptionCode:   -1,
		transModel:      transportModel,
		maxRows:         1000,
	}
	target.snmpServer = newSnmpServer()
	if err := target.initSnmpStore(); err != nil {
		log.Error("Error in initializing the SnmpStore. Failed to created SnmpTarget instance.")
		return target, err
	}

	return target, nil
}

//Initializes the SnmpServer's SnmpStore, and create new (or) reuse the SnmpSession/SnmpAPI from SnmpStore.
func (t *SnmpTarget) initSnmpStore() error {
	t.sessionName = t.getSessionName()

	var ok bool
	//Take the SnmpAPI instance
	if _, ok = t.snmpStore["API"]; !ok { //Creating API entry for very first time - new entry
		t.api = snmp.NewSnmpAPI()
		im := make(map[string]*value)
		im["API"] = &value{t.api, 1}
		t.snmpStore["API"] = im
	} else if _, ok = t.snmpStore["API"]["API"]; !ok { //Creating new SnmpAPI instance for first time
		t.api = snmp.NewSnmpAPI()
		im := t.snmpStore["API"]
		im["API"] = &value{t.api, 1}
	} else if val, ok := t.snmpStore["API"]["API"]; ok { //Reusing the already created SnmpAPI instance
		t.api = val.llInstance.(*snmp.SnmpAPI)
		val.concurrentUsers++
	}

	//Take the SnmpSession instance
	if _, ok = t.snmpStore["SESSION"]; !ok { //Creating SESSION for very first time - new entry
		t.session = snmp.NewSnmpSession(t.api) //Creating new SnmpSession
		im := make(map[string]*value)
		im[t.sessionName] = &value{t.session, 1}
		t.snmpStore["SESSION"] = im
	} else if _, ok = t.snmpStore["SESSION"][t.sessionName]; !ok { //Creating new SnmpSession instance for first time
		t.session = snmp.NewSnmpSession(t.api) //Creating new SnmpSession
		im := t.snmpStore["SESSION"]
		im[t.sessionName] = &value{t.session, 1}
	} else if val, ok := t.snmpStore["SESSION"][t.sessionName]; ok { //Reusing the already created SnmpSession instance
		t.session = val.llInstance.(*snmp.SnmpSession) //Reusing the exisiting SnmpSession
		val.concurrentUsers++
	}

	//Register the transport model in case custom transport model is used
	if t.transModel != nil {
		t.session.RegisterTransportModel(t.transModel)
	}

	//Try to open new SnmpSession only if you have newly created the SnmpSession (first time).
	if !t.session.IsSessionOpen() && (t.snmpStore["SESSION"][t.sessionName].concurrentUsers == 1) {
		//While opening a Session, we should set the ProtocolOptions
		if t.protocolOptions != nil {
			t.session.SetProtocolOptions(t.protocolOptions)
		}

		if err := t.session.Open(); err != nil {
			delete(t.snmpStore["SESSION"], t.sessionName)
			t.snmpStore["API"]["API"].concurrentUsers--
			return err
		}
	}

	t.errorMsgs, _ = newErrMap()
	initV3Errors()

	return nil
}

var v3ErrMap map[string]string

func initV3Errors() {

	if v3ErrMap == nil {
		v3ErrMap = make(map[string]string)

		v3ErrMap[unSupportedSecurityLevelOid] = "usmStatsUnsupportedSecurityLevel(.1.3.6.1.6.3.15.1.1.1.0) Counter value = "
		v3ErrMap[notInTimeWindowsOid] = "usmStatsNotInTimeWindows(.1.3.6.1.6.3.15.1.1.2.0) Counter value = "
		v3ErrMap[unknownUserNamesOid] = "usmStatsUnknownUserNames(.1.3.6.1.6.3.15.1.1.3.0) Counter value = "
		v3ErrMap[unknownEngineIdOid] = "usmStatsUnknownEngineIDs(.1.3.6.1.6.3.15.1.1.4.0) Counter value = "
		v3ErrMap[wrongDigestsOid] = "usmStatsWrongDigests(.1.3.6.1.6.3.15.1.1.5.0) Counter value = "
		v3ErrMap[decryptErrorOid] = "usmStatsDecryptionErrors(.1.3.6.1.6.3.15.1.1.6.0) Counter value = "
	}

}

//####### SnmpTarget - Getter and Setter methods #########//

//SetOIDList sets the list of ObjectIDs in SnmpTarget t.
//
//oidList can be a slice of type string (or) SnmpOID.
//For other types, this method sets the target's error-code to Illegal_Argument and returns.
func (t *SnmpTarget) SetOIDList(oidList interface{}) *SnmpTarget {
	t.oidList = []snmpvar.SnmpOID{}
	if oidList != nil {
		switch list := oidList.(type) {
		case []string:
			dropCount := 0
			for _, oidStr := range list {
				if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
					t.oidList = append(t.oidList, *oid)
				} else {
					dropCount++
					log.Info("Failure in adding the OID: '%s' to the Target. Invalid OID.", oidStr) //Invalid OID String
				}
			}
			//log.Println("Dropped count:",dropCount)
		case []snmpvar.SnmpOID:
			t.oidList = list
		default:
			t.errorCode = Illegal_Argument
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Info("Failure in adding the OID: '%v' to the Target. Invalid OID Type.", list)
			return t
		}
	} else {
		t.oidList = nil
	}

	return t
}

//OIDList returns a slice of ObjectID strings set on this SnmpTarget t.
func (t *SnmpTarget) OIDList() []string {
	oidStrSlice := make([]string, len(t.oidList))
	for i, oid := range t.oidList {
		oidStrSlice[i] = oid.String()
	}
	return oidStrSlice
}

//AddOID adds the ObjectID into SnmpTarget t's ObjectID List.
//
//oid can be of type string OID, snmpvar.SnmpOID (or) *snmpvar.SnmpOID.
//For other types, this method simply returns after setting the target's error code to Illegal_Argument.
func (t *SnmpTarget) AddOID(oid interface{}) *SnmpTarget {
	if oid != nil {
		switch o := oid.(type) {
		case string:
			if oid := snmpvar.NewSnmpOID(o); oid != nil {
				t.oidList = append(t.oidList, *oid)
			} else {
				log.Info("Failure in adding the OID: '%s' to the Target. Invalid OID.", o) //Dropped. Invalid OID.
			}
		case snmpvar.SnmpOID:
			t.oidList = append(t.oidList, o)
		case *snmpvar.SnmpOID:
			t.oidList = append(t.oidList, *o)
		default:
			t.errorCode = Illegal_Argument
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Info("Failure in adding the OID: '%v' to the Target. Invalid OID Type.", o)
			return t
		}
	}

	return t
}

//AddOIDs adds the ObjectID list to the end of SnmpTarget t's ObjectID List.
//
//oids can be slice of type string, snmpvar.SnmpOID (or) *snmpvar.SnmpOID.
//For other types, this method simply returns after setting the target's error code to Illegal_Argument.
func (t *SnmpTarget) AddOIDs(oids interface{}) *SnmpTarget {
	if oids != nil {
		switch ls := oids.(type) {
		case []string:
			dropCount := 0
			for _, oidStr := range ls {
				if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
					t.oidList = append(t.oidList, *oid)
				} else {
					dropCount++
					log.Info("Failure in adding the OID: '%s' to the Target. Invalid OID.", oidStr) //Invalid OID String
				}
			}
			log.Info("OID dropped count using target's AddOIDs:", dropCount)
		case []snmpvar.SnmpOID:
			t.oidList = append(t.oidList, ls...)
		case []*snmpvar.SnmpOID:
			for _, oid := range ls {
				t.oidList = append(t.oidList, *oid)
			}
		default:
			t.errorCode = Illegal_Argument
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Info("Failure in adding the OIDs: '%v' to the Target. Invalid OID Type.", ls)
			return t
		}
	}

	return t
}

//AddOIDAt adds the ObjectID into the specified index of SnmpTarget t's ObjectID List. Index starts from 0.
//oid should be of type string, snmpvar.SnmpOID or *snmpvar.SnmpOID.
//
//  Will not be added in following cases:
//  1. if index is less than 0
//  2. if index is greater than t's ObjectID List length plus one
//  3. if oid type is other than string, snmpvar.SnmpOID (or) *snmpvar.SnmpOID
//
//For oid type other than the mentioned above, this method simply returns after setting the target's error code to Illegal_Argument.
func (t *SnmpTarget) AddOIDAt(index int, oid interface{}) *SnmpTarget {
	if index < 0 || index > len(t.oidList) || oid == nil {
		return t
	}
	switch o := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(o); snmpOID != nil {
			t.oidList = append(t.oidList[:index], append([]snmpvar.SnmpOID{*snmpOID}, t.oidList[index:]...)...)
		} else {
			log.Info("Failure in adding the OID: '%s' to the Target. Invalid OID.", o) //Dropped. Invalid OID.
		}
	case snmpvar.SnmpOID:
		t.oidList = append(t.oidList[:index], append([]snmpvar.SnmpOID{o}, t.oidList[index:]...)...)
	case *snmpvar.SnmpOID:
		t.oidList = append(t.oidList[:index], append([]snmpvar.SnmpOID{*o}, t.oidList[index:]...)...)
	default:
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Info("Failure in adding the OID: '%v' to the Target. Invalid OID Type.", o)
		return t
	}

	return t
}

//RemoveOIDAt removes the ObjectID from the SnmpTarget t's ObjectID List at specified 'index'.
//Does nothing if index is negative or is greater than the target's ObjectID List size plus one.
func (t *SnmpTarget) RemoveOIDAt(index int) *SnmpTarget {
	if index < 0 || index > len(t.oidList) {
		return t
	}
	t.oidList = append(t.oidList[:index], t.oidList[index+1:]...)

	return t
}

//OIDAt returns the ObjectID at specified index from the SnmpTarget t's ObjectID List as a string. Index starts at 0.
//Returns "<nil>" if index is less than 0 or index is greater than the ObjectID List.
func (t *SnmpTarget) OIDAt(index int) string {
	if index < 0 || index > len(t.oidList) {
		//log.Println("Invalid index specified.")
		return "<nil>"
	}
	return t.oidList[index].String()
}

//SetVersion sets the SnmpVersion ver on this SnmpTarget t. Version1 is used by default.
func (t *SnmpTarget) SetVersion(ver consts.Version) *SnmpTarget {
	if ver == Version1 || ver == Version2C || ver == Version3 {
		t.version = ver
	}
	return t
}

//Version returns the SnmpVersion set on this SnmpTarget t.
func (t *SnmpTarget) Version() consts.Version { return t.version }

//SetCommunity sets the community string on SnmpTarget t, which will be used for all requests that happens through this target.
//Default value is public.
func (t *SnmpTarget) SetCommunity(comm string) *SnmpTarget {
	t.community = comm
	return t
}

//Community returns the community string set on this SnmpTarget t.
func (t *SnmpTarget) Community() string { return t.community }

//SetDebug determines if debug output will be printed on the console.
func (t *SnmpTarget) SetDebug(debug bool) *SnmpTarget {
	t.api.SetDebug(debug)
	return t
}

//Debug returns bool value indicating whether debug is enabled on SnmpTarget t.
func (t *SnmpTarget) Debug() bool { return t.api.Debug() }

//SetTargetHost sets the target hostname on t. 'host' added will be used as a target for all communications happens through t.
func (t *SnmpTarget) SetTargetHost(host string) *SnmpTarget {
	t.targetHost = host
	t.protocolOptions.SetRemoteHost(host)

	return t
}

//SetTargetHostPort sets the hostname and port on the SnmpTarget t.
//
//hostPort string should be of the form "host:port", "[host]:port", etc. Ex: localhost:161
//Returns error if hostPort string is invalid.
func (t *SnmpTarget) SetTargetHostPort(hostPort string) error {
	host, port, err := net.SplitHostPort(hostPort)
	if err != nil {
		return err
	}
	if host != "" {
		t.targetHost = host
		t.protocolOptions.SetRemoteHost(host)
	}
	if port != "" {
		if portNum, err := strconv.Atoi(port); err != nil {
			return err
		} else {
			t.SetTargetPort(portNum)
			t.protocolOptions.SetRemotePort(portNum)
		}
	}

	return nil
}

//TargetHost returns the hostname string set on t.
func (t *SnmpTarget) TargetHost() string { return t.targetHost }

//SetTargetPort sets the target port number on t. Port number should range between 0 and 65535.
func (t *SnmpTarget) SetTargetPort(port int) *SnmpTarget {
	if port >= 0 && port <= 65535 {
		t.targetPort = port
		t.protocolOptions.SetRemotePort(port)
	}
	return t
}

//TargetPort returns the target port number set on t.
func (t *SnmpTarget) TargetPort() int { return t.targetPort }

//SetTimeout sets the timeout value on t in seconds.
//The timeout is the time to wait for the first response in seconds, before attempting a retransmission
//
//Default value is 5 seconds.
func (t *SnmpTarget) SetTimeout(timeout int) *SnmpTarget {
	if timeout >= 0 && timeout*1000 != t.timeout {
		t.timeout = timeout * 1000
	}
	return t
}

//Timeout returns the timeout value set on t in seconds.
//Default value is 5 seconds.
func (t *SnmpTarget) Timeout() int { return (t.timeout / 1000) }

//SetRetries sets the number of retries before timeout on t. Default retries value is 0.
func (t *SnmpTarget) SetRetries(retries int) *SnmpTarget {
	if retries >= 0 {
		t.retries = retries
	}
	return t
}

//Retries returns the retries value set on t. Default value is 0.
func (t *SnmpTarget) Retries() int { return t.retries }

//SetNonRepeaters sets Non-Repeaters value on t, which will be used for Get-Bulk requests. Default value is 0.
//
//Sets the target's error-code to Invalid_Non_Rep and returns if a negative nonReps is specified.
func (t *SnmpTarget) SetNonRepeaters(nonReps int32) *SnmpTarget {
	if nonReps >= 0 {
		t.nonRepeaters = nonReps
	} else {
		t.errorCode = Invalid_Non_Rep
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
	}

	return t
}

//NonRepeaters returns the Non-Repeaters value set on t.
//Default value is 0.
func (t *SnmpTarget) NonRepeaters() int32 { return t.nonRepeaters }

//SetMaxRepetitions sets the Max-Repetitions value on t, which will be used for Get-Bulk requests.
//Default value is 50.
func (t *SnmpTarget) SetMaxRepetitions(maxReps int32) *SnmpTarget {
	if maxReps >= 0 {
		t.maxRepetitions = maxReps
	}
	return t
}

//MaxRepetitions returns the Max-Repetitions value set on t.
//Default value is 50.
func (t *SnmpTarget) MaxRepetitions() int32 { return t.maxRepetitions }

//MsgMaxSize returns the message max size field for the SNMPV3 packet.
func (t SnmpTarget) MsgMaxSize() int32 {
	return t.msgMaxSize
}

//SetMsgMaxSize sets the message max size field for the SNMPV3 packet. If maxSize is less than 484, it will be set to 484.
func (t *SnmpTarget) SetMsgMaxSize(maxSize int32) *SnmpTarget {
	t.msgMaxSize = maxSize
	if maxSize < 484 {
		t.msgMaxSize = 484
	}

	return t
}

//SetPrincipal sets the SNMPv3 principal/security-name.
func (t *SnmpTarget) SetPrincipal(userName string) *SnmpTarget {
	t.principal = userName
	return t
}

//Principal returns the SNMPv3 principal/security-name.
func (t *SnmpTarget) Principal() string { return t.principal }

//SetAuthProtocol sets the SNMPv3 auth protocol. The possible options are hl.NO_AUTH, hl.MD5_AUTH & hl.SHA_AUTH.
func (t *SnmpTarget) SetAuthProtocol(authProto consts.AuthProtocol) *SnmpTarget {
	if authProto == NO_AUTH || authProto == MD5_AUTH || authProto == SHA_AUTH {
		t.authProtocol = authProto
	}
	return t
}

//AuthProtocol returns the SNMPv3 auth protocol.
func (t *SnmpTarget) AuthProtocol() consts.AuthProtocol { return t.authProtocol }

//SetAuthPassword sets the SNMPv3 auth password.
func (t *SnmpTarget) SetAuthPassword(authPass string) *SnmpTarget {
	t.authPassword = authPass
	return t
}

//AuthPassword returns the SNMPv3 auth password.
func (t *SnmpTarget) AuthPassword() string { return t.authPassword }

//SetPrivProtocol sets the SNMPv3 priv protocol. Valid priv protocols are hl.NO_PRIV, hl.DES_PRIV, hl.TRIPLE_DES_PRIV, hl.AES_128_PRIV, hl.AES_192_PRIV & hl.AES_256_PRIV.
func (t *SnmpTarget) SetPrivProtocol(privProto consts.PrivProtocol) *SnmpTarget {
	if privProto == NO_PRIV || privProto == DES_PRIV || privProto == TRIPLE_DES_PRIV || privProto == AES_128_PRIV || privProto == AES_192_PRIV || privProto == AES_256_PRIV {
		t.privProtocol = privProto
	}
	return t
}

//PrivProtocol returns the SNMPv3 priv protocol.
func (t *SnmpTarget) PrivProtocol() consts.PrivProtocol { return t.privProtocol }

//SetPrivPassword sets the SNMPv3 priv password.
func (t *SnmpTarget) SetPrivPassword(privPass string) *SnmpTarget {
	t.privPassword = privPass
	return t
}

//PrivPassword returns the SNMPv3 priv password.
func (t *SnmpTarget) PrivPassword() string { return t.privPassword }

//SetContextName sets the SNMPv3 context name.
func (t *SnmpTarget) SetContextName(cName string) *SnmpTarget {
	t.contextName = cName
	return t
}

//ContextName returns the SNMPv3 context name.
func (t *SnmpTarget) ContextName() string { return t.contextName }

//SetContextEngineID sets the SNMPv3 context engine id.
func (t *SnmpTarget) SetContextEngineID(cID []byte) *SnmpTarget {
	t.contextEngineID = cID
	return t
}

//ContextEngineID returns the SNMPv3 context engine id.
func (t *SnmpTarget) ContextEngineID() []byte { return t.contextEngineID }

//ValidateUser enables the validation of the users for SNMPv3 cases.
//If enabled, validation will be performed for the user,
//
//	* NoAuthNoPriv: Simple GetNext-Request is sent to see if the user exist in the remote entity.
//	* AuthPriv: GetNext-Request as AuthPriv security level will be sent to validate the PrivProtocol and PrivPassword.
func (t *SnmpTarget) ValidateUser(validate bool) {
	t.validateUser = validate
}

//IsValidateUser returns bool indicating whether user validation in enabled on t for SNMPv3.
func (t *SnmpTarget) IsValidateUser() bool {
	return t.validateUser
}

//ErrorCode returns the error code for the last request made through t.
//
//Returns 0 if there is no error.
func (t *SnmpTarget) ErrorCode() int { return t.errorCode }

//ErrorString returns the error string for the last request made through t.
//
//Before sending any request the error string will be "No Error Code Registered".
//The Error index which is the index of the error in the PDU will be appended to this Error String.
//
//Returns an error string (or) "No Error" in case of no error.
func (t *SnmpTarget) ErrorString() string { return t.errorString }

//ExceptionCode returns the exception code for the last request made through t.
//Only for versions greater than SNMPV1.
//
//Returns 0 if there is no exception and -1 if no request is made through t.
func (t *SnmpTarget) ExceptionCode() int { return t.exceptionCode }

//ExceptionCodes returns a map of error index to exception code for the last request made through t.
//Only for versions greater than SNMPV1.
//
//Returns nil in case of no exceptions.
func (t *SnmpTarget) ExceptionCodes() map[int]int { return t.exceptionCodes }

//SetAttemptPartial sets the boolean state of whether this target will try to get partial data from an agent if the request is a multiple variable request.
func (t *SnmpTarget) SetAttemptPartial(part bool) *SnmpTarget {
	t.attemptPartial = part
	return t
}

//AttemptPartial returns the boolean state of whether this target will try to get partial data from an agent if the request is a multiple variable request.
func (t *SnmpTarget) AttemptPartial() bool { return t.attemptPartial }

//SetAttemptComplete sets the flag to get the complete data from the agent by splitting the varbinds into multiple request.
//This can set if request pdu is very big. The number of varbinds that should to be sent per request can be set using SetVarBindCount() method.
//By default pdu splitting is disabled.
func (t *SnmpTarget) SetAttemptComplete(complete bool) *SnmpTarget {
	t.attemptComplete = complete
	return t
}

//AttemptComplete returns if the splitting pdu is enabled to get the complete data by splitting the varbinds into multiple request. By default pdu splitting is disabled.
func (t *SnmpTarget) AttemptComplete() bool { return t.attemptComplete }

//SetVarBindsCount sets the number of varbinds per request to get the complete data from the agent by splitting the varbinds into multiple request in case if the request pdu is very big.
//Default value is 0.
func (t *SnmpTarget) SetVarBindsCount(varBindsCount int) *SnmpTarget {
	if varBindsCount > 0 {
		t.varBindsPerReq = varBindsCount
	}
	return t
}

//VarBindsCount returns the number of varbinds used with the splitted pdu. Default value is 0.
func (t *SnmpTarget) VarBindsCount() int {
	return t.varBindsPerReq
}

//SetMaxRowsCount sets the maximum number of rows that can be fetched, default value is 1000.
func (t *SnmpTarget) SetMaxRowsCount(rows int) *SnmpTarget {
	if rows > 0 {
		t.maxRows = rows
	}
	return t
}

//MaxRowsCount gets the maximum number of rows that can be fetched, default value is 1000.
func (t *SnmpTarget) MaxRowsCount() int { return t.maxRows }

//SetPacketBufferSize sets the Packet Buffer Size which will be used for receiving the snmp packets on t's SnmpSession.
//By default buffer size used is 65535.
func (t *SnmpTarget) SetPacketBufferSize(buffer int) *SnmpTarget {
	if t.session != nil && buffer > 0 {
		t.session.SetPacketBufferSize(buffer)
	}
	return t
}

//PacketBufferSize returns the Packet Buffer Size set on t's SnmpSession.
//Default value is 65535.
func (t *SnmpTarget) PacketBufferSize() int { return t.session.PacketBufferSize() }

//SetAutoInformResponse sets the automatic response flag for the Inform Request.
//If this flag is set to true, then the SNMP stack automatically sends a Get-Response message back to the sender. The default value is true.
func (t *SnmpTarget) SetAutoInformResponse(autoInform bool) *SnmpTarget {
	if t.session != nil {
		t.session.SetAutoInformResponse(autoInform)
	}
	return t
}

//AutoInformResponse returns bool indicating whether automatic response for Inform request is enabled or not.
func (t *SnmpTarget) AutoInformResponse() bool { return t.session.AutoInformResponse() }

//SetTimeoutPolicy sets TimeoutPolicy on this Session. Sets the user's own implementation of TimeoutPolicy.
//Default TimeoutPolicy is exponential.
func (t *SnmpTarget) SetTimeoutPolicy(timeoutPolicy snmp.TimeoutPolicy) *SnmpTarget {
	if t.session != nil {
		t.session.SetTimeoutPolicy(timeoutPolicy)
	}
	return t
}

//RestoreToDefaultTimeoutPolicy restores the TimeoutPolicy to default 'ExponentialTimeoutPolicy' on t's SnmpSession (low-level resource).
func (t *SnmpTarget) RestoreToDefaultTimeoutPolicy() {
	if t.session != nil {
		t.session.RestoreToDefaultTimeoutPolicy()
	}
}

//RegisterV3SecurityModel registers the msg security model to be used for SNMPv3.
//By default USM will be used.
//
//secModel should be an implementation of SnmpSecurityModel interface.
//This model will be registered with the security subsystem.
//
//This method should be used only when using custom security models.
//For more details, refer engine.SnmpSecurityModel doc.
func (t *SnmpTarget) RegisterV3SecurityModel(secModel engine.SnmpSecurityModel) {
	if secModel != nil {
		//Let's do the registeration of this security model with the security subsystem
		secSS := t.api.SnmpEngine().SecuritySubSystem()
		modelID := secModel.ID()
		if secSS.Model(modelID) == nil {
			//Model is not registered in SecSS
			secSS.AddModel(secModel)
		}
	}
}

//SetV3SecurityModel sets the default security model to be used for SNMPv3.
//By default USM will be used.
//
//  Note:
//  * ModelID should be already registered with the Security Subsystem.
//  * This method should be used only when using custom security models.
func (t *SnmpTarget) SetV3SecurityModel(secModelID int32) *SnmpTarget {
	t.secModelID = secModelID
	return t
}

//SecurityModel returns the name of the Security Model set on this target t.
func (t *SnmpTarget) SecurityModel() string {
	secSS := t.api.SnmpEngine().SecuritySubSystem()
	secModel := secSS.Model(t.secModelID)
	if secModel != nil {
		secModel.Name()
	}

	return "<nil>"
}

//CreateV3Tables creates new user entry and adds it to USMPeerEngineLCD. Discovery and Time-Synchronization will be performed, if applicable.
//Returns 1 if entry is successfully added, 0 or negative value otherwise.
//If an userEntry corresponding to the userName and the engineID(or host and port) is already present in the USMPeerEngineLCD, then no new user entry will be created.
//The error, if any, should be checked specifically with the method ErrorString/ErrorCode or the error returned.
//The following error codes can be set,
//
//  * Discovery_Failed
//  * Time_Sync_Failed
//  * UnSupported_SecurityLevel_Error
//  * Not_In_Time_Windows_Error
//  * Unknown_Usernames_Error
//  * Unknown_EngineID_Error
//  * Wrong_Digest_Error
//  * Decrypt_Error
//
//Returns,
//  0 if the version is other than v3.
//  1 if userTable is created Successfully.
//  -1 if discovery fails.
//  -2 if TimeSync fails.
//  -3 if User is already created for that host and port.
//
//Note: Only for USM Security Model
func (t *SnmpTarget) CreateV3Tables() (int, error) {
	start := 0
	log.Trace("* CreateV3Tables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	t.errorCode = Err_No_Error
	t.errorString = t.errorMsgs.ErrorString(t.errorCode)
	t.exceptionCode = -1

	if t.version != Version3 {
		errStr := "Invalid version to create v3 tables. Version should be V3."
		log.Trace("Returning.. Error: " + errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return 0, errors.New(errStr)
	}

	usmEngineLCD := t.session.API().USMPeerEngineLCD()
	remoteEngine := usmEngineLCD.Engine(t.protocolOptions.RemoteHost(), t.protocolOptions.RemotePort())
	var engineID []byte = nil
	if remoteEngine != nil &&
		remoteEngine.AuthoritativeEngineID() != nil &&
		len(remoteEngine.AuthoritativeEngineID()) > 0 {
		//Engine Entry for this remote entity already present.
		engineID = remoteEngine.AuthoritativeEngineID()
	}

	if engineID != nil {
		userLCD := t.session.API().USMUserLCD()
		usmSecureUser, _ := userLCD.SecureUser(engineID, t.principal)
		if usmSecureUser != nil {
			errStr := fmt.Sprintf("User entry already present for the user '%s'.", t.principal)
			log.Trace("Returning.. Error: " + errStr)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -3, errors.New(errStr)
		}
	}

	err := util.Init_V3_LCD(t.session,
		t.protocolOptions,
		t.principal,
		engineID,
		t.authProtocol,
		t.authPassword,
		t.privProtocol,
		t.privPassword,
		t.validateUser, //ValidateUser
	)

	//Check the error received.
	if err != nil {
		errorCode := err.(*util.InitV3Error).ErrorCode()

		switch errorCode {
		case consts.DiscoveryFailure:
			t.errorCode = Discovery_Failed
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning.. Error: " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, err
		case consts.TimeSyncFailure, consts.ValidationFailure, consts.DecryptionError, consts.UnknownUserNameError, consts.UnSupportedSecurityLevelError, consts.NotInTimeWindowsError, consts.UnknownEngineIDError, consts.WrongDigestsError:
			t.setCreateV3ErrorCode(errorCode)
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			t.exceptionCode = 0
			log.Trace("Returning.. Error: " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -2, err
		default:
			t.errorCode = Discovery_Failed
			t.errorString = t.errorMsgs.ErrorString(t.errorCode) + " : " + err.Error()
			log.Trace("Returning.. Error: " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return -1, err
		}
	}

	log.Trace("Returning.. V3 tables creation successful.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return 1, nil
}

//ManageV3Tables modifies the auth and priv parameters of the existing user entry, if any, or else creates new user entry and adds it to USMUserLCD.
//Discovery and Time-Synchronization will be performed, if applicable.
//The error, if any, should be checked using the method ErrorString/ErrorCode or the error returned.
//The following error codes can be set,
//
//  * Discovery_Failed
//  * Time_Sync_Failed
//  * UnSupported_SecurityLevel_Error
//  * Not_In_Time_Windows_Error
//  * Unknown_Usernames_Error
//  * Unknown_EngineID_Error
//  * Wrong_Digest_Error
//  * Decrypt_Error
//
//Note:
//
//	* Username cannot be modified
//	* Only for USM Security Model
func (t *SnmpTarget) ManageV3Tables() error {
	start := 0
	log.Trace("* ManageV3Tables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	t.errorCode = Err_No_Error
	t.errorString = t.errorMsgs.ErrorString(t.errorCode)
	t.exceptionCode = -1

	if t.version != Version3 {
		errStr := "Invalid version to create v3 tables. Version should be V3."
		log.Trace("Returning.. Error: " + errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return errors.New(errStr)
	}

	var err error = util.Init_V3_LCD(t.session,
		t.protocolOptions,
		t.principal,
		nil,
		t.authProtocol,
		t.authPassword,
		t.privProtocol,
		t.privPassword,
		t.validateUser,
	)

	if err != nil {
		errString := err.Error()
		errorCode := err.(*util.InitV3Error).ErrorCode()
		switch errorCode {
		case consts.DiscoveryFailure:
			t.errorCode = Discovery_Failed
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		case consts.TimeSyncFailure, consts.ValidationFailure, consts.DecryptionError, consts.UnknownUserNameError, consts.UnSupportedSecurityLevelError, consts.NotInTimeWindowsError, consts.UnknownEngineIDError, consts.WrongDigestsError:
			t.setCreateV3ErrorCode(errorCode)
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			t.exceptionCode = 0
		default:
			t.errorCode = Discovery_Failed
			t.errorString = t.errorMsgs.ErrorString(t.errorCode) + " : " + errString
		}

		log.Trace("Returning.. Error: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return err
	}

	log.Trace("Returning.. V3 tables creation/updation successful.")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return nil
}

//Returns instance of SnmpEngine, which holds all the SubSystems and their models.
//Represents a SNMP entity.
func (t *SnmpTarget) SnmpEngine() engine.SnmpEngine {
	if t.api != nil {
		return t.api.SnmpEngine()
	}
	return nil
}

//PeerEngineLCD returns the LCD of engine entries specific to the security model set on t.
//By default USM's PeerEngineLCD will be returned.
func (t *SnmpTarget) PeerEngineLCD() engine.SnmpPeerEngineLCD {
	return t.api.PeerEngineLCD(t.secModelID)
}

//USMUserLCD returns the User based security model's UserLCD instance, which contains details of
//Secure Users created under this model.
func (t *SnmpTarget) USMUserLCD() usm.USMUserLCD {
	return t.api.USMUserLCD()
}

//USMPeerEngineLCD returns the LCD of engine entries specific to USM.
//PeerEngineLCD contains details of the peer SNMP entities such as EngineID, EngineBoots etc.,
func (t *SnmpTarget) USMPeerEngineLCD() engine.SnmpPeerEngineLCD {
	return t.api.USMPeerEngineLCD()
}

//InitDB initializes the database connection using the driverName and dataSourceName provided.
//User should have loaded the specific DB driver to open Database connection.
//
//This opens the database connection and creates the necessary tables in the database required for the SNMP API.
//
//dialectID is an integer constant indicating the SQLDialect which the database will be using
//for performing database operations.
//Dialect is required for inter-operability between different database queries.
//
//Refer db package for the list of SQLDialect implementation and their associated constants. If specific dialect
//implementation is not available, user should implement their own and register the same using RegisterSQLDialect() method.
//Refer db.SQLDialect interface for more details.
//
//Returns error in case of any db related error occurs (or) Unsupported DialectID passed.
func (t *SnmpTarget) InitDB(driverName, dataSourceName string, dialectID int) error {
	start := 0
	dbStr := "DriverName: " + driverName + " DataSourceName: " + dataSourceName
	log.Trace("* InitDB(): %s;*", dbStr)
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Initialize the DB connection
	err := t.api.InitDB(driverName, dataSourceName, dialectID)
	if err != nil {
		errStr := "Error in establishing DB connection: " + err.Error()
		log.Trace(errStr)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return errors.New(errStr)
	}
	log.Trace("Succesfully established the DB connection and created the required tables")
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return nil
}

//RegisterSQLDialect adds/registers the new SQLDialect implementation on the database.
//dialect should be an implementation of db.SQLDialect interface.
//
//User should use this method when they want to provide the new implementation for the database
//other than provided by the API.
//
//Default implementation exist for: MySQL, Postgres, Sqlite3, SQLServer, Oracle, DB2, Sybase.
//
//Returns error in case of failure in registering the Dialect.
func (t *SnmpTarget) RegisterSQLDialect(dialectID int, dialect db.SQLDialect) error {
	return db.RegisterNewDialect(dialectID, dialect)
}

//SetDatabaseFlag enables/diables the usage of database for all the SNMP operations internally.
//If enabled DB will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
//
//Note: When Database usage is disabled in the middle, it may cause some adverse effect of re-synchronization to get the new values
//with all the SNMP entities and fill the local LCD.
func (t *SnmpTarget) SetDatabaseFlag(flag bool) {
	t.api.SetDatabaseFlag(flag)
}

//IsDatabaseFlag returns bool indicating whether database storage is enabled for SNMP operations.
//False indicates that the runtime memory (LCD) is used for storing/retriving V3 related data.
func (t *SnmpTarget) IsDatabaseFlag() bool {
	return t.api.IsDatabaseFlag()
}

//#################### SNMP GET ######################//

//Get sends SNMP Get request to the target host and returns the first SNMP Variable in the response as a string.
//This method will send Get request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) Get() (string, error) {
	start := 0
	log.Trace("* Get() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}
	varb, err := t.GetVariableBinding()
	if err != nil {
		log.Trace("Returning nil varbinds. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", err
	}

	varStr := varb.Variable().String()
	log.Trace("Returning variable string: " + varStr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varStr, nil
}

//GetOID sends SNMP Get request to the target host by using oid as ObjectID and returns the SNMP Variable in the response as a string.
//oid should be an ObjectID of type snmpvar.SnmpOID/*snmpvar.SnmpOID/string/[]uint32.
//
//Sets the relevant error-code and returns error, if 'oid' is of invalid type (or) any error in communication with the target host.
func (t *SnmpTarget) GetOID(oid interface{}) (string, error) {
	start := 0
	log.Trace("* GetOID() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	oidSlice := make([]snmpvar.SnmpOID, 2)
	switch snmpOid := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return "<nil>", errors.New(t.errorString)
		}
	case snmpvar.SnmpOID:
		oidSlice[0] = snmpOid
	case *snmpvar.SnmpOID:
		oidSlice[0] = *snmpOid
	case []uint32:
		if snmpOID := snmpvar.NewSnmpOIDByInts(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return "<nil>", errors.New(t.errorString)
		}
	default:
		//log.Println("Invalid OID Type")
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice[:1])
	varbSlice, err := t.getResponseVariableBindings(oidSlice[:1], consts.GetRequest)
	if err != nil {
		log.Trace("Returning nil varbinds. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", err
	}
	if len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	varStr := varbSlice[0].Variable().String()

	log.Trace("Returning variable string: " + varStr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varStr, nil
}

//GetVariable sends SNMP Get request to the target host and returns the first SNMP Variable in the response as snmpvar.SnmpVar.
//This method will send Get request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OID is set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetVariable() (snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetVariable() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varb, err := t.GetVariableBinding()
	if err != nil {
		log.Trace("Returning nil variables. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	variable := varb.Variable()
	log.Trace("Returning variable: " + variable.String())
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return variable, nil
}

//GetVariableBinding sends SNMP Get request to the target host and returns the first VariableBinding as *msg.SnmpVarBinds.
//This method will send Get request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetVariableBinding() (*msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetVariableBinding() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}

		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList[:1])
	varbSlice, err := t.getResponseVariableBindings(t.oidList[:1], consts.GetRequest)
	if err != nil {
		log.Trace("Returning nil variable binding. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable binding: " + varbSlice[0].String())
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return &varbSlice[0], nil
}

//GetVariables sends SNMP Get request to the target host and returns a slice of response SNMP Variables as []snmpvar.SnmpVar.
//This method will send Get request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetVariables() ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetVariableBindings()
	if err != nil {
		log.Trace("Returning nil variable slice. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}
	//Empty varb check should be done in GetVariableBinding method already
	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, nil
}

//GetList sends SNMP Get request to the target host and returns a slice of response VariableBindings as []string.
//This method will send Get request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetList() ([]string, error) {
	start := 0
	log.Trace("* GetList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetVariableBindings()
	if err != nil {
		log.Trace("Returning nil. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	var varbStrSlice []string
	for _, varb := range varbSlice {
		if varb.Variable() != nil { //Append only non-nil SnmpVar
			varbStrSlice = append(varbStrSlice, varb.String())
		}
	}

	log.Trace("Returning string slice: %v", varbStrSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbStrSlice, nil
}

//GetOIDList sends SNMP Get request to the target host and returns a slice of SNMP Variables in the response.
//This method will send Get request by using 'oidList' as VarBinds.
//
//oidList should be a slice of OIDs of type string/SnmpOID
//
//It returns nil (or) partial variables slice along with an error message in following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, Get operation will be perfomed on rest of the Valid OIDs
//and variables slice will be returned along with error message.
func (t *SnmpTarget) GetOIDList(oidList interface{}) ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++ //Invalid OID String
			}
		}
		//log.Println("Dropped count:",dropCount)
		if dropCount == len(argType) {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. No valid OIDs specified.")
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return nil, errors.New(t.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			t.errorCode = Dropped_Invalid_Oid
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Too few OIDs dropped in Get operation as they are invalid: " + t.errorString)
			retErr = errors.New(t.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)
	varbSlice, err := t.getResponseVariableBindings(oidSlice, consts.GetRequest)
	if err != nil {
		log.Trace("Returning nil. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning snmp variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, retErr
}

//GetVariableBindings sends SNMP Get request to the target host and returns a slice of response VariableBindings as []msg.SnmpVarBind.
//This method will send Get request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetVariableBindings() ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetVariableBindings() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}

		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)
	varbSlice, err := t.getResponseVariableBindings(t.oidList, consts.GetRequest)
	if err != nil {
		log.Trace("Returning nil variable binding. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable bindings slice: %v", varbSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbSlice, nil
}

//#################### SNMP GET-NEXT ######################//

//GetNext sends SNMP Get-Next request to the target host and returns the first SNMP Variable in the response as a string.
//This method will send Get-Next request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNext() (string, error) {
	start := 0
	log.Trace("* GetNext() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varb, err := t.GetNextVariableBinding()
	if err != nil {
		log.Trace("Returning nil. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", err
	}

	varStr := varb.Variable().String()
	log.Trace("Returning variable string: " + varStr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varb.Variable().String(), nil
}

//GetNextOID sends SNMP Get-Next request to the target host by using oid as ObjectID and returns the response SNMP Variable as a string.
//oid should be a OID of type SnmpOID/string/[]int.
//
//Sets the relevant error-code and returns error, if 'oid' is of invalid type (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextOID(oid interface{}) (string, error) {
	start := 0
	log.Trace("* GetNextOID() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	oidSlice := make([]snmpvar.SnmpOID, 2)
	switch snmpOid := oid.(type) {
	case string:
		if snmpOID := snmpvar.NewSnmpOID(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return "<nil>", errors.New(t.errorString)
		}
	case snmpvar.SnmpOID:
		oidSlice[0] = snmpOid
	case *snmpvar.SnmpOID:
		oidSlice[0] = *snmpOid
	case []uint32:
		if snmpOID := snmpvar.NewSnmpOIDByInts(snmpOid); snmpOID != nil {
			oidSlice[0] = *snmpOID
		} else {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return "<nil>", errors.New(t.errorString)
		}
	default:
		//log.Println("Invalid OID Type")
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice[:1])
	varbSlice, err := t.getResponseVariableBindings(oidSlice[:1], consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning nil. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	varStr := varbSlice[0].Variable().String()

	log.Trace("Returning variable string: " + varStr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varStr, nil
}

//GetNextVariable sends SNMP Get-Next request to the target host and returns the first SNMP Variable in the response as snmpvar.SnmpVar.
//This method will send Get-Next request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextVariable() (snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetNextVariable() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varb, err := t.GetNextVariableBinding()
	if err != nil {
		log.Trace("Returning nil variable. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	variable := varb.Variable()
	log.Trace("Returning variable: " + variable.String())
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return variable, nil
}

//GetNextVariableBinding sends SNMP Get-Next request to the target host and returns the first VariableBinding in the response as *msg.SnmpVarBind.
//This method will send Get-Next request by setting the first OID from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextVariableBinding() (*msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetNextVariableBinding() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList[:1])
	varbSlice, err := t.getResponseVariableBindings(t.oidList[:1], consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning nil variable binding. Error in Get operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable binding: " + varbSlice[0].String())
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return &varbSlice[0], nil
}

//GetNextList sends SNMP Get-Next request to the target host and returns a slice of response VariableBindings as []string.
//This method will send Get request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextList() ([]string, error) {
	start := 0
	log.Trace("* GetNextList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetNextVariableBindings()
	if err != nil {
		log.Trace("Returning nil. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	var varbStrSlice []string
	for _, varb := range varbSlice {
		if varb.Variable() != nil { //Append only non-nil SnmpVar
			varbStrSlice = append(varbStrSlice, varb.String())
		}
	}

	log.Trace("Returning variable bindings string slice: %v", varbStrSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbStrSlice, nil
}

//GetNextOIDList sends SNMP Get request to the target host and returns a slice of response SNMP Variables as []snmpvar.SnmpVar.
//This method will send Get-Next request by using 'oidList' as VarBinds.
//
//oidList should be a slice of OIDs of type string/SnmpOID
//
//It returns nil (or) partial variables slice along with an error message in following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, Get-Next operation will be perfomed on remaining Valid OIDs
//and variables slice will be returned along with error message.
func (t *SnmpTarget) GetNextOIDList(oidList interface{}) ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++ //Invalid OID String
			}
		}
		//log.Println("Dropped OIDs count:",dropCount)
		if dropCount == len(argType) {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. No valid OIDs specified.")
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return nil, errors.New(t.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			t.errorCode = Dropped_Invalid_Oid
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Too few OIDs dropped in Get-Next operation as they are invalid: " + t.errorString)
			retErr = errors.New(t.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)
	varbSlice, err := t.getResponseVariableBindings(oidSlice, consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning nil. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning snmp variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, retErr
}

//GetNextVariables sends SNMP Get-Next request to the target host and returns a slice of response SNMP Variables as []snmpvar.SnmpVar.
//This method will send Get-Next request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextVariables() ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetNextVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetNextVariableBindings()
	if err != nil {
		log.Trace("Returning nil variable slice. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, nil
}

//GetNextVariableBindings sends SNMP Get-Next request to the target host and returns a slice of response VariableBindings as []msg.SnmpVarBind.
//This method will send Get-Next request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetNextVariableBindings() ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetNextVariableBindings() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)
	varbSlice, err := t.getResponseVariableBindings(t.oidList, consts.GetNextRequest)
	if err != nil {
		log.Trace("Returning nil variable binding. Error in Get-Next operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable bindings slice: %v", varbSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbSlice, nil
}

//#################### SNMP GET-ALL ######################//

//GetAllList gets all instances of variables in the OID list. Used for columnar OIDs, mainly for SNMP Table Column.
//If the OID list contains more than one table column OIDs then each table column OID will be queried consecutively for each row.
//The OID list must contain the OIDs of the Table column without instances.
//SetMaxRowsCount method sets the maximum number of rows that can be requested for all the table column specified in the OID list.
//The maximum number of rows that can be processed by default is 1000.
//
//Returns variables as a two dimensional slices of strings. Returns nil if no OID is specified earlier or in case of errors.
//
//Note : This method will alter the target's OIDList as the oids in the subsequent responses are set to the target's oidList.
func (t *SnmpTarget) GetAllList() ([][]string, error) {

	start := 0
	log.Trace("* GetAllList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	allvarbs, err := t.GetAllVariableBindings()
	if err != nil {
		log.Trace("Returning nil. Error in Get All operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	//Empty varb check should be done in GetAllVariableBindings method already
	strArr := [][]string{}
	for _, varbs := range allvarbs {
		strVarbs := []string{}
		for _, varb := range varbs {
			strVarbs = append(strVarbs, varb.String())
		}
		strArr = append(strArr, strVarbs)
	}

	log.Trace("Returning string slice: %v", strArr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return strArr, nil
}

//GetAllVariables gets all instances of variables in the OID list. Used for columnar OIDs, mainly for SNMP Table Column.
//If the OID list contains more than one table column OIDs then each table column OID will be queried consecutively for each row.
//The OID list must contain the OIDs of the Table column without instances.
//SetMaxRowsCount method sets the maximum number of rows that can be requested for all the table column specified in the OID list.
//The maximum number of rows that can be processed by default is 1000.
//
//Returns variables as a two dimensional slices of snmpvar.SnmpVar. Returns nil if no OID is specified earlier or in case of errors.
//
//Note : This method will alter the target's OIDList as the oids in the subsequent responses are set to the target's oidList.
func (t *SnmpTarget) GetAllVariables() ([][]snmpvar.SnmpVar, error) {

	start := 0
	log.Trace("* GetAllVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	allvarbs, err := t.GetAllVariableBindings()
	if err != nil {
		log.Trace("Returning nil variable slice. Error in Get All operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	//Empty varb check should be done in GetAllVariableBindings method already
	strArr := [][]snmpvar.SnmpVar{}
	for _, varbs := range allvarbs {
		strVarbs := []snmpvar.SnmpVar{}
		for _, varb := range varbs {
			strVarbs = append(strVarbs, varb.Variable())
		}
		strArr = append(strArr, strVarbs)
	}

	log.Trace("Returning variable slice: %v", strArr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return strArr, nil
}

//GetAllVariableBindings gets all instances of variables in the OID list. Used for columnar OIDs, mainly for SNMP Table Column.
//If the OID list contains more than one table column OIDs then each table column OID will be queried consecutively for each row.
//The OID list must contain the OIDs of the Table column without instances.
//SetMaxRowsCount method sets the maximum number of rows that can be requested for all the table column specified in the OID list.
//The maximum number of rows that can be processed by default is 1000.
//
//Returns variables as a two dimensional slices of msg.SnmpVarBind. Returns nil if no OID is specified earlier or in case of errors.
//
//Note : This method will alter the target's OIDList as the oids in the subsequent responses are set to the target's oidList.
func (t *SnmpTarget) GetAllVariableBindings() ([][]msg.SnmpVarBind, error) {

	start := 0
	log.Trace("* GetAllVariableBindings() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)

	maxtry := 0
	arr := [][]msg.SnmpVarBind{}          //Final 2d slice to be returned
	emptyVar := snmpvar.NewSnmpString("") //To fill the value part if the oids fall apart the root oids
	rootOids := t.oidList                 //Initial oidlist to be taken as root oids

	for maxtry < t.maxRows {
		maxtry++
		count := 0

		varbs, err := t.snmpGetNextVariableBindings()
		if err != nil || varbs == nil || t.errorCode != Err_No_Error || t.exceptionCode != 0 {
			log.Trace("Breaking the loop. Error in GetNext operation: %s", err.Error())
			break
		}

		for i, varb := range varbs {
			//Check the resultant oids fall within their respective root oids.
			if !strings.HasPrefix(varb.ObjectID().String(), rootOids[i].String()) {
				//Set empty string variable for each oid that falls out of its respective suboid.
				varbs[i].SetVariable(emptyVar)
			} else {
				//Increment count for each oid in the oidlist that fall in suboid for its respective root oid.
				count++
			}
		}

		//Loop should continue until all oids are out of sub-tree (i.e) count value remains 0.
		if count > 0 {
			arr = append(arr, varbs)
		} else {
			break
		}
	}

	if len(arr) == 0 {
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New("Empty variable-bindings returned")
	}

	log.Trace("Returning variable bindings slice: %v", arr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return arr, nil
}

//Wrapper for GetNextVariableBindings. But it alters the oidList. OIDs obtained in the response will be set again as the target's oidList.
func (t *SnmpTarget) snmpGetNextVariableBindings() ([]msg.SnmpVarBind, error) {

	varbs, err := t.GetNextVariableBindings()
	if err != nil {
		return varbs, err
	}

	newOidList := []snmpvar.SnmpOID{}
	if varbs != nil && len(varbs) != 0 {
		for _, varb := range varbs {
			newOidList = append(newOidList, varb.ObjectID())
		}

		t.SetOIDList(newOidList)
	}

	return varbs, err
}

//#################### SNMP SET ######################//

//Set sends SNMP Set request to the target host and returns first SNMP Variable in the response as a string.
//Set is performed on first OID from t's ObjectID List using varString and varType as SNMP Variable.
//
//varType should be of any valid datatype as per ASN.1 standard.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) Set(varString string, varType byte) (string, error) {
	start := 0
	log.Trace("* Set() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	snmpVar, err := snmpvar.CreateSnmpVar(varType, varString)
	if err != nil {
		t.errorCode = Error_Creating_Variable
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", errors.New(t.errorString)
	}

	responseSnmpVar, err := t.SetVariable(snmpVar)
	if err != nil {
		log.Trace("Returning nil. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return "<nil>", err
	}

	varStr := responseSnmpVar.String()
	log.Trace("Returning variable string: " + varStr)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varStr, nil
}

//SetVariable sends SNMP Set request to the target host and returns the first SNMP Variable in the response as snmpvar.SnmpVar.
//Set is performed on the first OID from t's ObjectID List using snmpVar.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) SetVariable(snmpVar snmpvar.SnmpVar) (snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* SetVariable() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList[:1])
	t.varList = []snmpvar.SnmpVar{snmpVar} //Set this on VarList so that variable bindings will be constructed with value part
	varbSlice, err := t.getResponseVariableBindings(t.oidList[:1], consts.SetRequest)
	if err != nil {
		log.Trace("Returning nil variable. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds - Maybe Set Failure
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned for Set operation: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable: " + varbSlice[0].Variable().String())
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbSlice[0].Variable(), nil
}

//SetVariables sends SNMP Set request to the target host and returns a slice of response SNMP Variables as []snmpvar.SnmpVar.
//Set is performed on all the OIDs set on t's ObjectID list with 'varSlice' as SNMP Variables.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) SetVariables(varSlice []snmpvar.SnmpVar) ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* SetVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)
	t.varList = varSlice
	varbSlice, err := t.getResponseVariableBindings(t.oidList, consts.SetRequest)
	if err != nil {
		log.Trace("Returning nil variable slice. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds - Maybe Set Failure
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned for Set operation: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, nil
}

//SetVariableList sends SNMP Set request to the target host and returns a slice of response VariableBindings as []msg.SnmpVarBind.
//Set is performed on all the OIDs set on t's ObjectID list with 'varSlice' as SNMP Variables.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) SetVariableList(varSlice []snmpvar.SnmpVar) ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* SetVariableList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil variable binding slice. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}
	t.varList = varSlice

	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)
	varbSlice, err := t.getResponseVariableBindings(t.oidList, consts.SetRequest)

	if err != nil {
		log.Trace("Returning nil variable binding. Error in Set operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if varbSlice == nil || len(varbSlice) == 0 { //For response without varbinds
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned for Set operation: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable bindings slice: %v", varbSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbSlice, nil
}

//########## Trap Message ############//

//OIDs required for Trap message construction
var (
	sysUpTimeOID  string   = ".1.3.6.1.2.1.1.3.0"
	trapOID       string   = ".1.3.6.1.6.3.1.1.4.1.0"
	enterpriseOID string   = ".1.3.6.1.6.3.1.1.4.3.0"
	snmpTraps     string   = ".1.3.6.1.6.3.1.1.5.0"
	genericTraps  []string = []string{
		".1.3.6.1.6.3.1.1.5.1", //ColdStart
		".1.3.6.1.6.3.1.1.5.2", //WarmStart
		".1.3.6.1.6.3.1.1.5.3", //LinkDown
		".1.3.6.1.6.3.1.1.5.4", //LinkUp
		".1.3.6.1.6.3.1.1.5.5", //AuthenticationFailure
		".1.3.6.1.6.3.1.1.5.6", //egpNeighborLoss
	}
)

//SendTrap sends v1 trap message to the target host with the params specified and variable bindings using OIDs from t's ObjectID List and the 'values' specified.
//Sets the relevant error-code and returns error if any.
//
//  Note:
//  * This method is used to send v1 trap message.
//  * In case Version2C/Version3 is set on this target t, v2 trap message will be constructed with appropriate changes and send to target host.
func (t *SnmpTarget) SendTrap(enterprise, agentAddress string, genericType, specificType int, sysUpTime uint32, values []snmpvar.SnmpVar) error {
	start := 0
	log.Trace("* SendTrap() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Construct the trap message
	trapMsg := msg.NewSnmpMessage()
	t.constructOutgoingMessage(&trapMsg) //Set the generic params on the SnmpMessage

	//Trap Message
	if trapMsg.Version() == Version1 { //Construct V1 Trap
		trapMsg.SetCommand(consts.TrapRequest)
		trapMsg.SetEnterprise(*snmpvar.NewSnmpOID(enterprise))
		trapMsg.SetAgentAddress(agentAddress)
		trapMsg.SetGenericType(genericType)
		trapMsg.SetSpecificType(specificType)
		trapMsg.SetUpTime(sysUpTime)
	} else if trapMsg.Version() == Version2C || trapMsg.Version() == Version3 {
		//Convert to V2Trap as version used in target is SNMPV2C/SNMPV3.
		//V2Trap is constructed as per RFC3584 'Coexistence between Version 1, Version 2, and Version 3' - Section 3.
		trapMsg.SetCommand(consts.Trap2Request)
		//SysUpTime VarBind - RFC 3584 - Section 3.1, Step 1
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(sysUpTimeOID),
				snmpvar.NewSnmpTimeTicks(sysUpTime),
			),
		)
		//TrapOID VarBind - RFC 3584 - Section 3.1, Step 2 & 3
		if genericType >= 0 && genericType <= 5 { //Let's use the Generic Trap OID
			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					*snmpvar.NewSnmpOID(trapOID),
					snmpvar.NewSnmpOID(genericTraps[genericType]),
				),
			)
		} else if genericType == 6 {
			//Construct the TrapOID based on 'enterprise string' provided as per Coexistence standard.
			enterpriseIntArray := snmpvar.NewSnmpOID(enterprise).Value()
			trapOIDIntArray := make([]uint32, len(enterpriseIntArray)+2)
			copy(trapOIDIntArray, enterpriseIntArray)
			trapOIDIntArray[len(enterpriseIntArray)] = 0 //Two additional Sub-Identifiers
			trapOIDIntArray[len(enterpriseIntArray)+1] = uint32(specificType)

			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					*snmpvar.NewSnmpOID(trapOID),
					snmpvar.NewSnmpOIDByInts(trapOIDIntArray),
				),
			)
		} else {
			t.errorCode = Invalid_Generic_Type
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning.. " + t.errorString)
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return errors.New(t.errorString)
		}
	}

	//Add the remaining varbinds - RFC 3584 - Section 3.1, Step 4
	if values != nil {
		for i, oid := range t.oidList {
			if values[i] == nil {
				break
			}
			trapMsg.AddVarBind(
				msg.NewSnmpVarBind(
					oid,
					values[i],
				),
			)
		}
	}

	//EnterpriseOID VarBind: For trap conversion from v1 to v2c/v3, enterprise OID is attached as last varbind.
	if trapMsg.Version() > Version1 {
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(enterpriseOID),
				snmpvar.NewSnmpOID(enterprise),
			),
		)
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", trapMsg.VarBinds())
	//We have constructed the trap message, so call the 'handler' method directly
	_, err := t.handler.SendSNMPRequest(t.session, trapMsg)
	if err == nil {
		log.Trace("Returning.. Trap msg sent successfully.")
	} else {
		log.Trace("Returning.. Error in sending trap: %s", err.Error())
	}
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return err
}

//SendV2Trap sends v2 trap message to the target host with the params specified and variable bindings using OIDs from t's ObjectID List and the 'values' specified.
//Sets the relevant error-code and returns error if any.
//
//  Note:
//  * This method is used to send v2 trap message.
//  * In case Version1 is set on this target t, v1 trap message will be constructed with appropriate changes and send to target host.
func (t *SnmpTarget) SendV2Trap(sysUpTime uint32, trapOid snmpvar.SnmpOID, values []snmpvar.SnmpVar) error {
	start := 0
	log.Trace("* SendV2Trap() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	//Construct v2 trap message
	trapMsg := msg.NewSnmpMessage()
	t.constructOutgoingMessage(&trapMsg) //Set the generic params on the SnmpMessage

	if t.Version() < Version2C {
		//Convert V2Trap to V1Trap as per - RFC3584 'Coexistence between Version 1, Version 2, and Version 3'
		trapMsg.SetCommand(consts.TrapRequest)

		//RFC 3584 - Section 3.2
		//Step 2
		trapMsg.SetAgentAddress(t.localHost) //Entity sending this trap

		//Step 3
		genericType := 6
		for i, trap := range genericTraps {
			if strings.EqualFold(trapOid.String(), trap) {
				genericType = i
				break
			}
		}
		trapMsg.SetGenericType(genericType)

		//Step 4
		if genericType >= 0 && genericType <= 5 {
			trapMsg.SetSpecificType(0)
		} else {
			trapOIDIntArray := trapOid.Value()
			trapOIDLen := len(trapOIDIntArray)
			trapMsg.SetSpecificType(int(trapOIDIntArray[trapOIDLen-1]))

			//Set EnterpriseOID - Step 1
			if trapOIDIntArray[trapOIDLen-2] == 0 { //Netx-to-last Sub-Identifier
				trapMsg.SetEnterprise(*snmpvar.NewSnmpOIDByInts(trapOIDIntArray[:(trapOIDLen - 3)])) //Remove last two sub-identifiers
			} else {
				trapMsg.SetEnterprise(*snmpvar.NewSnmpOIDByInts(trapOIDIntArray[:(trapOIDLen - 2)])) //Remove last sub-identifier
			}
		}

		//Step 5
		trapMsg.SetUpTime(sysUpTime)

		//Step 6 - Add the remaining varbinds
		if values != nil {
			for i, oid := range t.oidList {
				if values[i] == nil {
					break
				}
				if values[i].Type() == consts.Counter64 {
					errStr := "Failure in converting to V1 Trap. Invalid Variable Binding Type: Counter64"
					log.Trace("Returning.. Error in sending trap: " + errStr)
					if log.IsPerf() {
						end := currentTimeInMillis()
						log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
					}
					return errors.New(errStr)
				}
				trapMsg.AddVarBind(
					msg.NewSnmpVarBind(
						oid,
						values[i],
					),
				)
			}
		}
	} else {
		trapMsg.SetCommand(consts.Trap2Request)

		//Add the variable bindings for v2 traps
		//SysUptime VarBind
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(sysUpTimeOID),
				snmpvar.NewSnmpTimeTicks(sysUpTime),
			),
		)

		//TrapOID VarBind
		trapMsg.AddVarBind(
			msg.NewSnmpVarBind(
				*snmpvar.NewSnmpOID(trapOID),
				trapOid,
			),
		)

		//Add the remaining varbinds
		if values != nil {
			for i, oid := range t.oidList {
				if values[i] == nil {
					break
				}
				trapMsg.AddVarBind(
					msg.NewSnmpVarBind(
						oid,
						values[i],
					),
				)
			}
		}
	}

	//For V1Trap, Set the EnterpriseOID - RFC 3584 - Section 3.2 Step 1
	if (trapMsg.Version() == Version1) &&
		(trapMsg.GenericType() >= 0 && trapMsg.GenericType() <= 5) {
		found := false
		for _, varb := range trapMsg.VarBinds() {
			if strings.EqualFold(varb.ObjectID().String(), enterpriseOID) {
				trapMsg.SetEnterprise(varb.Variable().(snmpvar.SnmpOID))
				found = true
				break
			}
		}
		if !found {
			trapMsg.SetEnterprise(*snmpvar.NewSnmpOID(snmpTraps))
		}
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", trapMsg.VarBinds())
	//We have constructed the trap message, so call the 'request_handler' method directly
	_, err := t.handler.SendSNMPRequest(t.session, trapMsg)
	if err == nil {
		log.Trace("Returning.. Trap msg sent successfully.")
	} else {
		log.Trace("Returning.. Error in sending trap: %s", err.Error())
	}
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return err
}

//############## Inform Request #############//

//SendInform sends inform request to target host with specified params and variable bindings using OIDs from t's ObjectID List and 'variables' specified.
//
//Returns a slice of response VariableBindings as []msg.SnmpVarbind (or)
//
//It sets the relevant error code and returns nil along with error message for the below cases,
//
//  * Version is not Version2C/Version3
//  * No OIDs set on target
//  * Error in communication with the target host
func (t *SnmpTarget) SendInform(upTime uint32, trapOid string, variables []snmpvar.SnmpVar) ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* SendInform() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if t.Version() < Version2C {
		t.errorCode = Invalid_Version
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}
	informMsg := msg.NewSnmpMessage()
	informMsg.SetCommand(consts.InformRequest)
	t.constructOutgoingMessage(&informMsg)

	//SysUptime OID
	informMsg.AddVarBind(
		msg.NewSnmpVarBind(
			*snmpvar.NewSnmpOID(sysUpTimeOID),
			snmpvar.NewSnmpTimeTicks(upTime),
		),
	)
	if snmpvar.NewSnmpOID(trapOid) == nil {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil variable binding slice. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}
	//Trap OID
	informMsg.AddVarBind(
		msg.NewSnmpVarBind(
			*snmpvar.NewSnmpOID(trapOID),
			snmpvar.NewSnmpOID(trapOid), //Check the OID using MibOps later
		),
	)
	//Add the remaining list of VarBinds
	if variables != nil {
		for i, oid := range t.oidList {
			if variables[i] == nil {
				break
			}
			informMsg.AddVarBind(
				msg.NewSnmpVarBind(
					oid,
					variables[i],
				),
			)
		}
	}

	log.Debug("Variable Bindings in outgoing SnmpMessage: %v", informMsg.VarBinds())
	//This perform SyncSend and returns the response SnmpMessage
	resp, err := t.handler.GetSnmpResponse(t.session, &informMsg, false, false, 0)
	if err != nil {
		log.Trace("Returning nil. Error in Inform operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	varbs := resp.VarBinds()

	if len(varbs) == 0 {
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Error in Inform operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable bindings slice: %v", varbs)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbs, nil
}

//############# SNMP Get-Bulk ###############//

//GetBulkVariableBindings sends SNMP Get-Bulk request to the target host and returns a slice of response VariableBindings as []msg.SnmpVarBind.
//
//Get-Bulk request is made on the OIDs from t's ObjectID List with non-repeaters and max-repetitions set on t.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetBulkVariableBindings() ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetBulkVariableBindings() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	if t.Version() < Version2C {
		t.errorCode = Invalid_Version
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if len(t.oidList) == 0 {
		t.errorCode = Oid_Not_Specified
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if int(t.nonRepeaters) > len(t.oidList) {
		t.nonRepeaters = int32(len(t.oidList)) // To make sure that non-repeaters value is less than the number of OIDs
	}

	//  To Construct two-dimensional Slice from the VarBinds received in response:
	//	varbSlice, err := t.getResponseVariableBindings(t.oidList, snmp.GetBulkRequest)
	//
	//	rowCount := (len(varbSlice) - t.nonRepeaters)/(len(t.oidList) - t.nonRepeaters) //MaxRep VarBinds Count/MaxRep OIDs Count
	//	varbArray := make([][]msg.SnmpVarBind, rowCount)
	//	index := 0
	//
	//	//Fill Non-Rep VarBinds
	//	for i := 0; i < t.nonRepeaters; i++ {
	//		varbArray[i] = make([]msg.SnmpVarBind, rowCount)
	//		varbArray[i][0] = varbSlice[i]
	//		index++
	//	}
	//	for i := t.nonRepeaters; i<len(t.oidList);i++ {
	//		varbArray[i] = make([]msg.SnmpVarBind, rowCount)
	//	}
	//
	//	//Fill Max-Rep VarBinds
	//	for i := 0; i < rowCount; i++ {
	//		//varbArray[j] = make([]msg.SnmpVarBind, rowCount)
	//		for j := t.nonRepeaters; j < len(t.oidList); j++ {
	//			if index >= len(varbSlice) { break } //Received Partial VarBinds
	//			varbArray[j][i] = varbSlice[index]
	//			index++
	//		}
	//	}
	log.Debug("OID(s) in outgoing SnmpMessage: %v", t.oidList)
	varbs, err := t.getResponseVariableBindings(t.oidList, consts.GetBulkRequest)

	if err != nil {
		log.Trace("Returning nil variable bindings. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbs == nil || len(varbs) == 0 {
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned for Get-Bulk: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}
	log.Trace("Returning variable bindings slice: %v", varbs)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbs, nil
}

//GetBulkOIDList sends SNMP Get-Bulk request to the target host with specified nonReps, maxReps and Variable Bindings constructed using 'oidList'.
//
//oidList should be a slice of OIDs of type string/SnmpOID.
//
//It returns nil (or) partial variables slice along with an error message in the following cases:
//
//  * oidList is of invalid type.
//  * Invalid OID(s) passed in oidList.
//  * Any error in communication with the target host.
//
//Note: If there are partially Invalid OIDs in oidList, GetBulk operation will be perfomed on remaining Valid OIDs
//and variables slice will be returned along with error message.
func (t *SnmpTarget) GetBulkOIDList(oidList interface{}, nonReps, maxReps int32) ([]msg.SnmpVarBind, error) {
	start := 0
	log.Trace("* GetBulkOIDList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	var oidSlice []snmpvar.SnmpOID
	var retErr error = nil
	switch argType := oidList.(type) {
	case []string:
		dropCount := 0
		for _, oidStr := range argType {
			if oid := snmpvar.NewSnmpOID(oidStr); oid != nil {
				oidSlice = append(oidSlice, *oid)
			} else {
				dropCount++ //Invalid OID String
			}
		}
		//log.Println("Dropped OIDs count:",dropCount)
		if dropCount == len(argType) {
			t.errorCode = Oid_Not_Specified
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Returning nil. No valid OIDs specified.")
			if log.IsPerf() {
				end := currentTimeInMillis()
				log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
			}
			return nil, errors.New(t.errorString)
		} else if dropCount > 0 {
			//Set the error code if any OIDs got dropped.
			//And continue with the operation.
			t.errorCode = Dropped_Invalid_Oid
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			log.Trace("Too few OIDs dropped in Get-Bulk operation as they are invalid: " + t.errorString)
			retErr = errors.New(t.errorString)
		}
	case []snmpvar.SnmpOID:
		oidSlice = argType
	default:
		t.errorCode = Illegal_Argument
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if t.Version() < Version2C {
		t.errorCode = Invalid_Version
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	if nonReps > int32(len(oidSlice)) {
		nonReps = int32(len(oidSlice)) // To make sure that non-repeaters value is less than number of OIDs
	}

	t.SetNonRepeaters(nonReps)
	t.SetMaxRepetitions(maxReps)

	log.Debug("OID(s) in outgoing SnmpMessage: %v", oidSlice)
	varbs, err := t.getResponseVariableBindings(oidSlice, consts.GetBulkRequest)

	if err != nil {
		log.Trace("Returning nil variable slice. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	if varbs == nil || len(varbs) == 0 {
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		log.Trace("Returning nil. Empty varbinds returned for Get-Bulk: " + t.errorString)
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, errors.New(t.errorString)
	}

	log.Trace("Returning variable bindings slice: %v", varbs)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbs, retErr
}

//GetBulkVariables sends SNMP Get-Bulk request to the target host and returns a slice of response SNMP Variables as []snmpvar.SnmpVar.
//This method will send Get-Bulk request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetBulkVariables() ([]snmpvar.SnmpVar, error) {
	start := 0
	log.Trace("* GetBulkVariables() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetBulkVariableBindings()
	if err != nil {
		log.Trace("Returning nil variable slice. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	snmpVarSlice := make([]snmpvar.SnmpVar, len(varbSlice))
	for i, varb := range varbSlice {
		snmpVarSlice[i] = varb.Variable()
	}

	log.Trace("Returning variable slice: %v", snmpVarSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return snmpVarSlice, nil
}

//GetBulkList sends SNMP Get-Bulk request to the target host and returns a slice of response VariableBindings as []string.
//This method will send Get-Bulk request by setting all the OIDs from t's ObjectID List.
//
//Sets the relevant error-code and returns error, if no OIDs set on target (or) any error in communication with the target host.
func (t *SnmpTarget) GetBulkList() ([]string, error) {
	start := 0
	log.Trace("* GetBulkList() *")
	if log.IsPerf() { //If performance is enabled, calc the perf
		start = currentTimeInMillis()
	}

	varbSlice, err := t.GetBulkVariableBindings()
	if err != nil {
		log.Trace("Returning nil slice. Error in Get-Bulk operation: %s", err.Error())
		if log.IsPerf() {
			end := currentTimeInMillis()
			log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
		}
		return nil, err
	}

	var varbStrSlice []string
	for _, varb := range varbSlice {
		if varb.Variable() != nil { //Append only non-nil SnmpVar
			varbStrSlice = append(varbStrSlice, varb.String())
		}
	}

	log.Trace("Returning variable bindings slice: %v", varbStrSlice)
	if log.IsPerf() {
		end := currentTimeInMillis()
		log.Logf(log.PERF, "Total time taken: "+strconv.Itoa(end-start)+"ms")
	}

	return varbStrSlice, nil
}

//ReleaseResources helps in releasing the resources used by t immediately.
//
//Closes the SnmpSession and SnmpAPI if number of targets sharing it becomes zero.
//Otherwise decrement the targets count for that SnmpSession and SnmpAPI.
func (t *SnmpTarget) ReleaseResources() {
	if t.session == nil {
		log.Warn("Failure in releasing resources. No SnmpSession is created on this SnmpTarget t.")
		return
	}
	if !t.session.IsSessionOpen() {
		log.Info("Session is already closed. Returning..")
		return
	}
	t.mut.Lock()
	defer t.mut.Unlock()
	//Close SnmpSession
	if val, ok := t.snmpStore["SESSION"][t.sessionName]; ok {
		if val.concurrentUsers == 1 {
			delete(t.snmpStore["SESSION"], t.sessionName)
			if t.session != nil {
				t.session.Close()
			}
		} else {
			t.snmpStore["SESSION"][t.sessionName].concurrentUsers--
		}
		t.session = nil
	}
	//Close SnmpAPI
	if val, ok := t.snmpStore["API"]["API"]; ok {
		if val.concurrentUsers == 1 {
			delete(t.snmpStore["API"], "API")
			if t.api != nil {
				t.api.Close()
			}
		} else {
			t.snmpStore["API"]["API"].concurrentUsers--
		}
		t.api = nil
	}

	log.Debug("Successfully released the underlying resources of SnmpTarget.")
}

//########### Unexported Methods - Utilities #########//

//Perform SyncSend after constructing SnmpMessage using oids/command and return slice of SnmpVarBind in response
func (t *SnmpTarget) getResponseVariableBindings(oids []snmpvar.SnmpOID, command consts.Command) ([]msg.SnmpVarBind, error) {

	t.errorCode = No_Err_Registered
	t.errorString = t.errorMsgs.ErrorString(t.errorCode)
	t.errorIndex = 0
	t.exceptionCode = -1

	//Call GetResponse
	reqMsg := msg.NewSnmpMessage()
	reqMsg.SetCommand(command)
	t.constructOutgoingMessage(&reqMsg)

	if reqMsg.Version() == Version3 &&
		strings.EqualFold(reqMsg.UserName(), "") {
		//We should not allow this request, as it would send a request with empty EngineId,
	}

	for _, v := range oids {
		reqMsg.AddNull(v)
	}
	//Add the value portion for Variable Bindings for Set/Trap operations
	if command == consts.SetRequest ||
		command == consts.TrapRequest ||
		command == consts.Trap2Request {
		for i, _ := range reqMsg.VarBinds() { //Add the SnmpVar to the VariableBinding
			if t.varList[i] == nil {
				break
			}
			reqMsg.VarBinds()[i].SetVariable(t.varList[i])
		}
	}

	resp, err := t.handler.GetSnmpResponse(t.session, &reqMsg, t.attemptPartial, t.attemptComplete, t.varBindsPerReq)
	if err != nil {
		if strings.Contains(err.Error(), "Time out occurred") {
			t.errorCode = Request_Timed_Out
			t.errorString = t.errorMsgs.ErrorString(t.errorCode) + " to " + t.targetHost
			t.exceptionCode = 0
		}
		return nil, err
	}

	responseVarBinds := resp.VarBinds()

	if !(resp.ErrorStatus() == 1 && strings.Contains(resp.ErrorString(), "too large")) && (responseVarBinds == nil || len(responseVarBinds) <= 0) {
		t.errorCode = Empty_Varbind
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		t.exceptionCode = 0
		return nil, errors.New(t.errorString)
	}

	if resp.Version() == Version1 {
		if resp.ErrorStatus() != 0 {
			t.errorCode = int(resp.ErrorStatus())
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			t.errorIndex = int(resp.ErrorIndex())
			t.exceptionCode = 0

			return nil, errors.New("Error Indication in response:\n" + t.errorString + "\n" + "Errindex : " + strconv.Itoa(t.errorIndex))
		}
	} else if resp.Version() == Version2C {
		if resp.ErrorStatus() != 0 {
			t.errorCode = int(resp.ErrorStatus())
			t.errorString = t.errorMsgs.ErrorString(t.errorCode)
			t.errorIndex = int(resp.ErrorIndex())
			t.exceptionCode = 0

			return nil, errors.New("Error Indication in response:\n" + t.errorString + "\n" + "Errindex : " + strconv.Itoa(t.errorIndex))
		} else {
			t.exceptionCodes = make(map[int]int)

			for key, varb := range responseVarBinds {
				code := varb.ExceptionIndex()
				if code != 0 {
					t.errorCode = 0
					t.errorIndex = key
					t.exceptionCode = code
					t.errorString = t.errorMsgs.ErrorString(code)
					t.exceptionCodes[key] = code

					if key+1 == len(responseVarBinds) {
						return responseVarBinds, errors.New("Error Indication in response:\n" + t.errorString + "\n" + varb.TagString())
					}
				}
			}
		}
	} else if resp.Version() == Version3 {
		if resp.Command() == consts.ReportMessage {
			oid := responseVarBinds[0].ObjectID()
			val := v3ErrMap[oid.String()]

			if !strings.EqualFold(val, "") {
				t.errorString = "SNMPV3 Error in Response. " + val + responseVarBinds[0].Variable().String()
				t.setV3ErrorCode(val)
			}

			return nil, errors.New(t.errorString)
		} else {
			if resp.ErrorStatus() != 0 {
				t.errorCode = int(resp.ErrorStatus())
				t.errorString = t.errorMsgs.ErrorString(t.errorCode)
				t.errorIndex = int(resp.ErrorIndex())
				t.exceptionCode = 0

				return nil, errors.New("Error Indication in response:\n" + t.errorString + "\n" + "Errindex : " + strconv.Itoa(t.errorIndex))
			} else {
				t.exceptionCodes = make(map[int]int)

				for key, varb := range responseVarBinds {
					code := varb.ExceptionIndex()
					if code != 0 {
						t.errorCode = 0
						t.errorIndex = key
						t.exceptionCode = code
						t.errorString = t.errorMsgs.ErrorString(code)
						t.exceptionCodes[key] = code

						if key+1 == len(responseVarBinds) {
							return responseVarBinds, errors.New("Error Indication in response:\n" + t.errorString + "\n" + varb.TagString())
						}
					}
				}
			}
		}
	} else {
		t.errorCode = Invalid_Version
		t.errorString = t.errorMsgs.ErrorString(t.errorCode)
		t.exceptionCode = 0

		return nil, errors.New(t.errorString)
	}

	t.errorCode = Err_No_Error
	t.errorString = t.errorMsgs.ErrorString(t.errorCode)
	t.errorIndex = int(resp.ErrorIndex())
	t.exceptionCode = 0

	return responseVarBinds, nil
}

//Set the basic and generic params on the outgoing SnmpMessage
func (t SnmpTarget) constructOutgoingMessage(msg *msg.SnmpMessage) {
	msg.SetVersion(t.version)
	msg.SetCommunity(t.community)

	if (t.protocolOptions != nil) &&
		(t.protocolOptions.RemotePort() >= 0) &&
		(t.protocolOptions.RemoteHost() != "") {
		msg.SetProtocolOptions(t.protocolOptions)
	} else {
		//Set the default UDP protocoloptions
		udp := snmp.NewUDPProtocolOptions()
		udp.SetRemoteHost(t.targetHost)
		udp.SetRemotePort(t.targetPort)
		msg.SetProtocolOptions(udp)
	}
	//Add Timeout, retries
	if t.timeout > 0 {
		msg.SetTimeout(t.timeout)
	}
	if t.retries > 0 {
		msg.SetRetries(t.retries)
	}

	//For SnmpV3
	if t.version == Version3 {
		msg.SetUserName(t.principal)
		msg.SetContextName(t.contextName)
		msg.SetContextEngineID(t.contextEngineID)
		msg.SetMsgMaxSize(t.msgMaxSize)
		msg.SetSecurityLevel(t.getSecurityLevel())
		msg.SetMsgSecurityModel(t.secModelID)
	}

	//For Get-Bulk request
	if msg.Command() == consts.GetBulkRequest {
		msg.SetNonRepeaters(t.nonRepeaters)
		msg.SetMaxRepetitions(t.maxRepetitions)
	}
}

func (t SnmpTarget) getSessionName() string {
	var sessionName string
	var port int
	//Construct session name with localport if user wants to bind to particular localport
	if t.protocolOptions != nil {
		port = t.protocolOptions.LocalPort()
	} else {
		port = t.localPort
	}
	if t.sessionName != "" && port != 0 {
		sessionName = "SESSION:" + t.sessionName + ":" + strconv.Itoa(port)
	} else if t.sessionName != "" {
		sessionName = "SESSION:" + t.sessionName
	} else if port != 0 {
		sessionName = "SESSION:" + strconv.Itoa(port)
	} else {
		sessionName = "SESSION"
	}

	return sessionName
}

func getMSGCommand(cmd consts.Command) string {

	var cmdStr string = ""

	switch cmd {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}

//setV3ErrorCode sets the error code based on the report-pdu
func (t *SnmpTarget) setV3ErrorCode(errVal string) {
	if strings.HasPrefix(errVal, "usmStatsUnsupportedSecurityLevel") {
		t.errorCode = UnSupported_SecurityLevel_Error
	} else if strings.HasPrefix(errVal, "usmStatsNotInTimeWindows") {
		t.errorCode = Not_In_Time_Windows_Error
	} else if strings.HasPrefix(errVal, "usmStatsUnknownUserNames") {
		t.errorCode = Unknown_Usernames_Error
	} else if strings.HasPrefix(errVal, "usmStatsUnknownEngineIDs") {
		t.errorCode = Unknown_EngineID_Error
	} else if strings.HasPrefix(errVal, "usmStatsWrongDigests") {
		t.errorCode = Wrong_Digest_Error
	} else if strings.HasPrefix(errVal, "usmStatsDecryptionErrors") {
		t.errorCode = Decrypt_Error
	}

	t.exceptionCode = 0
}

//setCreateV3ErrorCode sets the errorCode in target for CreateV3Tables method
func (t *SnmpTarget) setCreateV3ErrorCode(errVal int) {
	switch errVal {
	case consts.UnSupportedSecurityLevelError:
		t.errorCode = UnSupported_SecurityLevel_Error
	case consts.NotInTimeWindowsError:
		t.errorCode = Not_In_Time_Windows_Error
	case consts.UnknownUserNameError:
		t.errorCode = Unknown_Usernames_Error
	case consts.UnknownEngineIDError:
		t.errorCode = Unknown_EngineID_Error
	case consts.WrongDigestsError:
		t.errorCode = Wrong_Digest_Error
	case consts.DecryptionError:
		t.errorCode = Decrypt_Error
	default:
		t.errorCode = Time_Sync_Failed
	}
}

func (t *SnmpTarget) getSecurityLevel() consts.SecurityLevel {
	var securityLevel byte = 0

	if t.privProtocol != NO_PRIV && !strings.EqualFold(t.privPassword, "") {
		if t.authProtocol != NO_AUTH && !strings.EqualFold(t.authPassword, "") {
			securityLevel = 3
		} else {
			securityLevel = 0
		}
	} else {
		if t.authProtocol != NO_AUTH && !strings.EqualFold(t.authPassword, "") {
			securityLevel = 1
		} else {
			securityLevel = 0
		}
	}

	return consts.SecurityLevel(securityLevel)
}

//Get the Current System Time (in MilliSeconds) as an int
func currentTimeInMillis() int {
	return int(time.Now().UnixNano() / 1000000)
}
