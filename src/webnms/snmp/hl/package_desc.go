/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//package hl provides all the High-Level APIs such as SnmpTarget, SnmpRequestServer,
//TrapReceiver to perform all synchronous and Asynchronous SNMP operations at ease.
//These APIs are built on top of the low-level APIs, hiding the details of SNMP implementations from the users. It provides users with
//out of box support for performing all the SNMP operations.
//
//For example, SnmpTarget type provides us various methods to send Single/Multi VarBind
//Synchronous requests to the server.
//
//  target := hl.NewSnmpTarget()
//  target.SetTargetHostPort("localhost:161")
//  target.SetOIDList([]string{"1.4.0", "1.5.0"}) //Slice of OIDs
//  strVar, _ := target.Get() //Perform single varbind Get request
//  strVar2, _ := target.GetOID("1.1.0")
//  varbSlice, _ := target.GetVariableBindings() //Perform multi varbind Get request
//  for _, varb := range varbSlice {
//      fmt.Println(varb.String())
//  }
//
//SnmpRequestServer helps us to perform all the SNMP operations in Asynchronous manner.
//
//Refer other types such as 'SnmpRequestServer', 'SnmpTrapReceiver', 'Listener', 'TrapListener' for performing various SNMP operations.
package hl
