/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

// This example demonstrates how to perform Get request asynchronously using SnmpSession's Send method.
package snmp

import (
	"fmt"
	"os"
	
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

// This example sends get request with multi varbinds (1.4.0/1.5.0)
// to remote server 161 and returns immediately.
// Response message received and varbinds are printed in 'Callback()' method asynchronously.
func ExampleSnmpSession_Send() {
	//Create new SnmpAPI and SnmpSession instance
	api = snmp.NewSnmpAPI()
	ses = snmp.NewSnmpSession(api)
	
	//Create UDP ProtocolOptions and set it on the SnmpSession
	udp := snmp.NewUDPProtocolOptions()
	udp.SetRemoteHost("localhost")
	udp.SetRemotePort(161)
	ses.SetProtocolOptions(udp)
	
	//Add the SnmpClient to SnmpSession for async callback
	ses.AddSnmpClient(new(client))
	
	//Open a new SnmpSession
	if err = ses.Open(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer api.Close() //Close the SnmpAPI in any case
	
	//Construct a new SnmpMessage
	mesg := msg.NewSnmpMessage()
	mesg.SetVersion(consts.Version1)
	mesg.SetCommunity("public")
	mesg.SetCommand(consts.GetRequest)

	//Add the Varbinds
	oid := snmpvar.NewSnmpOID("1.4.0")
	if oid != nil {
		mesg.AddNull(*oid)
	}
	oid = snmpvar.NewSnmpOID("1.5.0")
	if oid != nil {
		mesg.AddNull(*oid)
	}
	
	//Make a Asynchronous request using the Session opened
	if _, err = ses.Send(mesg); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	ses.Wait() //Wait for all the Asynchronous requests to complete
}

//Implement SnmpClient interface for async callback
type client int

func (c client) Callback(resp *msg.SnmpMessage, reqID int32) {
	if resp != nil {
		po := resp.ProtocolOptions() //Get the ProtocolOptions from the response SnmpMessage
		fmt.Println("\nReceived response packet from", po.SessionID())
		fmt.Println(resp) //This will print the complete packet.
	} else {
		fmt.Println("Request Timed Out: Nil PDU received!")
	}
}

func (c client) Authenticate(res *msg.SnmpMessage, community string) bool {
	if res.Community() == community {
		return true
	}
	return true
}

func (c client) DebugStr(dbgStr string) {
	fmt.Println(dbgStr)
}