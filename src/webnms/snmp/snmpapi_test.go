/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"testing"

	//_ "github.com/lib/pq" //For testing DB
	"webnms/snmp/consts"
	"webnms/snmp/engine/security/usm"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

var (
	remoteHost string = "localhost"
	remotePort int    = 8001
	localHost  string = "localhost"
	localPort  int    = 7777
)

//Testing for all the methods in SnmpAPI type

//API Initailization Test
func TestApiInit(t *testing.T) {
	api := NewSnmpAPI()

	eng := api.SnmpEngine()
	if eng == nil ||
		eng.MsgProcessingSubSystem() == nil ||
		eng.SecuritySubSystem() == nil ||
		eng.SnmpLCD() == nil {
		t.Errorf("Failure in Engine initialization")
	}

	//Should be nil
	if eng.DBOperations() != nil {
		t.Error("Unexpected DB initialization..")
	}
	if api.IsDatabaseFlag() != false || api.Debug() != false {
		t.Error("Init error")
	}
	err := api.CloseDB()
	if err == nil {
		t.Error("No error returned when closing uninitiaized DB.")
	}

	if sess := api.CheckTimeouts(); len(sess) > 0 {
		t.Errorf("Timeout error: %v", sess)
	}
}

/*API close Test*/
func TestApiClose(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	err := ses.Open()
	if err != nil {
		t.Error(err.Error())
	}

	api.Close()
	if api.apiClosed != true {
		t.Error("Failure in closing SnmpAPI.")
	}
	//Check whether SnmpSessions are closed properly
	for _, session := range api.SnmpSessions() {
		if session.IsSessionOpen() {
			t.Error("Failure in API close method. SnmpSession's receiver routine is not closed.")
		}
	}

}

//Database details
var dbName string = "simulator"
var user string = "postgres"
var host string = "127.0.0.1"
var port string = "5432" //Default

/*Database Test*/
func TestDB(t *testing.T) {
	api := NewSnmpAPI()
	dataSrc := "dbname=" + dbName + " user=" + user + " host=" + host + " port=" + port + " sslmode=disable"
	//DB init test
	err := api.InitDB("postgres", dataSrc, consts.Postgres)
	if err != nil {
		t.Fatal(err)
	}
	//DB close test
	if err == nil {
		err = api.CloseDB()
		if err != nil {
			t.Errorf("SnmpAPI CloseDB: Error closing DB: %s", err.Error())
		}
	}
	//Invalid Dialect test
	err = api.InitDB("postgres", dataSrc, 70)
	if err == nil {
		t.Error("SnmpAPI InitDB: Not throwing invalid Dialect error")
	}

	//Invalid Driver test
	err = api.InitDB("invalid driver", dataSrc, 1)
	if err == nil {
		t.Error("SnmpAPI InitDB: Not throwing invalid driver error")
	}

	if api.IsDatabaseFlag() == true {
		t.Errorf("No DB connection. But flag is true.")
	}
}

/*SnmpGroup Test*/
func TestSnmpGroup(t *testing.T) {
	api := NewSnmpAPI()
	grp := api.SnmpGroup("localhost", 100)
	if grp != nil {
		t.Errorf("SnmpAPI SnmpGroup(): Invalid SnmpGroup returned: %v", grp)
	}

	//Open a SnmpSession and test whether SnmpGroup created in SnmpAPI
	ses := NewSnmpSession(api)
	udp := NewUDPProtocolOptions()
	udp.SetLocalHost(localHost)
	udp.SetLocalPort(localPort)
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err := ses.Open()
	if err != nil {
		t.Error(err.Error())
	}
	grp = api.SnmpGroup(localHost, localPort)
	if grp == nil {
		t.Error("SnmpGroup not created by SnmpSession open")
	}
	ses.Close()
	grp = api.SnmpGroup(localHost, localPort)
	if grp != nil {
		t.Error("SnmpGroup not removed by SnmpSession while closing")
	}

	//Add valid SnmpGroup in SnmpAPI and test
	grp = new(SnmpGroup)
	api.addSnmpGroup("192.168.11.170", 80, grp)
	grp = api.SnmpGroup("192.168.11.170", 80)
	if grp == nil {
		t.Error("SnmpAPI addSnmpGroup(): Failure in adding SnmpGroup")
	}

	//Add Invalid SnmpGroup in SnmpAPI and test
	grp = new(SnmpGroup)
	api.addSnmpGroup("aa", 1, grp)
	grp = api.SnmpGroup("aa", 1)
	if grp != nil {
		t.Error("SnmpAPI addSnmpGroup(): SnmpGroup created for invalid IP address.")
	}
}

/*Session List Test*/
func TestSessionList(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)
	ses.SetProtocolOptions(udp)
	err := ses.Open()
	if err != nil {
		t.Error(err.Error())
	}
	l := api.sessionList.Len()
	if l != 1 {
		t.Error("API's session list is invalid")
	}

	api.RemoveSnmpSession(ses) //Remove the SnmpSession from API
	if api.sessionList.Len() != 0 {
		t.Error("API's session list is invalid")
	}
}

/*CheckTimeouts Test*/
func TestTimeouts(t *testing.T) {
	api := NewSnmpAPI()
	ses := NewSnmpSession(api)
	udp := NewUDPProtocolOptions()
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(1) //Invalid Port
	ses.SetProtocolOptions(udp)
	err := ses.Open()
	if err != nil {
		t.Error(err.Error())
	}

	mesg := msg.NewSnmpMessage()
	mesg.SetCommand(consts.GetRequest)
	mesg.SetTimeout(1000)
	oid := snmpvar.NewSnmpOID("1.4.0")
	if oid != nil {
		mesg.AddNull(*oid)
	}

	noTimeouts := 5
	for i := 0; i < noTimeouts; i++ {
		ses.SyncSend(mesg)
	}

	sesList := api.CheckTimeouts()
	if len(sesList) < 1 { //There should be at least one timeout
		t.Errorf("SnmpAPI CheckTimeouts(): Failure in capturing Session Timeouts")
	}

	/*time.Sleep(time.Second*10)
	for _, se := range sesList {
		if se.timeoutList.Len() != noTimeouts {
			t.Errorf("Failure in capturing Session Timeouts. Timeouts list should be of length: %d. Received Timeouts: %d.", noTimeouts, se.timeoutList.Len())
		}
		break
	}*/

	api.Close()
}

/*LCD Test*/
func TestLCD(t *testing.T) {
	api := NewSnmpAPI()

	//Valid EngineLCD case
	engineLCD := api.PeerEngineLCD(consts.USM)
	if engineLCD == nil {
		t.Error("SnmpAPI PeerEngineLCD(): Nil EngineLCD returned for USM.")
	}
	//Invalid EngineLCD case
	engineLCD = api.PeerEngineLCD(100) //Invalid security model
	if engineLCD != nil {
		t.Error("SnmpAPI PeerEngineLCD(): Invalid EngineLCD returned by API.")
	}

	/*USM PeerEngineLCD test*/
	usmEngLCD := api.USMPeerEngineLCD()
	if usmEngLCD == nil {
		t.Error("SnmpAPI USMPeerEngineLCD(): Nil EngineLCD returned for USM.")
	} else { //Perform some addition, deletion on LCD
		peer := usm.NewUSMPeerEngine(remoteHost, remotePort)
		if err := usmEngLCD.AddEngine(peer); err != nil {
			t.Error(err)
		} else { //Check if the engine is added successfully
			usmEngLCD = api.USMPeerEngineLCD()
			eng := usmEngLCD.Engine(remoteHost, remotePort)
			if eng == nil {
				t.Error("Failure in adding usm peer engine through SnmpAPI")
			}
		}
	}

	/*USM UserLCD test*/
	if usmUserLCD := api.USMUserLCD(); usmUserLCD == nil {
		t.Error("SnmpAPI USMUserLCD(): Nil UserLCD returned for USM.")
	} else { //Perform some addition, deletion on LCD
		if _, err := usmUserLCD.AddUser([]byte("123"),
			"TestUser",
			"",
			"",
			"",
			"",
		); err != nil {
			t.Error(err)
		} else {
			usmUserLCD = api.USMUserLCD()
			user, err := usmUserLCD.SecureUser([]byte("123"), "TestUser")
			if err != nil || user == nil {
				t.Error("Failure in adding usm secure user through SnmpAPI")
			}

		}
	}

}
