/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package snmp

import (
	"sync"

	"webnms/log"
	"webnms/snmp/msg"
)

//SnmpCallBack allows us to receive the responses through a separate GoRoutine.
//User should not create an instance of this type.
//
//Use SetCallbackRoutine() method on the SnmpSession to set CallBackRoutine Flag, in case callback for response should come from a separate goroutine.
type SnmpCallBack struct {
	session       *SnmpSession
	mutex         sync.RWMutex
	callbackOpen  bool
	resNotify     chan *msg.SnmpMessage
	timeoutNotify chan *msg.SnmpMessage
	stop          chan bool //To Stop the callbackRoutine
}

//newSnmpCallBack returns new instance of SnmpCallBack after setting the default values. Only for internal package use.
func newSnmpCallBack(ses *SnmpSession) *SnmpCallBack {
	cb := new(SnmpCallBack)
	cb.session = ses
	cb.callbackOpen = true
	cb.resNotify = make(chan *msg.SnmpMessage, 1000)     //Buffered Channel
	cb.timeoutNotify = make(chan *msg.SnmpMessage, 1000) //Buffered Channel
	cb.stop = make(chan bool)

	go cb.callBackRoutine() //Start the CallBack routine

	return cb
}

//Add the SnmpMessage 'msg' to the response channel of SnmpCallBack 'cb'.
func (cb *SnmpCallBack) addToResponseMaps(msg *msg.SnmpMessage) {
	cb.mutex.Lock()
	defer cb.mutex.Unlock()
	cb.resNotify <- msg
}

//Add the SnmpMessage 'msg' to the timeout channel of SnmpCallBack 'cb'.
func (cb *SnmpCallBack) addToTimeoutMaps(msg *msg.SnmpMessage) {
	cb.mutex.Lock()
	defer cb.mutex.Unlock()
	cb.timeoutNotify <- msg
}

//stopCallBackRoutine stops the CallBackRoutine running.
func (cb *SnmpCallBack) stopCallBackRoutine() {
	cb.callbackOpen = false
	cb.stop <- true
}

//Monitoring routine which performs callback for response packets and timeout packets
func (cb *SnmpCallBack) callBackRoutine() {

	log.Debug("Started CallBack routine to perform SNMP callbacks.")
cbRoutine:
	for {
		if !cb.session.IsSessionOpen() || !cb.callbackOpen {
			break
		}
		//Wait for response/timeout msgs on channel
		select {
		case res := <-cb.resNotify:
			cb.session.processReceivedPacket(res) //Call the SnmpSession's processReceivedPacket() method to perform further processing
		case timeout := <-cb.timeoutNotify:
			cb.session.notifyTimeOut(timeout) //Notify SnmpClients with Nil PDU
		case <-cb.stop:
			break cbRoutine
		}
	}
	log.Debug("End of CallBack routine.")
}
