/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

/*
 * SnmpModel
 */

//Interface that all the SnmpModels must implement in order to be integrated with this framework.
//
//All the models defined under the Subsystem of the engine framework should implement this interface.
type SnmpModel interface { //SnmpSecurityModel, SnmpMsgProcessingModel
	//SubSystem returns instance of SnmpSubSystem to which this model is associated with.
	SubSystem() SnmpSubSystem

	//Name returns the name of the SnmpModel.
	Name() string

	//ID returns the unique id of the SnmpModel.
	ID() int32
}

/*
* SnmpMsgProcessingModel
 */

//SnmpMsgProcessingModel interface defines generic methods for processing all the versions of incoming/outgoing messages as per RFC 3411 - 4.2.
//Each message processing models should implement this interface.
//
//Ex: V3MP is an implementation of this interface.
//
//This model is called by the Dispatcher when messages are sent (or) received.
//
//Note: This interface type extends (Embeds) SnmpModel interface.
type SnmpMsgProcessingModel interface { //extends SnmpModel
	SnmpModel //  Extends SnmpModel interface

	//PrepareDataElements process the incoming message from the remote entity and returns decoded SNMP Message.
	//It performs the unmarshalling of the received raw bytes and performs necessary securtity checks
	//based on the SNMP Version and security level.
	//It returns the SnmpMessage in case of successful decoding/security checks, else returns nil.
	//StatusInformation contains security error if any.
	PrepareDataElements(dispatcher Dispatcher,
		protocolOptions transport.ProtocolOptions,
		wholeMsg []byte,
		wholeMsgLength int, //as received in the wire
		tmStateReference transport.TransportStateReference, //Transport specific cache
	) (int32, *msg.SnmpMessage, StateReference, *StatusInformation) //Can we return a decoded SnmpMessage instead??

	//PrepareOutgoingMessage process the outgoing message belongs to Read/Write/Notification class.
	//It performs marshalling (encoding) and necessary security services based on the security model
	//and returns a final encoded message as bytes.
	//Returns error if unable to prepare the message.
	PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
		securityModel int32,
		securityName string,
		securityLevel consts.SecurityLevel,
		contextEngineId []byte,
		contextName string,
		pdu msg.SnmpPDU,
		expectResponse bool,
	) (MsgID int32, SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/)

	//PrepareResponseMessage process the outgoing message belongs to Response/Internal class.
	//It performs marshalling (encoding) and necessary security services based on the security model
	//and returns a final encoded message as bytes.
	//cache will be used while preparing outgoing response message.
	//Returns error if unable to prepare the message.
	PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
		maxResponseSize int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
		securityModel int32,
		securityName string,
		securityLevel consts.SecurityLevel,
		contextEngineId []byte,
		contextName string,
		pdu msg.SnmpPDU,
		cache StateReference, //This cache is required for sending the response
		statusInfo *StatusInformation,
	) (SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/)

	//EncodeMessage encodes the SnmpMessage msg passed based on the SNMP version passed and returns an encoded byte array.
	//Returns error in case of failure in encoding.
	EncodeMessage(msg msg.SnmpMessage) ([]byte, error)

	//Encode encodes the data such as HeaderData, ScopedPDU and returns the byte array. Encoding should be used for Unencrypted data.
	//Returns error in case of failure in encoding.
	Encode(version consts.Version,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		msgSecurityModel int32,
		msgSecurityParameters []byte,
		scopedPDU msg.ScopedPDU,
	) ([]byte, error)

	//EncodePriv encodes the data such as HeaderData, Encrypted ScopedPDU and returns the byte array.
	//This method should be used for Encrypted data.
	//Returns error in case of failure in encoding.
	EncodePriv(version consts.Version,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		msgSecurityModel int32,
		msgSecurityParameters []byte,
		encryptedScopedPDU []byte,
	) ([]byte, error)

	//EncodeScopedPDU encodes the ScopedPDU portion the SNMPv3 message and returns the byte array.
	//Returns error in case of failure in encoding.
	EncodeScopedPDU(scopedPDU msg.ScopedPDU) ([]byte, error)

	//DecodeScopedPDu decodes the scopedBytes into ScopedPDU type.
	//Returns error in case of failure in decoding.
	DecodeScopedPDU(scopedBytes []byte) (*msg.ScopedPDU, error)

	//IncSnmpInASNParseErrsCounter increments the SnmpInParseErrs Counter in case of any error in parsing the incoming message.
	IncSnmpInASNParseErrsCounter() uint32
}

/*
 * SnmpSecurityModel
 */

//SnmpSecurityModel interface defines all generic methods to perform security related operations on incoming and outgoing message as per RFC 3411 - 4.4.
//All Security models such as USM should implement this interface and register itself with the Security SubSystem.
//This model is responsible for filling the security parameters part of the SNMP message.
//
//Ex: USM is an implementation of this interface.
//
//This model is called by the SnmpMsgProcessingModel when messages are sent (or) received to perform security related operations.
//
//Note: This interface type extends (Embeds) SnmpModel interface.
type SnmpSecurityModel interface { //Extends SnmpModel
	SnmpModel

	//ProcessIncomingRequest processes the incoming request messages. It performs the necessary security check
	//on the incoming messages and returns error in case of any security error as the StatusInformation,
	//which would be processed by SnmpMsgProcessingSubSystem.
	//It returns the decrypted scopedPDU and security parameters after all the security check.
	ProcessIncomingRequest(cache SecurityStateReference,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		securityParameters []byte,
		securityModel int32,
		wholeMsg []byte, //data as received in the wire
		wholeMsgLength int,
		scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
		protocolOptions transport.ProtocolOptions,
		tmStateReference transport.TransportStateReference, //Transport specific cache
	) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/) //Define required params for processing the message

	//ProcessIncomingResponse processes the incoming response/report messages. It performs the necessary
	//security check on the incoming messages and returns error in case of any security error as the StatusInformation,
	//which would be processed by SnmpMsgProcessingSubSystem.
	//Cache which has been prepared while sending a request will be used now for processing this response.
	//It returns the decrypted scopedPDU and security parameters after all the security check.
	ProcessIncomingResponse(cache SecurityStateReference,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		securityParameters []byte,
		securityModel int32,
		wholeMsg []byte, //data as received in the wire
		wholeMsgLength int,
		scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
		protocolOptions transport.ProtocolOptions,
		tmStateReference transport.TransportStateReference, //Transport specific cache
	) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/)

	//GenerateRequestMsg prepares the SnmpMessage after applying the necessary security on the message.
	//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
	//It returns the final encoded byte array along with necessary security parameters filled.
	//Returns error as a StatusInformation if any.
	GenerateRequestMsg(version consts.Version,
		msgId int32,
		msgMaxSize int32, //This should be used for outgoing response
		msgFlags byte,
		securityModel int32,
		securityName string, //For response, securityName is ignored and value from cache is used.
		securityEngineId []byte,
		scopedPDU msg.ScopedPDU,
		protocolOptions transport.ProtocolOptions,
	) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/)

	//GenerateResponseMsg prepares the SnmpMessage after applying the necessary security on the message.
	//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
	//It returns the final encoded byte array along with necessary security parameters filled.
	//cache which has been prepared from the incoming request will be used now for preparing this response message.
	//Returns error as a StatusInformation if any.
	GenerateResponseMsg(version consts.Version,
		msgId int32,
		msgMaxSize int32, //This should be used for outgoing response
		msgFlags byte,
		securityModel int32,
		securityName string, //For response, securityName is ignored and value from cache is used.
		securityEngineId []byte,
		scopedPDU msg.ScopedPDU,
		cache SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
		protocolOptions transport.ProtocolOptions,
	) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/)

	//PeerEngineLCD returns the instance of SnmpPeerEngineLCD, which contains the details of remote peers such as EngineID, EngineBoots etc.,
	//If the security model doesnot store the remote peers details, it returns nil.
	PeerEngineLCD() SnmpPeerEngineLCD

	//CreateNewSecurityCache creates and returns a security state reference, which will be used for sending response messages.
	CreateNewSecurityCache() SecurityStateReference

	//ReleaseSecurityCache releases the security cache (SecurityStateReference) stored.
	ReleaseSecurityCache(SecurityStateReference)
}

/*
	Transport Model (TM)
*/

//SnmpTransportModel interface defines all generic methods to perform transport operations for sending and receiving SNMP packets on network.
//All Transport models such as UDP/TCP should implement this interface and register itself (plugged in) with the Transport SubSystem.
//
//Transport Model is a part of transport subsystem as per definition of RFC5590.
//
//Ex: UDPTransportModel is an implementation of this interface.
//
//This model is used by the Dispatcher (SnmpSession) for sending and receiving the SNMP messages on the network.
//
//Note:
//  1. SnmpTransportModel has one-to-one association with the ProtocolOptions defined in transport package.
//  2. This interface type extends (Embeds) SnmpModel interface.
type SnmpTransportModel interface {
	SnmpModel //Extends SnmpModel interface

	//Sets the transport subsystem on the transport model.
	//Model may optionally register itself with this subsystem using 'AddModel' method.
	SetTransportSubSystem(SnmpTransportSubSystem)

	//Open opens the transport interface with the ProtocolOptions provided, over which the data is sent/received.
	//ProtocolOptions should be filled with the local host/port where the connection is opened.
	//
	//Returns error in case of failure in opening the connection.
	Open(transport.ProtocolOptions) error

	//Read receive data from the peer over the transport interface.
	//Data will be read into the Buffer provided in the SnmpTransportPacket and it returns the number of bytes read and error if any.
	//
	//TransportStateReference cache specific to the transport model can be created and set on this transport packet. This cache will be later utilized by the transport-aware security model
	//for processing. This cache can be ignored if it is not relevant to this model.
	//
	//This method should fill the transport packet's ProtocolOptions with the remote address and port from which the packet is received.
	Read(*transport.SnmpTransportPacket) (int, error)

	//Write sends data to the peer over the transport interface and returns error if there is any failure in sending data.
	//SnmpTranportPacket contains the encoded SnmpMessage as ProtocolData and the remote destination to which the data should be sent.
	//
	//TransportStateReference available in the transport packet can be used for transport layer security processing, if one is available.
	Write(*transport.SnmpTransportPacket) error

	//Close closes the transport interface after communication is over. Returns error in case of failure in closing the connection.
	Close() error
}
