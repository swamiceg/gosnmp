/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

/*
 *	Transport Subsystem (RFC5590)
 */

//SnmpTransportSubSystem interface is an extension of SnmpSubSystem. Transport Subsystem is defined as per the 'RFC5590'.
//It is part of the Engine architecture which takes care of the transport related operations.
//
//It holds all the Transport Models such as UDP, TCP required for sending and receiving SNMP messages on wire.
//All the Transport Models should be registered with this SubSystem.
//
//Note: This interface type extends (Embeds) SnmpSubSystem interface.
type SnmpTransportSubSystem interface {
	SnmpSubSystem //Extend SnmpSubSystem interface
}

//Transport SubSystem is a part of SnmpEngine architecture as per definition of RFC5590.
//This SubSystem will hold all the transport models of the API such as UDP, TCP.
//
//Default implementation of SnmpTransportSubSystem interface.
type SnmpTransportSubSystemImpl struct {
	*snmpSubSystemImpl //Implement SnmpSubSystem interface
}

//NewTransportSubSystem returns a new instance of Transport SubSystem. This method should be called to perform any transport operations.
//Users should not instantiate this type.
//
//SnmpEngine will hold an instance of this transport subsystem.
func NewTransportSubSystem(eng SnmpEngine) *SnmpTransportSubSystemImpl {
	tpSS := new(SnmpTransportSubSystemImpl)
	tpSS.snmpSubSystemImpl = newSubSystem(eng)

	return tpSS
}
