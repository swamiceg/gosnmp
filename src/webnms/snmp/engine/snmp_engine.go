/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"time"
	"webnms/snmp/db"
)

//########## SnmpEngine ##############//

//SnmpEngine represents the local entity's SNMP Engine as defined in RFC3411, SNMPv3 architecture.
//It holds the instance of all the SubSystems.
//
//API user need not instantiate this type. SnmpAPI in snmp package will instantiate this type and all their SubSystems.
//SnmpEngine has one-to-one mapping with the SnmpAPI type. Hence, instantiating an SnmpAPI represents a single
//engine, thereby representing a single SNMP Entity.
type SnmpEngine interface {
	//SnmpEngineID returns the engine id of the local entity's SnmpEngine.
	SnmpEngineID() []byte

	//SetSnmpEngineID sets the local entity's SnmpEngineID.
	SetSnmpEngineID(engineID []byte)

	//SnmpEngineBoots returns the engine boots value of the local entity's SnmpEngine.
	SnmpEngineBoots() int32

	//SnmpEngineTime returns the engine time value of the local entity's SnmpEngine.
	//It returns the latest running time of the engine.
	SnmpEngineTime() int32

	//MsgProcessingSubSystem returns the Message Processing SubSystem instance set on SnmpEngine.
	//It provides services for processing incoming and outgoing SNMP messages.
	MsgProcessingSubSystem() SnmpMsgProcessingSubSystem

	//SetMsgProcessingSubSystem sets the Message Processing SubSystem instance on this SnmpEngine.
	//User should not use this method. SnmpAPI will set this one on behalf of us.
	SetMsgProcessingSubSystem(SnmpMsgProcessingSubSystem) //We can remove the setter methods

	//SecuritySubSystem returns the Security SubSystem instance set on eng.
	//It provides services for performing authentication, encryption, decryption on incoming and outgoing messages.
	SecuritySubSystem() SnmpSecuritySubSystem

	//SetSecuritySubSystem sets the Security SubSystem instance on this SnmpEngine.
	//User should not use this method. SnmpAPI will set this one on behalf of us.
	SetSecuritySubSystem(SnmpSecuritySubSystem)

	//TransportSubSystem returns the Transport SubSystem instance set on SnmpEngine.
	//It provides services for sending and receiving messages on the network.
	TransportSubSystem() SnmpTransportSubSystem

	//TransportSubSystem sets the Transport SubSystem instance on this SnmpEngine.
	//User should not use this method. SnmpAPI will set this one on behalf of us.
	SetTransportSubSystem(SnmpTransportSubSystem)

	//AccessControlSubSystem() *AccessControlSubSystem

	//SnmpLCD returns the instance of SnmpLCD stored in this engine.
	//SnmpLCD is the local configuration datastore which stores the details of different subsystems and models specific information.
	SnmpLCD() *SnmpLCD

	//SetDBOperations sets the database implementation on SnmpEngine
	//which can be used by the SnmpModels to perform database Operations.
	SetDBOperations(dbImpl *db.DBOperationsImpl)

	//DBOperations returns the instance of DBOperations stored in SnmpEngine.
	//Returns nil if one is not initialized (or) Database usage has been disabled.
	DBOperations() *db.DBOperationsImpl

	//SetDatabase enables/diables the usage of database for all the SNMP operations internally.
	//If enabled, DB initialized through SNMP API will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
	SetDatabaseFlag(flag bool)
}

//SnmpEngineImpl is the default implementation of SnmpEngine interface.
//It holds the local entity's EngineID, EngineBoots, EngineTime and various SubSystems.
//
//API user need not instantiate this type.
type SnmpEngineImpl struct {
	engineID                 *SnmpEngineID //We will deal with this later
	authoritativeEngineID    []byte        //Cannot be all zeros or 'ff' or nil
	authoritativeEngineBoots int32         // 1 to 2147483647
	authoritativeEngineTime  int32         // 1 to 2147483647
	startTime                int64         //Time when this engine has been started first
	lcd                      *SnmpLCD      // Store the LCD instance
	dbImpl                   *db.DBOperationsImpl
	dbFlag                   bool //Helps in enabling/diabling the Database Operations.
	//Subsystem instances
	msgProcessingSubSystem SnmpMsgProcessingSubSystem
	secSubSystem           SnmpSecuritySubSystem
	transSubSystem         SnmpTransportSubSystem
}

//NewSnmpEngine creates a new SNMP Engine and initialize the SnmpEngineID based on the address, port and iana number.
func NewSnmpEngine(lcd *SnmpLCD, address string, port, iana int) *SnmpEngineImpl {
	engine := new(SnmpEngineImpl)
	engine.lcd = lcd
	engine.initEngine(address, port, iana)

	return engine
}

//To initialize the SnmpEngineID
func (eng *SnmpEngineImpl) initEngine(address string, port, iana int) {
	if eng.authoritativeEngineID == nil {
		//Initialize default engine id
		eng.engineID = CreateEngineID(address, port, iana)
		if eng.engineID != nil {
			eng.authoritativeEngineID = eng.engineID.Bytes()
		}
	}
	//Initializes start time and ET of the SnmpEngine
	eng.SetSnmpEngineTime(0)
	eng.storeEngBoots(0) //Initialize the engine boots for very first time
}

//MsgProcessingSubSystem returns the Message Processing SubSystem instance set on eng.
//It provides services for processing incoming and outgoing SNMP messages.
func (eng *SnmpEngineImpl) MsgProcessingSubSystem() SnmpMsgProcessingSubSystem {
	return eng.msgProcessingSubSystem
}

//SetMsgProcessingSubSystem sets the Message Processing SubSystem instance on this SnmpEngine eng.
//User should not call this method. SnmpAPI will set this one on behalf of us.
func (eng *SnmpEngineImpl) SetMsgProcessingSubSystem(msgProcessingSubSystem SnmpMsgProcessingSubSystem) {
	eng.msgProcessingSubSystem = msgProcessingSubSystem
}

//SecuritySubSystem returns the Security SubSystem instance set on eng.
//It provides services for performing authentication, encryption, decryption on incoming and outgoing messages.
func (eng *SnmpEngineImpl) SecuritySubSystem() SnmpSecuritySubSystem {
	return eng.secSubSystem
}

//SetSecuritySubSystem sets the Security SubSystem instance on this SnmpEngine eng.
//User should not call this method. SnmpAPI will set this one on behalf of us.
func (eng *SnmpEngineImpl) SetSecuritySubSystem(secSubSystem SnmpSecuritySubSystem) {
	eng.secSubSystem = secSubSystem
}

//TransportSubSystem returns the Transport SubSystem instance set on SnmpEngine eng.
//It provides services for sending and receiving messages on the network.
func (eng *SnmpEngineImpl) TransportSubSystem() SnmpTransportSubSystem {
	return eng.transSubSystem
}

//TransportSubSystem sets the Transport SubSystem instance on this SnmpEngine eng.
//User should not use this method. SnmpAPI will set this one on behalf of us.
func (eng *SnmpEngineImpl) SetTransportSubSystem(transSubSystem SnmpTransportSubSystem) {
	eng.transSubSystem = transSubSystem
}

//SnmpLCD returns the Local Data Store set on this SnmpEngine eng. It holds the data of all SNMP Models
//defined in different layers of SubSystems.
//
//For ex: USMUserLCD of USM Security model, which holds the user details required for USM.
func (eng *SnmpEngineImpl) SnmpLCD() *SnmpLCD {
	return eng.lcd
}

//SnmpEngineID returns the engine id of the local entity's SnmpEngine.
func (eng *SnmpEngineImpl) SnmpEngineID() []byte {
	return eng.authoritativeEngineID
}

//SetSnmpEngineID sets the local entity's SnmpEngineID.
func (eng *SnmpEngineImpl) SetSnmpEngineID(engineID []byte) {
	if engineID != nil {
		eng.authoritativeEngineID = engineID
	}
}

//SnmpEngineBoots returns the engine boots value of the local entity's SnmpEngine.
func (eng *SnmpEngineImpl) SnmpEngineBoots() int32 {
	return eng.authoritativeEngineBoots
}

func (eng *SnmpEngineImpl) SetSnmpEngineBoots(boots int32) {
	if boots < 0 || boots > 0x7FFFFFFF {
		eng.authoritativeEngineBoots = 0x7FFFFFFF
	}
	eng.authoritativeEngineBoots = boots
}

func (eng *SnmpEngineImpl) storeEngBoots(boots int32) { //While initialization
	if boots < 0 { //Lower Bound
		eng.authoritativeEngineBoots = 0x7FFFFFFF
	} else if boots >= 0x7FFFFFFF { //Upper Bound - Reinitialize to zero.
		eng.authoritativeEngineBoots = 0
	} else {
		eng.authoritativeEngineBoots = boots + 1
	}
}

//SnmpEngineTime returns the engine time value of the local entity's SnmpEngine.
//It returns the latest running time of the engine eng in seconds.
func (eng *SnmpEngineImpl) SnmpEngineTime() int32 {
	//Time elapse in milliseconds - can be greater than 2147483647
	timeElapsed := (time.Now().UnixNano() / 1e9) - eng.startTime
	if timeElapsed > 0x7FFFFFFF { //That's really great. It reached a max value of '68' years.
		//We should reboot our engine and reinitialize engineTime
		eng.startTime = time.Now().UnixNano() / 1e9
		eng.storeEngBoots(eng.authoritativeEngineBoots) //Increment EngineBoots value
	}

	runningTime := (time.Now().UnixNano() / 1e9) - eng.startTime
	return eng.authoritativeEngineTime + int32(runningTime&0x7FFFFFFF) //Current running time of the Engine (Updated value)
}

func (eng *SnmpEngineImpl) SetSnmpEngineTime(timeVal int32) { //Not needed
	if timeVal >= 0 && timeVal <= 0x7FFFFFFF {
		eng.authoritativeEngineTime = timeVal
		eng.startTime = time.Now().UnixNano() / 1e9 //Reset the engine start time
	}
}

//SetDBOperations sets the database implementation on SnmpEngine
//which can be used by the SnmpModels to perform database Operations.
func (eng *SnmpEngineImpl) SetDBOperations(dbImpl *db.DBOperationsImpl) {
	eng.dbImpl = dbImpl
}

//DBOperations returns the instance of DBOperations stored in SnmpEngine.
//Returns nil if one is not initialized (or) Database usage has been disabled.
func (eng *SnmpEngineImpl) DBOperations() *db.DBOperationsImpl {
	if !eng.dbFlag {
		return nil //If Database is diabled, pass nil always
	}

	return eng.dbImpl
}

//SetDatabase enables/diables the usage of database for all the SNMP operations internally.
//If enabled, DB initialized through SNMP API will be used, else it will use the local LCD (runtime memory) for storing/retrieving values.
func (eng *SnmpEngineImpl) SetDatabaseFlag(flag bool) {
	eng.dbFlag = flag
}
