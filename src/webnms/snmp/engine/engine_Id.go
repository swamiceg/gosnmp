/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"encoding/binary"
	"encoding/hex"
	"net"
	"strconv"
	"strings"
)

//SnmpEngineID represents the unique and unambigous identifier of SnmpEngine. It holds the EngineID instance.
//
//Provides various methods to generate unique EngineID based on the value of address, port and iana number.
//For internal use only.
type SnmpEngineID struct {
	engineIdBytes []byte
	engineIdHex   string
}

//Returns binary Engine ID.
func (engID *SnmpEngineID) Bytes() []byte {
	return engID.engineIdBytes
}

//Returns hex string format of the Engine ID.
func (engID *SnmpEngineID) HexString() string {
	return engID.engineIdHex
}

//Calculate the EngineID based on host address, port and IANA.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func (engID *SnmpEngineID) CreateEngineID(address string, port int, iana int) {
	if engineID := CreateEngineID(address, port, iana); engineID != nil {
		engID.engineIdBytes = engineID.engineIdBytes
		engID.engineIdHex = engineID.engineIdHex
	}
}

//Calculate the EngineID based on host address.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func (engID *SnmpEngineID) CreateEngineIDByHost(host string) {
	if engineID := CreateEngineIDByHost(host); engineID != nil {
		engID.engineIdBytes = engineID.engineIdBytes
		engID.engineIdHex = engineID.engineIdHex
	}
}

//Calculate the EngineID based on host port number.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func (engID *SnmpEngineID) CreateEngineIDByPort(port int) {
	if engineID := CreateEngineIDByPort(port); engineID != nil {
		engID.engineIdBytes = engineID.engineIdBytes
		engID.engineIdHex = engineID.engineIdHex
	}
}

//Calculate the EngineID based on host address and port.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func (engID *SnmpEngineID) CreateEngineIDByAddress(host string, port int) {
	if engineID := CreateEngineIDByAddress(host, port); engineID != nil {
		engID.engineIdBytes = engineID.engineIdBytes
		engID.engineIdHex = engineID.engineIdHex
	}
}

//Calculate the EngineID based on host address, port and IANA.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func CreateEngineID(address string, port int, iana int) *SnmpEngineID {

	if address == "" { //If address is empty, localhost will taken as address
		//We use localhost address
		addr, err := net.LookupIP("localhost")
		if err != nil {
			address = "0.0.0.0" //Default IP Address
		} else {
			address = addr[0].String()
		}
	}

	ipAddr, err := net.ResolveIPAddr("ip", address)
	if err != nil {
		return nil
	}

	var addrBytes []byte
	addrBytes = getAddressAsBytes(ipAddr.IP.String())

	//Our EngineID is variable length OCTET STRING
	var engineId []byte = make([]byte, (9 + len(addrBytes))) //4(IANA) + 1(IP) + 4(Port)

	//First 4 Octets IANA in MSB
	binary.BigEndian.PutUint32(engineId[:4], uint32(iana))
	engineId[0] |= 0x80 //First bit is set to 1.

	//It's an IPv4 Address
	if ipAddr.IP.To4() != nil {
		engineId[4] = 0x01
	} else { //It's an IPv6 Address
		engineId[4] = 0x02
	}

	//Fill the address now
	for i := 5; i < (5 + len(addrBytes)); i++ {
		engineId[i] = addrBytes[i-5]
	}

	//Port
	binary.BigEndian.PutUint32(engineId[(5+len(addrBytes)):], uint32(port))

	engId := new(SnmpEngineID)
	engId.engineIdBytes = engineId
	engId.engineIdHex = hex.EncodeToString(engineId)

	return engId
}

//Calculate the EngineID based on host address.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func CreateEngineIDByHost(host string) *SnmpEngineID {
	port := 0  //Default Port
	iana := 42 //Default SUN IANA

	return CreateEngineID(host, port, iana)
}

//Calculate the EngineID based on host port number.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func CreateEngineIDByPort(port int) *SnmpEngineID {
	var localAddr string
	var iana int = 42 //Default SUN IANA

	//We use localhost address
	addr, err := net.LookupIP("localhost")
	if err != nil {
		localAddr = "0.0.0.0" //Default IP Address
	} else {
		localAddr = addr[0].String()
	}

	return CreateEngineID(localAddr, port, iana)
}

//Calculate the EngineID based on host address and port.
//This algorithm is based on RFC3411 - Section 5 - SnmpEngineID MIB Node definition.
func CreateEngineIDByAddress(host string, port int) *SnmpEngineID {
	var iana int = 42 //Default SUN IANA

	return CreateEngineID(host, port, iana)
}

func getAddressAsBytes(ipAddrStr string) []byte {
	var addrBytes []byte
	var err error
	ipAdd, err := net.ResolveIPAddr("ip6", ipAddrStr)
	if err != nil {
		return nil
	}

	var num int
	if ipAdd.IP.To4() != nil { //IPv4 Address
		addrBytes = make([]byte, net.IPv4len)
		for i, str := range strings.Split(ipAdd.IP.String(), ".") {
			num, err = strconv.Atoi(str)
			if err != nil {
				return nil //Not a valid number
			}
			addrBytes[i] = byte(num)
		}
	} else { //IPv6 Address - We will give correct handling for this later.
		addrBytes = make([]byte, net.IPv6len)
		strArray := strings.Split(ipAdd.IP.String(), ":")
		j := 0
		for i := (len(addrBytes) - len(strArray)); i < len(addrBytes); i++ {
			if strArray[j] == "" {
				num = 0
			} else {
				num, err = strconv.Atoi(strArray[j])
				if err != nil {
					return nil //Not a valid number
				}
			}
			j++
			addrBytes[i] = byte(num)
		}
	}

	return addrBytes
}
