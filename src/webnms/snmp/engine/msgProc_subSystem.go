/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"errors"
	"fmt"

	"webnms/snmp/consts"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

/*
 * Message Processing Subsystem
 */

//SnmpMsgProcessingSubSystem interface is the extension of SnmpSubSystem.
//It defines generic methods for processing the incoming and outgoing messages for all the SNMP Versions.
//
//It holds all the SnmpMsgProcessingModels required for processing the incoming/outgoing messages.
//All the SnmpMsgProcessingModels should be registered with this SubSystem.
//
//Note: This interface type extends (Embeds) SnmpSubSystem interface.
type SnmpMsgProcessingSubSystem interface {
	SnmpSubSystem

	//PrepareDataElements process the incoming message from the remote entity and returns decoded SNMP Message.
	//It performs the unmarshalling of the received raw bytes and performs necessary securtity
	//checks based on the SNMP Version and security level.
	//It returns the SnmpMessage in case of successful decoding/security checks, else returns nil.
	//StatusInformation contains security error if any.
	//
	//This method routes the call to the specific message processing model based on the version.
	PrepareDataElements(version consts.Version,
		dispatcher Dispatcher, //SnmpSession by default
		protocolOptions transport.ProtocolOptions,
		wholeMsg []byte, //as received in the wire
		wholeMsgLength int,
		tmStateReference transport.TransportStateReference,
	) (int32, *msg.SnmpMessage, StateReference, *StatusInformation)

	//PrepareOutgoingMessage process the outgoing message belongs to Read/Write/Notification class.
	//It performs marshalling (encoding) and necessary security services based on the security model and
	//returns a final encoded message as bytes.
	//Returns error if unable to prepare the message.
	//
	//This method routes the call to the specific message processing model based on the version.
	PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
		version consts.Version,
		securityModel int32,
		securityName string,
		securityLevel consts.SecurityLevel,
		contextEngineId []byte,
		contextName string,
		pdu msg.SnmpPDU,
		expectResponse bool,
	) (MsgID int32, SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/)

	//PrepareResponseMessage process the outgoing message belongs to Response/Internal class.
	//It performs marshalling (encoding) and necessary security services based on the security model and
	//returns a final encoded message as bytes.
	//cache will be used while preparing outgoing response message.
	//Returns error if unable to prepare the message.
	//
	//This method routes the call to the specific message processing model based on the version.
	PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
		version consts.Version,
		maxResponseSize int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
		securityModel int32,
		securityName string,
		securityLevel consts.SecurityLevel,
		contextEngineId []byte,
		contextName string,
		pdu msg.SnmpPDU,
		cache StateReference, //This cache is required for sending the response
		statusInfo *StatusInformation,
	) (SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/)

	//EncodeMessage encodes the SnmpMessage msg passed based on the SNMP version passed and returns an encoded byte array.
	//Returns error in case of failure in encoding.
	//
	//This method routes the call to the specific message processing model based on the version.
	EncodeMessage(version consts.Version, msg msg.SnmpMessage) ([]byte, error)

	//Encode encodes the data such as HeaderData, ScopedPDU and returns the byte array. Encoding should be used for Unencrypted data.
	//Returns error in case of failure in encoding.
	//
	//This method routes the call to the specific message processing model based on the version.
	Encode(version consts.Version,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		msgSecurityModel int32,
		msgSecurityParameters []byte, //encoded security parameters
		scopedPDU msg.ScopedPDU,
	) ([]byte, error)

	//EncodePriv encodes the data such as HeaderData, Encrypted ScopedPDU and returns the byte array.
	//Encoding should be used only for Encrypted data.
	//Returns error in case of failure in encoding.
	//
	//This method routes the call to the specific message processing model based on the version.
	EncodePriv(version consts.Version,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		msgSecurityModel int32,
		msgSecurityParameters []byte,
		encryptedScopedPDU []byte,
	) ([]byte, error)

	//EncodeScopedPDU encodes the ScopedPDU portion the SNMPv3 message and returns the byte array.
	//Returns error in case of failure in encoding.
	//
	//This method routes the call to the specific message processing model based on the version.
	EncodeScopedPDU(version consts.Version, scopedPDU msg.ScopedPDU) ([]byte, error)

	//DecodeScopedPDu decodes the scopedBytes into ScopedPDU type.
	//Returns error in case of failure in decoding.
	//
	//This method routes the call to the specific message processing model based on the version.
	DecodeScopedPDU(version consts.Version, scopedBytes []byte) (*msg.ScopedPDU, error)

	//IncSnmpInASNParseErrsCounter increments the SnmpInParseErrs Counter in case of any error in parsing the incoming message.
	//
	//This method routes the call to the specific message processing model based on the version.
	IncSnmpInASNParseErrsCounter(version consts.Version) uint32
}

//Default implementation of SnmpMsgProcessingSubSystem interface.
//It holds all the SnmpMsgProcessingModels required for processing the incoming/outgoing messages.
//
//All the SnmpMsgProcessingModel should be registered with this SubSystem.
//User of the API need not create instance of this SubSystem. SnmpAPI will create one and add it to the SnmpEngine instance.
type SnmpMsgProcessingSubSystemImpl struct { //Implementation of SnmpMsgProcessingSubSytem interface and SubSystem interface
	*snmpSubSystemImpl //Implementing SubSystem interface - Maintains list of SnmpMsgProcessingModels
}

//NewMsgProcessingSubSystem returns new instance of message processing subsystem.
//We need engine instance to interact with other subsystems and to get other details.
//
//User need not use this method. For internal use only.
func NewMsgProcessingSubSystem(engine SnmpEngine) *SnmpMsgProcessingSubSystemImpl {
	return &SnmpMsgProcessingSubSystemImpl{
		snmpSubSystemImpl: newSubSystem(engine),
	}
}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) PrepareDataElements(version consts.Version,
	dispatcher Dispatcher,
	protocolOptions transport.ProtocolOptions,
	wholeMsg []byte, //as received in the wire
	wholeMsgLength int,
	tmStateReference transport.TransportStateReference,
) (int32, *msg.SnmpMessage, StateReference, *StatusInformation) {

	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return 0, nil, nil, NewStatusInformation(fmt.Sprintf("No MessageProcessingModels exist to process incoming message with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return 0, nil, nil, NewStatusInformation(fmt.Sprintf("No MessageProcessingModels exist to process incoming message with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.PrepareDataElements(dispatcher,
		protocolOptions,
		wholeMsg,
		wholeMsgLength,
		tmStateReference,
	)

}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
	version consts.Version,
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	pdu msg.SnmpPDU,
	expectResponse bool,
) (MsgID int32, SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return -1, nil, nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to process the outgoing message with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return -1, nil, nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to process the outgoing message with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.PrepareOutgoingMessage(protocolOptions,
		securityModel,
		securityName,
		securityLevel,
		contextEngineId,
		contextName,
		pdu,
		expectResponse,
	)

}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
	version consts.Version,
	maxResponseSize int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	pdu msg.SnmpPDU,
	cache StateReference, //This cache is required for sending the response
	statusInfo *StatusInformation,
) (SnmpMessage []byte, tmStateReference transport.TransportStateReference, err error /*ProtocolOptions (Destination address)*/) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to process the outgoing response with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to process the outgoing response with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.PrepareResponseMessage(protocolOptions,
		maxResponseSize,
		securityModel,
		securityName,
		securityLevel,
		contextEngineId,
		contextName,
		pdu,
		cache,
		statusInfo,
	)
}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) EncodeMessage(version consts.Version, msg msg.SnmpMessage) ([]byte, error) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.EncodeMessage(msg)

}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) Encode(version consts.Version,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32,
	msgSecurityParameters []byte,
	scopedPDU msg.ScopedPDU,
) ([]byte, error) {

	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.Encode(version,
		msgId,
		msgMaxSize,
		msgFlags,
		msgSecurityModel,
		msgSecurityParameters,
		scopedPDU,
	)

}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) EncodePriv(version consts.Version,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32,
	msgSecurityParameters []byte,
	encryptedScopedPDU []byte,
) ([]byte, error) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the message with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.EncodePriv(version,
		msgId,
		msgMaxSize,
		msgFlags,
		msgSecurityModel,
		msgSecurityParameters,
		encryptedScopedPDU,
	)

}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) EncodeScopedPDU(version consts.Version, scopedPDU msg.ScopedPDU) ([]byte, error) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the ScopedPDU with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to encode the ScopedPDU with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.EncodeScopedPDU(scopedPDU)
}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) DecodeScopedPDU(version consts.Version, scopedBytes []byte) (*msg.ScopedPDU, error) {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to decode the ScopedPDU with Version: %d.", version)) //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return nil, errors.New(fmt.Sprintf("No MessageProcessingModels exist to decode the ScopedPDU with Version: %d.", version)) //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired security model
	return mpm.DecodeScopedPDU(scopedBytes)
}

//Refer SnmpMsgProcessingSubSystem interface doc.
func (mps *SnmpMsgProcessingSubSystemImpl) IncSnmpInASNParseErrsCounter(version consts.Version) uint32 {
	var mpm SnmpMsgProcessingModel
	var ok bool

	if model := mps.Model(int32(version)); model != nil {
		if mpm, ok = model.(SnmpMsgProcessingModel); !ok {
			return 0 //SnmpModel available is not of type SnmpMsgProcessingModel
		}
	} else {
		return 0 //No MessageProcessingModels defined with for the version - cannot process the request
	}

	//This should call the desired MsgProcessing model
	return mpm.IncSnmpInASNParseErrsCounter()
}
