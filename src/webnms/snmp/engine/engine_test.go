/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/msg"

	//"fmt"
	"testing"
	"time"
)

//Test the SnmpEngine and all the SnmpSubSystems and SnmpModels under it.

var testEngineID = []struct {
	address string
	port    int
	iana    int
}{
	{"", -1, -1},
	{"localhost", 132, -1},
	{"", 1, -1},
	{"localhost", 100, 22},
}

func TestEngine(t *testing.T) {
	lcd := NewSnmpLCD()
	var eng SnmpEngine

	//1. EngineID test
	for _, test := range testEngineID {
		eng = NewSnmpEngine(lcd, test.address, test.port, test.iana)
		if eng == nil {
			t.Errorf("SnmpEngine: Failure in instantiating engine for address:%s; port :%d; iana:%d;", test.address, test.port, test.iana)
		} else {
			if eng.SnmpEngineID() == nil {
				t.Errorf("SnmpEngine: Failure in EngineID generation for address:%s; port :%d; iana:%d;", test.address, test.port, test.iana)
			}
			if eng.SnmpEngineBoots() != 1 {
				t.Errorf("SnmpEngine: Invalid EB value. Expected: 1. Got: %d,", eng.SnmpEngineBoots())
			}
		}
	}
	//2. EngineTime test - should increment in second
	et := eng.SnmpEngineTime()
	for i := 1; i <= 2; i++ {
		time.Sleep(time.Second * 1)
		if eng.SnmpEngineTime() != (et + 1) {
			t.Errorf("SnmpEngine: Invalid SnmpEngineTime. Expected: %d. Got: %d.", (et + 1), eng.SnmpEngineTime())
		}
		et++
	}

	//3. EngineBoots test
	engImpl := eng.(*SnmpEngineImpl)
	eb := engImpl.SnmpEngineBoots()

	engImpl.startTime = -2147483647 //We are manually changing the startTime to test the EB increment

	//Now, EB should be incremented and ET should be reinitialized.
	if engImpl.SnmpEngineTime() != 0 { //This method will update the EngineBoots value
		t.Errorf("SnmpEngine: ET value not reinitialized. Expected: %d. Got: %d.", 0, engImpl.SnmpEngineTime())
	}
	if engImpl.SnmpEngineBoots() != (eb + 1) {
		t.Errorf("SnmpEngine: EB value not incremented. Expected: %d. Got: %d.", eb+1, engImpl.SnmpEngineBoots())
	}

	//4. SubSystems test
	mps := NewMsgProcessingSubSystem(engImpl)
	secSS := NewSecuritySubSystem(engImpl)
	engImpl.SetMsgProcessingSubSystem(mps)
	engImpl.SetSecuritySubSystem(secSS)

	if engImpl.MsgProcessingSubSystem() == nil ||
		engImpl.SecuritySubSystem() == nil {
		t.Error("SnmpEngine: Failure in Engine's SubSystem initialization.")
	}

	//5. DBOperations
	engImpl.SetDBOperations(new(db.DBOperationsImpl))
	if engImpl.dbImpl == nil {
		t.Error("SnmpEngine: Failure in DB initialization.")
	}
	if engImpl.dbFlag == true {
		t.Error("SnmpEngine: Failure in DB flag initialization. Expected: false. Got: true.")
	}
	engImpl.SetDatabaseFlag(true)
	if engImpl.DBOperations() == nil {
		t.Error("SnmpEngine: Invalid value returned by DBOperations. Expected non nil instance.")
	}
	engImpl.SetDatabaseFlag(false)
	if engImpl.DBOperations() != nil {
		t.Error("SnmpEngine: Invalid value returned by DBOperations. Expected nil instance.")

	}
}

func TestSubSystems(t *testing.T) {
	lcd := NewSnmpLCD()

	eng := NewSnmpEngine(lcd, "localhost", 700, 1)

	//Create the SubSystems
	mps := NewMsgProcessingSubSystem(eng)
	secSS := NewSecuritySubSystem(eng)
	eng.SetMsgProcessingSubSystem(mps)
	eng.SetSecuritySubSystem(secSS)

	if mps.SnmpEngine() != eng {
		t.Error("MPS: Engine instance is wrong.")
	}
	if secSS.SnmpEngine() != eng {
		t.Error("SecuritySS: Engine instance is wrong.")
	}

	//Add/remove models in message processing subsystem
	mpm := new(dummyModel)
	usm := new(dummyModel)
	var ssStruct = []SnmpSubSystem{mps, secSS}
	for _, ss := range ssStruct {
		//We can register any type of models, but only subsystem specific models will be used.
		mpm.id = 1
		ss.AddModel(mpm)
		usm.id = 2
		ss.AddModel(usm)
		ss.AddModel(nil)
		if ss.Model(1) == nil {
			t.Error("AddModel: Failure in adding SnmpModel.")
		}
		if ss.Model(2) == nil {
			t.Error("AddModel: Failure in adding SnmpModel.")
		}
		if len(ss.Models()) == 3 {
			t.Error("AddModel: Added nil SnmpModel.")
		}
		for _, m := range ss.Models() {
			if m != mpm && m != usm {
				t.Error("Models: Failure in adding SnmpModel.")
			}
		}

		ss.RemoveModel(100)
		ss.RemoveModel(1)
		if ss.Model(1) != nil {
			t.Error("RemoveModel: Failure in removing SnmpModel.")
		}
	}

}

type modellcd struct{}     //Dummy implementation of SnmpModelLCD
func (m *modellcd) Clear() {}

var lcdTest = []struct {
	subSystem SnmpSubSystem
	id        []int32
	modelLCD  []SnmpModelLCD
	count     int
}{
	{msgSS, []int32{1, 2, 3}, []SnmpModelLCD{new(modellcd), new(modellcd), nil}, 2},
	{msgSS, []int32{1, -2, -3}, []SnmpModelLCD{new(modellcd), new(modellcd), nil}, 2},
	{secSS, []int32{1, 2, 3}, []SnmpModelLCD{new(modellcd), new(modellcd), new(modellcd)}, 3},
	{secSS, []int32{1}, nil, 0},
}

var msgSS = &SnmpMsgProcessingSubSystemImpl{
	snmpSubSystemImpl: newSubSystem(NewSnmpEngine(NewSnmpLCD(), "localhost", -1, -1)),
}
var secSS = &SnmpSecuritySubSystemImpl{
	snmpSubSystemImpl: newSubSystem(NewSnmpEngine(NewSnmpLCD(), "localhost", -1, -1)),
}

func TestLCD(t *testing.T) {
	var lcd *SnmpLCD

	//Add test
	for _, l := range lcdTest {
		lcd = NewSnmpLCD()
		for i, v := range l.id {
			if l.modelLCD == nil {
				lcd.AddModelLCD(l.subSystem, v, nil)
			} else {
				lcd.AddModelLCD(l.subSystem, v, l.modelLCD[i])
			}
		}
		if len(lcd.LCDsBySubSystem(l.subSystem)) != l.count {
			t.Errorf("AddModelLCD: Invalid model lcd added. Expected: %d. Got: %d.", l.count, len(lcd.LCDsBySubSystem(l.subSystem)))
		}
		if len(lcd.LCDs()) != l.count {
			t.Errorf("AddModelLCD: Invalid model lcd added. Expected: %d. Got: %d.", l.count, len(lcd.LCDs()))
		}
	}

	//Remove test
	lcd = NewSnmpLCD()
	lcd.AddModelLCD(msgSS, 1, new(modellcd))
	lcd.AddModelLCD(secSS, 1, new(modellcd))

	lcd.RemoveModelLCD(msgSS, 1)
	if lcd.ModelLCD(msgSS, 1) != nil {
		t.Error("RemoveModelLCD: Failure in removing ModelLCD.")
	}
	if lcd.ModelLCD(secSS, 1) == nil {
		t.Error("RemoveModelLCD: Failure in removing ModelLCD.")
	}
	lcd.RemoveModelLCD(msgSS, 1)
	lcd.RemoveModelLCD(secSS, 1)
	if lcd.ModelLCD(secSS, 1) != nil {
		t.Error("RemoveModelLCD: Failure in removing ModelLCD.")
	}
	lcd.RemoveModelLCD(nil, 1)

}

func TestMsgSubSystem(t *testing.T) {
	ver := consts.Version1
	msgByte := []byte("swamimsg")
	var secModel int32 = 3
	pdu := new(msg.SnmpPDU)

	lcd := NewSnmpLCD()
	eng := NewSnmpEngine(lcd, "localhost", 100, 11)
	var err error

	var in interface{}
	in = NewMsgProcessingSubSystem(eng)

	//Type checking
	if _, ok := in.(SnmpSubSystem); !ok {
		t.Error("Message processing subsystem doesnot implement SnmpSubSystem interface.")
	}
	if _, ok := in.(SnmpMsgProcessingSubSystem); !ok {
		t.Error("Message processing subsystem doesnot implement SnmpMsgProcessingSubSystem interface.")
	}
	msgSS := in.(SnmpMsgProcessingSubSystem)
	if msgSS.SnmpEngine() != eng {
		t.Error("MPS: Engine instance not initialized properly.")
	}
	//Add some dummy models
	msgSS.AddModel(new(dummyModel))
	msgSS.AddModel(new(dummyModel))

	_, _, _, err = msgSS.PrepareDataElements(ver, nil, nil, msgByte, len(msgByte), nil)
	_, _, _, err = msgSS.PrepareOutgoingMessage(nil, ver, secModel, "", 1, nil, "", *pdu, false)
	_, _, err = msgSS.PrepareResponseMessage(nil, ver, 11, secModel, "", 1, nil, "", *pdu, nil, nil)
	_, err = msgSS.EncodeMessage(ver, msg.NewSnmpMessage())
	_, err = msgSS.Encode(ver, 100, 100, 8, secModel, nil, msg.ScopedPDU{})
	_, err = msgSS.EncodePriv(ver, 100, 100, 8, secModel, nil, nil)
	_, err = msgSS.EncodeScopedPDU(ver, msg.ScopedPDU{})
	_, err = msgSS.DecodeScopedPDU(ver, nil)

	if err == nil {
		t.Error("MPS: Not throwing error when msg processing model is not added.")
	}
}

func TestSecSubSystem(t *testing.T) {
	lcd := NewSnmpLCD()
	eng := NewSnmpEngine(lcd, "localhost", 100, 11)
	var err error

	var in interface{}
	in = NewSecuritySubSystem(eng)

	//Type checking
	if _, ok := in.(SnmpSubSystem); !ok {
		t.Error("Security SubSystem doesnot implement SnmpSubSystem interface.")
	}
	if _, ok := in.(SnmpSecuritySubSystem); !ok {
		t.Error("Security SubSystem doesnot implement SnmpSecuritySubSystem interface.")
	}
	secSS := in.(SnmpSecuritySubSystem)
	if secSS.SnmpEngine() != eng {
		t.Error("Security SubSystem: Engine instance not initialized properly.")
	}
	//Add some dummy models
	secSS.AddModel(new(dummyModel))
	secSS.AddModel(new(dummyModel))

	_, _, _, err = secSS.ProcessIncomingRequest(nil,
		int32(1),
		int32(4343),
		byte(8),
		nil,
		int32(3),
		nil,
		0,
		nil,
		nil,
		nil,
	)

	if err == nil {
		t.Error("Security SubSystem: Not throwing error when security model is not added.")
	}
}

type dummyModel struct{ id int32 }

func (d *dummyModel) SubSystem() SnmpSubSystem {
	return nil
}
func (d *dummyModel) Name() string {
	return ""
}
func (d *dummyModel) ID() int32 {
	return d.id
}
