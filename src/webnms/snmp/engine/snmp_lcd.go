/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

////######################## LCD - Local Configuration DataStore ##########################////

//SnmpLCD stores the Local Configuration DataStore (LCD) of all the SNMP models defined under the SnmpEngine.
//
//Each SnmpEngine holds the single instance of SnmpLCD. One to one mapping with SnmpEngine.
//Each SubSystems defined under the SnmpEngine and each SnmpModels defined under the SubSystems will add/register their LCDs in this store.
//LCD should be an implementation of SnmpModelLCD interface.
//
//  Example:
//  UserSecurityModel defined under SecuritySubSystem will add the UserLCD (which holds the details of the SNMP USM Users)
//  into this SnmpLCD instance.
type SnmpLCD struct {
	subs map[SnmpSubSystem]*subSysStore
	//test map[SnmpSubSystem]map[int]SnmpModelLCD
}

//NewSnmpLCD returns a new instance of SnmpLCD.
//User need not create instance of this type. They can obtain the SnmpLCD instance from SnmpAPI (or) SnmpEngine instance.
func NewSnmpLCD() *SnmpLCD {
	return &SnmpLCD{
		subs: make(map[SnmpSubSystem]*subSysStore),
	}

}

//AddModelLCD add/register an LCD on this SubSystem.
//modelID is the unique id of the SnmpModel.
func (snmpLCD *SnmpLCD) AddModelLCD(subSystem SnmpSubSystem, modelID int32, modelLCD SnmpModelLCD) {
	if subSysStore, ok := snmpLCD.subs[subSystem]; ok { //Store already exists for this SubSystem
		subSysStore.addModelLCD(modelID, modelLCD)
	} else { //Create new store for this SubSystem
		subSysStore := newSubSysStore()
		snmpLCD.subs[subSystem] = subSysStore
		subSysStore.addModelLCD(modelID, modelLCD)
	}
}

//RemoveModelLCD removes the model LCD registered on this snmpLCD based on the modelID.
//Returns nil if no LCD exist for this modelID in this subSystem.
func (snmpLCD *SnmpLCD) RemoveModelLCD(subSystem SnmpSubSystem, modelID int32) SnmpModelLCD {
	if subSysStore, ok := snmpLCD.subs[subSystem]; ok {
		if modelLCD := subSysStore.removeModelLCD(modelID); modelLCD != nil {
			return modelLCD
		} else {
			return nil //return error - No LCD exists for this model 'modelID' under this SubSystem
		}

	} else {
		return nil //return error - No LCD exists under this SubSystem
	}
}

//ModelLCD returns the model LCD based on the modelID registered on this subSystem.
func (snmpLCD *SnmpLCD) ModelLCD(subSystem SnmpSubSystem, modelID int32) SnmpModelLCD {
	if subSysStore, ok := snmpLCD.subs[subSystem]; ok {
		return subSysStore.modelLCD(modelID)
	}

	return nil
}

//LCDsBySubSystem returns a slice of all the LCDs registered under this subSystem.
func (snmpLCD *SnmpLCD) LCDsBySubSystem(subSystem SnmpSubSystem) []SnmpModelLCD {
	if subSysStore, ok := snmpLCD.subs[subSystem]; ok {
		var lcds []SnmpModelLCD
		for _, lcd := range subSysStore.models {
			lcds = append(lcds, lcd)
		}
		return lcds
	}

	return nil
}

//LCDs returns slice of all the LCDs registered in this snmpLCD.
func (snmpLCD *SnmpLCD) LCDs() []SnmpModelLCD {
	var lcds []SnmpModelLCD
	for _, subSysStore := range snmpLCD.subs { //Get store of each SubSystem
		for _, lcd := range subSysStore.models { //Add Models exist under each SubSystem
			lcds = append(lcds, lcd)
		}
	}

	return lcds
}

//Store LCDs of each SNMP models
type subSysStore struct { //Shoud not be visible to others
	models map[int32]SnmpModelLCD
}

func newSubSysStore() *subSysStore {
	return &subSysStore{
		models: make(map[int32]SnmpModelLCD),
	}
}

func (store *subSysStore) addModelLCD(modelID int32, modelLCD SnmpModelLCD) {
	if modelLCD != nil {
		store.models[modelID] = modelLCD
	}
}

func (store *subSysStore) removeModelLCD(modelID int32) SnmpModelLCD {
	if modelLCD, ok := store.models[modelID]; ok {
		delete(store.models, modelID)
		return modelLCD
	}

	return nil
}

func (store *subSysStore) modelLCD(modelID int32) SnmpModelLCD {
	return store.models[modelID]
}

//Interface for all SnmpModels LCD. All SnmpModels should implement this interface when developing custom configuration datastore.
//
//Example: usm.USMUserLCD is an implementation of this interface.
type SnmpModelLCD interface {
	//Clears all the entries from the LCD.
	Clear()
}
