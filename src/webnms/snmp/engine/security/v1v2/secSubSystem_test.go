/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package v1v2

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/mpm"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport/udp"
	"webnms/snmp/msg"

	//"fmt"
	"testing"
)

var v1v2Model engine.SnmpModel
var mp engine.SnmpModel

var secSS engine.SnmpSecuritySubSystem

func init() {
	lcd := engine.NewSnmpLCD()
	eng := engine.NewSnmpEngine(lcd, "localhost", 999, 10)

	//Create SubSystems
	msgSS := engine.NewMsgProcessingSubSystem(eng)
	secSS = engine.NewSecuritySubSystem(eng)
	eng.SetMsgProcessingSubSystem(msgSS)
	eng.SetSecuritySubSystem(secSS)

	//Create Security Models
	v1v2Model = NewSnmpSecurityModelV1V2(secSS)
	mp = mpm.NewMsgProcessingModelV1(msgSS)
	secSS.RemoveModel(security.V1sec)
	secSS.RemoveModel(security.V2Csec)

	secSSTest[2].model = mp
}

var secSSTest = []struct {
	id    int
	model engine.SnmpModel
}{
	{1, nil},
	{2, mp},
	{100, mp},
}

func TestSecuritySubSystem(t *testing.T) {
	scopedPDU := msg.ScopedPDU{}
	udp := new(udp.UDPProtocolOptions)
	udp.SetRemoteHost("localhost")
	udp.SetRemotePort(8001)
	for _, secTest := range secSSTest {
		secSS.AddModel(secTest.model)
		securityModel := int32(secTest.id)
		_, _, _, si := secSS.ProcessIncomingRequest(nil, 1, 22, byte(4), nil, securityModel, nil, 0, nil, udp, nil)
		if si == nil {
			t.Error("SecSS: ProcessIncomingRequest. Not throwing error.")
		}

		_, _, _, si = secSS.ProcessIncomingResponse(nil, 1, 22, byte(4), nil, securityModel, nil, 0, nil, udp, nil)
		if si == nil {
			t.Error("SecSS: ProcessIncomingResponse. Not throwing error.")
		}

		_, _, _, si = secSS.GenerateRequestMsg(consts.Version1, 1, 22, byte(4), securityModel, "secName", nil, scopedPDU, udp)
		if si == nil {
			t.Error("SecSS: GenerateRequestMsg. Not throwing error.")
		}

		_, _, _, si = secSS.GenerateResponseMsg(consts.Version1, 1, 22, byte(4), securityModel, "secName", nil, scopedPDU, nil, udp)
		if si == nil {
			t.Error("SecSS: GenerateResponseMsg. Not throwing error.")
		}

		engLCD := secSS.PeerEngineLCD(securityModel)
		if engLCD != nil {
			t.Error("SecSS: PeerEngineLCD. Invalid Engine LCD returned.")
		}

		securityCache := secSS.CreateNewSecurityCache(securityModel)
		if securityCache != nil {
			t.Error("SecSS: CreateNewSecurityCache. Invalid security cache returned.")
		}

		secSS.ReleaseSecurityCache(securityModel, securityCache)

		secSS.RemoveModel(int32(secTest.id))
	}
}
