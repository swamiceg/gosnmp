/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package v1v2

//Implements SecurityParameters interface required for V1/V2C.
type SnmpV1V2SecurityParameters struct {
	community string
}

func newSnmpV1V2SecurityParameters(community string) *SnmpV1V2SecurityParameters {
	secParams := new(SnmpV1V2SecurityParameters)
	secParams.community = community

	return secParams
}

//We are not dealing with EngineID in V1/V2C.
func (secParams *SnmpV1V2SecurityParameters) SecurityEngineID() []byte {
	return nil
}

//SecurityName is the community string for v1/v2c.
func (secParams *SnmpV1V2SecurityParameters) SecurityName() string {
	return secParams.community
}

//V1/V2C doesn't require encoding of security parameters.
func (secParams *SnmpV1V2SecurityParameters) EncodeSecurityParameters() ([]byte, error) {
	return nil, nil
}

//V1/V2C doesn't require decoding of security parameters.
func (secParams *SnmpV1V2SecurityParameters) DecodeSecurityParameters(secParam []byte) error {
	return nil
}
