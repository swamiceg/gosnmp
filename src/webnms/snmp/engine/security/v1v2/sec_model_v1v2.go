/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package v1v2

import (
	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

//SnmpSecurityModelV1V2 is an implementation of SnmpSecurityModel interface for performing security operations on
//incoming and outgoing v1/v2c messages.
//
//Implements SnmpSecurityModel interface.
type SnmpSecurityModelV1V2 struct {
	*snmpModelImpl
	localEngine engine.SnmpEngine
}

//NewSnmpSecurityModelV1V2 creates and returns a new instance of SnmpSecurityModelV1V2 after registering itself with the SecuritySubSystem.
//
//For internal use only.
func NewSnmpSecurityModelV1V2(securitySubSystem engine.SnmpSecuritySubSystem) *SnmpSecurityModelV1V2 {
	sec := new(SnmpSecurityModelV1V2)
	sec.snmpModelImpl = newSnmpModelImpl(securitySubSystem, "V1/V2C Security Model", security.V1sec)
	sec.localEngine = securitySubSystem.SnmpEngine()

	//Register these models in SecSubSystem
	securitySubSystem.AddModel(sec)
	sec.snmpModelImpl = newSnmpModelImpl(securitySubSystem, "V1/V2C Security Model", security.V2Csec) //Just to add v2c model
	securitySubSystem.AddModel(sec)

	return sec
}

//Implementation of SnmpModel interface
//Just an additional type providing convenience to store SubSystem and Name.
//Should be used as an anonymous field by models.
type snmpModelImpl struct {
	subSystem engine.SnmpSubSystem
	name      string
	id        int32
}

func newSnmpModelImpl(subsystem engine.SnmpSubSystem, name string, modelID int32) *snmpModelImpl {
	return &snmpModelImpl{
		subSystem: subsystem,
		name:      name,
		id:        modelID,
	}
}

//Returns the SubSystem associated with the SnmpSecurityModel.
func (sm *snmpModelImpl) SubSystem() engine.SnmpSubSystem {
	return sm.subSystem
}

//Returns the name of the Model.
func (sm *snmpModelImpl) Name() string {
	return sm.name
}

//Returns the Security Model ID.
func (sm *snmpModelImpl) ID() int32 {
	return sm.id
}

//######### Implementation of SnmpSecurityModel interface #############//

//Refer engine.SnmpSecurityModel interface doc.
func (v1v2sec *SnmpSecurityModelV1V2) ProcessIncomingRequest(cache engine.SecurityStateReference, //This is newly created cache which should be filled by this module.
	msgId int32,
	msgMaxSize int32, //This will be used to calculate the maxSize response we can generate.
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32, engine.SecurityParameters, *msg.ScopedPDU, *engine.StatusInformation) {

	//RFC 3584 - Section 5.2.1
	/*
		//Perform a search in snmpCommunityTable
		commString  = string(securityParameters)

		var entry communityTableEntry  = nil
		//check for string match with snmpCommunityName
		if snmpCommunityTransportTag != "" {
			entry = searchTable()
			//transportDomain and transportAddress should match with one of the entries in snmpTargetAddrTable
			//selected by snmpCommunityTransportTag
		}

		if entry == nil (entry_not_found) {
			snmpInBadCommunityNames++
			return "authenticationFailure"
		}

		secParams  = newV1V2SecurityParameters(commString)

		scopedPDU = new(msg.ScopedPDU)
		scopedPDU.SetContextEngineID(entry.snmpCommunityContextEngineID)
		scopedPDU.SetContextName(snmpCommunityContextName)
		scopedPDU.SnmpPDU = snmpPDU (We should have performed decoding)

		cache = commString
	*/

	return -1, nil, nil, nil
}

//Refer engine.SnmpSecurityModel interface doc.
func (v1v2sec *SnmpSecurityModelV1V2) ProcessIncomingResponse(cache engine.SecurityStateReference, //This cache will not be filled, as this is incoming response.
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32, engine.SecurityParameters, *msg.ScopedPDU, *engine.StatusInformation) {

	return -1, nil, nil, nil
}

//Refer engine.SnmpSecurityModel interface doc.
func (v1v2sec *SnmpSecurityModelV1V2) GenerateRequestMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU, //Plain text, unencrypted
	protocolOptions transport.ProtocolOptions,
) (engine.SecurityParameters, []byte, transport.TransportStateReference, *engine.StatusInformation /*statusInfo*/) {

	//We are doing a common processing for both outgoing requests and responses
	return v1v2sec.GenerateResponseMsg(version,
		msgId,
		msgMaxSize,
		msgFlags,
		securityModel,
		securityName,
		securityEngineId,
		scopedPDU,
		nil,
		protocolOptions,
	)

}

//Refer engine.SnmpSecurityModel interface doc.
func (v1v2sec *SnmpSecurityModelV1V2) GenerateResponseMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU,
	cache engine.SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
	protocolOptions transport.ProtocolOptions,
) (engine.SecurityParameters, []byte, transport.TransportStateReference, *engine.StatusInformation /*statusInfo*/) {

	tmStateReference := new(transport.TmStateReference)
	/*
		FROM RFC 3584
		securityName = snmpCommunitySecurityName
		contextEngineID = snmpCommunityContextEngineID
		contextName = snmpCommunityContextName

		//SnmpCommunityTable -  SNMP-COMMUNITY-MIB
		SnmpCommunityEntry ::= SEQUENCE {
		    snmpCommunityIndex               SnmpAdminString,
		    snmpCommunityName                OCTET STRING,
		    snmpCommunitySecurityName        SnmpAdminString,
		    snmpCommunityContextEngineID     SnmpEngineID,
		    snmpCommunityContextName         SnmpAdminString,
		    snmpCommunityTransportTag        SnmpTagValue,
		    snmpCommunityStorageType         StorageType,
		    snmpCommunityStatus              RowStatus
		}
	*/

	//We can perform encoding here, which would be required by MPM
	msgPros := v1v2sec.localEngine.MsgProcessingSubSystem()
	msg := msg.NewSnmpMessage()
	msg.SetVersion(version)
	msg.SnmpPDU = scopedPDU.SnmpPDU

	secParams := newSnmpV1V2SecurityParameters(securityName) //We are using security name as community string as per RFC
	if cache != nil {
		//secParams := newSnmpV1V2SecurityParameters(cache.securityName)
	}
	msg.SetCommunity(secParams.community)

	encMsg, err := msgPros.EncodeMessage(version, msg)
	if err != nil {
		statInfo := engine.NewStatusInformation(err.Error())
		log.Fatal("Encoding error: %s. MSG:: ReqID: %d; Command: %s; Remote Entity: %s;", err.Error(), scopedPDU.RequestID(), getMSGCommand(scopedPDU.Command()), protocolOptions.SessionID())
		return nil, nil, tmStateReference, statInfo
	}

	return secParams, encMsg, tmStateReference, nil
}

//V1/V2C doesn't store the details of Peer Engine.
func (v1v2sec *SnmpSecurityModelV1V2) PeerEngineLCD() engine.SnmpPeerEngineLCD {
	return nil //We don't have any details about peer engines.
}

//We are not dealing with any security cache in v1/v2c
func (v1v2sec *SnmpSecurityModelV1V2) CreateNewSecurityCache() engine.SecurityStateReference {
	return nil
}

//We are not dealing with any security cache in v1/v2c
func (v1v2sec *SnmpSecurityModelV1V2) ReleaseSecurityCache(cache engine.SecurityStateReference) {
	//Do nothing
}

//getMSGCommand returns the string form of the SnmpMessage's command.
func getMSGCommand(cmd consts.Command) string {

	var cmdStr string = ""

	switch cmd {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	case consts.ReportMessage:
		cmdStr = "Report"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}
