/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package security provides various types for processing the incoming and outgoing SNMP messages for security. It is one of the parts of the Engine architecture.

  It defines different security models for different versions of SNMP, which provides the following services as per definition of RFC3414,

  * Process incoming SNMP messages and performs necessary security checks as per RFC3414 - Section 3.2
  * Prepare an outgoing message as per RFC3414 - Section 3.1
  * Prepare a response message as per RFC3414 - Section 3.1

SnmpSecurityModel interface defines all these services, which in turn is implemented by different security models such as SnmpSecurityModelV1V2,
User-based Security Model (USM) required for three versions SNMP v1, v2c, v3 respectively.

SnmpMsgProcessingModel calls the respective security model to perform security operations on the SNMP messages based on the security model.
This layer performs all the authentication and privacy related operations.
*/
package security

import (
	"webnms/snmp/consts"
)

//Constants used in Security layer of the API.
const (
	//Security Models
	V1sec  int32 = 1 //SNMPv1 Security model
	V2Csec int32 = 2 //SNMPv2c Security model
	USMID  int32 = 3 //User-based Security model

	//Security Levels
	NoAuthNoPriv = consts.NoAuthNoPriv
	AuthNoPriv   = consts.AuthNoPriv
	AuthPriv     = consts.AuthPriv

	//Mask to extract the required values from msgFlags
	NoAuthNoPrivMask byte = 0
	AuthMask         byte = 1
	PrivMask         byte = 2
	AuthPrivMask     byte = 3
	ReportableFlag   byte = 4
)
