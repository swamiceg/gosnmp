/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"bytes"
	"crypto/aes" //For AES-128, AES-192, AES-256
	"crypto/cipher"
	"crypto/des"
	"crypto/md5"
	"crypto/sha1"
	"encoding/binary"
	"errors"
	"fmt"
	"hash"
	"math"
	"math/rand"

	"webnms/snmp/engine"
)

//USMAlgoManager will store all the algorithms used in USM.
//SnmpAPI will add all the predefined algorithms like MD5/SHA/DES etc. in this mgr.
//User implementing their own algorithm must register their algorithm with this manager.
//
//  Note: AlgorithmManager instance can be obtained from USMUserLCD.
type USMAlgoManager struct {
	algos map[string]*algo //Algorithm OID is the key
}
type algo struct { //Algorithm Name + Algorithm Instance - Internal use only
	algoName     string
	algoInstance USMAlgorithm
}

//Returns new instance of algorithm manager.
//
//User need not create instance of algorithm manager. It's instance can be obtained from USMUserLCD.
func NewUSMAlgorithmMgr() *USMAlgoManager {
	return &USMAlgoManager{
		algos: make(map[string]*algo),
	}
}

//RegisterAlgorithm registers an USMAlgorithm with this algorithm manager am.
//algorithm should be an implementation of USMAlgorithm interface.
//
//Algorithm's OID value will be used to uniquely identify the algorithm. Hence it should be unique.
//Overrides the algorithm if one exist already.
func (am *USMAlgoManager) RegisterAlgorithm(algorithm USMAlgorithm) {
	if algorithm == nil {
		return
	}
	am.algos[algorithm.OID()] = &algo{ //Overrides the algorithm if one exist already
		algorithm.Algorithm(),
		algorithm,
	}
}

//UnRegisterAlgoirthm removes the usm algorithm from the algorithm manager based on the algorithm string passed.
//
//algorithm can be a valid algorithm name (or) oid string.
func (am *USMAlgoManager) UnRegisterAlgorithm(algorithm string) USMAlgorithm { //Can be algorithm name (or) oid

	if algo, ok := am.algos[algorithm]; ok { //Look by OID
		delete(am.algos, algorithm)
		return algo.algoInstance
	} else { //Look by Name
		for oid, algo := range am.algos {
			if algo.algoName == algorithm { //Matching algorithm name
				delete(am.algos, oid)
				return algo.algoInstance
			}
		}
	}

	return nil
}

//Algorithm returns the usm algorithm instance matching the OID/Algorithm Name passed.
//Returns nil if no algorithm found.
func (am *USMAlgoManager) Algorithm(algorithm string) USMAlgorithm {
	if algo, ok := am.algos[algorithm]; ok { //Look by OID
		return algo.algoInstance
	} else { //Look by Name
		for _, algo := range am.algos {
			if algo.algoName == algorithm {
				return algo.algoInstance
			}
		}
	}

	return nil
}

//Algorithms returns a slice of usm algorithms registered with this algorithm manager am.
func (am *USMAlgoManager) Algorithms() []USMAlgorithm {
	var algorithms []USMAlgorithm
	for _, algo := range am.algos {
		algorithms = append(algorithms, algo.algoInstance)
	}
	return algorithms
}

//User based security model algorithm. All the algorithms defined under USM should implement this interface.
//
//User implementing their own algorithm should implement this interface.
//Algorithm is uniquely identified by the name/oid string.
type USMAlgorithm interface {
	//Returns the name of the algorithm.
	Algorithm() string

	//Returns the OID string associated with the algorithm.
	OID() string
}

//User-based security model authentication algorithm. All the authentication algorithms defined under USM should implement this interface.
//User implementing their own authentication algorithm must implement this interface and register themselves with algorithm manager.
//
//usm package has provided default implementation for the following authentication algorithms,
//
//  * MD5 (as defined in RFC3414 - Section 6)
//  * SHA1 (as defined in RFC3414 - Section 7)
//
//Note: This interface type extends (Embeds) USMAlgorithm interface.
type USMAuthAlgorithm interface {
	USMAlgorithm //extends USMAlgorithm

	//SignMsg signs the data passed using the authKey with this auth algorithm and returns the final digest as bytes.
	SignMsg(data []byte, authKey []byte) []byte

	//VerifyMsg verifies the authenticity of the data after computing the digest over the data with the authKey
	//and comparing the same with signature passed.
	VerifyMsg(data []byte, authKey []byte, signature []byte) bool

	//PasswordToKey translates the password string to the authentication secret key (Ku) with this auth algorithm.
	//As per definitions of RFC3414 - A.2.
	PasswordToKey(password string) []byte

	//LocalizeAuthKey localizes the secretKey using the engineId passed with this auth algorithm.
	//Return the localized authentication key (Kul).
	LocalizeAuthKey(secretKey, engineId []byte) []byte

	//LocalizePrivKey localizes the secretKey using the engineId passed with this auth algorithm. Keysize indicates the size of the privacy key.
	//Return the localized privacy key (Kul).
	LocalizePrivKey(secretKey, engineId []byte, keySize int) []byte //return localized secret key (Kul)

	//ExtendPrivKey localizes the privacy secret key using engineId and pwd string provided.
	//
	//If privacy key size is greater than authentication protocol keysize, LocalizePrivKey method would
	//return nil value. This method will perform password to key chaining as per standards mentioned in rfc and return a extended priv key.
	ExtendPrivKey(pwd string, engineId []byte, keySize int) []byte

	//CalculateAuthDelta calculates the auth delta parameter based on the oldKey, newKey, random data passed, which can be passed to the
	//remote entity to configure the new key.
	//Implemented as per definitions of RFC 3414 - Section 5 KeyChange Definitions.
	//
	//Returns auth delta parameter.
	//
	//Note: Delta value concatenated with the random value is passed to remote entity to perform key change.
	CalculateAuthDelta(oldKey, newKey, random []byte) []byte

	//CalculatePrivDelta calculates the priv delta parameter based on the oldKey, newKey, random data passed, which can be passed to the
	//remote entity to configure the new key. deltaSize represents the DeltaSize of the priv algorithm,
	//Implemented as per definitions of RFC 3414 - Section 5 KeyChange Definitions.
	//
	//Returns priv delta parameter.
	//
	////Note: Delta value concatenated with the random value is passed to remote entity to perform key change.
	CalculatePrivDelta(oldKey, newKey, random []byte, deltaSize int) []byte

	//CalculateNewAuthKey calculates the new key based on the oldKey value and randomDelta parameter received from the remote entity.
	//randomDelta value should be a concatenation of random and Delta value.
	//Implemented as per definitions of RFC 3414 - Section 5 KeyChange Definitions.
	//
	//Returns the new auth key calculated.
	CalculateNewAuthKey(oldKey, randomDelta []byte) []byte

	//CalculateNewPrivKey calculates the new key based on the oldKey value and randomDelta parameter received from the remote entity.
	//deltaSize represents the DeltaSize of the priv algorithm.
	//randomDelta value should be a concatenation of random and Delta value.
	//Implemented as per definitions of RFC 3414 - Section 5 KeyChange Definitions.
	//
	//Returns the new priv key calculated.
	CalculateNewPrivKey(oldKey, randomDelta []byte, deltaSize int) []byte

	//KeySize returns the secret key size for this auth algorithm.
	KeySize() int

	//BlockSize returns the hash's underlying block size.
	BlockSize() int

	//This is needed for password to key conversion. Ex: MD5 - 64, SHA1 - 72.
	PasswordToKeySize() int

	//DeltaSize returns the delta size int needed for key change.
	DeltaSize() int
}

//User-based security model privacy algorithm. All the privacy algorithms defined under USM should implement this interface.
//User implementing their own privacy algorithm must implement this interface and register themselves with algorithm manager.
//
//usm package has provided default implementation for the following privacy algorithms,
//
//  * DES (as defined in RFC3414 - Section 8)
//  * 3-DES
//  * AES-128 (as defined in RFC3826)
//  * AES-192
//  * AES-256
//Note: This interface type extends (Embeds) USMAlgorithm interface.
type USMPrivAlgorithm interface {
	USMAlgorithm //extends USMAlgorithm

	//Encrypts the data using the key passed with this priv algorithm.
	//Security parameters would be used to generate IV based on the privacy algorithm.
	//Returns the encrypted cipher along with the PrivParameters (SALT). Returns error in case of failure in encryption.
	Encrypt(data, key []byte, secParams engine.SecurityParameters) (cipher []byte, privParams []byte, err error)

	//Decrypts the encrypted cipher text encData using the key and privParams provided.
	//secParams is used for SALT calculation.
	//
	//Returns decrypted data, else returns error in case of failure in decryption.
	Decrypt(encData, key, privParams []byte, secParams engine.SecurityParameters) (decData []byte, err error)

	//KeySize returns the secret key size for this priv algorithm.
	KeySize() int

	//BlockSize returns the priv algorithm's block size.
	BlockSize() int

	//DeltaSize returns the delta size int needed for key change.
	DeltaSize() int
}

//This can be used as an anonymous type in all the algorithms to implement USMAlgorithm interface.
//For internal use only.
type usmAlgoImpl struct {
	algorithm string
	oid       string
}

func newUSMAlgo(algo, oidString string) *usmAlgoImpl {
	return &usmAlgoImpl{
		algorithm: algo,
		oid:       oidString,
	}
}

//Refer USMAlgorithm interface doc.
func (algo *usmAlgoImpl) Algorithm() string {
	return algo.algorithm
}

//Refer USMAlgorithm interface doc.
func (algo *usmAlgoImpl) OID() string {
	return algo.oid
}

/* USM Authentication Algorithms Implementation - MD5/SHA1 */

/*
	MD5-96: usmHMACMD5AuthProtocol (1.3.6.1.6.3.10.1.1.2) //RFC 6151 - HMAC-MD5-96
	--------------------------------------------------
	* 128-bit message digest is calculated over portion of the message. First 96-bits (12 Octets) is attached to the message.
	* Authentication key size - 16 Octets (128 Bits)
	* BlockSize - 64 Octets
	* PasswordToKey Size - 64 Octets
*/

//MD5-96: usmHMACMD5AuthProtocol implements USMAuthAlgorithm.
type MD5 struct {
	*usmAlgoImpl //Implementing USMAlgorithm
	*hmac        //Implementing USMAuthAlgorithm
	//engine SnmpEngine //Local SNMP Engine
}

//NewMD5 returns new instance of MD5 auth protocol.
//
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
func NewMD5() *MD5 {
	md := new(MD5)
	md.usmAlgoImpl = newUSMAlgo("MD5", usmHMACMD5AuthProtocol)
	md.hmac = newHMAC(md5.New(), md)

	return md
}

//Refer USMAuthAlgorithm interface doc.
func (md *MD5) PasswordToKeySize() int {
	return 64
}

/*
	SHA-96: usmHMACSHAAuthProtocol (1.3.6.1.6.3.10.1.1.3) //RFC2104
	---------------------------------------------------------------
	* Digest Size: 12 Octets (96 Bits)
	* Key Size: 20 Octets (160 Bits)
	* BlockSize - 64 Octets
	* PasswordToKey Size: 72 Octets
*/

//SHA-96: usmHMACSHAAuthProtocol implements USMAuthAlgorithm.
type SHA1 struct {
	*usmAlgoImpl //Implementing USMAlgorithm
	*hmac        //Implementing USMAuthAlgorithm
	//engine SnmpEngine
}

//NewSHA returns new instance of SHA1 auth protocol.
//
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
func NewSHA1() *SHA1 {
	sha := new(SHA1)
	sha.usmAlgoImpl = newUSMAlgo("SHA1", usmHMACSHAAuthProtocol)
	sha.hmac = newHMAC(sha1.New(), sha)

	return sha
}

//Refer USMAuthAlgorithm interface doc.
func (sha *SHA1) PasswordToKeySize() int {
	return 72
}

//Let's do a common implementation for both MD5 and SHA algos
//Every authentication algorithm can have anonymous type of this, which provides implementation of USMAuthAlgorithm.
//PasswordToKeySize method should be implemented on their own.
type hmac struct {
	hash     hash.Hash
	authAlgo USMAuthAlgorithm
}

//Generate new HMAC instance by passing hash instance (Ex: md.New(), sha1.New()) and the algorithm instance
func newHMAC(hasher hash.Hash, algo USMAuthAlgorithm) *hmac {
	return &hmac{
		hash:     hasher,
		authAlgo: algo,
	}
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) KeySize() int {
	return hm.hash.Size()
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) BlockSize() int {
	return hm.hash.BlockSize()
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) DeltaSize() int { //Delta Size is same as key size for MD5/SHA
	return hm.KeySize()
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) SignMsg(data, key []byte) []byte { //RFC3414 - Section 6.3.1/7.3.1
	//AuthenticateOutgoingMsg
	if len(key) != hm.KeySize() { //16 Octets for MD5 and 20 Octets for SHA1
		return nil //Error: Cannot authenticate
	}

	blockSize := hm.BlockSize()
	keyLen := len(key)

	//Generate two keys k1, k2 from key:
	extendedAuthKey := append(key, bytes.Repeat([]byte{0}, (blockSize-keyLen))...)
	for i, k := range key {
		extendedAuthKey[i] = k
	}

	ipad := make([]byte, blockSize) //64 Octets for MD5, SHA1
	opad := make([]byte, blockSize)
	for i := 0; i < blockSize; i++ {
		ipad[i] = byte(0x36) //Replicate 0x36 64 times
		opad[i] = byte(0x5C) //Replicate 0x5c 64 times
	}
	var k1, k2 []byte
	for i := 0; i < len(ipad); i++ {
		k1 = append(k1, extendedAuthKey[i]^ipad[i])
		k2 = append(k2, extendedAuthKey[i]^opad[i])
	}
	//We have now generated two keys - k1, k2
	//We should generate hash using the keys and data
	hm.hash.Reset()
	hm.hash.Write(append(k1, data...))
	dig := hm.hash.Sum(nil) //Get the digest
	hm.hash.Reset()
	hm.hash.Write(append(k2, dig...))
	finalDigest := hm.hash.Sum(nil) //Get the digest

	return finalDigest[:12] //First 12 octets is the final digest
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) VerifyMsg(data, key, recHMAC []byte) bool { //AuthenticateIncomingMsg
	//First we should generate the digest for the message and check the same with received HMAC
	calcMAC := hm.SignMsg(data, key)

	return bytes.Equal(calcMAC, recHMAC)
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) PasswordToKey(pwd string) []byte { //Refer RFC3414 - A.2.1
	/*This medhod panics if the password string is empty. It can be handled if needed.*/

	//We should implement this conversion
	passwordBuf := make([]byte, hm.authAlgo.PasswordToKeySize()) //Password_to_key size for MD5 is 64 Bytes, 72 Bytes for SHA
	password := []byte(pwd)
	passwordLen := len(password)
	var count, pwdIndex int = 0, 0

	hm.hash.Reset()
	for count < 1048576 { //until we've done 1 Megabyte
		for i := 0; i < 64; i++ { //Read 64 bytes from password string in a circular fashion
			passwordBuf[i] = password[pwdIndex%passwordLen]
			pwdIndex++
		}
		hm.hash.Write(passwordBuf[:64])
		count += 64 //Done 64 Bytes
	}

	//We can do passwordToKey chaining in order to obtain desired keysize as defined by priv protocol

	return hm.hash.Sum(nil) //Secret key - Ku
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) LocalizeAuthKey(secretKey, engineId []byte) []byte { //Localized secret key - Kul
	//engineId := md.engine.msgAuthoritativeEngineId()
	//engineId, _ := hex.DecodeString("000000000000000000000002")
	hm.hash.Reset()
	hm.hash.Write(secretKey)
	hm.hash.Write(engineId)
	hm.hash.Write(secretKey)

	return hm.hash.Sum(nil)
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) LocalizePrivKey(key, engineId []byte, keySize int) []byte {
	//Let's calculate authKey first
	authKey := hm.LocalizeAuthKey(key, engineId)
	if authKey == nil || len(authKey) < keySize { //If privacy key size is greater than authKey size return nil
		return nil
	}

	//privacy algo's keysize should be less than or equal to auth algo's keysize
	privKey := authKey[:keySize]

	return privKey
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) ExtendPrivKey(pwd string, engineId []byte, keySize int) []byte {
	return passwordToKeyChaining(hm.authAlgo, pwd, engineId, keySize)
}

//Key Change::
//RFC 3414 - Section 5 - KeyChange definitions

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) CalculateAuthDelta(oldKey, newKey, random []byte) []byte {
	return hm.calculateDelta(oldKey, newKey, random, hm.authAlgo.DeltaSize())
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) CalculatePrivDelta(oldKey, newKey, random []byte, deltaSize int) []byte {
	return hm.calculateDelta(oldKey, newKey, random, deltaSize)
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) CalculateNewAuthKey(oldKey, randomDelta []byte) []byte {
	return hm.calculateNewKey(oldKey, randomDelta, hm.authAlgo.DeltaSize())
}

//Refer USMAuthAlgorithm interface doc.
func (hm *hmac) CalculateNewPrivKey(oldKey, randomDelta []byte, deltaSize int) []byte {
	return hm.calculateNewKey(oldKey, randomDelta, deltaSize)
}

//Calculate Delta value need for an entity, which will be passed to remote entity in order to change the key.
func (hm *hmac) calculateDelta(oldKey, newKey, random []byte, deltaSize int) []byte { //We should calculate the delta when we need to change the key in remote entity
	var delta []byte = make([]byte, deltaSize)

	i := 0
	//16 Octets for MD5, 20 Octets for SHA1
	tempKey := make([]byte, len(oldKey))
	copy(tempKey, oldKey)

	digestSize := hm.hash.Size()
	//In case new key size is greater than digest size. This cannot be the case for MD5/SHA as they have fixed keylength and deltasize.
	if len(newKey) > digestSize { //Delta Size
		iterations := (deltaSize - 1) / digestSize //No. of iterations iterations to be performed

		for i = 0; i < iterations; i++ {
			tempKey = append(tempKey, random...)
			hm.hash.Reset()
			hm.hash.Write(tempKey)
			tempKey = hm.hash.Sum(nil)
			k := 0
			for j := (i * digestSize); j < (i*digestSize)+digestSize; j++ {
				delta[j] = tempKey[k] ^ newKey[j]
				k++
			}
		}
	}

	//Append the random component to oldKey (tempKey) and compute digest
	hm.hash.Reset()
	hm.hash.Write(append(tempKey, random...))
	digest := hm.hash.Sum(nil)

	m := 0
	for k := i * digestSize; k < deltaSize; k++ {
		delta[k] = digest[m] ^ newKey[k]
		m++
	}

	return delta
}

//Entity should pass the random and delta value (concatenated) to the remote entity,
//which then uses to calculate the new key for it.
func (hm *hmac) calculateNewKey(oldKey, randomDelta []byte, deltaSize int) []byte {
	var newKey []byte = make([]byte, len(oldKey))
	var random []byte
	var delta []byte

	i := 0
	tempKey := make([]byte, len(oldKey))
	copy(tempKey, oldKey)

	//Divide the random and delta components received
	random = append(random, randomDelta[:(len(randomDelta)-deltaSize)]...)
	delta = append(delta, randomDelta[(len(randomDelta)-deltaSize):]...)

	digestSize := hm.hash.Size()
	//In case new key size is greater than deltaSize. This cannot be the case for MD5/SHA as they have fixed keylength and deltasize.
	if len(newKey) > digestSize {
		iterations := (deltaSize - 1) / digestSize

		for i = 0; i < iterations; i++ {
			tempKey = append(tempKey, random...)
			hm.hash.Reset()
			hm.hash.Write(tempKey)
			tempKey = hm.hash.Sum(nil)
			k := 0
			for j := i * digestSize; j < (i*digestSize)+digestSize; j++ {
				newKey[j] = tempKey[k] ^ delta[j]
				k++
			}
		}
	}

	hm.hash.Reset()
	hm.hash.Write(append(tempKey, random...))
	digest := hm.hash.Sum(nil)

	m := 0
	for k := i * digestSize; k < deltaSize; k++ {
		newKey[k] = digest[m]
		newKey[k] ^= delta[k]
		m++
	}

	return newKey
}

//Extension to the User-Based Security Model (USM) to Support Triple-DES EDE in "Outside" CBC Mode
//http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00
func passwordToKeyChaining(algo USMAuthAlgorithm, pwd string, engineId []byte, keySize int) []byte {
	var k1, k2 []byte
	var pw string = pwd

	kul := make([]byte, keySize)
	k1 = algo.LocalizeAuthKey(algo.PasswordToKey(pw), engineId)
	length := len(k1)
	for i := 0; i < length; i++ {
		kul[i] = k1[i]
	}

	for length < keySize {
		k2 = algo.LocalizeAuthKey(algo.PasswordToKey(string(kul[:length])), engineId)
		toCopy := math.Min(float64((keySize - length)), float64(algo.KeySize()))
		for i := 0; i < int(toCopy); i++ {
			kul[i+length] = k2[i]
		}
		length += int(toCopy)
		//fmt.Println("key chaining:", k1, k2, kul)
	}
	//fmt.Println("Final Key (priv):: ", hex.EncodeToString(kul))
	return kul
}

/* USM Privacy Algorithms Implementation - DES/3DES/AES-128/AES-192/AES-256 */

/*
	CBC-DES: usmDESPrivProtocol (1.3.6.1.6.3.10.1.2.2)
	-------------------------------------------------
	Key Size: 16 Octets
	DES Key: 56 Bits Key[:8] //First 8 octets is the des key, on which only first 56 bits will be used by DES.
	preIV: Key[8:] //Second 8 Octets of the key is used as pre-IV
	Salt - 8 Octet String - Generated by 32-bit snmpEngineId(4 octets) + 32 bit arbitrary local integer(4 octets)
	IV = (Salt XOR preIV)
*/

//CBC-DES: usmDESPrivProtocol implements USMPrivAlgorithm interface.
//Implemented as per RFC 3414 - Section 8.
type DES struct { //Implement USMPrivAlgorithm
	*usmAlgoImpl
	*desImpl
	//engine SnmpEngine
}

//NewDES returns new instance of DES priv protocol.
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
//
//engine instance is required to obtain the local engine's EB/ET for calculating SALT for encryption.
func NewDES(engine engine.SnmpEngine) *DES {
	des := new(DES)
	des.usmAlgoImpl = newUSMAlgo("DES", usmDESPrivProtocol)
	des.desImpl = newDes(des, engine) //Initialize Engine

	return des
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *DES) BlockSize() int { //8 Octets (64 Bits)
	return des.BlockSize
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *DES) KeySize() int { //16 Octets (But DES key is 8 Octets)
	return 16
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *DES) DeltaSize() int { //16 Octets
	return 16
}

/*
	Triple-DES
	DES Key Size: 24 Octets (168 Bits)
	Key: 24 (DES) + 8 (preIV)
	BlockSize: 8 Octets
*/

//TripleDES implements USMPrivAlgorithm interface.
type TripleDES struct {
	*usmAlgoImpl
	*desImpl
}

//NewTripleDES returns new instance of Triple-DES priv protocol.
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
//engine instance is required to obtain the local engine's EB/ET for calculating SALT for encryption.
func NewTripleDES(engine engine.SnmpEngine) *TripleDES {
	tdes := new(TripleDES)
	tdes.usmAlgoImpl = newUSMAlgo("3DES", usm3DESEDEPrivProtocol)
	tdes.desImpl = newDes(tdes, engine) //Initialize Engine

	return tdes
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *TripleDES) BlockSize() int { //8 Octets
	return des.BlockSize
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *TripleDES) KeySize() int { //32 Octets (But DES key is 24 Octets)
	return 32
}

//Refer USMPrivAlgorithm interface doc.
func (desAlgo *TripleDES) DeltaSize() int { //32 Octets
	return 32
}

//This is a common implementation for both DES/3DES protocol
//Both DES/3DES should have this as anonymous type.
//Cipher Block Chaining Mode (CBC)
type desImpl struct {
	algo   USMPrivAlgorithm
	engine engine.SnmpEngine
}

func newDes(privAlgo USMPrivAlgorithm, engine engine.SnmpEngine) *desImpl {
	return &desImpl{
		algo:   privAlgo,
		engine: engine,
	}
}

//Refer USMPrivAlgorithm interface doc.
func (di *desImpl) Encrypt(data, key []byte, secParams engine.SecurityParameters) ([]byte, []byte, error) {

	var privAlgo USMPrivAlgorithm = di.algo
	if len(key) != privAlgo.KeySize() { // 8+8 (16) Octets for DES, 24+8 (32) Octets for 3DES
		//fmt.Println(key)
		return nil, nil, fmt.Errorf("Encryption Error: Invalid Key Size: %d", len(key)) //KeySize Error
	}
	toEnc := make([]byte, len(data))
	copy(toEnc, data)

	blockSize := privAlgo.BlockSize()
	keySize := privAlgo.KeySize()

	if len(toEnc)%blockSize != 0 { //If data is not multiple of blocks, then perform padding
		//toEnc = zeroPadding(toEnc, blockSize) //Can be changed to pkcs5padding after confirmation
		toEnc = pkcs5Padding(toEnc, blockSize)
	}

	//Let's do a common implementation for DES/3DES first
	desCryptKey := key[:(keySize - 8)] //getCryptKey(PrivateKey)
	preIV := key[(keySize - 8):]       //Last 8 Octets are always used as preIV

	//Salt generation - to prepare unique IV
	salt := make([]byte, 8)                    //8 Octet Salt
	engineBoots := di.engine.SnmpEngineBoots() //4 Octet EngineBoots
	randInt := rand.Uint32()                   //4 Octet random integer

	//Salt: Boots + RandomInt in MSB First Order
	//MSB Byte Order - Hence using BigIndian
	binary.BigEndian.PutUint32(salt[:4], uint32(engineBoots))
	binary.BigEndian.PutUint32(salt[4:8], uint32(randInt))

	if _, ok := privAlgo.(*TripleDES); ok { //Salt should be hashed as per RFC - http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00
		hasher := md5.New()
		hasher.Write(salt)
		hashedSalt := hasher.Sum(nil)
		salt = hashedSalt[:8] //We need just 8 Octets
	}
	//privParameters = salt

	//Generate IV:
	iv := make([]byte, privAlgo.BlockSize()) //8 Octets
	for i := 0; i < len(salt); i++ {
		iv[i] = preIV[i] ^ salt[i]
	}

	var desBlock cipher.Block
	var err error

	//Create new cipher block using the key
	if _, ok := privAlgo.(*DES); ok {
		desBlock, err = des.NewCipher(desCryptKey)
		if err != nil {
			return nil, nil, err
		}
	} else if _, ok = privAlgo.(*TripleDES); ok {
		desBlock, err = des.NewTripleDESCipher(desCryptKey)
		if err != nil {
			return nil, nil, err
		}
	} else {
		return nil, nil, fmt.Errorf("Unsupported Privacy Protocol: %s", privAlgo.Algorithm()) //We are not dealing with other privacy protocols
	}

	cipherText := make([]byte, len(toEnc)) //To store encrypted data

	//Create BlockMode to encrypt data in cipher block chaining mode (CBC)
	mode := cipher.NewCBCEncrypter(desBlock, iv)
	mode.CryptBlocks(cipherText, toEnc)

	return cipherText, salt, nil
}

//Refer USMPrivAlgorithm interface doc.
func (di *desImpl) Decrypt(encData, key, privParams []byte, secParams engine.SecurityParameters) ([]byte, error) {

	privAlgo := di.algo
	if len(key) != privAlgo.KeySize() { //Keysize should be 16 Octets for DES, 32 Octets for 3DES
		return nil, fmt.Errorf("PrivAlgorithm - Invalid Key Size: %d", len(key))
	}
	if len(encData)%privAlgo.BlockSize() != 0 {
		//fmt.Println("Cipher text should be a multiple of block size ", privAlgo.BlockSize())
		return nil, fmt.Errorf("Decryption Error: Cipher text should be a multiple of block size: %d", privAlgo.BlockSize())
	}

	keySize := privAlgo.KeySize()
	desCryptKey := key[:(keySize - 8)] //getCryptKey(PrivateKey)
	preIV := key[(keySize - 8):]       //Last 8 Octets are always used as preIV

	salt := privParams

	//Generate IV: We need salt to calculate it.
	iv := make([]byte, privAlgo.BlockSize()) // 8 Octets
	for i := 0; i < len(salt); i++ {
		iv[i] = preIV[i] ^ salt[i]
	}

	var desBlock cipher.Block
	var err error

	//Create new cipher block using the key
	if _, ok := privAlgo.(*DES); ok {
		desBlock, err = des.NewCipher(desCryptKey)
		if err != nil {
			return nil, err
		}
	} else if _, ok = privAlgo.(*TripleDES); ok {
		desBlock, err = des.NewTripleDESCipher(desCryptKey)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("Unsupported Privacy Protocol: %s", privAlgo.Algorithm()) //We are not dealing with other privacy protocols
	}

	decData := make([]byte, len(encData)) //To store the decrypted data

	//Create BlockMode to decrypt the data in cipher block chaining mode (CBC)
	mode := cipher.NewCBCDecrypter(desBlock, iv)
	mode.CryptBlocks(decData, encData)

	//Unpadding - Remove the padded bytes.
	//decData = unPad(decData, privAlgo.BlockSize()) //Can be changed as pkcs5unpadding after confirmation
	header, body := calculatePacketLength(decData)
	totalLen := int(header) + int(body)
	if totalLen > len(decData) { //Additional check done. This should be validated in message processing model.
		return nil, fmt.Errorf("Decryption error: Received bytes is not a valid asn1 construct.")
	}
	decData = decData[:totalLen]

	return decData, nil
}

//We should generate des key by discard LSB in each 8 Octets and return 56 bits
//Note: But Java's DESKeySpec didn't implement it in a such a way. It does a simple arraycopy of 8 bytes.
//We will also do it in the same way as of now.
//We will have this method so that in future if we need, we can change it as required
func getDesKey(key []byte) []byte {
	//Perform some operation and return des key (56 Bits)
	deskey := make([]byte, 8) //only 56 bits - LSB should be discarded
	for i := 0; i < 8; i++ {
		deskey[i] = key[i]
	}

	return deskey
}

/*
	AES-128 - usmAesCfb128PrivProtocol
	KeySize, BlockSize - 128 bits (16 Octets)
*/

//AES128 implements USMPrivAlgorithm interface. It is implemented as per RFC 3826.
type AES128 struct {
	*AES // This is a common implementation for all the AES protocols
}

//NewAES128 returns new instance of AES-128 protocol.
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
//engine instance is required to obtain the local engine's EB/ET for calculating SALT for encryption.
func NewAES128(engine engine.SnmpEngine) *AES128 {
	aes := new(AES128)
	aes.AES = newAES("AES-128", usmAesCfb128Protocol, aes, engine)

	return aes
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES128) BlockSize() int {
	return aes.BlockSize //16 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES128) KeySize() int {
	return 16 //Mustbe at least 16 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES128) DeltaSize() int {
	return 16
}

/*
	AES-192 - usmAesCfb192PrivProtocol
	KeySize - 192 Bits (24 Octets), BlockSize - 128 bits (16 Octets)
*/

//AES192 implements USMPrivAlgorithm interface. It is implemented as per RFC 3826.
type AES192 struct {
	*AES // This is a common implementation for all the AES protocols
}

//NewAES192 returns new instance of AES-192 protocol.
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
//engine instance is required to obtain the local engine's EB/ET for calculating SALT for encryption.
func NewAES192(engine engine.SnmpEngine) *AES192 {
	aes := new(AES192)
	aes.AES = newAES("AES-192", usmAesCfb192Protocol, aes, engine)

	return aes
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES192) BlockSize() int {
	return aes.BlockSize //16 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES192) KeySize() int {
	return 24 //Mustbe at least 24 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES192) DeltaSize() int {
	return 24
}

/*
	AES-256 - usmAesCfb256PrivProtocol
	KeySize - 256 Bits (32 Octets), BlockSize - 128 bits (16 Octets)
*/

//AES256 implements USMPrivAlgorithm interface. It is implemented as per RFC 3826.
type AES256 struct {
	*AES // This is a common implementation for all the AES protocols
}

//NewAES256 returns new instance of AES-256 protocol.
//API user need not instantiate this type. SnmpAPI will create and register this instance with algorithm manager.
//engine instance is required to obtain the local engine's EB/ET for calculating SALT for encryption.
func NewAES256(engine engine.SnmpEngine) *AES256 {
	aes := new(AES256)
	aes.AES = newAES("AES-256", usmAesCfb256Protocol, aes, engine)

	return aes
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES256) BlockSize() int {
	return aes.BlockSize //16 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES256) KeySize() int {
	return 32 //Mustbe at least 32 Octets
}

//Refer USMPrivAlgorithm interface doc.
func (aesAlgo *AES256) DeltaSize() int {
	return 32
}

//This is a common implementation for all the AES protocols in Cipher Feedback Mode (CFB).
type AES struct { //Implement USMPrivAlgorithm - RFC3826
	*usmAlgoImpl
	engine       engine.SnmpEngine
	aesAlgorithm USMPrivAlgorithm //We need aes algorithm instance to calculate keysize, blocksize
}

func newAES(algoName, algoOID string,
	aesAlgo USMPrivAlgorithm,
	engine engine.SnmpEngine) *AES {

	return &AES{
		usmAlgoImpl:  newUSMAlgo(algoName, algoOID),
		engine:       engine,
		aesAlgorithm: aesAlgo,
	}
}

//Refer USMPrivAlgorithm interface doc.
func (ae *AES) Encrypt(data, key []byte, secParams engine.SecurityParameters) ([]byte, []byte, error) {
	if len(key) != ae.aesAlgorithm.KeySize() { //KeySize should be at least 16/24/32 Octets (128 bits)
		//fmt.Println("KeySizeError: Expected ks:: ", ae.aesAlgorithm.KeySize())
		return nil, nil, fmt.Errorf("PrivAlgorithm - Invalid Key Size: %d", len(key)) //Cannot encrypt
	}

	toEnc := make([]byte, len(data))
	copy(toEnc, data)
	//Stream block cipher mode doesnot require padding
	/*if len(toEnc) % ae.aesAlgorithm.BlockSize() != 0 {
		toPad := aes.BlockSize - (len(toEnc) % aes.BlockSize)
		toEnc = append(toEnc, bytes.Repeat([]byte{0}, toPad)...)
	}*/

	cipherText := make([]byte, len(toEnc))
	aesCryptKey := make([]byte, len(key)) //16 Octets key size for AES-128, 24 Octets for AES-192, 32 Octets for AES-256
	copy(aesCryptKey, key)

	//Generate IV of 128 Bits (16 Octets)
	iv := make([]byte, ae.aesAlgorithm.BlockSize())
	//4 Octet EB + 4 Octet ET + 8 Octet Arbitrary Integer
	eb := secParams.(*USMSecurityParameters).authoritativeEngineBoots() //Just make sure the EB/ET values are used correctly
	et := secParams.(*USMSecurityParameters).authoritativeEngineTime()
	salt := rand.Int63() //int64 - 2^63 non-negative integer - random integer

	//Using BigIndian to construct IV.
	//Most Significant Byte first order.
	binary.BigEndian.PutUint32(iv[:4], uint32(eb))
	binary.BigEndian.PutUint32(iv[4:8], uint32(et))
	binary.BigEndian.PutUint64(iv[8:16], uint64(salt))

	aesBlock, err := aes.NewCipher(aesCryptKey)
	if err != nil {
		return nil, nil, err //Encryption Error: err
	}

	//Encrypt by Cipher Feedback Mode - Stream
	stream := cipher.NewCFBEncrypter(aesBlock, iv)
	stream.XORKeyStream(cipherText, toEnc)

	return cipherText, iv[8:16], nil //64 Bit random integer is the SALT - PrivParams
}

//Refer USMPrivAlgorithm interface doc.
func (ae *AES) Decrypt(encData, key, privParams []byte, secParams engine.SecurityParameters) ([]byte, error) {
	if len(privParams) != 8 {
		return nil, errors.New("Decryption Error: SALT should be of size 8 Octets")
	}
	if len(key) != ae.aesAlgorithm.KeySize() { //Should be 16 Octets for AES-128, 24 Octets for AES-192, 32 Octets for AES-256
		return nil, fmt.Errorf("PrivAlgorithm - Invalid Key Size: %d", len(key))
	}

	//Padding not required for AES - Stream cipher block mode doesnot require padding
	/*if len(encData) % ae.aesAlgorithm.BlockSize() != 0 {
		fmt.Println("Cipher text should be a multiple of block size ", ae.aesAlgorithm.BlockSize())
		return nil
	}*/

	aesCryptKey := key //AES-128 privacy key is of 16 Octets, AES-192 privacy key is of 24 Octets, AES-256 privacy key is of 32 Octets
	salt := privParams //64 Bit randon integer for AES

	//Generate IV: We need salt to calculate it.
	iv := make([]byte, ae.aesAlgorithm.BlockSize())
	eb := secParams.(*USMSecurityParameters).authoritativeEngineBoots() //Just make sure the EB/ET values are used correctly
	et := secParams.(*USMSecurityParameters).authoritativeEngineTime()
	//Using BigIndian to construct IV.
	//Most Significant Byte first order.
	binary.BigEndian.PutUint32(iv[:4], uint32(eb))
	binary.BigEndian.PutUint32(iv[4:8], uint32(et))
	for i := 8; i < 16; i++ {
		iv[i] = salt[i-8]
	}

	//Create new cipher block using the key
	aesBlock, err := aes.NewCipher(aesCryptKey)
	if err != nil {
		return nil, err
	}
	decData := make([]byte, len(encData)) //To store the decrypted data

	//Create BlockMode to decrypt the data in cipher block chaining mode (CBC)
	stream := cipher.NewCFBDecrypter(aesBlock, iv)
	stream.XORKeyStream(decData, encData)

	//Remove the trailing padded zeroes - Stream cipher block mode is not padded.
	/*decData = bytes.TrimFunc(decData, func(r rune) bool {
			return r == rune(0)
	})*/

	return decData, nil
}

//This function can be used by CBC ciphers to unpad the decrypted data
func unPad(data []byte, blockSize int) []byte {
	var unpaddedData []byte
	if data[len(data)-1] == byte(0) {
		unpaddedData = zeroUnpadding(data)
		//unpaddedData = data[:(len(data)) - 5]
	} else {
		unpaddedData = pkcs5Unpadding(data, blockSize)
	}
	//Let's add other unpadding techniques like pkcs7 later

	return unpaddedData
}

//Padding functions
func zeroPadding(data []byte, blockSize int) []byte {
	toPad := blockSize - (len(data) % blockSize)
	data = append(data, bytes.Repeat([]byte{0}, toPad)...)
	return data
}

func zeroUnpadding(data []byte) []byte {
	return bytes.TrimFunc(data,
		func(r rune) bool {
			return r == rune(0)
		})
}

func pkcs5Padding(data []byte, blockSize int) []byte {
	toPad := blockSize - (len(data) % blockSize)
	data = append(data, bytes.Repeat([]byte{byte(toPad)}, toPad)...)
	return data
}

func pkcs5Unpadding(data []byte, blockSize int) []byte {
	totalLen := len(data)
	padLen := int(data[totalLen-1])         //Last byte representing the length
	if padLen <= 0 || padLen >= blockSize { //Padded length should be between 1 and BlockSize-1
		return data
	} else if !bytes.Equal(data[(totalLen-padLen):], //Let's check for the padding bytes - Additional check
		bytes.Repeat([]byte{byte(padLen)}, padLen)) { //This should be the original padded bytes
		return data
	}
	return data[:(totalLen - padLen)]
}

//Just an example for providing common implementation for all the privacy algorithms
//Since this looks cramped, not using this as of now. Providing separate implementation for each priv algorithms.
type cipherImpl struct {
	privAlgo USMPrivAlgorithm
	engine   engine.SnmpEngine
}

func newCipher(engine engine.SnmpEngine, algo USMPrivAlgorithm) *cipherImpl {
	return &cipherImpl{
		privAlgo: algo,
		engine:   engine,
	}
}

func (cip *cipherImpl) Encrypt(data, key []byte) ([]byte, []byte) {

	if len(key) != cip.privAlgo.KeySize() {
		//fmt.Println(key)
		return nil, nil //KeySize Error
	}
	toEnc := data
	if len(toEnc)%cip.privAlgo.BlockSize() != 0 { //If data is not multiple of blocks, then perform padding
		//Perform padding
		toPad := (cip.privAlgo.BlockSize()) - (len(toEnc) % cip.privAlgo.BlockSize())
		toEnc = append(toEnc, bytes.Repeat([]byte{0}, toPad)...)
	}
	cipherText := make([]byte, len(toEnc)) //To store encrypted data

	var block cipher.Block

	var cryptKey []byte = nil
	var preIV []byte = nil
	var salt []byte = make([]byte, 8)
	var iv []byte = make([]byte, cip.privAlgo.BlockSize())
	var err error
	switch cip.privAlgo.(type) {
	case *DES:
		cryptKey = getDesKey(key)
		preIV = key[8:]
		var randInt uint32
		randInt = uint32(rand.Int31()) //4 Octet random integer
		//engineBoots := desAlgo.engine.msgAuthoritativeEngineBoots() //4 Octet EngineBoots
		engineBoots := cip.engine.SnmpEngineBoots()
		//Salt: Boots + RandomInt in MSB First Order
		//MSB Byte Order - Hence using BigIndian
		binary.BigEndian.PutUint32(salt[:4], uint32(engineBoots))
		binary.BigEndian.PutUint32(salt[4:8], randInt)
		//IV generation
		for i := 0; i < len(salt); i++ {
			iv[i] = preIV[i] ^ salt[i]
		}
		block, err = des.NewCipher(cryptKey)
		if err != nil {
			return nil, nil
		}
		//case *TripleDES:
		cryptKey = key[:24]
		preIV = key[24:]
		randInt = uint32(rand.Int31()) //4 Octet random integer
		//engineBoots := desAlgo.engine.msgAuthoritativeEngineBoots() //4 Octet EngineBoots
		engineBoots = cip.engine.SnmpEngineBoots()
		//Salt: Boots + RandomInt in MSB First Order
		//MSB Byte Order - Hence using BigIndian
		binary.BigEndian.PutUint32(salt[:4], uint32(engineBoots))
		binary.BigEndian.PutUint32(salt[4:8], randInt)

		//IV generation
		for i := 0; i < len(salt); i++ {
			iv[i] = preIV[i] ^ salt[i]
		}
		block, err = des.NewTripleDESCipher(cryptKey)
		if err != nil {
			return nil, nil
		}
	case *AES128, *AES192, *AES256:
		cryptKey = key
		//4 Octet EB + 4 Octet ET + 8 Octet Arbitrary Integer
		eb := cip.engine.SnmpEngineBoots()
		et := cip.engine.SnmpEngineTime()
		saltInt := rand.Int63() //int64 - 2^63 non-negative integer - random integer

		//Using BigIndian to construct IV.
		//Most Significant Byte first order.
		binary.BigEndian.PutUint32(iv[:4], uint32(eb))
		binary.BigEndian.PutUint32(iv[4:8], uint32(et))
		binary.BigEndian.PutUint64(iv[8:16], uint64(saltInt))

		salt = iv[8:16]
		block, err = aes.NewCipher(cryptKey)
		if err != nil {
			return nil, nil
		}
	default:
		return nil, nil //Unsupported PrivProtocol
	}

	//Create BlockMode to encrypt data in cipher block chaining mode (CBC)
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText, toEnc)

	return cipherText, salt
}

func (cip *cipherImpl) Decrypt(encData, key, privParams []byte) []byte {

	if len(key) != cip.privAlgo.KeySize() {
		return nil
	}
	if len(encData)%cip.privAlgo.BlockSize() != 0 {
		fmt.Println("Cipher text should be a multiple of block size ", cip.privAlgo.BlockSize())
		return nil
	}

	salt := privParams
	var block cipher.Block

	var cryptKey []byte = nil
	var preIV []byte = nil
	var iv []byte = make([]byte, cip.privAlgo.BlockSize())
	var err error
	switch cip.privAlgo.(type) {
	case *DES:
		cryptKey = getDesKey(key)
		preIV = key[8:]

		//IV generation
		for i := 0; i < len(salt); i++ {
			iv[i] = preIV[i] ^ salt[i]
		}
		block, err = des.NewCipher(cryptKey)
		if err != nil {
			return nil
		}
		//case *TripleDES:
		cryptKey = key[:24]
		preIV = key[24:]
		//IV generation
		for i := 0; i < len(salt); i++ {
			iv[i] = preIV[i] ^ salt[i]
		}
		block, err = des.NewTripleDESCipher(cryptKey)
		if err != nil {
			return nil
		}
	case *AES128, *AES192, *AES256:
		cryptKey = key
		//4 Octet EB + 4 Octet ET + 8 Octet Arbitrary Integer
		eb := cip.engine.SnmpEngineBoots()
		et := cip.engine.SnmpEngineTime()
		//Using BigIndian to construct IV.
		//Most Significant Byte first order.
		binary.BigEndian.PutUint32(iv[:4], uint32(eb))
		binary.BigEndian.PutUint32(iv[4:8], uint32(et))
		for i := 8; i < cip.privAlgo.BlockSize(); i++ {
			iv[i] = salt[i-8]
		}

		block, err = aes.NewCipher(cryptKey)
		if err != nil {
			return nil
		}
	default:
		return nil //Unsupported PrivProtocol
	}

	decData := make([]byte, len(encData)) //To store the decrypted data

	//Create BlockMode to decrypt the data in cipher block chaining mode (CBC)
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(decData, encData)

	//Remove the trailing padded zeroes.
	decData = bytes.TrimFunc(decData, func(r rune) bool {
		return r == rune(0)
	})

	return decData
}

//Calculate and return the length of header and body
func calculatePacketLength(data []byte) (body, header uint64) {
	header = 1 //Sequence byte
	length := data[1]
	if length > 0x80 { //Length is of more than one bytes
		length = length - 0x80 //Number of bytes for calculating length
		body = uvarint(data[2:(2 + length)])
		header += (1 + uint64(length))
	} else { //Length is a single byte
		header += 1
		body = uint64(length)
	}

	return
}

//Calculate the length
func uvarint(buf []byte) (x uint64) {
	for i, b := range buf {
		x = x<<8 + uint64(b)
		if i == 7 {
			return
		}
	}
	return
}
