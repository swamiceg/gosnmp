/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"bytes"
	"encoding/asn1"
	"errors"
	"strconv"
	"strings"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

//USMSecurityModel is an implementation of engine.SnmpSecurityModel interface.
//
//This model is implemented as per definitions of RFC3414 - User-based Security Model (USM) for version 3.
//It provides all the security services required for processing the message such as authentication, privacy and timeliness services.
type USMSecurityModel struct {
	*snmpModelImpl //Extending snmpModelImpl
	localEngine    engine.SnmpEngine
	snmpLCD        *engine.SnmpLCD

	userLCD       USMUserLCD        //UserLCD
	peerEngineLCD *USMPeerEngineLCD //EngineLCD to store peer engines
	usmCounters

	errGen *usmErrorGenerator //Utility to generate USM Security Errors

	authModule *usmAuthModule    //AuthenticationModule - instance to perform authentication
	privModule *usmPrivModule    //PrivacyModule - instance to perform encryption and decryption
	timeModule *timelinessModule //TimelinessModule - instance to perform timeliness check
}

//NewUSMSecurityModel creates and returns a new instance of USMSecurityModel after registering itself with the SecuritySubSystem.
//It's UserLCD which holds the details of user details (as defined in RFC3414 - 2.1) will be registered with the SnmpLCD.
//
//User need not instantiate USMSecurityModel. SnmpAPI would register this model in SecuritySubSystem.
func NewUSMSecurityModel(securitySubSystem engine.SnmpSecuritySubSystem) *USMSecurityModel { //Should pass SnmpLCD to register our USMLCD
	usm := new(USMSecurityModel)
	usm.snmpModelImpl = newSnmpModelImpl(securitySubSystem, "USM", security.USMID)
	usm.localEngine = securitySubSystem.SnmpEngine() //Store the local engine instance
	usm.userLCD = newUSMLCD(usm.localEngine)         //Initialize the user lcd store
	usm.snmpLCD = usm.localEngine.SnmpLCD()          //Get the SnmpLCD instance from SnmpEngine for adding the UserLCD, EngineLCD

	//Utility to generate USM specific security error
	usm.errGen = newUSMErrorGenerator(usm)

	//Register this model with security subsystem
	securitySubSystem.AddModel(usm)

	//In SnmpAPI, we can provide method like GetSecurityLCD(modelNo.(3)), which will do snmpLCD.ModelLCD(securitySubSystem, modelNo.(3)) to return the userLCD.
	usm.snmpLCD.AddModelLCD(securitySubSystem, security.USMID, usm.userLCD) //Register itself to the SnmpLCD.

	//Init Authentication and Privacy Modules
	usm.authModule = newUSMAuthModule(usm.userLCD, usm) //Instantiate AuthModule by passing UserLCD
	usm.timeModule = newUSMTimelinessModule(usm)
	usm.privModule = newUSMPrivModule(usm.userLCD, usm) //Instantiate PrivModule by passing UserLCD

	//Peer Engine LCD creation
	//In SnmpAPI, we can provide method like GetPeerEnginesLCD(USMModel(3)), which will call snmpLCD.ModelLCD(securitySubSystem, 4 (engineTableId)) to return PeerEngines table.
	usm.peerEngineLCD = newUSMPeerEngineLCD(usm.localEngine)
	usm.snmpLCD.AddModelLCD(securitySubSystem, engineTableId, usm.peerEngineLCD) //Add the peer engine LCD.

	//Instantiate algorithm manager for USM and fill it with default algorithms
	algoMgr := NewUSMAlgorithmMgr()
	usm.userLCD.SetAlgorithmManager(algoMgr)

	//Register the Authentication algorithms
	md5 := NewMD5()
	algoMgr.RegisterAlgorithm(md5)

	sha := NewSHA1()
	algoMgr.RegisterAlgorithm(sha)

	//Register the Privacy algorithms
	des := NewDES(usm.localEngine)
	algoMgr.RegisterAlgorithm(des)

	tdes := NewTripleDES(usm.localEngine)
	algoMgr.RegisterAlgorithm(tdes)

	aes128 := NewAES128(usm.localEngine)
	algoMgr.RegisterAlgorithm(aes128)

	aes192 := NewAES192(usm.localEngine)
	algoMgr.RegisterAlgorithm(aes192)

	aes256 := NewAES256(usm.localEngine)
	algoMgr.RegisterAlgorithm(aes256)

	return usm
}

//Implementing SnmpModel interface. Just an additional type providing convenience to store SubSystem and Name.
//For usm internal use only.
type snmpModelImpl struct {
	subSystem engine.SnmpSubSystem
	name      string
	id        int32
}

func newSnmpModelImpl(subsystem engine.SnmpSubSystem, name string, modelID int32) *snmpModelImpl {
	return &snmpModelImpl{
		subSystem: subsystem,
		name:      name,
		id:        modelID,
	}
}

//Returns the SubSystem associated with the Model.
func (sm *snmpModelImpl) SubSystem() engine.SnmpSubSystem {
	return sm.subSystem
}

//Returns the name of the Model.
func (sm *snmpModelImpl) Name() string {
	return sm.name
}

//Returns the Model ID.
func (sm *snmpModelImpl) ID() int32 {
	return sm.id
}

//USM Counters for sending reports
type usmCounters struct { //Counter32 - 1 to 4294967295 (2^32-1)
	unsupportedSecLevelsCounter uint32
	notInTimeWindowsCounter     uint32
	unknownUserNamesCounter     uint32
	unknownEngineIdsCounter     uint32
	wrongDigestsCounter         uint32
	decryptionErrorsCounter     uint32
	//Other counters
	snmpInAsnParseErrorsCounter uint32 //This could have been done in message processing model already
}

//Returns the unsupportedSecLevels Counter.
func (uc *usmCounters) UnsupportedSecLevelsCounter() uint32 {
	return uc.unsupportedSecLevelsCounter
}

//Returns the notInTimeWindows Counter.
func (uc *usmCounters) NotInTimeWindowsCounter() uint32 {
	return uc.notInTimeWindowsCounter
}

//Returns the unknownUserNames Counter.
func (uc *usmCounters) UnknowUserNamesCounter() uint32 {
	return uc.unknownUserNamesCounter
}

//Returns the unknownEngineIds Counter.
func (uc *usmCounters) UnknownEngineIdsCounter() uint32 {
	return uc.unknownEngineIdsCounter
}

//Returns the wrongDigests Counter.
func (uc *usmCounters) WrongDigestsCounter() uint32 {
	return uc.wrongDigestsCounter
}

//Returns the decryptionErrors Counter.
func (uc *usmCounters) DecryptionErrorsCounter() uint32 {
	return uc.decryptionErrorsCounter
}

//Returns the snmpInAsnParseErrors Counter.
func (uc *usmCounters) SnmpInAsnParseErrorsCounter() uint32 {
	return uc.snmpInAsnParseErrorsCounter
}

func (uc *usmCounters) incUnSupportedSecLevelsCounter() uint32 {
	uc.unsupportedSecLevelsCounter++
	return uc.unsupportedSecLevelsCounter
}

func (uc *usmCounters) incNotInTimeWindowsCounter() uint32 {
	uc.notInTimeWindowsCounter++
	return uc.notInTimeWindowsCounter
}

func (uc *usmCounters) incUnknownUserNamesCounter() uint32 {
	uc.unknownUserNamesCounter++
	return uc.unknownUserNamesCounter
}

func (uc *usmCounters) incUnknownEngineIdsCounter() uint32 {
	uc.unknownEngineIdsCounter++
	return uc.unknownEngineIdsCounter
}

func (uc *usmCounters) incWrongDigestsCounter() uint32 {
	uc.wrongDigestsCounter++
	return uc.wrongDigestsCounter
}

func (uc *usmCounters) incDecryptionErrorsCounter() uint32 {
	uc.decryptionErrorsCounter++
	return uc.decryptionErrorsCounter
}

func (uc *usmCounters) incSnmpInAsnParseErrorsCounter() uint32 {
	uc.snmpInAsnParseErrorsCounter++
	return uc.snmpInAsnParseErrorsCounter
}

func isDiscoveryUser(name string) bool {
	if name == "" || name == "initial" {
		return true
	}
	return false
}

//###### USMSecModel - Implementation of USMModel interface ######//

//Lcd returns instance of USM UserLCD, which contains entries of USM users.
func (usm *USMSecurityModel) Lcd() USMUserLCD {
	return usm.userLCD
}

//SetLCD sets the USMUserLCD instance on this usm model. This will override the user details that exist already.
func (usm *USMSecurityModel) SetLCD(lcd USMUserLCD) {
	usm.userLCD = lcd
}

//PeerEngineLCD returns a engine lcd instance, which contains the peer engine entries.
//Implementation of SnmpSecurityModel interface.
func (usm *USMSecurityModel) PeerEngineLCD() engine.SnmpPeerEngineLCD {
	return usm.peerEngineLCD
}

//CreateNewSecurityCache creates and returns a security state reference.
//Implementation of SnmpSecurityModel interface.
func (usm *USMSecurityModel) CreateNewSecurityCache() engine.SecurityStateReference {
	return &USMSecurityCache{}
}

//ReleaseSecurityCache releases the security cache (SecurityStateReference) stored.
//Implementation of SnmpSecurityModel interface.
func (usm *USMSecurityModel) ReleaseSecurityCache(cache engine.SecurityStateReference) { /*let's deal with this later*/
}

//########## Incoming message processing ###########//

//ProcessIncomingRequest processes the incoming request messages.
//It performs the necessary security check on the incoming messages and returns error in case of any security error
//as the StatusInformation, which would be processed by MsgProcessingSubSystem.
//
//It returns the decrypted scopedPDU and security parameters after all the security check.
func (usm *USMSecurityModel) ProcessIncomingRequest(usmCache engine.SecurityStateReference, //This is newly created cache which should be filled by this module.
	msgId int32,
	msgMaxSize int32, //This will be used to calculate the maxSize response we can generate.
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32 /*MaxSizeResponseScopedPDU*/, engine.SecurityParameters, *msg.ScopedPDU, *engine.StatusInformation) {
	//Common processing for both incoming request and response
	return usm.processIncomingMessage(usmCache,
		msgId,
		msgMaxSize,
		msgFlags,
		securityParameters,
		securityModel,
		wholeMsg,
		wholeMsgLength,
		scopedPDU,
		protocolOptions,
		tmStateReference,
	)
}

//ProcessIncomingResponse processes the incoming response/report messages.
//It performs the necessary security check on the incoming messages and returns error in case of any security error as the StatusInformation, which would be processed by SnmpMsgProcessingSubSystem.
//Cache which has been prepared while sending a request will be used now for processing this response.
//
//It returns the decrypted scopedPDU and security parameters after all the security check.
func (usm *USMSecurityModel) ProcessIncomingResponse(usmCache engine.SecurityStateReference, //This cache will not be filled, as this is incoming response.
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32 /*MaxSizeResponseScopedPDU*/, engine.SecurityParameters, *msg.ScopedPDU, *engine.StatusInformation) {
	//Common processing for both incoming request and response
	return usm.processIncomingMessage(usmCache,
		msgId,
		msgMaxSize,
		msgFlags,
		securityParameters,
		securityModel,
		wholeMsg,
		wholeMsgLength,
		scopedPDU,
		protocolOptions,
		tmStateReference,
	)
}

//RFC 3414 Section 3.2
//Process the incoming request and response/report messages
func (usm *USMSecurityModel) processIncomingMessage(cache engine.SecurityStateReference, //Newly created security cache, which will be needed for response.
	msgId int32, //Maximum size of the sending SNMP Entity
	msgMaxSize int32,
	msgFlags byte,
	securityParameters []byte, //We should decode the security parameters
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32 /*MaxSizeResponseScopedPDU*/, engine.SecurityParameters, *msg.ScopedPDU, *engine.StatusInformation) {

	//Let's do the processing common for both Confirmed/Unconfirmed class
	var retScopedPDU *msg.ScopedPDU //This is the scoped pdu we are going to return
	var secError error              //To capture the security error
	var secStatusInfo *engine.StatusInformation

	//Message Processing SubSystem instance
	mps := usm.localEngine.MsgProcessingSubSystem()

	//Decode the security parameters
	secParams := newUSMSecurityParams() //This can also be done in Message Processing Model??
	secError = secParams.DecodeSecurityParameters(securityParameters)
	if secError != nil { //!isSerialized(securityParameters) { //As per RFC3417
		mps.IncSnmpInASNParseErrsCounter(consts.Version3)
		log.Debug("Error in decoding USM security parameters: " + secError.Error() + "Remote Entity: " + protocolOptions.SessionID())
		//We can pass snmpInParseErrs counter OID
		return -1, nil, nil, usm.errGen.genGeneralError(nil, //We dont have any data to provide now
			nil,
			"snmpInParseErrs: Error in decoding USM security parameters",
			nil,
			"",
			msgFlags,
			nil,
		)
	}

	//Process the Security Cache
	var usmCache *USMSecurityCache
	var ok bool
	if usmCache, ok = cache.(*USMSecurityCache); !ok {
		//return - This should not be possible - internal error
	}

	var securityLevel consts.SecurityLevel
	//This will provide the security level of the received message
	securityLevel = consts.SecurityLevel(msgFlags & security.AuthPrivMask)

	//If the security level is less than Auth-Priv, we will decode the scopedPDU now itself.
	//This will help us in extracting contextEngineID/contextName for sending reports.
	if securityLevel < security.AuthPriv {
		mps := usm.localEngine.MsgProcessingSubSystem()
		retScopedPDU, secError = mps.DecodeScopedPDU(3, scopedPDU)
		if secError != nil {
			mps.IncSnmpInASNParseErrsCounter(consts.Version3)
			log.Debug("Error in ScopedPDU decoding: " + secError.Error())
			return -1, nil, nil, usm.errGen.genGeneralError(nil, //We dont have any data to provide now
				nil,
				"snmpInParseErrs: Error in decoding of ScopedPDU."+secError.Error(),
				nil,
				"",
				msgFlags,
				nil,
			)
		}
	}
	//fmt.Println("USM Start2:\n", printOctets(wholeMsg, wholeMsgLength))
	usmCache.msgId = msgId

	//Let's extract the contextEngineId, contextName from scopedPDU
	var contextEngineId []byte = nil
	var contextName string
	if retScopedPDU != nil {
		contextEngineId = retScopedPDU.ContextEngineID()
		contextName = retScopedPDU.ContextName()
	}

	//Handle Discovery Requests and Responses
	if secStatusInfo, isContinue := usm.handleDiscoveryMessage(contextEngineId,
		contextName,
		msgFlags,
		protocolOptions, secParams,
	); secStatusInfo != nil {
		//Returning UnknownEngineID Error
		log.Debug("Unknown EngineID error for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
		return -1, secParams, retScopedPDU, secStatusInfo //return secError No need of further processing
	} else if !isContinue {
		//This could be the case with discovery response. No need of further processing.
		return -1, secParams, retScopedPDU, nil //Can we return the secParams, scopedPDU ??
	}

	//Continue processing of incoming message - Request Msg with valid EngineId (or) Response message
	//Check for user
	secureUser, err := usm.userLCD.SecureUser(secParams.AuthoritativeEngineID(),
		secParams.userName(),
	)
	//if it is confirmed and still err returned by 'SecureUser', then we should return UnknownUserName error
	if (err != nil) && (msgFlags&security.ReportableFlag == security.ReportableFlag) {
		log.Debug("Unknown UserName: '%s' with EngineID %v. Remote Entity: %s", secParams.userName(), secParams.AuthoritativeEngineID(), protocolOptions.SessionID())
		exp := usm.errGen.genUnknownUserNameError(contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)
		return -1, secParams, retScopedPDU, exp
	}
	//Shall we accept zero length UserName for NoAuthNoPriv Level??
	if err != nil { //No user exist
		var exp *engine.StatusInformation
		if strings.Contains(err.Error(), "Unknown EngineID") { //EngineID can be Unknown.
			log.Debug("Unknown EngineID: %v for user '%s'. Remote Entity: %s", secParams.AuthoritativeEngineID(), secParams.userName(), protocolOptions.SessionID())
			exp = usm.errGen.genUnknownEngineIdError(contextEngineId,
				contextName,
				msgFlags,
				secParams,
			)
		} else {
			log.Debug("Unknown UserName: '%s' with EngineID %v. Remote Entity: %s", secParams.userName(), secParams.AuthoritativeEngineID(), protocolOptions.SessionID())
			exp = usm.errGen.genUnknownUserNameError(contextEngineId,
				contextName,
				msgFlags,
				secParams,
			)
		}
		//We are returning the ScopedPDU, this will help in sending the report with contextEngineId/contextName/PDU
		return -1, secParams, retScopedPDU, exp
	}
	usmCache.msgUserName = secureUser.UserName() //Add the SecurityName to the cache

	//Check for valid security level
	if (securityLevel > secureUser.SecurityLevel()) ||
		(securityLevel >= security.AuthNoPriv && secureUser.AuthProtocol() == nil) || //Just an additional check
		(securityLevel >= security.AuthPriv && secureUser.PrivProtocol() == nil) {
		log.Debug("Security level error for user '%s' with EngineID: %v", secParams.userName(), secParams.AuthoritativeEngineID())
		secLevelExp := usm.errGen.genUnsupportedSecLevelError(contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)

		return -1, secParams, retScopedPDU, secLevelExp
	}

	switch securityLevel {

	case security.NoAuthNoPriv:
		{ //Do nothing
			//We are just dealing with unecrypted scopedPDU raw bytes. Let's decode it.
			//We have already decoded the ScopedPDU. Just return it.
			//return secParams, retScopedPDU, nil
		}

	case security.AuthNoPriv:
		{ //We are just dealing with unecrypted scopedPDU raw bytes.
			//Authenticate the message
			//Fills the usmUserAuthProtocol/AuthKey on the cache **
			secStatusInfo = usm.authModule.authenticateIncomingMsg(msgId,
				msgMaxSize,
				msgFlags,
				securityModel,                  //Should be USM
				retScopedPDU.ContextEngineID(), //We have decoded the ScopedPDU already
				retScopedPDU.ContextName(),
				retScopedPDU.SnmpPDU,
				nil,
				secParams,
				usmCache, //To fill the Authentication Protocol/Key
				mps,      //Pass the msgProcessing SubSystem instance to perform encoding
			)
			if secStatusInfo != nil {
				//Can we release the cache here?
				log.Debug("Authentication failure for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
				return -1, secParams, retScopedPDU, secStatusInfo
			}
			//Timeliness check - Performed Timeliness module
			secStatusInfo = usm.handleTimeliness(retScopedPDU.ContextEngineID(),
				retScopedPDU.ContextName(),
				msgFlags,
				protocolOptions,
				secParams,
			)
			if secStatusInfo != nil {
				log.Debug("Timeliness failure for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
				return -1, secParams, retScopedPDU, secStatusInfo
			}
			//This message is authenticated successfully
		}

	case security.AuthPriv:
		{
			//Authenticate the message
			//Fills the usmUserAuthProtocol/AuthKey on the cache **
			secStatusInfo = usm.authModule.authenticateIncomingMsg(msgId,
				msgMaxSize,
				msgFlags,
				securityModel, //Should be USM
				nil,
				"<nil>",
				msg.SnmpPDU{},
				scopedPDU, //Encrypted ScopedPDU byte array
				secParams,
				usmCache,
				mps, //Pass the msgProcessing SubSystem instance to perform encoding
			)
			if secStatusInfo != nil {
				//Can we release the cache here?
				log.Debug("Authentication failure for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
				return -1, secParams, retScopedPDU, secStatusInfo
			}
			//Timeliness Check - Performed by Timeliness modules
			secStatusInfo = usm.handleTimeliness(nil, //We haven't decoded the ScopedPDU, since it's auth-priv
				"",
				msgFlags,
				protocolOptions,
				secParams,
			)
			if secStatusInfo != nil {
				log.Debug("Timeliness failure for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
				return -1, secParams, retScopedPDU, secStatusInfo
			}
			//Let's decrypt the message
			var decScopedPDU *msg.ScopedPDU
			decScopedPDU, secStatusInfo = usm.privModule.decryptData(secParams, //Fills the usmUserPrivProtocol/PrivKey on the cache
				usmCache,
				scopedPDU, //Encrypted ScopedPDU byte array
				msgFlags,
			)
			if secStatusInfo != nil {
				log.Debug("Decryption failure for incoming message: %s. MSG:: %s", secStatusInfo.Error(), getMsgDebugStr(retScopedPDU, protocolOptions, secParams))
				return -1, secParams, retScopedPDU, secStatusInfo //It may contain general error as well. Without Report OIDs
			}
			//This message is authenticated and decrypted successfully
			retScopedPDU = new(msg.ScopedPDU)
			retScopedPDU.SetContextEngineID(decScopedPDU.ContextEngineID())
			retScopedPDU.SetContextName(decScopedPDU.ContextName())
			retScopedPDU.SnmpPDU = decScopedPDU.SnmpPDU
		}

	}
	//Set the security level of the msg
	if retScopedPDU != nil {
		retScopedPDU.SetSecurityLevel(securityLevel)
	}

	//Let's calculate the maxSizeResponseScopedPDU - RFC 3414 3.2 Step 9
	var maxSizeResponseScopedPDU int32
	maxSecParams := secParams.maxLen()
	maxSizeResponseScopedPDU = msgMaxSize - maxSecParams

	return maxSizeResponseScopedPDU, secParams, retScopedPDU, nil
}

//Handle the incoming request/report discovery messages.
func (usm *USMSecurityModel) handleDiscoveryMessage(contextEngineId []byte,
	contextName string,
	flags byte,
	protocolOptions transport.ProtocolOptions,
	secParams *USMSecurityParameters,
) (*engine.StatusInformation, bool) {
	//Empty EngineId - Send report immediately
	if secParams.AuthoritativeEngineID() == nil /*|| len(secParams.AuthoritativeEngineId()) != 12*/ { //EngineId is null or illegal sized engine id. We should send discovery response
		secParams.setAuthoritativeEngineID(usm.localEngine.SnmpEngineID())
		secParams.setAuthoritativeEngineBoots(0)
		secParams.setAuthoritativeEngineTime(0)
		//return Generate UnknownEngineId Exception
		//UnknownEngineId. Incoming discovery request. Send discovery report back.
		return usm.errGen.genUnknownEngineIdError(contextEngineId, contextName, flags, secParams), false //Donot continue processing
	}

	//Non-Empty EngineId
	if flags&security.ReportableFlag == security.ReportableFlag { //Confirmed class. We are authoritative
		if !bytes.Equal(usm.localEngine.SnmpEngineID(), secParams.AuthoritativeEngineID()) {
			//return Generate UnknownEngineId Exception
			return usm.errGen.genUnknownEngineIdError(contextEngineId, contextName, flags, secParams), false
		}
		return nil, true //Other response msgs, continue processing
	} else { //Unconfirmed class. Incoming response message. We are not authoritative.
		if isDiscoveryUser(secParams.userName()) { //Discovery Response
			//if !bytes.Equal(usm.localEngine.SnmpEngineID(), secParams.AuthoritativeEngineID()) {
			//Create new SnmpPeer (Authoritative Entity)
			if peer := usm.peerEngineLCD.Engine(protocolOptions.RemoteHost(), protocolOptions.RemotePort()); peer != nil {
				//There is already a peer exist with this EngineId, but we have received a discovery response.
				if peer.AuthoritativeEngineID() == nil || len(peer.AuthoritativeEngineID()) == 0 {
					peer.(*USMPeerEngine).SetAuthoritativeEngineID(secParams.AuthoritativeEngineID())
					usm.peerEngineLCD.AddEngine(peer) //Update the entry
				} else {
					//There is already peer exist with non-nil EngineID for this Entity
					if !bytes.Equal(peer.AuthoritativeEngineID(), secParams.AuthoritativeEngineID()) {
						//New EngineID received for the remote entity. Let's update the values
						peer.(*USMPeerEngine).SetAuthoritativeEngineID(secParams.AuthoritativeEngineID())
						peer.(*USMPeerEngine).SetAuthoritativeEngineBoots(0) //Helps in time-resynchronization
						peer.(*USMPeerEngine).SetLatestReceivedEngineTime(0)

						usm.peerEngineLCD.AddEngine(peer) //Update the entry
					}
				}
			} else {
				peer := NewUSMPeerEngine(protocolOptions.RemoteHost(), //Creating New Peer
					protocolOptions.RemotePort(),
				)
				peer.SetAuthoritativeEngineID(secParams.AuthoritativeEngineID())
				usm.peerEngineLCD.AddEngine(peer)
			}
			return nil, false //No need to continue processing
			//} else {
			//return Generate exception - Received discovery response with same engineid as local engine
			//This maybe possible
			//}
		}
		// else {} We dont care about other responses as of now
	}

	return nil, true //No StatusInfo, continue processing
}

//Handle the timeliness for incoming requests/responses.
func (usm *USMSecurityModel) handleTimeliness(contextEngineId []byte,
	contextName string,
	flags byte,
	protocolOptions transport.ProtocolOptions,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {

	//We are authoritative
	if bytes.Equal(secParams.AuthoritativeEngineID(), usm.localEngine.SnmpEngineID()) &&
		flags&security.ReportableFlag == security.ReportableFlag {
		if err := usm.timeModule.CheckAuthTimeliness(usm.localEngine, secParams, flags); err != nil {
			return err
		}
	} else { //We are not authoritative
		//Extract the engine peer
		var peer *USMPeerEngine
		if eng := usm.peerEngineLCD.Engine(protocolOptions.RemoteHost(),
			protocolOptions.RemotePort(),
		); eng != nil {
			peer = eng.(*USMPeerEngine)
		}
		/*if peer == nil {
			//This handling is done for Incoming Trap messages, where we have added only EngineID
			//(RemoteHost/RemotePort are Unknown by that time)
			peer = usm.peerEngineLCD.EngineByID(secParams.AuthoritativeEngineId())
		}*/
		if peer == nil {
			peer = NewUSMPeerEngine(protocolOptions.RemoteHost(), //Creating New Peer
				protocolOptions.RemotePort(),
			)
			peer.SetAuthoritativeEngineID(secParams.AuthoritativeEngineID())
			usm.peerEngineLCD.AddEngine(peer)
		}
		//Eventhough we are not authoritative, we can generate report/exception as per RFC 3414  Section 3.2 Step 7.b
		if err := usm.timeModule.CheckNonAuthTimeliness(peer, secParams, flags); err != nil {
			return err
		}
	}

	return nil
}

//###### Outgoing Message Processing ############//

//GenerateRequestMsg prepares the SnmpMessage after applying the necessary security on the message.
//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
//
//It returns the final encoded byte array along with necessary security parameters filled.
//Returns error as a StatusInformation if any.
func (usm *USMSecurityModel) GenerateRequestMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU, //Plain text, unencrypted
	protocolOptions transport.ProtocolOptions,
) (engine.SecurityParameters, []byte, transport.TransportStateReference, *engine.StatusInformation /*statusInfo*/) {

	//We are doing a common processing for both outgoing requests and responses
	return usm.generateOutgoingMsg(version,
		msgId,
		msgMaxSize,
		msgFlags,
		securityModel,
		securityName,
		securityEngineId,
		scopedPDU,
		nil, //We dont have any security cache for outgoing requests
		protocolOptions,
	)
}

//GenerateResponseMsg prepares the SnmpMessage after applying the necessary security on the message.
//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
//cache which has been prepared from the incoming request will be used now for preparing this response message.
//
//It returns the final encoded byte array along with necessary security parameters filled.
//Returns error as a StatusInformation if any.
func (usm *USMSecurityModel) GenerateResponseMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU,
	usmCache engine.SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
	protocolOptions transport.ProtocolOptions,
) (engine.SecurityParameters, []byte, transport.TransportStateReference, *engine.StatusInformation /*statusInfo*/) {

	//We are doing a common processing for both outgoing requests and responses
	return usm.generateOutgoingMsg(version,
		msgId,
		msgMaxSize,
		msgFlags,
		securityModel,
		securityName,
		securityEngineId,
		scopedPDU,
		usmCache,
		protocolOptions,
	)
}

//Common processing for both outgoing requests and responses
func (usm *USMSecurityModel) generateOutgoingMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU,
	cache engine.SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
	protocolOptions transport.ProtocolOptions,
) (engine.SecurityParameters, []byte, transport.TransportStateReference, *engine.StatusInformation) {

	//Let's do the common processing for both outgoing Requests/Responses
	var encodedData []byte //Final encoded SnmpMessage byte array to return
	var encScopedPDU msg.ScopedPDU
	var secError *engine.StatusInformation //StatusInformation Security Error

	//This is where we are going to fill the Message Security Parameters
	securityParameters := newUSMSecurityParams()
	var secUser USMSecureUser
	var secEngineId []byte = nil

	var usmCache *USMSecurityCache = nil
	if cache != nil {
		usmCache = cache.(*USMSecurityCache) //This is an outgoing response/report
		/*if usmCache.authoritativeEngineID == nil {
			securityParameters.SetAuthoritativeEngineId(securityEngineId)
			usmCache.authoritativeEngineID = securityEngineId
		}
		if usmCache.msgUserName == "" {
			securityParameters.SetUserName(securityName)
			usmCache.msgUserName = securityName
		} else {
			securityParameters.SetUserName(usmCache.msgUserName)
		}*/
	}
	tmStateReference := new(transport.TmStateReference)

	//Unconfirmed class (Response/Report)
	if (msgFlags&security.ReportableFlag != security.ReportableFlag) && (usmCache != nil) {
		//We should use the data from cache for Unconfirmed class
		secUser = NewUSMSecureUserImpl(usm.userLCD,
			usmCache.msgUserName,
			usmCache.authoritativeEngineID,
			usmCache.usmUserAuthProtocol, //These data could have been already filled by auth/priv module in processIncomingMsg() method
			usmCache.usmUserAuthKey,
			usmCache.usmUserPrivProtocol,
			usmCache.usmUserPrivKey,
		)
		securityName = usmCache.msgUserName
		//We are authoritative
		secEngineId = usm.localEngine.SnmpEngineID() //Set to local EngineId
	} else { //Confirmed class - EngineId can be empty
		var err error
		if securityEngineId == nil || len(securityEngineId) == 0 {
			if (isDiscoveryUser(securityName)) && //This is a discovery request. Don't expect user to be present, after all EngineId is null.
				(msgFlags&security.AuthPrivMask == byte(security.NoAuthNoPriv)) {
				//This would be the discovery request. Let's allow it
			} else {
				//Not a discovery user and EngineId is empty.
				peerEngine := usm.peerEngineLCD.Engine(protocolOptions.RemoteHost(),
					protocolOptions.RemotePort(),
				)
				if peerEngine == nil ||
					peerEngine.AuthoritativeEngineID() == nil ||
					len(peerEngine.AuthoritativeEngineID()) == 0 {
					//No Engine Entry found
					errStr := "Engine entry not found for the Remote Entity: " + protocolOptions.SessionID()
					log.Debug("Failure in generating message. " + errStr)
					engExp := engine.NewStatusInformation(errStr)
					return nil, nil, tmStateReference, engExp
				}
			}
		} else {
			secUser, err = usm.userLCD.SecureUser(securityEngineId, securityName)
			if err != nil {
				//return unknownSecurityName'
				errStr := "Failure in generating message. UnknownSecurityName: "
				if strings.EqualFold(securityName, "") {
					errStr += "<nil>"
				} else {
					errStr += securityName
				}
				log.Debug(errStr + " MSG:: " + getMsgDebugStr(&scopedPDU, protocolOptions, nil))
				userExp := engine.NewStatusInformation(errStr)
				return nil, nil, tmStateReference, userExp
			}
		}
		secEngineId = securityEngineId
	}
	securityParameters.setUserName(securityName /*secUser.UserName()*/) //Can be from cache or securityParameters itself (RFC3414 3.2 - Point 7)

	secLevel := consts.SecurityLevel(msgFlags & security.AuthPrivMask)
	if secUser != nil && secLevel > secUser.SecurityLevel() {
		//return UnsupportedSecLevel
		log.Debug("Failure in generating messsage. Unsupported security level for user '%s' with EngineID: %v", securityName, secEngineId)
		secLevelExp := engine.NewStatusInformation("Failure in generating messsage. UnsupportedSecurityLevel for User: " + securityName)
		return nil, nil, tmStateReference, secLevelExp
	}

	securityParameters.setAuthoritativeEngineID(secEngineId) //Can be null

	switch secLevel {
	case security.NoAuthNoPriv:
		{
			//Shall we clear all the Management Information as it is Auth-Priv

			//Just a plain text in this case
			encScopedPDU.SetContextName(scopedPDU.ContextName())
			encScopedPDU.SetContextEngineID(scopedPDU.ContextEngineID())
			encScopedPDU.SnmpPDU = scopedPDU.SnmpPDU

			securityParameters.setAuthParameters(nil) //Zero-Length octet string
			securityParameters.setPrivParameters(nil)

			//Encode the security parameters
			encSecParams, encError := securityParameters.EncodeSecurityParameters()
			if encError != nil {
				log.Debug("Failure in generating message. Error in security parameters encoding: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				//return //Problem SecurityParams encoding
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}

			//Let's encode the complete message,
			mps := usm.localEngine.MsgProcessingSubSystem()
			encodedData, encError = mps.Encode(version,
				msgId,
				msgMaxSize,
				msgFlags,
				security.USMID,
				encSecParams,
				encScopedPDU,
			)

			if encError != nil {
				//log the error and return
				log.Debug("Failure in generating message. Error in encoding the message: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}

		}

	case security.AuthNoPriv:
		{
			if (msgFlags&security.ReportableFlag == security.ReportableFlag) ||
				(!bytes.Equal(usm.localEngine.SnmpEngineID(), securityParameters.AuthoritativeEngineID())) {
				//This implies it's a request expecting response - We are not authoritative
				peer := usm.peerEngineLCD.Engine(protocolOptions.RemoteHost(),
					protocolOptions.RemotePort(),
				)

				if peer != nil {
					securityParameters.setAuthoritativeEngineBoots(peer.AuthoritativeEngineBoots())
					securityParameters.setAuthoritativeEngineTime(peer.AuthoritativeEngineTime())
				} else { //This could be discovery message. Send with the default zero EB/ET. Whether this is needed??
					securityParameters.setAuthoritativeEngineBoots(0)
					securityParameters.setAuthoritativeEngineTime(0)
				}
			} else { //This is response/report. We are authoritative
				securityParameters.setAuthoritativeEngineBoots(usm.localEngine.SnmpEngineBoots())
				securityParameters.setAuthoritativeEngineTime(usm.localEngine.SnmpEngineTime())
			}

			//Authenticate the message - PrivParameters is zeroed
			calcHMAC, secError := usm.authModule.authenticateOutgoingMsg(msgId,
				msgMaxSize,
				msgFlags,
				securityModel, //Should be USM
				scopedPDU.ContextEngineID(),
				scopedPDU.ContextName(),
				scopedPDU.SnmpPDU,
				nil,
				securityParameters,
				usmCache,
				usm.localEngine.MsgProcessingSubSystem(),
			)
			if secError != nil {
				//return "authenticationFailure: unable to authenticate outgoing message"
				log.Debug("Failure in generating message. Unable to authenticate the outgoing message: %s. MSG:: %s", secError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				return nil, nil, tmStateReference, secError
			}
			securityParameters.setAuthParameters(calcHMAC)
			securityParameters.setPrivParameters(nil) //PrivParameters should be zeroed here.

			encScopedPDU.SetContextName(scopedPDU.ContextName())
			encScopedPDU.SetContextEngineID(scopedPDU.ContextEngineID())
			encScopedPDU.SnmpPDU = scopedPDU.SnmpPDU

			//Encode the security parameters
			encSecParams, encError := securityParameters.EncodeSecurityParameters()
			if encError != nil {
				//return //Problem SecurityParams encoding
				log.Debug("Failure in generating message. Error in security parameters encoding: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}
			//By this time, we have security parameters filled and encScopedPDU filled, let's encode the data now.
			mps := usm.localEngine.MsgProcessingSubSystem()
			encodedData, encError = mps.Encode(version,
				msgId,
				msgMaxSize,
				msgFlags,
				security.USMID,
				encSecParams,
				encScopedPDU,
			)
			if encError != nil {
				//log the error and return
				log.Debug("Failure in generating message. Error in encoding the message: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}
			//fmt.Println("In Auth-NoPriv: ", encodedData, "\n", printOctets(encodedData, len(encodedData)))
		}

	case security.AuthPriv:
		{
			//Configure Timeliness Values
			//We are setting the timeliness values before encryption, because encryption algorithm might require
			//these values for encryption
			if (msgFlags&security.ReportableFlag == security.ReportableFlag) ||
				(!bytes.Equal(usm.localEngine.SnmpEngineID(), securityParameters.AuthoritativeEngineID())) {
				//This implies it's a request expecting response - We are not authoritative
				peer := usm.peerEngineLCD.Engine(protocolOptions.RemoteHost(),
					protocolOptions.RemotePort(),
				)
				if peer != nil {
					securityParameters.setAuthoritativeEngineBoots(peer.AuthoritativeEngineBoots())
					securityParameters.setAuthoritativeEngineTime(peer.AuthoritativeEngineTime())
				} else { //This could be discovery message. Send with the default zero EB/ET. Whether this is needed??
					securityParameters.setAuthoritativeEngineBoots(0)
					securityParameters.setAuthoritativeEngineTime(0)
				}
			} else { //This is response/report. We are authoritative
				securityParameters.setAuthoritativeEngineBoots(usm.localEngine.SnmpEngineBoots())
				securityParameters.setAuthoritativeEngineTime(usm.localEngine.SnmpEngineTime())
			}

			var dataToEncrypt []byte
			var encError error
			dataToEncrypt, encError = usm.localEngine.MsgProcessingSubSystem().EncodeScopedPDU(version, scopedPDU)
			if encError != nil {
				//return //Problem SecurityParams encoding
				log.Debug("Failure in generating message. Error in ScopedPDU encoding: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}

			encryptedData, privParams, secError := usm.privModule.encryptData(securityParameters,
				usmCache,
				dataToEncrypt, //Byte array of Scoped PDU
			)
			if secError != nil {
				log.Debug("Failure in generating message. Unable to encrypt the outgoing message: %s. MSG:: %s", secError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				return nil, nil, tmStateReference, secError
			}
			securityParameters.setPrivParameters(privParams) //SALT

			//Encrypted message should be authenticated
			calcHMAC, secError := usm.authModule.authenticateOutgoingMsg(msgId,
				msgMaxSize,
				msgFlags,
				securityModel, //Should be USM
				nil,
				"",
				msg.SnmpPDU{},
				encryptedData,      //This is encrypted byte array, which should be authenticated
				securityParameters, //Filled with privacy parameters
				usmCache,
				usm.localEngine.MsgProcessingSubSystem(), //Pass the msgProcessing SubSystem instance to perform encoding
			)
			if secError != nil {
				//return "authenticationFailure: unable to authenticate outgoing message"
				log.Debug("Failure in generating message. Unable to authenticate the outgoing message: %s. MSG:: %s", secError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				return nil, nil, tmStateReference, secError
			}
			securityParameters.setAuthParameters(calcHMAC)

			//Encode the security parameters
			encSecParams, encError := securityParameters.EncodeSecurityParameters()
			if encError != nil {
				//return //Problem SecurityParams encoding
				log.Debug("Failure in generating message. Error in security parameters encoding: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}

			//By this time, we have security parameters filled and encScopedPDU filled, let's encode the data now.

			mps := usm.localEngine.MsgProcessingSubSystem()
			encodedData, encError = mps.EncodePriv(version,
				msgId,
				msgMaxSize,
				msgFlags,
				security.USMID,
				encSecParams,
				encryptedData, //encScopedPDU.encryptedPdu, //Encrypted ScopedPDU byte array
			)
			if encError != nil {
				//return Encoding Failure
				log.Debug("Failure in generating message. Error in encoding the message: %s. MSG:: %s", encError.Error(),
					getMsgDebugStr(&encScopedPDU, protocolOptions, securityParameters))
				secError = usm.errGen.genGeneralError(nil,
					nil,
					encError.Error(),
					scopedPDU.ContextEngineID(),
					scopedPDU.ContextName(),
					msgFlags,
					securityParameters,
				)
				return nil, nil, tmStateReference, secError
			}
		}

	}
	//StatusInfo is just Success (or) Failure as we are the one sending the message
	return securityParameters, encodedData, tmStateReference, nil
}

//Implements SecurityStateReference interface to store/cache data required for USM.
//
//For incoming confirmed pdu, this cache will be generated, which in later will be used while sending response.
//For usm package internal use only.
type USMSecurityCache struct { //Implements Security Cache interface
	msgId                 int32
	authoritativeEngineID []byte
	msgUserName           string
	usmUserAuthProtocol   USMAuthAlgorithm //Can be given as AuthPair later
	usmUserAuthKey        []byte
	usmUserPrivProtocol   USMPrivAlgorithm
	usmUserPrivKey        []byte
}

//Returns the MsgID to which this cache is associated with.
func (usmCache *USMSecurityCache) MsgID() int32 {
	return usmCache.msgId
}

func calcMaxSizeResponsePDU(msgMaxSize int32) int32 {
	return 44
}

//USM Security Parameters to hold the security parameters specific to USM.
//
//It's an implementation of SecurityParameters interface.
//For usm package internal use only.
type USMSecurityParameters struct {
	authEngineId    []byte
	authEngineBoots int32 //Max value of 2^31-1
	authEngineTime  int32 //Max value of 2^31-1
	authUserName    string
	authParams      []byte //12 Octets Digest
	privParams      []byte //8 Octets SALT
}

func newUSMSecurityParams() *USMSecurityParameters {
	return new(USMSecurityParameters)
}

func newUSMSecurityParameters(engineId []byte,
	boots int32,
	time int32,
	userName string,
	authParams []byte,
	privParams []byte,
) *USMSecurityParameters {

	secParams := new(USMSecurityParameters)
	secParams.setAuthoritativeEngineID(engineId)
	secParams.setAuthoritativeEngineBoots(boots)
	secParams.setAuthoritativeEngineTime(time)
	secParams.setUserName(userName)
	secParams.setAuthParameters(authParams)
	secParams.setPrivParameters(privParams)

	return secParams
}

type usmSecParams struct {
	EngineIDRaw    []byte
	EngineBootsRaw int32
	EngineTimeRaw  int32
	UserNameRaw    []byte
	AuthParamsRaw  []byte
	PrivParamsRaw  []byte
}

//Encode security parameters specific to USM module.
func (usp *USMSecurityParameters) EncodeSecurityParameters() ([]byte, error) {
	var val []byte
	var err error

	secParams := usmSecParams{usp.authEngineId, usp.authEngineBoots, usp.authEngineTime, []byte(usp.authUserName), usp.authParams, usp.privParams}

	if val, err = asn1.Marshal(secParams); err != nil {
		return nil, errors.New("Error while encoding Security Params : " + err.Error())
	}

	return val, nil
}

//Decode and return the security parameters specific to USM.
func (usp *USMSecurityParameters) DecodeSecurityParameters(secParamsBytes []byte) error {
	secParams := new(usmSecParams)

	if _, err := asn1.Unmarshal(secParamsBytes, secParams); err != nil {
		return errors.New("Error while decoding Security Params : " + err.Error())
	}

	usp.authEngineId = secParams.EngineIDRaw
	usp.authEngineBoots = secParams.EngineBootsRaw
	usp.authEngineTime = secParams.EngineTimeRaw
	usp.authUserName = string(secParams.UserNameRaw)
	usp.authParams = secParams.AuthParamsRaw
	usp.privParams = secParams.PrivParamsRaw

	return nil
}

//SecurityEngineID returns the EngineID in the security paramters field as a byte array.
func (usp *USMSecurityParameters) SecurityEngineID() []byte {
	return usp.authEngineId
}

//SecurityName returns the security name (UserName) in the security parameters field.
func (usp *USMSecurityParameters) SecurityName() string {
	return usp.authUserName
}

//AuthoritativeEngineID returns the EngineID in the security paramters field as a byte array.
func (usp *USMSecurityParameters) AuthoritativeEngineID() []byte {
	return usp.authEngineId
}

func (usp *USMSecurityParameters) setAuthoritativeEngineID(engineId []byte) {
	usp.authEngineId = engineId
}

func (usp *USMSecurityParameters) authoritativeEngineBoots() int32 {
	return usp.authEngineBoots
}

func (usp *USMSecurityParameters) setAuthoritativeEngineBoots(boots int32) {
	if boots < 0 || boots > 0x7FFFFFFF { //EngineBoots range
		return
	}
	usp.authEngineBoots = boots
}

func (usp *USMSecurityParameters) authoritativeEngineTime() int32 {
	return usp.authEngineTime
}

func (usp *USMSecurityParameters) setAuthoritativeEngineTime(time int32) {
	if time < 0 || time > 0x7FFFFFFF { //EngineTime range
		return
	}
	usp.authEngineTime = time
}

func (usp *USMSecurityParameters) userName() string {
	return usp.authUserName
}

func (usp *USMSecurityParameters) setUserName(userName string) {
	usp.authUserName = userName
}

func (usp *USMSecurityParameters) authParameters() []byte {
	return usp.authParams
}

func (usp *USMSecurityParameters) setAuthParameters(authParams []byte) {
	usp.authParams = authParams
}

func (usp *USMSecurityParameters) privParameters() []byte {
	return usp.privParams
}

func (usp *USMSecurityParameters) setPrivParameters(privParams []byte) {
	usp.privParams = privParams
}

func (usp *USMSecurityParameters) maxLen() int32 {
	return 32 + 2 + 6 + 6 + 32 + 2 + 12
}

func getMsgDebugStr(mesg *msg.ScopedPDU, protocolOptions transport.ProtocolOptions, secParams *USMSecurityParameters) string {
	msgStr := ""
	if mesg != nil {
		msgStr += "ReqID: " + strconv.FormatInt(int64(mesg.RequestID()), 10) + "; "
		msgStr += "Command: " + getMSGCommand(mesg.Command()) + "; "
		if protocolOptions != nil {
			msgStr += "RemoteAddress: " + protocolOptions.SessionID() + "; "
		}
		if secParams != nil {
			msgStr += "UserName: " + secParams.userName() + "; "
			msgStr += "EngineID: " + string(secParams.AuthoritativeEngineID()) + "; "
		}
	} else {
		msgStr = "<nil>"
	}

	return msgStr
}

//getMSGCommand returns the string form of the SnmpMessage's command.
func getMSGCommand(cmd consts.Command) string {

	var cmdStr string = ""

	switch cmd {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	case consts.ReportMessage:
		cmdStr = "Report"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}
