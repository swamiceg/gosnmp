/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/engine"

	//"fmt"
	"bytes"
	"encoding/hex"
	"strconv"
	"strings"
	"testing"
)

//Test the algorithm manager
var algoTest = []struct {
	algoName string
	oid      string
	algo     USMAlgorithm
}{
	{"MD5", "1.1.0", newAlgo("MD5", "1.1.0")},
	{"DES", "1.2.0", newAlgo("DES", "1.2.0")},
	{"", "", nil},
	{"MD5", "", nil},
}

func TestAlgoMgr(t *testing.T) {
	algoMgr := NewUSMAlgorithmMgr()

	for _, at := range algoTest {
		algoMgr.RegisterAlgorithm(at.algo)
		if at.algo != nil {
			if len(algoMgr.algos) < 1 {
				t.Errorf("USMAlgoMgr: Failure in registering algorithm. Name: %s.", at.algoName)
			}
			if algoMgr.Algorithm(at.algoName) == nil {
				t.Errorf("USMAlgoMgr: Failure in obtaining algorithm. Name: %s.", at.algoName)
			}
			if algoMgr.Algorithm(at.oid) == nil {
				t.Errorf("USMAlgoMgr: Failure in obtaining algorithm. Name: %s.", at.algoName)
			}
		} else {
			if len(algoMgr.algos) > 0 {
				t.Errorf("USMAlgoMgr: Registered nil algorithm. Name: %s.", at.algoName)
			}
		}

		remAlgo := algoMgr.UnRegisterAlgorithm(at.algoName)
		if at.algo != nil && remAlgo == nil {
			t.Errorf("USMAlgoMgr: Failure in unregistering algorithm. Name: %s.", at.algoName)
		}
	}

	//Remove algorithm by OID string
	oid := "1.1.0"
	algoMgr.RegisterAlgorithm(newAlgo("TestAlgo", oid))
	remAlgo := algoMgr.UnRegisterAlgorithm(oid)
	if remAlgo == nil {
		t.Errorf("USMAlgoMgr: Failure in unregistering algorithm. OID: %s.", oid)
	}

	ct := 5
	for i := 0; i < ct; i++ {
		algoMgr.RegisterAlgorithm(newAlgo("TestAlgo", oid+"."+strconv.Itoa(i+1)))
	}
	if len(algoMgr.Algorithms()) != ct {
		t.Errorf("USMAlgoMgr: Invalid algo mgr length returned. Expected: %d. Got: %d.", ct, len(algoMgr.Algorithms()))
	}
}

type usmAlgo struct {
	algoName string
	oid      string
}

func newAlgo(algoName, oid string) *usmAlgo {
	return &usmAlgo{
		algoName: algoName,
		oid:      oid,
	}
}
func (u *usmAlgo) Algorithm() string {
	return u.algoName
}
func (u *usmAlgo) OID() string {
	return u.oid
}

//Test the authentication algorithms

var md5PKTest = []struct {
	pwd        string
	secKey     string
	engID      string
	locSecKey  string
	locPrivKey string
	digest     string
}{
	{"maplesyrup", "9faf3283884e92834ebc9847d8edd963", "000000000000000000000002", "526f5eed9fcce26f8964c2930787d82b", "526F5EED9FCCE26F8964C2930787D82B79EFF44A90650EE0A3A40ABFAC5ACC12", "9EBD1666A67D22C81FB94BCC"},

	{"LengthyPasswordForTestingHello", "C8C5B262F54E06CBA98CEAB5CAA159C6", "010202030406082101166f0c010101630001", "64149040A3E4D80691DF0474ADE3915D", "64149040A3E4D80691DF0474ADE3915D486A417098DCD16B490EFBD6F1B09E1F", "F5FB663E4161D59D71BAE8F4"},

	{"a", "7202826A7791073FE2787F0C94603278", "0101", "4ACCC0EE58DB36F3512B873DB7FB4C51", "4ACCC0EE58DB36F3512B873DB7FB4C5145567E0398AEE487868D29F7BE9BB8E7", "65DD523B0CA7968CF1E4EDB0"},

	{"EmptyEngineID", "5A02A5B629F42DA1B000CD28ECDCE202", "", "5560E091CA7EEFA485290D78067A26D6", "5560E091CA7EEFA485290D78067A26D689F8038C17C1EA0E83115C43560C05B9", "A1809C4FEC3197131826F069"},

	{"LongEngineID", "AC7C5E7077F23F2B24EF3B8586154BED", "01010405050515010117040506072c37160d08080808085404040302020505050505050203030203030303030303", "be22ef8449b5f02610da9fff2b1c3146", "be22ef8449b5f02610da9fff2b1c3146168d00f53ac798b68f453d11bc8bc5af", "ff4f3fa73a6f494ad2e7b7f2"},
}

var shaPKTest = []struct {
	pwd        string
	secKey     string
	engID      string
	locSecKey  string
	locPrivKey string
	digest     string
}{
	{"maplesyrup", "9FB5CC0381497B3793528939FF788D5D79145211", "000000000000000000000002", "6695FEBC9288E36282235FC7151F128497B38F3F", "6695febc9288e36282235fc7151f128497b38f3f9b8b6d78936ba6e7d19dfd9c", "4AABD714951EF96C6D589D3C"},

	{"LengthyPasswordForTestingHello", "86FF02B3FC5252E0CD78A2C293B4C454403146A2", "010202030406082101166f0c010101630001", "32B50504B85164E9B5440E5C8A2ABADCB1569D07", "32b50504b85164e9b5440e5c8a2abadcb1569d07a6439d423b5e7cb00e3332b3", "16C6A6AD163C2CBA7B64F694"},

	{"a", "454027D64E3B855735552D42230EEA1CBD645FA0", "0101", "FCBB5488B60555F0B966C95857B9D78FB30A6576", "fcbb5488b60555f0b966c95857b9d78fb30a657630b69e73a3c8487e34a91586", "495816DCA72C353D03306C2E"},

	{"EmptyEngineID", "05F7C060E737CFEB58D47AB30F8AB2399A932C32", "", "D0EDC5E6074C81DEF7208202357D2BD65E32DA27", "d0edc5e6074c81def7208202357d2bd65e32da2713bf5401927b858df8780aaf", "3748038B1EA05C12B87F7611"},

	{"LongEngineID", "cee5d6b7e6ea65970b8b610cdd4f4c0af9d617dc", "01010405050515010117040506072c37160d08080808085404040302020505050505050203030203030303030303", "d069af3f6492f4e3778a23a040c66a61cf5c9871", "d069af3f6492f4e3778a23a040c66a61cf5c987177e021611e343fcd893748ca", "e21fb3700d2907d983bfa331"},
}

func TestMD5AuthenticationAlgorithm(t *testing.T) {
	//MD5
	md := NewMD5()
	if md.Algorithm() == "" || md.OID() == "" {
		t.Error("MD5: Algorithm/OID name cannot be empty.")
	}

	//Test password to key conversion and message digest generation
	for _, pk := range md5PKTest {
		secKey := md.PasswordToKey(pk.pwd)
		if hex.EncodeToString(secKey) != strings.ToLower(pk.secKey) {
			t.Errorf("MD5: Error in password to key conversion. Expected: %s. Got: %s.", pk.secKey, hex.EncodeToString(secKey))
		}

		engID, _ := hex.DecodeString(pk.engID)

		localKey := md.LocalizeAuthKey(secKey, engID)
		if hex.EncodeToString(localKey) != strings.ToLower(pk.locSecKey) {
			t.Errorf("MD5: Error in key localization. Expected: %s. Got: %s.", pk.locSecKey, hex.EncodeToString(localKey))
		}

		data := ""
		digest := md.SignMsg([]byte(data), localKey)
		if hex.EncodeToString(digest) != strings.ToLower(pk.digest) {
			t.Errorf("MD5: Error in digest generation. Expected: %s. Got: %s.", strings.ToLower(pk.digest), hex.EncodeToString(digest))
		}

		if !md.VerifyMsg([]byte(data), localKey, digest) {
			t.Error("MD5: Error in digest verification.")
		}
		invalidDigest := digest
		invalidDigest[1] = 123
		if md.VerifyMsg([]byte(data), localKey, invalidDigest) {
			t.Error("MD5: Error in digest verification when sending invalid digest.")
		}

		lpk := md.LocalizePrivKey(secKey, engID, 32)
		if lpk == nil {
			lpk = md.ExtendPrivKey(pk.pwd, engID, 32)
		}
		if hex.EncodeToString(lpk) != strings.ToLower(pk.locPrivKey) {
			t.Errorf("MD5: Error in privacy key localization generation. Expected: %s. Got: %s.", strings.ToLower(pk.locPrivKey), hex.EncodeToString(lpk))
		}
	}

	//Key change test
	engID, _ := hex.DecodeString("000000000000000000000002")
	random, _ := hex.DecodeString("0000000000000000000000000000000000000000000000000000000000000000")
	oldKey := "maplesyrup"
	newKey := "newsyrup"

	//Auth key change test
	oldAuthKey := md.LocalizeAuthKey(md.PasswordToKey(oldKey), engID)
	newAuthKey := md.LocalizeAuthKey(md.PasswordToKey(newKey), engID)
	delta := md.CalculateAuthDelta(oldAuthKey, newAuthKey, random)
	calcNewKey := md.CalculateNewAuthKey(oldAuthKey, append(random, delta...))
	if !bytes.Equal(calcNewKey, newAuthKey) {
		t.Errorf("MD5: Auth KeyChange Failure. Expected: %s. Got: %s.", hex.EncodeToString(newAuthKey), hex.EncodeToString(calcNewKey))
	}

	//Privacy key change test
	oldPrivKey := md.ExtendPrivKey(oldKey, engID, 110)
	newPrivKey := md.ExtendPrivKey(oldKey, engID, 110)
	privDelta := md.CalculatePrivDelta(oldPrivKey, newPrivKey, random, 110)
	calcNewPrivKey := md.CalculateNewPrivKey(oldPrivKey, append(random, privDelta...), 110)
	if !bytes.Equal(calcNewPrivKey, newPrivKey) {
		t.Errorf("MD5: Priv KeyChange Failure. Expected: %s. Got: %s.", hex.EncodeToString(newPrivKey), hex.EncodeToString(calcNewPrivKey))
	}
}

func TestSHAAuthenticationAlgorithm(t *testing.T) {
	//SHA1
	sh := NewSHA1()
	if sh.Algorithm() == "" || sh.OID() == "" {
		t.Error("SHA1: Algorithm/OID name cannot be empty.")
	}

	//Test password to key conversion and message digest generation
	for _, pk := range shaPKTest {
		secKey := sh.PasswordToKey(pk.pwd)
		if hex.EncodeToString(secKey) != strings.ToLower(pk.secKey) {
			t.Errorf("SHA1: Error in password to key conversion. Expected: %s. Got: %s.", pk.secKey, hex.EncodeToString(secKey))
		}
		engID, _ := hex.DecodeString(pk.engID)

		localKey := sh.LocalizeAuthKey(secKey, engID)
		if hex.EncodeToString(localKey) != strings.ToLower(pk.locSecKey) {
			t.Errorf("SHA1: Error in key localization. Expected: %s. Got: %s.", pk.locSecKey, hex.EncodeToString(localKey))
		}

		data := ""
		digest := sh.SignMsg([]byte(data), localKey)
		if hex.EncodeToString(digest) != strings.ToLower(pk.digest) {
			t.Errorf("SHA1: Error in digest generation. Expected: %s. Got: %s.", strings.ToLower(pk.digest), hex.EncodeToString(digest))
		}

		if !sh.VerifyMsg([]byte(data), localKey, digest) {
			t.Error("SHA1: Error in digest verification.")
		}
		invalidDigest := digest
		invalidDigest[1] = 123
		if sh.VerifyMsg([]byte(data), localKey, invalidDigest) {
			t.Error("SHA1: Error in digest verification when sending invalid digest.")
		}

		lpk := sh.LocalizePrivKey(secKey, engID, 32)
		if lpk == nil {
			lpk = sh.ExtendPrivKey(pk.pwd, engID, 32)
		}

		if hex.EncodeToString(lpk) != strings.ToLower(pk.locPrivKey) {
			t.Errorf("SHA1: Error in privacy key localization. Expected: %s. Got: %s.", strings.ToLower(pk.locPrivKey), hex.EncodeToString(lpk))
		}
	}

	//Key change test
	engID, _ := hex.DecodeString("000000000000000000000002")
	random, _ := hex.DecodeString("0000000000000000000000000000000000000000000000000000000000000000")
	oldKey := "maplesyrup"
	newKey := "newsyrup"

	//Auth key change test
	oldAuthKey := sh.LocalizeAuthKey(sh.PasswordToKey(oldKey), engID)
	newAuthKey := sh.LocalizeAuthKey(sh.PasswordToKey(newKey), engID)
	delta := sh.CalculateAuthDelta(oldAuthKey, newAuthKey, random)
	calcNewKey := sh.CalculateNewAuthKey(oldAuthKey, append(random, delta...))
	if !bytes.Equal(calcNewKey, newAuthKey) {
		t.Errorf("MD5: Auth KeyChange Failure. Expected: %s. Got: %s.", hex.EncodeToString(newAuthKey), hex.EncodeToString(calcNewKey))
	}

	//Privacy key change test
	oldPrivKey := sh.ExtendPrivKey(oldKey, engID, 110)
	newPrivKey := sh.ExtendPrivKey(oldKey, engID, 110)
	privDelta := sh.CalculatePrivDelta(oldPrivKey, newPrivKey, random, 110)
	calcNewPrivKey := sh.CalculateNewPrivKey(oldPrivKey, append(random, privDelta...), 110)
	if !bytes.Equal(calcNewPrivKey, newPrivKey) {
		t.Errorf("MD5: Priv KeyChange Failure. Expected: %s. Got: %s.", hex.EncodeToString(newPrivKey), hex.EncodeToString(calcNewPrivKey))
	}
}

//Test the privacy algorithms
var privAlgosTest = []struct {
	privAlgo USMPrivAlgorithm
	authAlgo USMAuthAlgorithm
}{
	{NewDES(eng), NewMD5()},
	{NewDES(eng), NewSHA1()},
	{NewTripleDES(eng), NewMD5()},
	{NewTripleDES(eng), NewSHA1()},
	{NewAES128(eng), NewMD5()},
	{NewAES128(eng), NewSHA1()},
	{NewAES192(eng), NewMD5()},
	{NewAES192(eng), NewSHA1()},
	{NewAES256(eng), NewMD5()},
	{NewAES256(eng), NewSHA1()},
}
var eng = engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 10)

func TestPrivacyAlgorithms(t *testing.T) {
	//Privacy Algorithms test
	data := []byte{48, 38, 2, 1, 0, 4, 6, 112, 117, 98, 108, 105, 99, 160, 25, 2, 1, 1, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0} //Valid ASN1 encoded byte
	engID := []byte{1, 2, 3, 0, 1, 1}
	pwd := "LengthyPasswordStringLengthyPasswordStringLengthyPasswordString"
	secParams := newUSMSecurityParams()
	secParams.setAuthoritativeEngineBoots(2147483647)
	secParams.setAuthoritativeEngineTime(2147483647)

	for _, algoTest := range privAlgosTest {
		secKey := algoTest.authAlgo.PasswordToKey(pwd)
		lpk := algoTest.authAlgo.LocalizePrivKey(secKey, engID, algoTest.privAlgo.KeySize())
		if lpk == nil {
			lpk = algoTest.authAlgo.ExtendPrivKey(pwd, engID, algoTest.privAlgo.KeySize())
		}
		cipher, salt, err := algoTest.privAlgo.Encrypt(data, lpk, secParams)

		decData, err := algoTest.privAlgo.Decrypt(cipher, lpk, salt, secParams)
		if err != nil {
			t.Errorf("%s: Decryption error. Error: %s", algoTest.privAlgo.Algorithm(), err.Error())
		} else if !bytes.Equal(decData, data) {
			t.Errorf("%s: Decrypted data is not equal to string encrypted.", algoTest.privAlgo.Algorithm())
		}
	}
}
