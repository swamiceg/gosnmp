/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

//Used to generate USM specific security error.
//It provides handler methods to construct StatusInformation for each of the security errors - UnknownEngineId, UnsupportedSecLevel etc.,
type usmErrorGenerator struct {
	usmModel *USMSecurityModel
}

func newUSMErrorGenerator(model *USMSecurityModel) *usmErrorGenerator {
	return &usmErrorGenerator{
		usmModel: model,
	}
}

func (errGen *usmErrorGenerator) genGeneralError(oid *snmpvar.SnmpOID,
	val snmpvar.SnmpVar,
	msgStr string,
	contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {

	statInfo := engine.NewStatusInformation(msgStr)
	if oid != nil && val != nil {
		varbList := make([]msg.SnmpVarBind, 1)
		varbList[0] = msg.NewSnmpVarBind(*oid, val)
		statInfo.SetVarbindList(varbList)
	}
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	//Only Report flag is used - can be 0 or 1
	//NoAuthNoPriv Level
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genUnknownEngineIdError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("UnknownEngineID")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsUnknownEngineIDs),
		snmpvar.NewSnmpCounter(errGen.usmModel.incUnknownEngineIdsCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//Only Report flag is used - can be 0 or 1
	//NoAuthNoPriv Level
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genUnknownUserNameError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("UnknownSecurityName")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsUnknownUserNames),
		snmpvar.NewSnmpCounter(errGen.usmModel.incUnknownUserNamesCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//Only Report flag is used - can be 0 or 1
	//NoAuthNoPriv Level
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genUnsupportedSecLevelError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("UnsupportedSecurityLevel")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsUnsupportedSecLevels),
		snmpvar.NewSnmpCounter(errGen.usmModel.incUnSupportedSecLevelsCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//Only Report flag is used - can be 0 or 1
	//NoAuthNoPriv Level
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genNotInTimeWindowError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("NotInTimeWindow")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsNotInTimeWindows),
		snmpvar.NewSnmpCounter(errGen.usmModel.incNotInTimeWindowsCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//AuthNoPriv sec level with report flag
	statInfo.SetMsgFlags(byte(security.AuthNoPriv) | (msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genWrongDigestsError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("AuthenticationFailure")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsWrongDigests),
		snmpvar.NewSnmpCounter(errGen.usmModel.incWrongDigestsCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//Only reportable flag is used
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}

func (errGen *usmErrorGenerator) genDecryptionError(contextEngineId []byte,
	contextName string,
	msgFlags byte,
	secParams *USMSecurityParameters,
) *engine.StatusInformation {
	statInfo := engine.NewStatusInformation("DecryptionError")
	statInfo.SetContextEngineID(contextEngineId)
	statInfo.SetContextName(contextName)

	varbList := make([]msg.SnmpVarBind, 1)
	varbList[0] = msg.NewSnmpVarBind(*snmpvar.NewSnmpOID(usmStatsDecryptionErrors),
		snmpvar.NewSnmpCounter(errGen.usmModel.incDecryptionErrorsCounter()),
	)
	statInfo.SetVarbindList(varbList)
	secParamsBytes, _ := secParams.EncodeSecurityParameters()
	statInfo.SetSecurityParams(secParamsBytes)

	//Only reportable flag is used
	statInfo.SetMsgFlags((msgFlags & security.ReportableFlag))

	return statInfo
}
