/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package usm provides all security related operations and protocols required for processing of SNMPv3 messages.
User-based security model (USM) is implemented as per RFC3414.

This security model is part of the security subsystem in the engine architecture.

User based security model provides the following services,
  * Authentication Service: Provides default implementaion of various authentication protocols to authenticate the outgoing messages and to check the authenticity of incoming messages.
  * Privacy Service: Provides default implementation of various privacy protocols to encrypt the outgoing messages and to perform decryption of incoming messages.
  * Timeliness Service: Perform timeliness checks on the incoming and outgoing messages based on the timeliness values in the SNMPv3 message such as EngineBoots, EngineTime.

This package provides implementation for following Authentication Protocols,
  * MD5
  * SHA1

This package provides implementation for following Privacy Protocols,
  * DES
  * 3-DES
  * AES - 128/192/256

User can also implement their own authentication and privacy protocols. Refer USMAuthAlgorithm and USMPrivAlgorithm interface.
*/
package usm
