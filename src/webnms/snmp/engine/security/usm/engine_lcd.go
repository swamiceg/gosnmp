/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"strconv"
	"time"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/msg"
)

//////////////################### USM - Peer Engines ########################///////////////////////////

/*
 *	USMPeerEngine
 */

//USMPeerEngine stores the details of the remote peer engine specific to USM.
//Entry should be created for each remote SNMP entity we would like to communicate. Entity is uniquely identified by address and port.
//PeerEngine stores the remote entity's address, port, engineID, engineBoots etc.,
//
//Implements engine.SnmpPeerEngine interface.
type USMPeerEngine struct {
	authoritativeEngineId    []byte
	authoritativeEngineBoots int32
	authoritativeEngineTime  int32
	startTime                int64
	latestReceivedEngTime    int32 //Similar to authoritative engine time

	remoteHost string
	remotePort int
}

//NewUSMPeerEngine creates and returns a new USM PeerEngine with the address and port.
//Address and port uniquely identifies the remote SNMP entity.
//
//Returns nil, if address is not a valid host IP address (or) port number is invalid.
func NewUSMPeerEngine(address string, port int) *USMPeerEngine {
	//Address and Port will be used as a key for PeerEngineLCD.
	//Since EngineID can be same for different entities, so we're not using EngineID as primary key.

	ipAddr, err := net.ResolveIPAddr("ip", address)
	if err != nil {
		return nil //Invalid Remote Address
	}
	if port < 0 || port > 65535 {
		return nil //Invalid port number
	}

	peerEngine := new(USMPeerEngine)
	peerEngine.remoteHost = ipAddr.String()
	peerEngine.remotePort = port

	return peerEngine
}

//DiscoverEngineID performs v3 discovery to learn the remote authoritative SNMP Engine's SnmpEngineID.
//It synchronously sends the discovery message with 'initial' as UserName at NoAuthNoPriv security level as per RFC3414 - Section 4.
//It would automatically add this PeerEngine entry in the LCD as part of the API architecture.
//
//In case of failure, it returns nil (or) old EngineID if one exist.
//
//  Note: It performs discovery even if the EngineID already exist for the peer.
func (peer *USMPeerEngine) DiscoverEngineID(dispatcher engine.Dispatcher) []byte {
	if peer.authoritativeEngineId != nil {
		log.Info("EngineID already exist for the entity: %s. Performing re-discovery for this entity." +
			peer.remoteHost + ":" + strconv.Itoa(peer.remotePort))
		//return peer.authoritativeEngineId //If the remote EngineID changes??
	}

	//Obtain the EngineLCD instance
	var usmEngineLCD *USMPeerEngineLCD
	secSS := dispatcher.SnmpEngine().SecuritySubSystem()
	engineLCD := secSS.PeerEngineLCD(security.USMID)
	if engineLCD != nil {
		usmEngineLCD = engineLCD.(*USMPeerEngineLCD)
	}

	oldEngineID := peer.authoritativeEngineId

	//Construct Discovery Message
	discMsg := msg.NewSnmpMessage()
	discMsg.SetVersion(consts.Version3)
	discMsg.SetCommand(consts.GetRequest)
	discMsg.SetUserName("initial") //Discovery User
	discMsg.SetSecurityLevel(security.NoAuthNoPriv)

	//We should use the PeerEngine's RemoteHost/RemotePort
	if dispatcher.ProtocolOptions() != nil {
		protocolOptions := dispatcher.ProtocolOptions().Copy()
		protocolOptions.SetRemoteHost(peer.remoteHost)
		protocolOptions.SetRemotePort(peer.remotePort)

		discMsg.SetProtocolOptions(protocolOptions)
	}
	discResp, err := dispatcher.SyncSend(discMsg)
	if err != nil {
		log.Error("Failure in EngineID discovery. Error: %v", err)
		//Shall we remove the entry from EngineLCD if it has been created - Additional Check.
		usmEngineLCD.RemoveEngine(peer.remoteHost, peer.remotePort)

		return oldEngineID //errors.New("Unable to initialize Engine LCD: " + err.Error())
	}

	if discResp.Command() == consts.ReportMessage {
		reportVarb := discResp.VarBinds()
		if reportVarb != nil {
			reportOID := reportVarb[0].ObjectID().String()
			if reportOID == ".1.3.6.1.6.3.15.1.1.4.0" { //Unknown EngineID
				//Success
				log.Info("EngineID is successfully discovered for the entity: %s. EngineID: %s",
					peer.remoteHost+":"+strconv.Itoa(peer.remotePort), string(peer.authoritativeEngineId))

				if peerEng := usmEngineLCD.Engine(peer.remoteHost,
					peer.remotePort,
				); peerEng != nil {
					peer.authoritativeEngineId = peerEng.AuthoritativeEngineID()
					peer.authoritativeEngineBoots = peerEng.AuthoritativeEngineBoots()
					peer.authoritativeEngineTime = peerEng.AuthoritativeEngineTime()
					return peerEng.AuthoritativeEngineID() //Updated in USM Module
				}
			}
		}
	}

	return oldEngineID
}

func (peer *USMPeerEngine) key() string {
	//We will use UMSPeerEngineLCD getKey() method to generate a key
	return new(USMPeerEngineLCD).getKey(peer.remoteHost, peer.remotePort)
}

//AuthoritativeEngineID returns remote peer's SnmpEngineID.
//Returns nil in case EngineID is not yet discovered for this peer.
//
//Refer DiscoverEngineID method to learn peer's EngineID.
func (peer *USMPeerEngine) AuthoritativeEngineID() []byte {
	return peer.authoritativeEngineId
}

//SetAuthoritativeEngineID sets the remote peer's SnmpEngineID.
func (peer *USMPeerEngine) SetAuthoritativeEngineID(engineID []byte) {
	if engineID != nil {
		peer.authoritativeEngineId = engineID
	}
}

//AuthoritativeEngineBoots returns the EngineBoots value of the remote peer.
func (peer *USMPeerEngine) AuthoritativeEngineBoots() int32 {
	return peer.authoritativeEngineBoots
}

//SetAuthoritativeEngineBoots sets the EngineBoots value for the remote peer.
func (peer *USMPeerEngine) SetAuthoritativeEngineBoots(boots int32) { //Do we need to reset the EngineTime??
	if boots < 0 { //Negative value indicates that the boot has exceeded 2147483647. Hence reset to 0.
		peer.authoritativeEngineBoots = 0
	} else {
		peer.authoritativeEngineBoots = boots
	}
}

//AuthoritativeEngineTime returns the EngineTime value of the remote peer in seconds.
//
//This is the time since last reboot. It returns the updated time value.
func (peer *USMPeerEngine) AuthoritativeEngineTime() int32 { //Return an updated engine time for this auth engine entry
	if peer.authoritativeEngineTime == 0 {
		return 0
	}
	timeElapsed := (time.Now().UnixNano() / 1e9) - peer.startTime
	et := peer.authoritativeEngineTime
	curTime := (et + int32(timeElapsed&0x7FFFFFFF)) & 0x7FFFFFFF

	if timeElapsed > 0x7FFFFFFF || (curTime < et) { //That's really great. It reached a max value of '68' years.
		//We should update engineBoots and engineTime
		peer.SetAuthoritativeEngineTime(curTime)
		peer.SetAuthoritativeEngineBoots(peer.authoritativeEngineBoots + 1)
	}

	return curTime //ActualTime + Running Time
}

//SetAuthoritativeEngineTime sets the EngineTime value for the remote peer.
func (peer *USMPeerEngine) SetAuthoritativeEngineTime(timeVal int32) {
	if timeVal >= 0 && timeVal <= 0x7FFFFFFF {
		peer.startTime = (time.Now().UnixNano() / 1e9) //To calculate the running time of auth engine
		peer.authoritativeEngineTime = timeVal
	}
}

//LatestReceivedEngineTime returns the highest value of snmpEngineTime that was received from this authoritative peer engine.
func (peer *USMPeerEngine) LatestReceivedEngineTime() int32 {
	return peer.latestReceivedEngTime
}

//SetLatestReceivedEngineTime sets the last received engine time.
//For internal use only.
func (peer *USMPeerEngine) SetLatestReceivedEngineTime(latestReceived int32) {
	peer.latestReceivedEngTime = latestReceived
}

//RemoteHost returns the host address of the remote SNMP entity.
func (peer *USMPeerEngine) RemoteHost() string {
	return peer.remoteHost
}

//RemotePort returns the port number of the remote SNMP entity.
func (peer *USMPeerEngine) RemotePort() int {
	return peer.remotePort
}

/*
 *	USMPeerEngineLCD
 */

//USMPeerEngineLCD holds a list of remote SNMP Engine entries as USMPeerEngine. This LCD provide methods to add/remove/update/retrieve
//PeerEngines instance at ease.
//
//API User need not instantiate this type, they can obtain the instance from the SnmpAPI's USMPeerEngineLCD (or) from different High-Level APIs such
//as SnmpTarget, SnmpRequestServer.
//
//Implements engine.SnmpPeerEngineLCD interface.
type USMPeerEngineLCD struct {
	peerEngines map[string]*USMPeerEngine
	localEngine engine.SnmpEngine
}

//Should be initialized by USM
func newUSMPeerEngineLCD(engine engine.SnmpEngine) *USMPeerEngineLCD {
	return &USMPeerEngineLCD{
		peerEngines: make(map[string]*USMPeerEngine),
		localEngine: engine,
	}
}

//AddEngine adds the SnmpPeerEngine instance on this lcd.
func (pel *USMPeerEngineLCD) AddEngine(engine engine.SnmpPeerEngine) error {
	if engine == nil {
		return errors.New("Nil engine instance passed. Cannot add entry.")
	}
	dbImpl := pel.localEngine.DBOperations()

	//We deal only with USMEnginePeers
	if usmEngine, ok := engine.(*USMPeerEngine); ok {
		entry := pel.Engine(usmEngine.RemoteHost(), usmEngine.RemotePort())
		if entry == nil {
			//No entry exist with the address, port combination. Hence add new entry
			if dbImpl == nil {
				pel.peerEngines[usmEngine.key()] = usmEngine
				log.Debug("Successfully added the Engine Entry in LCD. RemoteHost: %s; RemotePort: %d", usmEngine.RemoteHost(), usmEngine.RemotePort())
				return nil //Successfully added
			} else {
				//Insert the Engine entry in database
				err := dbImpl.InsertEngineEntry(getIPFromHost(usmEngine.RemoteHost()), //Inserting dotted decimal IP address
					usmEngine.RemotePort(),
					getIPFromHost(usmEngine.RemoteHost())+":"+strconv.Itoa(usmEngine.RemotePort()), //EngineName = Host:Port
					(usmEngine.AuthoritativeEngineID()),
					usmEngine.AuthoritativeEngineTime(),
					usmEngine.AuthoritativeEngineBoots(),
					usmEngine.startTime,
					usmEngine.LatestReceivedEngineTime(),
				)
				if err != nil {
					log.Debug("Failure in adding the Engine Entry to DB. RemoteHost: %s; RemotePort: %d.\nError: %s.", usmEngine.RemoteHost(), usmEngine.RemotePort(), err.Error())
					return err
				}
				log.Debug("Successfully added the Engine Entry in DB. RemoteHost: %s; RemotePort: %d", usmEngine.RemoteHost(), usmEngine.RemotePort())

				return nil
			}
		} else {
			//Entry exist with given address, port. Hence Update the entry.
			if dbImpl == nil {
				pel.peerEngines[usmEngine.key()] = usmEngine
				log.Debug("Successfully updated the Engine Entry in LCD. RemoteHost: %s; RemotePort: %d", usmEngine.RemoteHost(), usmEngine.RemotePort())
				return nil //Successfully added
			} else {
				//Update the engine entry in DB
				err := dbImpl.UpdateEngineEntry(getIPFromHost(usmEngine.RemoteHost()),
					usmEngine.RemotePort(),
					getIPFromHost(usmEngine.RemoteHost())+":"+strconv.Itoa(usmEngine.RemotePort()), //EngineName = Host:Port
					(usmEngine.AuthoritativeEngineID()),
					usmEngine.AuthoritativeEngineTime(),
					usmEngine.AuthoritativeEngineBoots(),
					usmEngine.startTime,
					usmEngine.LatestReceivedEngineTime(),
				)
				if err != nil {
					log.Error("Failure in updating the Engine Entry in DB. RemoteHost: %s; RemotePort: %d.\nError: %s.", usmEngine.RemoteHost(), usmEngine.RemotePort(), err.Error())
					return err
				}
				log.Debug("Successfully updated the Engine Entry in DB. RemoteHost: %s; RemotePort: %d", usmEngine.RemoteHost(), usmEngine.RemotePort())
				return nil
			}
		}

	}

	return errors.New("Unable to add the engine entry: SnmpPeerEngine should be of type pointer to USMPeerEngine.")
}

//RemoveEngine removes the SnmpPeerEngine instance from the lcd based on the remote entity's address and port.
func (pel *USMPeerEngineLCD) RemoveEngine(address string, port int) error {
	key := pel.getKey(address, port)
	if key == "" {
		//log
		return fmt.Errorf("Address: '%s' provided is not an valid IP address. Cannot Remove Engine.", address)
	}

	var ok bool

	dbImpl := pel.localEngine.DBOperations()
	if dbImpl == nil {
		if _, ok = pel.peerEngines[key]; ok {
			log.Debug("Successfully removed the engine entry from LCD. RemoteHost: %s; RemotePort: %d.", address, port)
			delete(pel.peerEngines, key)
			return nil
		}
		errStr := fmt.Sprintf("No entry found in LCD with RemoteHost: %s; RemotePort: %d;", address, port)
		log.Error(errStr)
		return errors.New(errStr)
	} else {
		//Delete the engine entry from DB
		if err := dbImpl.DeleteEngineEntry(getIPFromHost(address),
			port,
		); err != nil {
			errStr := fmt.Sprintf("Failure in deleting the Engine Entry from DB. RemoteHost: %s; RemotePort: %d.\nError: %s.", address, port, err.Error())
			log.Error(errStr)
			return errors.New(errStr)
		}
		log.Debug("Successfully removed the engine entry from DB. RemoteHost: %s; RemotePort: %d.", address, port)
	}

	return nil
}

//RemoveEngineByEntry removes the peer engine based on the SnmpPeerEngine instance passed.
func (pel *USMPeerEngineLCD) RemoveEngineByEntry(engineEntry engine.SnmpPeerEngine) {
	//We deal only with USMEnginePeers
	if _, ok := engineEntry.(*USMPeerEngine); !ok {
		//log.Error("Unsupported type")
		return
	}

	dbImpl := pel.localEngine.DBOperations()
	if dbImpl == nil {
		entryKey := engineEntry.(*USMPeerEngine).key()
		for key, peerEngine := range pel.peerEngines {
			if (peerEngine == engineEntry) ||
				(entryKey == key) {
				log.Debug("Successfully removed the engine entry from LCD. RemoteHost: %s; RemotePort: %d.", engineEntry.(*USMPeerEngine).RemoteHost(), engineEntry.(*USMPeerEngine).RemotePort())
				delete(pel.peerEngines, key)
				return
			}
		}
		log.Error("No entry found in LCD with RemoteHost: %s; RemotePort: %d;", engineEntry.(*USMPeerEngine).RemoteHost(), engineEntry.(*USMPeerEngine).RemotePort())
	} else {
		//Delete the engine entry from DB
		if err := dbImpl.DeleteEngineEntry(getIPFromHost(engineEntry.(*USMPeerEngine).RemoteHost()),
			engineEntry.(*USMPeerEngine).RemotePort(),
		); err != nil {
			errStr := fmt.Sprintf("Failure in deleting the Engine Entry from DB. RemoteHost: %s; RemotePort: %d.\nError: %s.", engineEntry.(*USMPeerEngine).RemoteHost(), engineEntry.(*USMPeerEngine).RemotePort(), err.Error())
			log.Error(errStr)
			return
		}
		log.Debug("Successfully removed the engine entry from DB. RemoteHost: %s; RemotePort: %d.", engineEntry.(*USMPeerEngine).RemoteHost(), engineEntry.(*USMPeerEngine).RemotePort())
	}
}

//RemoveAllEngines deletes all the engine entries from engine lcd.
func (pel *USMPeerEngineLCD) RemoveAllEngines() {
	dbImpl := pel.localEngine.DBOperations()
	if dbImpl == nil {
		for key, _ := range pel.peerEngines {
			delete(pel.peerEngines, key)
		}
		log.Debug("Succesfully removed all the engine entries from LCD.")
	} else {
		//DBImpl is not nil, we should delete the row from DB.
		err := dbImpl.DeleteEngineEntries()
		if err != nil {
			log.Error("Failure in deleting the engine entries from DB. Error: %s.", err.Error())
			return
		}
		log.Debug("Succesfully removed all the engine entries from DB.")
	}

	return
}

//Clear removes all the engine entries from engine lcd.
func (pel *USMPeerEngineLCD) Clear() {
	pel.RemoveAllEngines()
}

//Engine returns the SnmpPeerEngine instance based on the address and port passed.
func (pel *USMPeerEngineLCD) Engine(address string, port int) engine.SnmpPeerEngine {
	key := pel.getKey(address, port)
	if key == "" {
		//log.Error("Invalid address")
		return nil
	}

	dbImpl := pel.localEngine.DBOperations()
	if dbImpl == nil {
		if usmEngine, ok := pel.peerEngines[key]; ok {
			return usmEngine
		}
	} else {
		//DBImpl is not nil, we should get the row from DB.
		engEntry, err := dbImpl.EngineEntry(getIPFromHost(address), //Let's pass the dotted decimal IP
			port,
		)
		if err != nil {
			log.Error("Error in getting the EngineEntry from DB. RemoteHost: %s; RemotePort: %d.\nError: %s.", address, port, err)
			return nil
		}
		//Copy the values from EngineEntry
		peerEngine := new(USMPeerEngine)
		peerEngine.copy(engEntry)

		return peerEngine
	}

	return nil
}

//EngineByID returns SnmpPeerEngine instance based on the engineID passed.
func (pel *USMPeerEngineLCD) EngineByID(engineId []byte) engine.SnmpPeerEngine {
	dbImpl := pel.localEngine.DBOperations()
	if dbImpl == nil {
		for _, peerEngine := range pel.peerEngines {
			if bytes.Equal(peerEngine.AuthoritativeEngineID(), engineId) {
				return peerEngine
			}
		}
	} else {
		engEntrySlice, err := dbImpl.EngineEntriesByID((engineId))
		if err != nil {
			log.Error("Error in getting the EngineEntry from DB. EngineID: %v.\nError: %s.", engineId, err)
			return nil
		}

		//Copy the values from EngineEntry
		peerEngine := new(USMPeerEngine)
		peerEngine.copy(engEntrySlice[0]) //Just take the first row. There is a possibility of multiple

		return peerEngine
	}

	return nil
}

//Engines returns a slice of all SnmpPeerEngine instances stored on this lcd.
func (pel *USMPeerEngineLCD) Engines() []engine.SnmpPeerEngine {
	var peerEngineSlice []engine.SnmpPeerEngine
	dbImpl := pel.localEngine.DBOperations()

	if dbImpl == nil {
		for _, engine := range pel.peerEngines {
			peerEngineSlice = append(peerEngineSlice, engine)
		}
	} else {
		//Get the entries from database
		engEntrySlice, err := dbImpl.EngineEntries()
		if err != nil {
			log.Error("Failure in obtaining the Engine Entries from DB. Error: " + err.Error())
			return nil
		}

		var usmPeerEngine *USMPeerEngine
		for _, engEntry := range engEntrySlice { //Copy the values from EngineEntry slice
			usmPeerEngine = new(USMPeerEngine)
			usmPeerEngine.copy(engEntry)
			peerEngineSlice = append(peerEngineSlice, usmPeerEngine)
		}
	}

	return peerEngineSlice
}

func getIPFromHost(host string) (addr string) {
	if host != "" {
		ipAddr, err := net.ResolveIPAddr("ip", host)
		if err != nil {
			return
		}
		addr = ipAddr.String()
	}
	return
}

//Genrate a key based on remote address and port.
//Should be unique.
func (pel *USMPeerEngineLCD) getKey(address string, port int) (key string) {
	if address != "" {
		ipAddr, err := net.ResolveIPAddr("ip", address)
		if err != nil {
			return
		}
		address = ipAddr.String()
		key = address + "##" + strconv.Itoa(port)
	}

	return
}

//To copy the values from EngineEntry (DB)
func (peer *USMPeerEngine) copy(entry db.EngineEntry) {
	peer.remoteHost = entry.Host
	peer.remotePort = entry.Port
	peer.authoritativeEngineId = []byte(entry.EngineId)
	peer.authoritativeEngineBoots = entry.EngineBoots
	peer.authoritativeEngineTime = entry.EngineTime
	peer.startTime = entry.StartTime
	peer.latestReceivedEngTime = entry.LatestReceivedEngineTime
}
