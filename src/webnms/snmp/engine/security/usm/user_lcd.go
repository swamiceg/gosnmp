/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/db"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

/*
 * USMSecureUser
 */

//USMSecureUser holds the details of an USM user on whose behalf management operations are authorized.
//It contains attributes defined in RFC3414 - Section 2.1 such as UserName, authProtocol, authKey, privProtocol etc.,
//It also contains localized keys and routines for performing Time-Synchronization with the remote entity.
//Also, it provides methods for performing key changes securely with the remote entity.
//
//UserName + EngineID uniquely identifies an USM user. EngineID can be obtained by performing discovery with the remote engine.
//Refer SnmpPeerEngine interface in order to perform discovery with remote engine.
//
//User can set the authentication and privacy passwords on the user. API user need not perform password_to_key conversion directly, instead while
//adding this entry to USMUserLCD it will perform the necessary password_to_key translation.
//
//User can also directly set the AuthKey/PrivKey by performing password_to_key translation themselves.
//Refer USMAuthAlgorithm and USMPrivAlgorithm interface for more details.
type USMSecureUser interface {
	//UserName returns the username string associated with this user.
	UserName() string

	//Sets the username for this user.
	SetUserName(string)

	//EngineID returns the remote entity's SnmpEngineID.
	EngineID() []byte

	//SetEngineID sets the remote peer's EngineID for this user.
	SetEngineID([]byte)

	//Returns the security level of the user.
	//
	//'0' indicates NoAuthNoPriv, '1' indicates AuthNoPriv and '3' indicates AuthPriv security level.
	SecurityLevel() consts.SecurityLevel

	//Returns the Peer Engine Entry associated with the User
	//PeerEngine() *USMPeerEngine

	//Achieves time sync with the remote host. This method is synchronous and blocks.
	//The timeSync request is sent over the dispatcher instance (SnmpSession) passed as a parameter.
	//Once the response is received, the engineBoots and engineTime is updated for the associated engine.
	//
	//Returns error in case of failure in time-synchronization.
	TimeSynchronize(engine.Dispatcher, transport.ProtocolOptions) error

	//AuthProtocol returns USMAuthAlgorithm instance associated with this user.
	AuthProtocol() USMAuthAlgorithm

	//AuthAlgorithm returns the name of the authentication algorithm associated with this user.
	AuthAlgorithm() string

	//SetAuthProtocol sets the USMAuthAlgorithm instance for this user.
	SetAuthProtocol(USMAuthAlgorithm)

	//SetAuthAlgorithm sets the USMAuthAlgorithm instance for this user based on the algorithm name.
	//Algorithm with this name have to be already registered with the algorithm manager.
	SetAuthAlgorithm(consts.AuthProtocol)

	//AuthKey returns the localized authentication key (kul) for the user.
	AuthKey() []byte //Return localized authkey

	//SetAuthKey sets the localized authentication key for the user.
	SetAuthKey([]byte)

	//PrivProtocol returns USMPrivAlgorithm instance associated with this user.
	PrivProtocol() USMPrivAlgorithm

	//PrivAlgorithm returns the name of the privacy algorithm associated with this user.
	PrivAlgorithm() string

	//SetPrivProtocol sets the USMPrivAlgorithm instance for this user.
	SetPrivProtocol(USMPrivAlgorithm)

	//SetPrivAlgorithm sets the USMPrivAlgorithm instance for this user based on the algorithm name.
	//Algorithm with this name have to be already registered with the algorithm manager.
	SetPrivAlgorithm(consts.PrivProtocol)

	//PrivKey returns the localized privacy key (kul) for the user.
	PrivKey() []byte

	//SetPrivKey sets the localized privacy key for the user.
	SetPrivKey([]byte)

	//AuthDelta calculates and returns the delta based on the newKey and random data.
	//Delta can be sent to the remote entity to change the keys remotely.
	AuthDelta(newKey, random []byte) []byte

	//PrivDelta calculates and returns the delta based on the newKey and random data.
	//Delta can be sent to the remote entity to change the keys remotely.
	PrivDelta(newKey, random []byte) []byte

	//SetAuthKeyChange changes the authentication key based on the random delta value that comes from remote configuration.
	SetAuthKeyChange(randomDelta []byte)

	//SetPrivKeyChange changes the privacy key based on the random delta value that comes from remote configuration.
	SetPrivKeyChange(randomDelta []byte)
}

//USMSecureUserImpl is the default implementation of USMSecureUser interface.
type USMSecureUserImpl struct {
	lcd          USMUserLCD
	userName     string
	engineId     []byte
	authProtocol USMAuthAlgorithm //Can be given as AuthPair (Algorithm + Key pair)
	authKey      []byte
	privProtocol USMPrivAlgorithm //Can be given as PrivPair (Algorithm + Key pair)
	privKey      []byte

	//For internal use
	authPasswd string
	privPasswd string
}

//NewUSMSecureUser creates and returns a new USM secure user using the username and engineId.
//engineId can be learnt from the discovery process.
//
//Refer USMPeerEngine.DiscoverEngineID method for more details.
func NewUSMSecureUser(usmLCD USMUserLCD,
	userName string,
	engineId []byte,
) *USMSecureUserImpl {
	return NewUSMSecureUserImpl(usmLCD,
		userName,
		engineId,
		nil,
		nil,
		nil,
		nil,
	)
}

//NewUSMSecureUser creates and returns a new USM secure user using the username, engineId and other data provided.
//engineId can be learnt from the discovery process.
//
//Refer USMPeerEngine.DiscoverEngineID method for more details.
func NewUSMSecureUserImpl(usmLCD USMUserLCD,
	userName string,
	engineId []byte,
	authProtocol USMAuthAlgorithm,
	authKey []byte,
	privProtocol USMPrivAlgorithm,
	privKey []byte) *USMSecureUserImpl {
	secureUser := &USMSecureUserImpl{
		lcd:          usmLCD,
		userName:     userName,
		engineId:     engineId,
		authProtocol: authProtocol,
		authKey:      authKey,
		privProtocol: privProtocol,
		privKey:      privKey,
	}

	return secureUser
}

//UserName returns the username string associated with this user.
func (user *USMSecureUserImpl) UserName() string {
	return user.userName
}

//Sets the username for this user.
func (user *USMSecureUserImpl) SetUserName(userName string) {
	user.userName = userName
}

//EngineID returns the remote entity's SnmpEngineID to which this user is associated with.
func (user *USMSecureUserImpl) EngineID() []byte {
	return user.engineId
}

//SetEngineID sets the remote peer's EngineID for this user.
func (user *USMSecureUserImpl) SetEngineID(engineId []byte) {
	user.engineId = engineId
}

/*func (user *USMSecureUserImpl) PeerEngine() *USMPeerEngine {
	if user.engineId != nil {

	}

	return nil
}*/

//AuthProtocol returns USMAuthAlgorithm instance associated with this user.
func (user *USMSecureUserImpl) AuthProtocol() USMAuthAlgorithm {
	return user.authProtocol
}

//AuthAlgorithm returns the name of the authentication algorithm associated with this user.
func (user *USMSecureUserImpl) AuthAlgorithm() string {
	if user.authProtocol != nil {
		return user.authProtocol.Algorithm()
	}

	return "<usmNoAuthProtocol>"
}

//SetAuthProtocol sets the USMAuthAlgorithm instance for this user.
func (user *USMSecureUserImpl) SetAuthProtocol(auth USMAuthAlgorithm) {
	user.authProtocol = auth
}

//SetAuthAlgorithm sets the USMAuthAlgorithm instance for this user based on the algorithm name.
//Algorithm with this name have to be already registered with the algorithm manager.
func (user *USMSecureUserImpl) SetAuthAlgorithm(algoName consts.AuthProtocol) { //Can be algorithm name (or) oid string
	if algoName == NO_AUTH {
		user.authProtocol = nil
		user.authKey = nil
	}
	authAlgo := user.lcd.AlgorithmManager().Algorithm(string(algoName)) //We should provide provision to search by 'Name' and 'OID'
	if authAlgo != nil {
		if _, ok := authAlgo.(USMAuthAlgorithm); ok {
			user.authProtocol = authAlgo.(USMAuthAlgorithm)
		}
	}
}

//AuthKey returns the localized authentication key (kul) for the user.
func (user *USMSecureUserImpl) AuthKey() []byte { //Localized privacy key
	return user.authKey
}

//SetAuthKey sets the localized authentication key for the user.
func (user *USMSecureUserImpl) SetAuthKey(authKey []byte) { //Localized privacy key
	user.authKey = authKey
}

//PrivProtocol returns USMPrivAlgorithm instance associated with this user.
func (user *USMSecureUserImpl) PrivProtocol() USMPrivAlgorithm {
	return user.privProtocol
}

//PrivAlgorithm returns the name of the privacy algorithm associated with this user.
func (user *USMSecureUserImpl) PrivAlgorithm() string {
	if user.privProtocol != nil {
		return user.privProtocol.Algorithm()
	}
	return "<usmNoPrivProtocol>"
}

//SetPrivProtocol sets the USMPrivAlgorithm instance for this user.
func (user *USMSecureUserImpl) SetPrivProtocol(priv USMPrivAlgorithm) {
	user.privProtocol = priv
}

//SetPrivAlgorithm sets the USMPrivAlgorithm instance for this user based on the algorithm name.
//Algorithm with this name have to be already registered with the algorithm manager.
func (user *USMSecureUserImpl) SetPrivAlgorithm(algoName consts.PrivProtocol) { //Can be algorithm name (or) oid string
	if algoName == NO_PRIV {
		user.privProtocol = nil
		user.privKey = nil
	}
	privAlgo := user.lcd.AlgorithmManager().Algorithm(string(algoName)) //So we should provide provision to search by 'Name' and 'OID'
	if privAlgo != nil {
		if _, ok := privAlgo.(USMPrivAlgorithm); ok {
			user.privProtocol = privAlgo.(USMPrivAlgorithm)
		}
	}
}

//PrivKey returns the localized privacy key (kul) for the user.
func (user *USMSecureUserImpl) PrivKey() []byte { //Localized privacy key
	return user.privKey
}

//SetPrivKey sets the localized privacy key for the user.
func (user *USMSecureUserImpl) SetPrivKey(privKey []byte) { //Localized privacy key
	user.privKey = privKey
}

//Returns the security level of the user.
//
//'0' indicates NoAuthNoPriv, '1' indicates AuthNoPriv and '3' indicates AuthPriv security level.
func (user *USMSecureUserImpl) SecurityLevel() consts.SecurityLevel {
	var level byte = security.NoAuthNoPrivMask //NoAuthNoPriv

	if user.AuthProtocol() != nil &&
		user.AuthProtocol().OID() != usmNoAuthProtocol &&
		user.AuthKey() != nil {
		level |= security.AuthMask

		if user.PrivProtocol() != nil &&
			user.PrivProtocol().OID() != usmNoPrivProtocol &&
			user.PrivKey() != nil {
			level |= security.PrivMask
		}
	}

	return consts.SecurityLevel(level)
}

//AuthDelta calculates and returns the delta based on the newKey and random data.
//Delta can be sent to the remote entity to change the keys remotely.
func (user *USMSecureUserImpl) AuthDelta(newKey, random []byte) []byte {
	if user.authProtocol == nil {
		return nil
	}
	return user.authProtocol.CalculateAuthDelta(user.authKey, newKey, random)
}

//PrivDelta calculates and returns the delta based on the newKey and random data.
//Delta can be sent to the remote entity to change the keys remotely.
func (user *USMSecureUserImpl) PrivDelta(newKey, random []byte) []byte {
	if user.authProtocol == nil || user.privProtocol == nil {
		return nil
	}
	return user.authProtocol.CalculatePrivDelta(user.privKey,
		newKey,
		random,
		user.privProtocol.DeltaSize(),
	)
}

//SetAuthKeyChange changes the authentication key based on the random delta value that comes from remote configuration.
func (user *USMSecureUserImpl) SetAuthKeyChange(randomDelta []byte) {
	if user.authProtocol == nil {
		return
	}

	user.authKey = user.authProtocol.CalculateNewAuthKey(user.authKey, randomDelta)
}

//SetPrivKeyChange changes the privacy key based on the random delta value that comes from remote configuration.
func (user *USMSecureUserImpl) SetPrivKeyChange(randomDelta []byte) {
	if user.authProtocol == nil || user.privProtocol == nil {
		return
	}

	user.privKey = user.authProtocol.CalculateNewPrivKey(user.privKey,
		randomDelta,
		user.privProtocol.DeltaSize(),
	)
}

//Achieves time sync with the remote host. This method is synchronous and blocks.
//The timeSync request is sent over the dispatcher instance (SnmpSession) passed as a parameter.
//Once the response is received, the engineBoots and engineTime is updated for the associated engine.
//
//Returns error in case of failure in time-synchronization.
func (user *USMSecureUserImpl) TimeSynchronize(dispatcher engine.Dispatcher, protocolOptions transport.ProtocolOptions) error {

	//Let's perform some check first
	if dispatcher == nil {
		return errors.New("Time-Sync Failure: Dispatcher instance is nil.")
	}

	if protocolOptions == nil {
		return errors.New("Time-Sync Failure:: ProtocolOptions instance is nil.")
	}

	if user.SecurityLevel() < security.AuthNoPriv {
		return errors.New("Time-Sync Failure: Time-Synchronization is not required for user with security level NoAuthNoPriv.")
	}

	//Get the USM's Peer EngineLCD
	localEngine := dispatcher.SnmpEngine()
	secSS := localEngine.SecuritySubSystem()
	engineLCD := secSS.PeerEngineLCD(security.USMID)

	//Let's perform a EngineID check - Additional Check
	peerEngine := engineLCD.Engine(protocolOptions.RemoteHost(),
		protocolOptions.RemotePort(),
	)
	if peerEngine == nil {
		return fmt.Errorf("Time-Sync Failure: No Engine Entry found for host: '%s:%d'", protocolOptions.RemoteHost(), protocolOptions.RemotePort())
	}
	//else if !bytes.Equal(peerEngine.AuthoritativeEngineID(), transportPeerEngine.AuthoritativeEngineID()) {
	//		//This should not be possible. Protocol
	//		return errors.New("Cannot Perform Time-Synchronization: EngineID mismatch. User maynot be associated with this remote entity.")
	//	}

	//Let's prepare Time-Sync Message
	timeSyncMsg := msg.NewSnmpMessage()
	timeSyncMsg.SetVersion(consts.Version3)
	timeSyncMsg.SetCommand(consts.GetRequest)
	timeSyncMsg.SetSecurityLevel(security.AuthNoPriv)
	timeSyncMsg.SetUserName(user.UserName()) //Should be a valid UserName
	timeSyncMsg.SetProtocolOptions(protocolOptions)

	timeSyncResp, err := dispatcher.SyncSend(timeSyncMsg)
	if err != nil {
		//Time_Sync Failure
		return errors.New("Time-Sync Failure: " + err.Error()) //Timeout error could have occurred.
	}

	//Decode Security Parameters
	secParams := timeSyncResp.MsgSecurityParameters()
	usmSecParams := newUSMSecurityParams()
	err = usmSecParams.DecodeSecurityParameters(secParams)
	if err != nil {
		//Internal Error. This should not happen.
	}

	//We have received a report message
	if timeSyncResp.Command() == consts.ReportMessage {
		reportVarb := timeSyncResp.VarBinds()
		if reportVarb != nil {

			reportOID := reportVarb[0].ObjectID().String()
			if reportOID == usmStatsNotInTimeWindows {
				//We have received Not_In_Time_Window_Report - Success
				return nil //Values could have been updated in Timeliness Module based on the report.
			} else {
				//Some other error occurred in Time_Sync
				//Let's check for the error to report the user.
				switch reportOID {
				case usmStatsUnsupportedSecLevels:
					return fmt.Errorf("Time-Sync Failure: UnSupportedSecurityLevel Error: %s.\nUsername: %s.", user.SecurityLevel(), user.userName)
				case usmStatsUnknownUserNames:
					return fmt.Errorf("Time-Sync Failure: UnknownUserName Error: %s.", user.userName)
				case usmStatsUnknownEngineIDs:
					return fmt.Errorf("Time-Sync Failure: UnknownEngineID Error: %v.\nUsername: %s.", peerEngine.AuthoritativeEngineID(), user.userName)
				case usmStatsWrongDigests:
					return fmt.Errorf("Time-Sync Failure: WrongDigests Error: AuthProtocol(%s)/AuthPassword is wrong.\nUsername: %s.", user.AuthAlgorithm(), user.userName)
				case usmStatsNotInTimeWindows:
					return fmt.Errorf("Time-Sync Failure: NotInTimeWindows Error. Username: %s.", user.userName)
				case usmStatsDecryptionErrors: //This should not be possible with Time-Sync
					return fmt.Errorf("Time-Sync Failure: Decryption Error: PrivProtocol(%s)/PrivPassword is wrong.\nUsername: %s.", user.PrivAlgorithm(), user.userName)
				default:
					return fmt.Errorf("Time-Sync Failure: Unknown error. Username: %s.", user.userName)
				}
			}
		} else { //No Report OID
			peerEngine = engineLCD.Engine(protocolOptions.RemoteHost(),
				protocolOptions.RemotePort(),
			)
			recET := usmSecParams.authoritativeEngineTime()
			//Let's check the EngineTable for a valid EB and ET
			if peerEngine.AuthoritativeEngineBoots() == usmSecParams.authoritativeEngineBoots() &&
				((peerEngine.AuthoritativeEngineTime() >= recET) && (peerEngine.AuthoritativeEngineTime() < recET+10)) {
				//This means that Time-Sync is successful
				return nil
			} else {
				return fmt.Errorf("Time-Sync Failure: EB/ET values not updated in LCD. Usenname: %s.", user.userName)
			}

		}

	} else if timeSyncResp.Command() == consts.GetResponse { //This implies that our local LCD is in sync with remote entity
		//We have received response for a time-sync message.
		peerEngine = engineLCD.Engine(protocolOptions.RemoteHost(),
			protocolOptions.RemotePort(),
		)

		recET := usmSecParams.authoritativeEngineTime()
		//Just an additional check - Check the EngineTable for a valid EB and ET
		if peerEngine.AuthoritativeEngineBoots() == usmSecParams.authoritativeEngineBoots() &&
			((peerEngine.AuthoritativeEngineTime() >= recET) && (peerEngine.AuthoritativeEngineTime() < recET+10)) {
			//This means that Time-Sync is successful
			return nil
		} else {
			return fmt.Errorf("Time-Sync Failure: EB/ET values not updated in LCD. Usenname: %s.", user.userName)
		}
	}

	return fmt.Errorf("Time-Sync Failure: Unknown error. Usenname: %s.", user.userName)
}

/*
 * USMUserLCD
 */

//USMUserLCD holds a list of USMSecureUser instance which contains the user configuration required for USM.
//Provide methods to add/retrieve/update/remove the user from the LCD.
//
//EngineID + UserName is the key for this store.
//
//API User need not instantiate this type. SnmpAPI will initialize this store and user can obtain the same from it.
//Also, the high-level API's provide methods to obtain the instance of this store.
type USMUserLCD interface {
	engine.SnmpModelLCD //Extends SnmpModelLCD interface

	//AddUser adds a new USM secure user after performing necessary translation on the parameters passed.
	//Attempts password_to_key translation.
	//
	//Returns error in case of failure in adding the user.
	AddUser(engineID []byte, userName string, authProtocol consts.AuthProtocol, authPassword string, privProtocol consts.PrivProtocol, privPassword string) (USMSecureUser, error)

	//AddSecureUser adds a new USM secure user to this LCD. If there is already usmUser exist in the store with same name,
	//it will be overridden.
	//
	//User authKey, privKey can be manually generated using USMAlgorithms and set on the usmUser.
	//
	//Refer USMAuthAlgorithm's PasswordToKey, LocalizeAuthKey, LocalizePrivKey methods.
	AddSecureUser(engineID []byte, usmUser USMSecureUser) error

	//UpdateUser updates the details of the user with the given userName.
	//Returns error in case of failure in updation.
	UpdateUser(engineID []byte, userName string, authProtocol consts.AuthProtocol, authPassword string, privProtocol consts.PrivProtocol, privPassword string) error

	//RemoveUser removes the user from the store based on the engineID and userName.
	RemoveUser(engineID []byte, userName string) error

	//SecureUser returns the instance of USMSecureUser based on engineID and userName.
	//Returns error in case engineID (or) userName is not found.
	SecureUser(engineID []byte, userName string) (USMSecureUser, error)

	//SecureUsers returns a slice of USMSecureUsers available in the store for all the SNMP Engines.
	SecureUsers() []USMSecureUser

	//SecureUsersByEngine returns a slice of USMSecureUser available in the store for the Engine with the engineID passed.
	SecureUsersByEngine(engineID []byte) []USMSecureUser

	//RemoveAllUsers deletes all the user entries from the UserLCD store.
	RemoveAllUsers()

	//SetAlgorithmManager sets the algorithm manager instance on the LCD,
	//which will be helpful for transforming algorithm names to respective instances.
	SetAlgorithmManager(algoMgr *USMAlgoManager)

	//AlgorithmManager returns instance of AlgorithmManager set on this LCD.
	//This mgr contains all the algorithms registered under USM.
	//User should register their own algorithm in it.
	AlgorithmManager() *USMAlgoManager
}

//Default Implementation of USMUserLCD interface.
//
//DataStore to store all the user details required by USM.
type USMUserLCDStore struct { //Similar to USMUserTable in java
	engineToUsersStore map[string]*engineUsersStore //Key will be the EngineId
	//test map[string](map[string]USMSecureUser)
	localEngine engine.SnmpEngine
	algos       *USMAlgoManager
}

//Users should not create instance of this store. Should get the instance from SnmpAPI through SnmpLCD.
//Instance is created by the corresponding model USM.
func newUSMLCD(engine engine.SnmpEngine) *USMUserLCDStore {
	lcd := &USMUserLCDStore{
		engineToUsersStore: make(map[string]*engineUsersStore),
		localEngine:        engine,
	}
	return lcd
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) AddUser(engineId []byte,
	userName string,
	authProtocol consts.AuthProtocol,
	authPassword string,
	privProtocol consts.PrivProtocol,
	privPassword string) (USMSecureUser, error) {

	//To-Do:
	//1. We have to translate password to key and localize it and store the same in LCD
	//2. We can convert the protocols to the respective Algorithm type.

	//We will perform translation first
	if engineId == nil || len(engineId) == 0 {
		return nil, fmt.Errorf("EngineID cannot be empty: %v", engineId) //Cannot add this user without an associated EngineId
	}

	//Check if the user exist already
	secureUser, _ := usmStore.SecureUser(engineId, userName)
	if secureUser != nil {
		//User already exist for the engine, hence update it.
		log.Debug("User '%s' already exist for the engine. Updating the entry.", userName)
	}

	//Declare needed params
	var authAlgo USMAuthAlgorithm
	var privAlgo USMPrivAlgorithm
	var authKey []byte
	var privKey []byte

	//Authenticate Parameters Translation:
	if authProtocol != "" && authProtocol != NO_AUTH {
		algo := usmStore.algos.Algorithm(string(authProtocol)) //This algorithm should be already registered in our Algorithm Manager
		if algo == nil {
			return nil, fmt.Errorf("Invalid Authentication Protocol: %s. Not registered in algorithm manager.", authProtocol) //Cannot add this user. Algorithm is not registered in Algo Manager.
		} else if algorithm, ok := algo.(USMAuthAlgorithm); ok {
			authAlgo = algorithm
		} else {
			return nil, fmt.Errorf("Not a valid Authentication Protocol: %s", authProtocol) //This is not an Authentication Algorithm
		}
	}

	if authAlgo != nil && authPassword != "" {
		authKey = authAlgo.PasswordToKey(authPassword) //Convert password to secret key
	}

	//Localize AuthKey with EngineID
	if authAlgo != nil && authKey != nil {
		authKey = authAlgo.LocalizeAuthKey(authKey, engineId)
	}

	//Privacy Parameters Translation:
	if privProtocol != "" && privProtocol != NO_PRIV {
		algo := usmStore.algos.Algorithm(string(privProtocol)) //This algorithm should be already registered in our Algorithm Manager
		if algo == nil {
			return nil, fmt.Errorf("Invalid Privacy Protocol: %s. Not registered in algorithm manager.", privProtocol) //Cannot add this user. Algorithm is not registered in Algo Manager.
		} else if algorithm, ok := algo.(USMPrivAlgorithm); ok {
			privAlgo = algorithm
		} else {
			return nil, fmt.Errorf("Not a valid Privacy Protocol: %s", privProtocol) //This is not a Privacy Algorithm
		}
	}

	//Convert password to secret key. Note: Authentication Algorithm is used to convert password for privacy as well.
	if privAlgo != nil && privPassword != "" {
		if authAlgo == nil {
			return nil, fmt.Errorf("No Authentication protocol exist to convert password to key for privacy protocol: %s. Privacy cannot exist without authentication.", privProtocol) //We dont have an authentication algorithm to convert priv password to key
		}
		privKey = authAlgo.PasswordToKey(privPassword) //Ku
		//Localize PrivKey with EngineId
		if privKey != nil {
			privKey = authAlgo.LocalizePrivKey(privKey, engineId, privAlgo.KeySize()) //Kul
			//if privKey is still nil, we should perform password_to_key chaining as per http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00
			if privKey == nil {
				privKey = authAlgo.ExtendPrivKey(privPassword, engineId, privAlgo.KeySize())
				//privKey = passwordToKeyChaining(authAlgo, privPassword, engineId, privAlgo.KeySize()) //This is localized privacy key
			}
		}
	}

	//Let's create the USMSecureUser now.
	secUser := NewUSMSecureUserImpl(usmStore, //Pass the USMUserLCD instance
		userName,
		engineId,
		authAlgo, //is nil for NO_AUTH
		authKey,
		privAlgo, //is nil for usmNoPrivsProtocol
		privKey,
	)

	if !strings.EqualFold(authPassword, "") {
		secUser.authPasswd = authPassword
	}
	if !strings.EqualFold(privPassword, "") {
		secUser.privPasswd = privPassword
	}

	//engineId and secUser's engineId are same. Do we need it in two places?
	err := usmStore.AddSecureUser(engineId, secUser)
	if err != nil {
		return nil, err
	}

	//Successfully added the user in USMUserLCD
	return secUser, nil
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) AddSecureUser(engineId []byte, user USMSecureUser) error {
	if engineId == nil {
		return errors.New("EngineID is nil. Cannot add the user: " + user.UserName()) //EngineId cannot be nil
	}
	if user == nil {
		return errors.New("User instance is nil. Cannot add the user.")
	}

	//Let's perform the check whether the required params are set on the user
	if user.AuthProtocol() != nil && user.AuthKey() == nil {
		//Authentication key is not generated for this user
		log.Warn("Authentication key is not generated for the user: %s. User will be added with NoAuthNoPriv security level.", user.UserName())
	}
	if user.PrivProtocol() != nil {
		if user.PrivKey() == nil {
			//Privacy key is not generated for this user
			log.Warn("Privacy key is not generated for the user: %s. User will be added with security level less than AuthPriv.", user.UserName())
		} else if user.AuthProtocol() == nil {
			return errors.New("Failure in adding user: Privacy cannot exist without authentication. Set the authentication protocol/authetication key for the user.")
		}
	}

	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		//Let's store in the runtime memory
		if engineStore, ok := usmStore.engineToUsersStore[string(engineId)]; ok {
			engineStore.addUser(user) //Add SnmpSecureUser
		} else {
			engine := initEngineUserStore(engineId)
			engine.addUser(user) //Add SnmpSecureUser
			usmStore.engineToUsersStore[string(engineId)] = engine
		}
		log.Debug("Succesfully added the user '%s' with EngineID '%v' in LCD.", user.UserName(), engineId)
	} else {
		//DBImpl is not nil, we should store the values in DB.
		userImpl := user.(*USMSecureUserImpl)
		var err error
		if sec, _ := usmStore.SecureUser(engineId, userImpl.UserName()); sec == nil {
			//No user exist, hence add one
			err = dbImpl.InsertUsmEntry((engineId),
				userImpl.UserName(),
				strconv.Itoa(int(userImpl.SecurityLevel())),
				userImpl.UserName(),
				userImpl.AuthAlgorithm(),
				encryptPwd(userImpl.authPasswd), //Need to encrypt the password
				(userImpl.AuthKey()),            //Can be empty
				userImpl.PrivAlgorithm(),
				encryptPwd(userImpl.privPasswd), //Need to encrypt the password
				(userImpl.PrivKey()),
			)
		} else {
			//User already exist, update it.
			err = dbImpl.UpdateUsmEntry((engineId),
				userImpl.UserName(),
				strconv.Itoa(int(userImpl.SecurityLevel())),
				userImpl.UserName(),
				userImpl.AuthAlgorithm(),
				encryptPwd(userImpl.authPasswd), //Need to encrypt the password
				(userImpl.AuthKey()),            //Can be empty
				userImpl.PrivAlgorithm(),
				encryptPwd(userImpl.privPasswd), //Need to encrypt the password
				(userImpl.PrivKey()),
			)
		}

		if err != nil {
			log.Error("Failure in adding the user to DB. EngineID: %v; UserName: %s.\nError: %s.", engineId, user.UserName(), err.Error())
			return err
		}
		log.Debug("Succesfully added the user '%s' with EngineID '%v' in DB.", user.UserName(), engineId)
	}

	return nil
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) UpdateUser(engineId []byte,
	userName string,
	newAuthProtocol consts.AuthProtocol,
	newAuthPassword string,
	newPrivProtocol consts.PrivProtocol,
	newPrivPassword string,
) error {

	secureUser, _ := usmStore.SecureUser(engineId, userName)
	if secureUser == nil {
		return errors.New("User not found with this UserName. Updation not performed.")
	}

	//Declare needed params
	var authAlgo USMAuthAlgorithm
	var privAlgo USMPrivAlgorithm
	var authKey []byte
	var privKey []byte

	//Authenticate Parameters Translation:
	if newAuthProtocol != "" && newAuthProtocol != NO_AUTH {
		algo := usmStore.algos.Algorithm(string(newAuthProtocol)) //This algorithm should be already registered in our Algorithm Manager
		if algo == nil {
			return fmt.Errorf("Not Updating the User. Invalid Authentication Protocol: %s. Not registered in algorithm manager.", newAuthProtocol) //Cannot add this user. Algorithm is not registered in Algo Manager.
		} else if algorithm, ok := algo.(USMAuthAlgorithm); ok {
			authAlgo = algorithm
		} else {
			return fmt.Errorf("Not Updating the User. Not a valid Authentication Protocol: %s", newAuthProtocol) //This is not an Authentication Algorithm
		}
	}

	if authAlgo != nil && newAuthPassword != "" {
		authKey = authAlgo.PasswordToKey(newAuthPassword) //Convert password to secret key
	}

	//Localize AuthKey with EngineID
	if authAlgo != nil && authKey != nil {
		authKey = authAlgo.LocalizeAuthKey(authKey, engineId)
	}

	//Privacy Parameters Translation:
	if newPrivProtocol != "" && newPrivProtocol != NO_PRIV {
		algo := usmStore.algos.Algorithm(string(newPrivProtocol)) //This algorithm should be already registered in our Algorithm Manager
		if algo == nil {
			return fmt.Errorf("Not Updating the User. Invalid Privacy Protocol: %s. Not registered in algorithm manager.", newPrivProtocol) //Cannot add this user. Algorithm is not registered in Algo Manager.
		} else if algorithm, ok := algo.(USMPrivAlgorithm); ok {
			privAlgo = algorithm
		} else {
			return fmt.Errorf("Not Updating the User. Not a valid Privacy Protocol: %s", newPrivProtocol) //This is not a Privacy Algorithm
		}
	}

	//Convert password to secret key. Note: Authentication Algorithm is used to convert password for privacy as well.
	if privAlgo != nil && newPrivPassword != "" {
		if authAlgo == nil {
			return fmt.Errorf("Not Updating the User. No Authentication protocol exist to convert password to key for privacy protocol: %s", newPrivProtocol) //We dont have an authentication algorithm to convert priv password to key
		}
		privKey = authAlgo.PasswordToKey(newPrivPassword) //Ku
		//Localize PrivKey with EngineId
		if privKey != nil {
			privKey = authAlgo.LocalizePrivKey(privKey, engineId, privAlgo.KeySize()) //Kul
			//if privKey is still nil, we should perform password_to_key chaining as per http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00
			if privKey == nil {
				privKey = authAlgo.ExtendPrivKey(newPrivPassword, engineId, privAlgo.KeySize())
				//privKey = passwordToKeyChaining(authAlgo, privPassword, engineId, privAlgo.KeySize()) //This is localized privacy key
			}
		}
		//		//if privKey is still nil, we should perform password_to_key chaining as per http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00
		//		if privKey == nil {
		//			privKey = passwordToKeyChaining(authAlgo, newPrivPassword, engineId, privAlgo.KeySize()) //This is localized privacy key
		//		}
	}

	//Let's Update the USMSecureUser now.
	secureUser.SetAuthProtocol(authAlgo) //Can be nil - NoAuth
	secureUser.SetAuthKey(authKey)
	secureUser.SetPrivProtocol(privAlgo) //Can be nil - NoPriv
	secureUser.SetPrivKey(privKey)

	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		if engineStore, ok := usmStore.engineToUsersStore[string(engineId)]; ok {
			engineStore.addUser(secureUser) //Update SnmpSecureUser
		}
	} else {
		//DBImpl is not nil, we should update the values in DB.
		userImpl := secureUser.(*USMSecureUserImpl)
		err := dbImpl.UpdateUsmEntry((engineId),
			userImpl.UserName(),
			strconv.Itoa(int(userImpl.SecurityLevel())),
			userImpl.UserName(),
			userImpl.AuthAlgorithm(),
			encryptPwd(userImpl.authPasswd), //Need to encrypt the password
			(userImpl.AuthKey()),            //Can be empty
			userImpl.PrivAlgorithm(),
			encryptPwd(userImpl.privPasswd), //Need to encrypt the password
			(userImpl.PrivKey()),
		)
		if err != nil {
			log.Error("Failure in updating the user in DB. EngineID: %v; UserName: %s.\nError: %s.", engineId, userName, err.Error())
			return err
		}
	}
	log.Debug("Succesfully updated the user '%s' with EngineID '%v'.", userName, engineId)

	return nil
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) RemoveUser(engineId []byte, user string) error {
	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		if engineStore, ok := usmStore.engineToUsersStore[string(engineId)]; ok {
			//We have an engine existing for the user
			usr := engineStore.removeUser(user)
			if usr == nil {
				return fmt.Errorf("No user exist with given EngineID: %s and Name: %s", string(engineId), user)
			}
			log.Debug("Succesfully removed the user '%s' with EngineID '%v' in LCD.", user, engineId)
		}
	} else {
		//DBImpl is not nil, we should delete the row from DB.
		err := dbImpl.DeleteUsmEntry(user,
			engineId,
		)
		if err != nil {
			log.Error("Failure in deleting the user from DB. EngineID: %v; UserName: %s.\nError: %s.", engineId, user, err.Error())
			return err
		}
		log.Debug("Succesfully removed the user '%s' with EngineID '%v' from Database.", user, engineId)
	}

	return nil
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) SecureUser(engineId []byte, name string) (USMSecureUser, error) {
	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		if engineStore, ok := usmStore.engineToUsersStore[string(engineId)]; ok {
			//Engine exist, lets check for the user
			if user := engineStore.user(name); user == nil {
				return nil, errors.New("User not found in LCD: Unknown UserName.") // Error: No User exist under engine: engineId
			} else {
				return user, nil
			}
		} else { //Error: Unknown EngineId
			return nil, errors.New("User not found in LCD: Unknown EngineID.")
		}
	} else {
		//DBImpl is not nil, we should get the row from DB.
		userEntry, err := dbImpl.UsmEntry(name,
			engineId,
		)
		if err != nil {
			log.Error("Failure in getting the user entry from DB. EngineID: %v; UserName: %s.\nError: %s.", engineId, name, err.Error())
			return nil, fmt.Errorf("User not found in LCD: Unknown EngineID. Err: %s", err.Error())
		}
		secUser := new(USMSecureUserImpl)
		secUser.lcd = usmStore
		secUser.copy(userEntry)

		return secUser, nil
	}
	return nil, errors.New("Failure in getting the user info.")
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) SecureUsers() []USMSecureUser {
	var secUsers []USMSecureUser
	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		for _, engineStore := range usmStore.engineToUsersStore {
			for _, user := range engineStore.usersStore {
				secUsers = append(secUsers, user)
			}
		}
	} else {
		//Get the entries from database
		usmEntrySlice, err := dbImpl.UsmEntries()
		if err != nil {
			log.Error("Failure in getting the user entries from DB.\nError: %s.", err.Error())
			return nil
		}

		var usmSecureUser *USMSecureUserImpl
		for _, usmEntry := range usmEntrySlice { //Copy the values from UsmEntry slice
			usmSecureUser = new(USMSecureUserImpl)
			usmSecureUser.lcd = usmStore
			usmSecureUser.copy(usmEntry)
			secUsers = append(secUsers, usmSecureUser)
		}
	}

	return secUsers
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) SecureUsersByEngine(engineId []byte) []USMSecureUser { //Should return an array of users
	var secUsers []USMSecureUser
	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		if engineStore, ok := usmStore.engineToUsersStore[string(engineId)]; ok {
			for _, user := range engineStore.usersStore {
				secUsers = append(secUsers, user)
			}
		}
	} else {
		//Get the entries from database
		usmEntrySlice, err := dbImpl.UsmEntriesByID(engineId)
		if err != nil {
			log.Error("Failure in getting the user entries from DB.\nError: %s.", err.Error())
			return nil
		}

		var usmSecureUser *USMSecureUserImpl
		for _, usmEntry := range usmEntrySlice { //Copy the values from UsmEntry slice
			usmSecureUser = new(USMSecureUserImpl)
			usmSecureUser.lcd = usmStore
			usmSecureUser.copy(usmEntry)
			secUsers = append(secUsers, usmSecureUser)
		}
	}

	return secUsers
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) RemoveAllUsers() {
	dbImpl := usmStore.localEngine.DBOperations()
	if dbImpl == nil {
		for engKey, engStore := range usmStore.engineToUsersStore {
			for userKey, _ := range engStore.usersStore {
				delete(engStore.usersStore, userKey)
			}
			delete(usmStore.engineToUsersStore, engKey)
		}
		log.Debug("Succesfully removed all the users from LCD.")
	} else {
		//DBImpl is not nil, we should delete the row from DB.
		err := dbImpl.DeleteUsmEntries()
		if err != nil {
			log.Error("Failure in deleting the users from DB. Error: %s.", err.Error())
			return
		}
		log.Debug("Succesfully removed all the users from DB.")
	}

	return
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) Clear() {
	usmStore.RemoveAllUsers()
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) SetAlgorithmManager(algoMgr *USMAlgoManager) {
	usmStore.algos = algoMgr
}

//Refer USMUserLCD interface doc.
func (usmStore *USMUserLCDStore) AlgorithmManager() *USMAlgoManager {
	return usmStore.algos
}

type engineUsersStore struct {
	engineId   []byte                   //This will be the key to the set of users
	usersStore map[string]USMSecureUser //UserName is the key
}

func initEngineUserStore(engineid []byte) *engineUsersStore {
	es := &engineUsersStore{
		engineId:   engineid,
		usersStore: make(map[string]USMSecureUser),
	}
	return es
}

func (eus *engineUsersStore) addUser(user USMSecureUser) {
	eus.usersStore[user.UserName()] = user
}

func (eus *engineUsersStore) removeUser(name string) USMSecureUser {
	if user, ok := eus.usersStore[name]; ok {
		delete(eus.usersStore, name)
		return user
	}

	return nil
}

func (eus *engineUsersStore) user(name string) USMSecureUser {
	return eus.usersStore[name]
}

func (secUser *USMSecureUserImpl) copy(entry db.UsmEntry) {
	secUser.SetUserName(entry.UserName)
	secUser.SetEngineID([]byte(entry.EngineId))
	secUser.SetAuthAlgorithm(consts.AuthProtocol(entry.AuthProtocol))
	secUser.SetPrivAlgorithm(consts.PrivProtocol(entry.PrivProtocol))
	secUser.SetAuthKey([]byte(entry.AuthKey))
	secUser.SetPrivKey([]byte(entry.PrivKey))
}

func encryptPwd(passwd string) string {
	encData := encrypt([]byte(passwd))
	if encData != nil {
		return hex.EncodeToString(encData) //Return encrypted hex string
	}
	return ""
}

func decryptPwd(encPwd string) string {
	cipher, err := hex.DecodeString(encPwd) //Encrypted hex string
	if err != nil {
		return ""
	}
	pwd := decrypt(cipher)
	if pwd != nil {
		return string(pwd)
	}
	return ""
}
