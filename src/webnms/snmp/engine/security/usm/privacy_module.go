/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/msg"
)

//usmPrivModule manages the privacy of SNMPv3 messages.
//
//Used internally by User Security model to encrypt/decrypt the incoming and outgoing messages.
//This layer interacts with the USMPrivAlgorithms.
type usmPrivModule struct {
	lcd      USMUserLCD
	usmModel *USMSecurityModel
}

func newUSMPrivModule(usmLCD USMUserLCD, usmModel *USMSecurityModel) *usmPrivModule {
	return &usmPrivModule{
		lcd:      usmLCD,
		usmModel: usmModel,
	}
}

//Encryption performed on outgoing message.
//Returns error as StatusInformation in case of any failure
func (pm *usmPrivModule) encryptData(secParams *USMSecurityParameters, //Should use the algorithm, key from secParams or cache
	cache *USMSecurityCache,
	dataToEncrypt []byte,
) ([]byte, []byte, *engine.StatusInformation) { //Encrypted ScopedPDU + PrivParameters (SALT)
	var encryptedData, privParameters []byte
	var err error
	var privAlgo USMPrivAlgorithm
	var privKey []byte

	//For outgoing response, cache data will be used to perform encryption. This cache could have been created from incoming request.
	//Not for outgoing request/notification
	if cache != nil && cache.usmUserPrivProtocol != nil {
		privAlgo = cache.usmUserPrivProtocol
		privKey = cache.usmUserPrivKey
	} else { //This would be outgoing request, which doesn't contain cache
		secUser, _ := pm.lcd.SecureUser(secParams.AuthoritativeEngineID(), secParams.userName())
		if secUser != nil {
			privAlgo = secUser.PrivProtocol()
			privKey = secUser.PrivKey()
		}
	}

	if privAlgo == nil { //If priv algorithm is still nil, return error
		//This encryption returns general error. Message Processing Model should not do anything with it.
		return nil, nil, pm.usmModel.errGen.genGeneralError(nil,
			nil,
			"Encryption Error: PrivAlgorithm doesn't exist to perform encryption.",
			nil,
			"",
			0,
			secParams,
		) //We dont have priv algo to encrypt the message
	}

	encryptedData, privParameters, err = privAlgo.Encrypt(dataToEncrypt, privKey, secParams)
	if err != nil {
		return nil, nil, pm.usmModel.errGen.genGeneralError(nil,
			nil,
			err.Error(),
			nil,
			"",
			0,
			secParams,
		)
	}

	return encryptedData, privParameters, nil
}

//Decryption performed on incoming message.
//Returns decrypted and decoded ScopedPDU, else returns error as StatusInformation in case of any failure.
func (pm *usmPrivModule) decryptData(secParams *USMSecurityParameters, //Should use the algorithm, key from secParams or cache
	cache *USMSecurityCache,
	encryptedData []byte,
	msgFlags byte,
) (*msg.ScopedPDU, *engine.StatusInformation) { //Decrypted Data
	var decryptedData []byte
	var privAlgo USMPrivAlgorithm
	var privKey []byte
	var err error

	var scopedPDU *msg.ScopedPDU = nil

	//We donot use cache for incoming messages
	secUser, _ := pm.lcd.SecureUser(secParams.AuthoritativeEngineID(), secParams.userName())
	privAlgo = secUser.PrivProtocol()
	privKey = secUser.PrivKey()

	//Fill the cache for incoming request
	if msgFlags&security.ReportableFlag == security.ReportableFlag { //Can we fill cache for both requests/responses??
		//cache.msgId = //This data could have been filled by AuthModule already as it is called before PrivModule for incoming messages.
		cache.usmUserPrivProtocol = privAlgo
		cache.usmUserPrivKey = privKey
	}

	if privAlgo == nil { //If priv algorithm is still nil, return error
		return nil, pm.usmModel.errGen.genGeneralError(nil,
			nil,
			"Decryption Error: PrivAlgorithm doesn't exist to perform decryption.",
			nil,
			"",
			msgFlags,
			secParams,
		)
	}

	privParameters := secParams.privParameters() //SALT - We need this to decrypt
	decryptedData, err = privAlgo.Decrypt(encryptedData,
		privKey,
		privParameters,
		secParams, //This would be need by the algorithm to calculate SALT (EB, ET)
	)

	if err != nil {
		return nil, pm.usmModel.errGen.genDecryptionError(nil,
			"",
			msgFlags,
			secParams,
		)
	}

	//Let's convert this decrypted byte array into DecryptedMessage
	scopedPDU, err = pm.usmModel.localEngine.MsgProcessingSubSystem().DecodeScopedPDU(3, decryptedData)
	if err != nil { //If message is not in sequence, it indicates decryption error
		return nil, pm.usmModel.errGen.genDecryptionError(nil,
			"",
			msgFlags,
			secParams,
		)
	}

	return scopedPDU, nil //This can be nil
}
