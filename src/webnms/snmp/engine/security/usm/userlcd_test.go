/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"

	"testing"
)

var addUserTest = []struct {
	engID        []byte
	userName     string
	authProtocol consts.AuthProtocol
	authPassword string
	privProtocol consts.PrivProtocol
	privPassword string
	resStruct
}{
	{[]byte{1, 2, 34, 1, 0}, "Swaminathan", NO_AUTH, "password", DES_PRIV, "", resStruct{security.NoAuthNoPriv, false, false}},
	{[]byte{1, 2, 34, 1, 0}, "Swaminathan", MD5_AUTH, "password", DES_PRIV, "", resStruct{security.AuthNoPriv, true, false}},
	{[]byte{1, 2, 34, 1, 0}, "Swaminathan2", MD5_AUTH, "password", DES_PRIV, "", resStruct{security.AuthNoPriv, true, false}},
	{[]byte{1, 2, 34, 1, 0}, "Swaminathan3", NO_AUTH, "password", NO_PRIV, "privpassword", resStruct{security.NoAuthNoPriv, false, false}},
	{[]byte{1, 2, 34, 1, 0}, "Swaminathan4", MD5_AUTH, "password", DES_PRIV, "privpassword", resStruct{security.AuthPriv, true, true}},
}

type resStruct struct {
	secLevel consts.SecurityLevel
	authKey  bool
	privKey  bool
}

func TestAdd(t *testing.T) {
	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 10)
	userLCD := newUSMLCD(eng)
	algoMgr := NewUSMAlgorithmMgr()
	userLCD.SetAlgorithmManager(algoMgr)

	//Register the Authentication algorithms
	md5 := NewMD5()
	algoMgr.RegisterAlgorithm(md5)

	sha := NewSHA1()
	algoMgr.RegisterAlgorithm(sha)

	//Register the Privacy algorithms
	des := NewDES(eng)
	algoMgr.RegisterAlgorithm(des)

	tdes := NewTripleDES(eng)
	algoMgr.RegisterAlgorithm(tdes)

	aes128 := NewAES128(eng)
	algoMgr.RegisterAlgorithm(aes128)

	aes192 := NewAES192(eng)
	algoMgr.RegisterAlgorithm(aes192)

	aes256 := NewAES256(eng)
	algoMgr.RegisterAlgorithm(aes256)

	for _, u := range addUserTest {
		secUser, err := userLCD.AddUser(u.engID, u.userName, u.authProtocol, u.authPassword, u.privProtocol, u.privPassword)
		if err != nil {
			t.Error(err)
		} else {
			if secUser.SecurityLevel() != u.secLevel {
				t.Errorf("AddUser: Invalid security level. Expected: %d. Got: %d.", u.secLevel, secUser.SecurityLevel())
			}
			if (secUser.AuthKey() == nil && u.authKey) || (secUser.AuthKey() != nil && !u.authKey) {
				t.Errorf("AddUser: Unexpected authkey generated for user: %s.", secUser.UserName())
			}
			if (secUser.PrivKey() == nil && u.privKey) || (secUser.PrivKey() != nil && !u.privKey) {
				t.Errorf("AddUser: Unexpected privkey generated for user: %s.", secUser.UserName())
			}
		}
	}
}

func TestUpdateAndRemove(t *testing.T) {

	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 10)
	userLCD := newUSMLCD(eng)
	algoMgr := NewUSMAlgorithmMgr()
	userLCD.SetAlgorithmManager(algoMgr)

	//Register the Authentication algorithms
	md5 := NewMD5()
	algoMgr.RegisterAlgorithm(md5)

	sha := NewSHA1()
	algoMgr.RegisterAlgorithm(sha)

	//Register the Privacy algorithms
	des := NewDES(eng)
	algoMgr.RegisterAlgorithm(des)

	tdes := NewTripleDES(eng)
	algoMgr.RegisterAlgorithm(tdes)

	aes128 := NewAES128(eng)
	algoMgr.RegisterAlgorithm(aes128)

	aes192 := NewAES192(eng)
	algoMgr.RegisterAlgorithm(aes192)

	aes256 := NewAES256(eng)
	algoMgr.RegisterAlgorithm(aes256)

	engID := eng.SnmpEngineID()
	userName := "testUser"

	err := userLCD.UpdateUser(engID, userName, "test", "", "", "")
	if err == nil {
		t.Errorf("UpdateUser: Not throwing error when update user is called on new username.")
	}

	secUser, err := userLCD.AddUser(engID, userName, "", "", "", "")
	if err != nil {
		t.Error(err)
	} else {
		err = userLCD.UpdateUser(engID, userName, MD5_AUTH, "password", "", "password")
		if err != nil {
			t.Error(err)
		} else {
			if secUser.SecurityLevel() != security.AuthNoPriv || secUser.AuthKey() == nil {
				t.Errorf("UpdateUser: Failure in updating user: %s.", secUser.UserName())
			}
		}

	}

	secUser, err = userLCD.SecureUser(engID, userName)
	if err != nil {
		t.Error(err)
	}
	secUser, err = userLCD.SecureUser(nil, "")
	if err == nil {
		t.Error("SecureUser not throwing error when EngineID is nil.")
	}
	if len(userLCD.SecureUsers()) <= 0 {
		t.Error("SecureUsers: Returns 0.")
	}

	for _, secuser := range userLCD.SecureUsers() {
		userLCD.RemoveUser(secuser.EngineID(), secuser.UserName())
	}

	if len(userLCD.SecureUsers()) > 0 {
		t.Error("Failure in removing all the users from UserLCD.")
	}

	//RemoveAllUsers test
	userLCD.AddUser(engID, userName, "", "", "", "")
	userLCD.AddUser(engID, "newUser", "", "", "", "")
	userLCD.AddUser([]byte{1, 2, 1}, userName, "", "", "", "")

	userLCD.RemoveAllUsers()
	if len(userLCD.SecureUsers()) > 0 {
		t.Error("RemoveAllUsers: Failure in removing all the users from UserLCD.")
	}

	userLCD.AddUser(engID, userName, "", "", "", "")
	userLCD.AddUser(engID, "newUser", "", "", "", "")
	userLCD.AddUser([]byte{1, 2, 1}, userName, "", "", "", "")

	if len(userLCD.SecureUsers()) <= 0 {
		t.Error("SecureUsers: Returns 0.")
	}
}
