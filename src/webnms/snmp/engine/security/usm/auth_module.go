/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"bytes"

	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/msg"
)

//Sender - Compute digest over portion of the message (with secret key prepended) and insert it into the message and send the same.
//Receiver - calculate digest using the shared key over the message and check whether the computed digest and received digests are same.

//usmAuthModule manages the authentication of SNMPv3 messages.
//
//Used internally by User Security model to authenticate incoming and outgoing messages.
//This layer interacts with the USMAuthAlgorithms.
type usmAuthModule struct {
	lcd USMUserLCD
	usm *USMSecurityModel
}

func newUSMAuthModule(usmLCD USMUserLCD, model *USMSecurityModel) *usmAuthModule {
	return &usmAuthModule{
		lcd: usmLCD,
		usm: model,
	}
}

//Authenticate the incoming message.
//If Success returns nil, else returns the error as StatusInformation.
func (am *usmAuthModule) authenticateIncomingMsg(msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32, //USM (3)
	contextEngineId []byte, //Nil if Auth-Priv
	contextName string, //Nil if Auth-Priv
	pduData msg.SnmpPDU, //Nil if Auth-Priv
	encryptedScopedPDU []byte, //Nil if not Auth-Priv
	secParams *USMSecurityParameters,
	usmCache *USMSecurityCache,
	msgProcessingSubsystem engine.SnmpMsgProcessingSubSystem,
) *engine.StatusInformation {

	recHMAC := secParams.authParameters()

	//Generate the encoded data - byte array required
	var encodedData []byte
	var err error

	//Encrypted message is authenticated here.
	//Encode encrypted message first.
	if msgFlags&security.AuthPrivMask < byte(security.AuthPriv) { //UnEncrypted Data
		//Digest should be zeroed before performing authentication and PrivParams should be made nil.
		//As per RFC 3414, Section 6.3.2 Point 3
		securityParams := zeroAuthPrivParams(secParams)
		secBytes, _ := securityParams.EncodeSecurityParameters()

		scopedPDU := new(msg.ScopedPDU)
		scopedPDU.SetContextEngineID(contextEngineId)
		scopedPDU.SetContextName(contextName)
		scopedPDU.SnmpPDU = pduData

		encodedData, err = msgProcessingSubsystem.Encode(consts.Version3,
			msgId,
			msgMaxSize,
			msgFlags,
			msgSecurityModel,
			secBytes,
			*scopedPDU,
		)
	} else {
		//Digest should be zeroed before performing authentication.
		//As per RFC 3414, Section 6.3.2 Point 3
		securityParams := zeroAuthParams(secParams) //Digest should be zeroed before performing authentication.
		secBytes, _ := securityParams.EncodeSecurityParameters()
		encodedData, err = msgProcessingSubsystem.EncodePriv(consts.Version3, //EncryptedData
			msgId,
			msgMaxSize,
			msgFlags,
			msgSecurityModel,
			secBytes,
			encryptedScopedPDU,
		)
	}
	if err != nil {
		msgProcessingSubsystem.IncSnmpInASNParseErrsCounter(consts.Version3)
		//return - unable to encode the message for authentication
		return am.usm.errGen.genGeneralError(nil,
			nil,
			err.Error(),
			contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)
	}

	//Let's authenticate the message with the cache/secParams
	var authAlgo USMAuthAlgorithm
	var authKey []byte

	secUser, _ := am.lcd.SecureUser(secParams.AuthoritativeEngineID(), secParams.userName())
	if secUser != nil {
		authAlgo = secUser.AuthProtocol()
		authKey = secUser.AuthKey()
	}

	if authAlgo == nil {
		//Authentication Failure. There is no valid AuthProtocol exist for the user to authenticate the message.
		return am.usm.errGen.genGeneralError(nil,
			nil,
			"Authentication Failure: There is no valid authentication protocl to authenticate the message",
			contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)
	}

	//Fill the cache for incoming request
	if (usmCache != nil) &&
		(msgFlags&security.ReportableFlag == security.ReportableFlag) { //Only for incoming confirmed class, we fill cache
		usmCache.usmUserAuthProtocol = authAlgo
		usmCache.usmUserAuthKey = authKey
	}

	calcHMAC := authAlgo.SignMsg(encodedData, authKey) //12 Octets Digest

	//Compare the received digest with the calculated digest
	if bytes.Equal(recHMAC, calcHMAC) {
		return nil //Message is authentic
	}

	return am.usm.errGen.genWrongDigestsError(contextEngineId,
		contextName,
		msgFlags,
		secParams,
	)
}

/*
//For signing the message. This method is added for sake of clarity,
//It just calls the authenticateOutgoingMessage method.
func (am *usmAuthModule) signMsg(msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32, //USM (3)
	contextEngineId []byte, //Nil if Auth-Priv
	contextName string, //Nil if Auth-Priv
	pduData msg.SnmpPDU, //Nil if Auth-Priv
	encryptedScopedPDU []byte, //Nil if not Auth-Priv
	secParams *USMSecurityParameters,
	usmCache *USMSecurityCache,
	msgProcessingSubsystem engine.SnmpMsgProcessingSubSystem,
) ([]byte, *USMStatusInformation) { //Return calculated HMAC, which should be set as AuthParameters

	return am.authenticateOutgoingMsg(msgId,
		msgMaxSize,
		msgFlags,
		msgSecurityModel,   //USM (3)
		contextEngineId,    //Nil if Auth-Priv
		contextName,        //Nil if Auth-Priv
		pduData,            //Nil if Auth-Priv
		encryptedScopedPDU, //Nil if not Auth-Priv
		secParams,
		usmCache,
		msgProcessingSubsystem,
	)
}
*/

//Sign the outgoing message.
//Returns error as status information in case of any failure in authentication.
func (am *usmAuthModule) authenticateOutgoingMsg(msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32, //USM (3)
	contextEngineId []byte, //Nil if Auth-Priv
	contextName string, //Nil if Auth-Priv
	pduData msg.SnmpPDU, //PDU part - Nil if Auth-Priv
	encryptedScopedPDU []byte, //Nil if not Auth-Priv
	secParams *USMSecurityParameters,
	usmCache *USMSecurityCache,
	msgProcessingSubsystem engine.SnmpMsgProcessingSubSystem,
) ([]byte, *engine.StatusInformation) { //Return calculated HMAC, which should be set as AuthParameters
	//Generate the encoded data - byte array required
	var encodedData []byte
	var err error

	if msgFlags&security.AuthPrivMask < byte(security.AuthPriv) { //Not encrypted
		if contextEngineId != nil { //If it is not encrypted (< Auth-Priv), implies that the privParams are not filled.
			//AuthParameters is zeroed and PrivParams in made nil
			//As per RFC 3414 7.3.1, Point 1
			securityParams := zeroAuthPrivParams(secParams) //Both the digest and SALT will be zeroed for this
			//Create scopedPDU
			scopedPDU := new(msg.ScopedPDU)
			scopedPDU.SetContextEngineID(contextEngineId)
			scopedPDU.SetContextName(contextName)
			scopedPDU.SnmpPDU = pduData

			secBytes, _ := securityParams.EncodeSecurityParameters()

			//This encoding is performed without AuthParameters. Security Module will perform encoding after filling auth parameters.
			encodedData, err = msgProcessingSubsystem.Encode(consts.Version3,
				msgId, //To Pradheep: Encode()
				msgMaxSize,
				msgFlags,
				msgSecurityModel,
				secBytes,
				*scopedPDU,
			)
		} else {
			//return encryption error -  We dont have required to perform encoding
		}
	} else { //If it is encrypted PDU (Auth-Priv), then it could have been encrypted by now and PrivParams are filled
		//AuthParameters is zeroed
		//As per RFC 3414 7.3.1, Point 1
		securityParams := zeroAuthParams(secParams) //Digest should be zeroed before performing authentication.
		secBytes, _ := securityParams.EncodeSecurityParameters()
		encodedData, err = msgProcessingSubsystem.EncodePriv(consts.Version3,
			msgId, //To Pradheep: Encode()
			msgMaxSize,
			msgFlags,
			msgSecurityModel,
			secBytes,
			encryptedScopedPDU,
		)

	}

	if err != nil {
		msgProcessingSubsystem.IncSnmpInASNParseErrsCounter(consts.Version3)
		//return - unable to encode the message for authentication
		return nil, am.usm.errGen.genGeneralError(nil,
			nil,
			err.Error(),
			contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)
	}

	//Let's authenticate the message with the cache/secParams
	var authAlgo USMAuthAlgorithm
	var authKey []byte

	//Let's use cache data to authenticate the message if it has auth protocol
	//Should be used only for outgoing response.
	if (usmCache != nil) &&
		(usmCache.usmUserAuthProtocol != nil) &&
		(msgFlags&security.ReportableFlag != security.ReportableFlag) { //Only for Outgoing Response/Report, we use cache
		authAlgo = usmCache.usmUserAuthProtocol
		authKey = usmCache.usmUserAuthKey
	} else { //Donot use cache data, this could be outgoing request
		secUser, _ := am.lcd.SecureUser(secParams.AuthoritativeEngineID(), secParams.userName())
		authAlgo = secUser.AuthProtocol()
		authKey = secUser.AuthKey()
	}

	if authAlgo == nil {
		//Authentication Failure. There is no valid AuthProtocol exist for the user to authenticate the message.
		return nil, am.usm.errGen.genGeneralError(nil,
			nil,
			"Authentication Failure: There is no valid authentication protocl to authenticate the message",
			contextEngineId,
			contextName,
			msgFlags,
			secParams,
		)
	}
	calcHMAC := authAlgo.SignMsg(encodedData, authKey) //12 Octets Digest

	return calcHMAC, nil
}

//Auth parameters should be zeroed before authenticating the message at security level less than Auht-Priv
func zeroAuthParams(secParams *USMSecurityParameters) *USMSecurityParameters {
	zeroBytes := make([]byte, 12)
	return &USMSecurityParameters{
		authEngineId:    secParams.AuthoritativeEngineID(),
		authEngineBoots: secParams.authoritativeEngineBoots(),
		authEngineTime:  secParams.authoritativeEngineTime(),
		authUserName:    secParams.userName(),
		authParams:      zeroBytes, //Zeroed Array of 12 Bytes
		privParams:      secParams.privParameters(),
	}
}

//Auth and priv parameters should be zeroed before authenticating the message at Auth-Priv level
func zeroAuthPrivParams(secParams *USMSecurityParameters) *USMSecurityParameters {
	zeroBytes := make([]byte, 12)
	return &USMSecurityParameters{
		authEngineId:    secParams.AuthoritativeEngineID(),
		authEngineBoots: secParams.authoritativeEngineBoots(),
		authEngineTime:  secParams.authoritativeEngineTime(),
		authUserName:    secParams.userName(),
		authParams:      zeroBytes, //Zeroed Array of 12 Bytes
		privParams:      nil,       //Zeroed
	}
}
