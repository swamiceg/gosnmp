/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/engine"
	"webnms/snmp/msg"
)

//USM interface is an extension of engine.SnmpSecurityModel interface.
//Defines methods specific to User-based security model.
type USMModel interface {
	engine.SnmpSecurityModel //Extends SnmpSecurityModel

	//Lcd returns instance of USM UserLCD, which contains entries of USM users.
	Lcd() USMUserLCD

	//SetLCD sets the USMUserLCD instance on this usm model. This will override the user details that exist already.
	SetLCD(USMUserLCD)

	//Services provided and used within USM.
	authenticateOutgoingMsg(msgId int32, msgMaxSize int32, msgFlags byte,
		msgSecurityModel int32, contextEngineId []byte, contextName []byte,
		pduData msg.SnmpPDU, encryptedScopedPDU []byte, secParams *USMSecurityParameters,
		usmCache *USMSecurityCache,
	) []byte
	authenticateIncomingMsg(msgId int32, msgMaxSize int32, msgFlags byte,
		msgSecurityModel int32, contextEngineId []byte, contextName []byte,
		pduData msg.SnmpPDU, encryptedScopedPDU []byte, secParams *USMSecurityParameters,
		usmCache *USMSecurityCache,
	) []byte
	//Return encrypted byte array along with SALT (8 Octet)
	encryptData(*USMSecurityParameters, *USMSecurityCache, []byte) ([]byte, []byte)
	//Return decrypted ScopedPDU
	decryptData(*USMSecurityParameters, *USMSecurityCache, []byte) (msg.ScopedPDU, error)

	//Counters

	//Returns the unsupportedSecLevels Counter.
	UnsupportedSecLevelsCounter() uint32

	//Returns the notInTimeWindows Counter.
	NotInTimeWindowsCounter() uint32

	//Returns the unknownUserNames Counter.
	UnknowUserNamesCounter() uint32

	//Returns the unknownEngineIds Counter.
	UnknownEngineIdsCounter() uint32

	//Returns the wrongDigests Counter.
	WrongDigestsCounter() uint32

	//Returns the decryptionErrors Counter.
	DecryptionErrorsCounter() uint32
}
