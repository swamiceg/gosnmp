/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"

	"bytes"
	"testing"
)

var secUsersTest = []struct {
	engID    []byte
	userName string
	authAlgo consts.AuthProtocol
	authKey  []byte
	privAlgo consts.PrivProtocol
	privKey  []byte
	secLevel consts.SecurityLevel
}{
	{engId, "Swami", NO_AUTH, authKey, NO_PRIV, nil, security.NoAuthNoPriv},
	{engId, "Swami", MD5_AUTH, authKey, NO_PRIV, nil, security.AuthNoPriv},
	{engId, "Swami", MD5_AUTH, authKey, DES_PRIV, privKey, security.AuthPriv},
	{engId, "Swami", NO_AUTH, authKey, DES_PRIV, privKey, security.NoAuthNoPriv},
	{engId, "", NO_AUTH, authKey, DES_PRIV, privKey, security.NoAuthNoPriv},
	{nil, "", NO_AUTH, authKey, DES_PRIV, privKey, security.NoAuthNoPriv},
}

var engId = []byte{1, 2, 3, 4, 5}
var authKey = []byte{1, 0, 1, 1}
var privKey = []byte{1, 0, 1, 1}

func TestSecureUser(t *testing.T) {
	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 10)
	userLCD := newUSMLCD(eng)
	algoMgr := NewUSMAlgorithmMgr()
	userLCD.SetAlgorithmManager(algoMgr)

	//Register the Authentication algorithms
	md5 := NewMD5()
	algoMgr.RegisterAlgorithm(md5)

	sha := NewSHA1()
	algoMgr.RegisterAlgorithm(sha)

	//Register the Privacy algorithms
	des := NewDES(eng)
	algoMgr.RegisterAlgorithm(des)

	tdes := NewTripleDES(eng)
	algoMgr.RegisterAlgorithm(tdes)

	aes128 := NewAES128(eng)
	algoMgr.RegisterAlgorithm(aes128)

	aes192 := NewAES192(eng)
	algoMgr.RegisterAlgorithm(aes192)

	aes256 := NewAES256(eng)
	algoMgr.RegisterAlgorithm(aes256)

	for _, secuser := range secUsersTest {
		user := NewUSMSecureUser(userLCD, secuser.userName, secuser.engID)
		user.SetAuthAlgorithm(secuser.authAlgo)
		user.SetPrivAlgorithm(secuser.privAlgo)
		user.SetAuthKey(secuser.authKey)
		user.SetPrivKey(secuser.privKey)

		if user.SecurityLevel() != secuser.secLevel {
			t.Errorf("SecureUser: Invalid security level. Expected: %d. Got: %d.", secuser.secLevel, user.SecurityLevel())
		}
		if !bytes.Equal(user.EngineID(), secuser.engID) {
			t.Errorf("SecureUser: Invalid EngineID. Expected: %v. Got: %v.", secuser.engID, user.EngineID())
		}
		if user.UserName() != secuser.userName {
			t.Errorf("SecureUser: Invalid UserName. Expected: %s. Got: %s.", secuser.userName, user.UserName())
		}
		if !bytes.Equal(user.AuthKey(), secuser.authKey) {
			t.Errorf("SecureUser: Invalid AuthKey. Expected: %v. Got: %v.", secuser.authKey, user.AuthKey())
		}
		if !bytes.Equal(user.PrivKey(), secuser.privKey) {
			t.Errorf("SecureUser: Invalid PrivKey. Expected: %v. Got: %v.", secuser.privKey, user.PrivKey())
		}
		if secuser.authAlgo != NO_AUTH && user.AuthProtocol().OID() != string(secuser.authAlgo) {
			t.Errorf("SecureUser: Invalid AuthAlgorithm. Expected: %s. Got: %s.", secuser.authAlgo, user.AuthAlgorithm())
		}
		if secuser.privAlgo != NO_PRIV && user.PrivProtocol().OID() != string(secuser.privAlgo) {
			t.Errorf("SecureUser: Invalid PrivAlgorithm. Expected: %s. Got: %s.", secuser.privAlgo, user.PrivAlgorithm())
		}

		//Delta and keychange test
		oldPwd := "OldPassword"
		newPwd := "NewPassword"
		random := []byte{1, 2, 3, 4, 4, 5, 6}

		if user.AuthProtocol() != nil {
			oldAuthKey := user.AuthProtocol().LocalizeAuthKey(user.AuthProtocol().PasswordToKey(oldPwd), engId)
			user.SetAuthKey(oldAuthKey)
			newAuthKey := user.AuthProtocol().LocalizeAuthKey(user.AuthProtocol().PasswordToKey(newPwd), engId)
			delta := user.AuthDelta(newAuthKey, random)
			user.SetAuthKeyChange(append(random, delta...))
			if !bytes.Equal(user.AuthKey(), newAuthKey) {
				t.Errorf("SecureUser: Failure in keychange. Expected: %v. Got: %v.", newAuthKey, user.AuthKey())
			}
		}
	}

}
