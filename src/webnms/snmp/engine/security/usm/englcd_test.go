/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/engine"

	//"fmt"
	"testing"
	"time"
)

var hostPortTest = []struct {
	host string
	port int
	res  bool
}{
	{"localhost", 1000, true},
	{"1.1.1.1", 111, true},
	{"aa", 11, false},
	{"localhost", -100, false},
}

func TestUSMPeerEngine(t *testing.T) {
	for _, hp := range hostPortTest {
		peerEngine := NewUSMPeerEngine(hp.host, hp.port)
		if (peerEngine == nil && hp.res) || (peerEngine != nil && !hp.res) {
			t.Errorf("NewUSMPeerEngine: Unexpected result.")
		}
	}

	//AuthoritativeEngineTime test
	peerEngine := NewUSMPeerEngine("10.1.1.2", 100)
	var et int32 = 100
	peerEngine.SetAuthoritativeEngineTime(et)
	time.Sleep(time.Second * 2)
	if peerEngine.AuthoritativeEngineTime() != et+2 {
		t.Errorf("AuthoritativeEngineTime: Not returning updated engine time. Expected: %d. Got: %d.", et+2, peerEngine.AuthoritativeEngineTime())
	}
}

var addEngTest = []struct {
	address string
	port    int
	engID   []byte
	eb      int32
	et      int32
}{
	{"1.1.1.1", 8001, []byte{1, 2, 34, 1, 0}, 100, 121},
	{"1.1.1.2", 8001, []byte{1, 2, 34, 1, 1}, 2147483647, 2147483647},
	{"1.1.1.3", 8001, []byte{1, 2, 34, 1, 1}, 100, 2147483647},
	{"1.1.1.4", 8001, []byte{1, 2, 34, 1, 2}, 1, 100},
}

func TestAddEngine(t *testing.T) {
	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 1)
	engLCD := newUSMPeerEngineLCD(eng)

	for _, e := range addEngTest {
		eng := NewUSMPeerEngine(e.address, e.port)
		eng.SetAuthoritativeEngineID(e.engID)
		eng.SetAuthoritativeEngineBoots(e.eb)
		eng.SetAuthoritativeEngineTime(e.et)

		err := engLCD.AddEngine(eng)
		if err != nil {
			t.Error(err)
		} else {
			time.Sleep(time.Second * 1)
			usmEng := engLCD.Engine(e.address, e.port)
			if usmEng == nil {
				t.Error("AddEngine: Failure in adding the SnmpEngine.")
			} else {
				var newEB int32 = 2323
				usmEng.SetAuthoritativeEngineBoots(newEB)
				usmEng = engLCD.Engine(e.address, e.port)
				if usmEng.AuthoritativeEngineBoots() != newEB {
					t.Errorf("Engine values are not getting updated. Expected: %d. Got: %d.", newEB, usmEng.AuthoritativeEngineBoots())
				}
			}

			usmEng = engLCD.EngineByID(e.engID)
			if usmEng == nil {
				t.Error("AddEngine: Failure in adding the SnmpEngine.")
			}
			err = engLCD.RemoveEngine(e.address, e.port)
			if err != nil {
				t.Error(err)
			}
		}
	}
}

func TestUpdateAndRemoveEngine(t *testing.T) {

	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 100, 1)
	engLCD := newUSMPeerEngineLCD(eng)

	engID := []byte{1, 1, 2, 3}
	usmEng := NewUSMPeerEngine("1.1.1.1", 161)
	usmEng.SetAuthoritativeEngineID(engID)
	usmEng.SetAuthoritativeEngineBoots(11)
	usmEng.SetAuthoritativeEngineTime(2134)

	//First add the engine
	err := engLCD.AddEngine(usmEng)
	if err != nil {
		t.Error(err)
	}

	var newEB int32 = 11111
	var newET int32 = 1232

	//Update the values of EB/ET for the engine
	usmEng = NewUSMPeerEngine("1.1.1.1", 161)
	usmEng.SetAuthoritativeEngineID(engID)
	usmEng.SetAuthoritativeEngineBoots(newEB)
	usmEng.SetAuthoritativeEngineTime(newET)
	err = engLCD.AddEngine(usmEng) //This should update the values and should not create new entry
	if err != nil {
		t.Error(err)
	}
	if len(engLCD.Engines()) != 1 {
		t.Errorf("EngineLCD: Unexpected no. of engine entries. Expected: %d. Got: %d.", 1, len(engLCD.Engines()))
	}

	peerEngine := engLCD.Engine("1.1.1.1", 161)
	if peerEngine.AuthoritativeEngineBoots() != newEB {
		t.Errorf("EngineLCD: Failure in updating engine boots. Expected: %d. Got: %d.", newEB, peerEngine.AuthoritativeEngineBoots())
	}
	if peerEngine.AuthoritativeEngineTime() != newET {
		t.Errorf("EngineLCD: Failure in updating engine time. Expected: %d. Got: %d.", newET, peerEngine.AuthoritativeEngineTime())
	}

	//Remove all engines test
	engLCD.AddEngine(NewUSMPeerEngine("1.2.3.3", 232))
	engLCD.AddEngine(NewUSMPeerEngine("1.2.3.4", 232))
	engLCD.RemoveAllEngines()
	if len(engLCD.Engines()) > 0 {
		t.Errorf("EngineLCD: Failure in removing all engine entries. Entries length: %d.", len(engLCD.Engines()))
	}

	engLCD.AddEngine(NewUSMPeerEngine("1.2.3.3", 232))
	engLCD.AddEngine(NewUSMPeerEngine("1.2.3.4", 232))
	for _, eng := range engLCD.Engines() {
		engLCD.RemoveEngineByEntry(eng)
	}
	if len(engLCD.Engines()) > 0 {
		t.Error("EngineLCD: Failure in removing all the engine entries from EngLCD.")
	}

}
