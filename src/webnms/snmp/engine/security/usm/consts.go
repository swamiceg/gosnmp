/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/consts"
)

//Constants used in the User based security model layer of the API.
const (
	//Constants required for USM based authentication protocols.
	NO_AUTH  consts.AuthProtocol = "1.3.6.1.6.3.10.1.1.1"
	MD5_AUTH consts.AuthProtocol = "1.3.6.1.6.3.10.1.1.2"
	SHA_AUTH consts.AuthProtocol = "1.3.6.1.6.3.10.1.1.3"

	//Constants required for USM based privacy protocols.
	NO_PRIV         consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.1"
	DES_PRIV        consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.2"
	TRIPLE_DES_PRIV consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.3"
	AES_128_PRIV    consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.4"
	AES_192_PRIV    consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.20"
	AES_256_PRIV    consts.PrivProtocol = "1.3.6.1.6.3.10.1.2.21"
)

//Internal use
const (
	//Protocols - SNMP-USER-BASED-SM-MIB
	usmNoAuthProtocol      string = "1.3.6.1.6.3.10.1.1.1"
	usmHMACMD5AuthProtocol string = "1.3.6.1.6.3.10.1.1.2" //RFC3414
	usmHMACSHAAuthProtocol string = "1.3.6.1.6.3.10.1.1.3" //RFC3414
	usmNoPrivProtocol      string = "1.3.6.1.6.3.10.1.2.1"
	usmDESPrivProtocol     string = "1.3.6.1.6.3.10.1.2.2"  //RFC3414
	usm3DESEDEPrivProtocol string = "1.3.6.1.6.3.10.1.2.3"  //Non-SNMP Standard - CISCO-SNMP-USM-OIDS-MIB/SNMP-USER-BASED-SM-3DES-MIB
	usmAesCfb128Protocol   string = "1.3.6.1.6.3.10.1.2.4"  //RFC3826
	usmAesCfb192Protocol   string = "1.3.6.1.6.3.10.1.2.20" //Non-SNMP Standard
	usmAesCfb256Protocol   string = "1.3.6.1.6.3.10.1.2.21" //Non-SNMP Standard

	//USM Report OIDs
	usmStatsUnsupportedSecLevels string = ".1.3.6.1.6.3.15.1.1.1.0"
	usmStatsNotInTimeWindows     string = ".1.3.6.1.6.3.15.1.1.2.0"
	usmStatsUnknownUserNames     string = ".1.3.6.1.6.3.15.1.1.3.0"
	usmStatsUnknownEngineIDs     string = ".1.3.6.1.6.3.15.1.1.4.0"
	usmStatsWrongDigests         string = ".1.3.6.1.6.3.15.1.1.5.0"
	usmStatsDecryptionErrors     string = ".1.3.6.1.6.3.15.1.1.6.0"

	engineTableId int32 = 111 //To register the peer engine table
)
