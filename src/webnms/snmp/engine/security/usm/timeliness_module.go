/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"bytes"
	"math"

	"webnms/snmp/engine"
)

//Timeliness module performs timeliness services required for authentication of messages.
//It uses timeliness values in an SNMP message to do timeliness checking.
//
//Used internally by User based Security model.
type timelinessModule struct {
	TimeWindow     int
	MaxEngineBoots int32
	*USMSecurityModel
}

func newUSMTimelinessModule(usmModel *USMSecurityModel) *timelinessModule {
	return &timelinessModule{
		USMSecurityModel: usmModel,
		TimeWindow:       150,
		MaxEngineBoots:   2147483647,
	}
}

//We are authoritative - RFC3414 Section 3.2 Step 7.a
//Perform Timeliness checks and return valid error if any
func (tm *timelinessModule) CheckAuthTimeliness(localEngine engine.SnmpEngine, params *USMSecurityParameters, msgFlags byte) *engine.StatusInformation {
	if !bytes.Equal(localEngine.SnmpEngineID(), params.AuthoritativeEngineID()) {
		//Just an additional check
		return tm.errGen.genUnknownEngineIdError(
			nil,
			"",
			msgFlags, //flags
			params,
		)
	}

	recBoots := params.authoritativeEngineBoots()
	recTime := params.authoritativeEngineTime()

	if localEngine.SnmpEngineBoots() == tm.MaxEngineBoots {
		//Generate NotInTimeWindowException - Should be in Auth-NoPriv security level
		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}
	if recBoots == 0 && recTime == 0 { //Discovery Request - Very first time
		params.setAuthoritativeEngineBoots(localEngine.SnmpEngineBoots())
		params.setAuthoritativeEngineTime(localEngine.SnmpEngineTime())

		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}

	//LocalEngine's EB/ET value will be set in USM's GenerateOutgoingMsg()
	if (recBoots != localEngine.SnmpEngineBoots()) || //Wrong EB value
		(recBoots == tm.MaxEngineBoots) {
		//Generate NotInTimeWindowException -  Do we need to provide EB/ET value?
		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}

	if int32(math.Abs(float64(recTime))) > (localEngine.SnmpEngineTime() + int32(tm.TimeWindow)) { //Not in Time Frame
		//Generate NotInTimeWindowException -  Do we need to provide EB/ET value?
		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}
	return nil
}

//We are Non-Authoritative - RFC3414 Section 3.2 Step 7.b
//We will update our local LCD based on the reports/responses
func (tm *timelinessModule) CheckNonAuthTimeliness(peer *USMPeerEngine, params *USMSecurityParameters, msgFlags byte) *engine.StatusInformation {
	//We are updating/storing the values of Authoritative Entity's Timeliness values in our EnginePeerTable, based on the report received.
	if peer.AuthoritativeEngineBoots() == 0 && peer.AuthoritativeEngineTime() == 0 { //Newly created entry
		peer.SetAuthoritativeEngineBoots(params.authoritativeEngineBoots())
		peer.SetAuthoritativeEngineTime(params.authoritativeEngineTime())
		peer.SetLatestReceivedEngineTime(params.authoritativeEngineTime())

		tm.peerEngineLCD.AddEngine(peer) //Update the entry
	}

	recBoots := params.authoritativeEngineBoots()
	recTime := params.authoritativeEngineTime()
	if recBoots == 0 && recTime == 0 { //This is possible with discovery response
		return nil //Received zero discovery response. No need to update our peer.
	}
	if (recBoots > peer.AuthoritativeEngineBoots()) ||
		(recBoots == peer.AuthoritativeEngineBoots() && recTime > peer.LatestReceivedEngineTime()) { //Update the entry
		peer.SetAuthoritativeEngineBoots(recBoots)
		peer.SetAuthoritativeEngineTime(recTime)
		peer.SetLatestReceivedEngineTime(recTime)

		tm.peerEngineLCD.AddEngine(peer) //Update the entry

		return nil
	}

	//Local EngineBoots
	if peer.AuthoritativeEngineBoots() == tm.MaxEngineBoots {
		//Generate NotInTimeWindowException
		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}

	if (recBoots < peer.AuthoritativeEngineBoots()) || //Auth EB less than Local EB
		(recBoots == peer.AuthoritativeEngineBoots() && recTime < (peer.AuthoritativeEngineTime()-int32(tm.TimeWindow))) { //Auth ET more than 150 secs less than local ET
		//Generate NotInTimeWindowException
		return tm.errGen.genNotInTimeWindowError(nil,
			"",
			msgFlags,
			params,
		)
	}

	return nil
}
