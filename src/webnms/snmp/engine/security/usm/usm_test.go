/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package usm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/mpm"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport/udp"
	"webnms/snmp/msg"

	"testing"
	//"fmt"
)

var usmStructTest = []struct {
	msgID      int32
	userName   string
	secParams  []byte
	scopedPDU  []byte
	maxSize    int32
	secLevel   consts.SecurityLevel
	reportable byte
	result     bool
}{
	{4, noAuthUser, []byte{48, 38, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 0, 2, 1, 0, 4, 10, 110, 111, 65, 117, 116, 104, 85, 115, 101, 114, 4, 0, 4, 0, 48, 51, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 6, 110, 111, 65, 117, 116, 104, 160, 25, 2, 1, 3, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}, []byte{48, 51, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 6, 110, 111, 65, 117, 116, 104, 160, 25, 2, 1, 3, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}, 496173, security.NoAuthNoPriv, security.ReportableFlag, true}, //Valid NoAuthNoPriv request
	{6, authUser, []byte{48, 50, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 145, 196, 4, 8, 97, 117, 116, 104, 85, 115, 101, 114, 4, 12, 118, 65, 109, 134, 71, 188, 174, 19, 128, 100, 123, 157, 4, 0}, []byte{48, 49, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 4, 97, 117, 116, 104, 160, 25, 2, 1, 5, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}, 65507, security.AuthNoPriv, security.ReportableFlag, true},                                                                                                                                                         //Valid AuthNoPriv request
	{6, privUser, []byte{48, 58, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 146, 45, 4, 8, 112, 114, 105, 118, 85, 115, 101, 114, 4, 12, 41, 71, 113, 52, 95, 126, 94, 18, 3, 28, 219, 109, 4, 8, 90, 33, 1, 100, 67, 60, 150, 212}, []byte{69, 146, 232, 230, 94, 203, 104, 57, 14, 111, 207, 134, 52, 146, 134, 134, 226, 36, 176, 111, 27, 99, 176, 23, 168, 158, 103, 133, 39, 140, 75, 106, 177, 22, 101, 201, 15, 143, 60, 210, 237, 175, 222, 114, 130, 252, 52, 5, 222, 195, 27, 219, 9, 145, 163, 17}, 65507, security.AuthPriv, security.ReportableFlag, true},                                                     //Valid AuthPriv request

	{4, noAuthUser, []byte{48, 40, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 147, 116, 4, 10, 110, 111, 65, 117, 116, 104, 85, 115, 101, 114, 4, 0, 4, 0}, []byte{48, 61, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 6, 110, 111, 65, 117, 116, 104, 162, 35, 2, 1, 3, 2, 1, 0, 2, 1, 0, 48, 24, 48, 22, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 4, 10, 115, 121, 115, 67, 111, 110, 116, 97, 99, 116}, 65507, security.NoAuthNoPriv, byte(0), true},                                                                                                                                   //Valid NoAuthNoPriv response
	{6, authUser, []byte{48, 50, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 146, 236, 4, 8, 97, 117, 116, 104, 85, 115, 101, 114, 4, 12, 149, 50, 33, 235, 25, 53, 12, 143, 178, 147, 157, 116, 4, 0}, []byte{48, 59, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 4, 97, 117, 116, 104, 162, 35, 2, 1, 5, 2, 1, 0, 2, 1, 0, 48, 24, 48, 22, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 4, 10, 115, 121, 115, 67, 111, 110, 116, 97, 99, 116}, 65507, security.AuthNoPriv, byte(0), true},                                                                                                    //Valid AuthNoPriv response
	{6, privUser, []byte{48, 58, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 146, 152, 4, 8, 112, 114, 105, 118, 85, 115, 101, 114, 4, 12, 91, 106, 232, 122, 88, 77, 78, 40, 25, 47, 110, 14, 4, 8, 163, 66, 70, 152, 43, 62, 160, 21}, []byte{192, 250, 206, 181, 7, 156, 250, 201, 100, 108, 217, 124, 249, 183, 199, 194, 83, 25, 212, 153, 154, 30, 163, 88, 93, 143, 46, 202, 224, 176, 252, 198, 90, 253, 2, 130, 167, 169, 134, 206, 127, 245, 203, 40, 217, 136, 230, 122, 199, 77, 121, 69, 34, 93, 214, 241, 252, 249, 233, 33, 130, 14, 162, 22}, 65507, security.AuthPriv, byte(0), true}, //Valid AuthPriv response

	{6, privUser, []byte{48, 58, 4, 14, 50, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 146, 152, 4, 8, 112, 114, 105, 118, 85, 115, 101, 114, 4, 12, 91, 106, 232, 122, 88, 77, 78, 40, 25, 47, 110, 14, 4, 8, 163, 66, 70, 152, 43, 62, 160, 21}, []byte{192, 250, 206, 181, 7, 156, 250, 201, 100, 108, 217, 124, 249, 183, 199, 194, 83, 25, 212, 153, 154, 30, 163, 88, 93, 143, 46, 202, 224, 176, 252, 198, 90, 253, 2, 130, 167, 169, 134, 206, 127, 245, 203, 40, 217, 136, 230, 122, 199, 77, 121, 69, 34, 93, 214, 241, 252, 249, 233, 33, 130, 14, 162, 22}, 65507, security.AuthPriv, byte(0), false}, //Invalid AuthPriv response - UnknownEngineID
	{6, authUser, []byte{48, 50, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 7, 145, 196, 4, 8, 100, 117, 116, 104, 85, 115, 101, 114, 4, 12, 118, 65, 109, 134, 71, 188, 174, 19, 128, 100, 123, 157, 4, 0}, []byte{48, 49, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 4, 97, 117, 116, 104, 160, 25, 2, 1, 5, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}, 65507, security.AuthNoPriv, security.ReportableFlag, false},                                                                                                                                 //Invalid AuthNoPriv request - UnknownUserName
	{6, privUser, []byte{48, 58, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 69, 2, 3, 7, 146, 45, 4, 8, 112, 114, 105, 118, 85, 115, 101, 114, 4, 12, 41, 71, 113, 52, 95, 126, 94, 18, 3, 28, 219, 109, 4, 8, 90, 33, 1, 100, 67, 60, 150, 212}, []byte{69, 146, 232, 230, 94, 203, 104, 57, 14, 111, 207, 134, 52, 146, 134, 134, 226, 36, 176, 111, 27, 99, 176, 23, 168, 158, 103, 133, 39, 140, 75, 106, 177, 22, 101, 201, 15, 143, 60, 210, 237, 175, 222, 114, 130, 252, 52, 5, 222, 195, 27, 219, 9, 145, 163, 17}, 65507, security.AuthPriv, security.ReportableFlag, false},                              //Invalid AuthPriv request - Authentication Failure

	{6, authUser, []byte{48, 50, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 10, 104, 36, 4, 8, 97, 117, 116, 104, 85, 115, 101, 114, 4, 12, 192, 33, 252, 201, 132, 8, 248, 203, 80, 171, 97, 211, 4, 0}, []byte{48, 59, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 4, 97, 117, 116, 104, 162, 35, 2, 1, 5, 2, 1, 0, 2, 1, 0, 48, 24, 48, 22, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 4, 10, 115, 121, 115, 67, 111, 110, 116, 97, 99, 116}, 65507, security.AuthNoPriv, byte(0), false}, //Invalid AuthNoPriv response - WrongDigests

	{6, privUser, []byte{48, 58, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 70, 2, 3, 10, 102, 122, 4, 8, 112, 114, 105, 118, 85, 115, 101, 114, 4, 12, 164, 195, 252, 213, 18, 207, 227, 238, 24, 250, 179, 80, 4, 8, 205, 154, 56, 30, 249, 23, 48, 169}, []byte{198, 33, 221, 214, 188, 112, 189, 151, 38, 16, 97, 49, 140, 68, 19, 251, 149, 160, 75, 143, 187, 190, 29, 97, 13, 9, 217, 122, 81, 23, 140, 69, 54, 112, 155, 158, 200, 131, 22, 117, 77, 252, 123, 34, 163, 255, 135, 111, 107, 241, 187, 75, 173, 84, 239, 197, 118, 41, 188, 69, 214, 13, 34, 79}, 65507, security.AuthPriv, byte(0), false}, //Invalid AuthPriv response - Decryption error

}

var usmOutgoingMessageTest = []struct {
	msgID      int32
	engID      []byte
	userName   string
	maxSize    int32
	secLevel   consts.SecurityLevel
	reportable byte
	result     bool
}{
	{4, engID, noAuthUser, 65507, security.NoAuthNoPriv, security.ReportableFlag, true},
	{6, engID, authUser, 65507, security.AuthNoPriv, security.ReportableFlag, true},
	{6, engID, privUser, 65507, security.AuthPriv, security.ReportableFlag, true},

	{6, []byte{1, 2, 3}, privUser, 65507, security.AuthPriv, security.ReportableFlag, false}, //UnknownEngineID
	{6, engID, "invaliduser", 65507, security.AuthPriv, security.ReportableFlag, false},      //UnknownUserName
	{6, engID, authUser, 65507, security.AuthPriv, security.ReportableFlag, false},           //InvalidSecurityLevel
	{6, engID, privUser, 65507, security.AuthNoPriv, security.ReportableFlag, true},
}
var (
	noAuthUser string = "noAuthUser"
	authUser   string = "authUser"
	privUser   string = "privUser"
)
var engID = []byte{49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49}

func TestUSM(t *testing.T) {
	/* USM Configuration*/
	var eb int32 = 70
	var et int32 = 496068

	authAlgo := MD5_AUTH
	authPwd := "MD5_AUTH"
	privAlgo := DES_PRIV
	privPwd := "DES_PRIV"

	/*Configure the SnmpEngine*/
	eng := engine.NewSnmpEngine(engine.NewSnmpLCD(), "localhost", 161, 11)
	eng.SetSnmpEngineID(engID)
	eng.SetSnmpEngineBoots(eb)
	eng.SetSnmpEngineTime(et)

	secSS := engine.NewSecuritySubSystem(eng)
	msgSS := engine.NewMsgProcessingSubSystem(eng)
	v1mp := mpm.NewMsgProcessingModelV1(msgSS)
	msgSS.AddModel(v1mp) //Register this MPM in Msg Processing SubSystem
	v2cmp := mpm.NewMsgProcessingModelV2C(msgSS)
	msgSS.AddModel(v2cmp)
	v3mp := mpm.NewMsgProcessingModelV3(msgSS)
	msgSS.AddModel(v3mp)
	eng.SetMsgProcessingSubSystem(msgSS)
	eng.SetSecuritySubSystem(secSS)

	/*Configure the USM*/
	usmModel := NewUSMSecurityModel(secSS)
	secCache := usmModel.CreateNewSecurityCache()
	udp := new(udp.UDPProtocolOptions)
	udp.SetRemoteHost("localhost")
	udp.SetRemotePort(8001)

	/*Configure the USM users*/
	userLCD := usmModel.Lcd()
	userLCD.AddUser(engID, noAuthUser, NO_AUTH, "", NO_PRIV, "")
	userLCD.AddUser(engID, authUser, authAlgo, authPwd, NO_PRIV, "")
	userLCD.AddUser(engID, privUser, authAlgo, authPwd, privAlgo, privPwd)
	/*secParams := []byte{48, 38, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 2, 1, 0, 2, 1, 0, 4, 10, 110, 111, 65, 117, 116, 104, 85, 115, 101, 114, 4, 0, 4, 0, 48, 51, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 6, 110, 111, 65, 117, 116, 104, 160, 25, 2, 1, 3, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}
	scopedPDU := []byte{48, 51, 4, 14, 49, 50, 55, 46, 48, 46, 48, 46, 49, 35, 56, 48, 48, 49, 4, 6, 110, 111, 65, 117, 116, 104, 160, 25, 2, 1, 3, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 4, 0, 5, 0}
	*/

	//ProcessIncoming Message
	for _, u := range usmStructTest {
		_, _, _, statusInfo := usmModel.processIncomingMessage(secCache,
			u.msgID,
			u.maxSize,
			byte(u.secLevel)|u.reportable,
			u.secParams,
			security.USMID,
			nil,
			0,
			u.scopedPDU, //ScopedPDU
			udp,
			nil,
		)
		if (u.result && statusInfo != nil) || (!u.result && statusInfo == nil) {
			t.Error("USM ProcessIncomingMessage: Invalid error returned.")
		}
	}

	sp := msg.ScopedPDU{}
	sp.SetCommand(consts.GetRequest)
	//ProcessOutgoing Message
	for _, u := range usmOutgoingMessageTest {
		_, _, _, statusInfo := usmModel.generateOutgoingMsg(consts.Version3,
			u.msgID,
			u.maxSize,
			byte(u.secLevel)|u.reportable,
			security.USMID,
			u.userName,
			u.engID,
			sp,
			secCache,
			udp,
		)
		if (u.result && statusInfo != nil) || (!u.result && statusInfo == nil) {
			t.Error("USM ProcessOutgoingMessage: Invalid error returned.")
		}
	}
}
