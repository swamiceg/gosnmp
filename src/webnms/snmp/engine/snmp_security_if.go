/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

//SecurityParameters are security model dependent. Each security model should provide its own security parameters implementation.
//SecurityParameters generally composed of - EngineID, EngineBoots, EngineTime, UserName, AuthParameters, PrivacyParameters
//
//When implementing custom security models other than USM, user should provide implementation for this interface, for seamless integration with Engine's SubSystems.
type SecurityParameters interface {
	//SecurityEngineID returns the EngineID in the security paramters field as a byte array.
	SecurityEngineID() []byte

	//SecurityName returns the security name (UserName) in the security parameters field.
	SecurityName() string

	//Encode the security parameters and returns an encoded byte array.
	EncodeSecurityParameters() ([]byte, error)

	//Decode the byte array passed and returns the security parameters instance.
	//Returns error in case of failure in decoding the byte array.
	DecodeSecurityParameters([]byte) error
}

//StateReference interface is used to cache the incoming/outgoing SnmpMessages in the message processing layer.
//Each message processing model should implement this interface to cache the msg data.
//
//Message processing model should fill the cache for incoming requests (Confirmed class),
//which should be later used while sending a response for that request.
type StateReference interface {
	//Can return RequestID for v1/v2c
	MsgID() int32
}

//SecurityStateReference interface is used to cache the incoming/outgoing SnmpMessages in the security layer.
//Every security model should implement this interface to cache the securityData.
//
//Security model should fill the cache for incoming requests (Confirmed class),
//which should be later used while sending a response for that request.
type SecurityStateReference interface { //SecurityStateReference
	//Should return the MsgID of the request to which this cache belongs
	MsgID() int32
}
