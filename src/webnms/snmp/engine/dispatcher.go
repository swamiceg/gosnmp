/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

//Dispatcher interface is used for sending/receiving Snmp Messages.
//SnmpSession is the only default implementation of Dispatcher interface. Refer snmp.SnmpSession type for more details.
//
//Note: This interface type extends (Embeds) SnmpSubSystem interface.
type Dispatcher interface { //Extends SnmpSubSystem interface
	SnmpSubSystem

	//Send asychronously sends SnmpMessage to the destination and returns the MsgID/RequestID of the message sent.
	Send(msg.SnmpMessage) (int32, error)

	//SyncSend sends the message synchronously and returns the response message received. It blocks for the response until timeout occurs.
	//Returns nil and error if no response received.
	SyncSend(msg.SnmpMessage) (*msg.SnmpMessage, error)

	//ReturnResponseMsg is used to return the response message to the remote entity in the event of incoming request.
	ReturnResponseMsg(msg.SnmpMessage, StateReference) error

	//ProtocolOptions returns the transport ProtocolOptions instance set on this dispatcher.
	ProtocolOptions() transport.ProtocolOptions
}
