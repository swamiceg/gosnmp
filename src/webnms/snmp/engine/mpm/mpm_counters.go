/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"sync"
)

//To store the counters specific to MsgProcessing Layer.
type mpmCounters struct {
	snmpInASNParseErrs        uint32
	snmpUnknownSecurityModels uint32
	snmpInvalidMsgs           uint32
}

//To maintain single counter instance for all MPM
var mpmCounterInstance *mpmCounters = nil
var once sync.Once

func initMpmCounters() *mpmCounters {
	once.Do(func() {
		mpmCounterInstance = new(mpmCounters)
	})
	return mpmCounterInstance
}

//Returns SnmpInASNParseErrs counter. SnmpInASNParseErrs is the total number of ASN.1 or BER errors encountered by
//the SNMP entity when decoding received SNMP messages.
func (mc *mpmCounters) SnmpInASNParseErrsCounter() uint32 {
	return mc.snmpInASNParseErrs
}

//Increments SnmpInASNParseErrs counter by 1. SnmpInASNParseErrs is the total number of ASN.1 or BER errors encountered by
//the SNMP entity when decoding received SNMP messages.
func (mc *mpmCounters) IncSnmpInASNParseErrsCounter() uint32 {
	mc.snmpInASNParseErrs++
	return mc.snmpInASNParseErrs
}

//Returns SnmpUnknownSecurityModels counter. SnmpUnknownSecurityModels is the total number of messages received
//with security models not supported by the entity.
func (mc *mpmCounters) SnmpUnknownSecurityModelsCounter() uint32 {
	return mc.snmpUnknownSecurityModels
}

func (mc *mpmCounters) incSnmpUnknownSecurityModelsCounter() uint32 {
	mc.snmpUnknownSecurityModels++
	return mc.snmpUnknownSecurityModels
}

//Returns SnmpInvalidMsgs counter. SnmpInvalidMsgs is the total number of invalid messages
//received by the entity.
func (mc *mpmCounters) SnmpInvalidMsgsCounter() uint32 {
	return mc.snmpInvalidMsgs
}

func (mc *mpmCounters) incSnmpInvalidMsgsCounter() uint32 {
	mc.snmpInvalidMsgs++
	return mc.snmpInvalidMsgs
}
