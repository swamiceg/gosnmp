/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	"webnms/snmp/engine/transport"
	"webnms/snmp/engine/transport/udp"
	"webnms/snmp/msg"

	//"fmt"
	"encoding/hex"
	"errors"
	"strconv"
	"testing"
)

//v2MsgResp := []byte{48, 46, 2, 1, 1, 4, 6, 112, 117, 98, 108, 105, 99, 162, 33, 2, 1, 117, 2, 1, 0, 2, 1, 0, 48, 22, 48, 20, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 4, 8, 115, 121, 115, 68, 101, 115, 99, 114}

var v3mpTestStruct = []struct {
	msgReqBytes  []byte
	msgRespBytes []byte
	secID        int32
	secIDErr     error
	decErr       error
}{
	{v3MsgReq, v3MsgResp, security.USMID, nil, nil},                         //Valid case
	{[]byte("swami"), []byte("swami"), security.USMID, nil, errors.New("")}, //decode error
	{v3MsgReq, v3MsgResp, security.USMID, nil, nil},
	{v3MsgReq, v3MsgResp, security.V1sec, errors.New(""), nil}, //Invalid sec model error
}
var v3MsgResp = []byte{48, 120, 2, 1, 3, 48, 14, 2, 1, 1, 2, 3, 0, 255, 227, 4, 1, 0, 2, 1, 3, 4, 40, 48, 38, 4, 12, 245, 50, 18, 194, 83, 139, 70, 140, 165, 110, 199, 228, 2, 1, 69, 2, 3, 1, 78, 79, 4, 10, 110, 111, 65, 117, 116, 104, 85, 115, 101, 114, 4, 0, 4, 0, 48, 57, 4, 12, 245, 50, 18, 194, 83, 139, 70, 140, 165, 110, 199, 228, 4, 6, 110, 111, 65, 117, 116, 104, 162, 33, 2, 1, 124, 2, 1, 0, 2, 1, 0, 48, 22, 48, 20, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 4, 8, 115, 121, 115, 68, 101, 115, 99, 114}

var v3MsgReq = []byte{48, 110, 2, 1, 3, 48, 14, 2, 1, 1, 2, 3, 0, 255, 227, 4, 1, 4, 2, 1, 3, 4, 38, 48, 36, 4, 12, 245, 50, 18, 194, 83, 139, 70, 140, 165, 110, 199, 228, 2, 1, 0, 2, 1, 0, 4, 10, 110, 111, 65, 117, 116, 104, 85, 115, 101, 114, 4, 0, 4, 0, 48, 49, 4, 12, 245, 50, 18, 194, 83, 139, 70, 140, 165, 110, 199, 228, 4, 6, 110, 111, 65, 117, 116, 104, 160, 25, 2, 1, 124, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 5, 0}

var remoteHost string = "localhost"
var remotePort int = 8001

func TestV3MP(t *testing.T) {
	lcd := engine.NewSnmpLCD()
	eng := engine.NewSnmpEngine(lcd, "localhost", 8001, 10)
	msgSS := engine.NewMsgProcessingSubSystem(eng)
	secSS := engine.NewSecuritySubSystem(eng)
	eng.SetMsgProcessingSubSystem(msgSS)
	eng.SetSecuritySubSystem(secSS)

	secSS.AddModel(usm.NewUSMSecurityModel(secSS))
	//Add the engine manually for testing
	engLCD := secSS.PeerEngineLCD(security.USMID)
	peerEngine := usm.NewUSMPeerEngine(remoteHost, remotePort)
	engID, _ := hex.DecodeString("f53212c2538b468ca56ec7e4")
	peerEngine.SetAuthoritativeEngineID(engID)
	eng.SetSnmpEngineID(engID)
	engLCD.AddEngine(peerEngine)
	//Add the User detail in USMUserLCD
	userLCD := lcd.ModelLCD(secSS, security.USMID)
	userLCD.(usm.USMUserLCD).AddUser(engID, "noAuthUser", "", "", "", "")

	secName := "noAuthUser"
	ctxName := "noAuth"
	disp := newDispatcher(eng)

	v3mp := NewMsgProcessingModelV3(msgSS)
	if v3mp == nil {
		t.Fatal("v3mp: Failure in creating v3mp instance.")
	}

	udp := new(udp.UDPProtocolOptions)
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	for _, mpTest := range v3mpTestStruct {
		//Test PrepareOutgoingMessage method - Outgoing Request
		pdu := msg.SnmpPDU{}
		pdu.SetRequestID(118) //ReqID of next incoming response - for matching
		pdu.SetCommand(consts.GetRequest)
		msgID, _, _, err := v3mp.PrepareOutgoingMessage(udp,
			mpTest.secID,
			secName,
			security.NoAuthNoPriv,
			nil,
			ctxName,
			pdu,
			true,
		)
		if !(mpTest.secIDErr == nil && err == nil) && !(mpTest.secIDErr != nil && err != nil) {
			t.Errorf("v3mp: PrepareOutgoingMessage. Error: %v.", err)
		}

		if err == nil { //Receive response only when there is an outstanding request
			//Test PrepareDataElements method - Incoming Response
			if len(mpTest.msgRespBytes) > 10 {
				mpTest.msgRespBytes[9] = byte(msgID)
			}
			_, _, _, si := v3mp.PrepareDataElements(disp, //No Dispatcher instance
				udp,
				mpTest.msgRespBytes,
				len(mpTest.msgRespBytes),
				nil,
			)
			if !(mpTest.decErr == nil && si == nil) && !(mpTest.decErr != nil && si != nil) {
				t.Errorf("v3mp: PrepareDataElements-Incoming Response. Error: %v.", si)
			}
		}

		//Test PrepareDataElements  method - Incoming Request
		_, mesg, stateRef, si := v3mp.PrepareDataElements(disp, //No Dispatcher instance
			udp,
			mpTest.msgReqBytes,
			len(mpTest.msgReqBytes),
			nil,
		)
		if !(mpTest.decErr == nil && si == nil) && !(mpTest.decErr != nil && si != nil) {
			t.Errorf("v3mp: PrepareDataElements-Incoming Request. Error: %v.", si)
		}

		if mesg != nil && si == nil {
			//Test PrepareResponseMessage method - Outgoing Response
			pdu.SetCommand(consts.GetResponse)
			pdu.SetRequestID(mesg.RequestID())
			_, _, err := v3mp.PrepareResponseMessage(udp,
				65535,
				mpTest.secID,
				secName,
				security.NoAuthNoPriv,
				nil,
				"",
				pdu,
				stateRef,
				nil,
			)
			if !(mpTest.secIDErr == nil && err == nil) && !(mpTest.secIDErr != nil && err != nil) {
				t.Errorf("v3mp: PrepareResponseMessage. Error: %v.", err)
			}
		}
	}
}

type dispatcher struct {
	*snmpSubSystemImpl
}

func newDispatcher(eng engine.SnmpEngine) *dispatcher {
	return &dispatcher{
		snmpSubSystemImpl: newSubSystem(eng),
	}
}

func (d *dispatcher) Send(msg.SnmpMessage) (int32, error) {
	return 1, nil
}

func (d *dispatcher) SyncSend(msg.SnmpMessage) (*msg.SnmpMessage, error) {
	return new(msg.SnmpMessage), nil
}

func (d *dispatcher) ReturnResponseMsg(msg.SnmpMessage, engine.StateReference) error {
	return nil
}

func (d *dispatcher) ProtocolOptions() transport.ProtocolOptions {
	return nil
}

//Implement SnmpSubSystem interface
//For dispatcher (SnmpSession), we are providing a dummy implementation
type snmpSubSystemImpl struct {
	subSystemModels map[int32]engine.SnmpModel
	snmpEngine      engine.SnmpEngine
}

//Returns instance of SubSystem. This holds all the SnmpModels.
func newSubSystem(snmpEngine engine.SnmpEngine) *snmpSubSystemImpl {
	return &snmpSubSystemImpl{
		subSystemModels: make(map[int32]engine.SnmpModel),
		snmpEngine:      snmpEngine, //Need to override SnmpEngine?
	}
}

func (ss *snmpSubSystemImpl) AddModel(model engine.SnmpModel) {
	if model != nil {
		if _, ok := ss.subSystemModels[model.ID()]; ok {
			return //Model already exist
		}

		ss.subSystemModels[model.ID()] = model
	}
}

func (ss *snmpSubSystemImpl) RemoveModel(id int32) engine.SnmpModel {
	if model, ok := ss.subSystemModels[id]; ok {
		delete(ss.subSystemModels, id)
		return model
	}
	return nil
}

func (ss *snmpSubSystemImpl) Model(id int32) engine.SnmpModel { //Check for nil value
	return ss.subSystemModels[id]
}

func (ss *snmpSubSystemImpl) Models() []engine.SnmpModel {
	var models []engine.SnmpModel
	for _, model := range ss.subSystemModels {
		models = append(models, model)
	}
	return models
}

func (ss *snmpSubSystemImpl) ModelIDs() []int32 {
	var ids []int32
	for id, _ := range ss.subSystemModels {
		ids = append(ids, id)
	}
	return ids
}

func (ss *snmpSubSystemImpl) ModelNames() []string {
	var modelNames []string
	for id, model := range ss.subSystemModels {
		modelNames = append(modelNames, model.Name()+" ["+strconv.Itoa(int(id))+"]")
	}
	return modelNames
}

//Returns the Engine associated with SubSystem
func (ss *snmpSubSystemImpl) SnmpEngine() engine.SnmpEngine {
	return ss.snmpEngine
}
