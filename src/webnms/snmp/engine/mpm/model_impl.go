/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"webnms/snmp/engine"
)

//Implements SnmpModel interface. Just an additional type providing convenience to store SubSystem and Name.
//For mpm internal use only.
type snmpModelImpl struct {
	subSystem engine.SnmpSubSystem
	name      string
	id        int32
}

func newSnmpModelImpl(subsystem engine.SnmpSubSystem, name string, modelID int32) *snmpModelImpl {
	return &snmpModelImpl{
		subSystem: subsystem,
		name:      name,
		id:        modelID,
	}
}

//Returns the SubSystem associated with the Message Processing Model.
func (sm *snmpModelImpl) SubSystem() engine.SnmpSubSystem {
	return sm.subSystem
}

//Returns the name of the Model.
func (sm *snmpModelImpl) Name() string {
	return sm.name
}

//Returns the Model ID.
func (sm *snmpModelImpl) ID() int32 {
	return sm.id
}
