/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"bytes"
	"encoding/asn1"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync/atomic"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

//SnmpMsgProcessingModelV3 is the message processing model implementation to process the incoming and outgoing SNMPv3 messages.
//
//Implements SnmpMsgProcessingModel interface.
type SnmpMsgProcessingModelV3 struct {
	*snmpModelImpl //Extending SnmpModelImpl - To implement SnmpModel interface
	localEngine    engine.SnmpEngine
	snmpLCD        *engine.SnmpLCD //LCD of the complete engine
	msgId          int32
	incMsgId       func() int32
	cacheStore     *msgCacheStore

	*mpmCounters
}

//Creates and returns new instance of v3 message processing model.
//'msgProcessingSubSystem' is the instance of SnmpMsgProcessingSubSystem to which this msg processing model belongs to.
//
//snmpLCD instance can be obtained from SubSystem's engine using SnmpLCD() method, where any local configuration data specific to the model can be stored.
//Refer engine's SnmpLCD for more details.
//
//For internal use only. API user should not use this method.
func NewMsgProcessingModelV3(msgProcessingSubSystem engine.SnmpMsgProcessingSubSystem) *SnmpMsgProcessingModelV3 {
	mp := new(SnmpMsgProcessingModelV3)
	mp.snmpModelImpl = newSnmpModelImpl(msgProcessingSubSystem, "V3 Message Processing Model", V3MP)
	mp.localEngine = msgProcessingSubSystem.SnmpEngine()
	mp.snmpLCD = mp.localEngine.SnmpLCD() //Obtain the SnmpLCD instance from the SnmpEngine
	mp.initMsgIdGeneration()

	mp.mpmCounters = initMpmCounters()

	//This is to store all the cache (state references) of incoming messages
	mp.cacheStore = initCacheStore()

	//Register this model with the SnmpMsgProcessingSubSystem
	msgProcessingSubSystem.AddModel(mp)

	return mp
}

func (v3mp *SnmpMsgProcessingModelV3) initMsgIdGeneration() {
	//We should initialize the msgId based on RFC3412 definition
	v3mp.msgId = v3mp.localEngine.SnmpEngineBoots()
	v3mp.incMsgId = v3mp.incrementMsgId()
}

//MsgId generation
//INTEGER (0..2147483647)
func (v3mp *SnmpMsgProcessingModelV3) incrementMsgId() func() int32 {
	return func() (id int32) {
		atomic.AddInt32(&id, v3mp.msgId)
		v3mp.msgId++
		return
	}
}

//The below structs are used purely for encoding and decoding purpose.

type headerData struct {
	MsgID            int32
	MsgMaxSize       int32
	MsgFlags         []byte
	MsgSecurityModel int32
}

type v3ScopedPDU struct {
	ContextEngineID []byte
	ContextName     []byte
	RawValueSnmpPDU asn1.RawValue
}

type v3Message struct {
	MsgVersion            int
	MsgGlobalData         headerData
	MsgSecurityParameters []byte
	ScopedPDUData         asn1.RawValue
}

type v3PrivMessage struct {
	MsgVersionPriv            int
	MsgGlobalDataPriv         headerData
	MsgSecurityParametersPriv []byte
	ScopedPDUDataPriv         []byte
}

type v3VarBind struct {
	RSnmpOID asn1.ObjectIdentifier
	RSnmpVar asn1.RawValue
}

type v3PDU struct {
	ReqIDRaw       int32
	ErrorStatusRaw int32
	ErrorIndexRaw  int32
	VarBindSlice   []v3VarBind
}

//########## Implementation of Message Processing Model ####################//

//PrepareDataElements process the incoming message from the remote entity and returns decoded SNMP Message. It performs the unmarshalling of the received raw bytes and performs necessary securtity checks based on the SNMP Version and security level.
//It returns the SnmpMessage in case of successful decoding/security checks, else returns nil. StatusInformation contains security error with error counter OIDs if any.
//Implemented as per RFC 3412 - Section 7.2.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) PrepareDataElements(dispatcher engine.Dispatcher, //SnmpSession by default
	protocolOptions transport.ProtocolOptions,
	wholeMsg []byte,
	wholeMsgLength int, //as received in the wire
	tmStateReference transport.TransportStateReference,
) (int32, *msg.SnmpMessage, engine.StateReference, *engine.StatusInformation) {

	receivedMsg := new(msg.SnmpMessage)
	var err error
	var isSecModelSupported bool

	//RFC3412 Section 7.2 step 3
	if receivedMsg, err = v3mp.Decode(wholeMsg); err != nil {
		//RFC3412 Section 7.2 step 2
		v3mp.IncSnmpInASNParseErrsCounter()
		log.Error("Error while decoding the received msg from remote entity: %s. Error: %s. Dropping the message.", protocolOptions.SessionID(), err.Error())
		return -1, nil, nil, engine.NewStatusInformation(err.Error())
	}

	//RFC3412 Section 7.2 step 4
	isSecModelSupported, err = v3mp.isSupportedSecurityModel(receivedMsg.MsgSecurityModel())
	if !isSecModelSupported || err != nil {
		v3mp.incSnmpUnknownSecurityModelsCounter()
		log.Error("Error in processing the received msg from remote entity: %s. Error: %s. Dropping the message.", protocolOptions.SessionID(), err.Error())
		return -1, nil, nil, engine.NewStatusInformation(err.Error())
	}

	//RFC3412 Section 7.2 step 5
	securityLevel := receivedMsg.MsgFlags() & security.AuthPrivMask
	if securityLevel == byte(2) {
		v3mp.incSnmpInvalidMsgsCounter()
		errStr := "Invalid Security level :: 2(Privacy attemped without authentication)"
		log.Warn(errStr + "Remote Address: " + protocolOptions.SessionID() + ". Dropping the message.")
		return -1, nil, nil, engine.NewStatusInformation(errStr)
	}

	//RFC3412 Section 7.2 step 6
	secSubSystem := v3mp.localEngine.SecuritySubSystem()
	securityStateReference := secSubSystem.CreateNewSecurityCache(receivedMsg.MsgSecurityModel())
	maxSizeResponseScopedPDU, securityParameters, scopedPDU, secStatusInfo := secSubSystem.ProcessIncomingRequest(securityStateReference,
		receivedMsg.MsgID(),
		receivedMsg.MsgMaxSize(),
		receivedMsg.MsgFlags(),
		receivedMsg.MsgSecurityParameters(),
		receivedMsg.MsgSecurityModel(),
		wholeMsg,
		wholeMsgLength,
		receivedMsg.ScopedPDUData(),
		protocolOptions,
		tmStateReference,
	)

	var report *msg.SnmpMessage
	if secStatusInfo != nil {
		if secStatusInfo.VarBinds() != nil { //RFC3412 Section 7.2 step 6a
			if (scopedPDU != nil && scopedPDU.IsConfirmed()) || (receivedMsg.MsgFlags()&security.ReportableFlag == 4) {

				report = generateReport(maxSizeResponseScopedPDU, //Received from security layer, used for computing Msg MaxSize
					v3mp.localEngine.SnmpEngineID(),
					securityParameters,
					scopedPDU,
					receivedMsg,
					secStatusInfo,
				)
				report.SetProtocolOptions(protocolOptions) //To be used for sending the report

				//StateRefernce should be cached & passed to dispatcher.ReturnResponseMsg()
				//7.2 6.a.2
				//Create a cache and add it to cache store
				cache := newStateReference(receivedMsg.MsgID(),
					receivedMsg.MsgMaxSize(),
					receivedMsg.MsgFlags(),
					receivedMsg.MsgSecurityModel(),
					maxSizeResponseScopedPDU, //maxSizeResponseScopedPDU can be calculated later
					nil, //No need to store SecurityEngineID
					"",  //No need to store SecurityName
					secStatusInfo.SecurityLevel(),
					nil, //No need to store ContextEngineID
					"",  //No need to store ContextName
					securityStateReference, //Security State Reference from Security Layer
					protocolOptions.RemoteHost(),
					protocolOptions.RemotePort(),
				) //Do we need to add this to our cache store??
				v3mp.cacheStore.addCache(receivedMsg.MsgID(), *cache) //Add the cache to our store

				//Send the Report
				dispatcher.ReturnResponseMsg(*report, cache)

				log.Info("Security error in received message: " + secStatusInfo.Error() + " Sent report to remote entity: " + protocolOptions.SessionID())
				if scopedPDU != nil {
					receivedMsg.SetScopedPDU(*scopedPDU)
					return -1, receivedMsg, nil, engine.NewStatusInformation("Security error occured: " + secStatusInfo.Error() + " Returned report to the remote entity.")
				}

				return -1, nil, nil, engine.NewStatusInformation("Security error occured: " + secStatusInfo.Error() + "Returned report to the remote entity.")
			} /*else {
				return -1, nil, nil, engine.NewStatusInformation("Recieved message not reportable. Hence discarding. " + secStatusInfo.Error())
			}*/
		} else { //RFC3412 Section 7.2 step 6b
			return -1, nil, nil, engine.NewStatusInformation(secStatusInfo.Error())
		}
	}

	//RFC3412 Section 7.2 step 7 - ScopedPDU parsing is done in security layer - Skipped here

	if scopedPDU == nil {
		var err string
		if secStatusInfo != nil {
			err = secStatusInfo.Error()
		}
		return -1, nil, nil, engine.NewStatusInformation("Cannot process incoming message: " + err)
	} else {
		receivedMsg.SetScopedPDU(*scopedPDU)
		pduType := scopedPDU.Command() //RFC3412 Section 7.2 step 9

		//As per RFC3412 Section 7.2 step 10 - GetResponse or ReportMessage case
		if pduType == consts.GetResponse || pduType == consts.ReportMessage {

			incomingMsgID := receivedMsg.MsgID()
			outStandingCache := v3mp.cacheStore.getCache(receivedMsg.MsgID())

			if outStandingCache != nil {
				//Outstanding cache is cleared
				v3mp.cacheStore.removeCache(incomingMsgID)

				//ScopedPDU returned from the security layer is set and receivedMsg is returned.
				//sendPduHandle part is not considered.

				//As per RFC3412 Section 7.2 step 12
				if pduType == consts.GetResponse {

					//Compare the cached info with the incoming message information

					if outStandingCache.msgSecurityModel != receivedMsg.MsgSecurityModel() ||
						!bytes.Equal(outStandingCache.securityEngineID, securityParameters.SecurityEngineID()) ||
						outStandingCache.securityName != securityParameters.SecurityName() ||
						byte(outStandingCache.securityLevel) != receivedMsg.MsgFlags()&security.AuthPrivMask ||
						!bytes.Equal(outStandingCache.contextEngineID, scopedPDU.ContextEngineID()) ||
						outStandingCache.contextName != scopedPDU.ContextName() {
						//						fmt.Println("Mismatch test:")
						//						fmt.Println(outStandingCache.msgSecurityModel, outStandingCache.securityEngineID, outStandingCache.securityName, outStandingCache.securityLevel, outStandingCache.contextEngineID, outStandingCache.contextName)
						//						fmt.Println(receivedMsg.MsgSecurityModel(), securityParameters.SecurityEngineID(), securityParameters.SecurityName(), (receivedMsg.MsgFlags()&security.AuthPrivMask), scopedPDU.ContextEngineID(), scopedPDU.ContextName())
						log.Warn("Received response/report message differ from outstanding request cache. Dropping the MSG: %s", getMsgDebugStr(*receivedMsg, protocolOptions))
						return -1, nil, nil, engine.NewStatusInformation("Dropping the Msg. Incoming response message differ from outstanding cache")
					}
				} else { //RFC3412 Section 7.2 step 11 - Internal class - Report-PDU case

					//Compare the cached info with the incoming message information

					if outStandingCache.msgSecurityModel == receivedMsg.MsgSecurityModel() ||
						byte(outStandingCache.securityLevel) == receivedMsg.MsgFlags()&security.AuthPrivMask {
						v3mp.cacheStore.removeCache(incomingMsgID)
					}

					secSubSystem.ReleaseSecurityCache(receivedMsg.MsgSecurityModel(), securityStateReference)

				}

				return 0, receivedMsg, nil, nil
			} else {
				log.Warn("Received response/report message which doesn't have matching outstanding requests. Dropping the MSG: %s", getMsgDebugStr(*receivedMsg, protocolOptions))
				return -1, nil, nil, engine.NewStatusInformation("Dropping the Msg. No relevant outstanding request found in cache. MsgID: " + strconv.Itoa(int(incomingMsgID)))
			}
		}

		//RFC3412 Section 7.2 step 13
		if scopedPDU.IsConfirmed() {
			//EngineID check is performed in the security layer itself. Hence skipped RFC3412 Section 7.2 step 13.a

			//Create a cache and add it to cache store
			cache := newStateReference(receivedMsg.MsgID(),
				receivedMsg.MsgMaxSize(),
				receivedMsg.MsgFlags(),
				receivedMsg.MsgSecurityModel(),
				345324, //maxSizeResponseScopedPDU can be calculated later
				nil,
				"",
				consts.SecurityLevel(receivedMsg.MsgFlags()&security.AuthPrivMask), //secStatusInfo.SecurityLevel(),
				scopedPDU.ContextEngineID(),
				scopedPDU.ContextName(),
				securityStateReference, //No security info
				protocolOptions.RemoteHost(),
				protocolOptions.RemotePort(),
			)
			log.Finest("Incoming confirmed request is added to cache. MSG:: %s", getMsgDebugStr(*receivedMsg, protocolOptions))
			v3mp.cacheStore.addCache(receivedMsg.MsgID(), *cache) //Add the cache to our store

			return 0, receivedMsg, cache, nil
		} else { //RFC3412 Section 7.2 step 14 - Trapv2-PDU case
			return 0, receivedMsg, nil, secStatusInfo //SecStatusInfo may contain error
		}
	}

	return 0, receivedMsg, nil, nil
}

//isSupportedSecurityModel checks whether the security model is registered one.
func (v3mp *SnmpMsgProcessingModelV3) isSupportedSecurityModel(securityModel int32) (bool, error) {
	secSubSystem := v3mp.localEngine.SecuritySubSystem()
	secModel := secSubSystem.Model(securityModel)

	if secModel == nil {
		return false, errors.New("UnSupported Security Model")
	} else if _, ok := secModel.(engine.SnmpSecurityModel); !ok {
		return false, errors.New("UnSupported Security Model")
	}

	return true, nil
}

//Generation of Report-PDU as per RFC3412 Section 7.1
func generateReport(maxSizeResponseScopedPDU int32,
	snmpEngineID []byte,
	securityParams engine.SecurityParameters,
	scopedPDU *msg.ScopedPDU,
	receivedMsg *msg.SnmpMessage,
	statusInfo *engine.StatusInformation,
) *msg.SnmpMessage {

	report := new(msg.SnmpMessage)

	report.SetVersion(consts.Version3)                           //V3MP
	report.SetMaxSizeResponseScopedPDU(maxSizeResponseScopedPDU) // - sender can accept
	report.SetVarBinds(statusInfo.VarBinds())
	report.SetErrorStatus(consts.NoError)
	report.SetErrorIndex(0)

	if scopedPDU != nil {
		report.SetRequestID(scopedPDU.RequestID())
	} else {
		report.SetRequestID(0)
	}

	report.SetMsgFlags(statusInfo.MsgFlags() & security.AuthPrivMask) //Remove the Reportable Flag
	report.SetSecurityLevel(statusInfo.SecurityLevel())
	report.SetMsgSecurityModel(receivedMsg.MsgSecurityModel())
	report.SetUserName(securityParams.SecurityName())

	//Corresponding fields will be filled based on the security level of the report in security model (while sending).
	report.SetMsgSecurityParameters(statusInfo.SecurityParams()) //Fill the security params

	if statusInfo.ContextEngineID() != nil {
		report.SetContextEngineID(statusInfo.ContextEngineID())
	} else {
		report.SetContextEngineID(snmpEngineID)
	}
	report.SetContextName(statusInfo.ContextName())

	report.SetCommand(consts.ReportMessage)

	return report
}

//PrepareOutgoingMessage process the outgoing message belongs to Read/Write/Notification class. It performs marshalling (encoding) and necessary security services based on the security model and returns a final encoded message as bytes ready to send.
//Returns error if unable to prepare the message.
//Implemented as per RFC 3412 - Section 7.1.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU,
	expectResponse bool,
) (int32, []byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	//Let's take the instance of security subsystem
	secSS := v3mp.localEngine.SecuritySubSystem()

	//Prepare ScopedPDU
	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SnmpPDU = snmpPDU

	//Additional check to ensure that the security model is not v1/v2
	if securityModel == security.V1sec || securityModel == security.V2Csec {
		return -1, nil, nil, errors.New(fmt.Sprintf("Message Processing Error: Unsupported security model for v3 message processing. ModelID: %d", securityModel))
	}

	//Let's determine the security engine id to be used for the request/notification
	var securityEngineID []byte

	if scopedPDU.IsConfirmed() { //Get/Get-Next/Set/Get-Bulk/Inform - Not Authoritative
		//securityEngineId = let's check the peer LCD for an Entry based on remote address, port
		engLCD := secSS.PeerEngineLCD(securityModel)
		var peerEngine engine.SnmpPeerEngine
		if engLCD != nil {
			peerEngine = engLCD.Engine(protocolOptions.RemoteHost(),
				protocolOptions.RemotePort(),
			)
		}
		//Use the Peer's SnmpEngineID
		if peerEngine != nil && peerEngine.AuthoritativeEngineID() != nil {
			securityEngineID = peerEngine.AuthoritativeEngineID()

			//This can be a discovery request.
			//This check is added to enable re-discovery process.
			if isDiscoveryUser(securityName) &&
				securityLevel == security.NoAuthNoPriv &&
				scopedPDU.Command() == consts.GetRequest &&
				len(scopedPDU.VarBinds()) <= 0 {
				securityEngineID = make([]byte, 0) //Nil EngineID
			}

		} else { //There is no snmp peer, this can be a discovery request
			securityEngineID = make([]byte, 0)
		}

		//Can we handle this empty engine id - for discovery
		if len(securityEngineID) == 0 { //This can be a discovery request
			securityLevel = security.NoAuthNoPriv
			securityModel = 3 //USM by default
		}
	} else { //Unconfirmed PDU - Notification (Trap) - Authoritative
		securityEngineID = v3mp.localEngine.SnmpEngineID()
		if len(scopedPDU.ContextEngineID()) == 0 {
			//Unconfirmed PDUs ContextEngineID cannot be null, lets set it to EngineID
			scopedPDU.SetContextEngineID(v3mp.localEngine.SnmpEngineID())
			contextEngineId = v3mp.localEngine.SnmpEngineID()
		}
	}

	//If no contextEngineId is passed, let's use the EngineID
	if contextEngineId == nil || len(contextEngineId) == 0 {
		//ContextEngineId is nil, so we are using authoritative engineID
		contextEngineId = securityEngineID
	}

	scopedPDU.SetContextName(contextName) //Can we deal with empty ContextName??
	scopedPDU.SetContextEngineID(contextEngineId)

	//Preparation of HeaderData
	headerData := new(msg.SnmpMessage)
	//Let's set the flags
	var msgFlags byte = 0
	switch securityLevel {
	case security.NoAuthNoPriv:
		msgFlags |= 0
	case security.AuthNoPriv:
		msgFlags |= 1
	case security.AuthPriv:
		msgFlags |= 3
	}
	if scopedPDU.IsConfirmed() {
		msgFlags |= security.ReportableFlag
	}

	headerData.SetMsgID(v3mp.incMsgId()) //Generate unique msgID
	headerData.SetMsgMaxSize(65535)      //Need to determine this
	headerData.SetMsgFlags(msgFlags)
	headerData.SetMsgSecurityModel(securityModel)

	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateRequestMsg(consts.Version3,
		headerData.MsgID(),
		headerData.MsgMaxSize(),
		headerData.MsgFlags(),
		headerData.MsgSecurityModel(),
		securityName,
		securityEngineID, //Can be local (or) from LCD
		*scopedPDU,
		protocolOptions,
	)

	//Security Module returned some error
	if secStatusInfo != nil {
		log.Fatal("Error in preparing the outgoing msg: " + secStatusInfo.Error())
		return -1, nil, tmStateReference, errors.New(secStatusInfo.Error())
	}

	if (expectResponse) || scopedPDU.IsConfirmed() {
		//Create a cache and add it to cache store
		cache := newStateReference(headerData.MsgID(),
			headerData.MsgMaxSize(),
			headerData.MsgFlags(),
			headerData.MsgSecurityModel(),
			-1, //No MaxSizeResponseScopedPDU for outgoing request
			securityEngineID,
			securityName,
			securityLevel,
			scopedPDU.ContextEngineID(),
			scopedPDU.ContextName(),
			nil, //No security info
			protocolOptions.RemoteHost(),
			protocolOptions.RemotePort(),
		)
		log.Finest("Outgoing message is added to the mpm cache. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
		v3mp.cacheStore.addCache(headerData.MsgID(), *cache) //Add the cache to our store
	}

	return headerData.MsgID(), encodedMsg, tmStateReference, nil
}

//PrepareResponseMessage process the outgoing message belongs to Response/Internal class. It performs marshalling (encoding) and necessary security services based on the security model and returns a final encoded message as bytes.
//cache will be used while preparing outgoing response message.
//Returns error if unable to prepare the message.
//Implemented as per RFC 3412 - Section 7.1.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
	maxSizeResponseScopedPDU int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU, //Should contain only PDU part
	cache engine.StateReference, //This cache is required for sending the response
	statusInfo *engine.StatusInformation,
) ([]byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	//For response message we should use cache stored already from incoming request
	//PrepareDataElements should cache the incoming requests (Confirmed)
	stateReference := v3mp.cacheStore.removeCache(cache.MsgID())
	if stateReference == nil {
		//There is no cache found for this response, which means no incoming request
		//has been cached with this MsgId
		return nil, nil, errors.New("Unknown MsgID: There is no outstanding request cache found for this response.")
	}

	//Let's deal with the status information as per RFC 3412 - 7.1 Step 3
	//We have done Report Generation in PrepareDataElements itself.
	if statusInfo != nil && statusInfo.VarBinds() != nil {
		reqID := snmpPDU.RequestID()
		if snmpPDU.IsConfirmed() {
			//It should be confirmed class
			return nil, nil, errors.New("Failure in message processing: Cannot process unconfirmed message with the status information.")
		}

		reportPDU := msg.SnmpPDU{}
		reportPDU.SetRequestID(reqID)
		reportPDU.SetErrorStatus(0)
		reportPDU.SetErrorIndex(0)
		for _, varb := range statusInfo.VarBinds() {
			reportPDU.AddVarBind(varb)
		}

		//Let's use other date from status information
		if statusInfo.SecurityLevel() != 0 {
			securityLevel = statusInfo.SecurityLevel()
		} else {
			securityLevel = security.NoAuthNoPriv
		}
		if statusInfo.ContextEngineID() != nil &&
			len(statusInfo.ContextEngineID()) > 0 {
			contextEngineId = statusInfo.ContextEngineID()
		} else {
			contextEngineId = v3mp.localEngine.SnmpEngineID()
		}

		contextName = statusInfo.ContextName()

		//Discarding the old PDU
		snmpPDU = reportPDU
	}

	//Can we do this??
	if contextEngineId == nil || len(contextEngineId) == 0 {
		contextEngineId = v3mp.localEngine.SnmpEngineID()
	}

	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SetContextEngineID(contextEngineId)
	scopedPDU.SetContextName(contextName)
	scopedPDU.SnmpPDU = snmpPDU

	mps := v3mp.SubSystem().(engine.SnmpMsgProcessingSubSystem)
	byteScopedPDU, _ := mps.EncodeScopedPDU(consts.Version3, *scopedPDU)
	if len(byteScopedPDU) > int(maxSizeResponseScopedPDU) {
		//We should do some handling to reduce the size
	}

	//Preparation of HeaderData
	headerData := new(msg.SnmpMessage)
	//Let's set the flags
	var msgFlags byte = 0
	switch securityLevel {
	case security.NoAuthNoPriv:
		msgFlags |= 0
	case security.AuthNoPriv:
		msgFlags |= 1
	case security.AuthPriv:
		msgFlags |= 3
	}

	//Need to compute this properly
	msgMaxSize := maxSizeResponseScopedPDU + getMsgMaxSize()

	headerData.SetMsgID(stateReference.msgId) //Cached MsgId
	headerData.SetMsgMaxSize(msgMaxSize)      //Need to determine this
	headerData.SetMsgFlags(msgFlags)
	headerData.SetMsgSecurityModel(securityModel)

	var securityEngineId []byte
	if (scopedPDU.Command() == consts.GetResponse) ||
		(scopedPDU.Command() == consts.ReportMessage) ||
		(scopedPDU.Command() == consts.TrapRequest) ||
		(scopedPDU.Command() == consts.Trap2Request) {
		securityEngineId = v3mp.localEngine.SnmpEngineID()
	} else {
		securityEngineId = stateReference.securityEngineID
	}

	secSS := v3mp.localEngine.SecuritySubSystem()

	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateResponseMsg(consts.Version3,
		headerData.MsgID(),
		headerData.MsgMaxSize(),
		headerData.MsgFlags(),
		headerData.MsgSecurityModel(),
		securityName,
		securityEngineId, //Local EngineId for sending response
		*scopedPDU,
		stateReference.securityStateReference(),
		protocolOptions,
	)

	//Release the security cache, as we are done with the response processing
	secSS.ReleaseSecurityCache(headerData.MsgSecurityModel(), stateReference.securityStateReference())
	if secStatusInfo != nil {
		v3mp.cacheStore.removeCache(stateReference.MsgID())
		log.Fatal("Error in preparing response message. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
		return nil, tmStateReference, errors.New(secStatusInfo.Error())
	}

	return encodedMsg, tmStateReference, nil
}

//Let's define the encoding/decoding of SnmpMessages here.

//EncodeMessage encodes the SnmpMessage msg passed based on the SNMP version passed and returns an encoded byte array.
//Returns error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) EncodeMessage(msg msg.SnmpMessage) ([]byte, error) {
	return v3mp.Encode(msg.Version(), msg.MsgID(), msg.MsgMaxSize(), msg.MsgFlags(), msg.MsgSecurityModel(), msg.MsgSecurityParameters(), msg.ScopedPDU)
}

//Encode encodes the data such as HeaderData, ScopedPDU and returns the byte array. Encoding should be used for Unencrypted data.
//Returns error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) Encode(version consts.Version,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32,
	msgSecurityParameters []byte,
	scopedpdu msg.ScopedPDU) ([]byte, error) {

	globalData := headerData{msgId, msgMaxSize, []byte{msgFlags}, msgSecurityModel}

	scopedData, scopedDataError := v3mp.EncodeScopedPDU(scopedpdu)

	if scopedDataError != nil {
		return nil, scopedDataError
	}

	scopedDataRawValue := asn1.RawValue{0, 0, false, []byte{}, scopedData}

	//Marshal whole V3Message
	rawSnmpMsg := v3Message{int(version), globalData, msgSecurityParameters, scopedDataRawValue}
	encodingFinal, encError := asn1.Marshal(rawSnmpMsg)

	return encodingFinal, encError
}

//EncodePriv encodes the data such as HeaderData, Encrypted ScopedPDU and returns the byte array. Encoding should be used for Encrypted data.
//Returns error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) EncodePriv(version consts.Version,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32,
	msgSecurityParameters []byte,
	encryptedScopedPDU []byte,
) ([]byte, error) {

	globalData := headerData{msgId, msgMaxSize, []byte{msgFlags}, msgSecurityModel}

	//Marshal whole V3Message
	rawSnmpMsg := v3PrivMessage{int(version), globalData, msgSecurityParameters, encryptedScopedPDU}
	encodingFinal, encError := asn1.Marshal(rawSnmpMsg)

	return encodingFinal, encError
}

//Decode decodes the v3MsgData raw bytes as SNMPv3 message.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) Decode(v3MsgData []byte) (*msg.SnmpMessage, error) {

	v3Data := make([]byte, len(v3MsgData))
	copy(v3Data, v3MsgData)

	v3Msg := new(v3Message)
	//Unmarshal whole v3Message
	if _, err := asn1.Unmarshal(v3Data, v3Msg); err != nil {
		return nil, errors.New("Error while decoding V3 message : " + err.Error())
	}

	msg := new(msg.SnmpMessage)
	msg.SetVersion(consts.Version(v3Msg.MsgVersion))

	msg.SetMsgID(v3Msg.MsgGlobalData.MsgID)
	msg.SetMsgMaxSize(v3Msg.MsgGlobalData.MsgMaxSize)
	msg.SetMsgFlags(v3Msg.MsgGlobalData.MsgFlags[0])
	msg.SetMsgSecurityModel(v3Msg.MsgGlobalData.MsgSecurityModel)

	msg.SetMsgSecurityParameters(v3Msg.MsgSecurityParameters)

	if msg.MsgFlags()&security.AuthPrivMask == byte(security.AuthPriv) {
		if encryptedScopedData := decodePrivScopedData(v3Msg.ScopedPDUData.FullBytes); encryptedScopedData == nil {
			return nil, errors.New("Error while decoding V3 message : Unable to decode the encrypted Scoped Data")
		} else {
			msg.SetScopedPDUData(encryptedScopedData)
		}
	} else {
		msg.SetScopedPDUData(v3Msg.ScopedPDUData.FullBytes)
	}

	return msg, nil

}

func decodePrivScopedData(privData []byte) []byte {

	privByte := new([]byte)
	if _, err := asn1.Unmarshal(privData, privByte); err != nil {
		return nil
	}
	return *privByte
}

//checkTrapAddress checks if the TrapAddress varBind is already present in the varBind list.
//If present, the value of the varBind is set to SnmpMessage's agentAddress.
func checkTrapAddress(pdu *msg.SnmpPDU) bool {

	var oid snmpvar.SnmpOID

	for _, varb := range pdu.VarBinds() {
		oid = varb.ObjectID()
		if strings.EqualFold(oid.String(), ".1.3.6.1.6.3.18.1.3.0") {
			pdu.SetAgentAddress(varb.Variable().String())
			return false
		}
	}

	return true
}

//EncodeScopedPDU encodes the ScopedPDU portion the SNMPv3 message and returns the byte array.
//Returns error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) EncodeScopedPDU(scopedPDU msg.ScopedPDU) ([]byte, error) {

	var scopedPDUData []byte

	//Encoding V3 VarBinds
	v3VarbSlice, err := encodeV3VarBinds(scopedPDU.VarBinds())
	if err != nil {
		return nil, err
	}

	//Marshal command PDU
	var v3CommandData []byte
	var pduEncError error
	var varbError error

	if scopedPDU.Command() == consts.GetBulkRequest {
		v3PDUImp := v3PDU{scopedPDU.RequestID(), scopedPDU.NonRepeaters(), scopedPDU.MaxRepetitions(), v3VarbSlice}
		v3CommandData, pduEncError = asn1.Marshal(v3PDUImp)
	} else {
		if scopedPDU.Command() == consts.TrapRequest {
			return nil, errors.New("Invalid SNMP Version for sending a v1 trap")
		}

		if scopedPDU.Command() == consts.Trap2Request && scopedPDU.AgentAddress() != "" && checkTrapAddress(&scopedPDU.SnmpPDU) {
			scopedPDU.SetVarBinds(addTrapAddressVarBind(scopedPDU.AgentAddress(), scopedPDU.VarBinds()))
			v3VarbSlice, varbError = encodeV3VarBinds(scopedPDU.VarBinds())
			if varbError != nil {
				return nil, varbError
			}
		}

		v3PDUImp := v3PDU{scopedPDU.RequestID(), int32(scopedPDU.ErrorStatus()), scopedPDU.ErrorIndex(), v3VarbSlice}
		v3CommandData, pduEncError = asn1.Marshal(v3PDUImp)
	}

	if pduEncError != nil {
		return nil, errors.New("Error while encoding Scoped PDU : " + pduEncError.Error())
	}

	if scopedPDU.Command() == 0 {
		return nil, errors.New("Invalid command type: 0")
	}

	v3CommandDataWSeq := v3CommandData
	v3CommandDataWSeq[0] = byte(scopedPDU.Command())
	//Marshaling command PDU ends here

	rawValueV3Command := asn1.RawValue{0, 0, false, []byte{}, v3CommandDataWSeq}
	scopedData := v3ScopedPDU{scopedPDU.ContextEngineID(), []byte(scopedPDU.ContextName()), rawValueV3Command}
	scopedPDUData, scopedPduError := asn1.Marshal(scopedData)

	return scopedPDUData, scopedPduError
}

//encode3VarBinds encodes the SnmpVar that is present in the SnmpVarBind.
//This encoded bytes are then set in the v3PDU to be encoded finally.
func encodeV3VarBinds(snmpVarbs []msg.SnmpVarBind) ([]v3VarBind, error) {
	//Struct formations for encoding
	v3Varb := v3VarBind{}
	v3VarbSlice := make([]v3VarBind, len(snmpVarbs))

	//Convert []SnmpVarBind to []v3VarBind
	for eachvarbkey, eachvarb := range snmpVarbs {
		v3Varb.RSnmpOID = convertUInt32ToInt(eachvarb.ObjectID().Value())

		varData, varErr := eachvarb.Variable().EncodeVar()
		if varErr != nil {
			return nil, errors.New("Unable to Encode V3 PDU : Problem encoding variable : " + varErr.Error())
		}
		v3Varb.RSnmpVar = asn1.RawValue{0, 0, false, []byte{}, varData}

		v3VarbSlice[eachvarbkey] = v3Varb
	}

	return v3VarbSlice, nil
}

//DecodeScopedPDu decodes the scopedBytes into ScopedPDU type.
//Returns error in case of failure in decoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v3mp *SnmpMsgProcessingModelV3) DecodeScopedPDU(scopedPDUDataCopy []byte) (*msg.ScopedPDU, error) {

	scopedPDUData := make([]byte, len(scopedPDUDataCopy))
	copy(scopedPDUData, scopedPDUDataCopy)

	rawscopedpdu := new(v3ScopedPDU)

	//Unmarshal whole ScopedPDUData
	if _, err := asn1.Unmarshal(scopedPDUData, rawscopedpdu); err != nil {
		return nil, errors.New("Error while decoding the Scoped PDU Data : " + err.Error())
	}

	scopedPdu := new(msg.ScopedPDU)

	scopedPdu.SetContextEngineID(rawscopedpdu.ContextEngineID)
	scopedPdu.SetContextName(string(rawscopedpdu.ContextName))

	v3CommandData := rawscopedpdu.RawValueSnmpPDU.FullBytes
	if v3CommandData == nil || len(v3CommandData) <= 0 {
		return nil, errors.New("Error while decoding Command PDU")
	}
	scopedPdu.SetCommand(consts.Command(v3CommandData[0]))

	v3CommandDataWSeq := v3CommandData
	v3CommandDataWSeq[0] = consts.Sequence

	v3Pdu := new(v3PDU)
	if _, commandErr := asn1.Unmarshal(v3CommandDataWSeq, v3Pdu); commandErr != nil {
		return nil, errors.New("Error while decoding command PDU : " + commandErr.Error())
	}

	scopedPdu.SetRequestID(v3Pdu.ReqIDRaw)

	//Reform SnmpVarBind slice from rawSnmpVarBind slice
	scopedPdu.SetVarBinds(decode3VarBinds(v3Pdu.VarBindSlice))

	if scopedPdu.Command() == consts.GetBulkRequest {
		scopedPdu.SetNonRepeaters(v3Pdu.ErrorStatusRaw)
		scopedPdu.SetMaxRepetitions(v3Pdu.ErrorIndexRaw)
	} else {
		if scopedPdu.Command() == consts.Trap2Request {
			checkTrapAddress(&scopedPdu.SnmpPDU)
		}
		scopedPdu.SetErrorStatus(consts.ErrorStatus(v3Pdu.ErrorStatusRaw))
		scopedPdu.SetErrorIndex(v3Pdu.ErrorIndexRaw)
	}

	return scopedPdu, nil
}

//decode3VarBinds converts the snmpvar data that is present in the SnmpVarBind into specific SnmpVar.
//This converted []SnmpVarBind is then set in the SnmpMessage.
func decode3VarBinds(v3Varbs []v3VarBind) []msg.SnmpVarBind {
	snmpVarb := new(msg.SnmpVarBind)
	snmpVarbSlice := make([]msg.SnmpVarBind, len(v3Varbs))

	for eachvarbkey, eachvarb := range v3Varbs {
		var snmpVar snmpvar.SnmpVar = snmpvar.DecodeSnmpVar(eachvarb.RSnmpVar.FullBytes)
		var snmpOid snmpvar.SnmpOID = *snmpvar.NewSnmpOIDByInts(convertIntToUInt32(eachvarb.RSnmpOID))

		snmpVarb.SetVariable(snmpVar)
		snmpVarb.SetObjectID(snmpOid)

		switch snmpVar.(type) {
		case *snmpvar.SnmpNull:
			nullVar := snmpVar.(*snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		case snmpvar.SnmpNull:
			nullVar := snmpVar.(snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		}

		snmpVarbSlice[eachvarbkey] = *snmpVarb
	}

	return snmpVarbSlice
}

//This will be set on outgoing request message
func getMsgMaxSize() int32 {
	return 65507 // 65507 (65,535 − 8 byte UDP header − 20 byte IP header) is the max limit for data length in a UDP packet over IPv4.
}

func getMsgDebugStr(mesg msg.SnmpMessage, protocolOptions transport.ProtocolOptions) string {
	msgStr := ""
	msgStr += "ReqID: " + strconv.FormatInt(int64(mesg.RequestID()), 10) + "; "
	msgStr += "Command: " + getMSGCommand(mesg.Command()) + "; "
	if protocolOptions != nil {
		msgStr += "RemoteAddress: " + protocolOptions.SessionID() + ";"
	} else if mesg.ProtocolOptions() != nil {
		msgStr += "RemoteAddress: " + mesg.ProtocolOptions().SessionID() + ";"
	}

	return msgStr
}

//getMSGCommand returns the string form of the SnmpMessage's command.
func getMSGCommand(cmd consts.Command) string {

	var cmdStr string = ""

	switch cmd {
	case consts.GetRequest:
		cmdStr = "Get-Request"
	case consts.GetNextRequest:
		cmdStr = "Get-Request"
	case consts.GetResponse:
		cmdStr = "Get-Response"
	case consts.SetRequest:
		cmdStr = "Set-Request"
	case consts.TrapRequest:
		cmdStr = "V1 Trap-Request"
	case consts.GetBulkRequest:
		cmdStr = "GetBulk-Request"
	case consts.InformRequest:
		cmdStr = "Inform-Request"
	case consts.Trap2Request:
		cmdStr = "V2 Trap-Request"
	case consts.ReportMessage:
		cmdStr = "Report"
	default:
		cmdStr = "UnKnown"
	}

	return cmdStr
}

func isDiscoveryUser(name string) bool {
	if name == "" || name == "initial" {
		return true
	}
	return false
}
