/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/v1v2"
	"webnms/snmp/engine/transport/udp"
	"webnms/snmp/msg"

	//"fmt"
	"errors"
	"testing"
)

//v2MsgResp := []byte{48, 46, 2, 1, 1, 4, 6, 112, 117, 98, 108, 105, 99, 162, 33, 2, 1, 117, 2, 1, 0, 2, 1, 0, 48, 22, 48, 20, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 4, 8, 115, 121, 115, 68, 101, 115, 99, 114}

var v2mpTestStruct = []struct {
	msgReqBytes  []byte
	msgRespBytes []byte
	secID        int32
	secIDErr     error
	decErr       error
}{
	{v2cMsgReq, v2cMsgResp, security.V2Csec, nil, nil},                       //Valid case
	{[]byte("swami"), []byte("swami"), security.V2Csec, nil, errors.New("")}, //decode error
	{v2cMsgReq, v2cMsgResp, security.V1sec, errors.New(""), nil},             //Invalid sec model error
	{v2cMsgReq, v2cMsgResp, security.USMID, errors.New(""), nil},
}
var v2cMsgResp = []byte{48, 46, 2, 1, 1, 4, 6, 112, 117, 98, 108, 105, 99, 162, 33, 2, 1, 118, 2, 1, 0, 2, 1, 0, 48, 22, 48, 20, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 4, 8, 115, 121, 115, 68, 101, 115, 99, 114}

var v2cMsgReq = []byte{48, 38, 2, 1, 1, 4, 6, 112, 117, 98, 108, 105, 99, 160, 25, 2, 1, 119, 2, 1, 0, 2, 1, 0, 48, 14, 48, 12, 6, 8, 43, 6, 1, 2, 1, 1, 1, 0, 5, 0}

func TestV2CMP(t *testing.T) {
	lcd := engine.NewSnmpLCD()
	eng := engine.NewSnmpEngine(lcd, "localhost", 8001, 10)
	msgSS := engine.NewMsgProcessingSubSystem(eng)
	secSS := engine.NewSecuritySubSystem(eng)
	eng.SetMsgProcessingSubSystem(msgSS)
	eng.SetSecuritySubSystem(secSS)

	secSS.AddModel(v1v2.NewSnmpSecurityModelV1V2(secSS))

	v2mp := NewMsgProcessingModelV2C(msgSS)
	if v2mp == nil {
		t.Fatal("v2mp: Failure in creating v2mp instance.")
	}

	udp := new(udp.UDPProtocolOptions)

	for _, mpTest := range v2mpTestStruct {

		//Test PrepareOutgoingMessage method - Outgoing Request
		pdu := msg.SnmpPDU{}
		pdu.SetRequestID(118) //ReqID of next incoming response - for matching
		pdu.SetCommand(consts.GetRequest)
		_, _, _, err := v2mp.PrepareOutgoingMessage(udp,
			mpTest.secID,
			"",
			security.NoAuthNoPriv,
			nil,
			"",
			pdu,
			true,
		)
		if !(mpTest.secIDErr == nil && err == nil) && !(mpTest.secIDErr != nil && err != nil) {
			t.Errorf("v2mp: PrepareOutgoingMessage. Error: %v.", err)
		}

		if err == nil { //Receive response only when there is an outstanding request
			//Test PrepareDataElements method - Incoming Response
			_, _, _, si := v2mp.PrepareDataElements(nil, //No Dispatcher instance
				udp,
				mpTest.msgRespBytes,
				len(mpTest.msgRespBytes),
				nil,
			)
			if !(mpTest.decErr == nil && si == nil) && !(mpTest.decErr != nil && si != nil) {
				t.Errorf("v2mp: PrepareDataElements. Error: %v.", si)
			}
		}

		//Test PrepareDataElements  method - Incoming Request
		_, mesg, _, si := v2mp.PrepareDataElements(nil, //No Dispatcher instance
			udp,
			mpTest.msgReqBytes,
			len(mpTest.msgReqBytes),
			nil,
		)
		if !(mpTest.decErr == nil && si == nil) && !(mpTest.decErr != nil && si != nil) {
			t.Errorf("v2mp: PrepareDataElements. Error: %v.", si)
		}

		if mesg != nil {
			//Test PrepareResponseMessage method - Outgoing Response
			pdu.SetCommand(consts.GetResponse)
			pdu.SetRequestID(mesg.RequestID())
			_, _, err := v2mp.PrepareResponseMessage(udp,
				65535,
				mpTest.secID,
				"",
				security.NoAuthNoPriv,
				nil,
				"",
				pdu,
				nil,
				nil,
			)
			if !(mpTest.secIDErr == nil && err == nil) && !(mpTest.secIDErr != nil && err != nil) {
				t.Errorf("v2mp: PrepareResponseMessage. Error: %v.", err)
			}
		}
	}
}
