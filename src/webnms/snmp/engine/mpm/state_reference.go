/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"sync"

	"webnms/snmp/consts"
	"webnms/snmp/engine"
)

//MessageProcessing layer's implementation of StateReference interface.
//
//This is to hold the information about the complete SnmpMessage.
//This will be used while preparing response message, cross check the request/responses for integrity.
type stateReference struct {
	remoteHost string
	remotePort int

	requestId int32

	msgId                    int32
	msgMaxSize               int32
	msgFlags                 byte
	msgSecurityModel         int32
	maxSizeResponseScopedPDU int32

	securityEngineID []byte
	securityName     string //Will be used as community for v1/v2c
	securityLevel    consts.SecurityLevel
	contextEngineID  []byte
	contextName      string

	securityReference engine.SecurityStateReference //Reference to security cache instance
}

func newStateReference(
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	msgSecurityModel int32,
	maxSizeResponseScopedPDU int32,
	securityEngineID []byte,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineID []byte,
	contextName string,
	securityStateReference engine.SecurityStateReference,
	remoteHost string,
	remotePort int,
) *stateReference {

	return &stateReference{msgId: msgId,
		msgMaxSize:               msgMaxSize,
		msgFlags:                 msgFlags,
		msgSecurityModel:         msgSecurityModel,
		maxSizeResponseScopedPDU: maxSizeResponseScopedPDU,
		securityEngineID:         securityEngineID,
		securityName:             securityName,
		securityLevel:            securityLevel,
		contextEngineID:          contextEngineID,
		contextName:              contextName,
		securityReference:        securityStateReference,
		remoteHost:               remoteHost,
		remotePort:               remotePort,
	}
}

func (sr *stateReference) MsgID() int32 {
	return sr.msgId
}

//Set the security cache on StateReference for reference
func (sr *stateReference) setSecurityStateReference(securityReference engine.SecurityStateReference) {
	sr.securityReference = securityReference
}

//Returns the security cache set
func (sr *stateReference) securityStateReference() engine.SecurityStateReference {
	return sr.securityReference
}

//Cache store for all the msg processing models.
//Maintains all the cache in this store for message processing model.
//
//V1MP/V2MP maintains list of statereference based on RequestID. V3MP maintains list of stateReference based on MsgID.
//
//Internal use only
type msgCacheStore struct {
	entries map[int32]stateReference
	mutex   sync.RWMutex //For synchronization
}

func initCacheStore() *msgCacheStore {
	return &msgCacheStore{
		entries: make(map[int32]stateReference),
	}
}

func (cs *msgCacheStore) addCache(id int32, cache stateReference) {
	cs.mutex.Lock()
	defer cs.mutex.Unlock()
	if _, ok := cs.entries[id]; ok {
		//There is already an entry exist. Can we overwrite??
		return
	}

	cs.entries[id] = cache
}

func (cs *msgCacheStore) removeCache(id int32) *stateReference {
	cs.mutex.Lock()
	defer cs.mutex.Unlock()
	if cache, ok := cs.entries[id]; ok {
		delete(cs.entries, id)
		return &cache
	}

	return nil
}

func (cs *msgCacheStore) getCache(id int32) *stateReference {
	cs.mutex.RLock()
	defer cs.mutex.RUnlock()
	if cache, ok := cs.entries[id]; ok {
		return &cache
	}

	return nil
}
