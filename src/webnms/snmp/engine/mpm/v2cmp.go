/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"encoding/asn1"
	"errors"
	"fmt"
	"strconv"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

//SnmpMsgProcessingModelV2C is the message processing model implementation to process the incoming and outgoing SNMPv2c messages.
//
//Implements SnmpMsgProcessingModel interface.
type SnmpMsgProcessingModelV2C struct {
	*snmpModelImpl //Extending SnmpModelImpl - To implement SnmpModel interface
	localEngine    engine.SnmpEngine
	cacheStore     *msgCacheStore

	*mpmCounters
}

//Creates and returns new instance of v2c message processing model.
//'msgProcessingSubSystem' is the instance of SnmpMsgProcessingSubSystem to which this msg processing model belongs to.
//
//snmpLCD instance can be obtained from SubSystem's engine using SnmpLCD() method, where any local configuration data specific to the model can be stored.
//Refer engine's SnmpLCD for more details.
//
//For internal use only. API user should not use this function.
func NewMsgProcessingModelV2C(msgProcessingSubSystem engine.SnmpMsgProcessingSubSystem) *SnmpMsgProcessingModelV2C {
	mp := new(SnmpMsgProcessingModelV2C)
	mp.snmpModelImpl = newSnmpModelImpl(msgProcessingSubSystem, "V2C Message Processing Model", V2CMP)
	mp.localEngine = msgProcessingSubSystem.SnmpEngine()

	//Register this model with the SnmpMsgProcessingSubSystem
	msgProcessingSubSystem.AddModel(mp)

	mp.mpmCounters = initMpmCounters()

	//This is to store all the cache (state references) of incoming messages
	mp.cacheStore = initCacheStore()

	return mp
}

//All the Raw* structs are used internally for ASN1 encoding and decoding purposes.
//API users should not use these types anywhere in their implementations.
type v2CMessage struct {
	VersionRaw      int
	CommunityRaw    []byte
	RawValueSnmpPDU asn1.RawValue
}

type v2CVarBind struct {
	RSnmpOID asn1.ObjectIdentifier
	RSnmpVar asn1.RawValue
}

type v2CPDU struct {
	ReqIDRaw       int32
	ErrorStatusRaw int32
	ErrorIndexRaw  int32
	VarBindSlice   []v2CVarBind
}

//########## Implementation of Message Processing Model ####################//

//PrepareDateElements process the incoming message. Decodes the received bytes and returns SnmpMessage.
//Returns error in case of failure in decoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) PrepareDataElements(dispatcher engine.Dispatcher, //SnmpSession by default
	protocolOptions transport.ProtocolOptions,
	wholeMsg []byte,
	wholeMsgLength int, //as received in the wire
	tmStateReference transport.TransportStateReference,
) (int32, *msg.SnmpMessage, engine.StateReference, *engine.StatusInformation) { //Can we return a decoded SnmpMessage instead??

	receivedMsg := new(msg.SnmpMessage)
	var err error

	if receivedMsg, err = v2cmp.Decode(wholeMsg); err != nil {
		v2cmp.IncSnmpInASNParseErrsCounter()
		log.Error("Error while decoding the received msg from remote entity: %s. Error: %s. Dropping the message.", protocolOptions.SessionID(), err.Error())
		return -1, nil, nil, engine.NewStatusInformation(err.Error())
	}
	/*
		Security part is ignored for now. Should be plugged later.
	*/
	pduType := receivedMsg.Command()

	if pduType == consts.GetResponse || pduType == consts.ReportMessage {
		reqID := receivedMsg.RequestID()
		outStandingCache := v2cmp.cacheStore.getCache(reqID)

		if outStandingCache != nil {
			//Outstanding cache is cleared
			v2cmp.cacheStore.removeCache(reqID)

			//Let's not perform any checks with the cache. Just return the msg
			return 0, receivedMsg, nil, nil
		} else {
			log.Warn("Received response/report message which doesn't have matching outstanding requests. Dropping the MSG: %s", getMsgDebugStr(*receivedMsg, protocolOptions))
			return -1, nil, nil, engine.NewStatusInformation("Dropping the Msg. No relevant outstanding request found in cache. " + strconv.Itoa(int(reqID)))
		}
	}

	//Let's prepare stateReference for the confirmed requests.
	//So that this cache can be used for sending response.
	if receivedMsg.IsConfirmed() {
		//Create a cache and add it to cache store
		cache := new(stateReference)
		cache.requestId = receivedMsg.RequestID()
		cache.securityName = receivedMsg.Community()
		v2cmp.cacheStore.addCache(receivedMsg.RequestID(), *cache) //Add the cache to our store

		log.Finest("Incoming confirmed request is added to cache. MSG:: %s", getMsgDebugStr(*receivedMsg, protocolOptions))

		return 0, receivedMsg, cache, nil
	} else { //RFC3412 Section 7.2 step 14 - Trapv2-PDU case
		return 0, receivedMsg, nil, nil //SecStatusInfo may contain error
	}

	return 0, receivedMsg, nil, nil
}

//ProcessOutgoingMessage prepares the message belongs to read/write/notification class. It performs necessary encoding and returns the bytes ready to be send to the destination.
//Return error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU,
	expectResponse bool,
) (int32, []byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	version := consts.Version(V2CMP)
	community := securityName //SecurityName is the community name as per RFC definition
	if securityLevel != security.NoAuthNoPriv ||
		securityModel != security.V2Csec {
		return -1, nil, nil, errors.New(fmt.Sprintf("Message Processing Error: Unsupported security model for v2c message processing. ModelID: %d", securityModel))
	}

	//We are preparing scoped pdu, because security layer expects scopedPDU
	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SnmpPDU = snmpPDU

	secSS := v2cmp.localEngine.SecuritySubSystem()
	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateRequestMsg(version,
		-1,
		-1,
		0,
		securityModel,
		community, //community string
		nil,       //Can be local (or) from LCD
		*scopedPDU,
		protocolOptions,
	)

	//Security Module returned some error
	if secStatusInfo != nil {
		log.Fatal("Error in preparing the outgoing msg: " + secStatusInfo.Error())
		return -1, nil, tmStateReference, errors.New(secStatusInfo.Error())
	}

	//Let's cache outgoing requests, so that we can match it with the incoming response
	//to avoid spoofing
	if (expectResponse) || scopedPDU.IsConfirmed() {
		//Create a cache and add it to cache store
		cache := new(stateReference)
		cache.requestId = snmpPDU.RequestID()
		cache.securityName = community
		v2cmp.cacheStore.addCache(snmpPDU.RequestID(), *cache) //Add the cache to our store
		log.Finest("Outgoing message is added to the mpm cache. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
	}

	return snmpPDU.RequestID(), encodedMsg, tmStateReference, nil
}

//PrepareResponseMessage prepares the message belongs to response/internal class. It performs necessary encoding and returns the bytes ready to be send to the destination.
//Return error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
	maxResponseSize int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU, //Should contain only PDU part
	cache engine.StateReference, //This cache is required for sending the response
	statusInfo *engine.StatusInformation,
) ([]byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	//No special processing for outgoing response.
	//Only the community string from cache will be used

	//For response message we should use cache stored already from incoming request
	stateReference := v2cmp.cacheStore.removeCache(snmpPDU.RequestID())
	if stateReference == nil {
		//There is no cache found for this response, which means no incoming request
		//has been cached with this MsgId
		return nil, nil, errors.New("Unknown RequestID: There is no outstanding request cache found for this response.")
	}

	version := consts.Version(V2CMP)
	community := securityName //SecurityName is the community name as per RFC definition
	if securityLevel != security.NoAuthNoPriv ||
		securityModel != security.V2Csec {
		return nil, nil, errors.New("Message Processing Error: Unsupported security model for v2c message processing.")
	}

	//We are preparing scoped pdu, because security layer expects scopedPDU
	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SnmpPDU = snmpPDU

	secSS := v2cmp.localEngine.SecuritySubSystem()
	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateResponseMsg(version,
		-1,
		-1,
		0,
		securityModel,
		community, //community string
		nil,       //Can be local (or) from LCD
		*scopedPDU,
		stateReference.securityStateReference(), //Can be used for community cache
		protocolOptions,
	)

	//Release the security cache, as we are done with the response processing
	secSS.ReleaseSecurityCache(securityModel, stateReference.securityStateReference())
	if secStatusInfo != nil {
		v2cmp.cacheStore.removeCache(stateReference.requestId)
		log.Fatal("Error in preparing response message. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
		return nil, tmStateReference, errors.New(secStatusInfo.Error())
	}

	return encodedMsg, tmStateReference, nil
}

//EncodeMessage encodes the SNMPv2c message and returns raw bytes.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) EncodeMessage(msg msg.SnmpMessage) ([]byte, error) {
	communityBytes := []byte(msg.Community())

	//Encode V2C VarBinds
	v2cVarbSlice, err := encodeV2CVarBinds(msg.VarBinds())
	if err != nil {
		return nil, err
	}

	//Marshal command PDU
	var v2cCommandData []byte
	var pduEncError error
	var varbError error

	if msg.Command() == consts.GetBulkRequest {
		v2cPDUImp := v2CPDU{msg.RequestID(), msg.NonRepeaters(), msg.MaxRepetitions(), v2cVarbSlice}
		v2cCommandData, pduEncError = asn1.Marshal(v2cPDUImp)
	} else {
		if msg.Command() == consts.TrapRequest {
			return nil, errors.New("Invalid SNMP Version for sending a v1 trap")
		}
		if msg.Command() == consts.Trap2Request && msg.AgentAddress() != "" && checkTrapAddress(&msg.SnmpPDU) {
			msg.SetVarBinds(addTrapAddressVarBind(msg.AgentAddress(), msg.VarBinds()))
			v2cVarbSlice, varbError = encodeV2CVarBinds(msg.VarBinds())
			if varbError != nil {
				return nil, err
			}
		}
		v2cPDUImp := v2CPDU{msg.RequestID(), int32(msg.ErrorStatus()), msg.ErrorIndex(), v2cVarbSlice}
		v2cCommandData, pduEncError = asn1.Marshal(v2cPDUImp)
	}

	if pduEncError != nil {
		return nil, errors.New("Error while encoding command PDU : " + pduEncError.Error())
	}

	if msg.Command() == 0 {
		return nil, errors.New("Invalid command type: 0")
	}

	v2cCommandDataWSeq := v2cCommandData
	v2cCommandDataWSeq[0] = byte(msg.Command())
	//Marshaling V2C PDU ends here

	//Marshal whole SnmpMessage
	rawValueV2CCommand := asn1.RawValue{0, 0, false, []byte{}, v2cCommandDataWSeq}
	v2cMsg := v2CMessage{int(msg.Version()), communityBytes, rawValueV2CCommand}
	encodingFinal, encError := asn1.Marshal(v2cMsg)

	return encodingFinal, encError

}

//encodeV2CVarBinds encodes the SnmpVar that is present in the SnmpVarBind.
//This encoded bytes are then set in the v2CPDU to be encoded finally.
func encodeV2CVarBinds(snmpVarbs []msg.SnmpVarBind) ([]v2CVarBind, error) {
	//Struct formations for encoding
	v2cVarb := v2CVarBind{}
	v2cVarbSlice := make([]v2CVarBind, len(snmpVarbs))

	//Convert []SnmpVarBind to []v2CVarBind
	for eachvarbkey, eachvarb := range snmpVarbs {
		v2cVarb.RSnmpOID = convertUInt32ToInt(eachvarb.ObjectID().Value())

		varData, varErr := eachvarb.Variable().EncodeVar()
		if varErr != nil {
			return nil, errors.New("Unable to Encode V2C PDU : Problem encoding variable : " + varErr.Error())
		}
		v2cVarb.RSnmpVar = asn1.RawValue{0, 0, false, []byte{}, varData}

		v2cVarbSlice[eachvarbkey] = v2cVarb
	}

	return v2cVarbSlice, nil
}

//addTrapAddressVarBind adds the TrapAddress varBind to the already present varBinds list.
func addTrapAddressVarBind(agentAdd string, varbs []msg.SnmpVarBind) []msg.SnmpVarBind {

	var varBind msg.SnmpVarBind
	oid := snmpvar.NewSnmpOID(".1.3.6.1.6.3.18.1.3.0")
	snmpVar := snmpvar.NewSnmpIp(agentAdd)

	if snmpVar != nil {
		varBind = msg.NewSnmpVarBind(*oid, snmpVar)
	} else {
		zeroIP := snmpvar.NewSnmpIp("0.0.0.0")
		varBind = msg.NewSnmpVarBind(*oid, zeroIP)
	}

	agentAddSlice := []msg.SnmpVarBind{varBind}
	newVarbs := append(agentAddSlice, varbs...)

	return newVarbs
}

//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) Encode(version consts.Version, msgId int32, msgMaxSize int32, msgFlags byte, msgSecurityModel int32, msgSecurityParameters []byte, scopedpdu msg.ScopedPDU) ([]byte, error) {
	return nil, errors.New("Error while encoding V2C message")
}

//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) EncodePriv(version consts.Version, msgId int32, msgMaxSize int32, msgFlags byte, msgSecurityModel int32, msgSecurityParameters []byte, encryptedScopedPDU []byte) ([]byte, error) {
	return nil, errors.New("Error while encoding V2C message")
}

//Decode decodes the v1MsgData raw bytes as SNMPv2c message.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v2cmp *SnmpMsgProcessingModelV2C) Decode(v2cMsgData []byte) (*msg.SnmpMessage, error) {

	v2cData := make([]byte, len(v2cMsgData))
	copy(v2cData, v2cMsgData)

	v2cMsg := new(v2CMessage)
	//Unmarshal whole SnmpMessage
	if _, wholeErr := asn1.Unmarshal(v2cData, v2cMsg); wholeErr != nil {
		return nil, errors.New("Error while decoding V2C message : " + wholeErr.Error())
	}

	msg := new(msg.SnmpMessage)
	msg.SetVersion(consts.Version(v2cMsg.VersionRaw))
	msg.SetCommunity(string(v2cMsg.CommunityRaw))

	v2cCommandData := v2cMsg.RawValueSnmpPDU.FullBytes
	if v2cCommandData == nil || len(v2cCommandData) <= 0 {
		return nil, errors.New("Error while decoding V2C Command PDU")
	}
	msg.SetCommand(consts.Command(v2cMsg.RawValueSnmpPDU.FullBytes[0]))

	v2cCommandDataWSeq := v2cMsg.RawValueSnmpPDU.FullBytes
	v2cCommandDataWSeq[0] = consts.Sequence

	var pduError error
	msg, pduError = decodeV2CPDU(msg, v2cCommandDataWSeq)
	if pduError != nil {
		return nil, pduError
	}

	return msg, nil
}

func decodeV2CPDU(msg *msg.SnmpMessage, pduData []byte) (*msg.SnmpMessage, error) {
	v2cPdu := new(v2CPDU)
	if _, commandErr := asn1.Unmarshal(pduData, v2cPdu); commandErr != nil {
		return nil, errors.New("Error while decoding V2C Command PDU : " + commandErr.Error())
	}

	msg.SetRequestID(v2cPdu.ReqIDRaw)

	//Decode SnmpVarBind slice
	msg.SetVarBinds(decodeV2CVarBinds(v2cPdu.VarBindSlice))

	if msg.Command() == consts.TrapRequest {
		return nil, errors.New("Invalid command for V2C PDU")
	} else if msg.Command() == consts.GetBulkRequest {
		msg.SetNonRepeaters(v2cPdu.ErrorStatusRaw)
		msg.SetMaxRepetitions(v2cPdu.ErrorIndexRaw)
	} else {
		if msg.Command() == consts.Trap2Request {
			checkTrapAddress(&msg.SnmpPDU)
		}
		msg.SetErrorStatus(consts.ErrorStatus(v2cPdu.ErrorStatusRaw))
		msg.SetErrorIndex(v2cPdu.ErrorIndexRaw)
	}

	return msg, nil
}

//decodeV2CVarBinds converts the snmpvar data that is present in the SnmpVarBind into specific SnmpVar.
//This converted []SnmpVarBind is then set in the SnmpMessage.
func decodeV2CVarBinds(v2cVarbs []v2CVarBind) []msg.SnmpVarBind {

	snmpVarb := new(msg.SnmpVarBind)
	snmpVarbSlice := make([]msg.SnmpVarBind, len(v2cVarbs))

	for eachvarbkey, eachvarb := range v2cVarbs {
		var snmpVar snmpvar.SnmpVar = snmpvar.DecodeSnmpVar(eachvarb.RSnmpVar.FullBytes)
		var snmpOid snmpvar.SnmpOID = *snmpvar.NewSnmpOIDByInts(convertIntToUInt32(eachvarb.RSnmpOID))

		snmpVarb.SetVariable(snmpVar)
		snmpVarb.SetObjectID(snmpOid)

		switch snmpVar.(type) {
		case *snmpvar.SnmpNull:
			nullVar := snmpVar.(*snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		case snmpvar.SnmpNull:
			nullVar := snmpVar.(snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		}

		snmpVarbSlice[eachvarbkey] = *snmpVarb
	}

	return snmpVarbSlice
}

//No ScopedPDU in case of V2C. Returns error.
func (v2cmp *SnmpMsgProcessingModelV2C) EncodeScopedPDU(scopedPDU msg.ScopedPDU) ([]byte, error) {
	return nil, errors.New("Error while encoding V2C message")
}

//No ScopedPDU in case of V2C. Returns error.
func (v2cmp *SnmpMsgProcessingModelV2C) DecodeScopedPDU(scopedPDUData []byte) (*msg.ScopedPDU, error) {
	return nil, errors.New("Error while encoding V2C message")
}
