/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/security/usm"
	"webnms/snmp/engine/security/v1v2"
	"webnms/snmp/engine/transport/udp"
	"webnms/snmp/msg"

	"encoding/hex"
	"testing"
)

//Test the Message Processing SubSytem with differen message processing models

var mpsTest = []struct {
	ver          consts.Version
	model        engine.SnmpModel
	msgReqBytes  []byte
	msgRespBytes []byte
	add          bool
	success      bool
}{
	{consts.Version1, v1mp, v1MsgReq, v1MsgResp, true, true},    //Valid v1 case
	{consts.Version1, v2mp, v1MsgReq, v1MsgResp, true, false},   //invalid mpm
	{consts.Version1, v1mp, v1MsgReq, v1MsgResp, false, false},  //no mpm
	{consts.Version1, sec, v1MsgReq, v1MsgResp, true, false},    //invalid model
	{consts.Version2C, v2mp, v2cMsgReq, v2cMsgResp, true, true}, //Valid v2 case
	{consts.Version3, v3mp, v3MsgReq, v3MsgResp, true, true},    //Valid v3 case
	{consts.Version3, sec, v3MsgReq, v3MsgResp, true, false},    //invalid model
}

var v1mp engine.SnmpMsgProcessingModel
var v2mp engine.SnmpMsgProcessingModel
var v3mp engine.SnmpMsgProcessingModel
var sec engine.SnmpSecurityModel

func TestMsgProcessingSubSystem(t *testing.T) {
	lcd := engine.NewSnmpLCD()
	eng := engine.NewSnmpEngine(lcd, "localhost", 100, 10)

	//Instantiate msg processing subsystem
	mps := engine.NewMsgProcessingSubSystem(eng)
	secSS := engine.NewSecuritySubSystem(eng)
	eng.SetMsgProcessingSubSystem(mps)
	eng.SetSecuritySubSystem(secSS)
	v1mp = NewMsgProcessingModelV1(mps)
	v2mp = NewMsgProcessingModelV2C(mps)
	v3mp = NewMsgProcessingModelV3(mps)
	sec = usm.NewUSMSecurityModel(secSS)

	//Register the security models
	v1v2sec := v1v2.NewSnmpSecurityModelV1V2(secSS)
	secSS.AddModel(v1v2sec) //Register V1 SecurityModel in Security SubSystem
	secSS.AddModel(v1v2sec) //Register V2C SecurityModel in Security SubSystem

	//usmModel := usm.NewUSMSecurityModel(secSS)
	secSS.AddModel(sec) //Register USM SecurityModel in Security SubSystem

	//Add the engine manually for testing
	engLCD := secSS.PeerEngineLCD(security.USMID)
	peerEngine := usm.NewUSMPeerEngine(remoteHost, remotePort)
	engID, _ := hex.DecodeString("f53212c2538b468ca56ec7e4")
	peerEngine.SetAuthoritativeEngineID(engID)
	eng.SetSnmpEngineID(engID)
	engLCD.AddEngine(peerEngine)
	//Add the User detail in USMUserLCD
	userLCD := lcd.ModelLCD(secSS, security.USMID)
	userLCD.(usm.USMUserLCD).AddUser(engID, "noAuthUser", "", "", "", "")

	secName := "noAuthUser"
	ctxName := "noAuth"
	disp := newDispatcher(eng)

	udp := new(udp.UDPProtocolOptions)
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	for _, mp := range mpsTest {

		if mp.add {
			mps.AddModel(mp.model)
		}
		//Test PrepareOutgoingMessage method - Outgoing Request
		pdu := msg.SnmpPDU{}
		pdu.SetRequestID(118) //ReqID of next incoming response - for matching
		pdu.SetCommand(consts.GetRequest)
		var secModel int32
		if mp.ver == consts.Version1 || mp.ver == consts.Version2C {
			secModel = int32(mp.ver) + 1
		} else {
			secModel = 3
		}
		msgID, _, _, err := mps.PrepareOutgoingMessage(udp,
			mp.ver,
			secModel,
			secName,
			security.NoAuthNoPriv,
			nil,
			ctxName,
			pdu,
			true,
		)
		if !(mp.success && err == nil) && !(!mp.success && err != nil) {
			t.Errorf("MPS: PrepareOutgoingMessage. Error: %v.", err)
		}
		var respByt []byte
		if err == nil && mp.success { //Receive response only when there is an outstanding request
			//Test PrepareDataElements method - Incoming Response

			switch mp.ver {
			case consts.Version1:
				respByt = v1MsgResp
			case consts.Version2C:
				respByt = v2cMsgResp
			case consts.Version3:
				respByt = mp.msgRespBytes
			}
			if mp.ver == consts.Version3 && len(respByt) > 10 {
				respByt[9] = byte(msgID)
			}
			_, _, _, si := mps.PrepareDataElements(mp.ver,
				disp,
				udp,
				respByt,
				len(respByt),
				nil,
			)
			if !(mp.success && si == nil) && !(!mp.success && si != nil) {
				t.Errorf("MPS: PrepareDataElements-Incoming Response. Error: %v.", si)
			}
		}

		var reqByt []byte
		switch mp.ver {
		case consts.Version1:
			reqByt = v1MsgReq
		case consts.Version2C:
			reqByt = v2cMsgReq
		case consts.Version3:
			reqByt = mp.msgReqBytes
		}
		//Test PrepareDataElements  method - Incoming Request
		_, mesg, stateRef, si := mps.PrepareDataElements(mp.ver,
			disp,
			udp,
			reqByt,
			len(reqByt),
			nil,
		)
		if !(mp.success && si == nil) && !(!mp.success && si != nil) {
			t.Errorf("MPS: PrepareDataElements-Incoming Request. Error: %v.", si)
		}

		if mesg != nil && si == nil && mp.success {
			//Test PrepareResponseMessage method - Outgoing Response
			pdu.SetCommand(consts.GetResponse)
			pdu.SetRequestID(mesg.RequestID())
			_, _, err := mps.PrepareResponseMessage(udp,
				mp.ver,
				65535,
				secModel,
				secName,
				security.NoAuthNoPriv,
				nil,
				"",
				pdu,
				stateRef,
				nil,
			)
			if !(mp.success && err == nil) && !(!mp.success && err != nil) {
				t.Errorf("MPS: PrepareResponseMessage. Error: %v.", err)
			}
		}

		if mp.add {
			mps.RemoveModel(int32(mp.ver))
		}
	}
}
