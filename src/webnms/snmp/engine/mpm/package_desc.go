/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package mpm provides various types for processing the incoming and outgoing SNMP messages. It is one of the parts of the Engine architecture.

  It defines message processing models for different versions of SNMP, which provides the following services as per definition of RFC3412,

  * Prepare data elements from incoming SNMP message and performs necessary security checks
    by interacting with security layer as per RFC3412 - Section 7.2
  * Prepare an outgoing message as per RFC3412 - Section 7.2
  * Prepare a response message as per RFC3412 - Section 7.2
  * Encoding and Decoding methods required for message processing

SnmpMsgProcessingModel interface defines all these services, which in turn is implemented by different message processing models such V1MP, V2CMP, V3MP required for three versions of SNMP v1, v2c, v3 respectively.

Dispatcher calls the respective message processing model to prepare the SNMP messages based on the SNMP Version such as V1/V2C/V3 available in the header.
Message Processing Models are responsible for interacting with the security subsystems based on the security model provided and prepare the messages accordingly.
This layer generates the unique MsgID for each outgoing messages.
*/
package mpm
