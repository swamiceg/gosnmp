/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package mpm

import (
	"encoding/asn1"
	"errors"
	"fmt"
	"strconv"

	"webnms/log"
	"webnms/snmp/consts"
	"webnms/snmp/engine"
	"webnms/snmp/engine/security"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
	"webnms/snmp/snmpvar"
)

//RFC 3584 - Coexistence between all three SNMP Versions

//SnmpMsgProcessingModelV1 is the message processing model implementation to process the incoming and outgoing SNMPv1 messages.
//
//Implements SnmpMsgProcessingModel interface.
type SnmpMsgProcessingModelV1 struct {
	*snmpModelImpl //Extending SnmpModelImpl - To implement SnmpModel interface
	localEngine    engine.SnmpEngine
	cacheStore     *msgCacheStore //Can be used to cache the Snmp Messages for coordination of request/response processing

	*mpmCounters
}

//Creates and returns new instance of v1 message processing model.
//'msgProcessingSubSystem' is the instance of SnmpMsgProcessingSubSystem to which this msg processing model belongs to.
//
//snmpLCD instance can be obtained from SubSystem's engine using SnmpLCD() method, where any local configuration data specific to the model can be stored.
//Refer engine's SnmpLCD for more details.
//
//For internal use only. API user should not use this function.
func NewMsgProcessingModelV1(msgProcessingSubSystem engine.SnmpMsgProcessingSubSystem) *SnmpMsgProcessingModelV1 {
	mp := new(SnmpMsgProcessingModelV1)
	mp.snmpModelImpl = newSnmpModelImpl(msgProcessingSubSystem, "V1 Message Processing Model", V1MP)
	mp.localEngine = msgProcessingSubSystem.SnmpEngine()

	//Register this model with the SnmpMsgProcessingSubSystem
	msgProcessingSubSystem.AddModel(mp)

	mp.mpmCounters = initMpmCounters()

	//This is to store all the cache (state references) of incoming messages
	mp.cacheStore = initCacheStore()

	return mp
}

//All the Raw* structs are used internally for ASN1 encoding and decoding purposes.
//API users should not use these types anywhere in their implementations.
type v1Message struct {
	VersionRaw      int
	CommunityRaw    []byte
	RawValueSnmpPDU asn1.RawValue
}

type v1VarBind struct {
	RSnmpOID asn1.ObjectIdentifier
	RSnmpVar asn1.RawValue
}

type v1PDU struct {
	ReqIDRaw       int32
	ErrorStatusRaw consts.ErrorStatus
	ErrorIndexRaw  int32
	VarBindSlice   []v1VarBind
}

type v1TrapPDU struct {
	EnterpriseRaw   asn1.ObjectIdentifier
	AgentAddRaw     asn1.RawValue
	GenericTypeRaw  int
	SpecificTypeRaw int
	UpTimeRaw       asn1.RawValue
	VarBindSlice    []v1VarBind
}

//########## Implementation of Message Processing Model ####################//

//PrepareDateElements process the incoming message. Decodes the received bytes and returns SnmpMessage.
//Returns error in case of failure in decoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) PrepareDataElements(dispatcher engine.Dispatcher, //SnmpSession by default
	protocolOptions transport.ProtocolOptions,
	wholeMsg []byte,
	wholeMsgLength int, //as received in the wire
	tmStateReference transport.TransportStateReference,
) (int32, *msg.SnmpMessage, engine.StateReference, *engine.StatusInformation) { //Can we return a decoded SnmpMessage instead??

	/*
		//Incoming Request
		commSecModel.ProcessIncomingMsg(
			msgMaxSize - maximum size message that we can generate
			securityParams - consist of security string
			securityModel - 0 for v1, 1 for v2c
			securityLevel - noAuthNoPriv
			wholeMsg,
			wholeMsgLength
		)

	*/

	receivedMsg := new(msg.SnmpMessage)
	var err error

	if receivedMsg, err = v1mp.Decode(wholeMsg); err != nil {
		v1mp.IncSnmpInASNParseErrsCounter()
		log.Error("Error while decoding the received msg from remote entity: %s. Error: %s. Dropping the message.", protocolOptions.SessionID(), err.Error())
		return -1, nil, nil, engine.NewStatusInformation(err.Error())
	}

	/*
		Security part is ignored for now. Should be plugged later.
	*/
	pduType := receivedMsg.Command()

	if pduType == consts.GetResponse || pduType == consts.ReportMessage {
		reqID := receivedMsg.RequestID()
		outStandingCache := v1mp.cacheStore.getCache(reqID)

		if outStandingCache != nil {
			//Outstanding cache is cleared
			v1mp.cacheStore.removeCache(reqID)

			//Let's not perform any checks with the cache. Just return the msg
			return 0, receivedMsg, nil, nil
		} else {
			log.Warn("Received response/report message which doesn't have matching outstanding requests. Dropping the MSG: %s", getMsgDebugStr(*receivedMsg, protocolOptions))
			return -1, nil, nil, engine.NewStatusInformation("Dropping the Msg. No relevant outstanding request found in cache. ReqID: " + strconv.Itoa(int(reqID)))
		}
	}

	//Let's prepare stateReference for the confirmed requests.
	//So that this cache can be used for sending response.
	if receivedMsg.IsConfirmed() {
		//Create a cache and add it to cache store
		cache := new(stateReference)
		cache.requestId = receivedMsg.RequestID()
		cache.securityName = receivedMsg.Community()
		v1mp.cacheStore.addCache(receivedMsg.RequestID(), *cache) //Add the cache to our store

		log.Finest("Incoming confirmed request is added to cache. MSG:: %s", getMsgDebugStr(*receivedMsg, protocolOptions))

		return 0, receivedMsg, cache, nil
	} else { //RFC3412 Section 7.2 step 14 - Trapv2-PDU case
		return 0, receivedMsg, nil, nil //SecStatusInfo may contain error
	}

	return 0, receivedMsg, nil, nil
}

//ProcessOutgoingMessage prepares the message belongs to read/write/notification class. It performs necessary encoding and returns the bytes ready to be send to the destination.
//Return error in case of failure in encoding.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) PrepareOutgoingMessage(protocolOptions transport.ProtocolOptions,
	securityModel int32,
	securityName string, //This will be used as the community string
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU,
	expectResponse bool,
) (int32, []byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	version := consts.Version(V1MP)
	community := securityName //SecurityName is the community name as per RFC definition
	if securityLevel != security.NoAuthNoPriv ||
		securityModel != security.V1sec {
		return -1, nil, nil, errors.New(fmt.Sprintf("Message Processing Error: Unsupported security model for v1 message processing. ModelID: %d", securityModel))
	}

	//We are preparing scoped pdu, because security layer expects scopedPDU
	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SnmpPDU = snmpPDU

	secSS := v1mp.localEngine.SecuritySubSystem()
	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateRequestMsg(version,
		-1,
		-1,
		0,
		securityModel,
		community, //community string
		nil,       //Can be local (or) from LCD
		*scopedPDU,
		protocolOptions,
	)

	//Security Module returned some error
	if secStatusInfo != nil {
		log.Fatal("Error in preparing the outgoing msg: " + secStatusInfo.Error())
		return -1, nil, tmStateReference, errors.New(secStatusInfo.Error())
	}

	//Let's cache outgoing requests, so that we can match it with the incoming response
	//to avoid spoofing
	if (expectResponse) || scopedPDU.IsConfirmed() {
		//Create a cache and add it to cache store
		cache := new(stateReference)
		cache.requestId = snmpPDU.RequestID()
		cache.securityName = community
		v1mp.cacheStore.addCache(snmpPDU.RequestID(), *cache) //Add the cache to our store
		log.Finest("Outgoing message is added to the mpm cache. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
	}

	return snmpPDU.RequestID(), encodedMsg, tmStateReference, nil
}

//PrepareResponseMessage prepares the message belongs to response/internal class. It performs necessary encoding and returns the bytes ready to be send to the destination.
//Return error in case of failure in encoding.
func (v1mp *SnmpMsgProcessingModelV1) PrepareResponseMessage(protocolOptions transport.ProtocolOptions,
	maxResponseSize int32, //Maximum msg. size that the remote entity can accept. Required for sending the response.
	securityModel int32,
	securityName string,
	securityLevel consts.SecurityLevel,
	contextEngineId []byte,
	contextName string,
	snmpPDU msg.SnmpPDU, //Should contain only PDU part
	cache engine.StateReference, //This cache is required for sending the response
	statusInfo *engine.StatusInformation,
) ([]byte, transport.TransportStateReference, error /*ProtocolOptions (Destination address)*/) {

	//No special processing for outgoing response.
	//Only the community string from cache will be used

	//For response message we should use cache stored already from incoming request
	stateReference := v1mp.cacheStore.removeCache(snmpPDU.RequestID())
	if stateReference == nil {
		//There is no cache found for this response, which means no incoming request
		//has been cached with this MsgId
		return nil, nil, errors.New("Unknown MsgID: There is no outstanding request cache found for this response.")
	}

	version := consts.Version(V1MP)
	community := securityName //SecurityName is the community name as per RFC definition
	if securityLevel != security.NoAuthNoPriv ||
		securityModel != security.V1sec {
		return nil, nil, errors.New("Message Processing Error: Unsupported security model for v1 message processing.")
	}

	//We are preparing scoped pdu, because security layer expects scopedPDU
	scopedPDU := new(msg.ScopedPDU)
	scopedPDU.SnmpPDU = snmpPDU

	secSS := v1mp.localEngine.SecuritySubSystem()
	_, encodedMsg, tmStateReference, secStatusInfo := secSS.GenerateResponseMsg(version,
		-1,
		-1,
		0,
		securityModel,
		community, //community string
		nil,       //Can be local (or) from LCD
		*scopedPDU,
		stateReference.securityStateReference(), //Can be used for community cache
		protocolOptions,
	)

	//Release the security cache, as we are done with the response processing
	secSS.ReleaseSecurityCache(securityModel, stateReference.securityStateReference())
	if secStatusInfo != nil {
		v1mp.cacheStore.removeCache(stateReference.requestId)
		log.Fatal("Error in preparing response message. MSG:: " + "ReqID: " + strconv.FormatInt(int64(scopedPDU.RequestID()), 10) + "Command: " + getMSGCommand(scopedPDU.Command()) + "Remote Entity:: " + protocolOptions.SessionID())
		return nil, nil, errors.New(secStatusInfo.Error())
	}

	return encodedMsg, tmStateReference, nil
}

//EncodeMessage encodes the SNMPv1 message and returns raw bytes.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) EncodeMessage(msg msg.SnmpMessage) ([]byte, error) {
	communityBytes := []byte(msg.Community())

	//Encoding V1 VarBinds
	v1VarbSlice, err := encodeV1VarBinds(msg.VarBinds())
	if err != nil {
		return nil, err
	}

	//Marshal command PDU
	var v1CommandData []byte
	var pduEncError error

	var agentRawValue asn1.RawValue

	if msg.Command() == consts.TrapRequest {
		//Encode Agent address
		agentAddImp := snmpvar.NewSnmpIp(msg.AgentAddress())
		if agentAddImp == nil {
			return nil, errors.New("Unable to encode the PDU : Problem in creating SnmpIpAddress for agent address : " + msg.AgentAddress())
		}

		agentAddBytes, agentAddErr := agentAddImp.EncodeVar()
		if agentAddErr != nil {
			return nil, errors.New("Unable to encode the Trap PDU : Problem in encoding Agent Address")
		}
		agentRawValue = asn1.RawValue{0, 0, false, []byte{}, agentAddBytes}

		upTimeBytes, upTimeErr := snmpvar.NewSnmpTimeTicks(msg.UpTime()).EncodeVar()
		if upTimeErr != nil {
			return nil, errors.New("Unable to encode the Trap PDU : Problem in encoding Agent UpTime")
		}
		upTimeRawValue := asn1.RawValue{0, 0, false, []byte{}, upTimeBytes}

		enterpriseArray := msg.Enterprise().Value()
		if enterpriseArray == nil {
			return nil, errors.New("Unable to encode the Trap PDU : Invalid Enterprise OID")
		}

		v1PDUImp := v1TrapPDU{convertUInt32ToInt(enterpriseArray), agentRawValue, msg.GenericType(), msg.SpecificType(), upTimeRawValue, v1VarbSlice}
		v1CommandData, pduEncError = asn1.Marshal(v1PDUImp)
	} else if msg.Command() == consts.Trap2Request || msg.Command() == consts.GetBulkRequest {
		return nil, errors.New("Invalid command for V1 PDU")
	} else {
		v1PDUImp := v1PDU{msg.RequestID(), msg.ErrorStatus(), msg.ErrorIndex(), v1VarbSlice}
		v1CommandData, pduEncError = asn1.Marshal(v1PDUImp)
	}

	if pduEncError != nil {
		return nil, errors.New("Error while encoding V1 PDU : " + pduEncError.Error())
	}

	if msg.Command() == 0 {
		return nil, errors.New("Invalid command type: 0")
	}

	v1CommandDataWSeq := v1CommandData
	v1CommandDataWSeq[0] = byte(msg.Command())
	//Marshaling command PDU ends here

	//Marshal whole SnmpMessage
	rawValueV1Command := asn1.RawValue{0, 0, false, []byte{}, v1CommandDataWSeq}
	v1Msg := v1Message{int(msg.Version()), communityBytes, rawValueV1Command}
	encodingFinal, encError := asn1.Marshal(v1Msg)

	return encodingFinal, encError
}

//encodeV1VarBinds encodes the SnmpVar that is present in the SnmpVarBind.
//This encoded bytes are then set in the v1PDU to be encoded finally.
func encodeV1VarBinds(snmpVarbs []msg.SnmpVarBind) ([]v1VarBind, error) {
	//Struct formations for encoding
	v1Varb := v1VarBind{}
	v1VarbSlice := make([]v1VarBind, len(snmpVarbs))

	//Convert []SnmpVarBind to []v1VarBind
	for eachvarbkey, eachvarb := range snmpVarbs {
		v1Varb.RSnmpOID = convertUInt32ToInt(eachvarb.ObjectID().Value())

		varBytes, varErr := eachvarb.Variable().EncodeVar()
		if varErr != nil {
			return nil, errors.New("Unable to Encode PDU : Problem in encoding variable : " + varErr.Error())
		}
		v1Varb.RSnmpVar = asn1.RawValue{0, 0, false, []byte{}, varBytes}

		v1VarbSlice[eachvarbkey] = v1Varb
	}

	return v1VarbSlice, nil
}

func convertUInt32ToInt(uIntSlice []uint32) []int {

	returnArr := make([]int, len(uIntSlice))
	for i, value := range uIntSlice {
		returnArr[i] = int(value)
	}
	return returnArr
}

//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) Encode(version consts.Version, msgId int32, msgMaxSize int32, msgFlags byte, msgSecurityModel int32, msgSecurityParameters []byte, scopedpdu msg.ScopedPDU) ([]byte, error) {
	return nil, errors.New("Error while encoding V1 message")
}

//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) EncodePriv(version consts.Version, msgId int32, msgMaxSize int32, msgFlags byte, msgSecurityModel int32, msgSecurityParameters []byte, encryptedScopedPDU []byte) ([]byte, error) {
	return nil, errors.New("Error while encoding V1 message")
}

//Decode decodes the v1MsgData raw bytes as SNMPv1 message.
//
//Implementation of SnmpMsgProcessingModel interface.
func (v1mp *SnmpMsgProcessingModelV1) Decode(v1MsgData []byte) (*msg.SnmpMessage, error) {

	v1Data := make([]byte, len(v1MsgData))
	copy(v1Data, v1MsgData)

	v1Msg := new(v1Message)
	//Unmarshal whole SnmpMessage
	if _, wholeErr := asn1.Unmarshal(v1Data, v1Msg); wholeErr != nil {
		return nil, errors.New("Error while decoding V1 message : " + wholeErr.Error())
	}

	msg := new(msg.SnmpMessage)
	msg.SetVersion(consts.Version(v1Msg.VersionRaw))
	msg.SetCommunity(string(v1Msg.CommunityRaw))

	v1CommandData := v1Msg.RawValueSnmpPDU.FullBytes
	if v1CommandData == nil || len(v1CommandData) <= 0 {
		return nil, errors.New("Error while decoding V1 Command PDU")
	}
	msg.SetCommand(consts.Command(v1Msg.RawValueSnmpPDU.FullBytes[0]))

	v1CommandDataWSeq := v1Msg.RawValueSnmpPDU.FullBytes
	v1CommandDataWSeq[0] = consts.Sequence

	var pduError error

	if msg.Command() == consts.TrapRequest {
		msg, pduError = decodeV1TrapRequest(msg, v1CommandDataWSeq)
	} else {
		msg, pduError = decodeV1PDU(msg, v1CommandDataWSeq)
	}

	if pduError != nil {
		return nil, pduError
	}

	return msg, nil
}

func decodeV1TrapRequest(msg *msg.SnmpMessage, trapData []byte) (*msg.SnmpMessage, error) {
	v1Pdu := new(v1TrapPDU)
	if _, trapDecErr := asn1.Unmarshal(trapData, v1Pdu); trapDecErr != nil {
		return nil, errors.New("Error while decoding V1Trap PDU : " + trapDecErr.Error())
	}

	msg.SetEnterprise(*snmpvar.NewSnmpOIDByInts(convertIntToUInt32(v1Pdu.EnterpriseRaw)))

	agentAddr, agentAddErr := snmpvar.SnmpIpAddress{}.DecodeVar(v1Pdu.AgentAddRaw.FullBytes)
	if agentAddErr != nil {
		return nil, errors.New("Error while decoding V1Trap's Agent address : " + agentAddErr.Error())
	}
	msg.SetAgentAddress(agentAddr.String())

	msg.SetGenericType(v1Pdu.GenericTypeRaw)
	msg.SetSpecificType(v1Pdu.SpecificTypeRaw)

	uptime, uptimeErr := snmpvar.SnmpTimeTicks{}.DecodeVar(v1Pdu.UpTimeRaw.FullBytes)
	if uptimeErr != nil {
		return nil, errors.New("Error while decoding the V1Trap's UpTime : " + uptimeErr.Error())
	}
	msg.SetUpTime(uptime.Value())

	//Reform SnmpVarBind slice from rawSnmpVarBind slice
	msg.SetVarBinds(decodeV1VarBinds(v1Pdu.VarBindSlice))

	return msg, nil
}

func decodeV1PDU(msg *msg.SnmpMessage, pduData []byte) (*msg.SnmpMessage, error) {
	v1Pdu := new(v1PDU)
	if _, commandErr := asn1.Unmarshal(pduData, v1Pdu); commandErr != nil {
		return nil, errors.New("Error while decoding V1 Command PDU : " + commandErr.Error())
	}

	msg.SetRequestID(v1Pdu.ReqIDRaw)

	//Reform SnmpVarBind slice from rawSnmpVarBind slice
	msg.SetVarBinds(decodeV1VarBinds(v1Pdu.VarBindSlice))

	if msg.Command() == consts.GetBulkRequest || msg.Command() == consts.Trap2Request {
		return nil, errors.New("Invalid command for V1 PDU")
	} else {
		msg.SetErrorStatus(consts.ErrorStatus(v1Pdu.ErrorStatusRaw))
		msg.SetErrorIndex(v1Pdu.ErrorIndexRaw)
	}

	return msg, nil
}

//resolveSnmpVarBinds converts the raw snmpvar bytes that is present in the SnmpVarBind into specific SnmpVar.
//This converted []SnmpVarBind is then set in the SnmpMessage.
func decodeV1VarBinds(v1Varbs []v1VarBind) []msg.SnmpVarBind {

	snmpVarb := new(msg.SnmpVarBind)
	snmpVarbSlice := make([]msg.SnmpVarBind, len(v1Varbs))

	for eachvarbkey, eachvarb := range v1Varbs {
		var snmpVar snmpvar.SnmpVar = snmpvar.DecodeSnmpVar(eachvarb.RSnmpVar.FullBytes)
		var snmpOid snmpvar.SnmpOID = *snmpvar.NewSnmpOIDByInts(convertIntToUInt32(eachvarb.RSnmpOID))

		snmpVarb.SetVariable(snmpVar)
		snmpVarb.SetObjectID(snmpOid)

		switch snmpVar.(type) {
		case *snmpvar.SnmpNull:
			nullVar := snmpVar.(*snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		case snmpvar.SnmpNull:
			nullVar := snmpVar.(snmpvar.SnmpNull)
			snmpVarb.SetExceptionIndex(int(nullVar.ErrorValue()))
		}

		snmpVarbSlice[eachvarbkey] = *snmpVarb
	}

	return snmpVarbSlice
}

//No ScopedPDU in case of V1. Returns error.
func (v1mp *SnmpMsgProcessingModelV1) EncodeScopedPDU(scopedPDU msg.ScopedPDU) ([]byte, error) {
	return nil, errors.New("Error while encoding V1 message")
}

//No ScopedPDU in case of V1. Returns error.
func (v1mp *SnmpMsgProcessingModelV1) DecodeScopedPDU(scopedPDUData []byte) (*msg.ScopedPDU, error) {
	return nil, errors.New("Error while decoding V1 message")
}

func convertIntToUInt32(intSlice []int) []uint32 {

	returnArr := make([]uint32, len(intSlice))
	for i, value := range intSlice {
		returnArr[i] = uint32(value)
	}
	return returnArr
}
