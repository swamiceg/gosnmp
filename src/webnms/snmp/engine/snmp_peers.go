/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

//SnmpPeerEngine interface to hold the SNMP Peer entity's details like EngineID, EngineBoots, EngineTime.
//
//Each Security model should implement this interface and handles the remote peer's data specific to it.
type SnmpPeerEngine interface {
	//AuthoritativeEngineID returns remote peer's SnmpEngineID.
	AuthoritativeEngineID() []byte

	//SetAuthoritativeEngineID sets the remote peer's SnmpEngineID.
	SetAuthoritativeEngineID(engineID []byte)

	//AuthoritativeEngineBoots returns the EngineBoots value of the remote peer.
	AuthoritativeEngineBoots() int32

	//SetAuthoritativeEngineBoots sets the EngineBoots value for the remote peer.
	SetAuthoritativeEngineBoots(engineBoots int32)

	//AuthoritativeEngineTime returns the EngineTime value of the remote peer in seconds.
	//This is the time from the last reboot. It returns the updated time value.
	AuthoritativeEngineTime() int32

	//SetAuthoritativeEngineTime sets the EngineTime value for the remote peer.
	SetAuthoritativeEngineTime(engineTime int32)
}

//SnmpPeerEngineLCD holds the entries of SnmpPeerEngine.
//It provides various methods to store and retrieve SnmpPeerEngine instance.
//
//Each security model should provide their own implementation to handle the remote engine entries.
//
//  Note: This interface type extends (Embeds) SnmpModelLCD interface.
type SnmpPeerEngineLCD interface {
	SnmpModelLCD //Extends SnmpModelLCD

	//AddEngine adds the SnmpPeerEngine instance on this lcd.
	AddEngine(SnmpPeerEngine) error

	//RemoveEngine removes the SnmpPeerEngine instance from the lcd based on the remote entity's address and port.
	RemoveEngine(address string, port int) error

	//RemoveEngineByEntry removes the peer engine based on the SnmpPeerEngine instance passed.
	RemoveEngineByEntry(SnmpPeerEngine)

	//RemoveAllEngines deletes all the engine entries from engine lcd.
	RemoveAllEngines()

	//Engine returns the SnmpPeerEngine instance based on the address and port passed.
	Engine(address string, port int) SnmpPeerEngine //Default method

	//EngineByID returns SnmpPeerEngine instance based on the engineID passed.
	EngineByID(engineID []byte) SnmpPeerEngine

	//Engines returns a slice of SnmpPeerEngine instance stored on this lcd.
	Engines() []SnmpPeerEngine
}
