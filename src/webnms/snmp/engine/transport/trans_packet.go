/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package transport

//SnmpTransportPacket is a packet struct used for transmission of SNMP requests/responses.
//SnmpTransportModel operate on SnmpTransportPackets for transmitting data.
type SnmpTransportPacket struct {
	//ProtocolOptions containing the transport specific params needed for communication.
	ProtocolOptions ProtocolOptions

	//Byte array containing the data to be transmitted.
	ProtocolData []byte

	//To cache model-specific and mechanism-specific parameters
	//between the Transport Subsystem and transport-aware Security Models.
	//
	//This cache should be generated and utilized by the transport models and
	//transport-aware security models.
	TmStateReference TransportStateReference
}
