/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package transport

//ProtocolOptions interface defines some transport protocol specific options that have to be implemented by the user.
//
//ProtocolOptions implementation should contain transport specific parameters such as localhost/localport/remotehost/remoteport
//needed for SnmpTransportModel implementation.
//
//In general, ProtocolOptions have on-to-one association with the TransportModel.
//
//UDP and TCP ProtocolOptions implementations are provided by default.
//
//Note: Implementation of this interface should contain pointer method receivers for proper working in the API.
type ProtocolOptions interface {
	//Returns localhost address as string.
	LocalHost() string
	//Returns local port set on protocoloptions.
	LocalPort() int
	//Returns remotehost address as string.
	RemoteHost() string
	//Return remote port set on protocoloptions.
	RemotePort() int
	//SetLocalHost sets the localhost address on the protocoloptions.
	SetLocalHost(string)
	//SetLocalPort sets the local port on the protocoloptions.
	//Returns error if port number is not valid.
	SetLocalPort(int) error
	//SetLocalHost sets the remotehost address on the protocoloptions.
	SetRemoteHost(string)
	//SetLocalPort sets the remote port on the protocoloptions.
	//Returns error if port number is not valid.
	SetRemotePort(int) error
	//SessionID returns the remote address of the form 'hostname:port'.
	SessionID() string
	//Returns a copy of the protocoloptions.
	Copy() ProtocolOptions
	//Returns the transport model ID to which this ProtocolOptions is associated with.
	//For instance, UDPProtocolOptions should return the Model ID of the UDP TransportModel.
	ID() int32
}
