/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

//Package transport provides different transport protocols like UDP/TCP to communicate with the server. it is one of the parts of Engine architecture.
//
//It defines all the Transport Models (TM) which helps in sending and receiving the packets on wire.
//
//User need not use this package directly unless want to use transport protocols other than UDP/TCP. Packages like snmp, hl internally uses it as a base layer
//for transferring packets to server.
//
//All the transport models defined under this package are implementation of engine.SnmpTransportModel interface.
//Users should implement this interface and provide implementations in order to use custom transport protocol.
//This package contains implementations for UDP and TCP, which will be used by default.
//
//Default protocol used for all communcations is UDP. Every packet sent through snmp and hl package will go through
//the UDP implementation of SnmpTransportModel and parameters needed for such transport operations is
//given through 'UDPProtocolOptions'.
//
//Refer udp and tcp package.
//
//  Steps to be followed for implementing custom transport protocol (or Transport Model):
//
//  1. Implement 'ProtocolOptions': Should implement the ProtocolOptions interface by providing necessary parameters
//     such as localhost/localport/remotehost needed for communication.
//  2. Implement 'engine.SnmpTransportModel': Should implement the SnmpTransportModel interface by handling the connection
//     open/close/read/write and should use the ProtocolOptions implemented in step 1.
//  3. This custom transport model should be plugged into the SnmpTransportSubsystem.
package transport
