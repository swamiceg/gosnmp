/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package udp is an implementation of UDP, transport level protocol for sending and receiving SNMP packets.
This package implements the required types defined in transport package

User need not use this package directly. It is internally used by low-level and high-level APIs for communication.
*/
package udp

import (
	"errors"
	"fmt"
	"net"
	"strconv"

	"webnms/snmp/engine/transport"
)

/*
	Implementation of transport's ProtocolOptions interface.
*/

//UDPProtocolOptions is an implementation of ProtocolOptions interface.
//
//Every packet that is sent through SnmpSession will go through the UDP implementation of SnmpTransportModel and
//parameters that is needed for such operations should be given through this UDPProtocolOptions.
//
//  Note: Use snmp's NewUDPProtocolOptions() function to create instance of UDPProtocolOptions.
type UDPProtocolOptions struct {
	localHost     string
	localPort     int
	remoteHost    string
	remotePort    int
	remoteAddress *net.UDPAddr

	localAddresses  []string
	localAddressIdx int

	readBuffer  int
	writeBuffer int
}

//###Getter and Setter methods for UDP###//

//SetLocalHost sets the localhost string on this UDPProtocolOptions.
func (udp *UDPProtocolOptions) SetLocalHost(host string) {
	udp.localHost = host
}

//LocalHost returns the localhost set on this UDPProtocolOptions as a string.
func (udp *UDPProtocolOptions) LocalHost() string {
	return udp.localHost
}

//SetLocalPort sets the localport number on this UDPProtocolOptions. Port number should range between 0 and 65535,
//Otherwise this method returns error without setting localport.
func (udp *UDPProtocolOptions) SetLocalPort(port int) error {
	if port < 0 || port > 65535 {
		return errors.New("Invalid port number. Port number cannot be less than 0 or greater than 65535.")
	}
	udp.localPort = port

	return nil
}

//LocalPort returns the localport set on this UDPProtocolOptions as an int.
func (udp *UDPProtocolOptions) LocalPort() int {
	return udp.localPort
}

//SetRemoteHost sets the remotehost string on this UDPProtocolOptions.
func (udp *UDPProtocolOptions) SetRemoteHost(host string) {
	udp.remoteHost = host
	udp.remoteAddress = nil //Should not use the previously set remote address
}

//RemoteHost returns the remotehost set on this UDPProtocolOptions as a string.
func (udp *UDPProtocolOptions) RemoteHost() string {
	return udp.remoteHost
}

//SetRemotePort sets the remoteport number on this UDPProtocolOptions. Port number should range between 0 and 65535,
//Otherwise this method returns error without setting remoteport.
func (udp *UDPProtocolOptions) SetRemotePort(port int) error {
	if port < 0 || port > 65535 {
		return errors.New("Invalid port number. Port number cannot be less than 0 or greater than 65535.")
	}
	udp.remotePort = port
	udp.remoteAddress = nil //Should not use the previously set remote address

	return nil
}

//RemotePort returns the remoteport set on this UDPProtocolOptions as an int.
func (udp *UDPProtocolOptions) RemotePort() int {
	return udp.remotePort
}

//SetRemoteAddress sets the UDP address on this UDPProtocolOptions.
//
//Overrides the previously set remote host and port if any.
func (udp *UDPProtocolOptions) SetRemoteAddress(address *net.UDPAddr) {
	if address != nil {
		udp.remoteAddress = address
		udp.remoteHost = address.IP.String() //Set the remote host and port
		udp.remotePort = address.Port
	}
}

//RemoteAddress returns the UDP address of remote host.
//Returns error in case remotehost (or) remoteport is invalid.
func (udp *UDPProtocolOptions) RemoteAddress() (*net.UDPAddr, error) {
	var err error = nil
	if udp.remoteAddress == nil { //If remote address (UDPAddr) is not set, then construct one from remote host string
		var remoteAddress string
		//Hostname should not be empty and port number should not be zero.
		if udp.remoteHost == "" {
			return nil, errors.New("Error in resolving remote address: Remote hostname is empty")
		}
		if udp.remotePort == 0 {
			return nil, errors.New("Error in resolving remote address: Remote port number is 0")
		}

		//Resolve IP Address. Ex: localhost -> 127.0.0.1
		if addrs, err := net.LookupIP(udp.remoteHost); err != nil {
			return nil, fmt.Errorf("Invalid IP address: %s\n%s", udp.remoteHost, err.Error())
		} else if addrs[0].To4() != nil { //IPv4 address
			remoteAddress = addrs[0].String() + ":" + strconv.Itoa(udp.remotePort)
		} else { //Not an IPv4 Address
			remoteAddress = "[" + addrs[0].String() + "]" + ":" + strconv.Itoa(udp.remotePort) //Add brackets for IPv6 Address
		}
		udp.remoteAddress, err = net.ResolveUDPAddr("udp", remoteAddress)
	}

	return udp.remoteAddress, err
}

//SetLocalAddresses sets the slice of local addresses in udp, for which it should bind.
//The session will bind to the first successfull address and will ignore the rest.
func (udp *UDPProtocolOptions) SetLocalAddresses(addresses []string) {
	udp.localAddresses = addresses
}

//LocalAddresses returns a slice of local addresses for which the session should bind with.
func (udp *UDPProtocolOptions) LocalAddresses() []string {
	return udp.localAddresses
}

//LocalAddressIndex returns the index of the successfully binded local address from the slice of
//local addresses set on this udp.
func (udp *UDPProtocolOptions) LocalAddressIndex() int {
	return udp.localAddressIdx
}

//SetReadBufferSize sets the buffer size for reading incoming SNMP packets over this UDP.
func (udp *UDPProtocolOptions) SetReadBufferSize(readBuffer int) {
	udp.readBuffer = readBuffer
}

//ReadBufferSize returns the read buffer size set on this UDPProtocolOptions.
func (udp *UDPProtocolOptions) ReadBufferSize() int {
	return udp.readBuffer
}

//SetWriteBufferSize sets the write buffer size for sending SNMP packets over this UDP.
func (udp *UDPProtocolOptions) SetWriteBufferSize(writeBuffer int) {
	udp.writeBuffer = writeBuffer
}

//WriteBufferSize returns the write buffer size set on this UDPProtocolOptions.
func (udp *UDPProtocolOptions) WriteBufferSize() int {
	return udp.writeBuffer
}

//###End - Getter and Setter methods for UDP###//

//SessionID returns the remote address of the form 'hostname:port'.
//Returns "<nil>" in case of failure in resolving the remote address.
func (udp *UDPProtocolOptions) SessionID() string {
	addr, err := udp.RemoteAddress()
	if err != nil {
		return "<nil>"
	}

	return addr.String()
}

//Returns a copy of UDPProtocolOptions.
func (udp *UDPProtocolOptions) Copy() transport.ProtocolOptions {
	po := new(UDPProtocolOptions)
	*po = *udp

	return po
}

//Returns the Model ID of the UDP Transport Model.
func (udp *UDPProtocolOptions) ID() int32 {
	return transport.UDP_TM
}
