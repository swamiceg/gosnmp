/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package udp

import (
	"net"
	"strconv"
	"testing"
)

var (
	remoteHost string = "localhost"
	remotePort int    = 8001
)

//Test UDP ProtocolOptions
func TestUDP(t *testing.T) {
	ipAddr, _ := net.ResolveIPAddr("ip", remoteHost)

	udp := new(UDPProtocolOptions)
	udp.SetLocalHost("localhost")
	udp.SetLocalPort(9001)
	udp.SetRemoteHost(remoteHost)
	udp.SetRemotePort(remotePort)

	addr, err := udp.RemoteAddress()
	if err != nil {
		t.Error(err)
	} else {
		if addr.IP.String() != ipAddr.String() {
			t.Error("UDP PO: Invalid remote address returned. Address: %s.", addr.IP.String())
		}
		if addr.Port != remotePort {
			t.Error("UDP PO: Invalid remote port returned. Port: %d.", addr.Port)
		}
	}

	if udp.SessionID() != (addr.IP.String() + ":" + strconv.Itoa(addr.Port)) {
		t.Errorf("UDP PO: Invalid SessionID returned: %s.", udp.SessionID())
	}

	//Invalid remote address test
	udp.SetRemoteHost("abc")
	addr, err = udp.RemoteAddress()
	if err == nil {
		t.Error("UDP PO: No error returned when setting invalid remote host address.")
	}
	//Change host and test
	udp.SetRemoteHost("1.1.1.1")
	addr, err = udp.RemoteAddress()
	if err != nil {
		t.Error(err)
	}
	if addr.IP.String() != "1.1.1.1" {
		t.Error("UDP PO: Invalid remote address returned. Address: %s.", addr.IP.String())
	}

	udpCopy := udp.Copy()
	if udpCopy == udp {
		t.Error("UDP PO: Copy method returning the same instance of UDP.")
	} else {
		if udpCopy.LocalHost() != udp.LocalHost() ||
			udpCopy.LocalPort() != udp.LocalPort() ||
			udpCopy.RemoteHost() != udp.RemoteHost() ||
			udpCopy.RemotePort() != udp.RemotePort() {
			t.Error("UDP PO: Failure in UDP copy")
		}
	}

}
