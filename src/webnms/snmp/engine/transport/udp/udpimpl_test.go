/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package udp

import (
	//"fmt"
	"errors"
	"net"
	"testing"
	"time"

	"webnms/snmp/engine/transport"
	"webnms/snmp/engine/transport/tcp"
)

//Test UDP transport implementation

var poStructTest = []struct {
	po     transport.ProtocolOptions
	result error
}{
	{nil, errors.New("")}, //Some error should occur
	{new(UDPProtocolOptions), nil},
	{udp, nil},
	{udp, errors.New("")},                         //Binding to same port should throw error
	{new(tcp.TCPProtocolOptions), errors.New("")}, //Some error should occur
}
var udp = &UDPProtocolOptions{
	localPort: 6000,
}

var localAddressesTest = []struct {
	addresses  []string
	index      int
	sucAddress string
}{
	{[]string{"abc", "localhost", "localhost"}, 1, "localhost"},
	{[]string{"abc", "ab", "localhost"}, 2, "localhost"},
	{[]string{"127.0.0.1", "ab", "localhost"}, 0, "127.0.0.1"},
	{[]string{"abc", "ab", "1.1.1.1"}, 0, ""},
}

func TestUDPOpen(t *testing.T) {
	udpImpl := new(UDPTransportModel)
	var err error

	//Open test with different combinations of ProtocolOptions
	lpBeforeOpen := 0
	for _, poStruct := range poStructTest {
		if poStruct.po != nil {
			lpBeforeOpen = poStruct.po.LocalPort()
		}

		err = udpImpl.Open(poStruct.po)
		if !((err == nil && poStruct.result == nil) || (err != nil && poStruct.result != nil)) {
			t.Errorf("UDP Open: Unexpected error returned while opening connection. Err: %s.", err)
		}
		if err == nil { //It should have opened the connection successfully
			if poStruct.po.LocalPort() <= 0 {
				t.Errorf("UDP Open: LocalPort is not filled in the ProtocolOptions.")
			}
			if lpBeforeOpen > 0 && poStruct.po.LocalPort() != lpBeforeOpen {
				t.Errorf("UDP Open: Binded to unexpected localport. Expected: %d. Got: %d.", lpBeforeOpen, poStruct.po.LocalPort())
			}
		}
		lpBeforeOpen = 0
	}

	//LocalAddresses Test
	for _, laddr := range localAddressesTest {
		udp := new(UDPProtocolOptions)
		udp.SetLocalAddresses(laddr.addresses)
		err = udpImpl.Open(udp)

		ipAddr, _ := net.ResolveIPAddr("ip", laddr.sucAddress) //Dotted IP address can be set on PO. hence adding this.
		if (udp.LocalHost() != laddr.sucAddress) && (udp.LocalHost() != ipAddr.String()) {
			t.Errorf("UDP Open: SetLocalAddresses - Invalid localhost set on PO. Expected: %s. Got: %s.", laddr.sucAddress, udp.LocalHost())
		}
		if udp.LocalAddressIndex() != laddr.index {
			t.Errorf("UDP Open: SetLocalAddresses - Invalid localaddress index set on PO. Expected: %d. Got: %d.", laddr.index, udp.LocalAddressIndex())
		}
	}
}

var tpPacketTest = []struct {
	po     transport.ProtocolOptions
	pData  []byte
	result error
}{
	{nil, make([]byte, 1000), errors.New("")},                         //Empty ProtocolOptions
	{new(tcp.TCPProtocolOptions), make([]byte, 1000), errors.New("")}, //Invalid ProtocolOptions
	{new(UDPProtocolOptions), nil, nil},                               //Nil ProtocolData
	{new(UDPProtocolOptions), make([]byte, 0), nil},                   //Empty ProtocolData
	{new(UDPProtocolOptions), make([]byte, 1000), nil},                //Valid ProtocolOptions
}

func TestUDPRead(t *testing.T) {
	var err error
	udpImpl := new(UDPTransportModel)

	tpPacket := new(transport.SnmpTransportPacket)
	udpPO := new(UDPProtocolOptions)
	tpPacket.ProtocolOptions = udpPO
	tpPacket.ProtocolData = make([]byte, 0)

	//Invalid Cases:
	//Call read method without opening connection
	_, err = udpImpl.Read(tpPacket)
	if err == nil {
		t.Error("UDP Read: Not throwing error while calling read on  Unopened connection.")
	}

	//Read on closed connection
	udpImpl.Open(udpPO)
	udpImpl.Close()
	tpPacket = new(transport.SnmpTransportPacket)
	udpPO = new(UDPProtocolOptions)
	udpPO.SetLocalHost("localhost")
	tpPacket.ProtocolOptions = udpPO
	tpPacket.ProtocolData = make([]byte, 1000)
	go write(udpImpl.conn, udpImpl.localAddress)
	_, err = udpImpl.Read(tpPacket)
	if err == nil {
		t.Error("UDP Read: Not throwing error while reading from closed connection.")
	}

	//Close the connection while reading
	udpImpl.Open(udpPO)
	go close(udpImpl.conn) //Close the connection while reading
	_, err = udpImpl.Read(tpPacket)
	if err == nil {
		t.Error("UDP Read: Not throwing error while reading from closed connection.")
	}

	//Read with different protocoloptions on TransportPacket
	udpImpl.Open(udpPO)
	for _, tpTest := range tpPacketTest {
		packet := new(transport.SnmpTransportPacket)
		packet.ProtocolOptions = tpTest.po
		packet.ProtocolData = tpTest.pData
		go write(udpImpl.conn, udpImpl.localAddress)
		_, err = udpImpl.Read(packet)
		if !((err == nil && tpTest.result == nil) || (err != nil && tpTest.result != nil)) {
			t.Errorf("UDP Read: Unexpected error returned while opening connection. Err: %v.", err)
		}
	}

	//Valid Cases:
	//Read on Open connection
	//udpImpl.Open(udpPO)
	go write(udpImpl.conn, udpImpl.localAddress)
	tpPacket.ProtocolOptions.SetRemoteHost("")
	tpPacket.ProtocolOptions.SetRemotePort(0)
	_, err = udpImpl.Read(tpPacket)
	if err != nil {
		t.Error(err)
	} else {
		//Check if RemoteHost/RemotePort is filled.
		if tpPacket.ProtocolOptions.RemoteHost() == "" ||
			tpPacket.ProtocolOptions.RemotePort() <= 0 {
			t.Error("UDP Read: RemoteAddress not set on the transport packet's protocoloptions.")
		}
	}
}

var tpPacketWriteTest = []struct {
	po     transport.ProtocolOptions
	pData  []byte
	result error
}{
	{nil, make([]byte, 1000), errors.New("")},                         //Empty ProtocolOptions
	{new(tcp.TCPProtocolOptions), make([]byte, 1000), errors.New("")}, //Invalid ProtocolOptions
	{new(UDPProtocolOptions), nil, errors.New("")},                    //Empty ProtocolData
	{new(UDPProtocolOptions), make([]byte, 1000), nil},                //Valid ProtocolOptions
}

func TestUDPWrite(t *testing.T) {
	var err error
	udpImpl := new(UDPTransportModel)

	tpPacket := new(transport.SnmpTransportPacket)
	udpPO := new(UDPProtocolOptions)
	tpPacket.ProtocolOptions = udpPO
	tpPacket.ProtocolData = make([]byte, 0)

	//Invalid Cases:
	//Call read method without opening connection
	err = udpImpl.Write(tpPacket)
	if err == nil {
		t.Error("UDP Write: Not throwing error while calling write on Unopened connection.")
	}

	//Write on closed connection
	udpImpl.Open(udpPO)
	udpImpl.Close()
	tpPacket = new(transport.SnmpTransportPacket)
	udpPO = new(UDPProtocolOptions)
	udpPO.SetLocalHost("localhost")
	tpPacket.ProtocolOptions = udpPO
	tpPacket.ProtocolData = make([]byte, 1000)

	err = udpImpl.Write(tpPacket)
	if err == nil {
		t.Error("UDP Write: Not throwing error while writing on closed connection.")
	}

	//Writing with different protocoloptions on TransportPacket
	err = udpImpl.Open(udpPO)
	for _, tpTest := range tpPacketWriteTest {
		packet := new(transport.SnmpTransportPacket)
		if tpTest.po != nil {
			tpTest.po.SetRemoteHost("localhost")
			tpTest.po.SetRemotePort(8001)
		}
		packet.ProtocolOptions = tpTest.po
		packet.ProtocolData = tpTest.pData

		err = udpImpl.Write(packet)
		if !((err == nil && tpTest.result == nil) || (err != nil && tpTest.result != nil)) {
			t.Errorf("UDP Write: Unexpected error returned while opening connection. Err: %v.", err)
		}
	}

	//Write with empty hostname/port
	tpPacket.ProtocolOptions.SetRemoteHost("")
	tpPacket.ProtocolOptions.SetRemotePort(0)
	err = udpImpl.Write(tpPacket)
	if err == nil {
		t.Error("UDP Write: Not throwing when remote hostname/port is empty.")
	}

	//Valid Cases:
	//Write on Open connection
	tpPacket.ProtocolOptions.SetRemoteHost("localhost")
	tpPacket.ProtocolOptions.SetRemotePort(8001)
	err = udpImpl.Write(tpPacket)
	if err != nil {
		t.Error(err)
	}
	udpImpl.Close()
}

func TestUDPClose(t *testing.T) {
	udpImpl := new(UDPTransportModel)
	var err error

	tpPacket := new(transport.SnmpTransportPacket)
	udpPO := new(UDPProtocolOptions)
	tpPacket.ProtocolOptions = udpPO
	tpPacket.ProtocolData = make([]byte, 100)

	err = udpImpl.Close()

	udpImpl.Open(udpPO)
	go func() {
		udpImpl.Read(tpPacket)
	}()
	time.Sleep(time.Second * 1) //Wait for read function to lock
	err = udpImpl.Close()
	if err != nil {
		t.Error(err)
	}
}

func write(conn *net.UDPConn, remAddress *net.UDPAddr) {
	time.Sleep(time.Millisecond * 200)
	if conn != nil {
		conn.WriteToUDP([]byte("Swami"), remAddress)
	}
}

func close(conn *net.UDPConn) {
	time.Sleep(time.Millisecond * 200)
	conn.Close()
}
