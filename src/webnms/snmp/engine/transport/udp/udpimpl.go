/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package udp

import (
	"errors"
	"fmt"
	"net"
	"strconv"
	"time"

	//User defined libs
	"webnms/log"
	"webnms/snmp/engine"
	"webnms/snmp/engine/transport"
)

/*
	Implementation of engine.SnmpTransportModel interface
*/

//UDPTransportModel is an implementation of SnmpTransportModel interface for UDP Communication.
//
//User need not create instance of this type. For internal use only.
type UDPTransportModel struct {
	transportSubsystem engine.SnmpTransportSubSystem
	conn               *net.UDPConn
	localAddress       *net.UDPAddr
	remoteAddress      *net.UDPAddr

	//Unexported fields
	connOpen bool
	readFlag bool
}

/*
 Implement SnmpModel interface
*/

//Returns the instance of Transport Subsystem to which this model is associated with.
func (udp *UDPTransportModel) SubSystem() engine.SnmpSubSystem {
	return udp.transportSubsystem
}

//Returns the name of this Transport Model.
func (udp *UDPTransportModel) Name() string {
	return "UDP Transport Model"
}

//Returns the unique id of this Transport Model.
func (udp *UDPTransportModel) ID() int32 {
	return transport.UDP_TM
}

/*
 Implement SnmpTransportModel Interface
*/

//Sets the SNMP Transport Subsystem instance to which this transport model belongs.
func (udp *UDPTransportModel) SetTransportSubSystem(transSubSystem engine.SnmpTransportSubSystem) {
	udp.transportSubsystem = transSubSystem
}

//Open opens a new UDP Connection. Binds to the localport specified in the ProtocolOptions,
//else bind to any random available port if one is not specified.
func (udp *UDPTransportModel) Open(protocolOptions transport.ProtocolOptions) error {

	//Define error params
	var errAddr error
	var errConn error

	if protocolOptions == nil {
		return errors.New("Error in opening connection: ProtocolOptions is nil.")
	}

	var udpOpt *UDPProtocolOptions

	switch (protocolOptions).(type) {
	case *UDPProtocolOptions:
		udpOpt = (protocolOptions).(*UDPProtocolOptions)
	default:
		return errors.New("Error in opening connection: Invalid ProtocolOptions.")
	}

	if udpOpt.LocalAddresses() != nil { //Try to bind with the slice of local addresses
		for i, addr := range udpOpt.LocalAddresses() {
			/*if ipAddr := net.ParseIP(addr); ipAddr == nil {
				fmt.Println("ParseIP continue:", addr)
				continue //Not an valid IP Address
			}*/
			localAddress := "[" + addr + "]" + ":" + strconv.Itoa(udpOpt.localPort) //Square brackets included for safety, when using ipv6 address
			//Resolve UDP Addresses
			if udp.localAddress, errAddr = net.ResolveUDPAddr("udp", localAddress); errAddr != nil {
				continue //Discard this invalid local address and continue
			}

			//Listen for UDP Packets
			if udp.conn, errConn = net.ListenUDP("udp", udp.localAddress); errConn == nil {
				(protocolOptions).(*UDPProtocolOptions).localAddressIdx = i //Set the successful local address index
				break
			}
		}
		//Connection is not created. Return error.
		if udp.conn == nil {
			errStr := fmt.Sprintf("Failure in opening UDP connection. LocalAddresses:: %v.", udpOpt.LocalAddresses())
			if errConn != nil {
				errStr += fmt.Sprintf(" Err: %s", errConn.Error())
			} else {
				errStr += " Err: No valid addresses found."
			}
			log.Fatal(errStr)
			return errors.New(errStr)
		}
		log.Info("UDP connection opened: Local Address: " + udp.localAddress.String())
	} else {
		localAddress := "[" + udpOpt.localHost + "]" + ":" + strconv.Itoa(udpOpt.localPort) //Square brackets included for safety, when using ipv6 address
		//Resolve UDP Addresses
		if udp.localAddress, errAddr = net.ResolveUDPAddr("udp", localAddress); errAddr != nil {
			return errors.New("Error in resolving the LocalAddress: " + errAddr.Error())
		}

		//Listen for UDP Packets
		udp.conn, errConn = net.ListenUDP("udp", udp.localAddress)
		if errConn != nil {
			log.Fatal("Failure in opening UDP connection. LocalAddress: " + udp.localAddress.String())
			return errors.New("Error in opening connection: " + errConn.Error())
		}

		//Set the binded ip address string in UDP's LocalAddresses slice.
		local_address := udp.conn.LocalAddr().(*net.UDPAddr).IP.String() //Get the local address IP string
		(protocolOptions).(*UDPProtocolOptions).SetLocalAddresses([]string{local_address})
		(protocolOptions).(*UDPProtocolOptions).localAddressIdx = 0
	}

	udp.connOpen = true //Connection open

	bindAddr := udp.conn.LocalAddr().String()
	udp.localAddress, _ = net.ResolveUDPAddr("udp", bindAddr)
	udpAddr := udp.conn.LocalAddr().(*net.UDPAddr)

	//Set the params like host/port where the connection is open in ProtocolOptions
	udpOpt.localHost = udpAddr.IP.String()
	if udpOpt.localHost == "::" {
		udpOpt.localHost = "localhost"
	}
	udpOpt.localPort = udpAddr.Port
	protocolOptions = udpOpt

	return nil
}

//Read reads the incoming packets over the UDP and return the number of bytes read
//and an error message in case of any error in reading.
func (udp *UDPTransportModel) Read(receiveSTP *transport.SnmpTransportPacket) (int, error) {

	var udpOpt *UDPProtocolOptions
	var errRead error

	//Perform all the checks
	if !udp.connOpen {
		return 0, errors.New("Connection is not open. Open method should be called through SnmpTransportModel.")
	}
	//Check for nil values
	if udp.conn == nil {
		return 0, errors.New("Cannot perform read operation: Connection object is nil.")
	}

	if receiveSTP == nil {
		return 0, errors.New("Cannot perform read operation: SnmpTransportPacket in nil. Cannot read the bytes into empty transport packet.")
	}

	po := receiveSTP.ProtocolOptions
	switch po.(type) {
	case *UDPProtocolOptions:
		udpOpt = po.(*UDPProtocolOptions)
	default:
		return 0, errors.New("Cannot perform read operation: Invalid ProtocolOptions used in reading the data.")
	}

	//Set the ReadBuffer size on the UDPConn
	if readBufferSize := udpOpt.readBuffer; readBufferSize > 0 {
		udp.conn.SetReadBuffer(readBufferSize)
	}

	//Read the incoming packets into data
	data := receiveSTP.ProtocolData

	//Blocks until packet is received
	udp.readFlag = true
	length, raddr, errRead := udp.conn.ReadFromUDP(data)
	udp.readFlag = false

	if errRead != nil {
		log.Fatal("Error during UDP read. " + errRead.Error())
		return 0, errors.New("Error in reading data: " + errRead.Error())
	}
	if string(data[:length]) == "EOF" { //Signal of UDP Connection closed
		log.Fatal("EOF signal received on UDP connection from the remote entity. Remote Address:: " + raddr.String())
		return 0, errors.New("UDP Connection has been closed")
	}

	//Set the target address:port in SnmpTransportPacket
	udpOpt.SetRemoteAddress(raddr)

	receiveSTP.ProtocolOptions = transport.ProtocolOptions(udpOpt)
	receiveSTP.ProtocolData = data[:length]

	return length, nil
}

//Write writes the packet over the UDP and return an error message if there is any failure in sending the packet.
func (udp *UDPTransportModel) Write(sendSTP *transport.SnmpTransportPacket) error {

	var udpOpt *UDPProtocolOptions
	var errAddr error
	var errWrite error

	//Perform all the checks
	if !udp.connOpen {
		return errors.New("Connection is not open. Open method should be called through SnmpTransportModel.")
	}

	if udp.conn == nil {
		return errors.New("Cannot perform write operation: UDP Connection object is nil.")
	}

	po := sendSTP.ProtocolOptions
	switch po.(type) {
	case *UDPProtocolOptions:
		udpOpt = po.(*UDPProtocolOptions)
	default:
		return errors.New("Cannot perform write operation: Invalid ProtocolOptions used for sending data.")
	}

	//Get the Remote Address (UDPAddr) from UDP ProtocolOptions
	if udp.remoteAddress, errAddr = udpOpt.RemoteAddress(); errAddr != nil {
		return errAddr
	}

	//Set the WriteBuffer size on the UDPConn
	if writeBufferSize := udpOpt.writeBuffer; writeBufferSize > 0 {
		udp.conn.SetWriteBuffer(writeBufferSize)
	}

	data := sendSTP.ProtocolData
	if data == nil {
		return errors.New("Cannot perform write operation: ProtocolData is empty.")
	}

	//Write data into remote address
	_, errWrite = udp.conn.WriteToUDP(data, udp.remoteAddress)
	if errWrite != nil {
		log.Fatal("Error in UDP write: " + errWrite.Error() + " Remote Address:: " + udp.remoteAddress.String())
		return errors.New("Error in sending data to remote host: " + errWrite.Error())
	}

	return nil
}

//Close closes the UDP Connection and return an error message if there is any failure in closing the connection.
func (udp *UDPTransportModel) Close() error {

	if !udp.connOpen || udp.conn == nil {
		//Log - Connection is not open. Closed already.
		return nil
	}

	//Close the connection after signaling read operation
	if udp.readFlag { //if we are reading on this connection
		data := []byte("EOF")
		for i := 0; i < 5; i++ { //5 attempts
			conn, err := net.ListenUDP("udp", nil) //Bind to any address
			if err != nil {
				time.Sleep(time.Nanosecond * 1) //Sleep for sometime before retrying to bind
				continue
			}
			conn.WriteToUDP(data, udp.localAddress) //Send 'EOF' to signal that we are closing this UDP connection
			conn.Close()
			break
		}
	}
	time.Sleep(time.Nanosecond * 10) //Very Impt -- Let the receiver routine to terminate

	//Close the UDP connection
	if err := udp.conn.Close(); err != nil {
		logStr := "Error in closing the UDP connection: " + err.Error()
		log.Fatal(logStr)
		return errors.New(logStr)
	}
	udp.connOpen = false //Closed the connection
	log.Info("Successfully closed the UDP connection.")

	return nil
}
