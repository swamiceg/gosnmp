/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package transport

//TransportStateReference interface is used to cache model-specific and mechanism-specific parameters
//between the Transport Subsystem and transport-aware Security Models.
//
//  TmStateReference is the default implementation of this interface.
type TransportStateReference interface {
	//Returns the security name specific to transport layer.
	//Directly used by the Transport and Security Models.
	TmSecurityName() string

	//Returns the security level requested by the security model.
	//
	//Note: Transport Model can use this to ensure that outgoing messages
	//will not be sent over an insufficiently secure session.
	TmReqSecurityLevel() byte

	//Returns the security level offered by the transport layer.
	//
	//Note: Security Model can use this to ensure that incoming messages
	//were suitably protected before acting on them.
	TmTransportSecurityLevel() byte

	//Returns bool indicating whether the Transport Model must enforce this restriction.
	TmSameSecurity() bool
}

//TmStateReference is the implementation of TransportStateReference interface.
//This statereference has to be used to cache the data required for transport model and
//transport-aware security models.
//
//Implemented as per definition of RFC5590 - Section 5.2.
//
//  Creation of TmStateReference:
//  For an incoming message, 'Transport Model' will create this cache and pass it onto Dispatcher, Messsage processing, security subsystems in order.
//  For an outgoin message, 'Security Model' will create this cache and returned to message processing subsystem, dispatcher, transport subsystem in order.
type TmStateReference struct {
	tmSecurityName      string
	tmReqSecLevel       byte
	tmTransportSecLevel byte
	tmSameSecurity      bool
}

//Getter and Setter methods for TmStateReference.

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) TmSecurityName() string {
	return tm.tmSecurityName
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) TmReqSecurityLevel() byte {
	return tm.tmReqSecLevel
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) TmTransportSecurityLevel() byte {
	return tm.tmTransportSecLevel
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) TmSameSecurity() bool {
	return tm.tmSameSecurity
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) SetTmSecurityName(tmSecurityName string) {
	tm.tmSecurityName = tmSecurityName
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) SetTmReqSecurityLevel(tmReqSecLevel byte) {
	tm.tmReqSecLevel = tmReqSecLevel
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) SetTmTransportSecurityLevel(tmTransportSecLevel byte) {
	tm.tmTransportSecLevel = tmTransportSecLevel
}

//Refer TransportStateReference interface doc.
func (tm *TmStateReference) SetTmSameSecurity(tmSameSecurity bool) {
	tm.tmSameSecurity = tmSameSecurity
}
