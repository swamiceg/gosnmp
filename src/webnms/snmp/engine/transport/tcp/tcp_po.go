/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package tcp is an implementation of TCP, transport level protocol for sending and receiving SNMP packets.
This package implements the required types defined in transport package

User need not use this package directly. It is internally used by low-level and high-level APIs for communication.
*/
package tcp

import (
	"errors"
	"fmt"
	"net"
	"strconv"

	"webnms/snmp/engine/transport"
)

/*
	Implementation of transport's ProtocolOptions interface.
*/

//TCPProtocolOptions is an implementation of ProtocolOptions interface.
//
//Every packet that is sent through SnmpSession will go through the TCP implementation of SnmpTransportModel and
//parameters that is needed for such operations should be given through this TCPProtocolOptions.
//
//  Note: Use snmp's NewTCPProtocolOptions() function to create instance of TCPProtocolOptions.
type TCPProtocolOptions struct {
	localHost     string
	localPort     int
	remoteHost    string
	remotePort    int
	remoteAddress *net.TCPAddr

	localAddresses  []string
	localAddressIdx int

	readBuffer  int
	writeBuffer int
}

//###Getter and Setter methods for TCP###//

//SetLocalHost sets the localhost string on this TCPProtocolOptions.
func (tcp *TCPProtocolOptions) SetLocalHost(host string) {
	tcp.localHost = host
}

//LocalHost returns the localhost set on this TCPProtocolOptions as a string.
func (tcp *TCPProtocolOptions) LocalHost() string {
	return tcp.localHost
}

//SetLocalPort sets the localport number on this TCPProtocolOptions. Port number should range between 0 and 65535,
//Otherwise this method returns error without setting localport.
func (tcp *TCPProtocolOptions) SetLocalPort(port int) error {
	if port < 0 || port > 65535 {
		return errors.New("Invalid port number. Port number cannot be less than 0 or greater than 65535.")
	}
	tcp.localPort = port

	return nil
}

//LocalPort returns the localport set on this TCPProtocolOptions as an int.
func (tcp *TCPProtocolOptions) LocalPort() int {
	return tcp.localPort
}

//SetRemoteHost sets the remotehost string on this TCPProtocolOptions.
func (tcp *TCPProtocolOptions) SetRemoteHost(host string) {
	tcp.remoteHost = host
	tcp.remoteAddress = nil //Should not use the previously set remote address
}

//RemoteHost returns the remotehost set on this TCPProtocolOptions as a string.
func (tcp *TCPProtocolOptions) RemoteHost() string {
	return tcp.remoteHost
}

//SetRemotePort sets the remoteport number on this TCPProtocolOptions. Port number should range between 0 and 65535,
//Otherwise this method returns error without setting remoteport.
func (tcp *TCPProtocolOptions) SetRemotePort(port int) error {
	if port < 0 || port > 65535 {
		return errors.New("Invalid port number. Port number cannot be less than 0 or greater than 65535.")
	}
	tcp.remotePort = port
	tcp.remoteAddress = nil //Should not use the previously set remote address

	return nil
}

//RemotePort returns the remoteport set on this TCPProtocolOptions as an int.
func (tcp *TCPProtocolOptions) RemotePort() int {
	return tcp.remotePort
}

//SetRemoteAddress sets the TCP address on this TCPProtocolOptions.
//
//Overrides the previously set remote host and port if any.
func (tcp *TCPProtocolOptions) SetRemoteAddress(address *net.TCPAddr) {
	if address != nil {
		tcp.remoteAddress = address
		tcp.remoteHost = address.IP.String()
		tcp.remotePort = address.Port
	}
}

//RemoteAddress returns the TCP address of remote host.
//Returns error in case remotehost (or) remoteport is invalid.
func (tcp *TCPProtocolOptions) RemoteAddress() (*net.TCPAddr, error) {
	var err error = nil
	if tcp.remoteAddress == nil { //If remote address (TCPAddr) is not set, then construct one from remote host and port
		var remoteAddress string
		//Hostname should not be empty and port number should not be zero.
		if tcp.remoteHost == "" {
			return nil, errors.New("Error in resolving remote address: Remote hostname is empty")
		}
		if tcp.remotePort == 0 {
			return nil, errors.New("Error in resolving remote address: Remote port number is 0")
		}

		//Resolve IP Address. Ex: localhost -> 127.0.0.1
		if addrs, err := net.LookupIP(tcp.remoteHost); err != nil {
			return nil, fmt.Errorf("Invalid IP address: %s\n%s", tcp.remoteHost, err.Error())
		} else if addrs[0].To4() != nil { //IPv4 address
			remoteAddress = addrs[0].String() + ":" + strconv.Itoa(tcp.remotePort)
		} else { //Not an IPv4 Address
			remoteAddress = "[" + addrs[0].String() + "]" + ":" + strconv.Itoa(tcp.remotePort) //Add brackets for IPv6 Address
		}
		tcp.remoteAddress, err = net.ResolveTCPAddr("tcp", remoteAddress)
	}

	return tcp.remoteAddress, err
}

//SetLocalAddresses sets the slice of local addresses in tcp, for which it should bind.
//The session will bind to the first successfull address and will ignore the rest.
//
//Note: This local addresses will be used for creating a TCP listener through which we can receive packets.
//TCP Implementation always binds to any available port for establishing communication with the remote entity.
func (tcp *TCPProtocolOptions) SetLocalAddresses(addresses []string) {
	tcp.localAddresses = addresses
}

//LocalAddresses returns a slice of local addresses for which the session should bind with.
func (tcp *TCPProtocolOptions) LocalAddresses() []string {
	return tcp.localAddresses
}

//LocalAddressIndex returns the index of the successfully binded local address from the slice of
//local addresses set on this tcp.
func (tcp *TCPProtocolOptions) LocalAddressIndex() int {
	return tcp.localAddressIdx
}

//SetReadBufferSize sets the buffer size for reading the incoming SNMP packets over this TCP.
func (tcp *TCPProtocolOptions) SetReadBufferSize(readBuffer int) {
	tcp.readBuffer = readBuffer
}

//ReadBufferSize returns the read buffer size set on this TCPProtocolOptions.
func (tcp *TCPProtocolOptions) ReadBufferSize() int {
	return tcp.readBuffer
}

//SetWriteBufferSize sets the write buffer size for sending SNMP packets over this TCP.
func (tcp *TCPProtocolOptions) SetWriteBufferSize(writeBuffer int) {
	tcp.writeBuffer = writeBuffer
}

//WriteBufferSize returns the write buffer size set on this TCPProtocolOptions.
func (tcp *TCPProtocolOptions) WriteBufferSize() int {
	return tcp.writeBuffer
}

//###End - Getter and Setter methods for TCP###//

//SessionID returns the remote address of the form 'hostname:port'.
//Returns "<nil>" in case of failure in resolving the remote address.
func (tcp *TCPProtocolOptions) SessionID() string {
	addr, err := tcp.RemoteAddress()
	if err != nil {
		return "<nil>"
	}

	return addr.String()
}

//Returns a copy of TCPProtocolOptions.
func (tcp *TCPProtocolOptions) Copy() transport.ProtocolOptions {
	po := new(TCPProtocolOptions)
	*po = *tcp

	return po
}

//Returns the Model ID of the TCP Transport Model.
func (tcp *TCPProtocolOptions) ID() int32 {
	return transport.TCP_TM
}
