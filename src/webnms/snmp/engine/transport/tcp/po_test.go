/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package tcp

import (
	"net"
	"strconv"
	"testing"
)

var (
	remoteHost string = "localhost"
	remotePort int    = 8001
)

//Test TCP ProtocolOptions
func TestTCP(t *testing.T) {
	ipAddr, _ := net.ResolveIPAddr("ip", remoteHost)

	tcp := new(TCPProtocolOptions)
	tcp.SetLocalHost("localhost")
	tcp.SetLocalPort(9001)
	tcp.SetRemoteHost(remoteHost)
	tcp.SetRemotePort(remotePort)

	addr, err := tcp.RemoteAddress()
	if err != nil {
		t.Error(err)
	} else {
		if addr.IP.String() != ipAddr.String() {
			t.Error("TCP PO: Invalid remote address returned. Address: %s.", addr.IP.String())
		}
		if addr.Port != remotePort {
			t.Error("TCP PO: Invalid remote port returned. Port: %d.", addr.Port)
		}
	}

	if tcp.SessionID() != (addr.IP.String() + ":" + strconv.Itoa(addr.Port)) {
		t.Errorf("TCP PO: Invalid SessionID returned: %s.", tcp.SessionID())
	}

	//Invalid remote address test
	tcp.SetRemoteHost("abc")
	addr, err = tcp.RemoteAddress()
	if err == nil {
		t.Error("TCP PO: No error returned when setting invalid remote host address.")
	}
	//Change host and test
	tcp.SetRemoteHost("1.1.1.1")
	addr, err = tcp.RemoteAddress()
	if err != nil {
		t.Error(err)
	}
	if addr.IP.String() != "1.1.1.1" {
		t.Error("TCP PO: Invalid remote address returned. Address: %s.", addr.IP.String())
	}

	tcpCopy := tcp.Copy()
	if tcpCopy == tcp {
		t.Error("TCP PO: Copy method returning the same instance of TCP.")
	} else {
		if tcpCopy.LocalHost() != tcp.LocalHost() ||
			tcpCopy.LocalPort() != tcp.LocalPort() ||
			tcpCopy.RemoteHost() != tcp.RemoteHost() ||
			tcpCopy.RemotePort() != tcp.RemotePort() {
			t.Error("TCP PO: Failure in TCP copy")
		}
	}

}
