/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package tcp

import (
	"bytes"
	"container/list"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	//User defined libs
	"webnms/log"
	"webnms/snmp/engine"
	"webnms/snmp/engine/transport"
)

/*
	Implementation of engine.SnmpTransportModel interface
*/

//TCPTransportModel is an implementation of SnmpTransportModel interface for TCP Communication.
//
//User need not create instance of this type. For internal use only.
type TCPTransportModel struct {
	transportSubsystem engine.SnmpTransportSubSystem
	localConn          *net.TCPConn
	localAddress       *net.TCPAddr
	remoteAddress      *net.TCPAddr
	listener           *tcpListener

	localConnOpen    bool
	listenerConnOpen bool
	once             sync.Once
	tcpResponses     list.List
}

/*
 Implement SnmpModel interface
*/

//Returns the instance of Transport Subsystem to which this model is associated with.
func (tcp *TCPTransportModel) SubSystem() engine.SnmpSubSystem {
	return tcp.transportSubsystem
}

//Returns the name of this Transport Model.
func (tcp *TCPTransportModel) Name() string {
	return "TCP Transport Model"
}

//Returns the unique id of this Transport Model.
func (tcp *TCPTransportModel) ID() int32 {
	return transport.TCP_TM
}

/*
 Implement SnmpTransportModel Interface
*/

//Sets the SNMP Transport Subsystem instance to which this transport model belongs.
func (tcp *TCPTransportModel) SetTransportSubSystem(transSubSystem engine.SnmpTransportSubSystem) {
	tcp.transportSubsystem = transSubSystem
}

//Open establishes a new TCP Connection with the server.
//
//  * If localport specified in protocolOptions is greater than 0, it will start the TCP listener in that port and wait for the connections,
//  through which packets can be read.
//  * If remotePort specfied in protocolOptions is greater than 0, it will establish a TCP connection with that host:port
//  after binding to any available local port.
func (tcp *TCPTransportModel) Open(protocolOptions transport.ProtocolOptions) error {

	//Define error params
	var errAddr error
	var errConn error

	if protocolOptions == nil {
		return errors.New("Error in opening connection: ProtocolOptions is nil.")
	}

	var tcpOpt *TCPProtocolOptions

	switch (protocolOptions).(type) {
	case *TCPProtocolOptions:
		tcpOpt = (protocolOptions).(*TCPProtocolOptions)
	default:
		return errors.New("Error in opening connection: Invalid ProtocolOptions.")
	}

	//If localport is non-zero, we should spawn a separate TCP receiver,
	//Which will wait for tcp connections.
	if tcpOpt.LocalAddresses() != nil {
		//If localport is specified explicitly, then create TCPListener on any valid local addressess slice.
		if tcpOpt.localPort > 0 && tcpOpt.localPort <= 65535 {
			for i, addr := range tcpOpt.LocalAddresses() {
				localAddress := "[" + addr + "]" + ":" + strconv.Itoa(tcpOpt.localPort) //Square brackets included for safety, when using ipv6 address
				//Resolve TCP Addresses
				if tcp.localAddress, errAddr = net.ResolveTCPAddr("tcp", localAddress); errAddr != nil {
					continue //Discard this local address and continue
				}

				//Start the TCP receiver to wait for TCP connection
				if tcp.listener == nil {
					tcp.listener = newTcpListener(tcp)
				}
				go tcp.listener.start(tcp.localAddress) //Start the listener at this local address

				tcp.listenerConnOpen = true                                 //TCP Listener Connection is open
				(protocolOptions).(*TCPProtocolOptions).localAddressIdx = i //Set the successful local address index
				break
			}
		}
	} else {

		//If localport is specified explicitly, then create TCPListener.
		if tcpOpt.localPort > 0 && tcpOpt.localPort <= 65535 {
			localAddress := "[" + tcpOpt.localHost + "]" + ":" + strconv.Itoa(tcpOpt.localPort) //Square brackets included for safety, when using ipv6 address
			//Resolve TCP Addresses
			if tcp.localAddress, errAddr = net.ResolveTCPAddr("tcp", localAddress); errAddr != nil {
				return errors.New("Error in resolving LocalAddress: " + errAddr.Error())
			}

			//Start the TCP receiver to wait for TCP connection
			if tcp.listener == nil {
				tcp.listener = newTcpListener(tcp)
			}
			go tcp.listener.start(tcp.localAddress) //Start the listener at this local address
			tcp.listenerConnOpen = true             //TCP Listener Connection is open

			//Set the binded ip address string in TCP's LocalAddresses slice.
			local_address := tcp.localAddress.IP.String() //Get the local address IP string
			(protocolOptions).(*TCPProtocolOptions).SetLocalAddresses([]string{local_address})
			(protocolOptions).(*TCPProtocolOptions).localAddressIdx = 0
		}
	}

	//Establish TCP connection only if remote port is specified.
	if tcpOpt.remotePort > 0 {
		if tcp.remoteAddress, errAddr = tcpOpt.RemoteAddress(); errAddr != nil {
			return errAddr
		}

		//Establish TCP Connection. Binding to any local port.
		if tcp.localConn, errConn = net.DialTCP("tcp", nil, tcp.remoteAddress); errConn == nil {
			log.Debug("TCP connection established with Remote Entity: " + tcp.remoteAddress.String())

		} else {
			//Unable to connect to remote host.
			log.Fatal("Failure in establishing TCP connection with Remote Entity: " + tcp.remoteAddress.String() + " . " + errConn.Error())
			return errors.New("Error in establishing connection: " + errConn.Error())
		}

		//Set the binded address in tcp's localaddress
		bindAddr := tcp.localConn.LocalAddr().String()
		tcp.localAddress, _ = net.ResolveTCPAddr("tcp", bindAddr)
		tcpAddr := tcp.localConn.LocalAddr().(*net.TCPAddr)

		//Set the params like local host/port where the connection is open, in ProtocolOptions
		tcpOpt.localHost = tcpAddr.IP.String()
		if tcpOpt.localHost == "::" {
			tcpOpt.localHost = "localhost"
		}
		tcpOpt.localPort = tcpAddr.Port
		protocolOptions = tcpOpt

		tcp.localConnOpen = true //TCP Connection is open
	}

	return nil
}

//Read reads the incoming packets over the TCP Connection and return the number of bytes read
//and an error message in case of any error in reading.
func (tcp *TCPTransportModel) Read(receiveSTP *transport.SnmpTransportPacket) (int, error) {

	//Start the reader routine only once - loop until this connection get closed
	if tcp.localConn != nil && tcp.localConnOpen {
		tcp.once.Do(func() {
			go tcp.read()
		})
	}
	var tcpOpt *TCPProtocolOptions

	//Perform all the checks
	if !tcp.localConnOpen && !tcp.listenerConnOpen {
		return 0, errors.New("Connection is not open. Call open method through SnmpTransportModel.")
	}

	if receiveSTP == nil {
		return 0, errors.New("Cannot perform read operation: SnmpTransportPacket in nil.")
	}

	po := receiveSTP.ProtocolOptions
	switch po.(type) {
	case *TCPProtocolOptions:
		tcpOpt = po.(*TCPProtocolOptions)
	default:
		return 0, errors.New("Cannot perform read operation: Invalid ProtocolOptions")
	}

	//Set the ReadBuffer size on the TCPConn
	if readBufferSize := tcpOpt.readBuffer; readBufferSize > 0 {
		if tcp.localConn != nil {
			tcp.localConn.SetReadBuffer(readBufferSize)
		}
	}

	//Loop until packet comes in response list
	for {
		for p := tcp.tcpResponses.Front(); p != nil; p = p.Next() {
			transPacket := p.Value.(*transport.SnmpTransportPacket)

			receiveSTP.ProtocolData = transPacket.ProtocolData
			rpo := receiveSTP.ProtocolOptions.(*TCPProtocolOptions)
			raddr, err := transPacket.ProtocolOptions.(*TCPProtocolOptions).RemoteAddress()
			if err == nil {
				rpo.SetRemoteAddress(raddr)
			}

			tcp.tcpResponses.Remove(p)
			return len(transPacket.ProtocolData), nil
		}
		time.Sleep(time.Nanosecond * 1000)               //Wait for some time before checking the response list
		if !tcp.localConnOpen && !tcp.listenerConnOpen { //If all TCP connections are closed, stop reading.
			log.Fatal("TCP connection is closed. Cannot read packets. Remote Address:: %s. LocalAddress(s): %v", tcp.remoteAddress.String(), tcpOpt.LocalAddresses())
			return 0, errors.New("TCP Connection is closed. Cannot read packets on this Transport Provider.")
		}
	}

	return 0, errors.New("Timed out. Didn't receive any response from the server.")
}

//Write writes the packet over the TCP Connection established and return an error message if there is any failure in sending the packet.
func (tcp *TCPTransportModel) Write(sendSTP *transport.SnmpTransportPacket) error {

	var errWrite error
	var errConn error

	if !tcp.localConnOpen { //Write is allowed only on local connection (localConn)
		//Can reconnect to the server
		return errors.New("Connection is not open. Call open method through SnmpTransportModel.")
	}

	if tcp.remoteAddress != nil && tcp.localConn == nil {
		//Reconnect to the server
		tcp.localConnOpen = false
		log.Debug("TCP Connection is not open!", "\n", "Trying to Reconnect to ", "\"", tcp.remoteAddress.IP.String(), ":", strconv.Itoa(tcp.remoteAddress.Port), "\"", "...")
		tcp.localConn, errConn = net.DialTCP("tcp", nil, tcp.remoteAddress)
		if errConn != nil {
			logStr := "Error in re-establishing TCP Connection. " + errConn.Error() + " Remote Address: " + tcp.remoteAddress.String()
			log.Fatal(logStr)
			return errors.New(logStr)
		}
		tcp.localConnOpen = true //Connection is re-established
	}

	data := sendSTP.ProtocolData
	if data == nil {
		return errors.New("Cannot perform write operation: ProtocolData is empty.")
	}

	//Write data into remote address
	_, errWrite = tcp.localConn.Write(data)
	if errWrite != nil {
		log.Fatal("Error in sending data to remote host: " + tcp.remoteAddress.String())
		return errors.New("Error in sending data to remote host: " + errWrite.Error())
	}

	return nil
}

//Close closes the TCP Connection and returns an error if there is any failure in closing the connection.
func (tcp *TCPTransportModel) Close() error {

	if !tcp.localConnOpen && !tcp.listenerConnOpen {
		log.Debug("No TCP connection(s) is open. Might be closed already (or) not opened at all.")
		return nil
	}

	//First close the local TCP connection
	if tcp.localConn != nil {
		if err := tcp.localConn.Close(); err != nil {
			logStr := fmt.Sprintf("Error in closing the connection established with remote entity: %s. Error: %s.", tcp.localConn.RemoteAddr().String(), err.Error())
			log.Fatal(logStr)
			return errors.New(logStr)
		}
		log.Debug("TCP connection established with the remote entity: '%s' is closed successfully.", tcp.remoteAddress.String())
	}
	tcp.localConnOpen = false //Closed the local tcp connection

	//Now, close the tcp listener and all the accepted connections
	if tcp.listener != nil {
		tcp.listener.close()

		log.Debug("TCP Listener connections are successfully closed.")
	}
	tcp.listenerConnOpen = false

	return nil
}

//Variables and constants used for processing received bytes
const (
	sp          byte = byte('@') //Special character for packet separation
	readTimeout int  = 1000      //In Millisecond
)

//Main read method to take care of reading from local TCP connection.
func (tcp *TCPTransportModel) read() {
	var buf [65535]byte
	var te *tcpConnEntity

	if tcp.localConn != nil {
		te = newTcpConnEntity(tcp, tcp.localConn)
	} else {
		log.Fatal("Local connection instance is nil. Failure in starting the read routine.")
		return //Donot start the read routine
	}

	log.Debug("Started read routine to read packets on local TCP connection.")

	for {
		if tcp.localConn == nil || !tcp.localConnOpen {
			break
		}
		length, err := te.conn.Read(buf[0:])
		if err != nil {
			if err == io.EOF {
				log.Debug("Breaking the read routine on the connection with RemoteAddr: %s; LocalAddr: %s; EOF signal.",
					te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
			} else {
				log.Fatal("Error during TCP read: %s. RemoteAddr: %s; LocalAddr: %s;", err.Error(),
					te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
			}
			tcp.localConnOpen = false //Signal the close the connection
			break
		} else {
			if length > 0 {
				te.processReceivedBytes(buf[0:length], uint64(length)) //Process only if we have received at least one byte
			}
		}
	}
	log.Debug("TCP Read routine is ended. No longer reads incoming packets on TCP connection. Remote Address: %s.", te.conn.RemoteAddr().String())
}

/*
	TCPListener - Listens for TCP connections, through which we can read packets
*/

//TCPListener to listen for TCP connections.
//It accepts the connection and spawn new go routine to read and process the incoming packets on that connection.
//No write will be performed on those connections.
type tcpListener struct {
	tcpConnPool map[int32]*tcpConnEntity //Holds all the connection entities
	id          int32

	tcpImpl  *TCPTransportModel
	listener *net.TCPListener
}

func newTcpListener(tcpImpl *TCPTransportModel) *tcpListener {
	return &tcpListener{
		tcpConnPool: make(map[int32]*tcpConnEntity),
		tcpImpl:     tcpImpl,
	}
}

//TCP Listener to wait for TCP connections
func (tl *tcpListener) start(localAddr *net.TCPAddr) {
	log.Debug("Started TCPListener routine to listen for TCP connections on address: %s.", localAddr.String())
	var err error = nil
	//Start the Trap receiver
	tl.listener, err = net.ListenTCP("tcp", localAddr)
	if err != nil {
		log.Fatal("Failure in starting the TCP listener. " + err.Error())
		tl.tcpImpl.listenerConnOpen = false //Failure in starting the TCP listener.
		return
	}
	log.Debug("TCP listener started at address: %s", tl.listener.Addr().String())

	for {
		//Loop until listener connection is closed.
		if !tl.tcpImpl.listenerConnOpen {
			break
		}
		conn, err := tl.listener.Accept() //Wait for tcp connection
		if err != nil {
			log.Fatal("Error while listening for TCP connection: %s. %s.", err.Error(), "TCP Listener is stopped.")
			break
		}
		tcpConn := conn.(*net.TCPConn)
		log.Debug("Accepted new TCP connection from Remote Address: %s.", tcpConn.RemoteAddr().String())

		//Handle this new TCP connection, this method will take care of reading from this connection.
		tl.handleTCPConn(tcpConn)
	}
	tl.tcpImpl.listenerConnOpen = false //Not listening for TCP connections.

	log.Debug("TCPListener routine is ended. No longer accepts TCP connection on local address: %s.", localAddr.String())
}

func (tl *tcpListener) handleTCPConn(tcpConn *net.TCPConn) {
	//Create new TCPEntity to serve this connection in separate routine
	tpp := newTcpConnEntity(tl.tcpImpl, tcpConn)
	tl.tcpConnPool[atomic.AddInt32(&tl.id, 1)] = tpp

	log.Debug("Started new go routine to handle the TCP connection from Remote Entity: %s.", tcpConn.RemoteAddr().String())
	go tpp.read() //Spawn a new read routine
}

//Close the listener and all the TCP connections accepted and available
//in the ConnPool
func (tl *tcpListener) close() {

	if tl.listener != nil {
		//Close the Listener
		err := tl.listener.Close()
		if err != nil {
			log.Fatal("Error in closing the TCP listener: %s", err.Error())
		} else {
			log.Debug("Sucessfully closed the TCP listener on address: %s.", tl.listener.Addr().String())
		}

		//Close all the existing/established connections
		for _, te := range tl.tcpConnPool {
			if te.conn != nil {
				err := te.conn.Close()
				if err != nil {
					log.Fatal("Error while closing TCP connection: %s. RemoteAddr: %s; LocalAddr: %s;", err.Error(),
						te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
				}
			}
		}

		tl.tcpImpl.listenerConnOpen = false
		log.Debug("Closed the TCP listener and all the established TCP Connections.")
	}

}

/*
	TCPConnEntity - Contains TCPConn and other details required for processing the data coming on that conn
*/

//To process bytes received in single TCP connection
//Represents params of single TCP connection. To perform read on the connection and to process the received bytes.
type tcpConnEntity struct {
	tcpImpl *TCPTransportModel
	conn    *net.TCPConn

	expected      bool
	bufPtr        uint64
	receiveBuffer []byte
	remLen        uint64
	remBuf        bytes.Buffer
}

func newTcpConnEntity(tcpImpl *TCPTransportModel, conn *net.TCPConn) *tcpConnEntity {
	return &tcpConnEntity{
		tcpImpl:  tcpImpl,
		conn:     conn,
		expected: true,
	}
}

//Read incoming tcp packets on tcpConnEntity's Connection until connection is closed.
func (te *tcpConnEntity) read() {

	var buf [65535]byte
	for {
		if !te.tcpImpl.localConnOpen && !te.tcpImpl.listenerConnOpen {
			log.Debug("Breaking the read routine on the connection with RemoteAddr: %s; LocalAddr: %s; TCP transport provider is closed.",
				te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
			break
		}

		length, err := te.conn.Read(buf[0:])
		if err != nil {
			if err == io.EOF {
				log.Debug("Breaking the read routine on the connection with RemoteAddr: %s; LocalAddr: %s; EOF signal.",
					te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
			} else {
				//log error
				log.Fatal("Error during TCP read: %s. RemoteAddr: %s; LocalAddr: %s;", err.Error(),
					te.conn.RemoteAddr().String(), te.conn.LocalAddr().String())
			}
			//Let's perform cleanup of this connection - TCPListener
			//if err == io.EOF || strings.Contains(err.Error(), "closed") {
			for key, entity := range te.tcpImpl.listener.tcpConnPool {
				if entity == te {
					delete(te.tcpImpl.listener.tcpConnPool, key)
					break
				}
			}
			//}
			break
		} else {
			if length > 0 {
				te.processReceivedBytes(buf[0:length], uint64(length)) //Process only if we have received at least one byte
			}
		}
	}
}

func (te *tcpConnEntity) processReceivedBytes(resp []byte, length uint64) {
	start := 0
	if resp[start] == byte(48) && te.expected { //Response beginning
		//receiveBuffer[bufPtr] = sp
		te.receiveBuffer = append(te.receiveBuffer, sp) //Insert special character to indicate the packet beginning
		te.bufPtr++
		dataLength, headerLength := calculatePacketLength(resp) //calculate length of header and body
		if length >= (dataLength + headerLength) {              //Received complete packet
			bufStart := te.bufPtr
			for i := 0; uint64(i) < (dataLength + headerLength); i++ {
				//receiveBuffer[bufPtr] = resp[i]
				te.receiveBuffer = append(te.receiveBuffer, resp[i])
				te.bufPtr++
			}
			te.expected = true //Flag - received complete packet

			//Push the transport packet to tcp response list
			transPacket := new(transport.SnmpTransportPacket)
			transPacket.ProtocolData = te.receiveBuffer[(bufStart):(bufStart + dataLength + headerLength)]
			po := new(TCPProtocolOptions)
			po.SetRemoteAddress(te.conn.RemoteAddr().(*net.TCPAddr))
			transPacket.ProtocolOptions = po

			te.tcpImpl.tcpResponses.PushBack(transPacket)

			if length > (dataLength + headerLength) { //Still more bytes left??
				te.processReceivedBytes(resp[(dataLength+headerLength):length], length-(dataLength+headerLength)) //Process the remaining bytes of next packet
			}
		} else { //Received partial packet
			te.remLen = (dataLength + headerLength) - length // Length of remaining bytes that we din't receive in this packet
			for i := 0; uint64(i) < length; i++ {
				//receiveBuffer[bufPtr] = resp[i]
				te.receiveBuffer = append(te.receiveBuffer, resp[i])
				te.bufPtr++
			}
			te.expected = false             //Flag - Didn't receive complete packet
			te.remBuf.Write(resp[0:length]) //Backup the received bytes
		}
	} else { //We are processing the packet somewhere in between (mid)
		for i := 0; uint64(i) < te.remLen; i++ { //Just copy remaining unreceived bytes
			//receiveBuffer[bufPtr] = resp[i]
			te.receiveBuffer = append(te.receiveBuffer, resp[i])
			te.bufPtr++
		}
		te.expected = true //Hurray! Received the complete packet!!

		var temp bytes.Buffer
		temp.Write(te.remBuf.Bytes()) //Packet's first half
		temp.Write(resp[0:te.remLen]) //Packet's second half
		te.remBuf.Reset()

		//Push the transport packet to tcp response list
		transPacket := new(transport.SnmpTransportPacket)
		transPacket.ProtocolData = temp.Bytes()
		po := new(TCPProtocolOptions)
		po.SetRemoteAddress(te.conn.RemoteAddr().(*net.TCPAddr))
		transPacket.ProtocolOptions = po

		te.tcpImpl.tcpResponses.PushBack(transPacket)

		if length-te.remLen > 0 { //Process the remaining bytes
			te.processReceivedBytes(resp[te.remLen:length], (length - te.remLen))
		}
	}

}

//Calculate and return the length of header and body
func calculatePacketLength(data []byte) (body, header uint64) {
	header = 1 //Sequence byte
	length := data[1]
	if length > 0x80 { //Length is of more than one bytes
		length = length - 0x80 //Number of bytes for calculating length
		body = uvarint(data[2:(2 + length)])
		header += (1 + uint64(length))
	} else { //Length is a single byte
		header += 1
		body = uint64(length)
	}

	return
}

//Calculate the length
func uvarint(buf []byte) (x uint64) {
	for i, b := range buf {
		x = x<<8 + uint64(b)
		if i == 7 {
			return
		}
	}
	return
}

//Get the Current System Time (in MilliSeconds) as an int
func currentTimeInMillis() int {
	return int(time.Now().UnixNano() / 1000000)
}
