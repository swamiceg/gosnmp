/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package tcp

import (
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"webnms/snmp/engine/transport"
	"webnms/snmp/engine/transport/udp"
)

//Test TCP transport implementation

var tcpRemoteHost string = "localhost"
var tcpRemotePort int = 9001

var tcpPOStructTest = []struct {
	po     transport.ProtocolOptions
	result error
}{
	{nil, errors.New("")},          //Some error should occur
	{new(TCPProtocolOptions), nil}, //Success
	{tcp, nil},                     //Success and create TCPListener
	{tcp, nil},                     //Binding to same port
	{new(udp.UDPProtocolOptions), errors.New("")}, //Some error should occur
}
var tcp = &TCPProtocolOptions{
	localPort: 6000,
}

/*var localAddressesTest = []struct {
	addresses []string
	index int
	sucAddress string
} {
	{[]string{"abc", "localhost", "localhost"}, 1, "localhost"},
	{[]string{"abc", "ab", "localhost"}, 2, "localhost"},
	{[]string{"127.0.0.1", "ab", "localhost"}, 0, "127.0.0.1"},
	{[]string{"abc", "ab", "1.1.1.1"}, 0, ""},
}*/

//Start a TCPListener to listen for connections, which would act as a remote entity for us to perform testing
func init() {
	go TCPListener()
}

var writeChannel = make(chan bool, 1)

func TCPListener() {
	tcpImpl := new(TCPTransportModel)

	tcpPO := new(TCPProtocolOptions)
	tcpPO.SetLocalHost(tcpRemoteHost)
	tcpPO.SetLocalPort(tcpRemotePort)

	//This should start the TCP listener
	err := tcpImpl.Open(tcpPO)
	if err != nil {
		fmt.Printf("Error while starting TCP Listener. Err: %s", err.Error())
	} else {
		for {
			select {
			case <-writeChannel:
				for _, entity := range tcpImpl.listener.tcpConnPool {
					entity.conn.Write([]byte("For testing"))
				}
			default:
				//fmt.Println("default")

			}
		}
	}
}

func TestTCPListener(t *testing.T) {
	tcpImpl := new(TCPTransportModel)
	var err error

	tcpPO := new(TCPProtocolOptions)

	err = tcpImpl.Open(tcpPO)
	if err != nil {
		t.Error(err)
	}
	if tcpImpl.listener != nil || tcpImpl.listenerConnOpen {
		t.Error("TCP Listener: Failure in tcp listener initialization.")
	}

	tcpPO.SetLocalPort(4000)
	err = tcpImpl.Open(tcpPO)
	if err != nil {
		t.Error(err)
	} else {
		if tcpImpl.listener == nil || !tcpImpl.listenerConnOpen {
			t.Error("TCP Listener: Failure in tcp listener initialization.")
		}
	}

	tcpImpl.Close()
	if tcpImpl.listenerConnOpen {
		t.Error("TCP Listener: Failure in closing tcp listener.")
	}

}

func TestTCPOpen(t *testing.T) {
	tcpImpl := new(TCPTransportModel)
	var err error

	//Open test with different combinations of ProtocolOptions
	lpBeforeOpen := 0
	for _, poStruct := range tcpPOStructTest {
		if poStruct.po != nil {
			lpBeforeOpen = poStruct.po.LocalPort()
			poStruct.po.SetRemoteHost(tcpRemoteHost)
			poStruct.po.SetRemotePort(tcpRemotePort)
		}

		err = tcpImpl.Open(poStruct.po)
		if err != nil && strings.Contains(err.Error(), "refused") {
			t.Fatalf("TCP Connection refused.")
		} else if !((err == nil && poStruct.result == nil) || (err != nil && poStruct.result != nil)) {
			t.Errorf("TCP Open: Unexpected error returned while opening connection. Err: %v.", err)
		}
		if err == nil { //It should have opened the connection successfully
			if poStruct.po.LocalPort() <= 0 {
				t.Errorf("TCP Open: LocalPort is not filled in the ProtocolOptions.")
			}
			if lpBeforeOpen > 0 && tcpImpl.listener == nil { //It should have created the TCP listener
				t.Errorf("TCP Open: TCP Listener is not created when localport is specified.")
			}
		}
		lpBeforeOpen = 0
		tcpImpl.Close()
	}
}

var tcpPacketTest = []struct {
	po     transport.ProtocolOptions
	pData  []byte
	result error
}{
	{nil, make([]byte, 1000), errors.New("")},                         //Empty ProtocolOptions
	{new(udp.UDPProtocolOptions), make([]byte, 1000), errors.New("")}, //Invalid ProtocolOptions
	{tcpPO, nil, nil},                                                 //Nil ProtocolData
	{tcpPO, make([]byte, 0), nil},                                     //Empty ProtocolData
	{tcpPO, make([]byte, 1000), nil},                                  //Valid ProtocolOptions
}

var tcpPO = &TCPProtocolOptions{
	remoteHost: tcpRemoteHost,
	remotePort: tcpRemotePort,
}

func TestTCPRead(t *testing.T) {
	var err error
	tcpImpl := new(TCPTransportModel)

	tpPacket := new(transport.SnmpTransportPacket)
	tcpPO := new(TCPProtocolOptions)
	tcpPO.SetRemoteHost(tcpRemoteHost)
	tcpPO.SetRemotePort(tcpRemotePort)
	tpPacket.ProtocolOptions = tcpPO
	tpPacket.ProtocolData = make([]byte, 0)

	//Invalid Cases:
	//Call read method without opening connection
	_, err = tcpImpl.Read(tpPacket)
	if err == nil {
		t.Error("TCP Read: Not throwing error while calling read on  Unopened connection.")
	}

	//Read on closed connection
	err = tcpImpl.Open(tcpPO)
	if err != nil && strings.Contains(err.Error(), "refused") {
		t.Fatalf("TCP Connection refused.")
	}
	tcpImpl.Close()
	tpPacket = new(transport.SnmpTransportPacket)
	tcpPO = new(TCPProtocolOptions)
	tcpPO.SetRemoteHost(tcpRemoteHost)
	tcpPO.SetRemotePort(tcpRemotePort)
	tpPacket.ProtocolOptions = tcpPO
	tpPacket.ProtocolData = make([]byte, 1000)

	_, err = tcpImpl.Read(tpPacket)
	if err == nil {
		t.Error("TCP Read: Not throwing error while reading from closed connection.")
	}

	//Close the connection while reading
	err = tcpImpl.Open(tcpPO)
	if err != nil && strings.Contains(err.Error(), "refused") {
		t.Fatalf("TCP Connection refused.")
	}
	go closeTCP(tcpImpl) //Close the connection while reading
	_, err = tcpImpl.Read(tpPacket)
	if err == nil {
		t.Error("TCP Read: Not throwing error while reading from closed connection.")
	}

	//Read with different protocoloptions on TransportPacket
	tcpImpl = new(TCPTransportModel)
	err = tcpImpl.Open(tcpPO)
	if err != nil && strings.Contains(err.Error(), "refused") {
		t.Fatalf("TCP Connection refused.")
	}
	for _, tpTest := range tcpPacketTest {
		packet := new(transport.SnmpTransportPacket)
		packet.ProtocolOptions = tpTest.po
		packet.ProtocolData = tpTest.pData
		writeChannel <- true //Send write signal
		_, err = tcpImpl.Read(packet)
		if !((err == nil && tpTest.result == nil) || (err != nil && tpTest.result != nil)) {
			t.Errorf("TCP Read: Unexpected error returned while reading from connection. Err: %v.", err)
		}
	}

	//Valid Cases:
	//Read on Open connection
	//udpImpl.Open(udpPO)
	writeChannel <- true //Send write signal
	tpPacket.ProtocolOptions.SetRemoteHost("")
	tpPacket.ProtocolOptions.SetRemotePort(0)
	_, err = tcpImpl.Read(tpPacket)
	if err != nil {
		t.Error(err)
	} else {
		//Check if RemoteHost/RemotePort is filled.
		if tpPacket.ProtocolOptions.RemoteHost() == "" ||
			tpPacket.ProtocolOptions.RemotePort() <= 0 {
			t.Error("TCP Read: RemoteAddress not set on the transport packet's protocoloptions.")
		}
	}
}

var tcpPacketWriteTest = []struct {
	po     transport.ProtocolOptions
	pData  []byte
	result error
}{
	//TCP write doesn't care about ProtocolOptions, only it needs non-nil ProtocolData
	{nil, make([]byte, 1000), nil},                         //Empty ProtocolOptions
	{new(udp.UDPProtocolOptions), make([]byte, 1000), nil}, //Invalid ProtocolOptions
	{tcp, nil, errors.New("")},                             //Empty ProtocolData
	{tcp, make([]byte, 1000), nil},                         //Valid ProtocolOptions
}

func TestTCPWrite(t *testing.T) {
	var err error
	tcpImpl := new(TCPTransportModel)

	tpPacket := new(transport.SnmpTransportPacket)
	tcpPO := new(TCPProtocolOptions)
	tcpPO.SetRemoteHost(tcpRemoteHost)
	tcpPO.SetRemotePort(tcpRemotePort)
	tpPacket.ProtocolOptions = tcpPO
	tpPacket.ProtocolData = make([]byte, 0)

	//Invalid Cases:
	//Call read method without opening connection
	err = tcpImpl.Write(tpPacket)
	if err == nil {
		t.Error("TCP Write: Not throwing error while calling write on Unopened connection.")
	}

	//Write on closed connection
	tcpImpl.Open(tcpPO)
	tcpImpl.Close()
	tpPacket = new(transport.SnmpTransportPacket)
	tcpPO = new(TCPProtocolOptions)
	tpPacket.ProtocolOptions = tcpPO
	tpPacket.ProtocolData = make([]byte, 1000)

	err = tcpImpl.Write(tpPacket)
	if err == nil {
		t.Error("TCP Write: Not throwing error while writing on closed connection.")
	}

	//Writing with different protocoloptions on TransportPacket
	tcpPO.SetRemoteHost(tcpRemoteHost)
	tcpPO.SetRemotePort(tcpRemotePort)
	err = tcpImpl.Open(tcpPO)
	if err != nil && strings.Contains(err.Error(), "refused") {
		t.Fatal("TCP connection refused.")
	}

	for _, tpTest := range tcpPacketWriteTest {
		packet := new(transport.SnmpTransportPacket)
		packet.ProtocolOptions = tpTest.po
		packet.ProtocolData = tpTest.pData

		err = tcpImpl.Write(packet)
		if !((err == nil && tpTest.result == nil) || (err != nil && tpTest.result != nil)) {
			t.Errorf("TCP Write: Unexpected error returned while writing on connection. Err: %v.", err)
		}
	}

	//Valid Cases:
	//Write on Open connection
	err = tcpImpl.Write(tpPacket)
	if err != nil {
		t.Error(err)
	}
	tcpImpl.Close()
}

func TestTCPClose(t *testing.T) {
	tcpImpl := new(TCPTransportModel)
	var err error

	tpPacket := new(transport.SnmpTransportPacket)
	tcpPO := new(TCPProtocolOptions)
	tcpPO.SetRemoteHost(tcpRemoteHost)
	tcpPO.SetRemotePort(tcpRemotePort)
	tpPacket.ProtocolOptions = tcpPO
	tpPacket.ProtocolData = make([]byte, 100)

	err = tcpImpl.Close()

	err = tcpImpl.Open(tcpPO)
	if err != nil && strings.Contains(err.Error(), "refused") {
		t.Fatal("TCP connection refused.")
	}

	go func() {
		tcpImpl.Read(tpPacket)
	}()
	time.Sleep(time.Second * 1) //Wait for read function to lock
	err = tcpImpl.localConn.Close()
	if err != nil {
		t.Error(err)
	}
}

func closeTCP(tcp *TCPTransportModel) {
	time.Sleep(time.Millisecond * 200)
	if tcp != nil {
		tcp.Close()
	}
}
