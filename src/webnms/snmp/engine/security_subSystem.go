/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"fmt"

	"webnms/snmp/consts"
	"webnms/snmp/engine/transport"
	"webnms/snmp/msg"
)

/*
 * Security Subsystem
 */

//SnmpSecuritySubSystem interface is the extension of SnmpSubSystem.
//It defines generic methods for performing security related processing on incoming and outgoing messages for all the SNMP Versions. It performs necessary security operations such as authentication/encryption on the outgoing messages, and performs ncessary security check for authenticity of incoming messages based on the security level.
//
//It holds all the SecurityModels required for processing the incoming/outgoing messages.
//All the SecurityModels should be registered with this SubSystem.
//
//Note: This interface type extends (Embeds) SnmpSubSystem interface.
type SnmpSecuritySubSystem interface { //Extends SnmpSubSystem interface
	SnmpSubSystem

	//ProcessIncomingRequest processes the incoming request messages. It performs the necessary security
	//check on the incoming messages and returns error in case of any security error as the StatusInformation,
	//which would be processed by SnmpMsgProcessingSubSystem.
	//It returns the decrypted scopedPDU and security parameters after all the security check.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	ProcessIncomingRequest(cache SecurityStateReference,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		securityParameters []byte,
		securityModel int32,
		wholeMsg []byte, //data as received in the wire
		wholeMsgLength int,
		scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
		protocolOptions transport.ProtocolOptions,
		tmStateReference transport.TransportStateReference,
	) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/)

	//ProcessIncomingResponse processes the incoming response/report messages. It performs the necessary
	//security check on the incoming messages and returns error in case of any security error as the StatusInformation,
	//which would be processed by SnmpMsgProcessingSubSystem.
	//cache which has been prepared while sending a request will be used now for processing this response.
	//It returns the decrypted scopedPDU and security parameters after all the security check.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	ProcessIncomingResponse(cache SecurityStateReference,
		msgId int32,
		msgMaxSize int32,
		msgFlags byte,
		securityParameters []byte,
		securityModel int32,
		wholeMsg []byte, //data as received in the wire
		wholeMsgLength int,
		scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
		protocolOptions transport.ProtocolOptions,
		tmStateReference transport.TransportStateReference,
	) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/)

	//GenerateRequestMsg prepares the SnmpMessage after applying the necessary security on the message.
	//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
	//It returns the final encoded byte array along with necessary security parameters filled.
	//Returns error as a StatusInformation if any.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	GenerateRequestMsg(version consts.Version,
		msgId int32,
		msgMaxSize int32, //This should be used for outgoing response
		msgFlags byte,
		securityModel int32,
		securityName string, //For response, securityName is ignored and value from cache is used.
		securityEngineId []byte,
		scopedPDU msg.ScopedPDU,
		protocolOptions transport.ProtocolOptions,
	) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/)

	//GenerateResponseMsg prepares the SnmpMessage after applying the necessary security on the message.
	//It prepares the message based on the security level. It performs encryption on the message and authenticates the same.
	//It returns the final encoded byte array along with necessary security parameters filled.
	//cache which has been prepared from the incoming request will be used now for preparing this response message.
	//Returns error as a StatusInformation if any.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	GenerateResponseMsg(version consts.Version,
		msgId int32,
		msgMaxSize int32, //This should be used for outgoing response
		msgFlags byte,
		securityModel int32,
		securityName string, //For response, securityName is ignored and value from cache is used.
		securityEngineId []byte,
		scopedPDU msg.ScopedPDU,
		cache SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
		protocolOptions transport.ProtocolOptions,
	) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/)

	//PeerEngineLCD returns the instance of SnmpPeerEngineLCD, which contains the details of remote peers such as EngineID, engineBoots etc.,
	//If the security model doesnot store the remote peers details, it returns nil.
	//Each Security model will maintain its own peer engine LCD
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	PeerEngineLCD(secModelID int32) SnmpPeerEngineLCD

	//CreateNewSecurityCache creates and returns a security state reference, which will be used for sending response messages.
	//It should contain security model specific details required for processing the response.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	CreateNewSecurityCache(secModelID int32) SecurityStateReference

	//ReleaseSecurityCache releases the security cache (SecurityStateReference) stored.
	//
	//This method routes the call to the specific security model based on the securityModel passed.
	ReleaseSecurityCache(secModelID int32, cache SecurityStateReference)
}

//Default implementation of SnmpSecuritySubSystem interface.
//It holds all the SecurityModels required for processing the incoming/outgoing messages.
//
//All the SecurityModel should be registered with this SubSystem.
//User of the API need not create instance of this SubSystem. SnmpAPI will create one and add it to the SnmpEngine instance.
type SnmpSecuritySubSystemImpl struct {
	*snmpSubSystemImpl //Anonymous Field holding different security models - Implemented SnmpSubSystem
}

//NewSecuritySubSystem returns a new instance of Security SubSystem. This method should be called to perform any security related operations on top of it.
//Users should not instantiate this type.
func NewSecuritySubSystem(engine SnmpEngine) *SnmpSecuritySubSystemImpl {
	secSS := new(SnmpSecuritySubSystemImpl)
	secSS.snmpSubSystemImpl = newSubSystem(engine)

	return secSS
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) ProcessIncomingRequest(cache SecurityStateReference,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/) {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(securityModel); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return -1, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //SnmpModel available is not of type SecurityModel
		}
	} else {
		return -1, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //No Security models defined with id 'securityModel' - cannot process the request
	}

	//This should call the desired security model
	return secModel.ProcessIncomingRequest(cache,
		msgId,
		msgMaxSize,
		msgFlags,
		securityParameters,
		securityModel,
		wholeMsg,
		wholeMsgLength,
		scopedPDU,
		protocolOptions,
		tmStateReference,
	)
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) ProcessIncomingResponse(cache SecurityStateReference,
	msgId int32,
	msgMaxSize int32,
	msgFlags byte,
	securityParameters []byte,
	securityModel int32,
	wholeMsg []byte, //data as received in the wire
	wholeMsgLength int,
	scopedPDU []byte, //Can be encrytped or unencrypted scoped pdu part
	protocolOptions transport.ProtocolOptions,
	tmStateReference transport.TransportStateReference,
) (int32 /*MaxSizeResponseScopedPDU*/, SecurityParameters, *msg.ScopedPDU, *StatusInformation /*StatusInfoException*/) {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(securityModel); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return -1, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //SnmpModel available is not of type SecurityModel
		}
	} else {
		return -1, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //No Security models defined with id 'securityModel' - cannot process the request
	}

	//This should call the desired security model
	return secModel.ProcessIncomingResponse(cache,
		msgId,
		msgMaxSize,
		msgFlags,
		securityParameters,
		securityModel,
		wholeMsg,
		wholeMsgLength,
		scopedPDU,
		protocolOptions,
		tmStateReference,
	)

}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) GenerateRequestMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU,
	protocolOptions transport.ProtocolOptions,
) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/) {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(securityModel); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return nil, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //SnmpModel available is not of type SecurityModel
		}
	} else {
		return nil, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //No Security models defined with id 'securityModel' - cannot process the request
	}

	//This should call the desired security model
	return secModel.GenerateRequestMsg(version,
		msgId,
		msgMaxSize,
		msgFlags,
		securityModel,
		securityName,
		securityEngineId,
		scopedPDU,
		protocolOptions,
	)
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) GenerateResponseMsg(version consts.Version,
	msgId int32,
	msgMaxSize int32, //This should be used for outgoing response
	msgFlags byte,
	securityModel int32,
	securityName string, //For response, securityName is ignored and value from cache is used.
	securityEngineId []byte,
	scopedPDU msg.ScopedPDU,
	cache SecurityStateReference, // This has been cached from the incoming request, which will be used now for generating response.
	protocolOptions transport.ProtocolOptions,
) (SecurityParameters, []byte, transport.TransportStateReference, *StatusInformation /*statusInfo*/) {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(securityModel); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return nil, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //SnmpModel available is not of type SecurityModel
		}
	} else {
		return nil, nil, nil, NewStatusInformation(fmt.Sprintf("No security models defined with Model ID: %d", securityModel)) //No Security models defined with id 'securityModel' - cannot process the request
	}

	//This should call the desired security model
	return secModel.GenerateResponseMsg(version,
		msgId,
		msgMaxSize,
		msgFlags,
		securityModel,
		securityName,
		securityEngineId,
		scopedPDU,
		cache,
		protocolOptions,
	)
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) PeerEngineLCD(secModelId int32) SnmpPeerEngineLCD {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(secModelId); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return nil //SnmpModel available is not of type SecurityModel
		}
	} else {
		return nil //No Security models defined with id 'securityModel' - cannot create cache
	}

	return secModel.PeerEngineLCD()
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) CreateNewSecurityCache(secModelID int32) SecurityStateReference {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(secModelID); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return nil //SnmpModel available is not of type SecurityModel
		}
	} else {
		return nil //No Security models defined with id 'securityModel' - cannot create cache
	}
	return secModel.CreateNewSecurityCache()
}

//Refer SnmpSecuritySubSystem interface doc.
func (ss *SnmpSecuritySubSystemImpl) ReleaseSecurityCache(secModelID int32, cache SecurityStateReference) {
	var secModel SnmpSecurityModel
	var ok bool

	if model := ss.Model(secModelID); model != nil {
		if secModel, ok = model.(SnmpSecurityModel); !ok {
			return //SnmpModel available is not of type SecurityModel
		}
	} else {
		return //No Security models defined with id 'securityModel' - cannot create cache
	}
	secModel.ReleaseSecurityCache(cache)
}
