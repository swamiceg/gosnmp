/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

package engine

import (
	"fmt"

	"webnms/snmp/consts"
	"webnms/snmp/engine/security"
	"webnms/snmp/msg"
)

//StatusInformation is used for processing the error between different SubSystems. It holds the error message.
//It also contains information about Errored OID with its counter, which is used for sending reports to remote entity.
//
//Used by all models to process errors/reports etc.,
type StatusInformation struct {
	errString      string
	varbindList    []msg.SnmpVarBind
	securityParams []byte
	flags          byte
	ctxEngineId    []byte
	ctxName        string
}

//Returns new instance of status information.
//For internal use only.
func NewStatusInformation(errMsg string) *StatusInformation {
	return &StatusInformation{
		errString: errMsg,
		flags:     byte(security.NoAuthNoPriv),
	}
}

//For internal use only.
func (si *StatusInformation) Error() string {
	var errMsg string
	for _, varb := range si.varbindList {
		errMsg += varb.String() + "\n"
	}
	return fmt.Sprintf("%s\n%s", si.errString, errMsg)
}

//For internal use only.
func (si *StatusInformation) ContextEngineID() []byte {
	return si.ctxEngineId
}

//For internal use only.
func (si *StatusInformation) SetContextEngineID(ctxEngineId []byte) {
	si.ctxEngineId = ctxEngineId
}

//For internal use only.
func (si *StatusInformation) ContextName() string {
	return si.ctxName
}

//For internal use only.
func (si *StatusInformation) SetContextName(ctxName string) {
	si.ctxName = ctxName
}

//For internal use only.
func (si *StatusInformation) VarBinds() []msg.SnmpVarBind {
	return si.varbindList
}

//For internal use only.
func (si *StatusInformation) SetVarbindList(varb []msg.SnmpVarBind) {
	si.varbindList = varb
}

//For internal use only.
func (si *StatusInformation) SecurityParams() []byte {
	return si.securityParams
}

//For internal use only.
func (si *StatusInformation) SetSecurityParams(secParams []byte) {
	si.securityParams = secParams
}

//For internal use only.
func (si *StatusInformation) SetMsgFlags(flag byte) {
	si.flags = flag
}

//For internal use only.
func (si *StatusInformation) MsgFlags() byte {
	return si.flags
}

//For internal use only.
func (si *StatusInformation) SecurityLevel() consts.SecurityLevel {
	return consts.SecurityLevel(si.flags & security.AuthPrivMask)
}
