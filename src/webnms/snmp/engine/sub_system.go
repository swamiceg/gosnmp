package engine

import (
	"strconv"
)

//########## SubSystem ###############//

//SNMP SubSystem interface. To allow engine framework integration, a sub system must implement this interface.
//
//A sub system is a model manager. It holds the various SnmpModels for conveninent processing.
//Every model is identified by an Unique ID within a SubSystem.
//Every sub system is associated to its SNMP engine, one-to-one mapping.
type SnmpSubSystem interface {
	//Name() string //Return a string indicating the subsystem name (Dispatcher/MPS/SecuritySS/VACM)

	//AddModel adds the SnmpModel to this SubSystem, where model is registered with the unique ID.
	//It will not register the model, if one is already registered with same ID.
	AddModel(model SnmpModel)

	//RemoveModel removes the SnmpModel from this SubSystem based on the model id.
	RemoveModel(modelID int32) SnmpModel

	//Model returns the SnmpModel instance stored on this SubSystem based on the model id, if one has registered previously.
	Model(modelID int32) SnmpModel

	//ModelIDs returns a slice of SnmpModel IDs stored in this SubSystem.
	ModelIDs() []int32

	//Models returns a slice of SnmpModel instance registered/stored in this SubSystem.
	Models() []SnmpModel

	//ModelNames returns a slice of SnmpModel names stored in this SubSystem.
	ModelNames() []string

	//SnmpEngine returns an instance of SnmpEngine to which this SubSystem is associated with.
	SnmpEngine() SnmpEngine
}

//Default implementation of SnmpSubSystem interface.
//This will hold all the SnmpModels for each SubSystems. Models should have unique model number.
//
//User not required to create an instance of this type. SnmpAPI will create all the SubSystems required such as
//Message Processing, Security SubSystems.
type snmpSubSystemImpl struct {
	subSystemModels map[int32]SnmpModel
	snmpEngine      SnmpEngine
}

//Returns instance of SubSystem. This holds all the SnmpModels.
//For internal use only.
func newSubSystem(engine SnmpEngine) *snmpSubSystemImpl {
	return &snmpSubSystemImpl{
		subSystemModels: make(map[int32]SnmpModel),
		snmpEngine:      engine, //Need to override SnmpEngine?
	}
}

//AddModel adds the SnmpModel on this SubSystem. Model id should be unique for each model.
//If model id already exist, it doesn't register this model.
func (ss *snmpSubSystemImpl) AddModel(model SnmpModel) {
	if model != nil {
		if _, ok := ss.subSystemModels[model.ID()]; ok {
			return //Model already exist
		}

		ss.subSystemModels[model.ID()] = model
	}
}

//RemoveModel removes the SnmpModel from this SubSystem based on the model id.
func (ss *snmpSubSystemImpl) RemoveModel(modelID int32) SnmpModel {
	if model, ok := ss.subSystemModels[modelID]; ok {
		delete(ss.subSystemModels, modelID)
		return model
	}
	return nil
}

//Model returns the SnmpModel instance stored on this SubSystem based on the model id, if one has registered previously.
func (ss *snmpSubSystemImpl) Model(modelID int32) SnmpModel { //Check for nil value
	return ss.subSystemModels[modelID]
}

//Models returns a slice of SnmpModel instance registered/stored in this SubSystem.
func (ss *snmpSubSystemImpl) Models() []SnmpModel {
	var models []SnmpModel
	for _, model := range ss.subSystemModels {
		models = append(models, model)
	}
	return models
}

//ModelIDs returns a slice of SnmpModel IDs stored in this SubSystem.
func (ss *snmpSubSystemImpl) ModelIDs() []int32 {
	var ids []int32
	for id, _ := range ss.subSystemModels {
		ids = append(ids, id)
	}
	return ids
}

//ModelNames returns a slice of SnmpModel names stored in this SubSystem.
func (ss *snmpSubSystemImpl) ModelNames() []string {
	var modelNames []string
	for id, model := range ss.subSystemModels {
		modelNames = append(modelNames, model.Name()+" ["+strconv.Itoa(int(id))+"]")
	}
	return modelNames
}

//SnmpEngine returns an instance of SnmpEngine to which this SubSystem is associated with.
func (ss *snmpSubSystemImpl) SnmpEngine() SnmpEngine {
	return ss.snmpEngine
}

//latestReceivedEngineTime - highest value of snmpEngineTime that was received by the
//non-authoritative SNMP engine from the authoritative SNMP engine. Non-Authoritative entity should maintain local notion of this value.

//SnmpEngineId generation algorithm - RFC3411 - Section 5
//Length of 12 octets
//First 4 bytes - Management private enterprise number assigned by IANA
//Remaining 8 octets are enterprise specific - IPAddress/MacAddress can be used.
//We can use IpAddress, port to compute the remaining 8 octets.
