/*
 * Copyright (c) 2015 ZOHO Corp. All Rights Reserved.
 * Please read the COPYRIGHT and LICENSE_AGREEMENT files bundled with the product
 * for terms and conditions associated with the usage of this source file.
 *
 * ZOHO Corp. MAKES NO REPRESENTATIONS OR WARRANTIES  ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  ZOHO Corp. SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE  OR ITS DERIVATIVES.
 */

/*
package engine is the core of SNMPv3 architecture. It comprises of all the SubSystems defined in RFC3411 such as
Dispatcher, Message Processing and Security SubSystem.

SnmpEngine represents the local entity's SNMP Engine, which holds the instance of all the SubSystems. Each sub system contains a set of models.

Example:
  * MsgProcessing SubSystem: V1MP, V2CMP, V3MP (Message Processing Models)
  * Security SubSystem: User-based Security Model (USM)
  * Transport SubSystem: UDP, TCP

It provides services for sending and receiving SNMP messages, authenticating and encrypting SNMP messages etc.,
All the SubSystems and SnmpModels defined under this package are pluggable. User can define their custom SubSystem models as per their requirement and same can be registered
with their respective SubSystems. Default implementation is provided for all the layers.

  Note: User need not use this package directly, SnmpAPI will create an instance of an SnmpEngine and the required SubSystems.
  It also registers the default SnmpModels required for all these SubSystems.
  Hence, API user can obtain the instance of SnmpEngine from the SnmpAPI instance when using Low-Level APIs.
*/
package engine
