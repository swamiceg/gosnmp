#Go SNMP API
___
&gt; Go SNMP API is a comprehensive set of libraries developed using Go Language (or GoLang), which aid users in developing 
effective network management applications at ease.
___

<u>**Introduction**</u>
Go SNMP API provides a comprehensive set of GoLang based development kit for SNMP based network management applications.
This API is built on top of Simple Network Management Protocol (SNMP), widely used application level protocol for managing 
devices in IP networks. 

Developers can leverage Go SNMP library to develop stand alone (or) web based applications for managing, monitoring and tracking network elements that are reliable and scalable.

Using this library, users can perform all the SNMP based operaions such as Get, Get-Next, Get-Bulk, Traps etc., out of the box.

Go SNMP API consists of,

* Low-Level API
* High-Level API

Low-Level API provides rich set of libraries to build efficient applications, which allows the user to manage all the
resources and all the handlings by themselves. These APIs give complete control of the resource management and processing to the user.

High-Level API abstracts the handling of low-level resources, SNMP implementation complexity and provides the user with high-level libraries to build applications very easily.
It help developers to get the job done in fewer steps when compared to Low-Level API.


<u>**Requirements**</u>
Requires Go binary distribution equivalent to the product build version to use this API.


<u>**Installation**</u>

* Install from precompiled binary distribution:
Download the precompiled binary distributions available in the website. 
Binary distributions are available for Linux/Windows - 32-bit (386) and 64-bit (amd64). If binary distribution is not available for your OS, Architecture combination, please feel free to drop a mail at snmp-support@webnms.com 

<u>Steps to install:</u>
1. Download the zip file and extract it to any custom location of your wish.
2. Set the location of your Go Executable in PATH environment variable. 
3. Go to 'conf' folder available in $PRODUCT_HOME and run the setEnv file to set the required environment variables for Go. This will set the GOROOT
   and GOPATH required for using this product.
4. Create your own Go workspace and import the packages available and start bulding the applications. 
   Please refer Documentation section for knowing the available packages and it's usage.

Note: To set up Go Workspace :: http://golang.org/doc/code.html#Workspaces


<u>**Documentation**</u>
GO SNMP API is completely documented as per Go standards, which is
available in the help folder of this product as GoDocs.
It is also available online in the following link,
[Go SNMP API Docs](<http: www.webnms.com:="">)


<u>**Licensing**</u>
Please refer to the [license file](<license.html>) bundled along with this product before using it.
Also, refer [GoLang's Open BSD License](<https: golang.org="" license="">).


<u>**Support/Contact**</u>
Have any queries about the product? (or) want to report any bugs??
Please feel free to reach us at snmp-support@webnms.com to get 24x7 Support.</https:></license.html></http:>