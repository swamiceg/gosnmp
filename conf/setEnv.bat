REM Set GOPATH and GOROOT environment variable

ECHO OFF

:: Search for go.exe
SET BREAK=
FOR /f %%i IN ('WHERE go') DO IF NOT DEFINED BREAK (
  SET GOEXE=%%i
  SET BREAK=yes
)

IF %GOEXE% == [] (
  ECHO "Go command not found"
  ECHO "Set the location of go in PATH"
  GOTO :EOF
) 

:: Set GOROOT
FOR %%F in (%GOEXE%) DO SET GOBIN=%%~dpF
FOR %%S in ("%GOBIN%\..\") DO SET GOROOT=%%~dpS

IF NOT EXIST %GOBIN%\go.exe (
  ECHO Go command not found in: %GOBIN%
  GOTO :EOF
) 

:: Set the Go bin directory in PATH
SET PATH=%PATH%;%GOBIN%

:: Set the current workspace directory in GOPATH
FOR %%i IN ("%~dp0..") DO SET "GOPATH=%%~fi"
:: ECHO GOPATH SET: %GOPATH%
