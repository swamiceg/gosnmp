#!/bin/sh

#Set GOROOT and GOPATH environment variables

# Search for GO
GOEXE=`which go`

if [ $? -ne 0 ];
then 
   echo "go command not found"
   echo "set the location of go in PATH"
   exit 1
fi

# Set GOROOT
GOBIN=`dirname $GOEXE`
GOROOT=`dirname $GOBIN`
export GOROOT
# echo "GOROOT SET: $GOROOT"

# Set the Go bin directory in PATH
if [ -f $GOBIN/go ]
then
   PATH=$GOBIN:$PATH
   export PATH
   # echo "PATH SET: $PATH"
else
   echo "Go executable not found in $GOBIN "
   exit 1
fi

# Set the current workspace in GOPATH
GP=`echo $GOPATH`
GOPATH=`pwd`
GOPATH=`dirname $GOPATH`
if [ ! -z "$GP" -a "$GP" != " " ]; then
	GOPATH=$GOPATH:$GP
fi
export GOPATH
# echo "GOPATH SET: $GOPATH"


